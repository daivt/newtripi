import WebViewApp from '../WebViewApp';
import { renderSetup as baseRenderSetup, RenderOption } from '../__test__/testUtils';

export function renderSetup(options: RenderOption = {}) {
  return baseRenderSetup(undefined, WebViewApp);
}
