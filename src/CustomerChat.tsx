import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { useFacebook } from './fb/useFacebook';
import { PAGE_FB_ID } from './constants';

const CustomerChat = React.memo(function CustomerChat() {
  const timeoutRef = React.useRef(0);
  const theme = useTheme();

  // Initialize Facebook widget(s) in 2 seconds after
  // the component is mounted.
  useFacebook({ xfbml: false }, (FB: any) => {
    if (timeoutRef.current !== null) {
      timeoutRef.current = setTimeout(() => {
        const el = document.createElement('div');
        el.className = 'fb-customerchat';
        el.setAttribute('attribution', 'setup_tool');
        el.setAttribute('page_id', PAGE_FB_ID);
        el.setAttribute('ptheme_color', theme.palette.primary.main);
        // el.setAttribute('plogged_in_greeting', '...');
        // el.setAttribute('plogged_out_greeting', '...');
        // el.setAttribute('pgreeting_dialog_display', '...');
        // el.setAttribute('pgreeting_dialog_delay', '...');
        // el.setAttribute('pminimized', 'false');
        document.body.appendChild(el);
        FB.XFBML.parse();
      }, 2000);
    }
  });

  return null;
});

export default CustomerChat;
