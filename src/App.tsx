import { WithStyles, withStyles } from '@material-ui/core';
import React from 'react';
import Helmet from 'react-helmet';
import Loadable from 'react-loadable';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DINOGO, ROUTES } from './constants';
import Account from './modules/account/pages/Account';
import InviteMember from './modules/account/pages/InviteMember';
import AuthDialogs from './modules/auth/components/AuthDialogs';
import ForgotPassword from './modules/auth/pages/ForgotPassword';
import Login from './modules/auth/pages/Login';
import LoginAfterResetPassword from './modules/auth/pages/LoginAfterResetPassword';
import SignUp from './modules/auth/pages/SignUp';
import VerifyAgency from './modules/auth/pages/VerifyAgency';
import { closeAuthDialog } from './modules/auth/redux/authReducer';
import FlightBookingInfo from './modules/booking/pages/FlightBookingInfo';
import FlightPay from './modules/booking/pages/FlightPay';
import FlightReview from './modules/booking/pages/FlightReview';
import HotelBookingInfo from './modules/booking/pages/HotelBookingInfo';
import HotelPay from './modules/booking/pages/HotelPay';
import TourBookingInfo from './modules/booking/pages/TourBookingInfo';
import TourPay from './modules/booking/pages/TourPay';
import TrainBookingInfo from './modules/booking/pages/TrainBookingInfo';
import TrainPay from './modules/booking/pages/TrainPay';
import TrainReview from './modules/booking/pages/TrainReview';
import InitAndPathnameChangeAware from './modules/common/components/InitAndPathnameChangeAware';
import NetworkProblemDialog from './modules/common/components/NetworkProblemDialog';
import ProtectedRoute from './modules/common/components/ProtectedRoute';
import RedirectRoute from './modules/common/components/RedirectRoute';
import RouteLoading from './modules/common/components/RouteLoading';
import NotFound from './modules/common/pages/NotFound';
import PaymentFail from './modules/common/pages/PaymentFail';
import PaymentSuccess from './modules/common/pages/PaymentSuccess';
import { setNetworkError } from './modules/common/redux/reducer';
import Flight from './modules/home/pages/Flight';
import FlightHunt from './modules/home/pages/FlightHunt';
import Home from './modules/home/pages/Home';
import Hotel from './modules/home/pages/Hotel';
import SecurityPolicies from './modules/home/pages/SecurityPolicies';
import Terms from './modules/home/pages/Terms';
import Tour from './modules/home/pages/Tour';
import Train from './modules/home/pages/Train';
import Trung from './modules/home/pages/Trung';
import ConfirmInvitation from './modules/management/components/Staff/confirmInvitation';
import FlightResult from './modules/result/pages/FlightResult';
import FlightResultInbound from './modules/result/pages/FlightResultInbound';
import HotelDetail from './modules/result/pages/HotelDetail';
import HotelResult from './modules/result/pages/HotelResult';
import MyRewardDetail from './modules/result/pages/MyRewardDetail';
import MyRewards from './modules/result/pages/MyRewards';
import RewardDetail from './modules/result/pages/RewardDetail';
import Rewards from './modules/result/pages/Rewards';
import RewardsByCategory from './modules/result/pages/RewardsByCategory';
import TourDetail from './modules/result/pages/TourDetail';
import TourResult from './modules/result/pages/TourResult';
import TrainResult from './modules/result/pages/TrainResult';
import CreateTicketAlert from './modules/ticketAlert/pages/CreateTicketAlert';
import TicketAlert from './modules/ticketAlert/pages/TicketAlert';
import TicketAlertDetail from './modules/ticketAlert/pages/TicketAlertDetail';
import { AppState } from './redux/reducers';
import { bodyStyles } from './utils';
import MobileCard from './modules/booking/pages/MobileCard';
import MobileTopupPay from './modules/booking/pages/MobileTopupPay';
import MobileHistory from './modules/booking/pages/MobileHistory';
import MobileTopup from './modules/booking/pages/MobileTopup';
import MobileCardPay from './modules/booking/pages/MobileCardPay';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import './scss/slickOverride.scss';

const LoadableManagement = Loadable({
  loader: () => import('./modules/management/pages/Management'),
  loading: RouteLoading,
});
const LoadableFlightHuntResult = Loadable({
  loader: () => import('./modules/result/pages/FlightHuntResult'),
  loading: RouteLoading,
});

const LoadablePartnerPaymentSuccess = Loadable({
  loader: () => import('./modules/checkoutPartner/pages/PartnerPaymentSuccess'),
  loading: RouteLoading,
});

const mapStateToProps = (state: AppState) => ({
  auth: state.auth,
  authDialog: state.auth.authDialog,
  common: state.common,
  account: state.account,
  pathname: state.router.location.pathname,
});

interface Props extends ReturnType<typeof mapStateToProps>, WithStyles<typeof bodyStyles> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const App: React.FC<Props> = appProps => {
  const { auth, authDialog, common, dispatch, account, classes, pathname } = appProps;
  const { networkErrorMsg } = common;

  const isPaymentMobilePath = React.useMemo(() => {
    return (
      pathname.indexOf(ROUTES.partnerPayment.originPathSuccess) !== -1 ||
      pathname.indexOf(ROUTES.partnerPayment.originPathPaymentSuccess) !== -1
    );
  }, [pathname]);

  React.useEffect(() => {
    document.body.className = classes.body;
  }, [classes]);

  return (
    <>
      <Helmet
        link={[{ rel: 'shortcut icon', href: DINOGO ? '/static/dinogoFavicon.ico' : '/favicon.ico' }]}
        script={
          !isPaymentMobilePath
            ? [
                {
                  type: 'text/javascript',
                  innerHTML: `window.__lc = window.__lc || {};
                  window.__lc.license = 12057540;
                  ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},
                  once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",
                  c.call(arguments)])}, call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");
                  n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};
                  !n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))`,
                },
              ]
            : undefined
        }
      />
      <NetworkProblemDialog msgId={networkErrorMsg} onClose={() => dispatch(setNetworkError(undefined))} />
      <AuthDialogs authDialog={authDialog} close={() => dispatch(closeAuthDialog())} />
      <InitAndPathnameChangeAware>
        <Switch>
          <ProtectedRoute auth={auth.auth} path="/account" component={Account} />
          <Route exact path="/" component={Home} />
          <Route exact path={ROUTES.securityPolicies} component={SecurityPolicies} />

          <Route exact path={ROUTES.terms} component={Terms} />
          <Route exact path={ROUTES.termsOld} component={Terms} />

          <Route exact path={ROUTES.flight.flight} component={Flight} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightResult} component={FlightResult} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightResultInbound} component={FlightResultInbound} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightBookingInfo} component={FlightBookingInfo} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightReview} component={FlightReview} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightPay} component={FlightPay} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.train.trainResult} component={TrainResult} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.train.trainBookingInfo} component={TrainBookingInfo} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.train.review} component={TrainReview} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.train.trainPay} component={TrainPay} />
          <Route exact path={ROUTES.hotel.hotel} component={Hotel} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.hotel.hotelResult} component={HotelResult} />
          <Route
            path={ROUTES.hotel.hotelDetail}
            render={props => <HotelDetail key={`${window.location.href}&${auth.auth}`} {...props} />}
          />
          <Route
            path={ROUTES.hotel.hotelDetailWithSlug}
            render={props => <HotelDetail key={`${window.location.href}&${auth.auth}`} {...props} />}
          />
          <ProtectedRoute auth={auth.auth} path={ROUTES.hotel.hotelBookingInfo} component={HotelBookingInfo} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.hotel.hotelPay} component={HotelPay} />
          <Route exact path={ROUTES.tour.tour} component={Tour} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.tour.tourResult} component={TourResult} />
          <Route
            path={ROUTES.tour.tourDetail}
            render={props => <TourDetail key={`${window.location.href}&${auth.auth}`} {...props} />}
          />
          <ProtectedRoute auth={auth.auth} path={ROUTES.tour.tourBookingInfo} component={TourBookingInfo} />

          <Route exact path={ROUTES.train.train} component={Train} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.train.trainResult} component={TrainResult} />

          <ProtectedRoute auth={auth.auth} path={ROUTES.tour.tourPay} component={TourPay} />

          <Route path={ROUTES.reward.rewards} component={Rewards} />
          <Route path={ROUTES.reward.rewardsByCategory.value} component={RewardsByCategory} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.reward.myRewards} component={MyRewards} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.reward.myRewardDetail.value} component={MyRewardDetail} />
          <Route path={ROUTES.reward.rewardDetail.value} component={RewardDetail} />

          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightHunt} component={FlightHunt} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.flightHuntResult} component={LoadableFlightHuntResult} />

          <ProtectedRoute exact auth={auth.auth} path={ROUTES.management} component={LoadableManagement} />

          <ProtectedRoute exact auth={auth.auth} path={ROUTES.confirmInvitation} component={ConfirmInvitation} />

          <RedirectRoute auth={auth.auth} path={ROUTES.login} component={Login} />
          <RedirectRoute auth={auth.auth} path={ROUTES.loginAfterResetPass} component={LoginAfterResetPassword} />
          <Route path={ROUTES.signUp} render={props => <SignUp key={JSON.stringify(account.userData)} {...props} />} />
          <ProtectedRoute
            auth={auth.auth}
            path={ROUTES.verifyAgency}
            render={props => <VerifyAgency key={JSON.stringify(account.userData)} {...props} />}
          />
          <ProtectedRoute auth={auth.auth} path={ROUTES.mobile.mobileCard} component={MobileCard} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.mobile.mobileTopup} component={MobileTopup} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.mobile.mobileCardPay} component={MobileCardPay} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.mobile.mobileTopupPay} component={MobileTopupPay} />

          <ProtectedRoute auth={auth.auth} path={ROUTES.mobile.mobileHistory} component={MobileHistory} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.ticketAlert} component={TicketAlert} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.ticketAlertCreate} component={CreateTicketAlert} />
          <ProtectedRoute auth={auth.auth} path={ROUTES.flight.ticketAlertDetail.url} component={TicketAlertDetail} />

          <Route auth={auth.auth} path={ROUTES.paySuccess.value} component={PaymentSuccess} />
          <Route auth={auth.auth} path={ROUTES.payFailure.value} component={PaymentFail} />

          <ProtectedRoute auth={auth.auth} path={ROUTES.inviteMember} component={InviteMember} />

          <Route auth={auth.auth} path={ROUTES.partnerPayment.success} component={LoadablePartnerPaymentSuccess} />
          <Route
            auth={auth.auth}
            path={ROUTES.partnerPayment.paymentSuccess}
            component={LoadablePartnerPaymentSuccess}
          />

          <Route path={ROUTES.forgotPassword} render={props => <ForgotPassword {...props} />} />
          <Route path="/devTrung" render={() => <Trung />} />
          <Route component={NotFound} />
        </Switch>
      </InitAndPathnameChangeAware>
    </>
  );
};

export default connect(mapStateToProps)(withStyles(bodyStyles)(App));
