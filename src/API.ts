import { DEV, TEST, STAGING } from './constants';

enum APIServices {
  flight,
  hotel,
  tour,
  account,
  oldTour,
  train,
  general,
}

function getBaseUrl(service: APIServices) {
  if (service === APIServices.general) {
    return DEV
      ? '/api/general/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://flightapi.tripi.vn/v3';
  }

  if (service === APIServices.flight) {
    return DEV
      ? '/api/flight/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://flightapi.tripi.vn/v3';
  }

  if (service === APIServices.hotel) {
    return DEV
      ? '/api/hotel/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://hotelapi.tripi.vn/v3';
  }

  if (service === APIServices.tour) {
    return DEV
      ? '/api/tour/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://tourapi.tripi.vn/v3';
  }

  if (service === APIServices.account) {
    return DEV
      ? '/api/flight/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://flightapi.tripi.vn/v3';
  }

  if (service === APIServices.oldTour) {
    return DEV
      ? '/api/oldTour'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://tourapi.tripi.vn/';
  }

  if (service === APIServices.train) {
    return DEV
      ? '/api/train/'
      : TEST
      ? 'https://train-dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://trainapi.tripi.vn';
  }

  return null;
}

export const API_PATHS = {
  searchAirports: (term: string) =>
    `${getBaseUrl(APIServices.flight)}/flights/searchAirports?num_items=10&term=${encodeURIComponent(term)}`,
  searchAirlines: (term: string) =>
    `${getBaseUrl(APIServices.flight)}/flight/searchAirline?term=${encodeURIComponent(term)}`,
  searchOneWayTickets: `${getBaseUrl(APIServices.flight)}/flights/searchOneDirectionTickets`,
  searchTwoWayTickets: `${getBaseUrl(APIServices.flight)}/flights/searchRoundTripTickets`,
  flightGeneralInfo: `${getBaseUrl(APIServices.flight)}/flights/getGeneralInformation`,
  getTicketDetail: `${getBaseUrl(APIServices.flight)}/flights/getTicketDetail`,
  getInsurancePackage: `${getBaseUrl(APIServices.flight)}/insurance/getInsurancePackage`,
  validateInsuranceInfo: `${getBaseUrl(APIServices.flight)}/insurance/validateInsuranceInfo`,
  getFlightPaymentMethods: `${getBaseUrl(APIServices.flight)}/flight/getPaymentMethods`,
  bookTicket: `${getBaseUrl(APIServices.flight)}/flight/bookTicket`,
  getAllCountries: `${getBaseUrl(APIServices.flight)}/account/getAllCountries`,
  checkBookingVoidability: `${getBaseUrl(APIServices.flight)}/flights/checkBookingVoidability`,
  checkDividable: `${getBaseUrl(APIServices.flight)}/flight/checkDividable`,
  divideBooking: `${getBaseUrl(APIServices.flight)}/flight/divideBooking`,
  refundBooking: `${getBaseUrl(APIServices.flight)}/flights/refundBooking`,
  getTopDestinations: `${getBaseUrl(APIServices.flight)}/flight/getTopDestinations`,
  getFlightBookingInformation: `${getBaseUrl(APIServices.flight)}/flight/getBookingInformation`,

  ticketAlertGeneralInformation: `${getBaseUrl(APIServices.flight)}/tpa/personalInformation`,
  ticketAlert: `${getBaseUrl(APIServices.flight)}/tpa`,
  searchHotTickets: `${getBaseUrl(APIServices.flight)}/hotFne/searchHotTickets`,

  breakingNews: `${getBaseUrl(APIServices.hotel)}/homepage/breakingNews`,
  suggestLocation: (term: string) =>
    `${getBaseUrl(APIServices.hotel)}/hotels/suggest?maxItems=5&term=${encodeURIComponent(term)}`,
  popularDestinations: `${getBaseUrl(APIServices.hotel)}/activity/getPopularDestinations`,
  searchHotel: `${getBaseUrl(APIServices.hotel)}/hotels/search`,
  hotelDetail: `${getBaseUrl(APIServices.hotel)}/hotels/details`,
  hotelPrice: `${getBaseUrl(APIServices.hotel)}/hotels/prices`,
  bookHotel: `${getBaseUrl(APIServices.hotel)}/hotels/bookHotelRoom`,
  getHotelReviews: `${getBaseUrl(APIServices.hotel)}/hotels/getReviews`, // always use production, dev has no data
  getHotelBookingInformation: `${getBaseUrl(APIServices.hotel)}/hotel/getBookingInformation`,
  getHotelPaymentMethods: `${getBaseUrl(APIServices.hotel)}/checkout/getPaymentMethods`,
  getTopHotelLocation: (size: number) => `${getBaseUrl(APIServices.hotel)}/homepage/topHotelLocations?size=${size}`,
  getHotelBookingDetail: `${getBaseUrl(APIServices.hotel)}/account/getHotelBookingDetail`,

  login: `${getBaseUrl(APIServices.tour)}/account/loginV2`,
  sendNewDeviceLoginOTP: `${getBaseUrl(APIServices.tour)}/account/sendNewDeviceLoginOTP`,
  validateAccessToken: `${getBaseUrl(APIServices.tour)}/account/validateAccessToken`,
  updateProfile: `${getBaseUrl(APIServices.tour)}/account/update/metaData`,

  getPointPayment: `${getBaseUrl(APIServices.tour)}/payment/getPointPayment`,
  bookingsReport: `${getBaseUrl(APIServices.tour)}/booker/bookingsReport`,
  bookerDashboard: `${getBaseUrl(APIServices.tour)}/booker/dashboard`,
  bookerRequest: `${getBaseUrl(APIServices.tour)}/booker/getRequests`,
  cancelBookerRequest: `${getBaseUrl(APIServices.tour)}/booker/cancelRequest`,
  flightBookingsReport: `${getBaseUrl(APIServices.tour)}/booker/flightBookingsReport`,
  hotelBookingsReport: `${getBaseUrl(APIServices.tour)}/booker/hotelBookingsReport`,
  getProvinces: `${getBaseUrl(APIServices.tour)}/hotels/getProvinces`,
  getContactList: `${getBaseUrl(APIServices.tour)}/booker/contact/getContactList`,
  tourGeneralInfo: `${getBaseUrl(APIServices.tour)}/tours/getGeneralInformation`,
  tourAutoComplete: `${getBaseUrl(APIServices.tour)}/tours/autoComplete`,
  searchTourByTerm: `${getBaseUrl(APIServices.tour)}/tours/searchByTerm`,
  searchTourByLocation: `${getBaseUrl(APIServices.tour)}/tours/searchByDestination`,
  tourDetail: `${getBaseUrl(APIServices.tour)}/tours/tourDetails`,
  getTourReviews: `${getBaseUrl(APIServices.tour)}/tours/getComments`,
  getTourSuggest: `${getBaseUrl(APIServices.tour)}/tours/suggest`,
  tourPackage: `${getBaseUrl(APIServices.tour)}/activity/getActivityPackageAvailabilities`,
  getTourPaymentMethods: `${getBaseUrl(APIServices.tour)}/checkout/getTourPaymentMethods`,

  getTopTourLocation: `${getBaseUrl(APIServices.hotel)}/homepage/topTourDestinations?page=1&size=8`,
  getListLocationsInformation: `${getBaseUrl(APIServices.tour)}/getListLocationsInformation`,
  flightHuntSearch: `${getBaseUrl(APIServices.flight)}/hotFne/searchHotTickets`,

  getMobileData: `${getBaseUrl(APIServices.account)}/mobileTopup/getTopupValues`,
  getMobileHistoryData: `${getBaseUrl(APIServices.account)}/mobileTopup/getUserBookings`,
  getMobilePaymentMethods: `${getBaseUrl(APIServices.account)}/mobileTopup/getPaymentMethods`,
  getBookingInfoCode: `${getBaseUrl(APIServices.account)}/card/getBookingInfo`,
  getBookingInfoDiret: `${getBaseUrl(APIServices.account)}/mobileTopup/getBookingInfo`,
  getCardsCode: `${getBaseUrl(APIServices.hotel)}/mobileTopup/getCards`,
  getCardsDirect: `${getBaseUrl(APIServices.hotel)}/mobileTopup/topupMobile`,

  getPromotionPrograms: `${getBaseUrl(APIServices.tour)}/promotion/getPromotionPrograms`,
  getTourAvailabilities: `${getBaseUrl(APIServices.tour)}/tours/getTourAvailabilities`,
  bookTour: `${getBaseUrl(APIServices.tour)}/tours/bookTour`,

  logout: '/api/auth/account/logout',
  sendSignUpOTP: `${getBaseUrl(APIServices.tour)}/account/sendSignupOTP`,
  updateAgencyInformation: `${getBaseUrl(APIServices.tour)}/account/updateAgencyInformation`,
  verifyAgencyInformation: `${getBaseUrl(APIServices.tour)}/account/verifyAgencyInformation`,
  signUpV2: `${getBaseUrl(APIServices.tour)}/account/signupV2`,
  sendForgotPasswordOTP: `${getBaseUrl(APIServices.tour)}/account/sendForgotPasswordOTP`,
  forgotPasswordV2: `${getBaseUrl(APIServices.tour)}/account/forgotPasswordV2`,
  changePasswordNew: `${getBaseUrl(APIServices.tour)}/account/changePasswordNew`,
  upload: `${getBaseUrl(APIServices.tour)}/photos/upload`,

  getRewardsHistory: `${getBaseUrl(APIServices.flight)}/account/getRewardsHistory`,
  checkPromotion: `${getBaseUrl(APIServices.flight)}/promotion/checkCode`,
  getRewardsProgram: `${getBaseUrl(APIServices.flight)}/account/getRewardsProgram`,
  getMyLoyaltyInfo: `${getBaseUrl(APIServices.flight)}/account/getMyLoyaltyInfo`,
  getRewardsProgramDetail: `${getBaseUrl(APIServices.flight)}/account/getRewardsProgramDetail`,
  rewardPurchase: `${getBaseUrl(APIServices.flight)}/account/rewardPurchase`,
  getRewardsProgramByCategory: `${getBaseUrl(APIServices.flight)}/account/getRewardsProgramByCategory `,

  getFlightMarkupOfBooker: `${getBaseUrl(APIServices.account)}/flight/getFlightMarkupOfBooker`,

  saveFlightMarkupOfBooker: `${getBaseUrl(APIServices.account)}/flight/saveFlightMarkupOfBooker`,
  deleteFlightMarkupOfBooker: `${getBaseUrl(APIServices.account)}/flight/deleteFlightMarkupOfBooker`,
  getHotelMarkup: `${getBaseUrl(APIServices.account)}/booker/markupConfig/getHotelMarkup`,
  changeHotelMarkup: `${getBaseUrl(APIServices.account)}/booker/markupConfig/changeHotelMarkup`,

  getFlightBookings: `${getBaseUrl(APIServices.account)}/account/getFlightBookings`,
  getHotelBookings: `${getBaseUrl(APIServices.account)}/account/getHotelBookings`,
  getTourBookings: `${getBaseUrl(APIServices.account)}/account/getActivityBookings`,
  getTourBookingDetail: `${getBaseUrl(APIServices.account)}/account/getActivityBookingDetail`,
  deactivate: `${getBaseUrl(APIServices.account)}/account/booking/deactivate`,
  getFlightBookingDetail: `${getBaseUrl(APIServices.account)}/account/getFlightBookingDetail`,
  getBonusPointHistories: `${getBaseUrl(APIServices.account)}/account/getBonusPointHistories`,

  getMemberList: `${getBaseUrl(APIServices.account)}/booker/getMemberList`,
  inviteMember: `${getBaseUrl(APIServices.account)}/booker/inviteMember`,
  getMyInviteList: `${getBaseUrl(APIServices.account)}/booker/getMyInviteList`,
  getInviteLink: `${getBaseUrl(APIServices.account)}/booker/getInviteLink`,
  acceptInvite: `${getBaseUrl(APIServices.account)}/booker/acceptInvite`,
  deleteMyInvite: `${getBaseUrl(APIServices.account)}/booker/deleteMyInvite`,
  removeMember: `${getBaseUrl(APIServices.account)}/booker/removeMember`,
  getBookingListOfMember: `${getBaseUrl(APIServices.account)}/booker/getBookingListOfMember`,

  getInvoiceInfoHistories: `${getBaseUrl(APIServices.account)}/booker/GetInvoiceInfoHistories`,

  getVatinvoice: `${getBaseUrl(APIServices.account)}/vatInvoice/getVatInvoice`,
  getServicePackOptions: `${getBaseUrl(APIServices.account)}/booker/getServicePackOptions`,
  getEnterpriseInfo: `${getBaseUrl(APIServices.account)}/utils/getEnterpriseInfo`,
  requestVATInvoice: `${getBaseUrl(APIServices.account)}/booker/requestVATInvoice`,

  getBaggageForFlightBooking: `${getBaseUrl(APIServices.account)}/booker/getBaggageForFlightBooking`,
  getPaymentMethodsForAddingBaggages: `${getBaseUrl(APIServices.account)}/booker/getPaymentMethodsForAddingBaggages`,
  getPaymentMethodOfHoldingBooking: `${getBaseUrl(APIServices.account)}/booker/getPaymentMethodOfHoldingBooking`,
  addBaggagesForFlightBooking: `${getBaseUrl(APIServices.account)}/booker/addBaggagesForFlightBooking`,
  resendBookingSuccessEmail: `${getBaseUrl(APIServices.account)}/booker/resendBookingSuccessEmail`,
  calculateItineraryChangingPrice: `${getBaseUrl(APIServices.account)}/booker/calculateItineraryChangingPrice`,
  changeFlightItinerary: `${getBaseUrl(APIServices.account)}/booker/changeFlightItinerary`,
  paymentForHoldingBooking: `${getBaseUrl(APIServices.account)}/booker/paymentForHoldingBooking`,
  sendRequest: `${getBaseUrl(APIServices.account)}/booker/sendRequest`,

  getFlightTask: `${getBaseUrl(APIServices.account)}/booker/getFlightTask`,
  getBookingInformation: `${getBaseUrl(APIServices.account)}/booker/getBookingInformation`,

  exportContacts: `${getBaseUrl(APIServices.account)}/booker/exportContacts`,
  deleteContact: `${getBaseUrl(APIServices.account)}/booker/contact/deleteContact`,
  updateContact: `${getBaseUrl(APIServices.account)}/booker/contact/updateContact`,
  addContact: `${getBaseUrl(APIServices.account)}/booker/contact/addContact`,
  getContactTransactionHistory: `${getBaseUrl(APIServices.account)}/booker/contactTransactionHistory`,

  getNotificationList: `${getBaseUrl(APIServices.account)}/account/getNotificationList`,
  readAllNotification: `${getBaseUrl(APIServices.account)}/account/readAllNotification`,
  readNotification: `${getBaseUrl(APIServices.account)}/account/readNotification`,
  deleteNotification: `${getBaseUrl(APIServices.account)}/account/deleteNotification`,
  deleteAllNotification: `${getBaseUrl(APIServices.account)}/account/deleteAllNotification`,

  getActiveBankList: `${getBaseUrl(APIServices.account)}/credit/getActiveBankList`,
  getTopupHistories: `${getBaseUrl(APIServices.account)}/credit/getRequestTopupHistories`,
  requestTopup: `${getBaseUrl(APIServices.account)}/credit/requestTopup`,

  trainGeneralInfo: `${getBaseUrl(APIServices.train)}/train/allCategory?keyword=`,
  getTrainBookings: `${getBaseUrl(APIServices.train)}/train/bookingHistory`,
  getTrainBookingDetail: `${getBaseUrl(APIServices.train)}/train/getBookDetail`,
  holdSeat: `${getBaseUrl(APIServices.train)}/train/holdSeat`,
  bookTicketTrain: `${getBaseUrl(APIServices.train)}/train/bookTicket`,
  paymentMethodTrain: `${getBaseUrl(APIServices.train)}/train/paymentMethod`,
  payTrain: `${getBaseUrl(APIServices.train)}/train/payment`,
  trainSearch: `${getBaseUrl(APIServices.train)}/train/search`,
  getSeatByCarriage: `${getBaseUrl(APIServices.train)}/train/getSeatByCarriage`,

  checkoutSuccess: `${getBaseUrl(APIServices.general)}/smartlink/displayResult`,

  sendChangeCreditPasswordOtp: `${getBaseUrl(APIServices.general)}/sendChangeCreditPasswordOtp`,
  sendCreateCreditPasswordOtp: `${getBaseUrl(APIServices.general)}/sendCreateCreditPasswordOtp`,
  changeCreditPassword: `${getBaseUrl(APIServices.general)}/changeCreditPassword`,
  createNewCreditPassword: `${getBaseUrl(APIServices.general)}/createNewCreditPassword`,
  validateCreditPassword: `${getBaseUrl(APIServices.general)}/validateCreditPassword`,

  sendFeedback: `${getBaseUrl(APIServices.general)}/product/web/rating`,
};
