import { Theme } from '@material-ui/core';
import { StyleRulesCallback } from '@material-ui/styles';
import { replace } from 'connected-react-router';
import 'firebase/analytics';
import firebase from 'firebase/app';
import { matchPath } from 'react-router';
import { Action, AnyAction, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { AUTOTEST, DEV, ROUTES, TAX, TEST } from './constants';
import { setBookingStateFromParams } from './modules/booking/redux/flightBookingReducer';
import { setParams as hotelBookingSetParams } from './modules/booking/redux/hotelBookingReducer';
import { setBookingParams as tourBookingSetParams } from './modules/booking/redux/tourBookingReducer';
import { parseBookRoomParams, parseFlightBookingParams } from './modules/booking/utils';
import { REASON } from './modules/common/components/SendHomeReasonDialog';
import { HOTEL_FILTER_PARAM_NAMES, SEND_HOME_REASONS } from './modules/common/constants';
import {
  setDates as flightHuntSetDatesParams,
  setFilterParams as flightHuntSetFilterParams,
} from './modules/result/redux/flightHuntResultReducer';
import { setFilter as setHotelFilter, setSortBy } from './modules/result/redux/hotelResultReducer';
import { setFilter as setTourFilter } from './modules/result/redux/tourResultReducer';
import { parseHotelDetailParams, parseTourDetailParams } from './modules/result/utils';
import { setParams as flightHuntSetParams } from './modules/search/redux/flightHuntSearchReducer';
import { setParams as flightSetParams } from './modules/search/redux/flightSearchReducer';
import { setParams as hotelSetParams } from './modules/search/redux/hotelSearchReducer';
import { setParams as tourSetParams } from './modules/search/redux/tourSearchReducer';
import { setParams as trainSetParams } from './modules/search/redux/trainSearchReducer';
import {
  parseFlightHuntDateParams,
  parseFlightHuntFilterParams,
  parseFlightHuntSearchParams,
  parseFlightSearchParams,
  parseHotelFilterParams,
  parseHotelSearchParams,
  parseTourFilterParams,
  parseTourSearchParams,
  parseTrainSearchState,
} from './modules/search/utils';
import { AppState } from './redux/reducers';

export const bodyStyles: StyleRulesCallback<Theme, {}> = theme => ({
  body: {
    fontSize: theme.typography.body2.fontSize,
    lineHeight: theme.typography.body2.lineHeight,
    color: theme.palette.text.primary,
    overflowY: 'scroll',
  },
});

export function loadParamsToStore(
  dispatch: Dispatch<AnyAction>,
  params = new URLSearchParams(window.location.search),
  pathname = window.location.pathname,
) {
  try {
    if (matchPath(pathname, ROUTES.flight.flightResult)) {
      const state = parseFlightSearchParams(params);
      dispatch(flightSetParams(state));
    } else if (matchPath(pathname, ROUTES.flight.flightResultInbound)) {
      const state = parseFlightSearchParams(params);
      dispatch(flightSetParams(state));
    } else if (matchPath(pathname, ROUTES.flight.flightBookingInfo)) {
      const state = parseFlightBookingParams(params);

      dispatch(setBookingStateFromParams(state));
    } else if (matchPath(pathname, ROUTES.hotel.hotelResult)) {
      const state = parseHotelSearchParams(params);
      const filter = parseHotelFilterParams(params);
      dispatch(hotelSetParams(state));
      dispatch(setHotelFilter(filter, true));

      const sortBy = params.get(HOTEL_FILTER_PARAM_NAMES.sort);
      if (sortBy) {
        dispatch(setSortBy(sortBy, true));
      }
    } else if (matchPath(pathname, ROUTES.hotel.hotelDetail) || matchPath(pathname, ROUTES.hotel.hotelDetailWithSlug)) {
      const state = parseHotelDetailParams(params);
      dispatch(hotelBookingSetParams(state));
    } else if (matchPath(pathname, ROUTES.hotel.hotelBookingInfo)) {
      const state = parseBookRoomParams(params);
      dispatch(hotelBookingSetParams(state));
    } else if (matchPath(pathname, ROUTES.tour.tourResult)) {
      const state = parseTourSearchParams(params);
      const filter = parseTourFilterParams(params);
      dispatch(tourSetParams(state));
      dispatch(setTourFilter(filter, true));
    } else if (matchPath(pathname, ROUTES.tour.tourDetail) || matchPath(pathname, ROUTES.tour.tourBookingInfo)) {
      const state = parseTourDetailParams(params);
      dispatch(tourBookingSetParams(state));
    } else if (matchPath(pathname, ROUTES.flight.flightHuntResult)) {
      const state = parseFlightHuntSearchParams(params);
      const filter = parseFlightHuntFilterParams(params);
      const date = parseFlightHuntDateParams(params);
      dispatch(flightHuntSetFilterParams(filter));
      dispatch(flightHuntSetParams(state));
      dispatch(flightHuntSetDatesParams(date.departureDate, date.returnDate));
    } else if (pathname === ROUTES.train.trainResult) {
      const state = parseTrainSearchState(params);
      dispatch(trainSetParams(state));
    } else if (matchPath(pathname, ROUTES.login)) {
      const fromParamValue = params.get('from');
      if (fromParamValue) {
        const url = new URL(`https://tripi.vn${fromParamValue}`);
        loadParamsToStore(dispatch, new URLSearchParams(url.search), url.pathname);
      }
    }
  } catch (err) {
    dispatch(
      replace({
        pathname: '/',
        search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidLink)}`,
      }),
    );
  }
}

export function roundUp(num: number, step: number) {
  return Math.ceil(num / step) * step;
}
export function calculateTax(num: number) {
  return Math.round((num * TAX) / (100 + TAX));
}

export function millisecondsToHour(milliseconds: number) {
  if (milliseconds <= 0) {
    return 0;
  }

  return Math.floor(milliseconds / 3600000);
}
export function scrollTo(id: string, offsetTop: number) {
  const el = document.getElementById(id);
  if (el) {
    window.scrollTo({ top: el.offsetTop - offsetTop, behavior: 'smooth' });
  }
}

export type ThunkController = {
  cancelled: boolean;
};

export function makeCancelOldThunk<A extends any[], R>(
  creator: (controller: ThunkController, ...args: A) => ThunkAction<R, AppState, null, Action<string>>,
) {
  let controller: ThunkController = { cancelled: false };
  return (...args: A): ThunkAction<R, AppState, null, Action<string>> => {
    controller.cancelled = true;
    controller = { cancelled: false };
    return creator(controller, ...args);
  };
}

export const analyticsRef: { current?: firebase.analytics.Analytics } = {};
if (!DEV && !TEST && !AUTOTEST) {
  try {
    firebase.initializeApp({
      apiKey: 'AIzaSyAjoB71f7WyaUJhHlYOb_9yaLIQvgW1X8Y',
      authDomain: 'sonic-trail-91902.firebaseapp.com',
      databaseURL: 'https://sonic-trail-91902.firebaseio.com',
      projectId: 'sonic-trail-91902',
      storageBucket: 'sonic-trail-91902.appspot.com',
      messagingSenderId: '125756005771',
      appId: '1:125756005771:web:117d1ce186b88e5772e38e',
      measurementId: 'G-8RQDCDB9PT',
    });
    analyticsRef.current = firebase.analytics();
  } catch (e) {
    console.error(e);
  }
}
