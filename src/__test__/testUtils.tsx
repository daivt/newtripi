import { MuiThemeProvider } from '@material-ui/core';
import { render } from '@testing-library/react';
import { ConnectedRouter, replace } from 'connected-react-router';
import { merge } from 'lodash';
import { SnackbarProvider } from 'notistack';
import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import App from '../App';
import { some } from '../constants';
import ConnectedIntlProvider from '../modules/intl/components/ConnectedIntlProvider';
import { setLocale } from '../modules/intl/redux/reducer';
import { configureStore, history } from '../redux/configureStore';
import { MUI_THEME, THEME } from '../setupTheme';

export interface RenderOption {
  width?: number;
  lang?: string;
  pathname?: string;
  initialAppState?: some;
}

export function renderSetup(options: RenderOption = {}, AppComponent: React.ComponentType = App) {
  const mergedOptions = merge({ width: 1440, lang: 'vi', pathname: '/', initialAppState: {} }, options);
  (window as any).innerWidth = mergedOptions.width;

  const store = configureStore(mergedOptions.initialAppState);
  store.dispatch(setLocale(mergedOptions.lang));
  store.dispatch(replace(mergedOptions.pathname));

  const queries = render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ConnectedIntlProvider>
          <ThemeProvider theme={THEME}>
            <MuiThemeProvider theme={MUI_THEME}>
              <SnackbarProvider maxSnack={3}>
                <AppComponent />
              </SnackbarProvider>
            </MuiThemeProvider>
          </ThemeProvider>
        </ConnectedIntlProvider>
      </ConnectedRouter>
    </Provider>,
  );

  return { queries, store };
}
