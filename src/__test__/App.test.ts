import { cleanup } from '@testing-library/react';
import fetchMock from 'fetch-mock';
import { install } from 'lolex';
import { API_PATHS } from '../API';
import breakingNews from './json/breakingNews.json';
import popularDestinations from './json/getPopularDestinations.json';
import { renderSetup } from './testUtils';

describe('Homepage', () => {
  let clock: ReturnType<typeof install>;

  beforeAll(() => {
    clock = install({ now: Date.parse('2020/01/01') });
    jest.setTimeout(2 * 60000);
  });

  afterEach(() => {
    cleanup();
  });

  it('Homepage tablet desktop', async pass => {
    fetchMock.getOnce(API_PATHS.breakingNews, breakingNews);
    fetchMock.getOnce(API_PATHS.popularDestinations, popularDestinations);

    const { queries } = renderSetup();

    await queries.findByText('Vé máy bay', { trim: true });
    pass();
  });

  it('404 page', async pass => {
    const { queries } = renderSetup({ pathname: '/something' });

    await queries.findByText('404');
    pass();
  });
});
