import { fireEvent } from '@testing-library/dom';
import { cleanup } from '@testing-library/react';
import fetchMock from 'fetch-mock';
import { install } from 'lolex';
import { API_PATHS } from '../../../API';
import breakingNews from '../../json/breakingNews.json';
import flightGeneralInfo from '../../json/flightGeneralInfo.json';
import popularDestinations from '../../json/getPopularDestinations.json';
import { renderSetup } from '../../testUtils';

describe('Flight search', () => {
  let clock: ReturnType<typeof install>;

  beforeAll(() => {
    clock = install({ now: Date.parse('2020/01/01') });
    jest.setTimeout(2 * 60000);
  });

  afterEach(() => {
    cleanup();
  });

  it('Flight search tablet desktop', async pass => {
    fetchMock.getOnce(API_PATHS.breakingNews, breakingNews);
    fetchMock.getOnce(API_PATHS.popularDestinations, popularDestinations);

    const { queries } = renderSetup();

    const tab = await queries.findByText('Vé máy bay');

    fetchMock.getOnce(API_PATHS.flightGeneralInfo, flightGeneralInfo);
    fireEvent.click(tab);

    const origin = await queries.findByPlaceholderText('Chọn điểm đi');
    fireEvent.focus(origin);

    queries.getByText('Hà Nội (HAN)', { exact: false });
    pass();
  });
});
