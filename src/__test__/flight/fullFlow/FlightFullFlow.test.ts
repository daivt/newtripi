import { fireEvent, waitForDomChange, within } from '@testing-library/dom';
import { cleanup } from '@testing-library/react';
import { deepStrictEqual } from 'assert';
import fetchMock from 'fetch-mock';
import { install } from 'lolex';
import { matchPath } from 'react-router';
import { API_PATHS } from '../../../API';
import { ROUTES } from '../../../constants';
import breakingNews from '../../json/breakingNews.json';
import allCountries from '../../json/flight/fullFlow/allCountries.json';
import contactList from '../../json/flight/fullFlow/contactList.json';
import insurance from '../../json/flight/fullFlow/insurance.json';
import login from '../../json/flight/fullFlow/login.json';
import pay from '../../json/flight/fullFlow/pay.json';
import searchResults from '../../json/flight/fullFlow/result.json';
import ticketDetail from '../../json/flight/fullFlow/ticketDetail.json';
import validate from '../../json/flight/fullFlow/validate.json';
import flightGeneralInfo from '../../json/flightGeneralInfo.json';
import popularDestinations from '../../json/getPopularDestinations.json';
import { renderSetup } from '../../testUtils';

describe('Flight full flow', () => {
  let clock: ReturnType<typeof install>;

  beforeAll(() => {
    clock = install({ now: Date.parse('2020/01/01') });
    jest.setTimeout(2 * 60000);
  });

  afterAll(() => {
    jest.setTimeout(5000);
    clock.uninstall();
  });

  afterEach(() => {
    cleanup();
  });

  it('Flight full flow', async done => {
    fetchMock.getOnce(API_PATHS.breakingNews, breakingNews);
    fetchMock.getOnce(API_PATHS.popularDestinations, popularDestinations);

    const { queries, store } = renderSetup();

    fetchMock.getOnce(API_PATHS.flightGeneralInfo, flightGeneralInfo);

    const tab = await queries.findByText('Vé máy bay');
    fireEvent.click(tab);

    let search = store.getState().search.flight;
    let { origin, destination } = search;

    expect(origin).toBeUndefined();
    expect(destination).toBeUndefined();

    fireEvent.focus(queries.getByPlaceholderText('Chọn điểm đi'));

    const hanoi = queries.getByText('(HAN)', { exact: false });
    fireEvent.click(hanoi);

    const saigon = queries.getByText('(SGN)', { exact: false });
    fireEvent.click(saigon);

    search = store.getState().search.flight;
    origin = search.origin;
    destination = search.destination;

    expect(origin!.code).toEqual('HAN');
    expect(destination!.code).toEqual('SGN');

    search = store.getState().search.flight;
    expect(search.departureDate.format('YYYY/MM/DD')).toEqual('2020/01/01');
    expect(search.returnDate).toBeUndefined();

    queries.getByText('01/01/2020');

    search = store.getState().search.flight;

    fireEvent.click(queries.getByRole('checkbox'));
    search = store.getState().search.flight;
    expect(search.returnDate!.format('YYYY/MM/DD')).toEqual('2020/01/04');

    const jan9 = queries
      .getAllByText('9')
      .filter(day => day.attributes.getNamedItem('aria-disabled')!.value === 'false')[0];
    fireEvent.click(jan9);
    search = store.getState().search.flight;
    expect(search.departureDate.format('YYYY/MM/DD')).toEqual('2020/01/09');

    const jan15 = queries
      .getAllByText('15')
      .filter(day => day.attributes.getNamedItem('aria-disabled')!.value === 'false')[0];
    fireEvent.click(jan15);
    search = store.getState().search.flight;
    expect(search.returnDate!.format('YYYY/MM/DD')).toEqual('2020/01/15');

    const completeButton = queries.getByText('Hoàn thành');
    fireEvent.click(completeButton);

    const passengersGroup = queries.getAllByRole('group')[1];

    fireEvent.focus(passengersGroup);
    fireEvent.click(await within(passengersGroup).findByText('Hạng Nhất'));
    fireEvent.click(within(passengersGroup).getByText('Thương Gia'));

    search = store.getState().search.flight;
    expect(search.seatClass.length).toBe(2);

    expect(search.seatClass.filter(seat => seat.code === 'business' || seat.code === 'first').length).toBe(2);

    const addAdult = within(passengersGroup).getAllByRole('button')[5];
    fireEvent.click(addAdult);
    search = store.getState().search.flight;
    expect(search.travellerCountInfo.adultCount).toBe(2);

    const addChild = within(passengersGroup).getAllByRole('button')[7];
    fireEvent.click(addChild);
    search = store.getState().search.flight;
    expect(search.travellerCountInfo.childCount).toBe(1);

    const addBaby = within(passengersGroup).getAllByRole('button')[9];
    fireEvent.click(addBaby);
    search = store.getState().search.flight;
    expect(search.travellerCountInfo.babyCount).toBe(1);

    fireEvent.click(within(passengersGroup).getAllByRole('button')[10]);
    fireEvent.click(queries.getAllByRole('search')[0]!);

    // Login page
    expect(matchPath(store.getState().router.location.pathname, ROUTES.login)).toBeTruthy();

    fireEvent.change(queries.getByPlaceholderText('Ví dụ: 0901234567', { exact: false }), {
      target: { value: '0904391987' },
    });
    fireEvent.change(queries.getByPlaceholderText('***', { exact: false }), {
      target: { value: 'password' },
    });

    fetchMock.postOnce(API_PATHS.login, login.login);
    fetchMock.getOnce(API_PATHS.bookerDashboard, login.dashboard);
    fetchMock.post(API_PATHS.getNotificationList, login.notificationList);
    fetchMock.postOnce(API_PATHS.bookerRequest, login.getRequests);
    fetchMock.postOnce(API_PATHS.bookingsReport, login.bookingReport);

    let searchCount = 0;
    fetchMock.post(
      API_PATHS.searchTwoWayTickets,
      () => {
        if (searchCount === 0) {
          searchCount += 1;
          return searchResults['1'];
        }
        if (searchCount === 1) {
          searchCount += 1;
          return searchResults['2'];
        }
        if (searchCount === 2) {
          searchCount += 1;
          return searchResults['3'];
        }
        return null;
      },
      { repeat: 3 },
    );

    fireEvent.click(queries.getByText('Đăng nhập')!);

    // Result page
    await queries.findByText('71 kết quả', { exact: false });
    expect(matchPath(store.getState().router.location.pathname, ROUTES.flight.flightResult)).toBeTruthy();

    const lastSearchRequest = JSON.parse(fetchMock.calls(API_PATHS.searchTwoWayTickets)![0][1]!.body as string);
    expect(lastSearchRequest.depart).toEqual('09-01-2020');
    expect(lastSearchRequest.return).toEqual('15-01-2020');
    expect(lastSearchRequest.from_airport).toEqual('HAN');
    expect(lastSearchRequest.to_airport).toEqual('SGN');
    expect(lastSearchRequest.num_adults).toEqual('2');
    expect(lastSearchRequest.num_childs).toEqual('1');
    expect(lastSearchRequest.num_infants).toEqual('1');
    expect(lastSearchRequest.filters.ticketClassCodes[0]).toEqual('first');
    expect(lastSearchRequest.filters.ticketClassCodes[1]).toEqual('business');

    const pickVna = queries.getAllByText('Chọn vé này')[2]!;

    fireEvent.click(pickVna);

    // Inbound result page
    expect(matchPath(store.getState().router.location.pathname, ROUTES.flight.flightResultInbound)).toBeTruthy();

    const pickBamboo = queries.getAllByText('Đặt vé')[1]!;
    fireEvent.click(pickBamboo);

    let getTicketCount = 0;
    fetchMock.post(
      API_PATHS.getTicketDetail,
      () => {
        if (getTicketCount === 0) {
          getTicketCount += 1;
          return ticketDetail.res1;
        }
        if (getTicketCount === 1) {
          return ticketDetail.res2;
        }
        return null;
      },
      { repeat: 2 },
    );
    fetchMock.getOnce(API_PATHS.getAllCountries, allCountries, { repeat: 2 });
    fetchMock.postOnce(API_PATHS.getInsurancePackage, insurance.res, { body: insurance.req });
    fireEvent.click(queries.getByText('Tiếp tục')!);
    fetchMock.get(API_PATHS.validateAccessToken, validate.accessToken);

    // Booking info page
    expect(matchPath(store.getState().router.location.pathname, ROUTES.flight.flightBookingInfo)).toBeTruthy();

    const familyNamesInputs = await queries.findAllByPlaceholderText('Ví dụ: Nguyen');
    const givenNamesInputs = queries.getAllByPlaceholderText('Ví dụ: Van Anh');

    expect(familyNamesInputs.length).toBe(4);
    expect(givenNamesInputs.length).toBe(4);

    expect(queries.queryByText('Tìm khách hàng')).toBeFalsy();

    fetchMock.postOnce(API_PATHS.getContactList, contactList);
    const addFromListButton = queries.getAllByText('Thêm từ danh bạ')[0]!;
    fireEvent.click(addFromListButton);

    const tel = await queries.findByText('0904868228');
    fireEvent.click(tel.closest('div')!);
    expect(store.getState().booking.flight.booker!.email).toBeTruthy();
    clock.tick(1000); // wait for the dialog to be closed

    expect(queries.queryByText('Tìm khách hàng')).toBeFalsy();

    fireEvent.change(familyNamesInputs[0], { target: { value: 'Ronaldo' } });
    fireEvent.change(givenNamesInputs[0], { target: { value: 'Cristiano' } });

    fireEvent.change(familyNamesInputs[1], { target: { value: 'Messi' } });
    fireEvent.change(givenNamesInputs[1], { target: { value: 'Lionel' } });

    fireEvent.change(familyNamesInputs[2], { target: { value: 'Gilman' } });
    fireEvent.change(givenNamesInputs[2], { target: { value: 'Billy' } });
    fireEvent.change(queries.getAllByPlaceholderText('DD/MM/YYYY')[0], {
      target: { value: '01/01/2015' },
    });

    fireEvent.change(familyNamesInputs[3], { target: { value: 'Swift' } });
    fireEvent.change(givenNamesInputs[3], { target: { value: 'Taylor' } });
    fireEvent.change(queries.getAllByPlaceholderText('DD/MM/YYYY')[1], {
      target: { value: '01/01/2019' },
    });

    fetchMock.postOnce(API_PATHS.validateInsuranceInfo, validate.insurance);

    fireEvent.click(queries.getByText('Tiếp tục')!);

    // Review page
    await queries.findByText('Kiểm tra thông tin');
    expect(matchPath(store.getState().router.location.pathname, ROUTES.flight.flightReview)).toBeTruthy();

    fetchMock.postOnce(API_PATHS.getFlightPaymentMethods, pay.methods);
    fetchMock.postOnce(API_PATHS.getPointPayment, pay.pointPayment);
    fireEvent.click(queries.getByText('Tới thanh toán')!);

    // Payment page
    await queries.findByText('Thanh toán');

    const payButton = queries.getAllByText('Thanh toán')[1]!;

    expect(payButton.closest('button')!.disabled).toBeTruthy();
    fireEvent.click(queries.getAllByRole('checkbox')[1]);

    expect(payButton.closest('button')!.disabled).toBeFalsy();

    fireEvent.click(payButton);

    fireEvent.click(queries.getByText('Xác nhận'));

    fireEvent.change(queries.getByPlaceholderText(''), { target: { value: '123456' } });
    clock.tick(500); // for debounce (to be removed)

    // Click on the second button;
    fetchMock.postOnce(API_PATHS.validateCreditPassword, validate.pass);
    fetchMock.postOnce(API_PATHS.bookTicket, pay.bookRes);
    fireEvent.click(queries.getAllByText('Thanh toán')[2]);

    await waitForDomChange();

    const bookTicketBody = JSON.parse(fetchMock.calls(API_PATHS.bookTicket)![0][1]!.body as string);
    deepStrictEqual(bookTicketBody, pay.bookReq);

    done();
  });
});
