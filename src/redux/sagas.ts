import { fork, takeEvery } from 'redux-saga/effects';
import { getType } from 'typesafe-actions';
import { inAction } from '../modules/auth/redux/authReducer';
import { loggedInFollowUp } from '../modules/auth/redux/sagas';
import {
  watchFlightHuntAirlinesChange,
  watchHotelDetailParamsChange,
  watchHotelFilterSortingChange,
  watchTourFilterSortByChange,
} from '../modules/result/redux/sagas';

export default function* rootSaga() {
  yield fork(watchHotelFilterSortingChange);
  yield fork(watchHotelDetailParamsChange);
  yield fork(watchTourFilterSortByChange);
  yield fork(watchFlightHuntAirlinesChange);
  yield takeEvery(getType(inAction), loggedInFollowUp);
}
