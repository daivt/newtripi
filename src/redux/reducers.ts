import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';
import { Action, combineReducers } from 'redux';
import { PersistConfig, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage/session';
import { createAction, getType } from 'typesafe-actions';
import accountReducer, {
  AccountState,
  DEFAULT_ACCOUNT_STATE,
} from '../modules/account/redux/accountReducer';
import authReducer, { AuthState } from '../modules/auth/redux/authReducer';
import bookingReducer, {
  BookingState,
  DEFAULT_BOOKING_STATE,
} from '../modules/booking/redux/reducer';
import commonReducer, { CommonState } from '../modules/common/redux/reducer';
import intlReducer, { IntlState } from '../modules/intl/redux/reducer';
import managementReducer, {
  DEFAULT_MANAGEMENT_STATE,
  ManagementState,
} from '../modules/management/redux/reducer';
import resultReducer, { DEFAULT_RESULT_STATE, ResultState } from '../modules/result/redux/reducer';
import searchReducer, { SearchState } from '../modules/search/redux/reducer';
import ticketAlertReducer, {
  TicketAlertState,
} from '../modules/ticketAlert/redux/ticketAlertReducer';
import { BookingTransform, ResultTransform, SearchTransform } from './persistTransforms';

const persistConfig: PersistConfig = {
  storage,
  key: 'root',
  transforms: [SearchTransform, ResultTransform, BookingTransform],
  blacklist: ['router', 'intl'],
};

export interface AppState {
  router: RouterState;
  auth: AuthState;
  account: AccountState;
  common: CommonState;
  intl: IntlState;
  search: SearchState;
  result: ResultState;
  booking: BookingState;
  management: ManagementState;
  ticketAlert: TicketAlertState;
}

export const clearStoreAfterLogout = createAction('clearStoreAfterLogout');

export default (history: History) => {
  const rawRootReducer = combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    account: accountReducer,
    common: commonReducer,
    intl: intlReducer,
    search: searchReducer,
    result: resultReducer,
    booking: bookingReducer,
    management: managementReducer,
    ticketAlert: ticketAlertReducer,
  });

  return persistReducer(
    persistConfig,
    (state: AppState | undefined, action: Action<string>): AppState => {
      if (state && action.type === getType(clearStoreAfterLogout)) {
        return rawRootReducer(
          {
            ...state,
            account: DEFAULT_ACCOUNT_STATE,
            result: DEFAULT_RESULT_STATE,
            booking: DEFAULT_BOOKING_STATE,
            management: DEFAULT_MANAGEMENT_STATE,
            ticketAlert: {},
          },
          action,
        );
      }
      return rawRootReducer(state, action);
    },
  );
};
