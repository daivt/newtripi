import { createTransform } from 'redux-persist';
import moment from 'moment';
import { FlightSearchParamsState } from '../modules/search/redux/flightSearchReducer';
import { HotelSearchParamsState } from '../modules/search/redux/hotelSearchReducer';
import { FlightResultState } from '../modules/result/redux/flightResultReducer';
import { HotelResultState } from '../modules/result/redux/hotelResultReducer';
import { HotelBookingState } from '../modules/booking/redux/hotelBookingReducer';
import { FlightBookingState } from '../modules/booking/redux/flightBookingReducer';
import { TourSearchParamsState } from '../modules/search/redux/tourSearchReducer';
import { TourResultState } from '../modules/result/redux/tourResultReducer';
import { TourBookingState } from '../modules/booking/redux/tourBookingReducer';
import { MobileState } from '../modules/booking/redux/mobileReducer';
import { FlightHuntSearchState } from '../modules/search/redux/flightHuntSearchReducer';
import { FlightHuntResultState } from '../modules/result/redux/flightHuntResultReducer';
import { TrainSearchParamsState } from '../modules/search/redux/trainSearchReducer';
import { TrainBookingState } from '../modules/booking/redux/trainBookingReducer';

export const SearchTransform = createTransform(
  // transform state on its way to being serialized and persisted.
  (
    inboundState: {
      flight: FlightSearchParamsState;
      train: TrainSearchParamsState;
      hotel: HotelSearchParamsState;
      tour: TourSearchParamsState;
      flightHunt: FlightHuntSearchState;
    },
    key,
  ) => {
    return {
      flight: {
        ...inboundState.flight,
        departureDate: inboundState.flight.departureDate.toDate().getTime(),
        returnDate: inboundState.flight.returnDate
          ? inboundState.flight.returnDate.toDate().getTime()
          : undefined,
      },
      train: {
        ...inboundState.train,
        departureDate: inboundState.train.departureDate.toDate().getTime(),
        returnDate: inboundState.train.returnDate
          ? inboundState.train.returnDate.toDate().getTime()
          : undefined,
      },
      hotel: {
        ...inboundState.hotel,
        checkIn: inboundState.hotel.checkIn.toDate().getTime(),
        checkOut: inboundState.hotel.checkOut.toDate().getTime(),
      },
      tour: inboundState.tour,
      flightHunt: inboundState.flightHunt,
    };
  },
  // transform state being rehydrated
  (outboundState, key) => {
    return {
      flight: {
        ...outboundState.flight,
        departureDate: moment(outboundState.flight.departureDate),
        returnDate:
          outboundState.flight.returnDate !== undefined
            ? moment(outboundState.flight.returnDate)
            : undefined,
      },
      train: {
        ...outboundState.train,
        departureDate: moment(outboundState.train.departureDate),
        returnDate:
          outboundState.train.returnDate !== undefined
            ? moment(outboundState.train.returnDate)
            : undefined,
      },
      hotel: {
        ...outboundState.hotel,
        checkIn: moment(outboundState.hotel.checkIn),
        checkOut: moment(outboundState.hotel.checkOut),
      },
      tour: outboundState.tour,
      flightHunt: outboundState.flightHunt,
    };
  },
  // define which reducers this transform gets called for.
  { whitelist: ['search'] },
);

export const ResultTransform = createTransform(
  // transform state on its way to being serialized and persisted.
  (
    inboundState: {
      flight: FlightResultState;
      hotel: HotelResultState;
      tour: TourResultState;
      flightHunt: FlightHuntResultState;
    },
    key,
  ) => {
    return {
      flight: {
        ...inboundState.flight,
        searchParams: inboundState.flight.searchParams
          ? {
              ...inboundState.flight.searchParams,
              departureDate: inboundState.flight.searchParams.departureDate.toDate().getTime(),
              returnDate: inboundState.flight.searchParams.returnDate
                ? inboundState.flight.searchParams.returnDate.toDate().getTime()
                : undefined,
            }
          : undefined,
      },
      hotel: {
        ...inboundState.hotel,
        lastParams: inboundState.hotel.lastParams
          ? {
              ...inboundState.hotel.lastParams,
              searchParams: {
                ...inboundState.hotel.lastParams.searchParams,
                checkIn: inboundState.hotel.lastParams.searchParams.checkIn.toDate().getTime(),
                checkOut: inboundState.hotel.lastParams.searchParams.checkOut.toDate().getTime(),
              },
            }
          : undefined,
      },
      tour: inboundState.tour,
      flightHunt: inboundState.flightHunt,
    };
  },
  // transform state being rehydrated
  (outboundState, key) => {
    return {
      flight: {
        ...outboundState.flight,
        searchParams: outboundState.flight.searchParams
          ? {
              ...outboundState.flight.searchParams,
              departureDate: moment(outboundState.flight.searchParams.departureDate),
              returnDate: outboundState.flight.searchParams.returnDate
                ? moment(outboundState.flight.searchParams.returnDate)
                : undefined,
            }
          : undefined,
      },
      hotel: {
        ...outboundState.hotel,
        lastParams: outboundState.hotel.lastParams
          ? {
              ...outboundState.hotel.lastParams,
              searchParams: {
                ...outboundState.hotel.lastParams.searchParams,
                checkIn: moment(outboundState.hotel.lastParams.searchParams.checkIn),
                checkOut: moment(outboundState.hotel.lastParams.searchParams.checkOut),
              },
            }
          : undefined,
      },
      tour: outboundState.tour,
      flightHunt: outboundState.flightHunt,
    };
  },
  // define which reducers this transform gets called for.
  { whitelist: ['result'] },
);

export const BookingTransform = createTransform(
  // transform state on its way to being serialized and persisted.
  (
    inboundState: {
      flight: FlightBookingState;
      hotel: HotelBookingState;
      tour: TourBookingState;
      mobile: MobileState;
      train: TrainBookingState;
    },
    key,
  ) => {
    return {
      flight: {
        ...inboundState.flight,
      },
      hotel: {
        ...inboundState.hotel,
        params: inboundState.hotel.params
          ? {
              ...inboundState.hotel.params,
              checkIn: inboundState.hotel.params.checkIn.toDate().getTime(),
              checkOut: inboundState.hotel.params.checkOut.toDate().getTime(),
            }
          : undefined,
      },
      tour: {
        ...inboundState.tour,
        date: inboundState.tour.date ? inboundState.tour.date.toDate().getTime() : undefined,
      },
      mobile: inboundState.mobile,
      train: {
        ...inboundState.train,
      },
    };
  },
  // transform state being rehydrated
  (outboundState, key) => {
    return {
      flight: {
        ...outboundState.flight,
      },
      hotel: {
        ...outboundState.hotel,
        params: outboundState.hotel.params
          ? {
              ...outboundState.hotel.params,
              checkIn: moment(outboundState.hotel.params.checkIn),
              checkOut: moment(outboundState.hotel.params.checkOut),
            }
          : undefined,
      },
      tour: {
        ...outboundState.tour,
        date: outboundState.tour.date ? moment(outboundState.tour.date) : undefined,
      },
      mobile: outboundState.mobile,
      train: {
        ...outboundState.train,
      },
    };
  },
  // define which reducers this transform gets called for.
  { whitelist: ['booking'] },
);
