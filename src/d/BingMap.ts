import { getDistanceTo } from 'Microsoft.Maps.SpatialMath';
import { Point, Pushpin, Events, Infobox, MapTypeId, Layer, Location, Map } from 'Microsoft.Maps';
export interface BingMapWindow extends Window {
  GetMap: () => void;
  Microsoft: {
    Maps: {
      Map: typeof Map;
      Point: typeof Point;
      Pushpin: typeof Pushpin;
      Events: Events;
      Location: typeof Location;
      Infobox: typeof Infobox;
      MapTypeId: typeof MapTypeId;
      Layer: typeof Layer;
      SpatialMath: { getDistanceTo: typeof getDistanceTo };
    };
  };
}
