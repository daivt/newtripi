export type some = { [key: string]: any };

export const DEV = process.env.NODE_ENV === 'development';
export const TEST =
  window.location.hostname.startsWith('alpha.') ||
  window.location.hostname.indexOf('-dev.') >= 0 ||
  window.location.hostname.startsWith('mytour.');
export const STAGING = window.location.hostname.startsWith('stage.');

export const AUTOTEST = process.env.NODE_ENV === 'test';

export const DEVICE_RID_KEY = 'deviceRid';

export const WEBVIEW =
  process.env.REACT_APP_PRODUCT === 'webview' || window.location.hostname.toLowerCase().indexOf('webview') >= 0;

export const DINOGO =
  process.env.REACT_APP_PRODUCT === 'dinogo' || window.location.hostname.toLowerCase().indexOf('dinogo') >= 0;

export const MY_TOUR = process.env.REACT_APP_PRODUCT === 'my_tour';

export const LS_LANG = 'lang';
export const PAGE_SIZE = 20;
export const TOUR_PAGE_SIZE = 24;
export const HOTEL_RESULT_CACHE_TIME = 1000 * 60 * 10;
export const FLIGHT_RESULT_CACHE_TIME = 1000 * 60 * 5;
export const FLIGHT_RESULT_INBOUND_CACHE_TIME = 1000 * 60 * 10;
export const TAX = 10;

export const HOTEL_RESULT_PAGE_SIZE_MAP = 150;

export const SLASH_DATE_FORMAT = 'DD/MM/YYYY';
export const DATE_FORMAT = 'DD-MM-YYYY';
export const C_DATE_FORMAT = 'YYYY-MM-DD';

export const TABLET_WIDTH = '1024px';
export const MIN_TABLET_WIDTH = '750px';
export const DESKTOP_WIDTH = '1260px';

export const TABLET_WIDTH_NUM = 1024;
export const MIN_TABLET_WIDTH_NUM = 750;
export const MOBILE_WIDTH_NUM = 480;
export const DESKTOP_WIDTH_NUM = 1260;
export const APP_FB_ID = '1545074012477997';
export const PAGE_FB_ID = '114630966955466';

export const DATE_TIME_FORMAT = 'DD-MM-YYYY HH:mm:ss';
export const SLASH_DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm';
export const DATE_TIME_FORMAT_NO_S = 'DD-MM-YYYY HH:mm';

export const HOTEL_SEARCH_HISTORY = 'hotelSearcHistory';
export const TOUR_SEARCH_HISTORY = 'tourSearchHistory';
export const FLIGHT_SEARCH_HISTORY = 'flightSearchHistory';
export const TRAIN_SEARCH_HISTORY = 'trainSearchHistory';
export const CURRENT_LOCATION = 'currentLocation';

function buildRoutePath(moduleName: ServiceType, path: string) {
  return `/${moduleName}${path}`;
}

export const ROUTES = {
  flight: {
    flight: MY_TOUR ? '/' : buildRoutePath('flight', ''),
    flightResult: MY_TOUR ? '/result' : buildRoutePath('flight', '/result'),
    flightResultInbound: MY_TOUR ? '/resultInbound' : buildRoutePath('flight', '/resultInbound'),
    flightBookingInfo: MY_TOUR ? '/bookingInfo' : buildRoutePath('flight', '/bookingInfo'),
    flightReview: MY_TOUR ? '/review' : buildRoutePath('flight', '/review'),
    flightPay: MY_TOUR ? '/pay' : buildRoutePath('flight', '/pay'),
    flightHunt: buildRoutePath('flight', '/hunt'),
    flightHuntResult: buildRoutePath('flight', '/huntResult'),

    ticketAlert: buildRoutePath('flight', '/ticketAlert'),
    ticketAlertCreate: buildRoutePath('flight', '/ticketAlertCreate'),
    ticketAlertDetail: {
      url: buildRoutePath('flight', '/ticketAlertDetail/:id'),
      setUrl: (id: number) => buildRoutePath('flight', `/ticketAlertDetail/${id}`),
    },
  },

  train: {
    train: buildRoutePath('train', ''),
    trainResult: buildRoutePath('train', '/result'),
    trainBookingInfo: buildRoutePath('train', '/trainBookingInfo'),
    review: buildRoutePath('train', '/review'),
    trainPay: buildRoutePath('train', '/pay'),
  },

  hotel: {
    hotel: buildRoutePath('hotel', ''),
    hotelResult: buildRoutePath('hotel', '/result'),
    hotelDetail: buildRoutePath('hotel', '/detail'),
    hotelDetailWithSlug: buildRoutePath('hotel', '/detail/:slug'),
    hotelBookingInfo: buildRoutePath('hotel', '/bookingInfo'),
    hotelPay: buildRoutePath('hotel', '/pay'),
  },

  tour: {
    tour: buildRoutePath('tour', ''),
    tourResult: buildRoutePath('tour', '/result'),
    tourDetail: buildRoutePath('tour', '/detail'),
    tourBookingInfo: buildRoutePath('tour', '/bookingInfo'),
    tourPay: buildRoutePath('tour', '/pay'),
  },

  reward: {
    rewards: buildRoutePath('reward', '/rewards'),
    myRewards: buildRoutePath('reward', '/myRewards'),
    myRewardDetail: {
      value: buildRoutePath('reward', '/myRewardDetail/:rewardId/:objectType'),
      gen: (rewardId: number, objectType: string) =>
        buildRoutePath('reward', `/myRewardDetail/${rewardId}/${objectType}`),
    },
    rewardsByCategory: {
      value: buildRoutePath('reward', '/rewardsByCategory/:categoryId'),
      gen: (categoryId: number) => buildRoutePath('reward', `/rewardsByCategory/${categoryId}`),
    },
    rewardDetail: {
      value: buildRoutePath('reward', '/detail/:id'),
      gen: (id: string) => buildRoutePath('reward', `/detail/${id}`),
    },
  },

  mobile: {
    mobileCard: buildRoutePath('mobile', '/card'),
    mobileTopup: buildRoutePath('mobile', '/topup'),
    mobileCardPay: buildRoutePath('mobile', '/cardPay'),
    mobileTopupPay: buildRoutePath('mobile', '/topupPay'),
    mobileHistory: buildRoutePath('mobile', '/history'),
  },

  management: '/management',
  confirmInvitation: '/management/confirmInvitation',

  paySuccess: {
    value: '/checkout/success/:moduleType/:bookingId',
    gen: (moduleType: ModuleType, bookingID: string) => `/checkout/success/${moduleType}/${bookingID}`,
  },

  payFailure: {
    value: '/checkout/fail/:moduleType',
    gen: (moduleType: ModuleType) => `/checkout/fail/${moduleType}`,
  },

  partnerPayment: {
    success: '/checkout/partnerSuccess/:moduleType/:bookingId',
    paymentSuccess: '/payment/partnerSuccess/:moduleType/:bookingId', // Duplicate route because server will return it
    originPathSuccess: '/checkout/partnerSuccess/',
    originPathPaymentSuccess: '/payment/partnerSuccess/',
  },

  login: '/login',
  loginAfterResetPass: '/loginAfterResetPass',
  signUp: '/signUp',
  verifyAgency: '/verifyAgency',
  forgotPassword: '/forgotPassword',
  terms: '/terms',
  termsOld: '/dieu-khoan-su-dung',
  securityPolicies: '/securityPolicies',
  inviteMember: '/invite-member',
};

export const WV_ROUTES = {
  flight: {
    flight: buildRoutePath('flight', ''),
    flightResult: buildRoutePath('flight', '/result'),
    flightResultInbound: buildRoutePath('flight', '/resultInbound'),
    flightBookingInfo: buildRoutePath('flight', '/bookingInfo'),
    flightReview: buildRoutePath('flight', '/review'),
    flightPay: buildRoutePath('flight', '/pay'),
  },

  train: {
    train: buildRoutePath('train', ''),
    trainResult: buildRoutePath('train', '/result'),
    trainBookingInfo: buildRoutePath('train', '/trainBookingInfo'),
    review: buildRoutePath('train', '/review'),
    trainPay: buildRoutePath('train', '/pay'),
  },

  hotel: {
    hotel: buildRoutePath('hotel', ''),
    hotelResult: buildRoutePath('hotel', '/result'),
    hotelDetail: buildRoutePath('hotel', '/detail'),
    hotelBookingInfo: buildRoutePath('hotel', '/bookingInfo'),
    hotelPay: buildRoutePath('hotel', '/pay'),
  },

  tour: {
    tour: buildRoutePath('tour', ''),
    tourResultLocation: buildRoutePath('tour', '/resultLocation'),
    tourResultTerm: buildRoutePath('tour', '/resultTerm'),
    tourDetail: buildRoutePath('tour', '/detail'),
    tourBookingInfo: buildRoutePath('tour', '/bookingInfo'),
    tourPay: buildRoutePath('tour', '/pay'),
  },

  paySuccess: {
    value: '/checkout/success/:moduleType/:bookingId',
    gen: (moduleType: ModuleType, bookingID: string) => `/checkout/success/${moduleType}/${bookingID}`,
  },

  payFailure: {
    value: '/checkout/fail/:moduleType',
    gen: (moduleType: ModuleType) => `/checkout/fail/${moduleType}`,
  },
};

export const KEY_MAP = 'AlrBUViuXlnV4-DY48h7GC6Xd3ycYOf1bBhGze8V4w93LKzvSEVUWQrrUiyliJcF';

export const KEY_GOOGLE_MAP = 'AIzaSyCktZ4VE5hoHILLG6N2M7XxCt-Zlq6HsdI';

export type ServiceType = 'flight' | 'hotel' | 'tour' | 'train' | 'reward' | 'mobile' | '';

export type ModuleType =
  | 'flight'
  | 'hotel'
  | 'tour'
  | 'train'
  | 'mobile_card'
  | 'mobile_topup'
  | 'baggages'
  | 'itinerary_changing'
  | 'flightPayLater'
  | 'hotelPayLater'
  | 'tourPayLater';
