const proxy = require('http-proxy-middleware');

const router = {
  '/api/general': 'https://dev-api.tripi.vn',
  '/api/hotel': 'https://dev-api.tripi.vn', // 'https://hotelapi.tripi.vn/v3'
  '/api/tour': 'https://dev-api.tripi.vn', // 'https://tourapi.tripi.vn/v3',
  '/api/oldTour': 'https://dev-api.tripi.vn', // 'https://tourapi.tripi.vn',
  '/api/train': 'https://train-dev-api.tripi.vn',
};

module.exports = app => {
  app.use(
    proxy('/api/', {
      target: 'https://dev-api.tripi.vn', // https://flightapi.tripi.vn/v3
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        '^/api/general/': '/',
        '^/api/flight/': '/',
        '^/api/hotel/': '/',
        '^/api/tour/': '/',
        '^/api/oldTour/': '/',
      },
      router,
      logLevel: 'debug',
    }),
  );
};
