import { DINOGO, MY_TOUR } from './constants';
import colors from './scss/colors.module.scss';

// export const {
//   BACKGROUND,
//   DINOGO_PRIMARY,
//   DINOGO_SECONDARY,
//   ORANGE,
//   GREY,
//   LIGHT_GREY,
//   DARK_GREY,
//   HOVER_GREY,
//   BLACK_TEXT,
//   BLUE,
//   BLUE_MT,
//   LIGHT_BLUE,
//   DARK_BLUE,
//   GREEN,
//   PURPLE,
//   RED,
//   DARK_GREEN,
//   YELLOW,
//   YELLOW_MT,
//   BROWN,
//   PINK,
//   BACKGROUND_MT,
// } = colors;

export const BACKGROUND = '#f9f9fa';

export const DINOGO_PRIMARY = '#57c478';
export const DINOGO_SECONDARY = '#cc0066';

export const ORANGE = '#ff7043';
export const GREY = '#bdbdbd';
export const LIGHT_GREY = '#e9e9e9';
export const DARK_GREY = '#757575';
export const HOVER_GREY = '#f5f5f5';
export const BLACK_TEXT = '#212121';
export const BLUE = '#1976d2';
export const BLUE_MT = '#36c5f4';
export const LIGHT_BLUE = '#4fc3f7';
export const DARK_BLUE = '#1976d2';
export const GREEN = '#00ad50';
export const PURPLE = '#ba68c8';
export const RED = '#f44336';
export const DARK_GREEN = '#009688';
export const YELLOW = '#ff9800';
export const YELLOW_MT = '#ffe42a';
export const BROWN = '#795548';
export const PINK = '#cc0066';
export const BACKGROUND_MT = '#fdfdfd';

export const SECONDARY = MY_TOUR ? YELLOW_MT : DINOGO ? DINOGO_SECONDARY : ORANGE;
export const PRIMARY = MY_TOUR ? BLUE_MT : DINOGO ? DINOGO_PRIMARY : GREEN;
