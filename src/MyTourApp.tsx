import { WithStyles, withStyles } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import Helmet from 'react-helmet';
import { ROUTES } from './constants';
import AuthDialogs from './modules/auth/components/AuthDialogs';
import { closeAuthDialog } from './modules/auth/redux/authReducer';
import FlightBookingInfo from './modules/booking/pages/FlightBookingInfo';
import FlightPay from './modules/booking/pages/FlightPay';
import FlightReview from './modules/booking/pages/FlightReview';
import InitAndPathnameChangeAware from './modules/common/components/InitAndPathnameChangeAware';
import NetworkProblemDialog from './modules/common/components/NetworkProblemDialog';
import NotFound from './modules/common/pages/NotFound';
import PaymentFail from './modules/common/pages/PaymentFail';
import PaymentSuccess from './modules/common/pages/PaymentSuccess';
import { setNetworkError } from './modules/common/redux/reducer';
import Flight from './modules/home/pages/Flight';
import SecurityPolicies from './modules/home/pages/SecurityPolicies';
import Terms from './modules/home/pages/Terms';
import FlightResult from './modules/result/pages/FlightResult';
import FlightResultInbound from './modules/result/pages/FlightResultInbound';
import { AppState } from './redux/reducers';
import './scss/slickOverride.scss';
import './scss/svg.scss';
import { bodyStyles } from './utils';
import PartnerPaymentSuccess from './modules/checkoutPartner/pages/PartnerPaymentSuccess';

const mapStateToProps = (state: AppState) => ({
  authDialog: state.auth.authDialog,
  common: state.common,
  account: state.account,
  pathname: state.router.location.pathname,
});

interface Props extends ReturnType<typeof mapStateToProps>, WithStyles<typeof bodyStyles> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const MyTourApp: React.FC<Props> = MyTourAppProps => {
  const { authDialog, common, dispatch, classes } = MyTourAppProps;
  const { networkErrorMsg } = common;

  React.useEffect(() => {
    document.body.className = classes.body;
  }, [classes]);

  return (
    <>
      <Helmet link={[{ rel: 'shortcut icon', href: '/static/mytour.ico' }]} />
      <NetworkProblemDialog msgId={networkErrorMsg} onClose={() => dispatch(setNetworkError(undefined))} />
      <AuthDialogs authDialog={authDialog} close={() => dispatch(closeAuthDialog())} />
      <InitAndPathnameChangeAware>
        <Switch>
          <Route exact path={ROUTES.flight.flight} component={Flight} />
          <Route path={ROUTES.flight.flightResult} component={FlightResult} />
          <Route path={ROUTES.flight.flightResultInbound} component={FlightResultInbound} />
          <Route path={ROUTES.flight.flightBookingInfo} component={FlightBookingInfo} />
          <Route path={ROUTES.flight.flightReview} component={FlightReview} />
          <Route path={ROUTES.flight.flightPay} component={FlightPay} />
          <Route path={ROUTES.paySuccess.value} component={PaymentSuccess} />
          <Route path={ROUTES.payFailure.value} component={PaymentFail} />
          <Route exact path={ROUTES.terms} component={Terms} />
          <Route exact path={ROUTES.securityPolicies} component={SecurityPolicies} />
          <Route path={ROUTES.partnerPayment.paymentSuccess} component={PartnerPaymentSuccess} />
          <Route component={NotFound} />
        </Switch>
      </InitAndPathnameChangeAware>
    </>
  );
};

export default connect(mapStateToProps)(withStyles(bodyStyles)(MyTourApp));
