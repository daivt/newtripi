import { red } from '@material-ui/core/colors';
import { createMuiTheme, darken, fade } from '@material-ui/core/styles';
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme';
import aphroditeInterface from 'react-with-styles-interface-aphrodite';
import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet';
import { BLACK_TEXT, PRIMARY, SECONDARY } from './colors';
import { MY_TOUR, WEBVIEW } from './constants';

const colorCalendar = MY_TOUR ? PRIMARY : SECONDARY;
ThemedStyleSheet.registerInterface(aphroditeInterface);
ThemedStyleSheet.registerTheme({
  reactDates: {
    ...DefaultTheme.reactDates,
    color: {
      ...DefaultTheme.reactDates.color,
      selected: {
        ...DefaultTheme.reactDates.color.selected,
        backgroundColor: colorCalendar,
        backgroundColor_active: colorCalendar,
        backgroundColor_hover: colorCalendar,
        borderColor: colorCalendar,
        borderColor_active: colorCalendar,
        borderColor_hover: colorCalendar,
      },
      selectedSpan: {
        backgroundColor: fade(colorCalendar, 0.2),
        backgroundColor_active: fade(colorCalendar, 0.2),
        backgroundColor_hover: fade(colorCalendar, 0.2),
        borderColor: fade(colorCalendar, 0.2),
        borderColor_active: fade(colorCalendar, 0.2),
        borderColor_hover: fade(colorCalendar, 0.2),
      },
      hoveredSpan: {
        backgroundColor: fade(colorCalendar, 0.2),
        backgroundColor_active: fade(colorCalendar, 0.2),
        backgroundColor_hover: fade(colorCalendar, 0.2),
        borderColor: fade(colorCalendar, 0.2),
        borderColor_active: fade(colorCalendar, 0.2),
        borderColor_hover: fade(colorCalendar, 0.2),
      },
    },
  },
});

export const THEME = {
  primary: PRIMARY,
  secondary: SECONDARY,
};

export const MUI_THEME = createMuiTheme({
  breakpoints: {
    values: WEBVIEW ? { xs: 0, sm: 360, md: 440, lg: 800, xl: 1000 } : undefined,
  },
  palette: {
    primary: {
      light: fade(PRIMARY, 0.9),
      main: PRIMARY,
      dark: darken(PRIMARY, 0.1),
      contrastText: '#ffffff',
    },
    secondary: {
      light: fade(SECONDARY, 0.9),
      main: SECONDARY,
      dark: darken(SECONDARY, 0.1),
      contrastText: '#ffffff',
    },
    error: {
      light: red[300],
      main: red[600],
      dark: red[700],
      contrastText: '#ffffff',
    },
    text: {
      primary: BLACK_TEXT,
      secondary: fade(BLACK_TEXT, 0.6),
    },
  },
  typography: WEBVIEW
    ? {
        htmlFontSize: 14,
        fontSize: 14,
        subtitle1: { fontSize: '20px', fontWeight: 500 },
        subtitle2: { fontSize: '16px', fontWeight: 500, lineHeight: '26px' },
        body1: { fontSize: '16px', lineHeight: '25px' },
        body2: { fontSize: '14px', lineHeight: '23px', fontWeight: 'normal' },
        caption: { fontSize: '12px', lineHeight: '18px' },
        button: { fontSize: '16px', textTransform: 'none', lineHeight: '25px', fontWeight: 500 },
        h4: { fontSize: '34px', lineHeight: '40px' },
        h5: { fontSize: '24px', fontWeight: 'bold', lineHeight: '30px' },
        h6: { fontSize: '20px', lineHeight: '27px' },
      }
    : {
        htmlFontSize: 15,
        fontSize: 15,
        subtitle1: { fontSize: '20px', fontWeight: 500 },
        subtitle2: { fontSize: '17px', fontWeight: 500, lineHeight: '26px' },
        body1: { fontSize: '17px', lineHeight: '26px' },
        body2: { fontSize: '15px', lineHeight: '24px', fontWeight: 'normal' },
        caption: { fontSize: '13px', lineHeight: '20px' },
        button: { fontSize: '17px', textTransform: 'none', lineHeight: '26px', fontWeight: 500 },
        h4: { fontSize: '34px', lineHeight: '40px' },
        h5: { fontSize: '24px', fontWeight: 'bold', lineHeight: '30px' },
        h6: { fontSize: '20px', lineHeight: '27px' },
      },
  overrides: {
    MuiInputLabel: {
      root: {
        fontSize: '15px',
        lineHeight: '24px',
        fontWeight: 'normal',
        color: BLACK_TEXT,
        position: 'relative',
        marginBottom: 4,
      },
      shrink: {
        transform: 'translate(0, 1.5px)',
      },
      formControl: {
        fontSize: '15px',
        lineHeight: '24px',
        fontWeight: 'normal',
        position: 'relative',
        marginBottom: 4,
      },
    },
    MuiFormHelperText: {
      root: { marginTop: 0, fontSize: '15px', lineHeight: '24px', fontWeight: 'normal' },
    },
    MuiButton: {
      containedSecondary: {
        color: MY_TOUR ? BLACK_TEXT : 'white',
      },
    },
    MuiRadio: {
      colorSecondary: {
        '&$checked': {
          color: MY_TOUR ? PRIMARY : SECONDARY,
        },
      },
    },
    MuiCheckbox: {
      colorSecondary: {
        '&$checked': {
          color: MY_TOUR ? PRIMARY : SECONDARY,
        },
      },
    },
  },
});
