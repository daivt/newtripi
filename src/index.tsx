import { MuiThemeProvider } from '@material-ui/core';
import { ConnectedRouter } from 'connected-react-router';
import 'intl-pluralrules/polyfill';
import 'moment/locale/vi';
import { SnackbarProvider } from 'notistack';
import React from 'react';
import ReactDOM from 'react-dom';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from 'styled-components';
import { DINOGO, LS_LANG, WEBVIEW, MY_TOUR } from './constants';
import './index.css';
import GAListener from './modules/common/components/GAListener';
import RouteLoading from './modules/common/components/RouteLoading';
import ConnectedIntlProvider from './modules/intl/components/ConnectedIntlProvider';
import { setLocale } from './modules/intl/redux/reducer';
import { configureStore, history } from './redux/configureStore';
import * as serviceWorker from './serviceWorker';
import { MUI_THEME, THEME } from './setupTheme';
import { loadParamsToStore } from './utils';
import MyTourApp from './MyTourApp';

const store = configureStore({});
const persistor = persistStore(store);

window.addEventListener('popstate', () => loadParamsToStore(store.dispatch));

const App = Loadable({
  loader: () => import('./App'),
  loading: RouteLoading,
});
const WebViewApp = Loadable({
  loader: () => import('./WebViewApp'),
  loading: RouteLoading,
});

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <PersistGate
        loading={null}
        persistor={persistor}
        onBeforeLift={() => {
          loadParamsToStore(store.dispatch);
          const lang =
            !DINOGO || (window as any).PRERENDER ? 'vi' : localStorage.getItem(LS_LANG) || window.navigator.language;
          if (!lang.startsWith('vi')) {
            store.dispatch(setLocale('en-US'));
          } else {
            store.dispatch(setLocale(lang));
          }
        }}
      >
        <ConnectedIntlProvider>
          <GAListener>
            <ThemeProvider theme={THEME}>
              <MuiThemeProvider theme={MUI_THEME}>
                <SnackbarProvider maxSnack={3}>
                  {WEBVIEW ? <WebViewApp /> : MY_TOUR ? <MyTourApp /> : <App />}
                </SnackbarProvider>
              </MuiThemeProvider>
            </ThemeProvider>
          </GAListener>
        </ConnectedIntlProvider>
      </PersistGate>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
