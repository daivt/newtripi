import { WithStyles, withStyles } from '@material-ui/core/styles';
import * as React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { CSSTransitionClassNames } from 'react-transition-group/CSSTransition';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BACKGROUND } from './colors';
import { WV_ROUTES } from './constants';
import InitAndPathnameChangeAware from './modules/common/components/InitAndPathnameChangeAware';
import NetworkProblemDialog from './modules/common/components/NetworkProblemDialog';
import { setNetworkError } from './modules/common/redux/reducer';
import Trung from './modules/home/pages/Trung';
import Home from './modules/home/wvPages/Home';
import Hotel from './modules/home/wvPages/Hotel';
import Tour from './modules/home/wvPages/Tour';
import Train from './modules/home/wvPages/Train';
import FlightResult from './modules/result/wvPages/FlightResult';
import FlightResultInbound from './modules/result/wvPages/FlightResultInbound';
import HotelResult from './modules/result/wvPages/HotelResult';
import TourDetail from './modules/result/wvPages/TourDetail';
import TourResultLocation from './modules/result/wvPages/TourResultLocation';
import TourResultTerm from './modules/result/wvPages/TourResultTerm';
import Flight from './modules/search/wvPages/Flight';
import { AppState } from './redux/reducers';
import styles from './scss/webviewRouteTransition.module.scss';
import { bodyStyles } from './utils';

interface WebViewAppProps extends ReturnType<typeof mapStateToProps>, WithStyles<typeof bodyStyles> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const WebViewApp: React.FunctionComponent<WebViewAppProps> = props => {
  const { common, router, dispatch, classes } = props;
  const { networkErrorMsg } = common;

  const { action } = router;
  const transitionClassNamesRef = React.useRef<CSSTransitionClassNames>({});

  const lastRouteYOffsetRef = React.useRef(0);

  const actionRef = React.useRef(action);
  actionRef.current = action;

  React.useEffect(() => {
    document.body.className = classes.body;
  }, [classes.body]);

  if (actionRef.current === 'PUSH') {
    transitionClassNamesRef.current.enter = styles.enter;
    transitionClassNamesRef.current.enterActive = styles.enterActive;
    transitionClassNamesRef.current.exit = undefined;
    transitionClassNamesRef.current.exitActive = undefined;
  } else {
    transitionClassNamesRef.current.enter = undefined;
    transitionClassNamesRef.current.enterActive = undefined;
    transitionClassNamesRef.current.exit = styles.exit;
    transitionClassNamesRef.current.exitActive = styles.exitActive;
  }

  return (
    <>
      <Helmet>
        <title>Webview</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      </Helmet>
      <NetworkProblemDialog msgId={networkErrorMsg} onClose={() => dispatch(setNetworkError(undefined))} />
      <InitAndPathnameChangeAware webview>
        <TransitionGroup style={{ background: BACKGROUND }}>
          <CSSTransition
            key={router.location.pathname}
            classNames={transitionClassNamesRef.current}
            timeout={300}
            onExited={() => {
              if (actionRef.current === 'PUSH') {
                window.scrollTo({ top: 0 });
              }
              document.body.className = classes.body;
            }}
            unmountOnExit
          >
            {status => {
              const style: React.CSSProperties =
                status === 'entering' || status === 'exiting' ? {} : { position: 'absolute' };
              if (status === 'exiting' || status === 'exited') {
                if (status === 'exiting') {
                  lastRouteYOffsetRef.current = window.pageYOffset;
                }
                style.top = -lastRouteYOffsetRef.current;
              }
              return (
                <div style={{ ...style, width: '100%' }}>
                  <Switch location={router.location}>
                    <Route exact path="/" component={Home} />
                    <Route exact path={WV_ROUTES.flight.flight} component={Flight} />
                    <Route exact path={WV_ROUTES.hotel.hotel} component={Hotel} />
                    <Route exact path={WV_ROUTES.tour.tour} component={Tour} />
                    <Route exact path={WV_ROUTES.tour.tourResultLocation} component={TourResultLocation} />
                    <Route exact path={WV_ROUTES.tour.tourResultTerm} component={TourResultTerm} />
                    <Route exact path={WV_ROUTES.tour.tourDetail} component={TourDetail} />
                    <Route exact path={WV_ROUTES.train.train} component={Train} />
                    <Route exact path={WV_ROUTES.flight.flightResult} component={FlightResult} />
                    <Route exact path={WV_ROUTES.flight.flightResultInbound} component={FlightResultInbound} />
                    <Route exact path={WV_ROUTES.hotel.hotelResult} component={HotelResult} />
                    <Route exact path="/devTrung" component={Trung} />
                  </Switch>
                </div>
              );
            }}
          </CSSTransition>
        </TransitionGroup>
      </InitAndPathnameChangeAware>
    </>
  );
};

function mapStateToProps(state: AppState) {
  return { common: state.common, router: state.router };
}

export default connect(mapStateToProps)(withStyles(bodyStyles)(WebViewApp));
