import { MY_TOUR } from '../../constants';

export const FLIGHT_SORT_BY = {
  fastest: 'fastest',
  cheapest: 'cheap',
  best: 'best',
  takeOff: 'takeOff',
};

export const HOTEL_SORT_BY = {
  default: 'default',
  expensive: 'price-',
  cheap: 'price+',
  starDesc: 'star-',
  starAsc: 'star+',
};

export const TOUR_SORT_BY = {
  default: 'default',
  expensive: 'price-',
  cheap: 'price+',
};

export const TRAIN_SORT_BY = {
  earliest: 'earliest',
  latest: 'latest',
  longest: 'longest',
  shortest: 'shortest',
};

export const PROMOTIONS_PARAMS = {
  sortBy: 'sortBy',
  hasPurchase: 'hasPurchase',
};

export const HotelTypes = [
  {
    id: 1,
    name: 'hotel.filter.hotelType.hotel',
  },
  {
    id: 2,
    name: 'hotel.filter.hotelType.resort',
  },
  {
    id: 3,
    name: 'hotel.filter.hotelType.motel',
  },
  {
    id: 4,
    name: 'hotel.filter.hotelType.villa',
  },
  {
    id: 5,
    name: 'hotel.filter.hotelType.yacht',
  },
];

export const HotelStars = [
  {
    id: 1,
    name: 'hotel.filter.starRate.oneStar',
    icon: '',
  },
  {
    id: 2,
    name: 'hotel.filter.starRate.twoStar',
    icon: '',
  },
  {
    id: 3,
    name: 'hotel.filter.starRate.threeStar',
    icon: '',
  },
  {
    id: 4,
    name: 'hotel.filter.starRate.fourStar',
    icon: '',
  },
  {
    id: 5,
    name: 'hotel.filter.starRate.fiveStar',
    icon: '',
  },
];

export const Facilities = [
  {
    id: 1,
    name: 'hotel.filter.facility.outdoorPool',
    icon: '',
  },
  {
    id: 2,
    name: 'hotel.filter.facility.indoorPool',
    icon: '',
  },
  {
    id: 3,
    name: 'hotel.filter.facility.gym',
    icon: '',
  },
  {
    id: 4,
    name: 'hotel.filter.facility.steamBath',
    icon: '',
  },
  {
    id: 5,
    name: 'hotel.filter.facility.spa',
    icon: '',
  },
  {
    id: 6,
    name: 'hotel.filter.facility.massage',
    icon: '',
  },
];

export const FlightHuntSupportedAirlines = [
  {
    aid: 1,
  },
  {
    aid: 4,
  },
  {
    aid: 2,
  },
  {
    aid: 3,
  },
];
export const REWARDS_SORT_BY = {
  common: 'usednum-',
  latest: 'updatedtime-',
  pointAsc: 'point+',
  pointDesc: 'point-',
};

export const skeleton = (value: number) => {
  const array = [];
  for (let i = 0; i < value; i += 1) {
    array.push({ id: i, skeleton: true });
  }
  return array;
};
export const VOUCHER = 'VOUCHER';
export const CREDIT = 'CREDIT';
export const MOBI_CARD = 'MOBI_CARD';
export const NORMAL_ITEM = 'NORMAL_ITEM';

export const TRAIN_SEAT_STATUS = {
  BOOKED: 1,
  HOLDING: 2,
  AVAILABLE: 3,
  NOT_FOR_SELL_TO_STUDENT: 4,
  NOT_FOR_SELL: 5,
};

export const TRAIN_SEAT_TYPE = {
  SEAT: 1,
  ROAD: 0,
  BULK_HEAD: -1,
  NON_BULK_HEAD: -2,
  NON_SEAT: -3,
};

export const TECHNICAL_STOP = 'TECHNICAL_STOP';

export const FILTER_MAX_PRICE = 30000000;

export const DEFAULT_HOTEL_FILTER_STATE = {
  price: [0, FILTER_MAX_PRICE],
  stars: [],
  subLocations: [],
  hotelTypes: [],
  facilities: [],
};

export const DEFAULT_HOTEL_STATE = {
  searching: false,
  sortBy: HOTEL_SORT_BY.default,
  searchTime: 0,
  authSearch: false,
  filterParams: DEFAULT_HOTEL_FILTER_STATE,
  showMap: false,
  pageOffset: 1,
};

export const DEFAULT_FILTER = {
  price: [0, 1],
  numStops: [],
  timeLand: [],
  flightDuration: [],
  transitDuration: [],
  timeTakeOff: [],
  airline: [],
  ticketClass: [],
};

export const DEFAULT_FLIGHT_STATE = {
  filterParams: DEFAULT_FILTER,
  searchCompleted: 0,
  authSearch: false,
  maxTransitDuration: { inbound: 0, outbound: 0 },
  maxFlightDuration: { inbound: 0, outbound: 0 },
  sortBy: FLIGHT_SORT_BY.best,
  searchTime: 0,
};

export const DEFAULT_TOUR_FILTER_STATE = {
  maxPrice: -1,
  tourTypes: [],
  activityTags: [],
  tourSubjects: [],
  transportations: [],
  tourServices: [],
  groupName: '',
  destinations: [],
};

export const DEFAULT_TOUR_STATE = {
  sortBy: TOUR_SORT_BY.default,
  filterParams: DEFAULT_TOUR_FILTER_STATE,
  filterMaxPrice: -1,
  loading: false,
  pageOffset: 1,
};

export const DEFAULT_TRAIN_RESULT_STATE = {
  loading: false,
  outboundSortBy: TRAIN_SORT_BY.earliest,
  inboundSortBy: TRAIN_SORT_BY.earliest,
  searchTime: 0,
};
export const TIME_TAKE_OFF_AND_LAND = [
  {
    id: 0,
    text: '00:00 - 06:00',
    value: ['00:00', '05:59'],
    isSelect: false,
  },
  {
    id: 1,
    text: '06:00 - 12:00',
    value: ['06:00', '11:59'],
    isSelect: false,
  },
  {
    id: 2,
    text: '12:00 - 18:00',
    value: ['12:00', '17:59'],
    isSelect: false,
  },
  {
    id: 3,
    text: '18:00 - 24:00',
    value: ['18:00', '23:59'],
    isSelect: false,
  },
];

export const PRICE_STEP = 10000;

export const FLIGHT_SORT_BY_DATA = [
  {
    id: 0,
    text: MY_TOUR ? 'filter.sortBy.bestMT' : 'filter.sortBy.best',
    value: FLIGHT_SORT_BY.best,
  },
  {
    id: 1,
    text: 'filter.sortBy.cheap',
    value: FLIGHT_SORT_BY.cheapest,
  },
  {
    id: 2,
    text: 'filter.sortBy.takeOff',
    value: FLIGHT_SORT_BY.takeOff,
  },
  {
    id: 3,
    text: 'filter.sortBy.fastest',
    value: FLIGHT_SORT_BY.fastest,
  },
];
