import { push, replace } from 'connected-react-router';
import stringify from 'json-stable-stringify';
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, FLIGHT_RESULT_INBOUND_CACHE_TIME, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { stringifyParams } from '../../booking/utils';
import { parseFlightSearchParams } from '../../search/utils';
import FlightResultDesktop from '../components/flight/FlightResultDesktop';
import FlightResultTablet from '../components/flight/FlightResultTablet';
import { DEFAULT_FILTER } from '../constants';
import { setFilter } from '../redux/flightResultReducer';
import { sameFlightSearchParams } from '../utils';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class FlightResultInbound extends PureComponent<Props> {
  componentDidMount() {
    const { dispatch, booking, result, auth } = this.props;
    const params = result.searchParams;
    if (
      !params ||
      !sameFlightSearchParams(params, parseFlightSearchParams(new URLSearchParams(window.location.search))) ||
      !booking.outbound.ticket ||
      new Date().getTime() - result.searchTime > FLIGHT_RESULT_INBOUND_CACHE_TIME ||
      result.authSearch !== auth
    ) {
      dispatch(replace({ pathname: ROUTES.flight.flightResult, search: window.location.search }));
    } else {
      dispatch(setFilter({ ...DEFAULT_FILTER, price: result.filterParams.price }));
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { result } = this.props;
    if (stringify(result.filterParams) !== stringify(prevProps.result.filterParams)) {
      window.scrollTo({ top: 0 });
    }
  }

  componentWillUnmount() {
    const { result, dispatch } = this.props;
    dispatch(setFilter({ ...DEFAULT_FILTER, price: result.filterParams.price }));
  }

  goToBooking = () => {
    const { booking, dispatch } = this.props;
    if (booking.tid) {
      const dest = `${ROUTES.flight.flightBookingInfo}?${stringifyParams(booking.tid)}`;
      dispatch(push(dest, { backableToResultTwoWay: true }));
    }
  };

  render() {
    const { result, intl } = this.props;
    const params = result.searchParams;
    return (
      <>
        <Helmet>
          <title>
            {params
              ? `${intl.formatMessage({ id: 'inbound' })}: ${params.origin.location} (${params.origin.code}) > ${
                  params.destination.location
                } (${params.destination.code}) - ${params.departureDate.format('L')} - ${params.returnDate &&
                  params.returnDate.format('L')}`
              : 'Flight search result'}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <FlightResultDesktop goToBooking={this.goToBooking} />;
            }
            return <FlightResultTablet goToBooking={this.goToBooking} />;
          }}
        </MediaQuery>
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    result: state.result.flight,
    search: state.search.flight,
    auth: state.auth.auth,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightResultInbound));
