import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { FLIGHT_HUNT_PARAM_NAMES } from '../../common/constants';
import FlightHuntResultTabletDesktop from '../components/flightHunt/FlightHuntResultTabletDesktop';
import { search } from '../redux/flightHuntResultReducer';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class FlightHuntResult extends PureComponent<Props> {
  componentDidMount() {
    this.props.dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { fetchSignal, dispatch } = this.props;
    const URL = new URLSearchParams(fetchSignal);
    URL.delete(`${FLIGHT_HUNT_PARAM_NAMES.departureDate}`);
    URL.delete(`${FLIGHT_HUNT_PARAM_NAMES.returnDate}`);
    const PREV_URL = new URLSearchParams(prevProps.fetchSignal);
    PREV_URL.delete(`${FLIGHT_HUNT_PARAM_NAMES.departureDate}`);
    PREV_URL.delete(`${FLIGHT_HUNT_PARAM_NAMES.returnDate}`);

    if (URL.toString() !== PREV_URL.toString()) {
      dispatch(search());
    }
  }

  render() {
    const { intl } = this.props;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'ticketHunt' })}</title>
        </Helmet>
        <FlightHuntResultTabletDesktop />
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    fetchSignal: state.router.location.search,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightHuntResult));
