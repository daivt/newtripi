import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MyRewardDetailTabletDesktop from '../components/reward/MyRewardDetailTabletDesktop';
import { fetchMyRewards } from '../redux/rewardResultReducer';

export interface Props extends RouteComponentProps<{ rewardId: string; objectType: string }> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

interface State {
  data?: some;
}

class MyRewardDetail extends React.Component<Props> {
  state: State = {};
  async componentDidMount() {
    const { dispatch, match } = this.props;
    const id = Number(match.params.rewardId);
    const objectType = match.params.objectType;
    dispatch(fetchMyRewards(1, 1, id, objectType)).then(json => {
      this.setState({ data: json.list[0] });
    });
  }

  public render() {
    const { data } = this.state;
    return <MyRewardDetailTabletDesktop data={data} />;
  }
}

export default connect()(MyRewardDetail);
