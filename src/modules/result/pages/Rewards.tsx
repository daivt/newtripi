import * as React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import RewardsTabletDesktop from '../components/reward/RewardsTabletDesktop';
import { skeleton } from '../constants';
import { fetchRewards } from '../redux/rewardResultReducer';

export interface IRewardsProps extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

interface State {
  data?: some[];
}

class Rewards extends React.Component<IRewardsProps, State> {
  state: State = {};

  async componentDidMount() {
    const json = await this.props.dispatch(fetchRewards(8));
    if (json.code === 200) {
      this.setState({ data: json.data.categories });
    }
  }

  public render() {
    const { intl, categories } = this.props;
    const { data } = this.state;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'rewards.title' })}</title>
        </Helmet>
        <RewardsTabletDesktop data={data || skeleton(7)} categories={categories || skeleton(7)} />
      </>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return { categories: state.common.rewardsCategories };
};

export default connect(mapState2Props)(injectIntl(Rewards));
