import { Typography } from '@material-ui/core';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { matchPath } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import {
  OneDirection,
  SeatingInfo,
  setErrorMessage,
  TrainTicketParams,
  goToBookingInfoPage,
} from '../../booking/redux/trainBookingReducer';
import MessageDialog from '../../common/components/MessageDialog';
import { TrainTravellersInfo } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { TrainSearchParamsState } from '../../search/redux/trainSearchReducer';
import { parseTrainSearchState } from '../../search/utils';
import TrainResultTabletDesktop from '../components/train/TrainResultTabletDesktop';
import { search } from '../redux/trainResultReducer';
import { validTrainBookingParams } from '../utils';
import { defaultTrainTicketParams, defaultSeatingInfo } from '../../booking/constants';

function mapStateToProps(state: AppState) {
  return {
    fetchSignal: `${state.router.location.search}&${state.auth.auth}`,
    trainSearch: state.search.train,
    router: state.router,
    errorMessageBooking: state.booking.train.errorMessage,
  };
}

function defaultSeating(travellersInfo: TrainTravellersInfo): SeatingInfo {
  return {
    adults: Array(travellersInfo.adultCount).fill({}),
    children: Array(travellersInfo.childCount).fill({}),
    seniors: Array(travellersInfo.seniorCount).fill({}),
    students: Array(travellersInfo.studentCount).fill({}),
  };
}

function defaultTrainBookingState(
  travellersInfo: TrainTravellersInfo,
  isTwoWay: boolean,
): TrainTicketParams {
  return {
    inbound: {},
    outbound: {},
    inboundSeating: isTwoWay ? defaultSeating(travellersInfo) : defaultSeatingInfo,
    outboundSeating: defaultSeating(travellersInfo),
  };
}

function getSearchParams(fetchSignal: string, trainSearch: TrainSearchParamsState) {
  let searchParams;
  try {
    searchParams = parseTrainSearchState(new URLSearchParams(fetchSignal));
  } catch (err) {}

  searchParams = searchParams || trainSearch;
  return searchParams;
}

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  seatData?: some;
  open: boolean;
  message: string;
  errorMessage: string;
  selectTicketFocus: 'inbound' | 'outbound';
  trainSelected?: some;
  carriageSelected?: some;
  trainBookingState: TrainTicketParams;
}

class TrainResult extends React.PureComponent<Props, State> {
  state: State = {
    open: false,
    message: '',
    errorMessage: '',
    selectTicketFocus: 'outbound',
    trainBookingState: defaultTrainTicketParams,
  };

  componentDidMount() {
    const { dispatch, fetchSignal, trainSearch } = this.props;
    dispatch(search());

    const searchParams = getSearchParams(fetchSignal, trainSearch);
    this.setState({
      trainBookingState: defaultTrainBookingState(
        searchParams.travellersInfo,
        !!searchParams.returnDate,
      ),
    });
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal, trainSearch, router } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      // bug on either react router OR connected react router OR react
      // need to check path matching
      if (matchPath(ROUTES.train.trainResult, router.location.pathname)) {
        dispatch(search());
        const searchParams = getSearchParams(fetchSignal, trainSearch);
        this.setState({
          open: false,
          trainBookingState: defaultTrainBookingState(
            searchParams.travellersInfo,
            !!searchParams.returnDate,
          ),
        });
      }
    }
  }

  onGetSeatByCarriage = async (
    oneDirection: OneDirection,
    resetTicket?: boolean,
    isInbound?: boolean,
  ) => {
    const { dispatch, fetchSignal, trainSearch } = this.props;
    const { trainBookingState } = this.state;

    const { carriage, train } = oneDirection;
    const { inbound, inboundSeating, outbound, outboundSeating } = trainBookingState;

    if (!carriage || !train) {
      return;
    }

    if (resetTicket) {
      const searchParams = getSearchParams(fetchSignal, trainSearch);

      this.setState({
        trainBookingState: {
          inbound: isInbound ? {} : { ...inbound },
          outbound: !isInbound ? {} : { ...outbound },
          inboundSeating:
            isInbound && searchParams
              ? defaultSeating(searchParams.travellersInfo)
              : { ...inboundSeating },
          outboundSeating:
            !isInbound && searchParams
              ? defaultSeating(searchParams.travellersInfo)
              : { ...outboundSeating },
        },
      });
    }

    this.setState({ open: true, seatData: undefined, message: '' });
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getSeatByCarriage}?doQueryId=${carriage.doQueryId}&doQueryDataId=${carriage.doQueryDataId}&carriageTempId=${carriage.carriageTempId}`,
        'get',
        true,
      ),
    );

    if (json.code === 200) {
      this.setState({
        seatData: json.data,
        trainSelected: train,
        carriageSelected: carriage,
        selectTicketFocus: isInbound ? 'inbound' : 'outbound',
      });
    } else {
      this.setState({ message: json.message });
    }
  };

  onUpdateSeatingInfo = (seatingInfo: SeatingInfo) => {
    const { trainBookingState, trainSelected, carriageSelected } = this.state;

    this.state.selectTicketFocus === 'outbound'
      ? this.setState({
          trainBookingState: {
            ...trainBookingState,
            outbound: { train: trainSelected, carriage: carriageSelected },
            outboundSeating: seatingInfo,
          },
          open: false,
        })
      : this.setState({
          trainBookingState: {
            ...trainBookingState,
            inbound: { train: trainSelected, carriage: carriageSelected },
            inboundSeating: seatingInfo,
          },
          open: false,
        });
  };

  goToBooking = () => {
    const { dispatch, fetchSignal, trainSearch, intl } = this.props;
    const { trainBookingState } = this.state;
    const searchParams = getSearchParams(fetchSignal, trainSearch);
    if (
      validTrainBookingParams(trainBookingState.outboundSeating) ||
      (searchParams.returnDate && validTrainBookingParams(trainBookingState.inboundSeating))
    ) {
      this.setState({ errorMessage: intl.formatMessage({ id: 'train.youNeedSelectSeat' }) });
      return;
    }
    dispatch(goToBookingInfoPage(trainBookingState));
  };

  render() {
    const {
      open,
      seatData,
      message,
      selectTicketFocus,
      trainBookingState,
      errorMessage,
    } = this.state;
    const { intl, fetchSignal, trainSearch, errorMessageBooking, dispatch } = this.props;
    const searchParams = getSearchParams(fetchSignal, trainSearch);

    return (
      <>
        <Helmet>
          <title>
            {intl.formatMessage({ id: 'train.title' })}:{' '}
            {searchParams
              ? `${searchParams.origin &&
                  searchParams.origin.trainStationName} > ${searchParams.destination &&
                  searchParams.destination.trainStationName} - ${searchParams.departureDate.format(
                  'L',
                )} ${!!searchParams.returnDate ? searchParams.returnDate.format('- L') : ''}`
              : ''}
          </title>
        </Helmet>
        <TrainResultTabletDesktop
          open={open}
          message={message}
          errorMessage={errorMessage}
          seatData={seatData}
          trainBookingState={trainBookingState}
          selectTicketFocus={selectTicketFocus}
          onGetSeatByCarriage={this.onGetSeatByCarriage}
          onCloseSelectSeat={() => this.setState({ open: false })}
          updateSeatingInfo={this.onUpdateSeatingInfo}
          goToBooking={this.goToBooking}
          onCloseErrorDialog={() => this.setState({ errorMessage: '' })}
        />
        <MessageDialog
          show={!!errorMessageBooking}
          message={
            <Typography variant="body1" style={{ padding: '0px 16px', textAlign: 'center' }}>
              {errorMessageBooking}
            </Typography>
          }
          onClose={() => dispatch(setErrorMessage())}
        />
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(TrainResult));
