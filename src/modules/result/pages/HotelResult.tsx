import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import HotelResultDesktop from '../components/hotel/HotelResultDesktop';
import HotelResultTablet from '../components/hotel/HotelResultTablet';
import { search } from '../redux/hotelResultReducer';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class HotelResult extends PureComponent<Props> {
  componentDidMount() {
    this.props.dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      window.scrollTo(0, 0);
      dispatch(search());
    }
  }

  render() {
    const { params } = this.props;
    return (
      <>
        <Helmet>
          <title>
            {params
              ? `${params.searchParams.location.provinceName}: ${params.searchParams.checkIn.format(
                  'L',
                )} - ${params.searchParams.checkOut.format('L')}`
              : 'Hotel result'}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <HotelResultDesktop />;
            }
            return <HotelResultTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    params: state.result.hotel.lastParams,
    fetchSignal: `${state.router.location.search}&${state.auth.auth}`,
  };
}

export default connect(mapStateToProps)(HotelResult);
