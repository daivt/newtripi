import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';
import RewardDetailTabletDesktop from '../components/reward/RewardDetailTabletDesktop';

export interface IRewardDetailProps extends RouteComponentProps<{ id: string }> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

interface State {
  data?: some;
}

class RewardDetail extends React.Component<IRewardDetailProps> {
  state: State = {};
  async componentDidMount() {
    const { dispatch, match } = this.props;
    const id = match.params.id;
    const json = await dispatch(fetchThunk(`${API_PATHS.getRewardsProgramDetail}?id=${id}`, 'get'));
    if (json.code === 200) {
      this.setState({ data: json.data });
    }
  }

  public render() {
    const { data } = this.state;
    return <RewardDetailTabletDesktop data={data} />;
  }
}

export default connect()(RewardDetail);
