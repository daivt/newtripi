import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import HotelDetailTabletDesktop from '../components/hotel/HotelDetailTabletDesktop';
import { fetchHotelDetails, fetchroomsData } from '../redux/hotelResultReducer';

interface Props extends ReturnType<typeof mapStateToProps>, RouteComponentProps<{ id: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  hotelData?: some;
  roomsData?: some[];
  showDialog: boolean;
}

class HotelDetail extends PureComponent<Props, State> {
  state: State = { showDialog: false };

  componentDidMount() {
    const { dispatch, booking } = this.props;
    if (booking.params) {
      dispatch(fetchHotelDetails(booking.params)).then(json => {
        if (json.code === 200) {
          this.setState({ hotelData: json.data });
        }
      });
      dispatch(fetchroomsData(booking.params)).then(json => {
        if (json.code === 200) {
          this.setState({ roomsData: json.data });
          if (json.data.length === 0) {
            this.setState({ showDialog: true });
          }
        } else {
          this.setState({ roomsData: [] });
          this.setState({ showDialog: true });
        }
      });
    }
  }

  render() {
    const { params } = this.props.booking;
    if (!params) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidLink)}`,
          }}
        />
      );
    }

    const { hotelData, roomsData } = this.state;

    return (
      <>
        <Helmet>
          <title>{hotelData ? `${hotelData.name} - ${hotelData.address}` : 'Hotel detail'}</title>
        </Helmet>
        <HotelDetailTabletDesktop hotelData={hotelData} roomsData={roomsData} params={params} />
        <MessageDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <FormattedMessage id="hotel.result.noRoom" />
            </div>
          }
          buttonMessageId="ok"
          show={this.state.showDialog}
          onClose={() => this.setState({ showDialog: false })}
        />
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return { booking: state.booking.hotel };
}

export default connect(mapStateToProps)(HotelDetail);
