import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { AppState } from '../../../redux/reducers';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import Helmet from 'react-helmet';
import { some, PAGE_SIZE } from '../../../constants';
import RewardsByCategoryTabletDesktop from '../components/reward/RewardsByCategoryTabletDesktop';
import { PROMOTIONS_PARAMS, REWARDS_SORT_BY, skeleton } from '../constants';
import { fetchThunk } from '../../common/redux/thunks';
import { API_PATHS } from '../../../API';

export interface Props
  extends ReturnType<typeof mapState2Props>,
    RouteComponentProps<{ categoryId: string }>,
    WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
const RewardsByCategory: React.FunctionComponent<Props> = props => {
  const { auth, dispatch, location, match, intl, categories } = props;
  const [data, setData] = React.useState<some | undefined>();
  const [loyalInfo, setLoyalInfo] = React.useState<some>({});
  const [page, setPage] = React.useState<number>(1);
  const [loading, setLoading] = React.useState<boolean>(false);
  const categoryId = parseInt(match.params.categoryId, 10);
  const params = new URLSearchParams(location.search);
  const sortBy = params.get(PROMOTIONS_PARAMS.sortBy) || REWARDS_SORT_BY.latest;
  const hasPurchase = params.get(PROMOTIONS_PARAMS.hasPurchase) === 'true';
  const fetchLoyalData = React.useCallback(async () => {
    dispatch(fetchThunk(`${API_PATHS.getMyLoyaltyInfo}`, 'post', true, '{}')).then(json => {
      if (json.code === 200) {
        setLoyalInfo(json.data);
      }
    });
  }, [dispatch]);
  const fetchData = React.useCallback(async () => {
    setPage(1);
    setLoading(true);
    setData(undefined);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getRewardsProgramByCategory,
        'post',
        true,
        JSON.stringify({
          categoryId,
          page: 1,
          hasPurchase: hasPurchase || null,
          order: sortBy || REWARDS_SORT_BY.common,
          size: PAGE_SIZE,
        }),
      ),
    );
    if (json.code === 200) {
      setData(json.data);
    }
    setLoading(false);
  }, [categoryId, dispatch, hasPurchase, sortBy]);
  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getRewardsProgramByCategory,
        'post',
        true,
        JSON.stringify({
          categoryId,
          page: page + 1,
          hasPurchase: hasPurchase || null,
          order: sortBy || REWARDS_SORT_BY.common,
          size: PAGE_SIZE,
        }),
      ),
    );
    if (json.code === 200) {
      setData(data =>
        data ? { ...json.data, rewards: data.rewards.concat(json.data.rewards) } : json.data,
      );
      setPage(page + 1);
    }
    setLoading(false);
  }, [categoryId, dispatch, hasPurchase, page, sortBy]);
  React.useEffect(() => {
    fetchData();
  }, [fetchData, match.params.categoryId, hasPurchase, sortBy, auth]);
  React.useEffect(() => {
    fetchLoyalData();
  }, [auth, fetchLoyalData]);
  const propsReward = {
    data,
    page,
    hasPurchase,
    sortBy,
    categoryId,
    loading,
    loyalInfo,
    loadMore,
    categories: categories || skeleton(7),
  };
  return (
    <div>
      <Helmet>
        <title>
          {intl.formatMessage(
            { id: 'rewards.byCategoryTitle' },
            { category: data ? data.description : '' },
          )}
        </title>
      </Helmet>
      <RewardsByCategoryTabletDesktop {...propsReward} />
    </div>
  );
};

const mapState2Props = (state: AppState) => {
  return {
    categories: state.common.rewardsCategories,
    auth: state.auth.auth,
  };
};

export default connect(mapState2Props)(injectIntl(RewardsByCategory));
