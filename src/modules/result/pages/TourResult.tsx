import { Helmet } from 'react-helmet';
import React, { PureComponent } from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import TourResultDesktop from '../components/tour/TourResultDesktop';
import TourResultTablet from '../components/tour/TourResultTablet';
import { search } from '../redux/tourResultReducer';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class TourResult extends PureComponent<Props> {
  componentDidMount() {
    this.props.dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      window.scrollTo(0, 0);
      dispatch(search());
    }
  }

  render() {
    const { intl, locationResultData, termResultData } = this.props;
    let input: string;
    let num: number;
    if (locationResultData) {
      input = locationResultData.destination.name;
      num = locationResultData.notFound ? 0 : locationResultData.total;
    }
    if (termResultData) {
      input = termResultData.input.term;
      num = termResultData.data.total;
    }
    return (
      <>
        <Helmet>
          <title>
            {`${intl.formatMessage({ id: 'tour' })}: ${
              locationResultData ? locationResultData.destination.name : ''
            }${termResultData ? termResultData.input.term : ''}`}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <TourResultDesktop num={num} input={input} />;
            }
            return <TourResultTablet num={num} input={input} />;
          }}
        </MediaQuery>
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    fetchSignal: `${state.router.location.search}&${state.auth.auth}`,
    locationResultData: state.result.tour.locationResultData,
    termResultData: state.result.tour.termResultData,
  };
}

export default connect(mapStateToProps)(injectIntl(TourResult));
