import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import TourDetailTabletDesktop from '../components/tour/TourDetailTabletDesktop';
import { fetchTourDetail } from '../redux/tourResultReducer';

interface Props extends ReturnType<typeof mapStateToProps>, RouteComponentProps<{ id: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class TourDetail extends PureComponent<Props, State> {
  state: State = {};

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchTourDetail());
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.location.search !== prevProps.location.search) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { params } = this.props.booking;
    if (!params) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidLink)}`,
          }}
        />
      );
    }

    const { tourData } = this.props;

    return (
      <>
        <Helmet>
          <title>{tourData ? `${tourData.name}` : 'Tour detail'}</title>
        </Helmet>
        <TourDetailTabletDesktop tourData={tourData} />
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    tourData: state.result.tour.tourData,
    booking: state.booking.tour,
    location: state.router.location,
  };
}

export default connect(mapStateToProps)(TourDetail);
