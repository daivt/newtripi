import * as React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some, PAGE_SIZE } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MyRewardsTabletDesktop from '../components/reward/MyRewardsTabletDesktop';
import { skeleton } from '../constants';
import { fetchMyRewards } from '../redux/rewardResultReducer';
export interface MyRewardsParams {
  page: number;
  total: number;
  loading: boolean;
}
interface State {
  data: some[];
  params: MyRewardsParams;
}
export interface Props
  extends ReturnType<typeof mapState2Props>,
    RouteComponentProps<{ category: string }>,
    WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

class MyRewards extends React.Component<Props, State> {
  state: State = {
    data: skeleton(8),
    params: {
      page: 1,
      total: 0,
      loading: false,
    },
  };
  componentDidMount() {
    const { params } = this.state;
    const { dispatch } = this.props;
    dispatch(fetchMyRewards(params.page)).then(json =>
      this.setState({ data: json.list, params: { ...params, total: json.total, loading: false } }),
    );
  }
  loadData() {
    const { params, data } = this.state;
    const { dispatch } = this.props;
    this.setState({ params: { ...params, loading: true } });
    dispatch(fetchMyRewards(params.page)).then(json =>
      this.setState({
        data: [...data, ...json.list],
        params: { ...params, total: json.total, loading: false },
      }),
    );
  }

  public render() {
    const { intl, categories } = this.props;
    const { data, params } = this.state;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'rewards.my' })}</title>
        </Helmet>
        <MyRewardsTabletDesktop
          data={data}
          categories={categories || skeleton(7)}
          loading={params.loading}
          remaining={params.total - params.page * PAGE_SIZE}
          loadMore={() => {
            this.setState({ params: { ...params, page: params.page + 1 } }, () => {
              this.loadData();
            });
          }}
        />
      </>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return { categories: state.common.rewardsCategories };
};

export default connect(mapState2Props)(injectIntl(MyRewards));
