import { push } from 'connected-react-router';
import stringify from 'json-stable-stringify';
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { stringifyParams } from '../../booking/utils';
import FlightResultDesktop from '../components/flight/FlightResultDesktop';
import FlightResultTablet from '../components/flight/FlightResultTablet';
import { search } from '../redux/flightResultReducer';
import { removeFilterParams } from '../utils';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class FlightResult extends PureComponent<Props> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal, filter } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      dispatch(search());
    }
    if (stringify(filter) !== stringify(prevProps.filter)) {
      window.scrollTo({ top: 0 });
    }
  }

  goToBooking = () => {
    const { booking, dispatch } = this.props;
    if (booking.tid) {
      const dest = `${ROUTES.flight.flightBookingInfo}?${stringifyParams(booking.tid)}`;
      dispatch(push(dest, { backableToResult: true }));
    }
  };

  render() {
    const { params, intl } = this.props;
    return (
      <>
        <Helmet>
          <title>
            {params
              ? `${intl.formatMessage({ id: 'outbound' })}: ${params.origin.location} (${params.origin.code}) > ${
                  params.destination.location
                } (${params.destination.code}) - ${params.departureDate.format('L')} ${
                  params.returnDate ? `-${params.returnDate.format('L')}` : ''
                }`
              : 'Flight search result'}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <FlightResultDesktop goToBooking={this.goToBooking} />;
            }
            return <FlightResultTablet goToBooking={this.goToBooking} />;
          }}
        </MediaQuery>
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    params: state.result.flight.searchParams,
    filter: state.result.flight.filterParams,
    fetchSignal: removeFilterParams(`?${state.router.location.search}&${state.auth.auth}`),
  };
}

export default connect(mapStateToProps)(injectIntl(FlightResult));
