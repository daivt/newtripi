import { Typography, Popover, Grid, Button } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { DARK_GREY, GREEN } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconCoinSvg } from '../../../../svg/coin.svg';
import { ReactComponent as IconLocation } from '../../../../svg/ic_pin.svg';
import { ReactComponent as IconHeart } from '../../../../svg/heart.svg';
import { ReactComponent as IconShare } from '../../../../svg/share.svg';
import { ReactComponent as ShareFacebook } from '../../../../svg/share_facebook.svg';
import { ReactComponent as ShareZalo } from '../../../../svg/share_zalo.svg';
import { BootstrapInput } from '../../../common/components/elements';
import CopyToClipboard from 'react-copy-to-clipboard';
import { scrollTo } from '../../../../utils';

const Box = styled.div`
  width: 472px;
  background: #ffffff;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 0px;
  padding: 15px;
`;

const OFFSET = 135;
const Line = styled.div`
  display: flex;
  align-items: center;
`;

interface IHotelBasicInfoProps {
  hotel: some;
  compact?: boolean;
  style?: React.CSSProperties;
  roomsData?: some[];
}

const HotelBasicInfo: React.FunctionComponent<IHotelBasicInfoProps> = props => {
  const { hotel, compact, roomsData } = props;
  const [anchorEl, setAnchorEl] = React.useState<Element | undefined>(undefined);
  const fullURL = window.location.href;
  const shareUrl = encodeURIComponent(fullURL);

  function handleShareHotel(event: React.MouseEvent<SVGSVGElement, MouseEvent>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(undefined);
  }

  const open = Boolean(anchorEl);
  return (
    <div
      style={{
        ...props.style,
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
        <Line style={{ minHeight: '40px', alignItems: 'center' }}>
          <Typography variant="h5">{hotel.name}</Typography>
          &nbsp;
          <Rating
            icon={<IconStar style={{ height: '20px' }} />}
            value={hotel.starNumber}
            readOnly
          />
        </Line>
        <Line style={{ height: '30px' }}>
          <IconLocation style={{ marginRight: '8px' }} />
          &nbsp;
          <Typography variant="body2" style={{ color: DARK_GREY }}>
            {hotel.address}
          </Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '64px',
              marginLeft: '15px',
            }}
          >
            <IconHeart style={{ cursor: 'pointer' }} />
            <IconShare style={{ cursor: 'pointer' }} onClick={handleShareHotel} />
            <Popover
              open={open}
              onClose={handleClose}
              anchorEl={anchorEl}
              style={{ zIndex: 100 }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <Box>
                <Grid container spacing={3}>
                  <Grid item xs={6} style={{ cursor: 'pointer' }}>
                    <a
                      href={`https://www.facebook.com/sharer/sharer.php?u=${shareUrl}`}
                      target="_blank"
                      rel="noopener noreferrer"
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        textDecoration: 'inherit',
                        color: 'inherit',
                      }}
                    >
                      <ShareFacebook style={{ paddingRight: '9px' }} />
                      &nbsp;
                      <Typography variant="body2">Share on Facebook</Typography>
                    </a>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}
                  >
                    <ShareZalo style={{ marginRight: '9px', height: '48px', width: 'auto' }} />
                    <Typography variant="body2">Share on Zalo</Typography>
                  </Grid>
                </Grid>
                <div style={{ marginBottom: '4px', marginTop: '15px' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="shareLink" />
                  </Typography>
                </div>
                <div style={{ marginRight: '20px', display: 'flex', alignItems: 'center' }}>
                  <BootstrapInput
                    value={fullURL}
                    inputProps={{
                      readOnly: true,
                    }}
                    style={{ width: '270px' }}
                  />
                  <CopyToClipboard text={fullURL}>
                    <Button
                      color="secondary"
                      variant="contained"
                      style={{ width: '150px', height: '36px', marginLeft: '16px' }}
                    >
                      <Typography variant="body2">
                        <FormattedMessage id="copyUrl" />
                      </Typography>
                    </Button>
                  </CopyToClipboard>
                </div>
              </Box>
            </Popover>
          </div>
        </Line>
      </div>
      <div
        style={{
          display: 'flex',
          width: '300px',
          flexDirection: 'column',
          marginTop: '8px',
        }}
      >
        <Line style={{ justifyContent: 'flex-end' }}>
          <Typography variant="h5" color="secondary">
            {roomsData && roomsData[0] ? (
              <FormattedNumber value={roomsData[0].finalPrice} />
            ) : (
              '---'
            )}
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
          <Typography>
            <FormattedMessage id="hotel.result.info.priceNote" />
          </Typography>
        </Line>
        {!compact && (
          <Line style={{ justifyContent: 'flex-end' }}>
            <IconCoinSvg style={{ marginRight: '8px' }} />
            <Typography variant="body1" style={{ color: GREEN }}>
              {roomsData && roomsData[0] ? <FormattedNumber value={roomsData[0].bonusPoint} /> : 0}
              &nbsp;
              <FormattedMessage id="booking.point" />
            </Typography>
          </Line>
        )}
        <Line style={{ justifyContent: 'flex-end' }}>
          <Button
            size="large"
            style={{
              width: '264px',
              marginBottom: '12px',
              marginTop: '8px',
            }}
            variant="contained"
            color="secondary"
            onClick={() => scrollTo('hotel.result.info.roomList', OFFSET)}
          >
            <FormattedMessage id="hotel.result.list.bookRoom" />
          </Button>
        </Line>
      </div>
    </div>
  );
};

export default HotelBasicInfo;
