import { Container } from '@material-ui/core';
import * as React from 'react';
import { DESKTOP_WIDTH, some } from '../../../../constants';
import { HotelBookingParams } from '../../../booking/redux/hotelBookingReducer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import HotelBasicInfo from './HotelBasicInfoBox';
import HotelDetailBreadcrumbs from './HotelDetailBreadcrumbs';
import HotelDetailImageBox from './HotelDetailImageBox';
import HotelDetailTabs from './HotelDetailTabs';
import Footer from '../../../common/components/Footer';
import { PageWrapper } from '../../../common/components/elements';

export interface IHotelDetailDesktopProps {
  hotelData?: some;
  roomsData?: some[];
  params: HotelBookingParams;
}

class HotelDetailDesktop extends React.Component<IHotelDetailDesktopProps> {
  public render() {
    const { hotelData, roomsData, params } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <div>
          <Header light />
        </div>
        {!hotelData ? (
          <div
            style={{
              flex: 1,
              height: '200px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingIcon />
          </div>
        ) : (
          <>
            <Container>
              <HotelDetailBreadcrumbs />
              <HotelBasicInfo hotel={hotelData} roomsData={roomsData} />
            </Container>
            <HotelDetailImageBox hotelData={hotelData} roomsData={roomsData} />
            <HotelDetailTabs hotelData={hotelData} roomsData={roomsData} params={params} />
          </>
        )}
        <Footer />
      </PageWrapper>
    );
  }
}

export default HotelDetailDesktop;
