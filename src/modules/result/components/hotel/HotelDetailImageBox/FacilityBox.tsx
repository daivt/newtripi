import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../colors';
import { some } from '../../../../../constants';
import { getFacilityIcon, getRelaxIcon, getInternetIcon } from '../../../utils';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 32px;
`;
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;

type Features = 'facility' | 'relax' | 'internet';
class FacilityBox extends React.PureComponent<{ hotelData: some }, { extend: boolean }> {
  state = { extend: false };

  public render() {
    const { hotelData } = this.props;
    const { extend } = this.state;
    const { features } = hotelData;
    const length = features.facilityIds.length + features.relaxIds.length;

    const data = features.internetId
      .map((value: number, index: number) => {
        return {
          id: value,
          des: features.internet[index],
          src: features.facilitiesUrls.find((v: any) => v[`I${value}`]),
          type: 'internet' as Features,
        };
      })
      .concat(
        features.facilityIds.map((value: number, index: number) => {
          return {
            id: value,
            des: features.facilities[index],
            src: features.facilitiesUrls.find((v: any) => v[`F${value}`]),
            type: 'facility' as Features,
          };
        }),
      )
      .concat(
        features.relaxIds.map((value: number, index: number) => {
          return {
            id: value,
            des: features.relaxes[index],
            src: features.facilitiesUrls.find((v: any) => v[`R${value}`]),
            type: 'relax' as Features,
          };
        }),
      );

    return (
      <>
        {features && (
          <div style={{ marginBottom: '12px' }}>
            <div
              style={{
                maxHeight: extend ? '100%' : '160px',
                overflow: 'hidden',
              }}
            >
              {data.slice(0, extend ? data.length : 7).map((v: some, index: number) => (
                <Line key={index}>
                  <img
                    style={{ marginRight: '12px', width: '24px', height: '24px' }}
                    src={
                      (v.type as Features) === 'relax'
                        ? v.src?.[`R${v.id}`] || getRelaxIcon(v.id)
                        : (v.type as Features) === 'facility'
                        ? v.src?.[`F${v.id}`] || getFacilityIcon(v.id)
                        : v.src?.[`I${v.id}`] || getInternetIcon(v.id)
                    }
                    alt=""
                  />
                  <Typography variant="caption">{v.des}</Typography>
                </Line>
              ))}
            </div>

            {length > 0 && (
              <Line
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  height: '32px',
                }}
              >
                <Extend onClick={() => this.setState({ extend: !extend })}>
                  <FormattedMessage id={extend ? 'collapse' : 'seeAll'} />
                </Extend>
              </Line>
            )}
          </div>
        )}
      </>
    );
  }
}

export default FacilityBox;
