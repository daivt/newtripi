import { Typography, Tab, withStyles, useTheme } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { some } from '../../../../../constants';
import { CustomTabs } from '../../../../common/components/elements';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import PickRoomBox from '../PickRoomBox';
import FacilitiyBox from './FacilityBox';
import ReviewBox from './ReviewBox';
import SwipeableViews from 'react-swipeable-views';

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
  flex-shrink: 0;
`;

export const INFO_BOX_WIDTH = '320px';

const TabContentContainer = styled.div`
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 15px;
`;
const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: 500,
    minWidth: 130,
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

const Wrapper = styled.div`
  background: white;
  width: ${INFO_BOX_WIDTH};
  min-height: 100vh;
  height: 100%;
  max-height: 600px;
  overflow: auto;
  display: flex;
  flex-direction: column;
  z-index: 1400;
  @media (min-height: 725px) {
    max-height: 100vh;
  }
`;
const ScrollBar = styled.div`
  overflow: auto;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border: 8px solid white;
  }
  &::-webkit-scrollbar {
    background: white;
    width: 24px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;
interface Props {
  hotelData: some;
  style?: React.CSSProperties;
  roomsData?: some[];
  onClick(): void;
}

const InfoBox: React.FunctionComponent<Props> = props => {
  const [currentTabIndex, setIndex] = React.useState(0);
  const { hotelData, roomsData, onClick } = props;
  const theme = useTheme();
  return (
    <Wrapper>
      <Line style={{ minHeight: 'auto' }}>
        <Typography variant="h5" style={{ marginLeft: '27px', marginTop: '8px' }}>
          {hotelData.name}
        </Typography>
      </Line>
      <Line style={{ marginLeft: '14px' }}>
        <Rating
          icon={<IconStar style={{ height: '20px' }} />}
          value={hotelData.starNumber}
          readOnly
        />
      </Line>
      <ScrollBar style={{ flex: 1 }}>
        <div style={{ marginLeft: '16px' }}>
          <HotelBasicStatsBox hotelData={hotelData} />
        </div>
        <div
          style={{
            marginTop: '16px',
            marginLeft: '12px',
            marginRight: '12px',
            position: 'relative',
            boxSizing: 'border-box',
            maxWidth: '320px',
          }}
        >
          <CustomTabs
            variant="fullWidth"
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => setIndex(val)}
          >
            <CustomTab fullWidth label={<FormattedMessage id="hotel.result.info.describe" />} />
            <CustomTab fullWidth label={<FormattedMessage id="hotel.result.info.review" />} />
          </CustomTabs>

          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={currentTabIndex}
            onChangeIndex={val => setIndex(val)}
          >
            <TabContentContainer>
              {currentTabIndex === 0 && <FacilitiyBox hotelData={hotelData} />}
            </TabContentContainer>
            <TabContentContainer>
              {currentTabIndex === 1 && <ReviewBox hotelData={hotelData} />}
            </TabContentContainer>
          </SwipeableViews>
        </div>
        <Line style={{ marginLeft: '27px', marginTop: '-12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="hotel.result.info.useful" />
          </Typography>
        </Line>
        {hotelData.checkInTime && (
          <Line style={{ padding: '0px 27px' }}>
            <Typography variant="body1">
              <FormattedMessage id="hotel.result.info.checkIn" />
            </Typography>
            <Typography color="textSecondary" variant="body2">
              {hotelData.checkInTime}
            </Typography>
          </Line>
        )}
        {hotelData.checkOutTime && (
          <Line style={{ padding: '0px 27px' }}>
            <Typography variant="body1">
              <FormattedMessage id="hotel.result.info.checkOut" />
            </Typography>
            <Typography color="textSecondary" variant="body2">
              {hotelData.checkOutTime}
            </Typography>
          </Line>
        )}
        <Line style={{ padding: '0px 27px' }}>
          <Typography variant="body1">
            <FormattedMessage id="hotel.result.info.distanceToAirport" />
          </Typography>
          <Typography color="textSecondary" variant="body2">
            <FormattedMessage
              id="hotel.result.info.distance"
              values={{ num: hotelData.distanceToAirport }}
            />
          </Typography>
        </Line>
        <br />
        <div style={{ padding: '0px 27px' }} dangerouslySetInnerHTML={{ __html: hotelData.desc }} />
      </ScrollBar>
      <PickRoomBox roomsData={roomsData} onClick={onClick} />
    </Wrapper>
  );
};

export default InfoBox;
