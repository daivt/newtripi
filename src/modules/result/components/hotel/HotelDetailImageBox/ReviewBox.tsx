import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../colors';
import { some } from '../../../../../constants';
const LIST_SERVICE: some = [
  { id: 'hotel.result.info.overallScore', value: 'overallScore' },
  { id: 'hotel.result.info.mealScore', value: 'mealScore' },
  { id: 'hotel.result.info.cleanlinessScore', value: 'cleanlinessScore' },
  { id: 'hotel.result.info.locationScore', value: 'locationScore' },
  { id: 'hotel.result.info.roomScore', value: 'roomScore' },
  { id: 'hotel.result.info.serviceScore', value: 'serviceScore' },
  { id: 'hotel.result.info.sleepQualityScore', value: 'sleepQualityScore' },
];

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 32px;
`;
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;

class ReviewBox extends React.PureComponent<{ hotelData: some }, { extend: boolean }> {
  state = { extend: false };
  public render() {
    const { hotelData } = this.props;
    const { extend } = this.state;
    return (
      <>
        {hotelData && (
          <div style={{ marginBottom: '12px' }}>
            <div
              style={{
                maxHeight: extend ? '100%' : '160px',
                overflow: 'hidden',
              }}
            >
              {LIST_SERVICE.map((v: some, index: number) => (
                <Line key={index}>
                  <Typography
                    variant={index !== 0 ? 'caption' : 'body2'}
                    style={{ color: index === 0 ? BLUE : undefined }}
                  >
                    <FormattedMessage id={v.id} />
                  </Typography>
                  <Typography variant="caption" style={{ color: index === 0 ? BLUE : undefined }}>
                    {hotelData[v.value]}
                  </Typography>
                </Line>
              ))}
            </div>
            <Line
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Extend onClick={() => this.setState({ extend: !extend })}>
                <FormattedMessage
                  id={extend ? 'hotel.result.info.collapse' : 'hotel.result.info.seeAll'}
                />
              </Extend>
            </Line>
          </div>
        )}
      </>
    );
  }
}

export default ReviewBox;
