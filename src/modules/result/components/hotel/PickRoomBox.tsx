import * as React from 'react';
import styled from 'styled-components';
import { Typography, Button } from '@material-ui/core';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { GREEN } from '../../../../colors';
import { ReactComponent as CoinSvg } from '../../../../svg/coin.svg';
import { some } from '../../../../constants';

const Line = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 30px;
`;

interface IPickRoomBoxProps {
  roomsData?: some[];
  onClick(): void;
}

const PickRoomBox: React.FunctionComponent<IPickRoomBoxProps> = props => {
  const { roomsData, onClick } = props;
  const price = roomsData && roomsData[0] ? roomsData[0].finalPrice : undefined;
  const points = roomsData && roomsData[0] ? roomsData[0].bonusPoint : undefined;

  return (
    <div
      style={{
        boxShadow:
          '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
        background: 'white',
        position: 'relative',
      }}
    >
      <div style={{ width: '275px', margin: '8px auto' }}>
        <Line style={{ justifyContent: 'flex-end' }}>
          <Typography variant="h5" color="secondary">
            {price !== undefined ? <FormattedNumber value={price} /> : '---'}&nbsp;
            <FormattedMessage id="currency" />
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="hotel.result.info.priceNote" />
          </Typography>
        </Line>
        <Line style={{ justifyContent: 'flex-end', minHeight: '24px' }}>
          <CoinSvg style={{ marginRight: '8px' }} />
          <Typography variant="body1" style={{ color: GREEN }}>
            {points !== undefined ? <FormattedNumber value={points} /> : 0}
            &nbsp;
            <FormattedMessage id="booking.point" />
          </Typography>
        </Line>
        <div style={{ textAlign: 'center', marginTop: '6px' }}>
          <Button color="secondary" variant="contained" size="large" fullWidth onClick={onClick}>
            <FormattedMessage id="hotel.result.info.bookRoom" />
          </Button>
        </div>
      </div>
    </div>
  );
};

export default PickRoomBox;
