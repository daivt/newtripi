import { Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import IconLocation from '@material-ui/icons/LocationOnOutlined';
import Rating from '@material-ui/lab/Rating';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREEN, GREY, LIGHT_BLUE, PRIMARY, RED, SECONDARY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconCoinSvg } from '../../../../svg/coin.svg';
import { ReactComponent as IconDiscount } from '../../../../svg/ic_discount.svg';
import { ReactComponent as IconHandMoney } from '../../../../svg/ic_hotel_result_hand_money.svg';
import { ReactComponent as IconPromo } from '../../../../svg/ic_hotel_result_promo.svg';
import { subtitle3Styles } from '../../../common/components/elements';
import { getScoreDescription } from '../../utils';
import ProgressiveImage from '../../../common/components/ProgressiveImage';

const Wrapper = styled.div<{ skeleton: boolean }>`
  min-height: 210px;
  display: flex;
  margin-bottom: 16px;
  border: 1px solid ${GREY};
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);
  background-color: #fff;
  :hover {
    background: ${props => (props.skeleton ? undefined : fade(SECONDARY, 0.05))};
  }
`;

export interface IHotelResultItemProps {
  hotel?: some;
}

const HotelResultItem: React.FunctionComponent<IHotelResultItemProps> = props => {
  const { hotel } = props;

  if (!hotel) {
    return (
      <Wrapper skeleton>
        <div style={{ position: 'relative', width: '270px' }}>
          <Skeleton
            variant="rect"
            style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          />
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            padding: '12px',
          }}
        >
          <Skeleton width="80%" />
          <Skeleton width="50%" />
        </div>
      </Wrapper>
    );
  }

  const { bestAgencyPrices } = hotel;
  const buySignal = bestAgencyPrices && bestAgencyPrices.length ? bestAgencyPrices[0].buySignal : undefined;
  const promotionInfo = bestAgencyPrices && bestAgencyPrices.length ? bestAgencyPrices[0].promotionInfo : undefined;

  return (
    <Wrapper skeleton={false}>
      <div style={{ position: 'relative', width: '270px' }}>
        <ProgressiveImage
          style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          src={hotel.logo}
          alt=""
        />
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: 1,
          paddingRight: '12px',
        }}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between', flex: 1 }}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              paddingLeft: '8px',
              paddingTop: '8px',
              flex: 1,
            }}
          >
            <Typography variant="subtitle1">{hotel.name}</Typography>
            <Rating value={hotel.starNumber} readOnly size="small" />
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                paddingLeft: '8px',
                paddingTop: '4px',
              }}
            >
              <IconLocation style={{ color: DARK_GREY, padding: '2px' }} />
              <Typography variant="body2" style={{ paddingLeft: '6px', color: DARK_GREY }}>
                {hotel.address}
              </Typography>
            </div>
            {buySignal && buySignal.point && !(promotionInfo.discountPercentage > 2) && (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  paddingLeft: '8px',
                  paddingTop: '4px',
                }}
              >
                <IconPromo style={{ height: '28px', width: 'auto' }} />
                <Typography variant="body2" style={{ paddingLeft: '6px', color: DARK_GREY }}>
                  <FormattedMessage
                    id="hotel.result.usePoint"
                    values={{
                      point: <FormattedNumber value={buySignal.point} />,
                      text: (
                        <span style={{ color: RED }}>
                          <FormattedMessage id="hotel.result.usePointDiscountPercent" />
                          &nbsp;
                          <FormattedNumber value={buySignal.pointValue || 0} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </span>
                      ),
                    }}
                  />
                </Typography>
              </div>
            )}
          </div>

          <div
            style={{
              flexShrink: 0,
              display: 'flex',
              alignItems: 'flex-end',
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}
          >
            <div style={{ display: 'flex', paddingTop: '12px', alignItems: 'center' }}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'flex-end',
                  flexDirection: 'column',
                  paddingRight: '12px',
                }}
              >
                <Typography variant="body2" style={{ ...subtitle3Styles, color: BLUE, fontWeight: 500 }}>
                  <FormattedMessage id={getScoreDescription(hotel.overallScore / 10)} />
                </Typography>
                <Typography variant="caption" color="textSecondary" style={{ lineHeight: '14px' }}>
                  <FormattedNumber value={hotel.numberOfReviews} maximumFractionDigits={0} />
                  &nbsp;
                  <FormattedMessage id="hotel.result.review" />
                </Typography>
              </div>
              <div
                style={{
                  width: '32px',
                  height: '32px',
                  backgroundColor: BLUE,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: '4px',
                }}
              >
                <Typography variant="subtitle1" style={{ color: '#fff' }}>
                  <FormattedNumber value={hotel.overallScore / 10} />
                </Typography>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'column' }}>
              {promotionInfo && !!promotionInfo.discountPercentage && (
                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                  <IconDiscount className="svgFill" style={{ fill: PRIMARY }} />
                  <Typography variant="caption">
                    <FormattedMessage
                      id="booking.hotel.saveAmount"
                      values={{ amount: promotionInfo.discountPercentage }}
                    />
                  </Typography>
                </div>
              )}
              {buySignal && buySignal.point && promotionInfo?.priceBeforePromotion < hotel?.minPrice ? (
                <>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Typography variant="body1" style={{ textDecoration: 'line-through', color: GREY }}>
                      <FormattedNumber value={hotel.minPrice} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'flex-end',
                      padding: '4px 0',
                    }}
                  >
                    <Typography variant="h5" style={{ color: SECONDARY, display: 'flex' }}>
                      <FormattedNumber value={buySignal.price || 0} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                    <Typography variant="body1" style={{ color: DARK_GREY, paddingLeft: '4px', display: 'flex' }}>
                      + <FormattedNumber value={buySignal.point} />
                      &nbsp;
                      <FormattedMessage id="booking.point" />
                    </Typography>
                  </div>
                </>
              ) : (
                <>
                  {hotel.minPrice < promotionInfo?.priceBeforePromotion && (
                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                      <Typography variant="body1" style={{ textDecoration: 'line-through', color: GREY }}>
                        <FormattedNumber value={promotionInfo.priceBeforePromotion} />
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </Typography>
                    </div>
                  )}
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'flex-end',
                      padding: '4px 0',
                    }}
                  >
                    <Typography variant="h5" style={{ color: SECONDARY, display: 'flex' }}>
                      <FormattedNumber value={hotel.minPrice || 0} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </>
              )}

              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
                <IconCoinSvg style={{ marginRight: '10px' }} />
                <Typography variant="body1" style={{ color: GREEN }}>
                  <FormattedNumber value={hotel.bonusPoint} />
                  &nbsp;
                  <FormattedMessage id="booking.point" />
                </Typography>
              </div>
            </div>
          </div>
        </div>

        <div
          style={{
            color: LIGHT_BLUE,
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '8px 0',
            alignItems: 'center',
          }}
        >
          <IconHandMoney />
          <Typography variant="caption" style={{ paddingLeft: '4px' }}>
            <FormattedMessage id="hotel.result.includeVAT" />
          </Typography>
        </div>
      </div>
    </Wrapper>
  );
};

export default connect()(HotelResultItem);
