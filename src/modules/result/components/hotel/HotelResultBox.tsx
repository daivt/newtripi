import { Typography, Button } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../colors';
import { ROUTES, some, PAGE_SIZE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setParams } from '../../../booking/redux/hotelBookingReducer';
import Link from '../../../common/components/Link';
import { HOTEL_BOOK_PARAMS_NAMES, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../../common/constants';
import { stringifyHotelSearchState } from '../../../search/utils';
import { searchMore } from '../../redux/hotelResultReducer';
import HotelResultItem from './HotelResultItem';

function mapStateToProps(state: AppState) {
  return {
    searching: state.result.hotel.searching,
    params: state.result.hotel.lastParams,
    pageOffset: state.result.hotel.pageOffset,
  };
}

export interface IHotelResultBoxProps extends ReturnType<typeof mapStateToProps> {
  data?: some;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

export default connect(mapStateToProps)(
  class HotelResultBox extends React.PureComponent<IHotelResultBoxProps> {
    public render() {
      const { data, dispatch, searching, params, pageOffset } = this.props;
      const hotels = data ? (data.hotels as some[]) : null;
      return (
        <div style={{ marginTop: '15px' }}>
          {params && hotels ? (
            hotels.map(hotel => (
              <Link
                key={hotel.id}
                to={{
                  pathname: `${ROUTES.hotel.hotelDetail}`,
                  search: `?${stringifyHotelSearchState(params.searchParams, undefined, undefined, true)}&${
                    HOTEL_BOOK_PARAMS_NAMES.hotelId
                  }=${hotel.id}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${false}`,
                  state: { backableToResult: true },
                }}
                onClick={() => {
                  dispatch(setParams({ ...params.searchParams, hotelId: hotel.id, direct: false }));
                }}
                style={{ color: 'inherit' }}
              >
                <HotelResultItem hotel={hotel} />
              </Link>
            ))
          ) : (
            <>
              <HotelResultItem />
              <HotelResultItem />
              <HotelResultItem />
              <HotelResultItem />
              <HotelResultItem />
            </>
          )}
          {searching && <HotelResultItem />}
          {data && data.totalResults > pageOffset * PAGE_SIZE && !searching && (
            <div style={{ textAlign: 'center', marginTop: '23px' }}>
              <Button
                onClick={async () => {
                  await dispatch(searchMore());
                }}
              >
                <Typography style={{ color: BLUE }}>
                  <FormattedMessage
                    id="result.displayMore"
                    values={{ num: data.totalResults - PAGE_SIZE * pageOffset }}
                  />
                </Typography>
              </Button>
            </div>
          )}
        </div>
      );
    }
  },
);
