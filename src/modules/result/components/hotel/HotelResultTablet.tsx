import { Container, IconButton } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import FilterList from '@material-ui/icons/FilterList';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import HotelSearch from '../../../search/components/HotelSearch';
import { setFilter } from '../../redux/hotelResultReducer';
import HotelResultBox from './HotelResultBox';
import HotelResultBoxHeader from './HotelResultBoxHeader';
import HotelResultFilter from './HotelResultFilter';
import HotelResultSortBox from './HotelResultSortBox';
import MapButtonBox from './MapButtonBox';
import MapDialog from './MapDialog';

const mapState2Props = (state: AppState) => {
  return {
    data: state.result.hotel.data,
    search: state.search.hotel,
    filter: state.result.hotel.filterParams,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const HotelResultTablet: React.FC<Props> = props => {
  const { data, search, filter, dispatch } = props;
  const [openMap, setOpenMap] = React.useState(false);
  const [showFilters, setShowFilters] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <HotelSearch />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex' }}>
          <div
            style={{
              width: '40px',
              overflow: 'visible',
              position: 'relative',
              zIndex: 100,
              minHeight: '100%',
              display: 'flex',
            }}
          >
            <div
              style={{
                flexShrink: 0,
                width: '300px',
                transform: showFilters ? 'translateX(0)' : 'translateX(calc(-100% + 40px))',
                backgroundColor: BACKGROUND,
                padding: '0 10px 20px 0',
                borderRight: `1px solid ${GREY}`,
                transition: 'all 300ms',
                minHeight: '100%',
              }}
            >
              <div style={{ position: 'sticky', top: 65 }}>
                <div style={{ textAlign: 'end' }}>
                  {showFilters ? (
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShowFilters(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  ) : (
                    <IconButton
                      onClick={() => setShowFilters(true)}
                      style={{
                        margin: '10px 0',
                      }}
                    >
                      <FilterList style={{ color: DARK_GREY }} />
                    </IconButton>
                  )}
                </div>
                <div
                  style={{
                    opacity: showFilters ? 1 : 0.4,
                    position: 'relative',
                  }}
                >
                  <HotelResultFilter
                    filterParams={filter}
                    setFilter={value => dispatch(setFilter(value))}
                    data={data}
                  />
                  <MapButtonBox setOpenMap={() => setOpenMap(true)} />
                  {!showFilters && (
                    <div
                      style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          <div style={{ padding: '22px 0', flex: 1, minHeight: '400px', marginLeft: '10px' }}>
            <div style={{ display: 'flex', alignItems: 'center', margin: '10px 0', height: '80px' }}>
              <HotelResultBoxHeader style={{ flex: 1 }} />
              <HotelResultSortBox />
            </div>
            <HotelResultBox data={data} />
          </div>
        </div>
      </Container>
      <Footer />
      <MapDialog open={openMap} onClose={() => setOpenMap(false)} filterTmp={filter} searchTmp={search} />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(HotelResultTablet);
