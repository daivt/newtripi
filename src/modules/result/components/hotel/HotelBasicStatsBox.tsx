import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';

const Line = styled.div`
  display: flex;
  align-items: center;
  min-height: 40px;
`;

const HotelBasicStatsBox: React.FunctionComponent<{ hotelData: some }> = props => {
  const { hotelData } = props;

  return (
    <div>
      <Line style={{ justifyContent: 'flex-start', marginTop: '16px' }}>
        <div
          style={{
            background: BLUE,
            borderRadius: '4px',
            width: '32px',
            height: '32px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Typography variant="h6" style={{ color: 'white' }}>
            <FormattedNumber value={hotelData.overallScore / 10} />
          </Typography>
        </div>
        <div style={{ marginLeft: '12px' }}>
          <Typography variant="body2" style={{ color: BLUE, lineHeight: '16px', fontWeight: 500 }}>
            <FormattedMessage
              id={
                hotelData.overallScore > 90
                  ? 'hotel.result.info.ratingExcellent'
                  : hotelData.overallScore > 80 && hotelData.overallScore < 90
                  ? 'hotel.result.info.ratingVeryGood'
                  : hotelData.overallScore > 70 && hotelData.overallScore < 80
                  ? 'hotel.result.info.ratingGood'
                  : hotelData.overallScore > 60 && hotelData.overallScore < 70
                  ? 'hotel.result.info.ratingSatisfied'
                  : 'hotel.result.info.ratingNormal'
              }
            />
          </Typography>
          <Typography color="textSecondary" variant="caption">
            <FormattedMessage
              id={'hotel.result.info.ratingNote'}
              values={{ num: hotelData.numReviews }}
            />
          </Typography>
        </div>
      </Line>
    </div>
  );
};

export default HotelBasicStatsBox;
