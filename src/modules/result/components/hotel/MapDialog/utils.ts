/* eslint-disable no-nested-ternary */
import { remove, uniq } from 'lodash';
import moment from 'moment';
import { DATE_FORMAT, HOTEL_SEARCH_HISTORY, PAGE_SIZE, some } from '../../../../../constants';
import {
  HOTEL_ACTIVITY_DIRECT_PARAM,
  HOTEL_BOOK_PARAMS_NAMES,
  HOTEL_FILTER_PARAM_NAMES,
  HOTEL_SEARCH_PARAM_NAMES,
} from '../../../../common/constants';
import { MAX_SEARCH_HISTORY } from '../../../../search/constants';
import { FILTER_MAX_PRICE, HOTEL_SORT_BY } from '../../../constants';

export interface Location {
  hotelId: number;
  provinceId: number;
  provinceName: string;
  streetId: number;
  streetName: string;
  districtId: number;
  districtName: string;
  slug: string;
  numHotels: number;
  name: string;
  latitude: number;
  longitude: number;
  thumbnailUrl: string | null;
}

export interface GuestCountInfo {
  roomCount: number;
  adultCount: number;
  childCount: number;
  childrenAges: number[];
}
export const defaultGuestCountInfo: GuestCountInfo = {
  roomCount: 1,
  adultCount: 2,
  childCount: 0,
  childrenAges: [],
};
export interface HotelSearchParams {
  location?: Location;
  checkIn: string; // DATE_FORMAT
  checkOut: string;
  guestInfo: GuestCountInfo;
}

export const defaultHotelSearchParams: HotelSearchParams = {
  checkIn: moment()
    .startOf('day')
    .format(DATE_FORMAT),
  checkOut: moment()
    .add(1, 'days')
    .startOf('day')
    .format(DATE_FORMAT),
  guestInfo: defaultGuestCountInfo,
};

export interface HotelFilterParams {
  price: number[];
  stars: number[];
  subLocations: some[];
  hotelTypes: number[];
  facilities: string[];
  sortBy: string;
  pageOffset: number;
  pageSize: number;
}

export const defaultHotelFilterParams: HotelFilterParams = {
  price: [0, FILTER_MAX_PRICE],
  stars: [],
  subLocations: [],
  hotelTypes: [],
  facilities: [],
  sortBy: HOTEL_SORT_BY.default,
  pageOffset: 1,
  pageSize: PAGE_SIZE,
};
export interface HotelBookingParams extends HotelSearchParams {
  hotelId: number;
  shrui?: string;
  direct?: boolean;
}
export function isValidGuestInfo(info: GuestCountInfo) {
  if (info.roomCount > 0 && info.adultCount > 0 && !info.childrenAges.includes(-1)) {
    return true;
  }
  return false;
}

export interface MapParams {
  lat: number;
  lon: number;
  radius: number;
}

export function stringifyHotelSearchAndFilterParams(
  search: HotelSearchParams,
  filter?: HotelFilterParams,
  mapParams?: MapParams,
  skipLocation = false,
) {
  const arr = [];
  if (!skipLocation) {
    arr.push(`${HOTEL_SEARCH_PARAM_NAMES.location}=${encodeURIComponent(JSON.stringify(search.location))}`);
  }
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkIn}=${search.checkIn}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkOut}=${search.checkOut}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`);

  if (filter) {
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.area}=${encodeURIComponent(JSON.stringify(filter.subLocations))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.facility}=${encodeURIComponent(JSON.stringify(filter.facilities))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.price}=${encodeURIComponent(JSON.stringify(filter.price))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.star}=${encodeURIComponent(JSON.stringify(filter.stars))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.type}=${encodeURIComponent(JSON.stringify(filter.hotelTypes))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.sort}=${encodeURIComponent(filter.sortBy)}`);
  }

  if (mapParams) {
    arr.push(`${HOTEL_SEARCH_PARAM_NAMES.mapParams}=${encodeURIComponent(JSON.stringify(mapParams))}`);
  }

  return arr.join('&');
}

export function parseHotelSearchParams(params: URLSearchParams, skipLocation = false): HotelSearchParams {
  const locationStr = params.get(HOTEL_SEARCH_PARAM_NAMES.location);
  if (!locationStr && !skipLocation) {
    throw new Error('No location');
  }
  const location = locationStr ? JSON.parse(locationStr) : undefined;

  let checkIn = params.get(HOTEL_SEARCH_PARAM_NAMES.checkIn) || moment().format(DATE_FORMAT);
  const checkInMoment = checkIn ? moment(checkIn, DATE_FORMAT) : undefined;
  if (!checkInMoment?.isValid() || checkInMoment?.isBefore(moment().startOf('day'))) {
    checkIn = moment().format(DATE_FORMAT);
  }

  let checkOut = params.get(HOTEL_SEARCH_PARAM_NAMES.checkOut) || '';
  const checkOutMoment = moment(checkOut, DATE_FORMAT);
  if (!checkOutMoment.isValid() || checkOutMoment.isBefore(moment().startOf('day'))) {
    checkOut = moment().format(DATE_FORMAT);
    throw new Error('Invalid checkout date');
  }

  const guestInfoStr = params.get(HOTEL_SEARCH_PARAM_NAMES.guestInfo);
  const guestInfo = guestInfoStr ? JSON.parse(guestInfoStr) : defaultGuestCountInfo;
  if (!isValidGuestInfo(guestInfo)) {
    throw new Error('Invalid guest info');
  }

  return { location, checkIn, checkOut, guestInfo };
}

export function validGuestCount(roomCount: number, adultCount: number, childCount: number) {
  return (
    roomCount >= 1 &&
    roomCount <= 9 &&
    adultCount >= 1 &&
    adultCount <= 36 &&
    childCount <= 9 &&
    adultCount >= roomCount
  );
}

export function validTravellerCount(adultCount: number, childCount: number, babyCount: number) {
  return (
    adultCount >= 1 && childCount >= 0 && babyCount >= 0 && adultCount + childCount <= 9 && babyCount <= adultCount
  );
}

export function validateChildrenAgesInfo(childrenAges: number[]) {
  return childrenAges.includes(-1);
}

export function addHotelRecentSearch(search: HotelSearchParams) {
  const recentSearchData = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;
  const searchString = stringifyHotelSearchAndFilterParams(search);

  if (!recentSearchData) {
    localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let hotelSearch: string[] = JSON.parse(recentSearchData);

  hotelSearch.unshift(searchString);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  hotelSearch = [...uniq(hotelSearch)];

  if (hotelSearch.length > MAX_SEARCH_HISTORY) {
    hotelSearch.pop();
  }
  localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify(hotelSearch));
}

export function getHotelSearchHistory(): string[] {
  const hotelSearchStr = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;

  if (!hotelSearchStr) {
    return [];
  }

  const hotelSearch: string[] = JSON.parse(hotelSearchStr);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  return hotelSearch;
}
export function clearHotelSearchHistory() {
  localStorage.removeItem(HOTEL_SEARCH_HISTORY);
}

export function parseHotelDetailParams(params: URLSearchParams, skipShrui = false) {
  const hotelIdStr = params.get(HOTEL_BOOK_PARAMS_NAMES.hotelId);
  if (!hotelIdStr) {
    throw new Error('No hotel ID');
  }
  const hotelId = parseInt(hotelIdStr, 10);
  if (!Number.isInteger(hotelId)) {
    throw new Error('Invalid hotel ID');
  }
  const shrui = params.get(HOTEL_BOOK_PARAMS_NAMES.shrui) || '';
  if (!shrui && !skipShrui) {
    throw new Error('No shrui ID');
  }
  const direct = params.get(HOTEL_ACTIVITY_DIRECT_PARAM) === 'true';

  return { hotelId, shrui, direct };
}

export function stringifyHotelBookingParams(search: HotelBookingParams, skipLocation = true) {
  const arr = [];
  if (!skipLocation) {
    arr.push(`${HOTEL_SEARCH_PARAM_NAMES.location}=${encodeURIComponent(JSON.stringify(search.location))}`);
  }
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkIn}=${search.checkIn}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkOut}=${search.checkOut}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`);
  arr.push(`${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${encodeURIComponent(JSON.stringify(search.hotelId))}`);
  if (search.shrui) {
    arr.push(`${HOTEL_BOOK_PARAMS_NAMES.shrui}=${encodeURIComponent(search.shrui)}`);
  }
  arr.push(`${HOTEL_ACTIVITY_DIRECT_PARAM}=${encodeURIComponent(JSON.stringify(search.direct))}`);

  return arr.join('&');
}
export function parseHotelBookingParams(
  params: URLSearchParams,
  skipShrui = true,
  skipLocation = true,
): HotelBookingParams {
  const value = parseHotelDetailParams(params, skipShrui);
  const search = parseHotelSearchParams(params, skipLocation);
  return { ...search, ...value };
}

function rad(x: number) {
  return (x * Math.PI) / 180;
}

export function getDistance(p1: some, p2: some) {
  const R = 6378137; // Earth’s mean radius in meter
  const dLat = rad(p2.lat - p1.lat);
  const dLong = rad(p2.lng - p1.lng);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d / 1000; // returns the distance in meter
}
