/* eslint-disable no-nested-ternary */
import { Button, IconButton, InputAdornment, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { DATE_FORMAT } from '../../../../../constants';
import { ReactComponent as IconCalender } from '../../../../../svg/calendar.svg';
import { ReactComponent as IconHotel } from '../../../../../svg/ic_hotel.svg';
import { ReactComponent as IconPin } from '../../../../../svg/ic_pin.svg';
import { Col } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { Location } from '../../../../common/models';
import { getInputStr } from '../../../../search/components/GuestInfoBox';
import { HotelSearchParamsState } from '../../../../search/redux/hotelSearchReducer';
import ParamsDialog from '../BookingParamsBox/ParamsDialog';

export function renderOption(option: Location) {
  const style: React.CSSProperties = {
    flexShrink: 0,
    margin: '0 15px 0 5px',
    width: '28px',
    height: '28px',
    borderRadius: option.thumbnailUrl ? '50%' : undefined,
  };
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {option.hotelId !== -1 ? (
        <IconHotel style={style} />
      ) : option.thumbnailUrl ? (
        <img style={style} alt="" src={option.thumbnailUrl} />
      ) : (
        <IconPin style={style} />
      )}

      <div style={{ display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
        <Typography variant="body2" style={{ padding: '5px' }} noWrap>
          {option.name}
        </Typography>
        {option.hotelId !== -1 && (
          <Typography variant="caption" style={{ padding: '0px 5px 3px' }}>
            {option.provinceName}
          </Typography>
        )}
      </div>
    </div>
  );
}

export const getLabel = (option: Location) => {
  return `${option.name || option.provinceName}`;
};

interface Props {
  params: HotelSearchParamsState;
  onSearch(params: HotelSearchParamsState): void;
  showButton?: boolean;
}

const SearchBox: React.FC<Props> = props => {
  const { params, onSearch, showButton } = props;
  const [open, setOpen] = React.useState(false);
  const intl = useIntl();

  return (
    <>
      <Col style={{ cursor: 'pointer', alignItems: 'center' }} onClick={() => setOpen(true)}>
        <FormControlTextField
          label={intl.formatMessage({ id: 'search.chooseLocation' })}
          placeholder={intl.formatMessage({ id: 'search.chooseLocation' })}
          readOnly
          value={`${params.checkIn.format(DATE_FORMAT)} - ${params.checkOut.format(DATE_FORMAT)}`}
          formControlStyle={{ margin: 0 }}
          optional
          startAdornment={
            <InputAdornment position="start" style={{ marginLeft: 8 }}>
              <IconButton size="small" edge="start">
                <IconCalender />
              </IconButton>
            </InputAdornment>
          }
        />
        <FormControlTextField
          label={intl.formatMessage({ id: 'search.stayDuration' })}
          placeholder={intl.formatMessage({ id: 'search.stayDuration' })}
          readOnly
          value={getInputStr(params.guestInfo, intl)}
          formControlStyle={{ margin: 0 }}
          optional
          startAdornment={
            <InputAdornment position="start" style={{ marginLeft: 8 }}>
              <IconButton size="small" edge="start">
                <IconCalender />
              </IconButton>
            </InputAdornment>
          }
        />
        {showButton && (
          <Button variant="contained" color="secondary" size="large" disableElevation style={{ width: 220 }}>
            <FormattedMessage id="change" />
          </Button>
        )}
      </Col>
      <ParamsDialog
        params={params}
        open={open}
        onClose={() => setOpen(false)}
        onChange={newParams => {
          onSearch(newParams);
        }}
      />
    </>
  );
};

export default SearchBox;
