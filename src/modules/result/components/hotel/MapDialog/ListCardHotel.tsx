/* eslint-disable no-nested-ternary */
import { Paper } from '@material-ui/core';
import React from 'react';
import { RESULT_WIDTH } from '.';
import { ROUTES, some } from '../../../../../constants';
import { Col } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { HOTEL_ACTIVITY_DIRECT_PARAM, HOTEL_BOOK_PARAMS_NAMES } from '../../../../common/constants';
import { HotelSearchParamsState } from '../../../../search/redux/hotelSearchReducer';
import { stringifyHotelSearchState } from '../../../../search/utils';
import HotelInfoCard from './HotelInfoCard';

interface Props {
  data?: some;
  showList: boolean;
  search: HotelSearchParamsState;
}

const ListCardHotel: React.FC<Props> = props => {
  const { data, showList, search } = props;
  const onMouseHover = React.useCallback((id?: number) => {
    const array = document.getElementsByClassName('svgAnimation');
    for (let i = 0; i < array.length; i += 1) {
      array[i].classList.remove('svgAnimation', 'mainPin');
    }
    if (id) {
      document.getElementById(`${id}`)?.classList.add('svgAnimation', 'mainPin');
    }
  }, []);

  return (
    <>
      <Paper
        style={{
          overflowY: 'auto',
          overflowX: 'hidden',
          position: 'relative',
          borderRadius: 0,
          transform: showList ? 'translateX(0)' : `translateX(-${RESULT_WIDTH}px)`,
          transition: 'all 0.2s',
          width: RESULT_WIDTH,
        }}
        elevation={2}
        onMouseLeave={() => onMouseHover()}
      >
        <Col
          style={{
            padding: 8,
          }}
        >
          {data?.hotels ? (
            data?.hotels?.map((hotel: some) => (
              <Link
                key={hotel.id}
                target="blank"
                to={{
                  pathname: ROUTES.hotel.hotelDetail,
                  search: `${stringifyHotelSearchState(search)}&${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${
                    hotel.id
                  }&${HOTEL_ACTIVITY_DIRECT_PARAM}=${false}`,
                }}
                style={{ color: 'inherit' }}
                onMouseEnter={() => {
                  onMouseHover(hotel.id);
                }}
              >
                <HotelInfoCard hotelData={hotel} />
              </Link>
            ))
          ) : (
            <>
              <HotelInfoCard />
              <HotelInfoCard />
              <HotelInfoCard />
              <HotelInfoCard />
              <HotelInfoCard />
            </>
          )}
        </Col>
      </Paper>
    </>
  );
};

export default ListCardHotel;
