/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable no-nested-ternary */
import { ButtonBase, Dialog, IconButton, Paper, Tooltip, withStyles, Zoom } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CloseIcon from '@material-ui/icons/Close';
import GoogleMapReact, { ChangeEventValue } from 'google-map-react';
import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { DATE_FORMAT, KEY_GOOGLE_MAP, ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IcLocation } from '../../../../../svg/ic_pin_map.svg';
import { Col, snackbarSetting } from '../../../../common/components/elements';
import { RawLink } from '../../../../common/components/Link';
import { HOTEL_ACTIVITY_DIRECT_PARAM, HOTEL_BOOK_PARAMS_NAMES } from '../../../../common/constants';
import { fetchThunk } from '../../../../common/redux/thunks';
import { HotelSearchParamsState } from '../../../../search/redux/hotelSearchReducer';
import { stringifyHotelSearchState } from '../../../../search/utils';
import { HOTEL_SORT_BY } from '../../../constants';
import { HotelFilterState } from '../../../redux/hotelResultReducer';
import HotelResultFilter from '../HotelResultFilter';
import HotelInfoCard from './HotelInfoCard';
import ListCardHotel from './ListCardHotel';
import SearchBox from './SearchBox';
import './style.css';
import { getDistance } from './utils';

export const FILTER_WIDTH = 275;
export const RESULT_WIDTH = 338;

const TooltipCS = withStyles(theme => ({
  tooltip: {
    padding: 0,
    margin: 0,
  },
}))(Tooltip);

const DialogCS = withStyles(theme => ({
  container: {
    textAlign: 'unset',
    overflow: 'hidden',
  },
}))(Dialog);

const LocationComponent = (props: { lat: number; lng: number; data: some; search: HotelSearchParamsState }) => {
  const { data, search } = props;
  return (
    <RawLink
      key={data.id}
      target="blank"
      to={{
        pathname: ROUTES.hotel.hotelDetail,
        search: `${stringifyHotelSearchState(search)}&${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${
          data.id
        }&${HOTEL_ACTIVITY_DIRECT_PARAM}=${false}`,
      }}
      style={{ color: 'inherit' }}
    >
      <TooltipCS key={data.id} TransitionComponent={Zoom} title={<HotelInfoCard hotelData={data} />}>
        <div className="svgMask" id={data.id}>
          <IcLocation className="svgPin" />
        </div>
      </TooltipCS>
    </RawLink>
  );
};

export interface MapParams {
  latitude: number;
  longitude: number;
  radius: number;
}

interface Props {
  filterTmp: HotelFilterState;
  searchTmp: HotelSearchParamsState;
  open: boolean;
  onClose(): void;
}

const MapDialog: React.FC<Props> = props => {
  const { filterTmp, searchTmp, open, onClose } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [search, setSearch] = React.useState<HotelSearchParamsState>(searchTmp);
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<HotelFilterState>(filterTmp);
  const [showList, setShowList] = React.useState(true);
  const [params, setParams] = React.useState<MapParams>({
    latitude: search?.location?.latitude || 0,
    longitude: search?.location?.longitude || 0,
    radius: 20,
  });
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const getCenter = React.useMemo(() => {
    return {
      lat: searchTmp?.location ? searchTmp?.location?.latitude : 0,
      lng: searchTmp?.location ? searchTmp?.location?.longitude : 0,
      radius: 20,
    };
  }, [searchTmp]);

  const searchData = React.useCallback(
    debounce(
      async (paramsSearch: HotelSearchParamsState, filterParams: HotelFilterState, paramsMap: MapParams) => {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.searchHotel,
            'post',
            true,
            JSON.stringify({
              adults: paramsSearch?.guestInfo.adultCount,
              children: paramsSearch?.guestInfo.childCount,
              childrenAges: paramsSearch?.guestInfo.childrenAges,
              rooms: paramsSearch?.guestInfo.roomCount,
              checkIn: paramsSearch?.checkIn.format(DATE_FORMAT),
              checkOut: paramsSearch?.checkOut.format(DATE_FORMAT),
              provinceId: paramsSearch?.location?.provinceId,
              districtId: paramsSearch?.location?.districtId,
              streetId: paramsSearch?.location?.streetId,
              filters: {
                subLocationIds: filterParams.subLocations,
                priceMax: filterParams.price[1],
                priceMin: filterParams.price[0],
                stars: filterParams.stars,
                types: filterParams.hotelTypes,
                relaxes: filterParams.facilities,
                services: [],
              },
              ...paramsMap,
              pageOffset: 1,
              size: 100,
              sortBy: HOTEL_SORT_BY.default,
            }),
          ),
        );
        if (json?.code === 200) {
          setData(json.data);
        } else {
          onClose();
          json.message &&
            enqueueSnackbar(
              json.message,
              snackbarSetting(json.message, key => closeSnackbar(key)),
            );
        }
      },
      500,
      {
        trailing: true,
        leading: true,
      },
    ),
    [],
  );

  const setParamsMap = React.useCallback(
    debounce(
      async (value: ChangeEventValue) => {
        setParams({
          latitude: value.center.lat,
          longitude: value.center.lng,
          radius: getDistance({ lat: params.latitude, lng: params.longitude }, value.marginBounds.ne),
        });
      },
      2500,
      {
        trailing: true,
        leading: true,
      },
    ),
    [],
  );
  React.useEffect(() => {
    if (search && open) {
      searchData(search, filter, params);
    }
  }, [filter, open, params, search, searchData]);

  React.useEffect(() => {
    setSearch(searchTmp);
  }, [searchTmp]);

  if (!searchTmp) {
    return null;
  }
  return (
    <>
      <DialogCS
        open={open}
        onEnter={() => {
          setFilter(filterTmp);
        }}
        PaperProps={{
          style: {
            position: 'absolute',
            margin: 40,
            maxWidth: 'calc(100% - 80px)',
            maxHeight: 'calc(100% - 80px)',
            overflow: 'hidden',
            display: 'flex',
            borderRadius: 4,
          },
        }}
        scroll="body"
        fullScreen
      >
        <IconButton
          size="small"
          onClick={() => onClose()}
          style={{
            position: 'fixed',
            top: 24,
            right: 24,
            zIndex: 2000,
            boxShadow: '0 2px 4px rgba(0,0,0,0.24)',
            background: 'white',
          }}
        >
          <CloseIcon />
        </IconButton>
        <Paper
          style={{
            overflowY: 'auto',
            overflowX: 'hidden',
            width: FILTER_WIDTH,
            position: 'relative',
            borderRadius: 0,
            zIndex: 1010,
          }}
          elevation={2}
        >
          <Col
            style={{
              padding: 8,
            }}
          >
            <SearchBox params={search} onSearch={value => setSearch(value)} />
            <HotelResultFilter filterParams={filter} setFilter={setFilter} data={data} />
          </Col>
        </Paper>
        <ListCardHotel search={search} showList={showList} data={data} />
        <ButtonBase
          style={{
            width: 24,
            height: 24,
            transform: showList ? 'translateX(0)' : `translateX(-${RESULT_WIDTH}px)`,
            transition: 'all 0.2s',
            position: 'absolute',
            top: 16,
            left: FILTER_WIDTH + RESULT_WIDTH,
            background: 'white',
            boxShadow: '1px 1px 2px 0 rgba(0,0,0,0.24)',
            borderRadius: ' 0px 2px 2px 0px',
            zIndex: 1100,
          }}
          onClick={() => setShowList(!showList)}
        >
          {showList ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </ButtonBase>
        <div
          className="scrollParent"
          style={{
            position: 'absolute',
            right: 0,
            top: 0,
            height: '100%',
            width: 'auto',
            zIndex: 1050,
            left: showList ? RESULT_WIDTH + FILTER_WIDTH : FILTER_WIDTH,
          }}
        >
          <GoogleMapReact
            bootstrapURLKeys={{ key: KEY_GOOGLE_MAP }}
            defaultCenter={getCenter}
            defaultZoom={14}
            onChange={value => {
              setParamsMap(value);
            }}
            options={(maps: any) => {
              return {
                mapTypeControl: true,
                mapTypeControlOptions: {
                  position: maps.ControlPosition.TOP_RIGHT,
                },
                fullscreenControl: false,
              };
            }}
          >
            {data?.hotels.map((one: some, index: number) => (
              <LocationComponent key={one.id} lat={one.latitude} lng={one.longitude} data={one} search={search} />
            ))}
          </GoogleMapReact>
        </div>
      </DialogCS>
    </>
  );
};

export default MapDialog;
