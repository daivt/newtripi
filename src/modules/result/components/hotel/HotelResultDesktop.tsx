import { Container } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import HotelSearch from '../../../search/components/HotelSearch';
import { setFilter } from '../../redux/hotelResultReducer';
import HotelResultBox from './HotelResultBox';
import HotelResultBoxHeader from './HotelResultBoxHeader';
import HotelResultFilter from './HotelResultFilter';
import HotelResultSortBox from './HotelResultSortBox';
import MapButtonBox from './MapButtonBox';
import MapDialog from './MapDialog';

const mapState2Props = (state: AppState) => {
  return {
    data: state.result.hotel.data,
    search: state.search.hotel,
    filter: state.result.hotel.filterParams,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const HotelResultDesktop: React.FC<Props> = props => {
  const { data, filter, search, dispatch } = props;
  const [openMap, setOpenMap] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light />
      <HotelSearch />
      <Container style={{ flex: 1 }}>
        <div style={{ padding: '22px 0', display: 'flex' }}>
          <div style={{ width: '270px', marginRight: '30px' }}>
            <StickyDiv style={{ top: 80 }}>
              <HotelResultFilter filterParams={filter} setFilter={value => dispatch(setFilter(value))} data={data} />
              <MapButtonBox setOpenMap={() => setOpenMap(true)} />
            </StickyDiv>
          </div>
          <div style={{ flex: 1, minHeight: '400px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <HotelResultBoxHeader />
              <HotelResultSortBox />
            </div>
            <HotelResultBox data={data} />
          </div>
        </div>
      </Container>
      <Footer />
      <MapDialog open={openMap} onClose={() => setOpenMap(false)} filterTmp={filter} searchTmp={search} />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(HotelResultDesktop);
