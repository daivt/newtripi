import { Button as MUIButton } from '@material-ui/core';
import styled from 'styled-components';
import { PRIMARY } from '../../../../colors';
import { MUI_THEME } from '../../../../setupTheme';

export const DropDownHeader = styled.div`
  cursor: pointer;
`;

export const ArrowStyle = styled.span<{ active: boolean }>`
  display: inline-block;
  margin-right: 6px;
  font-size: 10px;
  transition: all 300ms;
  transform: ${props => (!props.active ? 'rotate(0deg)' : 'rotate(180deg)')};
`;

export const DropDownStyle = {
  padding: '10px 0',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingLeft: '14px',
};

export const Button = styled(MUIButton)<{ selected: boolean }>`
  &&& {
    background-color: ${props => (props.selected ? props.theme.primary : undefined)};
    box-shadow: none;
    text-transform: none;
    color: ${props => (props.selected ? '#fff' : PRIMARY)};
    border-color: ${PRIMARY};
    font-size: ${MUI_THEME.typography.body2.fontSize};
    border-radius: 16px;
    max-height: 32px;
    padding: 0px 4px;
    min-width: 48px;
    margin-top: 8px;
  }
`;
