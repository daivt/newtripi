import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { fetchHotelReviews } from '../../../redux/hotelResultReducer';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import CommentCard from './CommentCard';
import { Extend, Line } from './styles';
import ReviewSideBox from './ReviewSideBox';
import { OFFSET } from '.';
import { scrollTo } from '../../../../../utils';

interface IReviewBoxProps {
  hotelData: some;
  id: string;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  roomsData?: some[];
}

const ReviewBox: React.FunctionComponent<IReviewBoxProps> = props => {
  const { hotelData, dispatch, id } = props;
  const [open, setOpen] = React.useState(false);
  const [reviews, setReviews] = React.useState<some | undefined | null>(undefined);

  React.useEffect(() => {
    const fn = async () => {
      const json = await dispatch(fetchHotelReviews(hotelData.id, 1));
      if (json.code === 200) {
        setReviews(json.data);
      } else {
        setReviews(null);
      }
    };
    fn();
  }, [dispatch, hotelData]);

  return (
    <>
      {reviews ? (
        <>
          <Typography id={id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id="hotel.result.reviews" values={{ num: reviews.totalReviews }} />
          </Typography>
          <HotelBasicStatsBox hotelData={hotelData} />

          <div style={{ marginTop: '16px' }}>
            {reviews.reviews.slice(0, 2).map((v: some, index: number) => (
              <CommentCard key={index} data={v} />
            ))}
          </div>
          <Line style={{ height: '32px' }}>
            <Extend style={{ margin: 0 }} onClick={() => setOpen(true)}>
              <Typography variant="caption">
                <FormattedMessage id="hotel.result.seeReview" />
              </Typography>
            </Extend>
          </Line>
          <ReviewSideBox
            hotelData={hotelData}
            reviews={reviews.reviews}
            totalReviews={reviews.totalReviews}
            open={open}
            onClose={() => setOpen(false)}
            roomsData={props.roomsData}
            onClick={() => {
              setOpen(false);
              scrollTo('hotel.result.info.roomList', OFFSET);
            }}
          />
        </>
      ) : reviews === null ? (
        <>
          <Typography id={id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id="hotel.result.reviews" values={{ num: 0 }} />
          </Typography>
          <HotelBasicStatsBox hotelData={hotelData} />
        </>
      ) : (
        <LoadingIcon />
      )}
    </>
  );
};

export default connect()(ReviewBox);
