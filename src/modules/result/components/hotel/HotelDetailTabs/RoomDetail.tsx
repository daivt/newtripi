import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import ReactImageGallery from 'react-image-gallery';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREEN, GREY, RED } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import default_image from '../../../../../svg/default_image.svg';
import { HotelBookingParams } from '../../../../booking/redux/hotelBookingReducer';
import Link from '../../../../common/components/Link';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../../../common/constants';
import { stringifyHotelSearchState } from '../../../../search/utils';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;

const ScrollBox = styled.div`
  width: 576px;
  /* box-shadow: 0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12),
    0px 24px 38px rgba(0, 0, 0, 0.14); */
  overflow: auto;
  /* margin-top: 16px; */
  &::-webkit-scrollbar-thumb {
    background: gray;
    border: 8px solid white;
  }
  &::-webkit-scrollbar {
    background: white;
    width: 24px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;
interface Props {
  data: some;
  open: boolean;
  onClose(): void;
  params: HotelBookingParams;
  setParams(params: HotelBookingParams): void;
  routerState?: some;
}

const RoomDetail: React.FunctionComponent<Props> = props => {
  const { open, data, onClose, params, setParams, routerState } = props;
  return (
    <Dialog
      open={open}
      PaperProps={{
        style: {
          backgroundColor: 'white',
          boxShadow: 'none',
          minHeight: '95vh',
          //   minWidth: '576px',
          display: 'flex',
          borderRadius: '0px',
          flexDirection: 'column',
        },
      }}
      onBackdropClick={onClose}
    >
      <Line style={{ justifyContent: 'space-between', margin: '10px 16px 10px' }}>
        <Typography variant="subtitle2">{data.roomTitle}</Typography>{' '}
        <IconButton
          style={{
            padding: '0px',
            background: 'white',
          }}
          color="default"
          size="medium"
          onClick={() => onClose()}
        >
          <CloseIcon style={{ color: GREY }} />
        </IconButton>
      </Line>
      <ScrollBox style={{ flex: 1 }}>
        {Object.keys(data).length > 1 && (
          <>
            <div style={{ padding: '8px 0px 0px 16px' }}>
              <ReactImageGallery
                items={
                  data.roomImages.length > 0
                    ? data.roomImages.map((v: string) => {
                        return {
                          renderItem() {
                            return (
                              <ProgressiveImage
                                title={data.roomTitle}
                                src={v}
                                alt={v}
                                style={{
                                  maxWidth: '536px',
                                  maxHeight: '302px',
                                  objectFit: 'cover',
                                }}
                              />
                            );
                          },
                          renderThumbInner() {
                            return (
                              <ProgressiveImage
                                title={data.roomTitle}
                                src={v}
                                alt={v}
                                style={{
                                  maxWidth: '126px',
                                  maxHeight: '94px',
                                  objectFit: 'cover',
                                }}
                              />
                            );
                          },
                        };
                      })
                    : [
                        {
                          original: default_image,
                          thumbnail: default_image,
                          originalTitle: 'no image',
                          thumbnailTitle: 'no image',
                        },
                      ]
                }
                infinite
                thumbnailPosition="bottom"
                showThumbnails
                showIndex
                slideDuration={400}
                startIndex={0}
                showFullscreenButton={false}
              />
              <div style={{ padding: '0px 16px' }}>
                {data.bedInfo && (
                  <>
                    <Line style={{ height: '48px' }}>
                      <Typography variant="h6">
                        <FormattedMessage id="hotel.result.info.roomInfo" />
                      </Typography>
                    </Line>
                    <Line style={{ width: '100%' }}>
                      <Typography variant="body2">{data.bedInfo}</Typography>
                    </Line>
                    {data.isMultiBedGroup && (
                      <Line style={{ width: '100%', display: 'flex' }}>
                        <Typography variant="body2">
                          <span style={{ fontWeight: 500 }}>
                            <FormattedMessage id="m.setup.note2" />
                          </span>
                          <FormattedMessage id="hotel.result.info.warning" />
                        </Typography>
                      </Line>
                    )}
                  </>
                )}
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.result.info.feature" />
                  </Typography>
                </Line>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                  {data.roomFeatures.map((v: string) => (
                    <Line key={v} style={{ width: '50%' }}>
                      <Typography variant="body2">{v}</Typography>
                    </Line>
                  ))}
                </div>
              </div>
              <div style={{ padding: '0px 16px' }}>
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.result.list.cancel" />
                  </Typography>
                </Line>
                {data.cancellationPoliciesList.length > 0 ? (
                  <div>
                    {data.cancellationPoliciesList.map((v: string, index: number) => (
                      <Typography key={index} variant="body2" style={{ padding: '4px 0px' }}>
                        +&nbsp;
                        {v}
                      </Typography>
                    ))}
                  </div>
                ) : (
                  <Typography variant="body2" style={{ color: RED }}>
                    <FormattedMessage id="hotel.result.list.noRefund" />
                  </Typography>
                )}
              </div>
              <div style={{ padding: '0px 16px' }}>
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.result.useful.title" />
                  </Typography>
                </Line>
                <div
                  dangerouslySetInnerHTML={{
                    __html: data.roomDescription,
                  }}
                />
              </div>
            </div>
          </>
        )}
      </ScrollBox>
      <div
        style={{
          height: '88px',
          bottom: 0,
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          position: 'relative',
          background: 'white',
        }}
      >
        <Line style={{ justifyContent: 'space-between', padding: '0px 32px', height: '88px' }}>
          <div>
            <Line>
              <Typography variant="subtitle1" style={{ color: RED }}>
                <FormattedNumber value={data.finalPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
              <Typography variant="subtitle2">
                <FormattedMessage id="hotel.result.list.oneNight" />
              </Typography>
            </Line>
            <Typography variant="caption" style={{ color: GREEN }}>
              <FormattedMessage id="hotel.result.list.bonusPoint" values={{ num: data.bonusPoint }} />
            </Typography>
          </div>
          <Link
            onClick={() => setParams({ ...params, shrui: data.SHRUI })}
            to={{
              pathname: `${ROUTES.hotel.hotelBookingInfo}`,
              search: `?${stringifyHotelSearchState(params, undefined, undefined, true)}&${
                HOTEL_BOOK_PARAMS_NAMES.hotelId
              }=${params.hotelId}&${HOTEL_BOOK_PARAMS_NAMES.shrui}=${encodeURIComponent(data.SHRUI)}`,
              state: { ...routerState, backableToDetail: true },
            }}
          >
            <Button
              color="secondary"
              variant="contained"
              size="large"
              style={{ minWidth: '112px', borderRadius: '16px' }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="hotel.result.list.bookNow" />
              </Typography>
            </Button>
          </Link>
        </Line>
      </div>
    </Dialog>
  );
};

export default RoomDetail;
