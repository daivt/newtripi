import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { some } from '../../../../../constants';

const Tip = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 32px;
  min-width: 170px;
  width: 50%;
`;

const LIST_USE_FULL = [
  { id: 'checkInTime', title: 'hotel.result.useful.checkInTime' },
  { id: 'checkOutTime', title: 'hotel.result.useful.checkOutTime' },
  { id: 'distanceToCityCenter', title: 'hotel.result.useful.distanceToCityCenter', note: 'km' },
  {
    id: 'distanceToDistrictCenter',
    title: 'hotel.result.useful.distanceToDistrictCenter',
    note: 'km',
  },
  { id: 'distanceToAirport', title: 'hotel.result.useful.distanceToAirport', note: 'km' },
  { id: 'numFloors', title: 'hotel.result.useful.numFloors' },
  { id: 'buildYear', title: 'hotel.result.useful.buildYear' },
  { id: 'numRestaurants', title: 'hotel.result.useful.numRestaurants' },
  { id: 'numBars', title: 'hotel.result.useful.numBars' },
  { id: 'numRooms', title: 'hotel.result.useful.numRooms' },
];
const UsefulInfoBox: React.FunctionComponent<{ hotelData: some }> = props => {
  const { hotelData } = props;
  return (
    <div>
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {LIST_USE_FULL.map((v: some, index: number) => (
          <Tip
            key={index}
            style={{
              paddingRight: index % 2 === 0 ? '32px' : undefined,
              paddingLeft: index % 2 === 1 ? '32px' : undefined,
            }}
          >
            <Typography variant="body1">
              <FormattedMessage id={v.title} />
            </Typography>
            <Typography variant="caption">
              <FormattedMessage
                id={'hotel.result.useful'}
                values={{
                  num:
                    hotelData[v.id] &&
                    (typeof hotelData[v.id] === 'string' ||
                      (typeof hotelData[v.id] === 'number' && hotelData[v.id] > 0))
                      ? hotelData[v.id]
                      : '---',
                  note: v.note,
                }}
              />
            </Typography>
          </Tip>
        ))}
      </div>
    </div>
  );
};

export default UsefulInfoBox;
