import { IconButton, Slide, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { Action } from 'typesafe-actions';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import CommentCard from './CommentCard';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import { fetchHotelReviews } from '../../../redux/hotelResultReducer';
import PickRoomBox from '../PickRoomBox';

const WIDTH = '416px';
const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
`;
const ScrollBar = styled.div`
  overflow: auto;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border-right: 8px solid white;
    /* border-left: 8px solid white; */
  }
  &::-webkit-scrollbar {
    background: transparent;
    width: 16px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;

interface Props {
  open: boolean;
  onClose(): void;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  reviews: some[];
  hotelData: some;
  totalReviews: number;
  roomsData?: some[];
  onClick(): void;
}

interface State {
  reviews: some[];
}

class ReviewSideBox extends React.PureComponent<Props, State> {
  state: State = {
    reviews: this.props.reviews,
  };

  loadMore = async (page: number) => {
    const { dispatch, hotelData } = this.props;
    if (page > 25) {
      // safety valve
      return;
    }
    const json = await dispatch(fetchHotelReviews(hotelData.id, page));
    if (json.code === 200) {
      this.setState({ reviews: this.state.reviews.concat(json.data.reviews) });
    }
  };

  public render() {
    const { hotelData, open, onClose, totalReviews, roomsData, onClick } = this.props;
    const { reviews } = this.state;
    return (
      <div>
        <Slide in={open} direction={'left'}>
          <div
            style={{
              background: 'white',
              width: WIDTH,
              position: 'fixed',
              top: '0px',
              right: '0px',
              bottom: '0px',
              zIndex: 1300,
              boxShadow:
                '0px 8px 10px rgba(0, 0, 0, 0.2), 0px 6px 30px rgba(0, 0, 0, 0.12), 0px 16px 24px rgba(0, 0, 0, 0.14)',
              overflow: 'auto',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Line>
              <div style={{ margin: '10px 0' }}>
                <Typography variant="h5" style={{ marginLeft: '27px' }}>
                  {hotelData.name}
                </Typography>
                <Line style={{ minHeight: '25px' }}>
                  <Rating
                    style={{ marginLeft: '14px' }}
                    icon={<IconStar style={{ height: '20px' }} />}
                    value={hotelData.starNumber}
                    readOnly
                  />
                </Line>
              </div>
              <IconButton size="medium" onClick={onClose}>
                <CloseIcon />
              </IconButton>
            </Line>
            <ScrollBar style={{ flex: 1 }}>
              <div style={{ marginLeft: '16px' }}>
                <HotelBasicStatsBox hotelData={hotelData} />
              </div>
              <InfiniteScroll
                pageStart={1}
                initialLoad={false}
                loadMore={this.loadMore}
                hasMore={reviews.length < totalReviews}
                loader={
                  <LoadingIcon
                    key="loader"
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  />
                }
                useWindow={false}
              >
                <div style={{ marginTop: '16px', marginLeft: '16px' }}>
                  {reviews.map((v: some, index: number) => (
                    <CommentCard key={index} data={v} style={{ justifyContent: 'space-between' }} />
                  ))}
                </div>
              </InfiniteScroll>
            </ScrollBar>
            <PickRoomBox roomsData={roomsData} onClick={onClick} />
          </div>
        </Slide>
      </div>
    );
  }
}

export default connect()(ReviewSideBox);
