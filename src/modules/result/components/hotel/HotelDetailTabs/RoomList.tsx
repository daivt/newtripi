import * as React from 'react';
import { some } from '../../../../../constants';
import { HotelBookingParams, setParams } from '../../../../booking/redux/hotelBookingReducer';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import RoomCard from './RoomCard';
import RoomDetail from './RoomDetail';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';

interface IRoomListProps extends ReturnType<typeof mapStateToProps> {
  roomsData?: some[];
  params: HotelBookingParams;
  dispatch: Dispatch;
}

const RoomList: React.FunctionComponent<IRoomListProps> = props => {
  const { roomsData, params, dispatch, routerState } = props;
  const [currentRoom, setCurrentRoom] = React.useState<some | undefined>(undefined);
  return (
    <>
      {roomsData ? (
        <div>
          {roomsData.map(room => (
            <RoomCard
              key={room.SHRUI}
              data={room}
              onSeeMore={() => setCurrentRoom(room)}
              params={params}
              routerState={routerState}
              setParams={(params: HotelBookingParams) => dispatch(setParams(params))}
            />
          ))}
        </div>
      ) : (
        <LoadingIcon style={{ margin: '10px 0px' }} />
      )}
      {currentRoom && (
        <RoomDetail
          data={currentRoom}
          params={params}
          setParams={(params: HotelBookingParams) => dispatch(setParams(params))}
          open={!!currentRoom}
          routerState={routerState}
          onClose={() => setCurrentRoom(undefined)}
        />
      )}
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    routerState: state.router.location.state,
  };
}

export default connect(mapStateToProps)(RoomList);
