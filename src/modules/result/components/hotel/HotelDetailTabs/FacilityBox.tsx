import { Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { getFacilityIcon, getRelaxIcon, getInternetIcon } from '../../../utils';
import { Extend } from './styles';

interface IFacilityBoxProps {
  hotelData: some;
}
type Features = 'facility' | 'relax' | 'internet';
const FacilityBox: React.FunctionComponent<IFacilityBoxProps> = props => {
  const { hotelData } = props;
  const { features } = hotelData;
  const [extend, setExtend] = React.useState(false);
  const data = features.internetId
    .map((value: number, index: number) => {
      return {
        id: value,
        des: features.internet[index],
        src: features.facilitiesUrls.find((v: any) => v[`I${value}`]),
        type: 'internet' as Features,
      };
    })
    .concat(
      features.facilityIds.map((value: number, index: number) => {
        return {
          id: value,
          des: features.facilities[index],
          src: features.facilitiesUrls.find((v: any) => v[`F${value}`]),
          type: 'facility' as Features,
        };
      }),
    )
    .concat(
      features.relaxIds.map((value: number, index: number) => {
        return {
          id: value,
          des: features.relaxes[index],
          src: features.facilitiesUrls.find((v: any) => v[`R${value}`]),
          type: 'relax' as Features,
        };
      }),
    );

  return (
    <>
      {features && (
        <div>
          <Grid container spacing={3}>
            {data.slice(0, extend ? data.length : 7).map((v: some, index: number) => (
              <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }} key={index}>
                <img
                  style={{ marginRight: '12px', width: '24px', height: '24px' }}
                  src={
                    (v.type as Features) === 'relax'
                      ? v.src?.[`R${v.id}`] || getRelaxIcon(v.id)
                      : (v.type as Features) === 'facility'
                      ? v.src?.[`F${v.id}`] || getFacilityIcon(v.id)
                      : v.src?.[`I${v.id}`] || getInternetIcon(v.id)
                  }
                  alt=""
                />
                <Typography variant="caption">{v.des}</Typography>
              </Grid>
            ))}
            {data.length > 8 && (
              <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }}>
                <Extend onClick={() => setExtend(!extend)}>
                  <FormattedMessage id={extend ? 'hotel.result.info.collapse' : 'hotel.result.info.seeAll'} />
                </Extend>
              </Grid>
            )}
          </Grid>
        </div>
      )}
    </>
  );
};

export default FacilityBox;
