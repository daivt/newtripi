import { Button, Typography } from '@material-ui/core';
import Popover from '@material-ui/core/Popover';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, BLUE, GREEN, GREY, RED, PRIMARY } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import default_image from '../../../../../svg/default_image.svg';
import ic_dinner from '../../../../../svg/facilityIcon/ic_dinner.svg';
import ic_wifi from '../../../../../svg/facilityIcon/ic_wifi.svg';
import ic_traveller from '../../../../../svg/ic_traveller.svg';
import { HotelBookingParams } from '../../../../booking/redux/hotelBookingReducer';
import Link from '../../../../common/components/Link';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../../../common/constants';
import { stringifyHotelSearchState } from '../../../../search/utils';
import { ReactComponent as IconDiscount } from '../../../../../svg/ic_discount_red.svg';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;
interface IRoomCardProps {
  data: some;
  params: HotelBookingParams;
  onSeeMore(): void;
  setParams(params: HotelBookingParams): void;
  routerState?: some;
}

const RoomCard: React.FunctionComponent<IRoomCardProps> = props => {
  const { data, params, onSeeMore, setParams, routerState } = props;
  const [anchorEl, setAnchorEl] = React.useState<Element | undefined>(undefined);
  const open = Boolean(anchorEl);
  return (
    <div
      style={{
        border: `1px solid ${GREY}`,
        height: '224px',
        padding: '16px',
        marginTop: '8px',
        borderRadius: '8px',
        display: 'flex',
        background: 'white',
        cursor: 'pointer',
      }}
      onClick={onSeeMore}
    >
      <div style={{ marginRight: '16px', display: 'flex', position: 'relative' }}>
        <ProgressiveImage
          style={{ width: '262px', height: '192px', objectFit: 'cover' }}
          src={data.roomImage ? data.roomImage : data.roomImages.length > 0 ? data.roomImages[0] : default_image}
          alt=""
        />
        {data.isPackageRate && (
          <div
            style={{
              background: PRIMARY,
              width: '70px',
              height: '20px',
              borderRadius: '4px',
              right: '12px',
              top: '6px',
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Typography variant="caption" style={{ color: 'white' }}>
              <FormattedMessage id="booking.hotel.packageRate" />
            </Typography>
          </div>
        )}
        <Typography
          style={{
            position: 'absolute',
            bottom: '8px',
            right: '8px',
            alignSelf: 'flex-end',
            color: 'white',
          }}
        >
          [
          <FormattedNumber value={data.roomImages.length} />]
        </Typography>
      </div>
      <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
        <Line style={{ justifyContent: 'flex-start' }}>
          <Typography variant="subtitle2">{data.roomTitle}</Typography>
        </Line>
        <Line>
          <img src={ic_traveller} alt="" />
          <Typography variant="body2" style={{ marginLeft: '8px' }}>
            <FormattedMessage id="hotel.result.list.maxGuest" values={{ num: data.maxGuests }} />
          </Typography>
        </Line>
        {data.freeBreakfast === 1 && (
          <Line>
            <img src={ic_dinner} alt="" />
            <Typography variant="body2" style={{ marginLeft: '8px' }}>
              <FormattedMessage id="hotel.result.list.freeBreakfast" />
            </Typography>
          </Line>
        )}
        <Line>
          <img src={ic_wifi} alt="" />
          <Typography variant="body2" style={{ marginLeft: '8px' }}>
            <FormattedMessage id="hotel.result.list.freeWifi" />
          </Typography>
        </Line>
        <Line
          style={{
            padding: '5px 10px',
            height: 'max-content',
            flex: 1,
            alignItems: 'flex-end',
          }}
        >
          <Button>
            <Typography
              variant="body2"
              style={{
                color: BLUE,
              }}
            >
              <FormattedMessage id="booking.roomDetail" />
            </Typography>
          </Button>
        </Line>
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-end', width: '80%' }}>
        <div style={{ minWidth: '176px', textAlign: 'end' }}>
          {data.isPackageRate && (
            <div>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <IconDiscount />
                <Typography variant="caption" style={{ color: RED }}>
                  {data.promotionInfo.description}
                </Typography>
              </div>
              <div>
                {data?.promotionInfo?.priceBeforePromotion > 0 && (
                  <Typography variant="body2" style={{ textDecoration: 'line-through', color: GREY }}>
                    <FormattedNumber value={data.promotionInfo.priceBeforePromotion} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                )}
              </div>
            </div>
          )}
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Typography variant="subtitle1" style={{ color: RED }}>
              <FormattedNumber value={data.finalPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
            <Typography variant="subtitle1">
              <FormattedMessage id="hotel.result.list.oneNight" />
            </Typography>
          </div>
          <Typography variant="body2" style={{ color: GREEN }}>
            <FormattedMessage id="hotel.result.list.bonusPoint" values={{ num: data.bonusPoint }} />
          </Typography>
          <div style={{ paddingBottom: '8px' }}>
            <Button
              onClick={event => {
                event.stopPropagation();
                setAnchorEl(event.currentTarget);
              }}
            >
              <Typography
                variant="caption"
                style={{
                  color: data.freeCancellation === 1 ? BLUE : data.cancellationPoliciesList.length > 0 ? BLUE : RED,
                }}
              >
                <FormattedMessage
                  id={
                    data.freeCancellation === 1
                      ? 'hotel.result.list.refund'
                      : data.cancellationPoliciesList.length > 0
                      ? 'hotel.result.list.checkRefund'
                      : 'hotel.result.list.noRefund'
                  }
                />
              </Typography>
            </Button>
            <Popover
              open={open}
              anchorEl={anchorEl}
              onClose={event => {
                setAnchorEl(undefined);
              }}
              onClick={event => event.stopPropagation()}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
            >
              <div style={{ padding: '16px', background: BLACK_TEXT, borderRadius: '4px' }}>
                {data.cancellationPoliciesList && data.cancellationPoliciesList.length > 0 ? (
                  <>
                    {data.cancellationPoliciesList.map((v: string, index: number) => (
                      <Typography key={index} variant="body2" style={{ padding: '4px 0px ', color: 'white' }}>
                        -&nbsp;
                        {v}
                      </Typography>
                    ))}
                  </>
                ) : (
                  <Typography variant="body2" style={{ color: RED }}>
                    <FormattedMessage id="hotel.result.list.noRefund" />
                  </Typography>
                )}
              </div>
            </Popover>
          </div>
          <Link
            onClick={() => setParams({ ...params, shrui: data.SHRUI })}
            to={{
              pathname: `${ROUTES.hotel.hotelBookingInfo}`,
              search: `?${stringifyHotelSearchState(params, undefined, undefined, true)}&${
                HOTEL_BOOK_PARAMS_NAMES.hotelId
              }=${params.hotelId}&${HOTEL_BOOK_PARAMS_NAMES.shrui}=${encodeURIComponent(data.SHRUI)}`,
              state: { ...routerState, backableToDetail: true },
            }}
          >
            <Button
              size="large"
              style={{
                borderRadius: '16px',
                minWidth: '101px',
              }}
              variant="contained"
              color="secondary"
            >
              <FormattedMessage id="hotel.result.list.book" />
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default RoomCard;
