import { Container, Tab, Typography, withStyles } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY, PRIMARY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { scrollTo } from '../../../../../utils';
import { HotelBookingParams } from '../../../../booking/redux/hotelBookingReducer';
import { CustomTabs, StickyDiv } from '../../../../common/components/elements';
import BookingParamsBox from '../BookingParamsBox';
import HotelBasicInfo from '../HotelBasicInfoBox';
import HotelMap from '../HotelMap';
import FacilityBox from './FacilityBox';
import ReviewBox from './ReviewBox';
import RoomList from './RoomList';
import UsefulInfoBox from './UsefulInfoBox';

export const OFFSET = 145;

const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: 'normal',
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

const tabs = [
  { id: 'hotel.result.info.introduce', content: <></> },
  { id: 'hotel.result.info.facility', content: <></> },
  { id: 'hotel.result.info.review', content: <></> },
  { id: 'hotel.result.info.useful', content: <></> },
  { id: 'hotel.result.info.roomList', content: <></> },
];

const STICKY_HEIGHT = 90;

interface HotelDetailTabsProps {
  hotelData: some;
  roomsData?: some[];
  params: HotelBookingParams;
}

const HotelDetailTabs: React.FunctionComponent<HotelDetailTabsProps> = props => {
  const [currentTabIndex, setIndex] = React.useState(0);
  const [showSticky, setShowSticky] = React.useState(false);
  const rootDiv = React.useRef<HTMLDivElement>(null);
  const { hotelData, roomsData, params } = props;

  React.useEffect(() => {
    const handler = () => {
      if (rootDiv.current) {
        if (rootDiv.current.getBoundingClientRect().top < STICKY_HEIGHT) {
          setShowSticky(true);
        } else {
          setShowSticky(false);
        }
      }
    };
    window.addEventListener('scroll', handler);
    handler();
    return () => window.removeEventListener('scroll', handler);
  }, []);

  return (
    <div ref={rootDiv}>
      <StickyDiv
        style={{
          borderBottom: `1px solid ${GREY}`,
          boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2)',
          background: BACKGROUND,
          top: `${STICKY_HEIGHT - 2}px`,
          zIndex: 100,
        }}
      >
        <div
          style={{
            position: 'absolute',
            width: '100%',
            transform: 'translateY(-100%)',
            background: BACKGROUND,
            height: `${STICKY_HEIGHT}px`,
            visibility: showSticky ? 'visible' : 'hidden',
            opacity: showSticky ? 1 : 0,
            transition: 'opacity 300ms',
          }}
        >
          <Container>
            <HotelBasicInfo hotel={hotelData} roomsData={roomsData} compact />
          </Container>
        </div>
        <Container>
          <CustomTabs
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => {
              setIndex(val);
              scrollTo(tabs[val].id, OFFSET);
            }}
          >
            {tabs.map((tab, index) => (
              <CustomTab
                label={
                  <Typography
                    variant="body2"
                    style={{
                      textDecoration: 'none',
                      color: index === currentTabIndex ? PRIMARY : DARK_GREY,
                    }}
                  >
                    <FormattedMessage id={tab.id} />
                  </Typography>
                }
                key={tab.id}
              />
            ))}
          </CustomTabs>
        </Container>
      </StickyDiv>
      <Container style={{ display: 'flex' }}>
        <div style={{ flex: 1, marginBottom: '32px' }}>
          <Typography id={tabs[0].id} variant="h6" style={{ margin: '16px 0' }}>
            <FormattedMessage id={tabs[0].id} />
          </Typography>
          <div dangerouslySetInnerHTML={{ __html: hotelData.desc }} />

          <Typography id={tabs[1].id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id={tabs[1].id} />
          </Typography>
          <FacilityBox hotelData={hotelData} />

          <ReviewBox id={tabs[2].id} hotelData={hotelData} roomsData={roomsData} />

          <Typography id={tabs[3].id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id={tabs[3].id} />
          </Typography>
          <UsefulInfoBox hotelData={hotelData} />

          <Typography id={tabs[4].id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id={tabs[4].id} />
          </Typography>
          <RoomList roomsData={roomsData} params={params} />
        </div>
        <div style={{ width: '270px', margin: '32px 0 32px 32px' }}>
          <div style={{ position: 'sticky', top: 150 }}>
            <HotelMap hotelData={hotelData} roomsData={roomsData} />
            <BookingParamsBox hotelData={hotelData} roomsData={roomsData} params={params} />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default HotelDetailTabs;
