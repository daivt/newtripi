import * as React from 'react';
import { some } from '../../../../../constants';
import { scrollTo } from '../../../../../utils';
import { OFFSET } from '../HotelDetailTabs';
import MapButtonBox from '../MapButtonBox';
import MapModal from './MapModal';

interface Props {
  hotelData: some;
  roomsData?: some[];
}
const HotelMap: React.FC<Props> = props => {
  const { hotelData, roomsData } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <div style={{ marginBottom: '12px' }}>
      <MapButtonBox setOpenMap={() => setOpen(true)} />
      <MapModal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        hotelData={hotelData}
        roomsData={roomsData}
        onClick={() => {
          setOpen(false);
          scrollTo('hotel.result.info.roomList', OFFSET);
        }}
      />
    </div>
  );
};
export default HotelMap;
