import { IconButton, Modal } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import { some } from '../../../../../constants';
import InfoBox from '../HotelDetailImageBox/InfoBox';
import HotelMapBox from './HotelMapBox';

export interface Props {
  open: boolean;
  onClose(): void;
  hotelData: some;
  roomsData?: some[];
  onClick(): void;
}
const MapModal: React.FC<Props> = props => {
  const { open, onClose, hotelData, roomsData, onClick } = props;
  return (
    <Modal keepMounted={false} open={open} style={{ overflow: 'auto' }} onClose={onClose}>
      <div style={{ display: 'flex' }}>
        <IconButton
          style={{ position: 'absolute', top: 0, right: 0, zIndex: 1500, background: 'white' }}
          color="default"
          size="medium"
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <HotelMapBox hotelData={hotelData} roomsData={roomsData || []} />
        </div>
        <InfoBox hotelData={hotelData} roomsData={roomsData} onClick={onClick} />
      </div>
    </Modal>
  );
};
export default MapModal;
