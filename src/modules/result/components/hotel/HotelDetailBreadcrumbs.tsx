import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { goBack } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';

const mapStateToProps = (state: AppState) => ({ state: state.router.location.state });
interface IHotelDetailBreadcrumbsProps extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const HotelDetailBreadcrumbs: React.FunctionComponent<IHotelDetailBreadcrumbsProps> = props => {
  const backableToResult = props.state && props.state.backableToResult;
  return (
    <div style={{ paddingTop: '20px' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToResult ? 'pointer' : undefined }}
          onClick={() => (backableToResult ? props.dispatch(goBack()) : undefined)}
        >
          1. <FormattedMessage id="booking.search" />
        </Typography>
        <Typography variant="body2" color="secondary">
          2. <FormattedMessage id="result.hotelInfo" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          3. <FormattedMessage id="booking.enterInformation" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

export default connect(mapStateToProps)(HotelDetailBreadcrumbs);
