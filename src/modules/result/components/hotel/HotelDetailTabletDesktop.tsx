import { Container } from '@material-ui/core';
import * as React from 'react';
import { some, TABLET_WIDTH } from '../../../../constants';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import HotelBasicInfo from './HotelBasicInfoBox';
import HotelDetailBreadcrumbs from './HotelDetailBreadcrumbs';
import HotelDetailImageBox from './HotelDetailImageBox';
import HotelDetailTabs from './HotelDetailTabs';
import { HotelBookingParams } from '../../../booking/redux/hotelBookingReducer';
import Footer from '../../../common/components/Footer';
import { PageWrapper } from '../../../common/components/elements';

export interface Props {
  hotelData?: some;
  roomsData?: some[];
  params: HotelBookingParams;
}

const HotelDetailTabletDesktop: React.FC<Props> = props => {
  const { hotelData, roomsData, params } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <div>
        <Header light />
      </div>
      {!hotelData ? (
        <div
          style={{
            flex: 1,
            height: '200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      ) : (
        <>
          <Container>
            <HotelDetailBreadcrumbs />
            <HotelBasicInfo hotel={hotelData} roomsData={roomsData} />
          </Container>
          <HotelDetailImageBox hotelData={hotelData} roomsData={roomsData} />
          <HotelDetailTabs hotelData={hotelData} roomsData={roomsData} params={params} />
        </>
      )}
      <Footer />
    </PageWrapper>
  );
};

export default HotelDetailTabletDesktop;
