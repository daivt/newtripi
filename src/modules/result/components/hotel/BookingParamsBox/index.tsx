import { Button, Typography } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { DARK_GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IcLocation } from '../../../../../svg/ic_pin.svg';
import { ReactComponent as TravellerSvg } from '../../../../../svg/ic_traveller.svg';
import { HotelBookingParams, setParams } from '../../../../booking/redux/hotelBookingReducer';
import DateLabel from './DateLabel';
import GuestInfoLabel from './GuestInfoLabel';
import ParamsDialog from './ParamsDialog';

const Line = styled.div`
  display: flex;
  align-items: center;
  min-height: 40px;
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  hotelData: some;
  roomsData?: some[];
  params: HotelBookingParams;
}
interface State {
  open: boolean;
}
class HotelParamsBox extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { open: false };
  }

  public render() {
    const { params, hotelData, dispatch } = this.props;
    const { open } = this.state;

    return (
      <div
        style={{
          background: 'white',
          boxShadow:
            ' 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
          borderRadius: '4px',
          width: '270px',
          padding: '16px 8px',
          top: '150px',
        }}
      >
        {hotelData && (
          <>
            <div>
              <Line>
                <Typography variant="h5" style={{ marginTop: '8px', marginLeft: '8px' }}>
                  {hotelData.name}
                </Typography>
              </Line>
              <Line>
                <Rating
                  name="half-rating"
                  icon={<IconStar style={{ height: '20px' }} />}
                  value={hotelData.starNumber}
                  readOnly
                />
              </Line>
              <Line style={{ minHeight: '24px' }}>
                <IcLocation />
                &nbsp;
                <Typography variant="caption" style={{ color: DARK_GREY }}>
                  {hotelData.address}
                </Typography>
              </Line>
              <Line style={{ height: '48px', marginLeft: '8px' }}>
                <Typography variant="h6">
                  <FormattedMessage id="hotel.result.filter.bookingHotelInfo" />
                </Typography>
              </Line>
              <div style={{ width: '100%' }}>
                <DateLabel
                  checkInDate={params.checkIn}
                  checkOutDate={params.checkOut}
                  onClick={() => {
                    this.setState({ open: true });
                  }}
                  locale={this.props.locale}
                />

                <GuestInfoLabel
                  onClick={() => {
                    this.setState({ open: true });
                  }}
                  guestInfo={params.guestInfo}
                  icon={<TravellerSvg />}
                />
              </div>
              <Line style={{ justifyContent: 'center' }}>
                <Button
                  size="large"
                  style={{ width: '234px' }}
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    this.setState({ open: true });
                  }}
                >
                  <FormattedMessage id="booking.hotel.change" />
                </Button>
              </Line>
            </div>
            {open && (
              <ParamsDialog
                params={params}
                open={open}
                onClose={() => this.setState({ open: false })}
                onChange={newParams => dispatch(setParams({ ...params, ...newParams }, false))}
              />
            )}
          </>
        )}
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
  };
}

export default connect(mapStateToProps)(HotelParamsBox);
