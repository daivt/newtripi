import { Button, Dialog, Divider, IconButton, Typography } from '@material-ui/core';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
import CloseIcon from '@material-ui/icons/Close';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import styled from 'styled-components';
import { GREY } from '../../../../../colors';
import DateRangeFormControl from '../../../../common/components/DateRangeFormControl';
import { Col } from '../../../../common/components/elements';
import { GuestCountInfo } from '../../../../common/models';
import ChildrenAgeSelectSection from '../../../../search/components/GuestInfoBox/ChildrenAgeSelectSection';
import { HotelSearchParamsState } from '../../../../search/redux/hotelSearchReducer';
import { validateChildrenAgesInfo, validGuestCount } from '../../../../search/utils';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;

const CountBox = styled.div`
  height: 56px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TypeLine = styled.div`
  height: 20px;
  display: flex;
  align-items: center;
`;

export const ValueBox = styled.div`
  width: 44px;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ValueControl = styled.div`
  display: flex;
  align-items: center;
`;
export const TypeBox = styled.div`
  height: 40px;
`;

interface Props extends WrappedComponentProps {
  params: HotelSearchParamsState;
  open: boolean;
  onClose(): void;
  onChange(params: HotelSearchParamsState): void;
}
interface State {
  checkIn: Moment;
  checkOut?: Moment;
  guestCountInfo: GuestCountInfo;
  childrenAgesError: boolean;
}
class ParamsDialog extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      checkIn: props.params.checkIn,
      checkOut: props.params.checkOut,
      guestCountInfo: props.params.guestInfo,
      childrenAgesError: false,
    };
  }

  handleChangeChildrenAge = (value: any, index: number) => {
    const { guestCountInfo } = this.state;
    const newAgesArray = [...guestCountInfo.childrenAges];
    newAgesArray[index] = value;
    this.setState({
      guestCountInfo: {
        ...guestCountInfo,
        childrenAges: newAgesArray,
      },
    });
  };

  render() {
    const { open, onClose, onChange, params, intl } = this.props;
    const { guestCountInfo, checkIn, checkOut, childrenAgesError } = this.state;
    return (
      <Dialog
        open={open}
        PaperProps={{
          style: { padding: '16px 32px', overflow: 'initial', minWidth: 570 },
        }}
        scroll="body"
        maxWidth="lg"
      >
        <Typography variant="h6">
          <FormattedMessage id="booking.hotel.changeParams" />
        </Typography>
        <IconButton color="default" size="small" onClick={onClose} style={{ position: 'absolute', top: 8, right: 8 }}>
          <CloseIcon />
        </IconButton>
        <Col>
          <DateRangeFormControl
            label={intl.formatMessage({ id: 'search.stayDuration' })}
            placeholder={intl.formatMessage({ id: 'search.stayDuration' })}
            style={{ marginRight: 0 }}
            optional
            startDate={params.checkIn}
            endDate={params.checkOut}
            onChange={(start?: Moment, end?: Moment) => {
              if (start) {
                this.setState({
                  checkIn: start,
                  checkOut: end,
                });
              }
            }}
            numberOfMonths={2}
            startAdornment
            isOutsideRange={date =>
              moment()
                .startOf('day')
                .isAfter(date)
            }
            direction="center"
          />
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              height: '48px',
            }}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.hotel.guestInfo" />
            </Typography>
          </div>
          <CountBox>
            <TypeBox>
              <TypeLine>
                <Typography variant="body2">
                  <FormattedMessage id="search.guestInfo.adult" />
                </Typography>
              </TypeLine>

              <TypeLine style={{ color: GREY }}>
                <Typography variant="body2">
                  <FormattedMessage id="search.guestInfo.adultDef" />
                </Typography>
              </TypeLine>
            </TypeBox>
            <ValueControl>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(guestCountInfo.roomCount, guestCountInfo.adultCount - 1, guestCountInfo.childCount)
                }
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      adultCount: guestCountInfo.adultCount - 1,
                    },
                  })
                }
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '16px',
                    color: !validGuestCount(
                      guestCountInfo.roomCount,
                      guestCountInfo.adultCount - 1,
                      guestCountInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
              <ValueBox>{guestCountInfo.adultCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(guestCountInfo.roomCount, guestCountInfo.adultCount + 1, guestCountInfo.childCount)
                }
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      adultCount: guestCountInfo.adultCount + 1,
                    },
                  })
                }
              >
                <AddCircleOutline
                  style={{
                    fontSize: '16px',
                    color: !validGuestCount(
                      guestCountInfo.roomCount,
                      guestCountInfo.adultCount + 1,
                      guestCountInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
          <CountBox>
            <TypeBox>
              <TypeLine>
                <Typography variant="body2">
                  <FormattedMessage id="search.guestInfo.children" />
                </Typography>
              </TypeLine>

              <TypeLine style={{ color: GREY }}>
                <Typography variant="body2">
                  <FormattedMessage id="search.guestInfo.childrenDef" />
                </Typography>
              </TypeLine>
            </TypeBox>
            <ValueControl>
              <IconButton
                size="small"
                disabled={guestCountInfo.childCount === 0}
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      childCount: guestCountInfo.childCount - 1,
                      childrenAges: guestCountInfo.childrenAges.slice(0, guestCountInfo.childrenAges.length - 1),
                    },
                  })
                }
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '16px',
                    color: guestCountInfo.childCount === 0 ? GREY : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
              <ValueBox>{guestCountInfo.childCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(guestCountInfo.roomCount, guestCountInfo.adultCount, guestCountInfo.childCount + 1)
                }
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      childCount: guestCountInfo.childCount + 1,
                      childrenAges: guestCountInfo.childrenAges.concat(-1),
                    },
                  })
                }
              >
                <AddCircleOutline
                  style={{
                    fontSize: '16px',
                    color: !validGuestCount(
                      guestCountInfo.roomCount,
                      guestCountInfo.adultCount,
                      guestCountInfo.childCount + 1,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
          <Divider style={{ marginTop: '16px' }} />
          <ChildrenAgeSelectSection
            childrenAges={guestCountInfo.childrenAges}
            onChange={this.handleChangeChildrenAge}
            error={childrenAgesError}
          />
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              height: '48px',
              marginTop: '16px',
            }}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.hotel.roomInfo" />
            </Typography>
          </div>
          <CountBox>
            <TypeLine>
              <Typography variant="body2">
                <FormattedMessage id="search.guestInfo.room" />
              </Typography>
            </TypeLine>
            <ValueControl>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(guestCountInfo.roomCount - 1, guestCountInfo.adultCount, guestCountInfo.childCount)
                }
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      roomCount: guestCountInfo.roomCount - 1,
                    },
                  })
                }
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '16px',
                    color: !validGuestCount(
                      guestCountInfo.roomCount - 1,
                      guestCountInfo.adultCount,
                      guestCountInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
              <ValueBox>{guestCountInfo.roomCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(guestCountInfo.roomCount + 1, guestCountInfo.adultCount, guestCountInfo.childCount)
                }
                onClick={() =>
                  this.setState({
                    guestCountInfo: {
                      ...guestCountInfo,
                      roomCount: guestCountInfo.roomCount + 1,
                    },
                  })
                }
              >
                <AddCircleOutline
                  style={{
                    fontSize: '16px',
                    color: !validGuestCount(
                      guestCountInfo.roomCount + 1,
                      guestCountInfo.adultCount,
                      guestCountInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="primary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
        </Col>
        <Line
          style={{
            height: '88px',
            bottom: 0,
            position: 'relative',
            background: 'white',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Button
            variant="outlined"
            size="large"
            style={{
              width: '168px',
              marginRight: '16px',
            }}
            onClick={onClose}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.hotel.back" />
            </Typography>
          </Button>
          <Button
            color="secondary"
            variant="contained"
            size="large"
            disableElevation
            style={{ width: '168px', marginLeft: '16px' }}
            onClick={() => {
              const errors = validateChildrenAgesInfo(guestCountInfo.childrenAges);
              if (errors) {
                this.setState({ childrenAgesError: errors });
              } else {
                onChange({
                  ...params,
                  checkIn,
                  checkOut: checkOut || checkIn.clone().add(1, 'days'),
                  guestInfo: guestCountInfo,
                });
                onClose();
              }
            }}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.hotel.change" />
            </Typography>
          </Button>
        </Line>
      </Dialog>
    );
  }
}

export default injectIntl(ParamsDialog);
