import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../../colors';
import { GuestCountInfo } from '../../../../common/models';
import { getInputStr } from '../../../../search/components/GuestInfoBox';

const InputContainer = styled.div`
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  background-color: white;
  color: ${BLACK_TEXT};
  cursor: pointer;
`;

interface Props extends WrappedComponentProps {
  icon: React.ReactNode;
  guestInfo: GuestCountInfo;
  onClick(): void;
}

const DateLabel: React.FunctionComponent<Props> = props => {
  const { icon, guestInfo, intl } = props;
  return (
    <div
      style={{
        flex: 1,
        height: '86px',
        backgroundColor: 'transparent',
      }}
    >
      <Typography
        variant="body2"
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '30px',
        }}
      >
        <FormattedMessage id="search.guestInfo" />
      </Typography>
      <InputContainer onClick={props.onClick}>
        <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>
        <div
          style={{
            display: 'flex',
            height: '24px',
            margin: '7px 1px',
            paddingRight: '5px',
            flex: 1,
            overflow: 'hidden',
          }}
        >
          <Typography noWrap variant="body2" style={{ overflow: 'hidden' }}>
            {getInputStr(guestInfo, intl)}
          </Typography>
        </div>
      </InputContainer>
    </div>
  );
};

export default injectIntl(DateLabel);
