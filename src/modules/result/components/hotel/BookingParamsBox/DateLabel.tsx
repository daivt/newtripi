import { Typography } from '@material-ui/core';
import { Moment } from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../../colors';
import { ReactComponent as CalendarSvg } from '../../../../../svg/calendar.svg';

const InputContainer = styled.div`
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  background-color: white;
  color: ${BLACK_TEXT};
  cursor: pointer;
`;
interface Props {
  checkInDate: Moment;
  checkOutDate: Moment;
  onClick(): void;
  locale: string;
}

interface State {}

const DateLabel: React.FunctionComponent<Props> = props => {
  const { checkInDate, checkOutDate } = props;
  return (
    <div
      style={{
        flex: 1,
        height: '86px',
        backgroundColor: 'transparent',
      }}
    >
      <Typography
        variant="body2"
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '30px',
        }}
      >
        <FormattedMessage id="search.stayDuration" />
      </Typography>
      <InputContainer onClick={props.onClick}>
        <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
          <CalendarSvg />
        </div>
        <div
          style={{
            display: 'flex',
            height: '24px',
            margin: '7px 1px',
            paddingRight: '5px',
            flex: 1,
          }}
        >
          <Typography
            variant="body2"
            style={{
              cursor: 'pointer',
            }}
          >
            {checkInDate.locale(props.locale).format('L')}
          </Typography>
          <>
            &nbsp;&nbsp;- &nbsp;&nbsp;
            <Typography
              variant="body2"
              style={{
                cursor: 'pointer',
              }}
            >
              {checkOutDate ? checkOutDate.locale(props.locale).format('L') : <FormattedMessage id="notChosen" />}
            </Typography>
          </>
        </div>
      </InputContainer>
    </div>
  );
};

export default DateLabel;
