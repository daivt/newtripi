import { Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { AppState } from '../../../../redux/reducers';
import Skeleton from '@material-ui/lab/Skeleton';

const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
function mapStateToProps(state: AppState) {
  return {
    result: state.result.hotel,
    locale: state.intl.locale,
  };
}

interface HotelResultBoxHeaderProps extends ReturnType<typeof mapStateToProps> {
  style?: React.CSSProperties;
}

const HotelResultBoxHeader: React.FunctionComponent<HotelResultBoxHeaderProps> = props => {
  const { result, style } = props;
  const search = result.lastParams && result.lastParams.searchParams;

  if (!search) {
    return (
      <div style={style}>
        <Skeleton width="300px" height="25px" />
        <Skeleton width="350px" />
      </div>
    );
  }

  return (
    <div style={style}>
      <Line>
        <Typography variant="h5">
          {search.location.name || search.location.provinceName}:&nbsp;
          <FormattedMessage
            id="result.header.found"
            values={{ num: result.data ? result.data.totalResults : 0 }}
          />
        </Typography>
      </Line>
      <Line style={{ maxWidth: '500px' }}>
        <Typography variant="body2" style={{ paddingRight: '8px' }}>
          {moment(search.checkIn.valueOf()).format('L')}&nbsp;-&nbsp;
          {moment(search.checkOut.valueOf()).format('L')}
        </Typography>
        <Typography variant="body2" style={{ padding: '0px 8px' }}>
          <FormattedMessage
            id="result.header.night"
            values={{ num: moment(search.checkOut).diff(moment(search.checkIn), 'days') }}
          />
        </Typography>
        <Typography variant="body2" style={{ padding: '0px 8px' }}>
          <FormattedMessage
            id="result.header.adult"
            values={{ num: search.guestInfo.adultCount }}
          />
          {search.guestInfo.childCount > 0 && (
            <>
              ,&nbsp;
              <FormattedMessage
                id="result.header.child"
                values={{ num: search.guestInfo.childCount }}
              />
            </>
          )}
        </Typography>

        <Typography variant="body2" style={{ paddingLeft: '8px' }}>
          <FormattedMessage id="result.header.room" values={{ num: search.guestInfo.roomCount }} />
        </Typography>
      </Line>
    </div>
  );
};

export default connect(mapStateToProps)(HotelResultBoxHeader);
