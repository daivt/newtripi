import { MenuItem, Select, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../redux/reducers';
import { BootstrapInput } from '../../../common/components/elements';
import { HOTEL_SORT_BY } from '../../constants';
import { setSortBy } from '../../redux/hotelResultReducer';

export const SORT_BY = [
  {
    id: 0,
    text: 'result.sortBy.default',
    value: HOTEL_SORT_BY.default,
  },
  {
    id: 1,
    text: 'result.sortBy.priceDesc',
    value: HOTEL_SORT_BY.expensive,
  },
  {
    id: 2,
    text: 'result.sortBy.priceAsc',
    value: HOTEL_SORT_BY.cheap,
  },
];

function mapStateToProps(state: AppState) {
  return {
    sortBy: state.result.hotel.sortBy,
  };
}

interface HotelResultSortBoxProps extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const HotelResultSortBox: React.FunctionComponent<HotelResultSortBoxProps> = props => {
  const { sortBy, dispatch } = props;

  return (
    <>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '210px', display: 'flex', flexDirection: 'column' }}>
          <div>
            <Typography variant="body2">
              <FormattedMessage id="result.sortBy.sortBy" />
            </Typography>
          </div>
          <div
            style={{
              backgroundColor: '#fff',
              marginTop: '8px',
            }}
          >
            <Select
              value={sortBy}
              onChange={event => dispatch(setSortBy(event.target.value as string))}
              input={<BootstrapInput />}
              fullWidth
            >
              {SORT_BY.map(item => (
                <MenuItem key={item.id} value={item.value}>
                  <Typography variant="body2">
                    <FormattedMessage id={item.text} />
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(mapStateToProps)(HotelResultSortBox);
