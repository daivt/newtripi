import { Button, Collapse, Typography, ButtonBase, Tooltip } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import styled from 'styled-components';
import { withStyles } from '@material-ui/styles';
import { BLUE, GREEN, GREY, PRIMARY, ORANGE } from '../../../../colors';
import { SLASH_DATE_FORMAT, some } from '../../../../constants';
import { MUI_THEME } from '../../../../setupTheme';
import { durationMillisecondToHour } from '../../../common/utils';
import { getStops } from '../../utils';
import { ReactComponent as IconCoin } from '../../../../svg/coin.svg';
import { ReactComponent as IconClock } from '../../../../svg/ic_flight_ticket_clock.svg';
import { ReactComponent as IconSeat } from '../../../../svg/ic_flight_ticket_seat.svg';
import { FlightDirectionDetail } from './FlightTicketDetail';
import { BoxInfoFlight, BoxLogoAirline } from './FlightTicketItem.style';

const ParnershipBadge = styled.div`
  display: flex;
  align-items: center;
  max-width: fit-content;
  background: #cbedfd;
  color: ${BLUE};
  overflow: hidden;
  margin: 0 0 8px -16px;
  padding: 0 8px;
`;

const StyledList = styled.ul`
  margin: 0;
  padding: 0;
`;

const StyledListItem = styled.li`
  margin: 4px 0 4px 12px;
`;

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    fontSize: '14px',
    padding: '16px',
  },
}))(Tooltip);

interface Props extends WrappedComponentProps {
  ticket: some;
  airlines?: some[];
  onClick(): void;
  buttonMsgId?: string;
  getAirlineInfo(id: number): some;
  getTicketClass(classCode: string): some | undefined;
  getPrice(ticket: some): React.ReactNode;
  priceRemark?: JSX.Element;
}

const FlightTicketItem: React.FC<Props> = props => {
  const { ticket, getAirlineInfo, getTicketClass, intl, getPrice, priceRemark, onClick, buttonMsgId } = props;
  const [showInfo, setShowInfo] = React.useState(false);

  const renderPartnershipPolicies = React.useCallback((data?: some) => {
    if (!data) {
      return '';
    }
    return (
      <StyledList>
        {data?.[0]?.description}
        {data?.map((val: some, index: number) => {
          if (index === 0) return null;
          if (val?.code === 'CHANGE_ROUTE') return null;
          if (val?.code === 'VOID_TICKET') return null;
          return <StyledListItem key={index}>{val.description}</StyledListItem>;
        })}
      </StyledList>
    );
  }, []);

  if (!ticket || !ticket.outbound) return <div />;

  const flightTime = ticket.outbound.duration;
  const transitTickets = ticket.outbound.transitTickets ? ticket.outbound.transitTickets : null;
  const { bonusPoint } = ticket.outbound.ticketdetail;

  const arrivalDay = moment(ticket.outbound.arrivalDayStr, SLASH_DATE_FORMAT);
  const departureDay = moment(ticket.outbound.departureDayStr, SLASH_DATE_FORMAT);
  const diffDay = arrivalDay.diff(departureDay, 'days');

  const ticketClass = getTicketClass(ticket.outbound.ticketdetail.ticketClassCode);

  const price = getPrice(ticket);
  const discountPrice = ticket.outbound.ticketdetail.discountAdultUnit;
  const totalPriceAfterDiscount = ticket.outbound.ticketdetail.priceAdultUnit + discountPrice;

  return (
    <div
      style={{
        marginTop: '13px',
        border: `1px solid ${GREY}`,
        boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
        borderRadius: '4px',
        background: 'white',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexFlow: 'column',
          justifyContent: 'space-between',
        }}
      >
        <div style={{ margin: '0px 16px 8px 16px', display: 'flex', position: 'relative' }}>
          <div style={{ display: 'flex', flexFlow: 'column' }}>
            {ticket.outbound.operatingAirlineId && ticket.outbound.operatingAirlineId !== ticket.outbound.aid && (
              <HtmlTooltip title={renderPartnershipPolicies(ticket.outbound.ticketdetail.polices)} arrow>
                <ParnershipBadge>
                  <span>
                    <FormattedMessage id="result.partnershipAction" />
                    &nbsp;
                  </span>
                  <span>
                    {getAirlineInfo(ticket.outbound.operatingAirlineId).name}
                    &nbsp;
                  </span>
                  <img
                    alt="Partnership logo"
                    style={{ maxHeight: '28px' }}
                    src={getAirlineInfo(ticket.outbound.operatingAirlineId).logo}
                  />
                </ParnershipBadge>
              </HtmlTooltip>
            )}
            <div style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
              <BoxLogoAirline>
                <img alt="" style={{ height: '32px' }} src={getAirlineInfo(ticket.outbound.aid).logo} />
                <span>{getAirlineInfo(ticket.outbound.aid).name}</span>
              </BoxLogoAirline>

              <BoxInfoFlight>
                <div
                  style={{
                    display: 'flex',
                    flexFlow: 'column',
                    textAlign: 'right',
                    position: 'relative',
                  }}
                >
                  <Typography variant="subtitle1">
                    <span>{ticket.outbound.departureTimeStr}</span>
                  </Typography>
                  <Typography
                    variant="body2"
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      right: 0,
                      transform: 'translateY(100%)',
                    }}
                  >
                    <span>{ticket.outbound.departureAirport}</span>
                  </Typography>
                </div>
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                    alignItems: 'center',
                    height: '35px',
                    padding: '0 12px',
                  }}
                >
                  {getStops(ticket).map(item => (
                    <React.Fragment key={item.code}>
                      <div
                        style={{
                          flex: 1,
                        }}
                      >
                        <div
                          style={{
                            borderBottom: `2px solid ${GREY}`,
                          }}
                        />
                      </div>
                      <div
                        style={{
                          position: 'relative',
                          display: 'flex',
                          alignItems: 'center',
                          flexFlow: 'column',
                        }}
                      >
                        <div style={{ alignItems: 'center', display: 'flex', height: '35px' }}>
                          <div
                            style={{
                              border: `2px solid ${GREY}`,
                              borderRadius: '50%',
                              backgroundColor: '#FFFFFF',
                              height: item.technical ? '7px' : '10px',
                              width: item.technical ? '7px' : '10px',
                              margin: '0 1px',
                            }}
                          />
                        </div>
                        <div
                          style={{
                            position: 'absolute',
                            bottom: 0,
                            transform: 'translateY(100%)',
                            color: '#757575',
                          }}
                        >
                          <Typography variant={item.technical ? 'caption' : 'body2'}>{item.code}</Typography>
                        </div>
                      </div>
                    </React.Fragment>
                  ))}

                  <div
                    style={{
                      flex: 1,
                    }}
                  >
                    <div
                      style={{
                        borderBottom: `2px solid ${GREY}`,
                      }}
                    />
                  </div>
                </div>
                <div style={{ display: 'flex', flexFlow: 'column', position: 'relative' }}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Typography variant="subtitle1">
                      <span>{ticket.outbound.arrivalTimeStr}</span>
                    </Typography>
                    <Typography
                      variant="caption"
                      style={{
                        width: '32px',
                        paddingLeft: '9px',
                      }}
                    >
                      <span>{diffDay ? `(+${diffDay}d)` : ''}</span>
                    </Typography>
                  </div>
                  <Typography
                    variant="body2"
                    style={{ position: 'absolute', bottom: 0, transform: 'translateY(100%)' }}
                  >
                    <span>{ticket.outbound.arrivalAirport}</span>
                  </Typography>
                </div>
              </BoxInfoFlight>
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <div
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                  borderBottom: showInfo ? '1px solid #BDBDBD' : 'none',
                  right: 0,
                  left: 0,
                }}
              />
              <ButtonBase
                onClick={() => setShowInfo(!showInfo)}
                style={{
                  color: BLUE,
                  cursor: 'pointer',
                  padding: '18px 0 12px',
                  position: 'relative',
                }}
              >
                <FormattedMessage id="result.flightInfo" />
                <span
                  style={{
                    display: 'inline-block',
                    marginLeft: '6px',
                    fontSize: '10px',
                    transition: 'all 300ms',
                    marginRight: '10px',
                    transform: showInfo ? 'rotate(0deg)' : 'rotate(180deg)',
                  }}
                >
                  &#9650;
                </span>

                <div
                  style={{
                    position: 'absolute',
                    visibility: showInfo ? 'visible' : 'hidden',
                    bottom: 0,
                    width: '100%',
                    borderBottom: `4px solid ${PRIMARY}`,
                  }}
                />
              </ButtonBase>
            </div>
          </div>

          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              flex: 1,
              paddingTop: '8px',
            }}
          >
            <div
              style={{
                width: '400px',
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  paddingTop: '6px',
                  minWidth: '200px',
                }}
              >
                {/* <div style={{ display: 'flex', padding: '4px 0' }}>
                    <img alt="" style={{ paddingRight: '8px' }} src={IconAirPlane} />
                    <Typography
                      variant="body2"
                      style={{
                        color: LIGHT_GREY,
                      }}
                    >
                      {transitTickets
                        ? transitTickets[0].flightNumber
                        : ticket.outbound.flightNumber}
                    </Typography>
                  </div> */}
                <div style={{ display: 'flex', padding: '4px 0', alignItems: 'center' }}>
                  <IconClock style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                  <Typography variant="body2" color="textSecondary">
                    {durationMillisecondToHour(flightTime)}
                  </Typography>
                </div>
                <div
                  style={{
                    display: 'flex',
                    padding: '4px 0',
                    alignItems: 'center',
                  }}
                >
                  <IconSeat style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                  <Typography variant="body2" color="textSecondary">
                    {ticketClass ? (intl.locale.startsWith('vi') ? ticketClass.v_name : ticketClass.i_name) : ''}
                  </Typography>
                  <div
                    style={{
                      height: '18px',
                      marginLeft: '5px',
                      minWidth: '18px',
                      overflow: 'hidden',
                      alignItems: 'center',
                      borderRadius: '2px',
                      padding: '0 5px',
                      display: 'flex',
                      justifyContent: 'center',
                      color: 'white',
                      backgroundColor: PRIMARY,
                      fontWeight: 500,
                      fontSize: MUI_THEME.typography.body2.fontSize,
                    }}
                  >
                    {transitTickets
                      ? transitTickets[0].detailTicketClass
                      : ticket.outbound.ticketdetail.detailTicketClass}
                  </div>
                </div>
                {bonusPoint > 0 && (
                  <div style={{ display: 'flex', padding: '4px 0', alignItems: 'center' }}>
                    <IconCoin style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                    <Typography variant="body2" style={{ color: GREEN }}>
                      <FormattedNumber value={bonusPoint} />
                      &nbsp;
                      <FormattedMessage id="result.bonusPoint" />
                    </Typography>
                  </div>
                )}
              </div>
              <div
                style={{
                  height: '95%',
                  borderLeft: `1px solid ${GREY}`,
                }}
              />

              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  paddingTop: '6px',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  paddingBottom: '16px',
                  minWidth: '170px',
                }}
              >
                {!!discountPrice && (
                  <Typography variant="body2" color="textSecondary" style={{ textDecoration: 'line-through' }}>
                    <FormattedNumber value={totalPriceAfterDiscount} />
                    &nbsp;VNĐ
                  </Typography>
                )}

                <Typography variant="h5" style={{ paddingBottom: '8px', color: ORANGE }}>
                  {price || '---'}
                  &nbsp;
                  <span style={{ fontSize: '16px', fontWeight: 'normal' }}>VNĐ</span>
                </Typography>
                <div style={{ marginTop: '-12px', marginBottom: '5px' }}>{priceRemark}</div>
                <Button
                  style={{ padding: '1px 0', width: '160px', height: '30px', boxShadow: 'none' }}
                  color="secondary"
                  variant="contained"
                  size="small"
                  onClick={onClick}
                  disabled={!price}
                >
                  <FormattedMessage id={buttonMsgId || 'result.bookTicket'} />
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Collapse collapsedHeight="0px" in={showInfo}>
        <FlightDirectionDetail ticket={ticket} getAirlineInfo={getAirlineInfo} />
      </Collapse>
    </div>
  );
};

export default injectIntl(FlightTicketItem);
