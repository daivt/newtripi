import { Button, Divider, Theme, Typography, withStyles } from '@material-ui/core';
import { goBack, push } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { matchPath } from 'react-router';
import { Dispatch } from 'redux';
import styled, { keyframes } from 'styled-components';
import { BLUE, GREY, LIGHT_BLUE } from '../../../../colors';
import { ROUTES, SLASH_DATE_FORMAT, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setInbound, setOutbound } from '../../../booking/redux/flightBookingReducer';
import { subtitle3Styles } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { durationMillisecondToHour, makeGetAirlineInfo, makeGetTicketClass } from '../../../common/utils';
import { DEFAULT_FILTER, FLIGHT_SORT_BY } from '../../constants';
import { FlightFilterState, setFilter } from '../../redux/flightResultReducer';
import {
  filterAndSort,
  flightCheapestCmp,
  flightCheapestRoundtripCmp,
  flightFastestCmp,
  flightTimeTakeOffCmp,
} from '../../utils';
import FlightResultHeader from './FlightResultHeader';
import FlightSortBox from './FlightSortBox';
import FlightTicketItem from './FlightTicketItem';
import { BoxLogoAirline } from './FlightTicketItem.style';

const STEP = 10;
const WIDTH = 190;
const MARGIN = 10;

const slideLeft = keyframes`
  from {
    transform: translateX(0);
  }

  to {
    transform: translateX(-${WIDTH + MARGIN}px);
  }
`;

const SlideLeft = styled.div`
  animation-name: ${slideLeft};
  animation-duration: 0.4s;
`;

const WhiteOutlineButton = withStyles((theme: Theme) => ({
  root: {
    color: 'white',
    borderColor: 'white',
    fontWeight: 400,
    fontSize: theme.typography.body2.fontSize,
  },
}))(Button);

const PairDiv = (props: { firstDiv: React.ReactNode; secondDiv: React.ReactNode; fade?: boolean }) => {
  return (
    <div
      style={{
        flex: 1,
        background: LIGHT_BLUE,
        color: 'white',
        display: 'flex',
        borderRadius: '4px',
        opacity: props.fade ? 0.5 : 1,
      }}
    >
      <div style={{ flex: 1 }}>{props.firstDiv}</div>
      <div
        style={{
          width: 0,
          borderLeft: '1px dashed white',
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <div
          style={{
            width: '24px',
            height: '24px',
            borderRadius: '50%',
            background: '#fafafc',
            transform: 'translateY(-50%)',
          }}
        />
        <div
          style={{
            width: '24px',
            height: '24px',
            borderRadius: '50%',
            background: '#fafafc',
            transform: 'translateY(50%)',
          }}
        />
      </div>
      <div
        style={{
          width: '230px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        {props.secondDiv}
      </div>
    </div>
  );
};

const transition: React.CSSProperties = { transition: 'all 0.4s' };

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  filterButton?: React.ReactNode;
}

interface State {
  maxDisplayOutbound: number;
  maxDisplayInbound: number;
  returnView: boolean;
}

class FlightResultBoxTwoWay extends React.PureComponent<Props, State> {
  state = {
    maxDisplayInbound: 10,
    maxDisplayOutbound: 10,
    returnView: !!matchPath(this.props.location.pathname, ROUTES.flight.flightResultInbound) || false,
  };

  public render(): JSX.Element {
    const {
      booking,
      data,
      filterParams,
      dispatch,
      getAirlineInfo,
      searchCompleted,
      getTicketClass,
      sortBy,
      filterButton,
      searchRequest,
    } = this.props;
    const { maxDisplayInbound, maxDisplayOutbound, returnView } = this.state;

    if (!data || !data.outbound) {
      return (
        <div
          style={{
            height: '200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      );
    }
    let outbound: some | undefined;
    let matchingGroupId: some;
    let seatClass;
    if (booking.outbound.ticket) {
      matchingGroupId = booking.outbound.ticket.outbound.matchingGroupId;
      outbound = booking.outbound.ticket.outbound as some;
      seatClass = getTicketClass(outbound.ticketdetail.ticketClassCode);
    }
    const matchedInboundTickets = data.inbound.tickets.filter(
      (ticket: some) => ticket.outbound.matchingGroupId === matchingGroupId,
    );
    const filteredInboundTickets = filterAndSort(
      matchedInboundTickets,
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
    const inboundTickets = filteredInboundTickets.filter((_: some, i: number) => i < maxDisplayInbound);
    const filteredOutboundTickets = filterAndSort(
      data.outbound.tickets,
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestRoundtripCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
    const outboundTickets = filteredOutboundTickets.filter((_: some, i: number) => i < maxDisplayOutbound);

    return (
      <div style={{ width: '100%', position: 'relative' }}>
        <div style={{ display: 'flex' }}>
          {filterButton}
          <FlightResultHeader
            count={booking.outbound.ticket ? filteredInboundTickets.length : filteredOutboundTickets.length}
          />
          <FlightSortBox sortBy={sortBy} dispatch={dispatch} />
        </div>
        <div style={{ marginTop: '20px', overflow: 'hidden', position: 'relative', height: '110px', display: 'flex' }}>
          <SlideLeft
            style={{
              flex: 1,
              display: 'flex',
              transform: booking.outbound.ticket ? `translateX(-${MARGIN + WIDTH}px)` : 'translateX(0)',
              height: '100%',
              ...transition,
              animation: !returnView ? 'none' : undefined,
            }}
          >
            <div
              style={{
                marginRight: MARGIN,
                width: WIDTH,
                borderRadius: '4px',
                background: LIGHT_BLUE,
                color: 'white',
                padding: '8px 12px',
              }}
            >
              <Typography style={{ marginBottom: '8px', fontWeight: 500 }}>
                <FormattedMessage id="outbound" />
              </Typography>
              <Divider style={{ background: 'white' }} />
              <Typography variant="body2" style={{ marginTop: '8px' }}>
                <FormattedMessage id="result.pleaseChooseOutboundTicket" />
              </Typography>
            </div>
            {!outbound ? (
              <PairDiv
                fade
                firstDiv={
                  <div
                    style={{
                      marginRight: '10px',
                      width: '100%',
                      borderRadius: '4px',
                      background: LIGHT_BLUE,
                      color: 'white',
                      padding: '8px 12px',
                    }}
                  >
                    <Typography style={{ marginBottom: '8px', fontWeight: 500 }}>
                      <FormattedMessage id="outbound" />
                    </Typography>
                    <Divider style={{ background: 'white' }} />
                  </div>
                }
                secondDiv={<div />}
              />
            ) : (
              <PairDiv
                firstDiv={
                  <div
                    style={{
                      marginRight: '10px',
                      width: '100%',
                      borderRadius: '4px',
                      background: LIGHT_BLUE,
                      color: 'white',
                      padding: '8px 12px',
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginBottom: '8px',
                      }}
                    >
                      <Typography style={{ fontWeight: 500, display: 'flex' }}>
                        <FormattedMessage id="outbound" />
                        &nbsp;-&nbsp;
                        {outbound ? moment(outbound.departureDayStr, SLASH_DATE_FORMAT).format('L') : ''}
                      </Typography>
                      <div style={{ display: 'flex' }}>
                        <BoxLogoAirline style={{ width: 'auto', paddingTop: '4px', paddingRight: '4px' }}>
                          <img
                            alt=""
                            style={{ height: '14px' }}
                            src={outbound ? getAirlineInfo(outbound.aid).logo : ''}
                          />
                        </BoxLogoAirline>
                        {outbound ? getAirlineInfo(outbound.aid).name : ''}
                      </div>
                    </div>
                    <Divider style={{ background: 'white' }} />
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        marginTop: '8px',
                        justifyContent: 'space-between',
                        height: '50px',
                      }}
                    >
                      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div style={{ display: 'flex' }}>
                          <Typography variant="body2" style={{ width: '52px' }}>
                            {outbound?.departureTimeStr}
                          </Typography>
                          <Typography variant="body2">
                            {outbound?.departureCity}
                            &nbsp;&#40;
                            {outbound?.departureAirport}
                            &#41;
                          </Typography>
                        </div>
                        <div style={{ display: 'flex' }}>
                          <Typography variant="body2">
                            <FormattedMessage id="search.travellerInfo.seatClass" />
                            :&nbsp;
                            {seatClass && (this.props.locale.startsWith('vi') ? seatClass.v_name : seatClass.e_name)}
                          </Typography>
                        </div>
                      </div>
                      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div style={{ display: 'flex' }}>
                          <Typography variant="body2" style={{ width: '52px' }}>
                            {outbound?.arrivalTimeStr}
                          </Typography>
                          <Typography variant="body2">
                            {outbound?.arrivalCity}
                            &nbsp;&#40;
                            {outbound?.arrivalAirport}
                            &#41;
                          </Typography>
                        </div>
                        <div style={{ display: 'flex' }}>
                          <FormattedMessage id="search.flyDuration" />
                          :&nbsp;
                          {outbound ? durationMillisecondToHour(outbound.duration) : ''}
                        </div>
                      </div>
                    </div>
                  </div>
                }
                secondDiv={
                  <div>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: '12px 0px',
                      }}
                    >
                      <div>
                        <FormattedMessage id="result.oneWayPrice" />
                        &nbsp;
                      </div>
                      <span style={subtitle3Styles}>
                        {booking.outbound.ticket?.outbound.ticketdetail.priceAdultUnit ? (
                          <FormattedNumber value={booking.outbound.ticket.outbound.ticketdetail.priceAdultUnit} />
                        ) : (
                          '---'
                        )}
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </span>
                    </div>
                    <div>
                      <WhiteOutlineButton
                        variant="outlined"
                        size="small"
                        onClick={() => {
                          const filter: FlightFilterState = {
                            ...DEFAULT_FILTER,
                            ticketClass: searchRequest ? searchRequest.seatClass : [],
                            price: [0, data.filters.maxPrice],
                          };
                          dispatch(setFilter(filter));
                          dispatch(goBack());
                        }}
                      >
                        <FormattedMessage id="result.changeFlight" />
                      </WhiteOutlineButton>
                    </div>
                  </div>
                }
              />
            )}
          </SlideLeft>
          <SlideLeft
            style={{
              width: WIDTH,
              border: `1px solid ${GREY}`,
              borderRadius: '4px',
              marginLeft: MARGIN,
              padding: '8px 12px',
              position: 'absolute',
              right: `-${WIDTH + MARGIN}px`,
              transform: booking.outbound.ticket ? `translateX(-${MARGIN + WIDTH}px)` : 'translateX(0)',
              ...transition,
              animation: !returnView ? 'none' : undefined,
            }}
          >
            <Typography style={{ marginBottom: '8px', fontWeight: 500 }}>
              <FormattedMessage id="inbound" />
            </Typography>
            <Divider />
            <Typography variant="body2" style={{ marginTop: '8px' }}>
              <FormattedMessage id="result.pleaseChooseInboundTicket" />
            </Typography>
          </SlideLeft>
        </div>
        {booking.outbound.ticket ? (
          <>
            {inboundTickets.map((ticket: some) => (
              <FlightTicketItem
                getAirlineInfo={getAirlineInfo}
                getTicketClass={getTicketClass}
                key={ticket.tid}
                ticket={ticket}
                getPrice={ticketObj =>
                  ticketObj.outbound.ticketdetail.priceAdultUnit !== undefined ? (
                    <FormattedNumber value={ticketObj.outbound.ticketdetail.priceAdultUnit} />
                  ) : (
                    undefined
                  )
                }
                onClick={() =>
                  dispatch(
                    setInbound(
                      ticket,
                      data.requestId,
                      searchRequest
                        ? {
                            numAdults: searchRequest.travellerCountInfo.adultCount,
                            numChildren: searchRequest.travellerCountInfo.childCount,
                            numInfants: searchRequest.travellerCountInfo.babyCount,
                          }
                        : {},
                    ),
                  )
                }
                priceRemark={
                  <Typography variant="caption">
                    <FormattedMessage id="booking.inboundPrice" />
                  </Typography>
                }
              />
            ))}
            {searchCompleted < 99.9 && (
              <div
                style={{
                  height: '200px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <LoadingIcon />
              </div>
            )}
            {maxDisplayInbound < filteredInboundTickets.length && (
              <div style={{ textAlign: 'center', color: BLUE, lineHeight: '24px', margin: '20px 0' }}>
                <Button
                  onClick={() => {
                    if (maxDisplayInbound < filteredInboundTickets.length) {
                      this.setState({ maxDisplayInbound: maxDisplayInbound + STEP });
                    }
                  }}
                >
                  <Typography variant="body2" style={{ color: BLUE }}>
                    <FormattedMessage
                      id="result.displayMore"
                      values={{
                        num: filteredInboundTickets.length - maxDisplayInbound,
                      }}
                    />
                  </Typography>
                </Button>
              </div>
            )}
          </>
        ) : (
          <>
            {outboundTickets.map((ticket: some) => (
              <FlightTicketItem
                getAirlineInfo={getAirlineInfo}
                getTicketClass={getTicketClass}
                key={ticket.tid}
                ticket={ticket}
                buttonMsgId="result.selectTicket"
                onClick={() => {
                  dispatch(
                    setOutbound(
                      ticket,
                      data.requestId,
                      searchRequest
                        ? {
                            numAdults: searchRequest.travellerCountInfo.adultCount,
                            numChildren: searchRequest.travellerCountInfo.childCount,
                            numInfants: searchRequest.travellerCountInfo.babyCount,
                          }
                        : {},
                    ),
                  );
                  dispatch(
                    push({
                      pathname: ROUTES.flight.flightResultInbound,
                      search: window.location.search,
                    }),
                  );
                }}
                getPrice={ticketObj =>
                  ticketObj.outbound.ticketdetail.priceAdultUnit !== undefined ? (
                    <FormattedNumber value={ticketObj.outbound.ticketdetail.priceAdultUnit} />
                  ) : (
                    undefined
                  )
                }
                priceRemark={
                  <Typography variant="caption">
                    <FormattedMessage id="booking.outboundPrice" />
                  </Typography>
                }
              />
            ))}
            {searchCompleted < 99.9 && (
              <div
                style={{
                  height: '200px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <LoadingIcon />
              </div>
            )}
            {maxDisplayOutbound < filteredOutboundTickets.length && (
              <div style={{ textAlign: 'center', lineHeight: '24px', margin: '20px 0' }}>
                <Button
                  onClick={() => {
                    if (maxDisplayOutbound < filteredOutboundTickets.length) {
                      this.setState({ maxDisplayOutbound: maxDisplayOutbound + STEP });
                    }
                  }}
                >
                  <Typography variant="body2" style={{ color: BLUE }}>
                    <FormattedMessage
                      id="result.displayMore"
                      values={{
                        num: filteredOutboundTickets.length - maxDisplayOutbound,
                      }}
                    />
                  </Typography>
                </Button>
              </div>
            )}
          </>
        )}
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
    data: state.result.flight.data,
    sortBy: state.result.flight.sortBy,
    booking: state.booking.flight,
    filterParams: state.result.flight.filterParams,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    searchCompleted: state.result.flight.searchCompleted,
    searchRequest: state.result.flight.searchParams,
    location: state.router.location,
  };
}

export default connect(mapStateToProps)(FlightResultBoxTwoWay);
