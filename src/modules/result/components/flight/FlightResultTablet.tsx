import { Button, Container, IconButton, LinearProgress } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import FilterList from '@material-ui/icons/FilterList';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { MY_TOUR, TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { clearInbound, clearOutbound } from '../../../booking/redux/flightBookingReducer';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import FlightSearch from '../../../search/components/FlightSearch';
import FlightFilter from './FlightFilter';
import FlightResultBoxOneWay from './FlightResultBoxOneWay';
import FlightResultBoxTwoWay from './FlightResultBoxTwoWay';

function mapStateToProps(state: AppState) {
  return {
    result: state.result.flight,
    booking: state.booking.flight,
    oneWay: state.result.flight.data && state.result.flight.data.tickets,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  goToBooking(): void;
}

interface State {
  showFilters: boolean;
}

class FlightResultTablet extends React.PureComponent<Props, State> {
  state = {
    showFilters: false,
  };

  closeDialog = () => {
    const { booking, dispatch } = this.props;
    if (booking.inbound.ticket) {
      dispatch(clearInbound());
    } else {
      dispatch(clearOutbound());
    }
  };

  public render(): JSX.Element {
    const { result, booking, oneWay, goToBooking } = this.props;
    const { showFilters } = this.state;

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        {MY_TOUR && <LinearProgress variant="determinate" value={result.searchCompleted} color="secondary" />}
        <FlightSearch />
        {!MY_TOUR && <LinearProgress variant="determinate" value={result.searchCompleted} color="secondary" />}
        <Container style={{ flex: 1 }}>
          <div style={{ display: 'flex' }}>
            <div
              style={{
                width: '40px',
                overflow: 'visible',
                position: 'relative',
                zIndex: 100,
                minHeight: '100%',
                display: 'flex',
              }}
            >
              <div
                style={{
                  flexShrink: 0,
                  width: '300px',
                  transform: showFilters ? 'translateX(0)' : 'translateX(calc(-100% + 40px))',
                  backgroundColor: BACKGROUND,
                  padding: '0 10px 20px 0',
                  borderRight: `1px solid ${GREY}`,
                  transition: 'all 300ms',
                  minHeight: '100%',
                }}
              >
                <StickyDiv style={{ top: 65 }}>
                  <div style={{ textAlign: 'end' }}>
                    {showFilters ? (
                      <IconButton style={{ margin: '10px 0' }} onClick={() => this.setState({ showFilters: false })}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    ) : (
                      <IconButton onClick={() => this.setState({ showFilters: true })} style={{ margin: '10px 0' }}>
                        <FilterList style={{ color: DARK_GREY }} />
                      </IconButton>
                    )}
                  </div>
                  <div
                    style={{
                      opacity: showFilters ? 1 : 0.4,
                      position: 'relative',
                    }}
                  >
                    <FlightFilter />
                    {!showFilters && (
                      <div
                        style={{
                          position: 'absolute',
                          top: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                        }}
                      />
                    )}
                  </div>
                </StickyDiv>
              </div>
            </div>
            <div style={{ padding: '22px 0', flex: 1, minHeight: '400px', marginLeft: '10px' }}>
              {result.data && result.data.outbound ? <FlightResultBoxTwoWay /> : <FlightResultBoxOneWay />}
            </div>
          </div>
        </Container>
        <FlightTicketDialog
          close={this.closeDialog}
          open={(oneWay && !!booking.outbound.ticket) || (!oneWay && !!booking.inbound.ticket)}
          button={
            <Button color="secondary" variant="contained" size="large" onClick={goToBooking}>
              <FormattedMessage id="continue" />
            </Button>
          }
        />
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapStateToProps)(FlightResultTablet);
