import { Typography } from '@material-ui/core';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../colors';
import { SLASH_DATE_FORMAT, some } from '../../../../constants';
import { ReactComponent as IconAirPlaneDown } from '../../../../svg/ic_flight_ticket_airplane_down.svg';
import { ReactComponent as IconAirPlaneUp } from '../../../../svg/ic_flight_ticket_airplane_up.svg';
import { ReactComponent as IconBaggage } from '../../../../svg/ic_flight_ticket_baggage.svg';
import { ReactComponent as IconClock } from '../../../../svg/ic_flight_ticket_clock_2.svg';
import { ReactComponent as IconRefresh } from '../../../../svg/ic_flight_ticket_refresh_gray.svg';
import { ReactComponent as IconTicketBack } from '../../../../svg/ic_flight_ticket_ticket_back.svg';
import { durationMillisecondToHour, LONG_TRANSIT_DURATION } from '../../../common/utils';
import { TECHNICAL_STOP } from '../../constants';
import { BoxColumn, BoxInfoFlight, BoxLogoAirline, LabelTicketDetail } from './FlightTicketItem.style';

export const FlightDirectionDetail = (props: { ticket: some; getAirlineInfo(id: number): some }) => {
  const { ticket, getAirlineInfo } = props;
  return ticket.outbound.transitTickets ? (
    ticket.outbound.transitTickets.map((transitTicket: some, index: number) => (
      <React.Fragment key={index}>
        {transitTicket.transitDuration > LONG_TRANSIT_DURATION && (
          <LongTransitMessage
            from={`${transitTicket.departureCity} (${transitTicket.departureAirport})`}
            duration={durationMillisecondToHour(transitTicket.transitDuration)}
          />
        )}
        <FlightTicketDetail ticket={transitTicket} isTransit airlineInfo={getAirlineInfo(transitTicket.aid)} />
      </React.Fragment>
    ))
  ) : (
    <FlightTicketDetail ticket={ticket.outbound} isTransit={false} airlineInfo={getAirlineInfo(ticket.outbound.aid)} />
  );
};

const LongTransitMessage = (props: { from: string; duration: string }) => {
  const { from, duration } = props;
  return (
    <div style={{ paddingLeft: '128px' }}>
      <div
        style={{
          margin: 'auto',
          width: '710px',
          display: 'flex',
          alignItems: 'center',
          background: '#F5F5F5',
          height: '47px',
        }}
      >
        <div style={{ width: '116px', textAlign: 'center' }}>
          <Typography variant="body1" color="secondary">
            {duration}
          </Typography>
        </div>
        <Typography variant="body2">
          <FormattedMessage id="result.flightFrom" values={{ from }} />
        </Typography>
        &nbsp;-&nbsp;
        <Typography variant="caption" color="secondary">
          <FormattedMessage id="result.longTransitRecheckin" />
        </Typography>
      </div>
    </div>
  );
};

function getPolicyIcon(code: string) {
  if (code === 'LUGGAGE_INFO') {
    return <IconBaggage style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
  }

  if (code === 'CHANGE_ROUTE') {
    return <IconRefresh style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
  }

  return <IconTicketBack style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
}

const Policy: React.FC<{ item: some }> = props => {
  const { item } = props;
  return (
    <div style={{ display: 'flex', minWidth: '125px', alignItems: 'flex-start' }}>
      <div style={{ flexShrink: 0 }}>{getPolicyIcon(item.code)}</div>
      <LabelTicketDetail>
        <Typography variant="caption">{item.description}</Typography>
        {item.code === TECHNICAL_STOP && (
          <Typography color="error" variant="caption" style={{ fontWeight: 'bold' }}>
            &nbsp;
            <FormattedMessage id="result.needVisa" />
          </Typography>
        )}
      </LabelTicketDetail>
    </div>
  );
};

interface Props {
  ticket: some;
  airlineInfo: some;
  isTransit: boolean;
}

interface State {}

export class FlightTicketDetail extends PureComponent<Props, State> {
  render() {
    const { ticket, airlineInfo, isTransit } = this.props;

    if (!ticket) {
      return <div />;
    }

    const flightTime = isTransit ? ticket.flightDuration : ticket.duration;

    const aircraft = ((ticket.ticketdetail || {}).facilities || []).find((f: some) => f.code === 'AIRCRAFT_INFO');

    return (
      <div
        style={{
          padding: '16px',
          display: 'flex',
        }}
      >
        <BoxLogoAirline>
          <img alt="" style={{ height: '32px' }} src={airlineInfo.logo} />
          <Typography variant="caption">{ticket.flightNumber}</Typography>
          <Typography variant="caption">{aircraft && aircraft.description}</Typography>
        </BoxLogoAirline>

        <BoxInfoFlight>
          <BoxColumn
            style={{
              alignItems: 'center',
              height: '85%',
              justifyContent: 'space-between',
            }}
          >
            <IconAirPlaneUp style={{ height: 'auto', width: '42px' }} />
            <div style={{ borderLeft: `1px solid ${BLUE}`, height: '100%' }} />
            <IconAirPlaneDown style={{ height: 'auto', width: '42px' }} />
          </BoxColumn>
          <BoxColumn
            style={{
              height: '100%',
              justifyContent: 'space-between',
            }}
          >
            <BoxColumn>
              <Typography variant="subtitle2">
                <span>{ticket.departureTimeStr}</span>
              </Typography>
              <LabelTicketDetail>{moment(ticket.departureDayStr, SLASH_DATE_FORMAT).format('L')}</LabelTicketDetail>
            </BoxColumn>
            <div style={{ display: 'flex', padding: '15px 0', minWidth: '80px' }}>
              <IconClock style={{ width: '20px', height: 'auto', marginBottom: '4px' }} />
              <LabelTicketDetail style={{ color: BLUE }}>{durationMillisecondToHour(flightTime)}</LabelTicketDetail>
            </div>
            <BoxColumn>
              <Typography variant="subtitle2">
                <span>{ticket.arrivalTimeStr}</span>
              </Typography>
              <LabelTicketDetail>{moment(ticket.arrivalDayStr, SLASH_DATE_FORMAT).format('L')}</LabelTicketDetail>
            </BoxColumn>
          </BoxColumn>

          <BoxColumn style={{ height: '100%', justifyContent: 'space-between' }}>
            <BoxColumn>
              <Typography variant="subtitle2">
                {ticket.departureCity}
                &nbsp;&#40;
                {ticket.departureAirport}
                &#41;
              </Typography>
              <span>{ticket.departureAirportName}</span>
            </BoxColumn>
            <BoxColumn>
              <Typography variant="subtitle2">
                {ticket.arrivalCity}
                &nbsp;&#40;
                {ticket.arrivalAirport}
                &#41;
              </Typography>
              <span>{ticket.arrivalAirportName}</span>
            </BoxColumn>
          </BoxColumn>
        </BoxInfoFlight>
        <div style={{ flex: 1, display: 'flex' }}>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              padding: '10px 5px',
              flex: 1,
              flexFlow: 'column',
            }}
          >
            {isTransit &&
              ticket.polices &&
              ticket.polices.map((item: some, index: number) => <Policy item={item} key={index} />)}

            {!isTransit &&
              ticket.ticketdetail.polices &&
              ticket.ticketdetail.polices.map((item: some, index: number) => <Policy item={item} key={index} />)}
          </div>
        </div>
      </div>
    );
  }
}
