import { Button, Container, LinearProgress } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH, MY_TOUR } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { clearInbound, clearOutbound } from '../../../booking/redux/flightBookingReducer';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import FlightSearch from '../../../search/components/FlightSearch';
import FlightFilter from './FlightFilter';
import FlightResultBoxOneWay from './FlightResultBoxOneWay';
import FlightResultBoxTwoWay from './FlightResultBoxTwoWay';

function mapStateToProps(state: AppState) {
  return {
    result: state.result.flight,
    booking: state.booking.flight,
    oneWay: state.result.flight.data && state.result.flight.data.tickets,
    auth: state.auth.auth,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  goToBooking(): void;
}

interface State {}

class FlightResultDesktop extends React.PureComponent<Props, State> {
  state = {};

  closeDialog = () => {
    const { booking, dispatch } = this.props;
    if (booking.inbound.ticket) {
      dispatch(clearInbound());
    } else {
      dispatch(clearOutbound());
    }
  };

  public render(): JSX.Element {
    const { result, booking, oneWay, goToBooking } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        {MY_TOUR && <LinearProgress variant="determinate" value={result.searchCompleted} color="secondary" />}
        <FlightSearch />
        {!MY_TOUR && <LinearProgress variant="determinate" value={result.searchCompleted} color="secondary" />}
        <Container style={{ flex: 1 }}>
          <div style={{ padding: '22px 0', display: 'flex' }}>
            <div style={{ width: '270px', marginRight: '30px' }}>
              <StickyDiv style={{ top: 80 }}>
                <FlightFilter />
              </StickyDiv>
            </div>
            <div style={{ flex: 1, minHeight: '400px' }}>
              {result.data && result.data.outbound ? <FlightResultBoxTwoWay /> : <FlightResultBoxOneWay />}
            </div>
          </div>
        </Container>
        <FlightTicketDialog
          close={this.closeDialog}
          open={(oneWay && !!booking.outbound.ticket) || (!oneWay && !!booking.inbound.ticket)}
          button={
            <Button color="secondary" variant="contained" size="large" onClick={goToBooking}>
              <FormattedMessage id="continue" />
            </Button>
          }
        />
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapStateToProps)(FlightResultDesktop);
