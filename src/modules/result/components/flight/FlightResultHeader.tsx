import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BLACK_TEXT } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { parseFlightSearchParams } from '../../../search/utils';

export interface IFlightResultHeaderProps extends ReturnType<typeof mapStateToProps> {
  count: number;
}

export default connect(mapStateToProps)(
  class FlightResultHeader extends React.PureComponent<IFlightResultHeaderProps> {
    public render() {
      let state;
      try {
        state = parseFlightSearchParams(new URLSearchParams(window.location.search));
      } catch (_) {
        state = this.props.search;
      }
      const { outbound, count } = this.props;

      return (
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            flex: 1,
            paddingRight: '8px',
          }}
        >
          {!outbound.ticket ? (
            <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
              <Typography variant="h5">
                {state.origin ? `${state.origin.name} (${state.origin.code}) - ` : ''}
                {state.destination ? `${state.destination.name} (${state.destination.code}):` : ''}
                &nbsp;
              </Typography>
              <Typography variant="h5">
                <FormattedMessage id="result.totalResult" values={{ num: count }} />
              </Typography>
            </div>
          ) : (
            <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
              <Typography variant="h5">
                {state.destination ? `${state.destination.name} (${state.destination.code}) - ` : ''}
                {state.origin ? `${state.origin.name} (${state.origin.code}): ` : ''}
                &nbsp;
              </Typography>
              <Typography variant="h5">
                <FormattedMessage id="result.totalResult" values={{ num: count }} />
              </Typography>
            </div>
          )}
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Typography variant="body2">
              {state.departureDate.format('L')}
              {state.returnDate ? ` - ${moment(state.returnDate.valueOf()).format('L')}` : ''}
            </Typography>
            <Typography
              style={{
                width: '4px',
                height: '4px',
                borderRadius: '50%',
                background: BLACK_TEXT,
                margin: '0 6px',
              }}
            />
            <Typography variant="body2">
              <FormattedMessage
                id="result.totalPassengers"
                values={{
                  num:
                    state.travellerCountInfo.adultCount +
                    state.travellerCountInfo.childCount +
                    state.travellerCountInfo.babyCount,
                }}
              />
            </Typography>
          </div>
        </div>
      );
    }
  },
);

function mapStateToProps(state: AppState) {
  return {
    flightResult: state.result.flight.data,
    outbound: state.booking.flight.outbound,
    search: state.search.flight,
    locale: state.intl.locale,
  };
}
