import styled from 'styled-components';
import { DARK_GREY } from '../../../../colors';

export const FlightTransit = styled.span`
  ::after {
    content: '';
    display: block;
    width: 100%;
    height: 2px;
    top: 50%;
    background: ${props => props.theme.primary};
    transition: all ease-in-out 300ms;
  }
`;

export const LabelTicketDetail = styled.span`
  color: ${DARK_GREY};
`;

export const BoxColumn = styled.div`
  display: flex;
  flex-flow: column;
  padding: 0 2px;
`;

export const BoxLogoAirline = styled.div`
  display: flex;
  padding-top: 8px;
  width: 148px;
  flex-flow: column;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  padding-top: 8px;
  min-width: 285px;
  max-width: 385px;
  display: flex;
  align-items: start;
`;
