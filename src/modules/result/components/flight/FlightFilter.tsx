import { Checkbox, Collapse, Divider, FormControlLabel, Slider, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import { remove } from 'lodash';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DARK_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { roundUp } from '../../../../utils';
import { CheckButton, Col, PriceLabelComponent, TimeLabelComponent } from '../../../common/components/elements';
import { durationMillisecondToHour } from '../../../common/utils';
import { stringifyFlightSearchStateAndFilterParams } from '../../../search/utils';
import { PRICE_STEP, TIME_TAKE_OFF_AND_LAND } from '../../constants';
import { FlightFilterState, setFilter } from '../../redux/flightResultReducer';
import { ArrowStyle, DropDownHeader, DropDownStyle } from './FlightFilter.styles';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  isChangeItinerary?: boolean;
}

interface State {
  showNumberStop: boolean;
  fromAndTime: boolean;
  airline: boolean;
}
class FlightFilter extends PureComponent<Props, State> {
  state = {
    showNumberStop: true,
    fromAndTime: true,
    airline: true,
  };

  setNumStop(event: React.ChangeEvent<HTMLInputElement>, value: number) {
    const numStops = [...this.props.filter.numStops];
    if (event.target.checked) {
      numStops.push(value);
    } else {
      remove(numStops, obj => obj === value);
    }

    const filter: FlightFilterState = { ...this.props.filter, numStops };
    this.props.dispatch(setFilter(filter));
  }

  setPrice(value: number[]) {
    const filter: FlightFilterState = { ...this.props.filter, price: value };
    this.props.dispatch(setFilter(filter));
  }

  setTicketClass(data: some) {
    const ticketClass = [...this.props.filter.ticketClass];
    if (ticketClass.findIndex(obj => obj.cid === data.cid) === -1) {
      ticketClass.push(data);
    } else {
      remove(ticketClass, obj => obj.cid === data.cid);
    }

    const filter: FlightFilterState = { ...this.props.filter, ticketClass };
    this.props.dispatch(setFilter(filter));
  }

  setAirline(event: React.ChangeEvent<HTMLInputElement>, value: some) {
    const { searchParams, isChangeItinerary, router } = this.props;
    const airline = [...this.props.filter.airline];
    if (event.target.checked) {
      airline.push(value.id);
    } else {
      remove(airline, obj => obj === value.id);
    }

    const filter: FlightFilterState = { ...this.props.filter, airline };

    if (searchParams && !isChangeItinerary) {
      const params = stringifyFlightSearchStateAndFilterParams(searchParams, filter);
      this.props.dispatch(
        replace({
          pathname: router.location.pathname,
          search: `?${params}`,
          state: router.location.state,
        }),
      );
    }

    this.props.dispatch(setFilter(filter));
  }

  setTimeTakeOff(value: some, checked: boolean) {
    const timeTakeOff = [...this.props.filter.timeTakeOff];
    if (checked) {
      timeTakeOff.push(value);
    } else {
      remove(timeTakeOff, obj => obj.id === value.id);
    }

    const filter: FlightFilterState = { ...this.props.filter, timeTakeOff };
    this.props.dispatch(setFilter(filter));
  }

  setTimeLand(value: some, checked: boolean) {
    const timeLand = [...this.props.filter.timeLand];
    if (checked) {
      timeLand.push(value);
    } else {
      remove(timeLand, obj => obj.id === value.id);
    }

    const filter: FlightFilterState = { ...this.props.filter, timeLand };
    this.props.dispatch(setFilter(filter));
  }

  setFlightDuration(value: number[]) {
    const filter: FlightFilterState = { ...this.props.filter, flightDuration: value };
    this.props.dispatch(setFilter(filter));
  }

  setTransitDuration(value: number[]) {
    const filter: FlightFilterState = { ...this.props.filter, transitDuration: value };
    this.props.dispatch(setFilter(filter));
  }

  render() {
    const { showNumberStop, fromAndTime, airline } = this.state;
    const { flight, filter, intl, maxTransitDuration, maxFlightDuration } = this.props;
    return (
      <div>
        <div>
          <Typography variant="subtitle2" style={{ padding: '12px 0' }}>
            <FormattedMessage id="filter.showResultBy" />
          </Typography>
          <div>
            <Typography
              variant="body2"
              style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
            >
              <span>
                <FormattedMessage id="filter.showResult.price" />
              </span>
              <span style={{ color: DARK_GREY }}>
                <FormattedNumber value={filter.price[0]} />
                &nbsp;
                <FormattedMessage id="currency" />
                &nbsp;-&nbsp;
                <FormattedNumber value={filter.price[1]} />
                &nbsp;
                <FormattedMessage id="currency" />
              </span>
            </Typography>
            <div style={{ margin: '0 12px' }}>
              <Slider
                key={JSON.stringify(filter.price)}
                defaultValue={filter.price.slice()}
                min={0}
                step={PRICE_STEP}
                max={flight && flight.filters ? roundUp(flight.filters.maxPrice, PRICE_STEP) : PRICE_STEP}
                onChangeCommitted={(e: any, value: any) => this.setPrice(value)}
                ValueLabelComponent={PriceLabelComponent}
              />
            </div>
          </div>

          {/* <div style={{ padding: '14px 0px 12px' }}>
            <Typography variant="body1">
              <FormattedMessage id="filter.ticketClass" />
            </Typography>
            <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
              {flight &&
                flight.ticketClasses &&
                flight.ticketClasses.map((item: some) => (
                  <div
                    style={{ margin: '6px', display: 'flex', alignItems: 'center' }}
                    key={item.cid}
                  >
                    <CheckButton
                      active={
                        filter.ticketClass.findIndex(value => value.code === item.code) !== -1
                      }
                      onClick={() => this.setTicketClass(item)}
                    >
                      <Typography variant="body2" style={{ padding: '0 12px' }}>
                        {intl.locale.startsWith('vi') ? item.v_name : item.i_name}
                      </Typography>
                    </CheckButton>
                  </div>
                ))}
            </div>
          </div> */}
        </div>
        <div>
          <div>
            <DropDownHeader onClick={() => this.setState({ showNumberStop: !showNumberStop })}>
              <Typography variant="body1" style={DropDownStyle}>
                <FormattedMessage id="filter.numberStop" />
                <ArrowStyle active={showNumberStop}>&#9660;</ArrowStyle>
              </Typography>
              <Divider />
            </DropDownHeader>
            <Collapse in={showNumberStop}>
              {flight &&
                flight.filters &&
                flight.filters.numStops &&
                flight.filters.numStops.map((item: number) => (
                  <FormControlLabel
                    key={item}
                    style={{ outline: 'none', margin: '0', color: DARK_GREY }}
                    labelPlacement="end"
                    label={intl.formatMessage(
                      { id: item === 0 ? 'filter.numberStop.noStop' : 'filter.numberStop.stop' },
                      { num: item },
                    )}
                    control={
                      <Checkbox
                        size="medium"
                        onChange={event => this.setNumStop(event, item)}
                        value={item}
                        color="secondary"
                      />
                    }
                  />
                ))}
            </Collapse>
          </div>
        </div>
        <div>
          <div>
            <Col style={{ padding: '14px 0px 6px' }}>
              <Typography variant="body2">
                <FormattedMessage id="filter.takeOff" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                {TIME_TAKE_OFF_AND_LAND.map(item => (
                  <div style={{ padding: '6px' }} key={item.id}>
                    <CheckButton
                      active={filter.timeTakeOff.indexOf(item) !== -1}
                      onClick={() => this.setTimeTakeOff(item, filter.timeTakeOff.indexOf(item) === -1)}
                    >
                      <Typography variant="body2">{item.text}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </div>
            </Col>
            <Col style={{ padding: '14px 0px 6px' }}>
              <Typography variant="body2">
                <FormattedMessage id="filter.land" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                {TIME_TAKE_OFF_AND_LAND.map(item => (
                  <div style={{ padding: '6px' }} key={item.id}>
                    <CheckButton
                      active={filter.timeLand.indexOf(item) !== -1}
                      onClick={() => this.setTimeLand(item, filter.timeLand.indexOf(item) === -1)}
                    >
                      <Typography variant="body2">{item.text}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </div>
            </Col>
          </div>
        </div>

        <div>
          <div>
            <DropDownHeader onClick={() => this.setState({ fromAndTime: !fromAndTime })}>
              <Typography variant="body1" style={DropDownStyle}>
                <FormattedMessage id="filter.flightTime" />
                <ArrowStyle active={fromAndTime}>&#9660;</ArrowStyle>
              </Typography>
              <Divider />
            </DropDownHeader>
            <Collapse in={fromAndTime} style={{ paddingTop: '10px' }}>
              <>
                <Typography
                  variant="body2"
                  style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
                >
                  <span>
                    <FormattedMessage id="filter.leg" />
                  </span>
                  <span style={{ color: DARK_GREY }}>
                    {filter.flightDuration.length ? durationMillisecondToHour(filter.flightDuration[0]) : '0h'}
                    &nbsp;-&nbsp;
                    {durationMillisecondToHour(
                      filter.flightDuration.length ? filter.flightDuration[1] : this.props.maxFlightDuration,
                    )}
                  </span>
                </Typography>
                <div style={{ margin: '0 12px' }}>
                  <Slider
                    key={maxFlightDuration}
                    defaultValue={[0, maxFlightDuration]}
                    min={0}
                    max={maxFlightDuration}
                    onChangeCommitted={(e: any, value: any) => this.setFlightDuration(value)}
                    ValueLabelComponent={TimeLabelComponent}
                  />
                </div>
                <Typography
                  variant="body2"
                  style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
                >
                  <span>
                    <FormattedMessage id="filter.transit" />
                  </span>
                  <span style={{ color: DARK_GREY }}>
                    {filter.transitDuration.length ? durationMillisecondToHour(filter.transitDuration[0]) : '0h'}
                    &nbsp;-&nbsp;
                    {durationMillisecondToHour(
                      filter.transitDuration.length ? filter.transitDuration[1] : this.props.maxTransitDuration,
                    )}
                  </span>
                </Typography>
                <div style={{ margin: '0 12px' }}>
                  <Slider
                    key={maxTransitDuration}
                    defaultValue={[0, maxTransitDuration]}
                    min={0}
                    max={maxTransitDuration}
                    disabled={!maxTransitDuration}
                    onChangeCommitted={(e: any, value: any) => this.setTransitDuration(value)}
                    ValueLabelComponent={TimeLabelComponent}
                  />
                </div>
              </>
            </Collapse>
          </div>
        </div>

        <div>
          <div>
            <DropDownHeader onClick={() => this.setState({ airline: !airline })}>
              <Typography variant="body1" style={DropDownStyle}>
                <FormattedMessage id="filter.airline" />
                <ArrowStyle active={airline}>&#9660;</ArrowStyle>
              </Typography>
              <Divider />
            </DropDownHeader>
            <Collapse in={airline}>
              {flight &&
                flight.airlines &&
                flight.airlines.map((item: some) => (
                  <FormControlLabel
                    key={item.id}
                    style={{ outline: 'none', margin: '0', color: DARK_GREY }}
                    label={item.name}
                    labelPlacement="end"
                    control={
                      <Checkbox
                        checked={!!filter.airline.find(one => one === item.id)}
                        size="medium"
                        onChange={event => this.setAirline(event, item)}
                        value={item.id}
                        color="secondary"
                      />
                    }
                  />
                ))}
            </Collapse>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  let maxTransitDuration = 0;
  if (state.result.flight.data) {
    if (state.booking.flight.outbound.ticket) {
      maxTransitDuration = state.result.flight.maxTransitDuration.inbound;
    } else {
      maxTransitDuration = state.result.flight.maxTransitDuration.outbound;
    }
  }
  let maxFlightDuration = 0;
  if (state.result.flight.data) {
    if (state.booking.flight.outbound.ticket) {
      maxFlightDuration = state.result.flight.maxFlightDuration.inbound;
    } else {
      maxFlightDuration = state.result.flight.maxFlightDuration.outbound;
    }
  }
  return {
    maxTransitDuration,
    maxFlightDuration,
    flight: state.result.flight.data,
    filter: state.result.flight.filterParams,
    searchParams: state.result.flight.searchParams,
    router: state.router,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightFilter));
