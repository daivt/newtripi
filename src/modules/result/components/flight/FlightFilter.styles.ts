import styled from 'styled-components';

export const DropDownStyle = {
  padding: '10px 0',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};

export const ArrowStyle = styled.span<{ active: boolean }>`
  display: inline-block;
  margin-right: 6px;
  font-size: 10px;
  transition: all 300ms;
  transform: ${props => (!props.active ? 'rotate(0deg)' : 'rotate(180deg)')};
`;

export const DropDownHeader = styled.div`
  cursor: pointer;
`;
