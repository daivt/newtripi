import * as React from 'react';
import { Typography, Button } from '@material-ui/core';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { setOutbound } from '../../../booking/redux/flightBookingReducer';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../common/utils';
import { FLIGHT_SORT_BY } from '../../constants';
import { filterAndSort, flightCheapestCmp, flightFastestCmp, flightTimeTakeOffCmp } from '../../utils';
import FlightTicketItem from './FlightTicketItem';
import FlightSortBox from './FlightSortBox';
import FlightResultHeader from './FlightResultHeader';
import { some } from '../../../../constants';

const STEP = 10;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  filterButton?: React.ReactNode;
}

interface State {
  maxDisplay: number;
}

class FlightResultBoxOneWay extends React.Component<Props, State> {
  state = {
    maxDisplay: 10,
  };

  public render(): JSX.Element {
    const {
      data,
      filterParams,
      dispatch,
      getAirlineInfo,
      searchCompleted,
      getTicketClass,
      sortBy,
      filterButton,
      searchRequest,
    } = this.props;
    const { maxDisplay } = this.state;
    if (!data || !data.tickets) {
      return (
        <div
          style={{
            height: '200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      );
    }

    const filteredTickets = filterAndSort(
      data.tickets as some[],
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
    const tickets = filteredTickets.filter((ticket: some, i: number) => i < maxDisplay);

    return (
      <>
        <div style={{ display: 'flex' }}>
          {filterButton}
          <FlightResultHeader count={filteredTickets.length} />
          <FlightSortBox sortBy={sortBy} dispatch={dispatch} />
        </div>
        {tickets.map(ticket => (
          <FlightTicketItem
            getAirlineInfo={getAirlineInfo}
            getTicketClass={getTicketClass}
            key={ticket.tid}
            ticket={ticket}
            onClick={() =>
              dispatch(
                setOutbound(
                  ticket,
                  data.requestId,
                  searchRequest
                    ? {
                        numAdults: searchRequest.travellerCountInfo.adultCount,
                        numChildren: searchRequest.travellerCountInfo.childCount,
                        numInfants: searchRequest.travellerCountInfo.babyCount,
                      }
                    : {},
                ),
              )
            }
            getPrice={currentTicket => <FormattedNumber value={currentTicket.outbound.ticketdetail.priceAdultUnit} />}
            priceRemark={
              <Typography variant="caption">
                <FormattedMessage id="result.oneWayPrice" />
              </Typography>
            }
          />
        ))}
        {searchCompleted < 99.9 && (
          <div
            style={{
              height: '200px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingIcon />
          </div>
        )}
        {maxDisplay < filteredTickets.length && (
          <div style={{ textAlign: 'center', color: BLUE, lineHeight: '24px', margin: '20px 0' }}>
            <Button
              onClick={() => {
                if (maxDisplay < filteredTickets.length) {
                  this.setState({ maxDisplay: maxDisplay + STEP });
                }
              }}
            >
              <Typography variant="body2" style={{ color: BLUE }}>
                <FormattedMessage id="result.displayMore" values={{ num: filteredTickets.length - maxDisplay }} />
              </Typography>
            </Button>
          </div>
        )}
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    data: state.result.flight.data,
    filterParams: state.result.flight.filterParams,
    sortBy: state.result.flight.sortBy,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    searchCompleted: state.result.flight.searchCompleted,
    searchRequest: state.result.flight.searchParams,
  };
}

export default connect(mapStateToProps)(FlightResultBoxOneWay);
