import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Dispatch } from 'redux';
import { BootstrapInput } from '../../../common/components/elements';
import { FLIGHT_SORT_BY_DATA } from '../../constants';
import { setSortBy } from '../../redux/flightResultReducer';

export interface IFlightSortBoxProps {
  sortBy?: string;
  dispatch: Dispatch;
}

export default class FlightSortBox extends React.PureComponent<IFlightSortBoxProps> {
  public render() {
    const { sortBy } = this.props;

    return (
      <div style={{ width: '190px', display: 'flex', flexDirection: 'column' }}>
        <div>
          <Typography variant="body2">
            <FormattedMessage id="result.sortBy" />
          </Typography>
        </div>
        <div
          style={{
            backgroundColor: '#fff',
          }}
        >
          <Select
            value={sortBy}
            onChange={event => this.props.dispatch(setSortBy(event.target.value as string))}
            input={<BootstrapInput />}
            fullWidth
          >
            {FLIGHT_SORT_BY_DATA.map(item => (
              <MenuItem key={item.id} value={item.value}>
                <Typography variant="body2">
                  <FormattedMessage id={item.text} />
                </Typography>
              </MenuItem>
            ))}
          </Select>
        </div>
      </div>
    );
  }
}
