import { Container } from '@material-ui/core';
import { push } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { ROUTES, some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import MyRewardsCardBox from './MyRewardsCardBox';
import RewardsByCategoryBreadcrumbs from './RewardsByCategoryBreadcrumbs';
import RewardsCategoryBox from './RewardsCategoryBox';
import { AppState } from '../../../../redux/reducers';

interface IMyRewardsTabletDesktopProps extends ReturnType<typeof mapStateToProps> {
  data: some[];
  loadMore(): void;
  categories: some[];
  dispatch: Dispatch;
  remaining: number;
  loading: boolean;
}

const MyRewardsTabletDesktop: React.FunctionComponent<IMyRewardsTabletDesktopProps> = props => {
  const { categories, loadMore, data, dispatch, remaining, loading, state } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <RewardsByCategoryBreadcrumbs path={<FormattedMessage id="rewards.myRewards" />} />
        <RewardsCategoryBox
          data={categories}
          categoryId={null}
          onClick={(categoryId: number | null) => {
            if (categoryId) {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.rewardsByCategory.gen(categoryId)}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage ? state.backableToRewardPage + 1 : 1,
                  },
                }),
              );
            } else {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.myRewards}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage ? state.backableToRewardPage + 1 : 1,
                  },
                }),
              );
            }
          }}
        />
        <MyRewardsCardBox data={data} loadMore={loadMore} remaining={remaining} loading={loading} />
      </Container>
      <Footer />
    </PageWrapper>
  );
};
const mapStateToProps = (state: AppState) => ({
  state: state.router.location.state,
});

export default connect(mapStateToProps)(MyRewardsTabletDesktop);
