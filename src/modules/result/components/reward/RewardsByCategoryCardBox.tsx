import { Grid, Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some, PAGE_SIZE } from '../../../../constants';
import RewardCard from './RewardCard';
import SkeletonCard from './RewardCard/SkeletonCard';
import NoDataResult from '../../../common/components/NoDataResult';
import { BLUE } from '../../../../colors';
import { FormattedMessage } from 'react-intl';
interface Props {
  data?: some;
  loading: boolean;
  page: number;
  loadMore: () => void;
  dispatch: Dispatch;
}

const RewardsByCategoryCardBox: React.FunctionComponent<Props> = props => {
  const { data, page, loading, loadMore } = props;
  return (
    <div style={{ marginBottom: '30px' }}>
      {data && (
        <>
          {data.rewards.length > 0 ? (
            <Grid container spacing={3}>
              {data.rewards.map((rewards: some, i: number) => (
                <Grid item key={i} xs={3}>
                  <RewardCard data={rewards} />
                </Grid>
              ))}
            </Grid>
          ) : (
            <NoDataResult id="rewards.noDataFilter" style={{ marginTop: '56px' }} />
          )}
        </>
      )}
      {loading && (
        <Grid container spacing={3} style={{}}>
          {Array(4)
            .fill(0)
            .map((v: some, index: number) => (
              <Grid item xs={3} key={index}>
                <SkeletonCard key={index} />
              </Grid>
            ))}
        </Grid>
      )}
      {!loading && data && data.total - PAGE_SIZE * page > 0 && (
        <div style={{ textAlign: 'center', marginTop: '23px' }}>
          <Button onClick={loadMore}>
            <Typography style={{ color: BLUE }}>
              <FormattedMessage
                id="result.displayMore"
                values={{ num: data.total - PAGE_SIZE * page }}
              />
            </Typography>
          </Button>
        </div>
      )}
    </div>
  );
};

export default connect()(RewardsByCategoryCardBox);
