import { Container } from '@material-ui/core';
import * as React from 'react';
import { some, TABLET_WIDTH } from '../../../../constants';
import { Wrapper } from '../../../booking/components/styles';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import RewardsDetailBox from './RewardDetailBox';

interface IRewardDetailTabletDesktopProps {
  data?: some;
}

const RewardDetailTabletDesktop: React.FunctionComponent<
  IRewardDetailTabletDesktopProps
> = props => {
  const { data } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      {data ? (
        <>
          <img
            style={{ width: '100%', height: '360px', objectFit: 'cover' }}
            src={data && data.banner}
            alt=""
          />
          <Container style={{ flex: 1 }}>
            <RewardsDetailBox data={data} />
          </Container>
        </>
      ) : (
        <Wrapper>
          <LoadingIcon
            style={{
              height: '300px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          />
        </Wrapper>
      )}
      <Footer />
    </PageWrapper>
  );
};

export default RewardDetailTabletDesktop;
