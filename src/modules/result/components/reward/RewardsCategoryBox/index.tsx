import { Divider, IconButton, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { SECONDARY } from '../../../../../colors';
import {
  DESKTOP_WIDTH_NUM,
  MIN_TABLET_WIDTH_NUM,
  some,
  TABLET_WIDTH_NUM,
} from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import giftboxSvg from '../../../../../svg/ic_flight_payment_giftbox.svg';
import { slideSettings } from '../../../../home/components/common/Slider/setting';
import { fetchRewards } from '../../../redux/rewardResultReducer';
import styles from './slick.module.scss';

const CategoryDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 1px 16px;
  width: 105px;
  padding: 0px 4px;
`;

const Slide = styled.div`
  outline: none;
`;

interface Props {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  data: some[];
  categoryId?: number | null;
  onClick: (categoryId: number | null) => void;
}

const RewardsCategoryBox: React.FunctionComponent<Props> = props => {
  const { data, categoryId, onClick, dispatch } = props;
  const title = data.find(v => v.id === categoryId);
  React.useEffect(() => {
    dispatch(fetchRewards(1));
  }, [dispatch]);

  const dataWithMyRewards = [
    {
      id: null,
      icon: giftboxSvg,
      description: <FormattedMessage id="rewards.myRewards" />,
      skeleton: data.length > 0 ? data[0].skeleton : undefined,
    },
    ...data,
  ];

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-between', padding: '16px 0px' }}>
        <Typography variant="h5" style={{ width: '300px', wordBreak: 'break-word', flexShrink: 0 }}>
          {categoryId && title ? (
            title.description
          ) : categoryId === null ? (
            <FormattedMessage id="rewards.myRewards" />
          ) : (
            <FormattedMessage id="rewards.title" />
          )}
        </Typography>
        <div
          style={{
            textAlign: 'end',
            padding: '10px 40px',
            width: 'calc(100% - 300px)',
          }}
        >
          <Slider
            variableWidth={false}
            {...slideSettings(7, styles.category)}
            responsive={[
              {
                breakpoint: DESKTOP_WIDTH_NUM,
                settings: {
                  slidesToShow: 6,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: TABLET_WIDTH_NUM,
                settings: {
                  slidesToShow: 5,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: MIN_TABLET_WIDTH_NUM,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 1,
                },
              },
            ]}
          >
            {dataWithMyRewards.map((v: some, index: number) => (
              <Slide key={index}>
                <CategoryDiv>
                  {v.skeleton ? (
                    <>
                      <Skeleton variant="circle" width={50} height={50} />
                      <Skeleton variant="text" height={20} width={90} />
                    </>
                  ) : (
                    <>
                      <IconButton
                        style={{
                          backgroundColor: categoryId === v.id ? SECONDARY : 'transparent',
                          boxShadow:
                            '0px 1px 3px rgba(0, 0, 0, 0.2),0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',

                          width: '50px',
                          height: '50px',
                          flexDirection: 'column',
                        }}
                        onClick={() => onClick(v.id)}
                      >
                        <img
                          style={{
                            width: '26px',
                            filter: categoryId === v.id ? 'brightness(0) invert(1)' : undefined,
                            objectFit: 'cover',
                          }}
                          src={v.icon}
                          alt=""
                        />
                      </IconButton>
                      <div
                        style={{
                          textAlign: 'center',
                          width: '100%',
                        }}
                      >
                        <Typography
                          variant="caption"
                          style={{
                            height: '28px',
                            marginTop: '12px',
                            textAlign: 'center',
                            wordBreak: 'break-word',
                          }}
                        >
                          {v.description}
                        </Typography>
                      </div>
                    </>
                  )}
                </CategoryDiv>
              </Slide>
            ))}
          </Slider>
        </div>
      </div>
      <Divider style={{ marginTop: '4px', marginBottom: '4px' }} />
    </>
  );
};

export default connect()(RewardsCategoryBox);
