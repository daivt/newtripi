import { Button, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import { push } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../API';
import { GREEN, HOVER_GREY, SECONDARY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IcGift } from '../../../../svg/ic_gift.svg';
import { AuthDialog, setAuthDialog } from '../../../auth/redux/authReducer';
import MessageDialog from '../../../common/components/MessageDialog';
import MessageOptionDialog from '../../../common/components/MessageOptionDialog';
import { fetchThunk } from '../../../common/redux/thunks';
import RewardDetailBreadcrumbs from './RewardDetailBreadcrumbs';
interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const RewardDetailBox: React.FunctionComponent<Props> = props => {
  const { data, dispatch, auth } = props;
  const [message, setMessage] = React.useState('');
  const [messageOption, setMessageOption] = React.useState<{
    body: React.ReactNode;
    mesId: string;
    onClick: () => void;
    onClose: boolean;
  }>({ body: <div />, mesId: 'ok', onClick: () => {}, onClose: false });
  const [countTime, setTime] = React.useState<number>(data.signal ? data.signal.countDownTime : 0);

  React.useEffect(() => {
    const interval = setInterval(() => {
      setTime(time => time - 1);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const getPurchase = React.useCallback(() => {
    dispatch(
      fetchThunk(
        `${API_PATHS.rewardPurchase}`,
        'post',
        true,
        JSON.stringify({ rewardId: data.rewardId }),
      ),
    ).then(json => {
      if (json.code === 200 || json.code === '200') {
        setMessageOption({
          body: (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                marginBottom: '32px',
              }}
            >
              <IcGift style={{ margin: '50px 0px 32px 0px', width: '92px', height: '96px' }} />
              <Typography variant="h5">{json.data.responseData.message}</Typography>
              {json.data.responseData && (
                <Typography style={{ marginTop: '10px' }}>
                  <FormattedMessage
                    id="rewards.validToDate"
                    values={{
                      num: moment(json.data.responseData.validTo).format('L'),
                    }}
                  />
                </Typography>
              )}
            </div>
          ),
          mesId: 'rewards.useMyReward',
          onClick: () => {
            dispatch(
              push(`${ROUTES.reward.myRewardDetail.gen(json.data.id, json.data.objectType)}`),
            );
          },
          onClose: true,
        });
      } else {
        setMessage(json.message);
      }
    });
  }, [data.rewardId, dispatch]);
  const time = moment.duration(Math.abs(countTime), 'seconds');
  return (
    <div style={{ marginLeft: '16px', marginBottom: '80px' }}>
      <RewardDetailBreadcrumbs
        path={data.categories && data.categories.map((v: some) => v.description).join('/')}
        name={data.title}
      />
      <Typography style={{ marginBottom: '20px' }} variant="h5">
        {data.title}
      </Typography>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '50%' }}>
          <div dangerouslySetInnerHTML={{ __html: data.description }} />
          <div dangerouslySetInnerHTML={{ __html: data.policy }} />
        </div>
        <div style={{ marginLeft: '16px' }}>
          {data.signal && (
            <div
              style={{
                height: '48px',
                borderRadius: '4px',
                fontWeight: 'normal',
                background: data.signal ? fade(SECONDARY, 0.2) : fade(GREEN, 0.2),
                padding: '10px 12px',
                marginBottom: '20px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Typography variant="body2">
                {!data.isExpired ? (
                  <>
                    {data.signal.hotReward ? (
                      <FormattedMessage
                        id="rewards.limitTotal"
                        values={{ num: data.signal.rewardNum }}
                      />
                    ) : (
                      <FormattedMessage
                        id="rewards.expiryDate"
                        values={{
                          day: Math.floor(time.asDays()),
                          time: moment.utc(time.asMilliseconds()).format('HH:mm:ss'),
                        }}
                      />
                    )}
                  </>
                ) : (
                  <FormattedMessage id="rewards.expired" />
                )}
              </Typography>
            </div>
          )}
          <div style={{ background: HOVER_GREY, borderRadius: '4px', padding: '12px' }}>
            <div style={{ display: 'flex', padding: '10px 0px' }}>
              <Typography variant="body1">
                <FormattedMessage id="rewards.rewardNum" />: &nbsp;
              </Typography>
              <Typography variant="body1" style={{ fontWeight: 500 }}>
                {data.point}&nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
            <Typography variant="body1" style={{ padding: '10px 0px' }}>
              <FormattedMessage id="rewards.applyTime" />
              :&nbsp;
              {moment(data.fromDate * 1000).format('L')} &nbsp;- &nbsp;
              {moment(data.toDate * 1000).format('L')}
            </Typography>
            <Typography variant="body1" style={{ padding: '10px 0px' }}>
              {data.supportedRanksDesc}
            </Typography>
          </div>
          <Button
            style={{ marginTop: '20px', width: '270px', height: '40px' }}
            variant="contained"
            color="secondary"
            onClick={() => {
              if (auth) {
                setMessageOption({
                  body: (
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        paddingTop: '70px',
                        paddingBottom: '50px',
                      }}
                    >
                      <Typography variant="h6" style={{ marginBottom: '12px' }}>
                        <FormattedMessage id="rewards.confirm" />
                      </Typography>
                      <div style={{ display: 'flex' }}>
                        <Typography>
                          <FormattedMessage id="rewards.use" />
                        </Typography>
                        &nbsp;
                        <Typography color="primary">
                          {data.point}&nbsp;
                          <FormattedMessage id="point" />
                        </Typography>
                        &nbsp;
                        <Typography>
                          <FormattedMessage id="rewards.buyReward" />
                          &nbsp;?
                        </Typography>
                      </div>
                    </div>
                  ),
                  mesId: 'rewards.ok',
                  onClick: () => {
                    getPurchase();
                  },
                  onClose: true,
                });
              } else {
                dispatch(setAuthDialog(AuthDialog.login));
              }
            }}
          >
            <FormattedMessage id="rewards.getReward" />
          </Button>
        </div>
      </div>
      <MessageDialog
        show={!!message}
        onClose={() => setMessage('')}
        message={
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Typography variant="body1">{message}</Typography>
          </div>
        }
      />
      <MessageOptionDialog
        style={{ minWidth: '250px' }}
        show={messageOption.onClose}
        onClose={() => setMessageOption({ ...messageOption, onClose: false })}
        message={<div style={{ padding: '16px' }}>{messageOption.body}</div>}
        buttonMessageIdSecond={messageOption.mesId}
        onClick={() => {
          messageOption.onClick();
          setMessageOption({ ...messageOption, onClose: false });
        }}
      />
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    auth: state.auth.auth,
  };
};
export default connect(mapStateToProps)(RewardDetailBox);
