import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  path: JSX.Element | string;
}

const RewardsByCategoryBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state, path } = props;
  const backableToHomePage = state && state.backableToHomePage;
  const backableToRewardPage = state && state.backableToRewardPage;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToHomePage ? 'pointer' : undefined }}
          onClick={() =>
            backableToHomePage && backableToRewardPage
              ? props.dispatch(go(-backableToRewardPage - 1))
              : undefined
          }
        >
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToRewardPage ? 'pointer' : undefined }}
          onClick={() =>
            backableToRewardPage ? props.dispatch(go(-backableToRewardPage)) : undefined
          }
        >
          <FormattedMessage id="promotion" />
        </Typography>
        {!!path && (
          <Typography variant="body2" color="secondary">
            {path}
          </Typography>
        )}
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(RewardsByCategoryBreadcrumbs);
