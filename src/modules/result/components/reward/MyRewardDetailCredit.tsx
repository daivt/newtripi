import { Container, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../constants';
import RewardDetailBreadcrumbs from './RewardDetailBreadcrumbs';
interface Props {
  data: some;
}

const MyRewardDetailCredit: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <>
      <img
        src={data.rewardProgram.banner}
        alt=""
        style={{
          width: '100%',
          height: '335px',
          objectFit: 'cover',
          left: '0px',
          right: '0px',
        }}
      />
      <Container style={{ flex: 1, paddingBottom: '36px' }}>
        <RewardDetailBreadcrumbs
          path={<FormattedMessage id="rewards.myRewards" />}
          name={data.rewardProgram.title}
        />
        <Typography variant="h5" style={{ marginBottom: '20px' }}>
          {data.rewardProgram.title}
        </Typography>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.description }} />
            {data.rewardProgram.point && (
              <div style={{ display: 'flex' }}>
                <Typography>
                  <FormattedMessage id="rewards.used" />
                </Typography>
                &nbsp;
                <Typography color="primary">
                  {data.rewardProgram.point}&nbsp;
                  <FormattedMessage id="point" />
                </Typography>
                &nbsp;
                <Typography>
                  <FormattedMessage id="rewards.buyReward" />
                </Typography>
              </div>
            )}
          </Grid>
          <Grid item xs={6}>
            <div>
              {data.isUsed && (
                <Typography
                  variant="body2"
                  style={{
                    height: '48px',
                    borderRadius: '4px',
                    background:
                      'linear-gradient(0deg, rgba(0, 150, 136, 0.1), rgba(0, 150, 136, 0.1)), #FFFFFF',
                    padding: '0px 12px',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <FormattedMessage id="rewards.isUsed" />
                </Typography>
              )}
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default MyRewardDetailCredit;
