import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  path: JSX.Element | string;
  name: JSX.Element | string;
}

const RewardDetailBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state, path, name } = props;
  const backableToHomePage = state && state.backableToHomePage;
  const backableToRewardPage = state && state.backableToRewardPage;
  const backableToMyRewardPage = state && state.backableToMyRewardPage;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToHomePage ? 'pointer' : undefined }}
          onClick={() =>
            backableToHomePage && backableToRewardPage
              ? props.dispatch(go(-backableToRewardPage - (backableToMyRewardPage ? 2 : 1)))
              : undefined
          }
        >
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToRewardPage ? 'pointer' : undefined }}
          onClick={() =>
            backableToRewardPage
              ? props.dispatch(go(-backableToRewardPage - (backableToMyRewardPage ? 1 : 0)))
              : undefined
          }
        >
          <FormattedMessage id="promotion" />
        </Typography>
        {backableToMyRewardPage && (
          <Typography
            variant="body2"
            color="textPrimary"
            style={{ cursor: 'pointer' }}
            onClick={() => props.dispatch(go(-1))}
          >
            {path}
          </Typography>
        )}
        <Typography variant="body2" color="secondary">
          {name}
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(RewardDetailBreadcrumbs);
