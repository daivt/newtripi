import { Container, Grid, Typography } from '@material-ui/core';
import moment, { now } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { HOVER_GREY, GREEN, SECONDARY } from '../../../../colors';
import { some } from '../../../../constants';
import { CopyTextField } from '../../../booking/components/Form';
import RewardDetailBreadcrumbs from './RewardDetailBreadcrumbs';
import { fade } from '@material-ui/core/styles';
interface Props {
  data: some;
}

const MyRewardDetailVoucher: React.FunctionComponent<Props> = props => {
  const { data } = props;
  const [countTime, setTime] = React.useState(now());
  React.useEffect(() => {
    const interval = setInterval(() => {
      setTime(now());
    }, 1000);
    return () => clearInterval(interval);
  }, []);
  const time = moment.duration(Math.abs(countTime - data.responseData.validTo));
  return (
    <>
      <img
        src={data.rewardProgram.banner}
        alt=""
        style={{
          width: '100%',
          height: '335px',
          objectFit: 'cover',
          left: '0px',
          right: '0px',
        }}
      />
      <Container style={{ flex: 1, paddingBottom: '36px' }}>
        <RewardDetailBreadcrumbs
          path={<FormattedMessage id="rewards.myRewards" />}
          name={data.rewardProgram.title}
        />
        <Typography variant="h5" style={{ marginBottom: '20px' }}>
          {data.rewardProgram.title}
        </Typography>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.guide }} />
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.description }} />
          </Grid>
          <Grid item xs={6}>
            <div>
              <div
                style={{
                  height: '48px',
                  borderRadius: '4px',
                  fontWeight: 'normal',
                  background: data.isUsed ? fade(GREEN, 0.2) : fade(SECONDARY, 0.2),
                  padding: '10px 12px',
                  marginBottom: '20px',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Typography variant="body2">
                  {data.isUsed ? (
                    <FormattedMessage id="rewards.isUsed" />
                  ) : !data.isExpired ? (
                    <FormattedMessage
                      id="rewards.expiryDate"
                      values={{
                        day: Math.floor(time.asDays()),
                        time: moment.utc(time.asMilliseconds()).format('HH:mm:ss'),
                      }}
                    />
                  ) : (
                    <FormattedMessage id="rewards.expired" />
                  )}
                </Typography>
              </div>
              <div
                style={{
                  background: HOVER_GREY,
                  borderRadius: '4px',
                  padding: '16px 100px 40px 12px',
                }}
              >
                <Typography variant="subtitle2">
                  <FormattedMessage id="rewards.codeDetail" />
                </Typography>
                <CopyTextField
                  style={{ marginTop: '16px' }}
                  inputStyle={{ width: '288px' }}
                  text={data.codeDetail}
                />
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default MyRewardDetailVoucher;
