import { Container } from '@material-ui/core';
import { push } from 'connected-react-router';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { ROUTES, some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import RewardsBreadcrumbs from './RewardsBreadcrumbs';
import RewardsCardBox from './RewardsCardBox';
import RewardsCategoryBox from './RewardsCategoryBox';
import { AppState } from '../../../../redux/reducers';
interface Props extends ReturnType<typeof mapStateToProps> {
  data: some[];
  categories: some[];
  dispatch: Dispatch;
}

const RewardsTabletDesktop: React.FunctionComponent<Props> = props => {
  const { categories, data, dispatch, state } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <RewardsBreadcrumbs />
        <RewardsCategoryBox
          data={categories}
          onClick={(categoryId: number | null) => {
            if (categoryId) {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.rewardsByCategory.gen(categoryId)}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage ? state.backableToRewardPage + 1 : 1,
                  },
                }),
              );
            } else {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.myRewards}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage ? state.backableToRewardPage + 1 : 1,
                  },
                }),
              );
            }
          }}
        />
        <RewardsCardBox
          data={data}
          onClick={(categoryId: number) => {
            dispatch(
              push({
                pathname: `${ROUTES.reward.rewardsByCategory.gen(categoryId)}`,
                state: {
                  ...state,
                  backableToRewardPage:
                    state && state.backableToRewardPage ? state.backableToRewardPage + 1 : 1,
                },
              }),
            );
          }}
        />
      </Container>
      <Footer />
    </PageWrapper>
  );
};
const mapStateToProps = (state: AppState) => ({
  state: state.router.location.state,
});

export default connect(mapStateToProps)(RewardsTabletDesktop);
