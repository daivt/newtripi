import { Container } from '@material-ui/core';
import { replace, push } from 'connected-react-router';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { ROUTES, some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import RewardsByCategoryBreadcrumbs from './RewardsByCategoryBreadcrumbs';
import RewardsByCategoryCardBox from './RewardsByCategoryCardBox';
import RewardsByCategoryHeader from './RewardsByCategoryHeader';
import RewardsCategoryBox from './RewardsCategoryBox';
import { AppState } from '../../../../redux/reducers';

interface Props extends ReturnType<typeof mapStateToProps> {
  data?: some;
  categoryId: number | null;
  hasPurchase: boolean;
  sortBy: string;
  page: number;
  loading: boolean;
  loadMore: () => void;
  loyalInfo: some;
  categories: some[];
  dispatch: Dispatch;
}

const RewardsByCategoryTabletDesktop: React.FunctionComponent<Props> = props => {
  const {
    data,
    page,
    categoryId,
    hasPurchase,
    sortBy,
    loading,
    loadMore,
    dispatch,
    loyalInfo,
    categories,
    state,
  } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <RewardsByCategoryBreadcrumbs path={data ? data.description : ''} />
        <RewardsCategoryBox
          data={categories}
          categoryId={categoryId}
          onClick={categoryId => {
            if (categoryId) {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.rewardsByCategory.gen(categoryId)}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage
                        ? state.backableToRewardPage + 1
                        : undefined,
                  },
                }),
              );
            } else {
              dispatch(
                push({
                  pathname: `${ROUTES.reward.myRewards}`,
                  state: {
                    ...state,
                    backableToRewardPage:
                      state && state.backableToRewardPage
                        ? state.backableToRewardPage + 1
                        : undefined,
                  },
                }),
              );
            }
          }}
        />
        <RewardsByCategoryHeader
          sortBy={sortBy}
          hasPurchase={hasPurchase}
          data={loyalInfo}
          replace={search => {
            dispatch(
              replace({
                search,
                state,
              }),
            );
          }}
        />
        <RewardsByCategoryCardBox loading={loading} page={page} data={data} loadMore={loadMore} />
        {/* {!loading && total - PAGE_SIZE * page > 0 && (
          <div style={{ textAlign: 'center', marginTop: '23px' }}>
            <Button onClick={fetchMore}>
              <Typography style={{ color: BLUE }}>
                <FormattedMessage
                  id="result.displayMore"
                  values={{ num: total - PAGE_SIZE * page }}
                />
              </Typography>
            </Button>
          </div>
        )} */}
      </Container>
      <Footer />
    </PageWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(RewardsByCategoryTabletDesktop);
