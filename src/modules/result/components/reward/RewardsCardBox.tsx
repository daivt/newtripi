import { Grid, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import RewardCard from './RewardCard';
import SkeletonCard from './RewardCard/SkeletonCard';
import { ReactComponent as IcNoResult } from '../../../../svg/ic_contact_no_result.svg';
interface Props {
  dispatch: Dispatch;
  data: some[];
  onClick?: (categoryId: number) => void;
}

const RewardsCardBox: React.FunctionComponent<Props> = props => {
  const { data, onClick } = props;
  return (
    <div style={{ marginBottom: '30px' }}>
      {data.map((v: some, index: number) =>
        v.skeleton ? (
          <div key={index}>
            <Skeleton
              variant="text"
              width="320px"
              height="19px"
              style={{ marginTop: '24px', marginBottom: '16px' }}
            />
            <Grid container spacing={3} style={{}}>
              {Array(4)
                .fill(0)
                .map((v: some, index: number) => (
                  <Grid item xs={3} key={index}>
                    <SkeletonCard key={index} />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          <div key={index}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                marginTop: '32px',
                marginBottom: '16px',
              }}
            >
              <Typography
                style={{
                  fontSize: '16px',
                  lineHeight: '19px',
                  fontWeight: 'bold',
                }}
              >
                {v.description}
              </Typography>
              {v.rewards.length > 4 && (
                <Typography
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    color: BLUE,
                    cursor: 'pointer',
                    alignItems: 'center',
                  }}
                  onClick={() => !!onClick && onClick(v.id)}
                >
                  <FormattedMessage id="rewards.seeAll" />
                  &nbsp;
                  <NavigateNextIcon fontSize="small" />
                </Typography>
              )}
            </div>
            <Grid container spacing={3}>
              {v.rewards.map(
                (rewards: some, i: number) =>
                  i < 4 && (
                    <Grid item key={i} xs={3}>
                      <RewardCard data={rewards} />
                    </Grid>
                  ),
              )}
            </Grid>
          </div>
        ),
      )}
      {!data.length && (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            marginTop: '56px',
          }}
        >
          <IcNoResult />
          <Typography variant="body1" style={{ marginTop: '16px' }}>
            <FormattedMessage id="rewards.noData" />
          </Typography>
        </div>
      )}
    </div>
  );
};

export default connect()(RewardsCardBox);
