import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { GREY } from '../../../../../colors';
import { Grid } from '@material-ui/core';

interface Props {}

const SkeletonCardMyRewards: React.FunctionComponent<Props> = props => {
  return (
    <div
      style={{
        border: `1px solid ${GREY}`,
        borderRadius: '4px',
        boxSizing: 'border-box',
        background: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        padding: '12px 16px',
      }}
    >
      <Skeleton width={40} height={40} style={{ margin: '0px' }} />
      <div
        style={{
          marginLeft: '16px',
          flex: 1,
        }}
      >
        <Skeleton height={24} width={'80%'} style={{ margin: '0px' }} />
        <Grid
          container
          style={{
            marginTop: '16px',
          }}
        >
          <Grid item xs={4} style={{ paddingRight: '12px' }}>
            <Skeleton height={20} style={{ margin: '0px' }} />
            <Skeleton height={20} style={{ margin: '8px 0px 0px 0px' }} />
          </Grid>
          <Grid
            item
            xs={4}
            style={{
              paddingLeft: '12px',
              paddingRight: '12px',
              borderLeft: `1px solid ${GREY}`,
            }}
          >
            <Skeleton height={20} style={{ margin: '0px' }} />
            <Skeleton height={20} style={{ margin: '8px 0px 0px 0px' }} />
          </Grid>
          <Grid
            item
            xs={4}
            style={{
              paddingLeft: '12px',
              paddingRight: '12px',
              borderLeft: `1px solid ${GREY}`,
            }}
          >
            <Skeleton height={20} style={{ margin: '0px' }} />
            <Skeleton height={20} style={{ margin: '8px 0px 0px 0px' }} />
          </Grid>
        </Grid>
      </div>
      <div
        style={{
          display: 'flex',
          alignSelf: 'flex-end',
          justifyContent: 'flex-end',
        }}
      >
        <Skeleton width={100} height={32} style={{ margin: '0px' }} />
      </div>
    </div>
  );
};

export default SkeletonCardMyRewards;
