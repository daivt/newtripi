import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DARK_GREY, GREEN, GREY, RED } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import Link from '../../../../common/components/Link';
import SkeletonCardMyRewards from './SkeletonCardMyRewards';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
}

const MyRewardCard: React.FunctionComponent<Props> = props => {
  const { data, state } = props;

  if (!!data.skeleton) {
    return <SkeletonCardMyRewards />;
  }

  return (
    <div
      style={{
        border: `1px solid ${GREY}`,
        borderRadius: '4px',
        boxSizing: 'border-box',
        background: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        padding: '12px 16px',
        height: '100%',
      }}
    >
      <>
        <ProgressiveImage src={data.iconType} alt="" style={{ width: '40px', height: '40px', objectFit: 'cover' }} />
        <div
          style={{
            marginLeft: '16px',
            flex: 1,
          }}
        >
          <Typography style={{ fontSize: '16px', lineHeight: '24px', fontWeight: 'bold' }}>
            {data.rewardProgram.title}
          </Typography>
          <div
            style={{
              display: 'flex',
              alignSelf: 'flex-end',
              marginTop: '16px',
            }}
          >
            {data.responseData.code && (
              <div style={{ borderRight: `1px solid ${GREY}`, width: '35%' }}>
                <Typography variant="body1" style={{ color: GREY, lineHeight: '20px' }}>
                  <FormattedMessage id="rewards.code" />
                </Typography>
                <Typography
                  variant="body1"
                  style={{
                    lineHeight: '20px',
                    marginTop: '8px',
                    wordBreak: 'break-word',
                  }}
                >
                  {data.responseData.code}
                </Typography>
              </div>
            )}
            {data.responseData.validTo && (
              <div style={{ paddingLeft: '8px', borderRight: `1px solid ${GREY}`, width: '35%' }}>
                <Typography variant="body1" style={{ color: GREY, lineHeight: '20px' }}>
                  <FormattedMessage id="rewards.validTo" />
                </Typography>
                <div
                  // variant="body1"
                  style={{
                    lineHeight: '20px',
                    marginTop: '8px',
                    wordBreak: 'break-word',
                    fontSize: '16px',
                    color: data.isExpired ? RED : undefined,
                  }}
                >
                  {data.isExpired ? (
                    <FormattedMessage id="rewards.expired" />
                  ) : data.signal ? (
                    <div dangerouslySetInnerHTML={{ __html: data.signal }} />
                  ) : (
                    moment(data.responseData.validTo).format('L')
                  )}
                </div>
              </div>
            )}
            <div style={{ paddingLeft: '8px' }}>
              <Typography variant="body1" style={{ color: GREY, lineHeight: '20px' }}>
                <FormattedMessage id="rewards.slot" />
              </Typography>
              <Typography
                variant="body1"
                style={{
                  lineHeight: '20px',
                  marginTop: '8px',
                  wordBreak: 'break-word',
                }}
              >
                {data.remainNum || 0}
              </Typography>
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            alignItems: 'flex-end',
            flexDirection: 'column',
            minWidth: '120px',
          }}
        >
          <div style={{ flex: 1 }}>
            <Typography style={{ color: data.typeCode === 'GIFTCODE' ? '#FAAA29' : GREEN }}>
              {data.typeDetail}
            </Typography>
          </div>
          <Link
            to={{
              pathname: `${ROUTES.reward.myRewardDetail.gen(data.id, data.objectType)}`,
              state: { ...state, backableToMyRewardPage: true },
            }}
          >
            <Button
              variant={data.isExpired ? 'outlined' : data.isUsed ? 'outlined' : 'contained'}
              color={data.isExpired ? undefined : data.isUsed ? undefined : 'secondary'}
              style={{
                minWidth: '100px',
              }}
            >
              <Typography
                variant="button"
                style={{
                  color: data.isUsed ? DARK_GREY : data.isExpired ? DARK_GREY : 'white',
                }}
              >
                <FormattedMessage id={data.isUsed ? 'used' : data.isExpired ? 'rewards.expired' : 'rewards.useNow'} />
              </Typography>
            </Button>
          </Link>
        </div>
      </>
    </div>
  );
};
const mapStateToProps = (state: AppState) => ({
  state: state.router.location.state,
});

export default connect(mapStateToProps)(MyRewardCard);
