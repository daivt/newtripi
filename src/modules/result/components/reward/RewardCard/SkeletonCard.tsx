import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';

interface Props {}

const SkeletonCard: React.FunctionComponent<Props> = props => {
  return (
    <div
      style={{
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        minHeight: '284px',
      }}
    >
      <Skeleton variant="rect" style={{ width: '100%', height: '150px' }} />
      <div
        style={{
          padding: '8px 8px 16px 8px',
        }}
      >
        <Skeleton style={{ margin: '4px 0px' }} width="80%" height="19px" />
        <Skeleton height="32px" />
        <Skeleton height="30px" style={{ marginBottom: '0px', marginTop: '16px' }} />
      </div>
    </div>
  );
};

export default SkeletonCard;
