import { Button, Typography } from '@material-ui/core';
import moment, { now } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DARK_GREY } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import HotDealTag from '../../../../common/components/HotDealTag';
import Link from '../../../../common/components/Link';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';
import SkeletonCard from './SkeletonCard';
interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
}

const RewardCard: React.FunctionComponent<Props> = props => {
  const { data, state } = props;

  if (!data) {
    return <SkeletonCard />;
  }

  const days = Math.floor(moment(data.toDate * 1000).diff(now(), 'days', true));
  const hours = Math.floor(moment(data.toDate * 1000).diff(now(), 'hours', true));
  const minutes = Math.floor(moment(data.toDate * 1000).diff(now(), 'minutes', true));

  return (
    <div
      style={{
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        minHeight: '284px',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div style={{ position: 'relative' }}>
        <ProgressiveImage
          style={{ height: '150px', width: '100%', objectFit: 'cover' }}
          src={data.banner}
          alt=""
        />
        <img
          style={{
            width: '40px',
            height: '40px',
            position: 'absolute',
            top: '10px',
            right: '10px',
            borderRadius: '4px',
            boxShadow: '0px 0px 3px white',
            objectFit: 'cover',
          }}
          src={data.icon}
          alt=""
        />
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: data.signal && data.signal.hotReward ? 'space-between' : 'flex-end',
            alignItems: 'center',
            position: 'absolute',
            bottom: '-8px',
            left: '-4px',
            transform: 'translateY(-100%)',
          }}
        >
          {data.signal && data.signal.hotReward && (
            <HotDealTag style={{ minWidth: '96px' }}>
              <FormattedMessage id="rewards.hot" />
            </HotDealTag>
          )}
          {data.signal && data.signal.hotReward ? (
            <Typography
              variant="caption"
              style={{
                minHeight: '24px',
                minWidth: '96px',
                background: 'white',
                color: 'black',
                marginRight: '8px',
                opacity: 0.8,
                borderRadius: '4px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <FormattedMessage id="rewards.limitTotal" values={{ num: data.signal.rewardNum }} />
            </Typography>
          ) : days <= 3 ? (
            <Typography
              variant="caption"
              style={{
                minHeight: '24px',
                minWidth: '96px',
                background: 'white',
                color: 'black',
                marginRight: '8px',
                opacity: 0.8,
                borderRadius: '4px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              {minutes < 60 ? (
                <FormattedMessage id="rewards.remainingMinutes" values={{ num: minutes }} />
              ) : hours < 34 ? (
                <FormattedMessage id="rewards.remainingHours" values={{ num: hours }} />
              ) : (
                <FormattedMessage id="rewards.remainingDays" values={{ num: days }} />
              )}
            </Typography>
          ) : (
            undefined
          )}
        </div>
      </div>
      <div
        style={{
          padding: '4px 8px 16px 8px',
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <div style={{ flex: 1 }}>
          <Typography
            style={{
              textOverflow: 'ellipsis',
              overflow: 'hidden',
              whiteSpace: 'nowrap',
            }}
            variant="body1"
          >
            {data.title}
          </Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              marginTop: '8px',
              marginBottom: '16px',
            }}
          >
            <Typography variant="caption" color="primary">
              {data.point}&nbsp;
              <FormattedMessage id="point" />
            </Typography>
            <Typography style={{ maxWidth: '188px', color: DARK_GREY }} variant="caption">
              {data.supportedRanksDesc}
            </Typography>
          </div>
        </div>
        <Link
          to={{
            pathname: `${ROUTES.reward.rewardDetail.gen(data.rewardId)}`,
            state: {
              ...state,
              backableToMyRewardPage: state && state.backableToRewardPage ? true : false,
              backableToRewardPage:
                state && state.backableToRewardPage ? state.backableToRewardPage : 1,
            },
          }}
        >
          <Button variant="contained" color="secondary" fullWidth size="small">
            <Typography variant="body2">
              <FormattedMessage id="rewards.promotionDetail" />
            </Typography>
          </Button>
        </Link>
      </div>
    </div>
  );
};
const mapStateToProps = (state: AppState) => ({
  state: state.router.location.state,
});

export default connect(mapStateToProps)(RewardCard);
