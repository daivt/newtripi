import { Checkbox, Typography, FormControlLabel } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../constants';
import RewardsByCategorySortBox from './RewardsByCategorySortBox';
import { stringify } from 'querystring';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
  style?: React.CSSProperties;
  hasPurchase: boolean;
  sortBy: string;
  replace(search: string): void;
}

const RewardsByCategoryHeader: React.FunctionComponent<Props> = props => {
  const { data, style, hasPurchase, sortBy, replace, auth } = props;
  return (
    <div
      style={{
        minHeight: '32px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: '24px 16px',
        ...style,
      }}
    >
      {auth ? (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            minWidth: '600px',
          }}
        >
          {Number.isInteger(data.point) ? (
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                minWidth: '272px',
              }}
            >
              <Typography variant="body1">
                <FormattedMessage id="rewards.pointAvailable" />
              </Typography>
              <Typography variant="body1" color="secondary">
                <FormattedNumber value={data.point} />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
          ) : (
            <div />
          )}
          <div style={{ display: 'flex', marginLeft: '48px', alignItems: 'center' }}>
            <FormControlLabel
              control={
                <Checkbox
                  size="medium"
                  onChange={event =>
                    replace(`?${stringify({ sortBy, hasPurchase: event.target.checked })}`)
                  }
                  checked={hasPurchase}
                  color="primary"
                />
              }
              label={
                <Typography variant="body1">
                  <FormattedMessage id="rewards.onlyMine" />
                </Typography>
              }
            />
          </div>
        </div>
      ) : (
        <div />
      )}
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
      >
        <Typography variant="body1" style={{ marginRight: '12px' }}>
          <FormattedMessage id="rewards.sort" />
        </Typography>
        <RewardsByCategorySortBox
          sortBy={sortBy}
          onChange={value => replace(`?${stringify({ hasPurchase, sortBy: value })}`)}
        />
      </div>
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return { auth: state.auth.auth };
};
export default connect(mapStateToProps)(RewardsByCategoryHeader);
