import { Container, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../constants';
import RewardDetailBreadcrumbs from './RewardDetailBreadcrumbs';
interface Props {
  data: some;
}

const MyRewardDetailNormal: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <>
      <img
        src={data.rewardProgram.banner}
        alt=""
        style={{
          width: '100%',
          height: '335px',
          objectFit: 'cover',
          left: '0px',
          right: '0px',
        }}
      />
      <Container style={{ flex: 1, paddingBottom: '36px' }}>
        <RewardDetailBreadcrumbs
          path={<FormattedMessage id="rewards.myRewards" />}
          name={data.rewardProgram.title}
        />
        <Typography variant="h5" style={{ marginBottom: '20px' }}>
          {data.rewardProgram.title}
        </Typography>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <div style={{ display: 'flex', marginBottom: '8px' }}>
              <Typography>
                <FormattedMessage id="rewards.use" />
              </Typography>
              &nbsp;
              <Typography color="primary">
                {data.rewardProgram.point}&nbsp;
                <FormattedMessage id="point" />
              </Typography>
              &nbsp;
              <Typography>
                <FormattedMessage id="rewards.buyReward" />
                &nbsp;?
              </Typography>
            </div>
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.guide }} />
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.description }} />
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                height: '48px',
                borderRadius: '4px',
                background:
                  'linear-gradient(0deg, rgba(0, 150, 136, 0.1), rgba(0, 150, 136, 0.1)), #FFFFFF',
                padding: '0px 12px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              {data.isUsed ? (
                <FormattedMessage id="rewards.isUsed" />
              ) : (
                <FormattedMessage id="rewards.isUsedFail" />
              )}
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default MyRewardDetailNormal;
