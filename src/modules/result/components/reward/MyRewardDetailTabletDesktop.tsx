import { Container } from '@material-ui/core';
import * as React from 'react';
import { some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { CREDIT, MOBI_CARD, VOUCHER, NORMAL_ITEM } from '../../constants';
import MyRewardDetailCredit from './MyRewardDetailCredit';
import MyRewardDetailMobiCard from './MyRewardDetailMobiCard';
import MyRewardDetailVoucher from './MyRewardDetailVoucher';
import MyRewardDetailNormal from './MyRewardDetailNormal';

interface Props {
  data?: some;
}

const MyRewardDetailTabletDesktop: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      {data ? (
        <>
          {data.type === VOUCHER ? (
            <MyRewardDetailVoucher data={data} />
          ) : data.type === MOBI_CARD ? (
            <MyRewardDetailMobiCard data={data} />
          ) : data.type === CREDIT ? (
            <MyRewardDetailCredit data={data} />
          ) : data.type === NORMAL_ITEM ? (
            <MyRewardDetailNormal data={data} />
          ) : (
            <div style={{ flex: 1 }} />
          )}
        </>
      ) : (
        <Container style={{ flex: 1 }}>
          <LoadingIcon style={{ height: '320px', marginTop: '120px' }} />{' '}
        </Container>
      )}

      <Footer />
    </PageWrapper>
  );
};

export default MyRewardDetailTabletDesktop;
