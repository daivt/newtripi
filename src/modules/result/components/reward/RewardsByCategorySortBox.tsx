import { MenuItem, Select, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BootstrapInput } from '../../../common/components/elements';
import { REWARDS_SORT_BY } from '../../constants';

const SORT_BY = [
  {
    id: 0,
    text: 'rewards.sortBy.latest',
    value: REWARDS_SORT_BY.latest,
  },
  {
    id: 1,
    text: 'rewards.sortBy.common',
    value: REWARDS_SORT_BY.common,
  },
  {
    id: 2,
    text: 'rewards.sortBy.pointAsc',
    value: REWARDS_SORT_BY.pointAsc,
  },
  {
    id: 3,
    text: 'rewards.sortBy.pointDesc',
    value: REWARDS_SORT_BY.pointDesc,
  },
];

interface Props {
  sortBy: string;
  onChange: (value: string) => void;
}

const RewardsByCategorySortBox: React.FunctionComponent<Props> = props => {
  const { sortBy, onChange } = props;

  return (
    <>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '210px', display: 'flex', flexDirection: 'column' }}>
          <div
            style={{
              backgroundColor: '#fff',
            }}
          >
            <Select
              value={sortBy}
              onChange={event =>
                event.target.value !== sortBy && onChange(event.target.value as string)
              }
              input={<BootstrapInput />}
              fullWidth
            >
              {SORT_BY.map(item => (
                <MenuItem key={item.id} value={item.value}>
                  <Typography variant="body2">
                    <FormattedMessage id={item.text} />
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </div>
        </div>
      </div>
    </>
  );
};

export default RewardsByCategorySortBox;
