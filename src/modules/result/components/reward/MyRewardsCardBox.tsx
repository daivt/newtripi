import { Button, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import NoDataResult from '../../../common/components/NoDataResult';
import { skeleton } from '../../constants';
import MyRewardCard from './MyRewardCard';
interface Props {
  data: some[];
  loading: boolean;
  remaining: number;
  loadMore(): void;
}

const MyRewardsCardBox: React.FunctionComponent<Props> = props => {
  const { data, loadMore, loading, remaining } = props;
  return (
    <div style={{ marginTop: '24px', marginBottom: '30px' }}>
      <Grid container spacing={4}>
        {data.map((v: some, index: number) => (
          <Grid item xs={6} key={v.id}>
            <MyRewardCard data={v} />
          </Grid>
        ))}
        {loading &&
          skeleton(4).map((v, index) => (
            <Grid item xs={6} key={index}>
              <MyRewardCard data={v} />
            </Grid>
          ))}
      </Grid>{' '}
      {!loading && !data.length && (
        <NoDataResult id="rewards.noData" style={{ marginTop: '56px' }} />
      )}
      {remaining > 0 && !loading && (
        <div style={{ marginTop: '20px', textAlign: 'center' }}>
          <Button variant="text" disabled={loading} onClick={loadMore}>
            <Typography style={{ color: BLUE }}>
              <FormattedMessage
                id="result.displayMore"
                values={{
                  num: remaining,
                }}
              />
            </Typography>
          </Button>
        </div>
      )}
    </div>
  );
};

export default MyRewardsCardBox;
