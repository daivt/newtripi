import { Container, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { HOVER_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { CopyTextField } from '../../../booking/components/Form';
import RewardDetailBreadcrumbs from './RewardDetailBreadcrumbs';
interface Props {
  data: some;
}

const MyRewardDetailMobiCard: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <>
      <img
        src={data.rewardProgram.banner}
        alt=""
        style={{
          width: '100%',
          height: '335px',
          objectFit: 'cover',
          left: '0px',
          right: '0px',
        }}
      />
      <Container style={{ flex: 1, paddingBottom: '36px' }}>
        <RewardDetailBreadcrumbs
          path={<FormattedMessage id="rewards.myRewards" />}
          name={data.rewardProgram.title}
        />
        <Typography variant="h5" style={{ marginBottom: '20px' }}>
          {data.rewardProgram.title}
        </Typography>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.guide }} />
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.policy }} />
            <div dangerouslySetInnerHTML={{ __html: data.rewardProgram.description }} />
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                background: HOVER_GREY,
                borderRadius: '4px',
                padding: '16px 100px 40px 12px',
              }}
            >
              {data.responseData.cardInfo ? (
                <>
                  <Typography variant="subtitle2" style={{ marginBottom: '12px' }}>
                    <FormattedMessage id="rewards.cardInfo" />
                  </Typography>
                  <CopyTextField
                    style={{ marginTop: '12px' }}
                    inputStyle={{ width: '288px' }}
                    text={data.responseData.cardInfo.pinCode}
                    header={<FormattedMessage id="rewards.serial" />}
                  />
                  <CopyTextField
                    style={{ marginTop: '12px' }}
                    inputStyle={{ width: '288px' }}
                    text={data.responseData.cardInfo.serial}
                    header={<FormattedMessage id="rewards.pinCode" />}
                  />
                  <CopyTextField
                    style={{ marginTop: '12px' }}
                    inputStyle={{ width: '288px' }}
                    text={`*100*${data.responseData.cardInfo.serial}#`}
                    header={<FormattedMessage id="rewards.syntax" />}
                  />
                </>
              ) : (
                <Typography variant="subtitle2">{data.responseData.message}</Typography>
              )}
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default MyRewardDetailMobiCard;
