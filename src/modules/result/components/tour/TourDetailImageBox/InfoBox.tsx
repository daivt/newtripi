import { IconButton, Tab, Tabs, Typography, withStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import PickTourBox from '../PickTourBox';
import TourBasicInfo from '../../../../common/components/Tour/TourBasicInfo';
import RatingBox from './RatingBox';
import ReviewBox from './ReviewBox';
const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
  flex-shrink: 0;
`;

export const INFO_BOX_WIDTH = '320px';
const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: 500,
    minWidth: 130,
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

const CustomTabs = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${GREY}`,
    position: 'sticky',
    top: '0px',
    background: 'white',
    zIndex: 1,
  },
  indicator: {
    backgroundColor: theme.palette.primary.main,
    height: 4,
  },
}))(Tabs);

const Wrapper = styled.div`
  background: white;
  width: ${INFO_BOX_WIDTH};
  min-height: 100vh;
  height: 100%;
  max-height: 600px;
  overflow: auto;
  display: flex;
  flex-direction: column;
  z-index: 1400;
  @media (min-height: 725px) {
    max-height: 100vh;
  }
`;

interface Props {
  tour: some;
  style?: React.CSSProperties;
  currentTabIndex?: number;
  onClose?: () => void;
}

const InfoBox: React.FunctionComponent<Props> = props => {
  const [currentTabIndex, setIndex] = React.useState(props.currentTabIndex || 0);
  const { tour, onClose } = props;
  return (
    <Wrapper>
      <Line style={{ minHeight: 'auto' }}>
        <Typography
          title={tour.name}
          noWrap={true}
          variant="h5"
          style={{ marginLeft: '27px', marginTop: '8px', marginRight: '8px' }}
        >
          {tour.name}
        </Typography>
        {onClose && (
          <IconButton
            style={{ alignSelf: 'flex-end' }}
            color="default"
            size="medium"
            onClick={onClose}
          >
            <CloseIcon style={{ color: GREY }} />
          </IconButton>
        )}
      </Line>
      <div
        style={{
          marginLeft: '20px',
        }}
      >
        <TourBasicInfo tour={tour} />
      </div>
      <div
        style={{
          marginLeft: '12px',
          marginRight: '12px',
          position: 'relative',
          boxSizing: 'border-box',
          maxWidth: '320px',
        }}
      >
        <CustomTabs
          variant="fullWidth"
          value={currentTabIndex === -1 ? false : currentTabIndex}
          onChange={(e, val) => setIndex(val)}
        >
          <CustomTab fullWidth label={<FormattedMessage id="hotel.result.info.describe" />} />
          <CustomTab fullWidth label={<FormattedMessage id="hotel.result.info.review" />} />
        </CustomTabs>
      </div>
      {currentTabIndex === 0 ? (
        <ReviewBox tour={tour} />
      ) : currentTabIndex === 1 ? (
        <RatingBox tour={tour} />
      ) : (
        undefined
      )}
      <div
        style={{
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          background: 'white',
          position: 'relative',
        }}
      >
        <div style={{ margin: '8px auto' }}>
          <PickTourBox tour={tour} />
        </div>
      </div>
    </Wrapper>
  );
};

export default InfoBox;
