import { IconButton, Modal, Typography } from '@material-ui/core';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import Helmet from 'react-helmet';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import { FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { some } from '../../../../../constants';
import default_image from '../../../../../svg/default_image.svg';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';
import { INFO_BOX_WIDTH } from '../../hotel/HotelDetailImageBox/InfoBox';
import InfoBox from './InfoBox';
import './override.css';

export const Image = styled.img`
  max-height: 375px;
  width: 100%;
  object-fit: contain;
  margin: auto;
  display: block;
  @media (min-height: 875px) {
    max-height: 720px;
  }
  @media (min-width: 1000px) {
    max-width: 650px;
  }
  @media (min-width: 1400px) {
    max-width: 900px;
  }
`;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      width: '100%',
      diplay: 'flex',
      cursor: 'pointer',
      //   flexDirection: 'column',
      //   height: 350,
    },
  }),
);

interface Props {
  tour: some;
}

const TourDetailImageBox: React.FunctionComponent<Props> = props => {
  const classes = useStyles();
  const [showImageViewer, setShowImageViewer] = React.useState(false);
  const [startIndex, setIndex] = React.useState(0);
  const { tour } = props;
  const images: some[] = tour.images.map((v: some) => v);
  while (images.length < 5) {
    images.push({ imageURL: default_image });
  }
  return (
    <div>
      {images && images.length > 0 && (
        <GridList className={classes.gridList} cols={4}>
          <GridListTile
            key={0}
            cols={2}
            rows={2}
            onClick={() => {
              setIndex(0);
              setShowImageViewer(!showImageViewer);
            }}
          >
            <Helmet>
              <meta property="og:image" content={images[0].imageURL} />
            </Helmet>
            <img src={images[0].imageURL} alt="" />
          </GridListTile>
          <GridListTile key={1} cols={2} rows={2}>
            <GridList className={classes.gridList} cols={2}>
              {images.map((v: some, index: number) => {
                if (index > 0 && index < 5) {
                  return (
                    <GridListTile
                      key={index}
                      cols={1}
                      rows={1}
                      onClick={() => {
                        if (index < tour.images.length) setIndex(index);
                        setShowImageViewer(!showImageViewer);
                      }}
                    >
                      {index === 4 && images.length > 5 ? (
                        <div style={{ width: '100%', height: '100%' }}>
                          <div
                            style={{
                              position: 'absolute',
                              top: 0,
                              bottom: 0,
                              left: 0,
                              right: 0,
                              background:
                                'linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(image.png)',
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <Typography style={{ color: 'white' }}>
                              [<FormattedNumber value={images.length - index - 1} />]
                            </Typography>
                          </div>
                          <ProgressiveImage
                            style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                            src={v.imageURL}
                            alt=""
                          />
                        </div>
                      ) : (
                        <ProgressiveImage
                          src={v.imageURL}
                          alt=""
                          style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                        />
                      )}
                    </GridListTile>
                  );
                }
                return undefined;
              })}
            </GridList>
          </GridListTile>
        </GridList>
      )}
      <Modal
        open={showImageViewer}
        style={{ overflow: 'auto' }}
        BackdropProps={{ style: { background: 'rgba(0,0,0,0.75)' } }}
        onClose={() => setShowImageViewer(false)}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              flex: 1,
              minHeight: '100vh',
              height: '100%',
              maxHeight: '870px',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <div style={{ textAlign: 'end' }}>
              <IconButton
                style={{ alignSelf: 'flex-end' }}
                color="default"
                size="medium"
                onClick={() => setShowImageViewer(!showImageViewer)}
              >
                <CloseIcon style={{ color: 'white' }} />
              </IconButton>
            </div>
            <div
              style={{
                width: `calc(100vw - ${INFO_BOX_WIDTH} - 10px)`,
                margin: 'auto',
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
            >
              <ImageGallery
                items={tour.images.map((v: some) => {
                  return {
                    original: v.imageURL as string,
                    thumbnail: v.imageURL as string,
                    originalTitle: v.desc as string,
                    thumbnailTitle: v.desc as string,
                  };
                })}
                renderItem={item => <Image src={item.original} alt={item.originalTitle} />}
                showFullscreenButton={false}
                infinite={true}
                thumbnailPosition={'bottom'}
                showThumbnails={true}
                showIndex={true}
                startIndex={startIndex}
                // showBullets={true}
              />
            </div>
          </div>
          <InfoBox tour={tour} />
        </div>
      </Modal>
    </div>
  );
};

export default TourDetailImageBox;
