import { Divider, Typography } from '@material-ui/core';
import 'font-awesome/css/font-awesome.min.css';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, DARK_GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { ReactComponent as IcClock } from '../../../../../svg/ic_clock.svg';
import { ReactComponent as IcHotDeal } from '../../../../../svg/ic_hot_deal.svg';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 32px;
`;

const ScrollBar = styled.div`
  overflow: auto;
  margin-left: 15px;
  margin-right: 15px;
  flex: 1;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border-right: 8px solid white;
    /* border-left: 8px solid white; */
  }
  &::-webkit-scrollbar {
    background: transparent;
    width: 16px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;
class ReviewBox extends React.PureComponent<{ tour: some }, some> {
  public render() {
    const { tour } = this.props;

    return (
      <ScrollBar>
        <div style={{ marginBottom: '12px' }}>
          <div
            style={{
              // maxHeight: extend ? '100%' : '160px',
              overflow: 'hidden',
            }}
          >
            <Line style={{ height: '48px' }}>
              <Typography variant="h6">
                <FormattedMessage id="tour.commonInfo" />
              </Typography>
            </Line>
            <Line>
              <div style={{ width: '32px', textAlign: 'center' }}>
                <IcClock />
              </div>
              <Typography style={{ marginLeft: '12px' }} variant="body2">
                {tour.durationString}
              </Typography>
            </Line>
            {tour.means.map((v: some) => (
              <Line key={v.id}>
                <i
                  style={{ fontSize: '20px', width: '32px', textAlign: 'center', color: DARK_GREY }}
                  className={v.vehicleIconURL}
                />
                <Typography style={{ marginLeft: '12px' }} variant="body2">
                  {v.vehicleName}
                </Typography>
              </Line>
            ))}
            <Line>
              <div style={{ width: '32px', textAlign: 'center' }}>
                <IcClock />
              </div>
              <Typography style={{ marginLeft: '12px', color: BLUE }} variant="body2">
                {tour.startedSelling ? (
                  <FormattedMessage id="tour.startedSelling" />
                ) : (
                  <FormattedMessage id="tour.startedSellingFrom" values={{ date: tour.openDate }} />
                )}
              </Typography>
            </Line>
            {tour.instantBooking && (
              <Line>
                <div style={{ width: '32px', textAlign: 'center' }}>
                  <IcHotDeal />
                </div>
                <Typography style={{ marginLeft: '12px' }} variant="body2">
                  <FormattedMessage id="tour.detail.hotDeal" />
                </Typography>
              </Line>
            )}
          </div>
          <Divider />
          <div>
            <Line style={{ height: '48px' }}>
              <Typography variant="h6">
                <FormattedMessage id="tour.services" />
              </Typography>
            </Line>
            {tour.tourServices.map((v: some, index: number) => (
              <Line key={index}>
                <img style={{ height: '32px', marginRight: '8px' }} src={v.iconUrl} alt="" />
                <Typography variant="body2">{v.description}</Typography>
              </Line>
            ))}
          </div>
        </div>
      </ScrollBar>
    );
  }
}

export default ReviewBox;
