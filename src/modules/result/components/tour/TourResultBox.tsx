import { Button, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { BLUE } from '../../../../colors';
import { some, TOUR_PAGE_SIZE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import NoDataResult from '../../../common/components/NoDataResult';
import { searchMore } from '../../redux/tourResultReducer';
import TourResultItem from './TourResultItem';
interface ITourResultBoxProps extends ReturnType<typeof mapStateToProps> {
  tours?: some[];
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TourResultBox: React.FunctionComponent<ITourResultBoxProps> = props => {
  const { tours, result, dispatch } = props;
  const { locationResultData, termResultData } = result;
  let totalLength = 0;
  if (locationResultData) {
    totalLength = locationResultData.total;
  }
  if (termResultData) {
    totalLength = termResultData.data ? termResultData.data.total : 0;
  }
  const loading = result.loading;
  const notFound = result.locationResultData
    ? result.locationResultData.notFound
    : result.termResultData && result.termResultData.data
    ? !result.termResultData.data.tours
    : true;
  const pageOffset = result.pageOffset;
  return (
    <>
      <div style={{ margin: '10px 0' }}>
        {!loading && notFound && (
          <NoDataResult
            content={
              <Typography
                variant="body1"
                style={{
                  marginTop: '16px',
                  paddingLeft: '200px',
                  paddingRight: '200px',
                  textAlign: 'center',
                }}
              >
                {result.message || <FormattedMessage id="tour.noDataByTerm" />}
              </Typography>
            }
            style={{ marginTop: '48px', marginBottom: '32px' }}
          />
        )}
        {tours && (
          <Grid container spacing={5}>
            {tours.map(tour => (
              <Grid key={tour.id} item xs={4}>
                <TourResultItem key={tour.id} data={tour}></TourResultItem>
              </Grid>
            ))}
          </Grid>
        )}
      </div>

      {loading && (
        <Grid container spacing={5}>
          {[
            { id: 0, skeleton: true },
            { id: 1, skeleton: true },
            { id: 2, skeleton: true },
            { id: 3, skeleton: true },
            { id: 4, skeleton: true },
            { id: 5, skeleton: true },
          ].map(tour => (
            <Grid key={tour.id} item xs={4}>
              <TourResultItem key={tour.id} data={tour}></TourResultItem>
            </Grid>
          ))}
        </Grid>
      )}

      {!loading && totalLength - TOUR_PAGE_SIZE * pageOffset > 0 && (
        <div style={{ textAlign: 'center', marginTop: '23px' }}>
          <Button
            onClick={async () => {
              await dispatch(searchMore());
            }}
          >
            <Typography style={{ color: BLUE }}>
              <FormattedMessage
                id="result.displayMore"
                values={{ num: totalLength - TOUR_PAGE_SIZE * pageOffset }}
              />
            </Typography>
          </Button>
        </div>
      )}
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}

export default connect(mapStateToProps)(TourResultBox);
