import { IconButton, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconCheckAll } from '../../../../svg/ic_check_all.svg';
import { setFilter, TourFilterState } from '../../redux/tourResultReducer';

interface Props {
  dispatch: Dispatch;
  generalInformation?: some;
  filter: TourFilterState;
}

const TourResultFilterHeader: React.FC<Props> = props => {
  const { generalInformation, dispatch, filter } = props;
  if (!generalInformation) {
    return <></>;
  }

  const tourSubjects = generalInformation.tourSubjects;

  const onSelectAllTourSubject = () => {
    dispatch(setFilter({ ...filter, tourSubjects: [], groupName: '' }));
  };

  const onSelectTourSubject = (tourSubject: some) => {
    if (filter.groupName && filter.groupName === tourSubject.groupName) {
      return;
    }

    dispatch(
      setFilter({
        ...filter,
        tourSubjects: tourSubject.subjectList.map((obj: some) => obj.id),
        groupName: tourSubject.groupName,
      }),
    );
  };

  return (
    <div
      style={{
        background: '#fff',
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        borderRadius: '4px',
        display: 'flex',
        justifyContent: 'space-between',
        padding: '0 12px',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          width: '112px',
          justifyContent: 'flex-start',
          alignItems: 'center',
          padding: '20px 4px',
        }}
      >
        <IconButton
          style={{
            backgroundColor: !filter.groupName ? PRIMARY : 'transparent',
            boxShadow:
              '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
          }}
          onClick={() => onSelectAllTourSubject()}
        >
          <IconCheckAll
            style={{
              width: '26px',
              filter: !filter.groupName ? 'brightness(0) invert(1)' : undefined,
            }}
          />
        </IconButton>
        <Typography variant="caption" style={{ textAlign: 'center', paddingTop: '12px' }}>
          <FormattedMessage id="all" />
        </Typography>
      </div>
      {tourSubjects.map((subject: some, index: number) => (
        <div
          key={index}
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '112px',
            justifyContent: 'flex-start',
            alignItems: 'center',
            padding: '20px 4px',
          }}
        >
          <IconButton
            style={{
              backgroundColor:
                filter.groupName && filter.groupName === subject.groupName
                  ? PRIMARY
                  : 'transparent',
              boxShadow:
                '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
            }}
            onClick={() => onSelectTourSubject(subject)}
          >
            <img
              style={{
                width: '26px',
                height: '26px',
                filter:
                  filter.groupName && filter.groupName === subject.groupName
                    ? 'brightness(0) invert(1)'
                    : undefined,
              }}
              src={subject.groupIcon}
              alt=""
            />
          </IconButton>
          <Typography variant="caption" style={{ textAlign: 'center', paddingTop: '12px' }}>
            {subject.groupName}
          </Typography>
        </div>
      ))}
    </div>
  );
};

export default connect()(TourResultFilterHeader);
