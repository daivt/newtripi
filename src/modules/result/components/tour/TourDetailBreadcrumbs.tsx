import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';
import { Dispatch } from 'redux';
import { go } from 'connected-react-router';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const TourDetailBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToSearch = state && state.backableToSearch;

  return (
    <div style={{ paddingTop: '20px' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToSearch ? 'pointer' : undefined }}
          onClick={() => (backableToSearch ? props.dispatch(go(-backableToSearch)) : undefined)}
        >
          1. <FormattedMessage id="tour.search" />
        </Typography>
        <Typography variant="body2" color="secondary">
          2. <FormattedMessage id="tour.infoTour" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          3. <FormattedMessage id="tour.bookingInfo" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(TourDetailBreadcrumbs);
