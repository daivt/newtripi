import { Container } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import TourSearch from '../../../search/components/TourSearch';
import TourResultBox from './TourResultBox';
import TourResultBoxHeader from './TourResultBoxHeader';
import TourResultFilter from './TourResultFilter';
import TourResultFilterHeader from './TourResultFilterHeader';
import TourResultSortBox from './TourResultSortBox';

interface ITourResultDesktopProps extends ReturnType<typeof mapStateToProps> {
  num?: number;
  input?: string;
}

const TourResultDesktop: React.FunctionComponent<ITourResultDesktopProps> = props => {
  const { result, generalInformation, filter, num, input } = props;
  const { locationResultData, termResultData } = result;

  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light={true} />
      <TourSearch home={false} />
      <Container style={{ flex: 1 }}>
        <div style={{ padding: '22px 0', display: 'flex' }}>
          <div style={{ width: '280px', marginRight: '30px' }}>
            <TourResultFilter
              generalInformation={generalInformation}
              locationResultData={locationResultData}
              filter={filter}
            />
          </div>
          <div style={{ flex: 1, minHeight: '400px' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row-reverse',
              }}
            >
              <TourResultSortBox />
              <TourResultBoxHeader style={{ flex: 1 }} input={input} num={num} />
            </div>
            <div style={{ margin: '16px 0' }}>
              <TourResultFilterHeader generalInformation={generalInformation} filter={filter} />
            </div>
            <TourResultBox
              tours={
                locationResultData
                  ? locationResultData.tours
                  : termResultData && termResultData.data
                  ? termResultData.data.tours
                  : undefined
              }
            />
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}

export default connect(mapStateToProps)(TourResultDesktop);
