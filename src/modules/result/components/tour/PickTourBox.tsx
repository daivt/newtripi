import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREEN, DARK_GREY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { ReactComponent as CoinSvg } from '../../../../svg/coin.svg';
import Link from '../../../common/components/Link';
import { setTourData } from '../../../booking/redux/tourBookingReducer';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';

const Line = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 30px;
`;

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  tour: some;
  compact?: boolean;
}

const PickTourBox: React.FunctionComponent<Props> = props => {
  const { dispatch, tour, compact, router } = props;

  return (
    <div
      style={{
        display: 'flex',
        width: '285px',
        flexDirection: 'column',
        marginTop: '8px',
      }}
    >
      <Line style={{ justifyContent: 'flex-end' }}>
        {tour.originPrice > 0 && (
          <Typography variant="body1" style={{ textDecoration: 'line-through', color: DARK_GREY }}>
            <FormattedNumber value={tour.originPrice} /> <FormattedMessage id="currency" />
          </Typography>
        )}
        &nbsp; &nbsp;
        <Typography variant="h5" color="secondary">
          {tour && tour.price ? <FormattedNumber value={tour.price} /> : 0}
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
      {!compact && (
        <Line style={{ justifyContent: 'flex-end' }}>
          <CoinSvg style={{ marginRight: '8px' }} />
          <Typography variant="body1" style={{ color: GREEN }}>
            {tour && tour.bonusPoint ? <FormattedNumber value={tour.bonusPoint} /> : 0}
            &nbsp;
            <FormattedMessage id="booking.point" />
          </Typography>
        </Line>
      )}
      <Line style={{ justifyContent: 'flex-end' }}>
        {tour && (
          <Link
            onClick={() => dispatch(setTourData(tour))}
            to={{
              search: router.location.search,
              pathname: `${ROUTES.tour.tourBookingInfo}`,
              state: { ...router.location.state, backableToDetail: true },
            }}
          >
            <Button
              size="large"
              style={{
                width: '264px',
                marginBottom: '12px',
                marginTop: '8px',
              }}
              variant="contained"
              color="secondary"
            >
              <FormattedMessage id="tour.continue" />
            </Button>
          </Link>
        )}
      </Line>
    </div>
  );
};

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(PickTourBox);
