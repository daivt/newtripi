import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';

interface Props {
  tour: some;
  tabs: some[];
}

const DetailActivityBox: React.FunctionComponent<Props> = props => {
  const { tour, tabs } = props;
  return (
    <>
      {tour.tourPlanning && (
        <>
          <Typography id={tabs[2].id} variant="h6" style={{ margin: '24px 0px 16px 0px' }}>
            <FormattedMessage id="tour.experience" />
          </Typography>
          {tour.tourPlanning.map((v: some, index: number) => (
            <div key={index} style={{ overflow: 'hidden' }}>
              <Typography style={{ fontSize: '16px', fontWeight: 500 }}>{v.title}</Typography>
              <div
                style={{ wordBreak: 'break-word' }}
                dangerouslySetInnerHTML={{
                  __html: v.description,
                }}
              />
            </div>
          ))}
        </>
      )}
      {tour.info &&
        tour.info.map((v: some, index: number) => (
          <div key={index} style={{ overflow: 'hidden' }}>
            <Typography
              id={
                v.key === 'cancellationPolicies'
                  ? tabs[3].id
                  : v.key === 'notes'
                  ? tabs[4].id
                  : undefined
              }
              variant="h6"
              style={{ margin: '24px 0px 16px 0px' }}
            >
              {v.key === 'cancellationPolicies' ? (
                <FormattedMessage id={tabs[3].id} />
              ) : v.key === 'notes' ? (
                <FormattedMessage id={tabs[4].id} />
              ) : (
                v.title
              )}
            </Typography>
            <div
              style={{ wordBreak: 'break-word' }}
              dangerouslySetInnerHTML={{
                __html: v.description,
              }}
            />
          </div>
        ))}
    </>
  );
};

export default DetailActivityBox;
