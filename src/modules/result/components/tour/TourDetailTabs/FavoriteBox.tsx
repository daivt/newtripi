import { Grid } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchTourSuggest } from '../../../redux/tourResultReducer';
import TourResultItem from '../TourResultItem';
import { scrollTo } from '../../../../../utils';
import { skeleton } from '../../../constants';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  tour: some;
}

interface State {
  suggest?: some[];
}

class FavoriteBox extends React.PureComponent<Props, State> {
  state: State = {};
  async componentDidMount() {
    const { dispatch, tour } = this.props;
    const json = await dispatch(fetchTourSuggest(tour.id));
    if (json.code === 200) {
      this.setState({
        suggest: json.data,
      });
    }
  }

  public render() {
    const { suggest } = this.state;
    return (
      <div style={{ display: 'flex' }}>
        <Grid container spacing={6}>
          {suggest
            ? suggest.map(tour => (
                <Grid style={{ padding: '28px' }} key={tour.id} item xs={3}>
                  <div
                    onClick={() => {
                      scrollTo('root', 0);
                    }}
                  >
                    <TourResultItem key={tour.id} data={tour}></TourResultItem>
                  </div>
                </Grid>
              ))
            : skeleton(4).map(v => (
                <Grid style={{ padding: '28px' }} key={v.id} item xs={3}>
                  <TourResultItem key={v.id} data={v}></TourResultItem>
                </Grid>
              ))}
        </Grid>
      </div>
    );
  }
}

export default connect()(FavoriteBox);
