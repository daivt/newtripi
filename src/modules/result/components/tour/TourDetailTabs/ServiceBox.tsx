import { Grid, Typography, Divider } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { Extend } from '../../hotel/HotelDetailTabs/styles';

interface Props {
  tour: some;
}

const ServiceBox: React.FunctionComponent<Props> = props => {
  const { tour } = props;
  const [extend, setExtend] = React.useState(false);
  return (
    <>
      {tour && tour.tourServices.length > 0 && (
        <>
          <Grid container spacing={4}>
            {tour.tourServices
              .slice(0, extend ? tour.tourServices.length : 7)
              .map((v: some, index: number) => (
                <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }} key={index}>
                  <img
                    style={{ marginRight: '12px', width: '24px', height: '24px' }}
                    src={v.iconUrl}
                    alt=""
                  />
                  <Typography variant="body2">{v.description}</Typography>
                </Grid>
              ))}
            {tour.tourServices.length > 8 && (
              <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }}>
                <Extend onClick={() => setExtend(!extend)}>
                  <FormattedMessage
                    id={extend ? 'hotel.result.info.collapse' : 'hotel.result.info.seeAll'}
                  />
                </Extend>
              </Grid>
            )}
          </Grid>
          <Divider style={{ margin: '24px 0px', color: '#f4f4f4' }} />
        </>
      )}
    </>
  );
};

export default ServiceBox;
