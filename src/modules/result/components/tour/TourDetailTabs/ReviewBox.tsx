import { IconButton, Slide, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import ImageGallery from 'react-image-gallery';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import TourBasicInfo from '../../../../common/components/Tour/TourBasicInfo';
import { setReviewImagesToShow } from '../../../redux/tourResultReducer';
import { Image } from '../TourDetailImageBox';
import CommentCard from '../TourDetailImageBox/CommentCard';
import InfoBox, { INFO_BOX_WIDTH } from '../TourDetailImageBox/InfoBox';
interface Props extends ReturnType<typeof mapStateToProps> {
  tour: some;
  dispatch: Dispatch;
}
const WIDTH = '320px';
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;
const ReviewBox: React.FunctionComponent<Props> = props => {
  const { tour, listImagesToShow, dispatch } = props;
  const [extend, setExtend] = React.useState(false);
  React.useEffect(() => {
    const escAction = (event: some) => {
      if (event.keyCode === 27) {
        setExtend(false);
        dispatch(setReviewImagesToShow());
      }
    };
    window.addEventListener('keyup', escAction);

    return () => window.removeEventListener('keyup', escAction);
  }, [dispatch]);

  React.useEffect(() => {
    if (!!listImagesToShow) {
      setExtend(true);
    }
  }, [listImagesToShow]);
  return (
    <>
      <TourBasicInfo tour={tour} />
      {tour.comments.slice(0, 2).map((v: some, index: number) => {
        return <CommentCard key={index} data={v} style={{ justifyContent: 'flex-start' }} />;
      })}
      <Extend onClick={() => setExtend(!extend)}>
        <Typography variant="caption">
          <FormattedMessage id="tour.seeAllReview" />
        </Typography>
      </Extend>
      <Slide in={extend} direction={'left'}>
        <Line
          style={{
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            zIndex: 1300,
            // width: !!listImagesToShow && !extend ? '100%' : undefined,
          }}
        >
          {listImagesToShow && (
            <div
              style={{
                flex: 1,
                minHeight: '100vh',
                height: '100%',
                display: 'flex',
                flexDirection: 'column',
                background: 'rgba(0, 0, 0, 0.75)',
              }}
            >
              <div style={{ textAlign: 'end' }}>
                <IconButton
                  style={{ alignSelf: 'flex-end' }}
                  color="default"
                  size="medium"
                  onClick={() => dispatch(setReviewImagesToShow())}
                >
                  <CloseIcon style={{ color: 'white' }} />
                </IconButton>
              </div>
              <div
                style={{
                  width: `calc(100vw - ${INFO_BOX_WIDTH} - 10px)`,
                  margin: 'auto',
                  flex: 1,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                }}
              >
                <ImageGallery
                  items={listImagesToShow.data.photos.map((v: some) => {
                    return {
                      original: v.thumbLink as string,
                      thumbnail: v.thumbLink as string,
                    };
                  })}
                  renderItem={item => <Image src={item.original} alt={item.originalTitle} />}
                  showFullscreenButton={false}
                  infinite={true}
                  thumbnailPosition={'bottom'}
                  showThumbnails={true}
                  showIndex={true}
                  startIndex={listImagesToShow.index}
                />
              </div>
            </div>
          )}
          <div
            style={{
              background: 'white',
              width: WIDTH,
              boxShadow:
                // tslint:disable-next-line:max-line-length
                '0px 8px 10px rgba(0, 0, 0, 0.2), 0px 6px 30px rgba(0, 0, 0, 0.12), 0px 16px 24px rgba(0, 0, 0, 0.14)',
              overflow: 'auto',
            }}
          >
            <InfoBox
              tour={tour}
              currentTabIndex={1}
              onClose={() => {
                setExtend(false);
                dispatch(setReviewImagesToShow());
              }}
            />
          </div>
        </Line>
      </Slide>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return { listImagesToShow: state.result.tour.reviewImagesToShow };
};
export default connect(mapStateToProps)(ReviewBox);
