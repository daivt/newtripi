import { Container, Tab, Typography, withStyles } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY, PRIMARY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { scrollTo } from '../../../../../utils';
import { CustomTabs, StickyDiv } from '../../../../common/components/elements';
import TourBasicInfoBox from '../TourBasicInfoBox';
import CommonInfoBox from './CommonInfoBox';
import DetailActivityBox from './DetailActivityBox';
import FavoriteBox from './FavoriteBox';
import ReviewBox from './ReviewBox';
import ServiceBox from './ServiceBox';
const OFFSET = 175;

const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: 'normal',
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

const tabs = [
  { id: 'tour.commonInfo', content: <></> },
  { id: 'tour.reView', content: <></> },
  { id: 'tour.detailActivity', content: <></> },
  { id: 'tour.refund', content: <></> },
  { id: 'tour.moreInfo', content: <></> },
  { id: 'tour.favorite', content: <></> },
];

const STICKY_HEIGHT = 96;

interface Props {
  tour: some;
}

const TourDetailTabs: React.FunctionComponent<Props> = props => {
  const [currentTabIndex, setIndex] = React.useState(0);
  const [showSticky, setShowSticky] = React.useState(false);
  const rootDiv = React.useRef<HTMLDivElement>(null);
  const { tour } = props;
  React.useEffect(() => {
    const handler = () => {
      if (rootDiv.current) {
        if (rootDiv.current.getBoundingClientRect().top < STICKY_HEIGHT) {
          setShowSticky(true);
        } else {
          setShowSticky(false);
        }
      }
    };
    window.addEventListener('scroll', handler);
    handler();
    return () => window.removeEventListener('scroll', handler);
  }, []);

  return (
    <div ref={rootDiv}>
      <StickyDiv
        style={{
          borderBottom: `1px solid ${GREY}`,
          boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2)',
          background: BACKGROUND,
          top: `${STICKY_HEIGHT - 2}px`,
          zIndex: 100,
        }}
      >
        <div
          style={{
            position: 'absolute',
            width: '100%',
            transform: 'translateY(-100%)',
            background: BACKGROUND,
            height: `${STICKY_HEIGHT}px`,
            visibility: showSticky ? 'visible' : 'hidden',
            opacity: showSticky ? 1 : 0,
            transition: 'opacity 300ms',
          }}
        >
          <Container>
            <TourBasicInfoBox tour={tour} compact />
          </Container>
        </div>
        <Container>
          <CustomTabs
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => {
              setIndex(val);
              scrollTo(tabs[val].id, OFFSET);
            }}
          >
            {tabs.map((tab, index) => (
              <CustomTab
                label={
                  <Typography
                    variant="body2"
                    style={{
                      textDecoration: 'none',
                      color: index === currentTabIndex ? PRIMARY : DARK_GREY,
                    }}
                  >
                    <FormattedMessage id={tab.id} />
                  </Typography>
                }
                key={index}
              />
            ))}
          </CustomTabs>
        </Container>
      </StickyDiv>
      <Container style={{ display: 'flex' }}>
        <div style={{ flex: 1, marginBottom: '32px', overflow: 'hidden' }}>
          <Typography id={tabs[0].id} variant="h6" style={{ margin: '24px 0px 16px 0px' }}>
            <FormattedMessage id={'tour.serviceInfo'} />
          </Typography>
          <CommonInfoBox tour={tour} />
          <ServiceBox tour={tour} />
          <Typography id={tabs[1].id} variant="h6" style={{ margin: '0px 0px 8px 0px' }}>
            {tour.numberOfComments >= 0 && `${tour.numberOfComments} `}
            <FormattedMessage id={tabs[1].id} />
          </Typography>
          <ReviewBox tour={tour} />
          <DetailActivityBox tour={tour} tabs={tabs} />
          <Typography id={tabs[5].id} variant="h6" style={{ margin: '24px 0px 16px 0px' }}>
            <FormattedMessage id={tabs[5].id} />
          </Typography>
          <FavoriteBox tour={tour} />
        </div>
      </Container>
    </div>
  );
};

export default TourDetailTabs;
