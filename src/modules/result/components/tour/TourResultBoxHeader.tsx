import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Line } from '../hotel/HotelDetailTabs/styles';
import Skeleton from '@material-ui/lab/Skeleton';

interface ITourResultBoxHeaderProps {
  input?: string;
  num?: number;
  style?: React.CSSProperties;
}

const TourResultBoxHeader: React.FunctionComponent<ITourResultBoxHeaderProps> = props => {
  const { input, num, style } = props;

  if (input === undefined || num === undefined) {
    return (
      <div style={style}>
        <Skeleton width="300px" height="25px" />
      </div>
    );
  }

  return (
    <div style={style}>
      <Line>
        <Typography variant="h5">
          {input}:&nbsp;
          <FormattedMessage id="result.header.found" values={{ num }} />
        </Typography>
      </Line>
    </div>
  );
};

export default TourResultBoxHeader;
