import { Checkbox, FormControlLabel, Slider } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { remove } from 'lodash';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DARK_GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CheckButton, PriceLabelComponent } from '../../../common/components/elements';
import { PRICE_STEP } from '../../constants';
import { setFilter, TourFilterState } from '../../redux/tourResultReducer';
import { BoxColumn, BoxContent } from './TourFilter.styles';

function mapState2Props(state: AppState) {
  return {
    filterMaxPrice: state.result.tour.filterMaxPrice,
  };
}

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  generalInformation?: some;
  locationResultData?: some;
  filter: TourFilterState;
}

interface State {}

class TourResultFilter extends React.PureComponent<Props, State> {
  state: State = {};

  onSelectAllTourSubject(isChecked: boolean, subjectList: some[]) {
    const { filter, dispatch } = this.props;
    const allSubjects = subjectList.map((obj: some) => obj.id);
    dispatch(setFilter({ ...filter, tourSubjects: allSubjects }));
  }

  setPrice(value: number) {
    const filter: TourFilterState = { ...this.props.filter, maxPrice: value };
    this.props.dispatch(setFilter(filter));
  }

  setActivityTag(activityId: number) {
    const { filter, dispatch } = this.props;
    const activityTags = [...filter.activityTags];
    remove(activityTags, id => id === activityId);

    if (activityTags.length === filter.activityTags.length) {
      activityTags.push(activityId);
    }

    dispatch(setFilter({ ...filter, activityTags }));
  }

  setTourType(tourId: number) {
    const { filter, dispatch } = this.props;
    const tourTypes = [...filter.tourTypes];
    remove(tourTypes, id => id === tourId);

    if (tourTypes.length === filter.tourTypes.length) {
      tourTypes.push(tourId);
    }

    dispatch(setFilter({ ...filter, tourTypes }));
  }

  setTourService(tourId: number) {
    const { filter, dispatch } = this.props;
    const tourServices = [...filter.tourServices];
    remove(tourServices, id => id === tourId);

    if (tourServices.length === filter.tourServices.length) {
      tourServices.push(tourId);
    }

    dispatch(setFilter({ ...filter, tourServices }));
  }

  setTransport(index: number) {
    const { filter, dispatch } = this.props;
    const transportations = [...filter.transportations];
    remove(transportations, id => id === index);

    if (transportations.length === filter.transportations.length) {
      transportations.push(index);
    }

    dispatch(setFilter({ ...filter, transportations }));
  }

  setTourSubject(isChecked: boolean, subjectList: some[], index: number) {
    const { filter, dispatch } = this.props;
    const tourSubjects = [...filter.tourSubjects];

    if (isChecked) {
      tourSubjects.push(index);
    } else {
      remove(tourSubjects, id => id === index);

      if (!tourSubjects.length) {
        tourSubjects.push(...subjectList.map((obj: some) => obj.id));
      }
    }
    dispatch(setFilter({ ...filter, tourSubjects }));
  }

  public render() {
    const { filter, generalInformation, filterMaxPrice } = this.props;
    const tourSubjectSelected = generalInformation
      ? generalInformation.tourSubjects.find((obj: some) => filter.groupName && obj.groupName === filter.groupName)
      : undefined;

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <BoxColumn>
          <Typography variant="subtitle2">
            <FormattedMessage id="hotel.filter.selectResult" />
          </Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              paddingTop: '16px',
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="filter.showResult.price" />
            </Typography>
            <Typography variant="body2" color="textSecondary">
              0&nbsp;
              <FormattedMessage id="currency" />
              &nbsp;-&nbsp;
              <FormattedNumber value={filter.maxPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </div>
          <div style={{ margin: '12px 12px 0 12px' }}>
            <Slider
              key={`filter${filterMaxPrice}`}
              defaultValue={filter.maxPrice}
              min={0}
              max={filterMaxPrice < 0 ? 0 : filterMaxPrice}
              step={PRICE_STEP}
              onChangeCommitted={(e: any, value: any) => this.setPrice(value)}
              ValueLabelComponent={PriceLabelComponent}
            />
          </div>
        </BoxColumn>
        {tourSubjectSelected && (
          <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
            <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
              <FormattedMessage id="tour.filter.tourSubject" />
            </Typography>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }}>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '32px',
                  }}
                >
                  <img style={{ width: '24px' }} src={tourSubjectSelected.groupIcon} alt="" />
                </div>
                <Typography variant="body2" color="primary" style={{ fontWeight: 500, paddingLeft: '6px' }}>
                  {tourSubjectSelected.groupName}
                </Typography>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '40px' }}>
                <FormControlLabel
                  label={
                    <Typography
                      variant="caption"
                      style={{
                        color:
                          filter.tourSubjects.length === tourSubjectSelected.subjectList.length ? PRIMARY : DARK_GREY,
                        paddingLeft: '3px',
                      }}
                    >
                      <FormattedMessage id="all" />
                    </Typography>
                  }
                  labelPlacement="end"
                  onChange={(e, checked) => this.onSelectAllTourSubject(checked, tourSubjectSelected.subjectList)}
                  control={
                    <Checkbox
                      color="primary"
                      checked={filter.tourSubjects.length === tourSubjectSelected.subjectList.length}
                      style={{ padding: '4px' }}
                    />
                  }
                />

                {tourSubjectSelected.subjectList.map((item: some) => (
                  <FormControlLabel
                    key={item.id}
                    label={
                      <Typography
                        variant="caption"
                        style={{
                          color: filter.tourSubjects.indexOf(item.id) !== -1 ? PRIMARY : DARK_GREY,
                          paddingLeft: '3px',
                        }}
                      >
                        {item.name}
                      </Typography>
                    }
                    labelPlacement="end"
                    value={item.id}
                    style={{ marginRight: 0 }}
                    onChange={(e, value) => this.setTourSubject(value, tourSubjectSelected.subjectList, item.id)}
                    control={
                      <Checkbox
                        color="primary"
                        checked={filter.tourSubjects.indexOf(item.id) !== -1}
                        style={{ padding: '4px' }}
                      />
                    }
                  />
                ))}
              </div>
            </div>
          </BoxColumn>
        )}
        {generalInformation && (
          <>
            <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
              <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                <FormattedMessage id="tour.filter.activity" />
              </Typography>
              <BoxContent>
                {generalInformation.activityTags.map((item: some) => (
                  <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                    <CheckButton
                      active={filter.activityTags.indexOf(item.id) !== -1}
                      style={{ padding: '0px 10px' }}
                      onClick={() => this.setActivityTag(item.id)}
                    >
                      <Typography variant="body2">{item.name}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </BoxContent>
            </BoxColumn>

            <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
              <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                <FormattedMessage id="tour.filter.tourTypes" />
              </Typography>
              <BoxContent>
                {generalInformation.tourTypes.map((item: some) => (
                  <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                    <CheckButton
                      active={filter.tourTypes.indexOf(item.id) !== -1}
                      style={{ padding: '0px 10px' }}
                      onClick={() => this.setTourType(item.id)}
                    >
                      <Typography variant="body2">{item.name}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </BoxContent>
            </BoxColumn>

            <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
              <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                <FormattedMessage id="tour.filter.tourService" />
              </Typography>
              <BoxContent>
                {generalInformation.tourServices.map((item: some) => (
                  <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                    <CheckButton
                      active={filter.tourServices.indexOf(item.id) !== -1}
                      style={{ padding: '0px 10px' }}
                      onClick={() => this.setTourService(item.id)}
                    >
                      <Typography variant="body2">{item.name}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </BoxContent>
            </BoxColumn>

            <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
              <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                <FormattedMessage id="tour.filter.transport" />
              </Typography>
              <BoxContent>
                {generalInformation.transportations.map((item: some) => (
                  <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                    <CheckButton
                      onClick={() => this.setTransport(item.id)}
                      active={filter.transportations.indexOf(item.id) !== -1}
                      style={{ padding: '0px 10px' }}
                    >
                      <Typography variant="body2">{item.name}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </BoxContent>
            </BoxColumn>
          </>
        )}
      </div>
    );
  }
}

export default connect(mapState2Props)(TourResultFilter);
