import * as React from 'react';
import { some, ROUTES } from '../../../../constants';
import { Card, Typography } from '@material-ui/core';
import Link from '../../../common/components/Link';
import { TOUR_BOOK_PARAMS_NAMES, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../../common/constants';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { setBookingParams } from '../../../booking/redux/tourBookingReducer';
import Skeleton from '@material-ui/lab/Skeleton';
import { BLUE, PURPLE, GREEN, RED } from '../../../../colors';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { getScoreDescription, getHotDealColor } from '../../utils';
import IconLocation from '@material-ui/icons/LocationOn';
import IconFavorite from '@material-ui/icons/FavoriteBorder';
import { ReactComponent as CoinSvg } from '../../../../svg/coin.svg';
import { ReactComponent as IconHotDeal } from '../../../../svg/ic_hot_deal.svg';
import { ReactComponent as RedArrow } from '../../../../svg/ic_red_arrow.svg';
import { AppState } from '../../../../redux/reducers';
import ProgressiveImage from '../../../common/components/ProgressiveImage';

const HEIGHT = '333px';
const IMAG_HEIGHT = '150px';
const MenuTabStyle = {
  fontSize: '12px',
  lineHeight: '14px',
};

const MIN_DISCOUNT_SHOW = 0.05;

interface ITourResultItemProps extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  data: some;
  dispatch: Dispatch;
}

const TourResultItem: React.FunctionComponent<ITourResultItemProps> = props => {
  const { data, dispatch, intl, routerState } = props;
  const mainDestination = data.mainDestination;
  const activityTag = data.activityTag;

  const discountPrice =
    data.originPrice && data.originPrice > data.price ? (data.originPrice - data.price) / data.originPrice : 0;

  if (data.skeleton) {
    return (
      <Card style={{ height: HEIGHT }} elevation={2}>
        <Skeleton height={IMAG_HEIGHT} variant="rect" />
        <div style={{ padding: '8px' }}>
          <Skeleton variant="text" height="30px" />
          <Skeleton variant="text" />
          <div style={{ direction: 'rtl' }}>
            <Skeleton variant="text" width="80%" />
          </div>
        </div>
      </Card>
    );
  }

  return (
    <Link
      style={{ position: 'relative' }}
      to={{
        pathname: `${ROUTES.tour.tourDetail}`,
        search: `?${TOUR_BOOK_PARAMS_NAMES.tourId}=${data.id}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${false}`,
        state: {
          ...routerState,
          backableToSearch: routerState ? routerState.backableToSearch + 1 : 1,
        },
      }}
      onClick={() => dispatch(setBookingParams({ tourId: data.id, direct: false }))}
    >
      <Card style={{ height: HEIGHT, display: 'flex', flexDirection: 'column' }} elevation={2}>
        {!!discountPrice && discountPrice >= MIN_DISCOUNT_SHOW && (
          <div
            style={{
              position: 'absolute',
              top: '116px',
              left: '-4px',
              zIndex: 1,
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                position: 'relative',
                width: '68px',
                height: '24px',
                color: '#fff',
                background: RED,
              }}
            >
              <RedArrow style={{ position: 'absolute', bottom: '-5px', left: 0 }} />
              <Typography variant="subtitle2">
                -
                {intl.formatNumber(discountPrice, {
                  style: 'percent',
                })}
              </Typography>
            </div>
          </div>
        )}
        <div style={{ position: 'relative' }}>
          <ProgressiveImage
            src={data.imageURL}
            alt=""
            style={{ height: IMAG_HEIGHT, objectFit: 'cover', width: '100%', display: 'block' }}
          />
          {mainDestination && (
            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                background: 'linear-gradient(rgb(33,33,33) 0%,rgba(33,33,33,0) 100%)',
              }}
            >
              <div
                style={{
                  paddingTop: '4px',
                  padding: '8px',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <IconLocation style={{ color: '#fff', width: '24px' }} />
                <Typography style={{ ...MenuTabStyle, color: '#fff' }}>{mainDestination.name}</Typography>
              </div>
              <IconFavorite style={{ color: '#fff', marginRight: '12px', width: '20px' }} />
            </div>
          )}

          {activityTag && (
            <div
              style={{
                position: 'absolute',
                borderRadius: '2px',
                bottom: '10px',
                right: '8px',
                backgroundColor: getHotDealColor(activityTag.id),
              }}
            >
              <Typography style={{ ...MenuTabStyle, padding: '3px 12px', color: '#fff' }}>
                {activityTag.name}
              </Typography>
            </div>
          )}
        </div>
        <div style={{ padding: '8px', display: 'flex', flexDirection: 'column', flex: 1 }}>
          <div
            style={{
              minHeight: '40px',
            }}
          >
            <Typography
              variant="body1"
              style={{
                display: '-webkit-box',
                WebkitLineClamp: 2,
                WebkitBoxOrient: 'vertical',
                overflow: 'hidden',
              }}
            >
              {data.name}
            </Typography>
          </div>
          <div style={{ display: 'flex', alignItems: 'center', paddingTop: '5px' }}>
            <div
              style={{
                borderRadius: '2px',
                backgroundColor: BLUE,
                width: '32px',
                height: '20px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Typography variant="caption" style={{ color: '#fff' }}>
                <FormattedNumber value={data.expertRating} minimumFractionDigits={1} maximumFractionDigits={1} />
              </Typography>
            </div>
            <Typography style={{ ...MenuTabStyle, paddingLeft: '4px' }}>
              <span style={{ color: BLUE }}>
                <FormattedMessage id={getScoreDescription(data.expertRating)} />
              </span>
              {!!data.numberOfComments && (
                <span>
                  &nbsp;
                  <FormattedMessage id="tour.rateCount" values={{ rateCount: data.numberOfComments }} />
                </span>
              )}
              {!!data.numberBooking && (
                <span style={{ color: PURPLE }}>
                  &nbsp;
                  <FormattedMessage id="tour.bookCount" values={{ num: data.numberBooking }} />
                </span>
              )}
            </Typography>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
            }}
          >
            {!!discountPrice && (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2" color="textSecondary" style={{ textDecorationLine: 'line-through' }}>
                  <FormattedNumber value={data.originPrice} />
                  &nsbp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            )}
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <IconHotDeal />
              <Typography variant="body2" color="secondary" style={{ fontWeight: 500 }}>
                <FormattedNumber value={data.price} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <CoinSvg style={{ marginRight: '8px' }} />
              <Typography variant="caption" style={{ color: GREEN }}>
                {!!data.bonusPoint ? <FormattedNumber value={data.bonusPoint} /> : '0'}
                &nbsp;
                <FormattedMessage id="booking.point" />
              </Typography>
            </div>
            <Typography style={{ ...MenuTabStyle, color: BLUE }}>
              <FormattedMessage id="tour.canBookToday" />
            </Typography>
          </div>
        </div>
      </Card>
    </Link>
  );
};

const mapState2Props = (state: AppState) => {
  return {
    routerState: state.router.location.state,
  };
};

export default connect(mapState2Props)(injectIntl(TourResultItem));
