import { Container } from '@material-ui/core';
import * as React from 'react';
import { some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import TourBasicInfoBox from './TourBasicInfoBox';
import TourDetailBreadcrumbs from './TourDetailBreadcrumbs';
import TourDetailImageBox from './TourDetailImageBox';
import TourDetailTabs from './TourDetailTabs';

export interface ITourDetailTabletDesktopProps {
  tourData?: some;
}

class TourDetailTabletDesktop extends React.Component<ITourDetailTabletDesktopProps> {
  public render() {
    const { tourData } = this.props;
    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <div>
          <Header light={true} />
        </div>

        <div
          style={{
            flex: 1,
          }}
        >
          {!tourData ? (
            <LoadingIcon style={{ height: '320px' }} />
          ) : (
            <>
              <Container>
                <TourDetailBreadcrumbs />
                <TourBasicInfoBox tour={tourData} />
              </Container>
              <TourDetailImageBox tour={tourData} />
              <TourDetailTabs tour={tourData} />
            </>
          )}
        </div>

        <Footer />
      </PageWrapper>
    );
  }
}

export default TourDetailTabletDesktop;
