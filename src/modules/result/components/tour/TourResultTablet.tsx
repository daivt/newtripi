import { Container, IconButton } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import FilterList from '@material-ui/icons/FilterList';
import * as React from 'react';
import { connect } from 'react-redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import TourSearch from '../../../search/components/TourSearch';
import TourResultBox from './TourResultBox';
import TourResultBoxHeader from './TourResultBoxHeader';
import TourResultFilter from './TourResultFilter';
import TourResultFilterHeader from './TourResultFilterHeader';
import TourResultSortBox from './TourResultSortBox';

interface ITourResultDesktopProps extends ReturnType<typeof mapStateToProps> {
  num?: number;
  input?: string;
}

const TourResultTablet: React.FunctionComponent<ITourResultDesktopProps> = props => {
  const { result, generalInformation, filter, num, input } = props;
  const { locationResultData, termResultData } = result;
  const [showFilters, setShowFilters] = React.useState(false);

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <TourSearch home={false} />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex' }}>
          <div
            style={{
              width: '40px',
              overflow: 'visible',
              position: 'relative',
              zIndex: 100,
              minHeight: '100%',
              display: 'flex',
            }}
          >
            <div
              style={{
                flexShrink: 0,
                width: '300px',
                transform: showFilters ? 'translateX(0)' : 'translateX(calc(-100% + 40px))',
                backgroundColor: BACKGROUND,
                padding: '0 10px 20px 0',
                borderRight: `1px solid ${GREY}`,
                transition: 'all 300ms',
                minHeight: '100%',
              }}
            >
              <StickyDiv style={{ top: 65 }}>
                <div style={{ textAlign: 'end' }}>
                  {showFilters ? (
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShowFilters(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  ) : (
                    <IconButton
                      onClick={() => setShowFilters(true)}
                      style={{
                        margin: '10px 0',
                      }}
                    >
                      <FilterList style={{ color: DARK_GREY }} />
                    </IconButton>
                  )}
                </div>
                <div
                  style={{
                    opacity: showFilters ? 1 : 0.4,
                    position: 'relative',
                  }}
                >
                  <TourResultFilter
                    generalInformation={generalInformation}
                    locationResultData={locationResultData}
                    filter={filter}
                  />
                  {!showFilters && (
                    <div
                      style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                      }}
                    />
                  )}
                </div>
              </StickyDiv>
            </div>
          </div>
          <div style={{ padding: '22px 0', flex: 1, minHeight: '400px', marginLeft: '10px' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row-reverse',
              }}
            >
              <TourResultSortBox />
              {num && input && <TourResultBoxHeader style={{ flex: 1 }} input={input} num={num} />}
            </div>
            <div style={{ margin: '16px 0' }}>
              <TourResultFilterHeader generalInformation={generalInformation} filter={filter} />
            </div>
            <TourResultBox
              tours={
                locationResultData
                  ? locationResultData.tours
                  : termResultData && termResultData.data
                  ? termResultData.data.tours
                  : undefined
              }
            />
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}

export default connect(mapStateToProps)(TourResultTablet);
