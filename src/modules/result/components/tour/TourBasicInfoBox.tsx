import { Button, Grid, Popover, Typography } from '@material-ui/core';
import * as React from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { RED } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconHeart } from '../../../../svg/heart.svg';
import { ReactComponent as IconShare } from '../../../../svg/share.svg';
import { ReactComponent as ShareFacebook } from '../../../../svg/share_facebook.svg';
import { ReactComponent as ShareZalo } from '../../../../svg/share_zalo.svg';
import { BootstrapInput } from '../../../common/components/elements';
import PickTourBox from './PickTourBox';

const Box = styled.div`
  width: 472px;
  background: #ffffff;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 0px;
  padding: 15px;
`;

const Line = styled.div`
  display: flex;
  align-items: center;
`;

interface Props {
  tour: some;
  compact?: boolean;
  style?: React.CSSProperties;
}

const TourBasicInfoBox: React.FunctionComponent<Props> = props => {
  const { tour, compact } = props;
  const [anchorEl, setAnchorEl] = React.useState<Element | undefined>(undefined);
  const fullURL = window.location.href;
  const shareUrl = encodeURIComponent(fullURL);

  function handleShareHotel(event: React.MouseEvent<SVGSVGElement, MouseEvent>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(undefined);
  }

  const open = Boolean(anchorEl);
  const promotion = Math.round(((tour.originPrice - tour.price) / tour.originPrice) * 100);
  return (
    <div
      style={{
        ...props.style,
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
        <Line style={{ minHeight: '40px', alignItems: 'center' }}>
          {promotion >= 5 && (
            <div
              style={{
                background: RED,
                height: '32px',
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex',
                padding: '0px 8px',
                marginRight: '24px',
                borderRadius: '4px',
              }}
            >
              <Typography style={{ color: 'white' }}>-{promotion}%</Typography>
            </div>
          )}
          <Typography variant="h5" style={{ wordBreak: 'break-word' }}>
            {tour.name}
          </Typography>
        </Line>
        <Line style={{ height: '30px' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '64px',
            }}
          >
            <IconHeart style={{ cursor: 'pointer' }} />
            <IconShare style={{ cursor: 'pointer' }} onClick={handleShareHotel} />
            <Popover
              open={open}
              onClose={handleClose}
              anchorEl={anchorEl}
              style={{ zIndex: 100 }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <Box>
                <Grid container spacing={3}>
                  <Grid item xs={6} style={{ cursor: 'pointer' }}>
                    <a
                      href={`https://www.facebook.com/sharer/sharer.php?u=${shareUrl}`}
                      target="_blank"
                      rel="noopener noreferrer"
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        textDecoration: 'inherit',
                        color: 'inherit',
                      }}
                    >
                      <ShareFacebook style={{ paddingRight: '9px' }} />
                      &nbsp;
                      <Typography variant="body2">Share on Facebook</Typography>
                    </a>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}
                  >
                    <ShareZalo style={{ marginRight: '9px', height: '48px', width: 'auto' }} />
                    <Typography variant="body2">Share on Zalo</Typography>
                  </Grid>
                </Grid>
                <div style={{ marginBottom: '4px', marginTop: '15px' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="shareLink" />
                  </Typography>
                </div>
                <div style={{ marginRight: '20px', display: 'flex', alignItems: 'center' }}>
                  <BootstrapInput
                    value={fullURL}
                    inputProps={{
                      readOnly: true,
                    }}
                    style={{ width: '270px' }}
                  />
                  <CopyToClipboard text={fullURL}>
                    <Button
                      color="secondary"
                      variant="contained"
                      style={{ width: '150px', height: '36px', marginLeft: '16px' }}
                    >
                      <Typography variant="body2">
                        <FormattedMessage id="copyUrl" />
                      </Typography>
                    </Button>
                  </CopyToClipboard>
                </div>
              </Box>
            </Popover>
          </div>
        </Line>
      </div>
      <PickTourBox tour={tour} compact={compact} />
    </div>
  );
};

export default TourBasicInfoBox;
