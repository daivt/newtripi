import { MenuItem, Select, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { BootstrapInput } from '../../../common/components/elements';
import { TRAIN_SORT_BY } from '../../constants';
import { setInboundSortBy, setOutboundSortBy } from '../../redux/trainResultReducer';

const SORT_BY = [
  {
    id: 0,
    name: 'train.earliest',
    value: TRAIN_SORT_BY.earliest,
  },
  {
    id: 1,
    name: 'train.latest',
    value: TRAIN_SORT_BY.latest,
  },
  {
    id: 2,
    name: 'train.longest',
    value: TRAIN_SORT_BY.longest,
  },
  {
    id: 3,
    name: 'train.shortest',
    value: TRAIN_SORT_BY.shortest,
  },
];

function mapState2Props(state: AppState) {
  return {
    outboundSortBy: state.result.train.outboundSortBy,
    inboundSortBy: state.result.train.inboundSortBy,
  };
}

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  isInbound?: boolean;
}

const TrainResultFilter: React.FC<Props> = props => {
  const { dispatch, outboundSortBy, inboundSortBy, isInbound } = props;
  return (
    <div style={{ minWidth: '240px', display: 'flex', flexDirection: 'column' }}>
      <Typography variant="body2" style={{ paddingBottom: '2px' }}>
        <FormattedMessage id="rewards.sort" />
      </Typography>

      <Select
        value={isInbound ? inboundSortBy : outboundSortBy}
        onChange={e =>
          isInbound
            ? dispatch(setInboundSortBy(e.target.value as string))
            : dispatch(setOutboundSortBy(e.target.value as string))
        }
        fullWidth
        input={<BootstrapInput style={{ backgroundColor: '#fff' }} />}
      >
        {SORT_BY.map(one => (
          <MenuItem key={one.id} value={one.value}>
            <Typography variant="body2">
              <FormattedMessage id={one.name} />
            </Typography>
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

export default connect(mapState2Props)(TrainResultFilter);
