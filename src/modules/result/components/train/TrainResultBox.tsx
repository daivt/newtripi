import React, { useState, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { OneDirection, TrainTicketParams } from '../../../booking/redux/trainBookingReducer';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { TrainResultHeader } from './TrainResultHeader';
import { TrainResultItem } from './TrainResultItem';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { TRAIN_SORT_BY } from '../../constants';
import { trainEarliestCmp, trainLatestCmp, trainLongestCmp, trainShortestCmp } from '../../utils';

function mapState2Props(state: AppState) {
  return {
    trainData: state.result.train.trainData,
    loading: state.result.train.loading,
    outboundSortBy: state.result.train.outboundSortBy,
    inboundSortBy: state.result.train.inboundSortBy,
    fetchSignal: `${state.router.location.search}&${state.auth.auth}`,
  };
}

function getSortBy(sortBy: string) {
  return sortBy === TRAIN_SORT_BY.earliest
    ? trainEarliestCmp
    : sortBy === TRAIN_SORT_BY.latest
    ? trainLatestCmp
    : sortBy === TRAIN_SORT_BY.longest
    ? trainLongestCmp
    : trainShortestCmp;
}

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  trainBookingState: TrainTicketParams;
  onGetSeatByCarriage(oneDirection: OneDirection, resetTicket?: boolean, isInbound?: boolean): void;
}

const TrainResultBox: React.FC<Props> = props => {
  const {
    trainData,
    loading,
    trainBookingState,
    onGetSeatByCarriage,
    outboundSortBy,
    inboundSortBy,
  } = props;
  const [outbound, setOutbound] = useState<OneDirection>(trainBookingState.outbound);
  const [inbound, setInbound] = useState<OneDirection>(trainBookingState.inbound);
  const [confirmDialogType, setConfirmDialogType] = React.useState<
    'inbound' | 'outbound' | undefined
  >(undefined);

  useEffect(() => {
    setOutbound(trainBookingState.outbound);
    setInbound(trainBookingState.inbound);
  }, [trainBookingState.inbound, trainBookingState.outbound]);

  const onSelectCarriage = useCallback((oneDirection: OneDirection, isInbound?: boolean) => {
    if (isInbound) {
      setInbound(oneDirection);
    } else {
      setOutbound(oneDirection);
    }

    setConfirmDialogType(isInbound ? 'inbound' : 'outbound');
  }, []);

  const filteredOutboundTickets =
    trainData && trainData.departureTrains.length
      ? trainData.departureTrains.sort(getSortBy(outboundSortBy))
      : [];

  const filteredInboundTickets =
    trainData && trainData.returnTrains.length
      ? trainData.returnTrains.sort(getSortBy(inboundSortBy))
      : [];

  return (
    <div style={{ flex: 1, paddingBottom: '58px' }}>
      {!loading && trainData ? (
        <>
          {!filteredOutboundTickets.length && !filteredInboundTickets.length && (
            <div
              style={{
                display: 'flex',
                margin: '40px',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography variant="body1">
                <FormattedMessage id="search.noTrainResult" />
              </Typography>
            </div>
          )}
          {!!filteredOutboundTickets.length && (
            <>
              <TrainResultHeader train={filteredOutboundTickets[0]} />
              {filteredOutboundTickets.map((train: some) => (
                <TrainResultItem
                  key={train.Id}
                  isCurrent={outbound.train && outbound.train.Id === train.Id}
                  train={train}
                  onSelect={train =>
                    setOutbound(outbound.train && outbound.train.Id === train.Id ? {} : { train })
                  }
                  onGetSeat={car =>
                    trainBookingState.outbound.train &&
                    trainBookingState.outbound.train.doQueryDataId !== car.doQueryDataId
                      ? onSelectCarriage({ ...outbound, carriage: car })
                      : onGetSeatByCarriage({ ...outbound, carriage: car })
                  }
                />
              ))}
            </>
          )}

          {!!filteredInboundTickets.length && (
            <div style={{ marginTop: '32px' }}>
              <TrainResultHeader train={filteredInboundTickets[0]} isInbound={true} />
              {filteredInboundTickets.map((train: some) => (
                <TrainResultItem
                  key={train.Id}
                  isCurrent={inbound.train && inbound.train.Id === train.Id}
                  train={train}
                  onSelect={train =>
                    setInbound(inbound.train && inbound.train.Id === train.Id ? {} : { train })
                  }
                  onGetSeat={car =>
                    trainBookingState.inbound.train &&
                    trainBookingState.inbound.train.doQueryDataId !== car.doQueryDataId
                      ? onSelectCarriage({ ...inbound, carriage: car }, true)
                      : onGetSeatByCarriage({ ...inbound, carriage: car }, false, true)
                  }
                />
              ))}
            </div>
          )}
        </>
      ) : (
        <div
          style={{
            display: 'flex',
            margin: '40px',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      )}

      <ConfirmDialog
        header={
          <Typography variant="h6">
            <FormattedMessage id="train.selectAnotherTrain" />
          </Typography>
        }
        message={
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <IconWarning style={{ height: '120px' }} />
            <Typography variant="body1">
              <FormattedMessage id="train.selectAnotherTrainDescription" />
            </Typography>
          </div>
        }
        show={!!confirmDialogType}
        cancelMessageId="ignore"
        confirmMessageId="accept"
        onCancel={() => setConfirmDialogType(undefined)}
        onAccept={() => {
          setConfirmDialogType(undefined);
          onGetSeatByCarriage(
            confirmDialogType === 'inbound' ? inbound : outbound,
            true,
            confirmDialogType === 'inbound',
          );
        }}
      />
    </div>
  );
};

export default connect(mapState2Props)(TrainResultBox);
