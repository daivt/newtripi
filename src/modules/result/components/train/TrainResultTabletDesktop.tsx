import { Button, ButtonBase, Container, Dialog, DialogActions, DialogContent, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../colors';
import { some, TABLET_WIDTH } from '../../../../constants';
import { ReactComponent as IconQuestion } from '../../../../svg/question.svg';
import TrainBreadcrumbs from '../../../booking/components/train/TrainBreadcrumbs';
import { OneDirection, SeatingInfo, TrainTicketParams } from '../../../booking/redux/trainBookingReducer';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import MessageDialog from '../../../common/components/MessageDialog';
import BuyTrainTicketPolicies from '../../../home/pages/BuyTrainTicketPolicies';
import TrainSearch from '../../../search/components/TrainSearch';
import SelectSeatPopup from './selectSeat/SelectSeatPopup';
import TrainResultBox from './TrainResultBox';
import TrainResultSideBox from './TrainResultSideBox';

interface Props {
  open: boolean;
  message: string;
  errorMessage: string;
  seatData?: some;
  selectTicketFocus: 'inbound' | 'outbound';
  trainBookingState: TrainTicketParams;
  onCloseSelectSeat(): void;
  onGetSeatByCarriage(oneDirection: OneDirection, resetTicket?: boolean, isInbound?: boolean): void;
  updateSeatingInfo(seatingInfo: SeatingInfo): void;
  goToBooking(): void;
  onCloseErrorDialog(): void;
}

const TrainResultTabletDesktop: React.FunctionComponent<Props> = props => {
  const [openDialog, setOpenDialog] = React.useState(false);

  const {
    open,
    message,
    errorMessage,
    seatData,
    selectTicketFocus,
    trainBookingState,
    onGetSeatByCarriage,
    onCloseSelectSeat,
    updateSeatingInfo,
    goToBooking,
    onCloseErrorDialog,
  } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <TrainSearch />
      <Container style={{ flex: 1 }}>
        <TrainBreadcrumbs step={2} />
        <div style={{ display: 'flex' }}>
          <TrainResultBox trainBookingState={trainBookingState} onGetSeatByCarriage={onGetSeatByCarriage} />
          <div style={{ width: '270px', marginLeft: '30px' }}>
            <StickyDiv style={{ top: 80 }}>
              <TrainResultSideBox trainBookingState={trainBookingState} goToBooking={goToBooking} />
              <ButtonBase onClick={() => setOpenDialog(true)} style={{ color: BLUE, paddingTop: 10 }}>
                <IconQuestion />
                <Typography variant="body2">
                  <FormattedMessage id="booking.train.guideBuyTrainTickets" />
                </Typography>
              </ButtonBase>
              <Dialog open={openDialog} onClose={() => setOpenDialog(false)} maxWidth="lg" fullWidth>
                <DialogContent>
                  <BuyTrainTicketPolicies />
                </DialogContent>
                <DialogActions style={{ justifyContent: 'center' }}>
                  <Button variant="contained" onClick={() => setOpenDialog(false)} color="secondary">
                    <FormattedMessage id="accept" />
                  </Button>
                </DialogActions>
              </Dialog>
            </StickyDiv>
          </div>
        </div>
      </Container>

      <SelectSeatPopup
        open={open}
        message={message}
        seatData={seatData}
        seatingInfo={
          selectTicketFocus === 'outbound' ? trainBookingState.outboundSeating : trainBookingState.inboundSeating
        }
        onClose={onCloseSelectSeat}
        updateSeatingInfo={updateSeatingInfo}
      />

      <MessageDialog
        show={!!errorMessage}
        onClose={onCloseErrorDialog}
        message={
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Typography variant="body1">{errorMessage}</Typography>
          </div>
        }
      />

      <Footer />
    </PageWrapper>
  );
};

export default TrainResultTabletDesktop;
