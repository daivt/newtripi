import { Card, Typography } from '@material-ui/core';
import { sumBy } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';
import { TrainTicketParams } from '../../../booking/redux/trainBookingReducer';
import LoadingButton from '../../../common/components/LoadingButton';
import { TrainResultItemSideBox } from './TrainResultItemSideBox';

interface Props extends ReturnType<typeof mapStateToProps> {
  trainBookingState: TrainTicketParams;
  goToBooking(): void;
}

const TrainResultSideBox: React.FC<Props> = props => {
  const { trainBookingState, goToBooking, loading } = props;

  const totalPrice =
    (trainBookingState.outbound.train
      ? (sumBy(trainBookingState.outboundSeating.adults, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.outboundSeating.children, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.outboundSeating.seniors, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.outboundSeating.students, 'ticketPrice') || 0)
      : 0) +
    (trainBookingState.inbound.train
      ? (sumBy(trainBookingState.inboundSeating.adults, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.inboundSeating.children, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.inboundSeating.seniors, 'ticketPrice') || 0) +
        (sumBy(trainBookingState.inboundSeating.students, 'ticketPrice') || 0)
      : 0);

  return (
    <Card elevation={3} style={{ display: 'flex', flexDirection: 'column', padding: '0 16px' }}>
      <Typography
        variant="h5"
        style={{
          padding: '20px 0 10px',
        }}
      >
        <FormattedMessage id="train.ticketDetail" />
      </Typography>
      {totalPrice > 0 ? (
        <>
          {trainBookingState.outbound.train && (
            <TrainResultItemSideBox
              train={trainBookingState.outbound.train}
              seatingInfo={trainBookingState.outboundSeating}
              showGeneralInfo
            />
          )}

          {trainBookingState.inbound.train && (
            <TrainResultItemSideBox
              isInbound
              train={trainBookingState.inbound.train}
              seatingInfo={trainBookingState.inboundSeating}
              showGeneralInfo
            />
          )}

          <div style={{ display: 'flex', justifyContent: 'space-between', padding: '12px 0' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.totalPay" />
            </Typography>

            <Typography variant="body2" color="secondary">
              <FormattedNumber value={totalPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </div>

          <LoadingButton
            variant="contained"
            color="secondary"
            style={{ margin: '12px 8px 24px' }}
            size="large"
            onClick={goToBooking}
            loading={loading}
          >
            <FormattedMessage id="continue" />
          </LoadingButton>
        </>
      ) : (
        <Typography variant="body2" color="textSecondary" style={{ padding: '16px 0' }}>
          <FormattedMessage id="train.noChooseTicket" />
        </Typography>
      )}
    </Card>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    loading: state.booking.train.loading,
  };
};
export default connect(mapStateToProps)(TrainResultSideBox);
