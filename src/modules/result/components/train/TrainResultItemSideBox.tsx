import React from 'react';
import { SeatingInfo } from '../../../booking/redux/trainBookingReducer';
import { some } from '../../../../constants';
import styled from 'styled-components';
import { Typography } from '@material-ui/core';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ReactComponent as IconTrain } from '../../../../svg/Train.svg';
import { getCarriageDetail } from '../../utils';
import { GREY } from '../../../../colors';
import { TravellersInfo } from '../../../common/models/train';

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
`;
interface Props {
  isInbound?: boolean;
  train: some;
  seatingInfo: SeatingInfo;
  travellersInfo?: TravellersInfo;
  isReview?: boolean;
  showGeneralInfo?: boolean;
}

function renderTravellerInfo(
  travellerType: string,
  travellers: some[],
  train: some,
  travellersInfo?: some[],
  isReview?: boolean,
) {
  return travellers.map((obj: some, index: number) => {
    const carriage = getCarriageDetail(obj.carriageTempId, train.carriages);
    if (!carriage) {
      return (
        <Column key={index} style={{ borderBottom: `0.5px solid ${GREY}`, padding: '8px 0' }}>
          <Typography variant="body2">
            <FormattedMessage id={travellerType} />
            &nbsp;
            {travellers.length > 1 ? index + 1 : ''}
          </Typography>

          <Typography variant="caption" color="textSecondary">
            <FormattedMessage id="train.noChooseSeat" />
          </Typography>
        </Column>
      );
    }

    return (
      <Row key={obj.seatTempID} style={{ borderBottom: `0.5px solid ${GREY}`, padding: '8px 0' }}>
        <Column>
          {isReview && travellersInfo ? (
            <>
              <Typography variant="body2">
                {`${travellersInfo[index].familyName} ${travellersInfo[index].givenName}`}
              </Typography>
              <Typography variant="caption">
                <FormattedMessage id="train.seat" values={{ num: obj.seatNumber }} />
                &nbsp;-&nbsp;
                <FormattedMessage id="train.carNum" values={{ num: carriage.carriageNo }} />
              </Typography>
            </>
          ) : !isReview ? (
            <>
              <Typography variant="body2">
                <FormattedMessage id={travellerType} />
                &nbsp;
                {travellers.length > 1 ? index + 1 : ''}
              </Typography>
              <Typography variant="caption">
                <FormattedMessage id="train.seat" values={{ num: obj.seatNumber }} />
                &nbsp;-&nbsp;
                <FormattedMessage id="train.carNum" values={{ num: carriage.carriageNo }} />
              </Typography>
            </>
          ) : (
            <>
              <Typography variant="body2">
                <FormattedMessage id="train.seat" values={{ num: obj.seatNumber }} />
                &nbsp;-&nbsp;
                <FormattedMessage id="train.carNum" values={{ num: carriage.carriageNo }} />
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {carriage.detailCarriage}
              </Typography>
            </>
          )}
        </Column>
        <Typography variant="body2">
          <FormattedNumber value={obj.ticketPrice} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Row>
    );
  });
}

export const TrainResultItemSideBox: React.FC<Props> = props => {
  const { isInbound, train, seatingInfo, travellersInfo, isReview, showGeneralInfo } = props;

  return (
    <>
      {!!showGeneralInfo && (
        <Row style={{ padding: '16px 0' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id={isInbound ? 'train.inbound' : 'train.outbound'} />
          </Typography>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            {train.tripCode}
            <IconTrain style={{ marginLeft: '8px' }} />
          </div>
        </Row>
      )}

      {!!seatingInfo.adults.length &&
        renderTravellerInfo(
          'search.trainTravellersInfo.adult',
          seatingInfo.adults,
          train,
          travellersInfo ? travellersInfo.adults : undefined,
          isReview,
        )}
      {!!seatingInfo.students.length &&
        renderTravellerInfo(
          'search.trainTravellersInfo.student',
          seatingInfo.students,
          train,
          travellersInfo ? travellersInfo.students : undefined,
          isReview,
        )}
      {!!seatingInfo.children.length &&
        renderTravellerInfo(
          'search.trainTravellersInfo.children',
          seatingInfo.children,
          train,
          travellersInfo ? travellersInfo.childrens : undefined,
          isReview,
        )}
      {!!seatingInfo.seniors.length &&
        renderTravellerInfo(
          'search.trainTravellersInfo.senior',
          seatingInfo.seniors,
          train,
          travellersInfo ? travellersInfo.seniors : undefined,
          isReview,
        )}
    </>
  );
};
