import React from 'react';
import { some } from '../../../../constants';
import { Typography } from '@material-ui/core';
import { ReactComponent as IconArrow } from '../../../../svg/ic_arrow.svg';
import { FormattedMessage, FormattedDate } from 'react-intl';
import moment from 'moment';
import TrainResultFilter from './TrainResultFilter';

interface Props {
  train: some;
  isInbound?: boolean;
}

export const TrainResultHeader: React.FC<Props> = props => {
  const { train, isInbound } = props;
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '22px' }}>
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography variant="h5">{train.departureStationName}</Typography>
          <IconArrow style={{ margin: '0 12px' }} />
          <Typography variant="h5">{train.arrivedStationName}</Typography>
        </div>

        <Typography variant="body1" color="textSecondary">
          <FormattedMessage
            id="flightHunt.departDate"
            values={{
              num: (
                <>
                  &nbsp;
                  <FormattedDate value={moment(train.departureDate).toDate()} weekday="short" />,
                  &nbsp;
                  {moment(train.departureDate).format('L')}
                </>
              ),
            }}
          />
        </Typography>
      </div>

      <TrainResultFilter isInbound={isInbound} />
    </div>
  );
};
