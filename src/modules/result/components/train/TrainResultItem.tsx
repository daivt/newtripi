import { Button, Collapse, List, ListItem, Typography, IconButton } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import ArrowForwardIosOutlinedIcon from '@material-ui/icons/ArrowForwardIosOutlined';
import { GREY, HOVER_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconArrow } from '../../../../svg/ic_arrow.svg';
import { ReactComponent as TrainPartSvg } from '../../../../svg/ic_train_part.svg';
import { ReactComponent as TrainSvg } from '../../../../svg/Train.svg';
import { subtitle3Styles } from '../../../common/components/elements';
import { durationMillisecondToHour } from '../../../common/utils';

const TrainItem = styled.div<{ isCurrent: boolean | undefined }>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${props => (props.isCurrent ? HOVER_GREY : '#ffffff')};
  border-bottom: ${props => (!props.isCurrent ? `1px solid ${GREY}` : undefined)};
  padding: 14px 20px 10px 12px;
  box-shadow: ${props => (!props.isCurrent ? '0px 1px 1px rgba(0, 0, 0, 0.2)' : undefined)};
`;

interface Props {
  train: some;
  isCurrent?: boolean;
  onSelect(train: some): void;
  onGetSeat(car: some): void;
}

export const TrainResultItem: React.FC<Props> = props => {
  const { train, onSelect, onGetSeat, isCurrent } = props;
  return (
    <>
      <TrainItem isCurrent={isCurrent}>
        <div style={{ display: 'flex', paddingLeft: '8px', minWidth: '110px' }}>
          <TrainSvg style={{ width: '24px', height: '24px', marginRight: '3px' }} />
          <Typography variant="h5">{train.tripCode}</Typography>
        </div>

        <div style={{ display: 'flex', justifyContent: 'space-between', minWidth: '400px' }}>
          <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
            <div style={{ minWidth: '150px', display: 'flex', alignItems: 'flex-end' }}>
              <Typography variant="h5">{moment(train.departureDate).format('HH:mm')}</Typography>
              <Typography variant="body2" color="textSecondary">
                &nbsp;
                {moment(train.departureDate).format('L')}
              </Typography>
            </div>
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="train.station" values={{ name: train.departureStationName }} />
            </Typography>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
            <IconArrow />
            <Typography variant="body2" color="textSecondary">
              {durationMillisecondToHour(train.arriveDate - train.departureDate)}
            </Typography>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
            <div style={{ minWidth: '150px', display: 'flex', alignItems: 'flex-end' }}>
              <Typography variant="h5">{moment(train.arriveDate).format('HH:mm')}</Typography>
              <Typography variant="body2" color="textSecondary">
                &nbsp;
                {moment(train.arriveDate).format('L')}
              </Typography>
            </div>
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="train.station" values={{ name: train.arrivedStationName }} />
            </Typography>
          </div>
        </div>

        <Button
          variant="contained"
          color="secondary"
          size="large"
          style={{ width: '115px' }}
          onClick={() => onSelect(train)}
        >
          <Typography variant="button">
            <FormattedMessage id={isCurrent ? 'flightHunt.choosed' : 'select'} />
          </Typography>
        </Button>
      </TrainItem>

      <Collapse in={isCurrent} unmountOnExit>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            paddingTop: '4px',
            paddingBottom: '16px',
            background: HOVER_GREY,
          }}
        >
          <List style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography style={{ ...subtitle3Styles, paddingBottom: '8px' }}>
              <FormattedMessage id="train.chooseCar" />
            </Typography>

            {train.carriages.map((car: some, index: number) => (
              <ListItem
                key={car.carriageTempId}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  padding: '6px 20px',
                  justifyContent: 'space-between',
                  background: '#fff',
                  border: `0.5px solid ${GREY}`,
                  borderRadius:
                    train.carriages.length === 1
                      ? '4px'
                      : index === 0
                      ? '4px 4px 0px 0px'
                      : index === train.carriages.length - 1
                      ? '0px 0px 4px 4px'
                      : undefined,
                  minWidth: '470px',
                }}
                button
                onClick={() => onGetSeat(car)}
              >
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <TrainPartSvg />
                  <Typography variant="subtitle2" style={{ paddingLeft: '12px' }}>
                    <FormattedMessage
                      id="train.carNum"
                      values={{
                        num: car.carriageNo,
                      }}
                    />
                  </Typography>
                  &nbsp;-&nbsp;
                  <Typography variant="body2" color="textSecondary">
                    {car.detailCarriage}
                  </Typography>
                </div>
                <IconButton>
                  <ArrowForwardIosOutlinedIcon style={{ color: GREY }} fontSize="small" />
                </IconButton>
              </ListItem>
            ))}
          </List>
        </div>
      </Collapse>
    </>
  );
};
