import { Tooltip, Typography } from '@material-ui/core';
import Zoom from '@material-ui/core/Zoom';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { DARK_GREY, GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconSeat } from '../../../../../svg/ic_train_seat_available.svg';
import { SeatingInfo } from '../../../../booking/redux/trainBookingReducer';
import { TRAIN_SEAT_TYPE } from '../../../constants';
import { getSeatColor } from '../../../utils';

const ItemSeat = styled.div`
  margin: 6px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  cursor: pointer;
`;

interface Props {
  seatData: some[];
  seatingInfo: SeatingInfo;
  hasFloor?: boolean;
  onSelectSeat(seat: some): void;
}

export const Type0SeatMap: React.FC<Props> = props => {
  const { seatData, onSelectSeat, seatingInfo, hasFloor } = props;

  return (
    <div
      style={{
        border: `1px solid ${GREY}`,
        borderRadius: '8px',
        padding: '2px',
      }}
    >
      {seatData &&
        seatData.map((seats: some, rowIndex: number) => (
          <div
            key={rowIndex}
            style={{
              display: 'flex',
            }}
          >
            {seats.filter((obj: some) => obj && !obj.seatTempID).length !== seats.length ? (
              seats.map((one: some, seatIndex: number) => {
                const isHardSeat = one.seatType === 'NC' || one.seatType === 'NCL';
                const isOpposite = isHardSeat
                  ? seatIndex && seats[seatIndex - 1].carriageTempId
                  : seatIndex > seats.length / 2;

                return one && one.seatTempID ? (
                  <Tooltip
                    key={one.seatTempID}
                    TransitionComponent={Zoom}
                    title={
                      <span>
                        <FormattedNumber value={one.ticketPrice} />
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </span>
                    }
                    placement="top"
                  >
                    <ItemSeat onClick={() => onSelectSeat(one)}>
                      <IconSeat
                        style={{
                          fill: getSeatColor(one, seatingInfo),
                          stroke: DARK_GREY,
                          flexShrink: 0,
                          transform: isOpposite || hasFloor ? 'rotate(180deg)' : undefined,
                        }}
                        className="svgFillAll"
                      />
                      <Typography
                        variant="caption"
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          width: '100%',
                          height: '100%',
                          display: 'flex',
                          justifyContent: isOpposite || hasFloor ? 'flex-start' : 'flex-end',
                          alignItems: 'center',
                          paddingRight: isOpposite || hasFloor ? undefined : '4px',
                          paddingLeft: isOpposite || hasFloor ? '4px' : undefined,
                        }}
                      >
                        {one.seatNumber}
                      </Typography>
                    </ItemSeat>
                  </Tooltip>
                ) : one ? (
                  one.vnpayType === TRAIN_SEAT_TYPE.BULK_HEAD ? (
                    <div key={seatIndex} style={{ border: `2px solid ${GREY}`, margin: '0 3px' }} />
                  ) : one.vnpayType === TRAIN_SEAT_TYPE.NON_BULK_HEAD ? (
                    <div key={seatIndex} style={{ margin: '0 5px' }} />
                  ) : (
                    <div key={seatIndex} style={{ width: '32px', height: '32px', margin: '6px' }} />
                  )
                ) : (
                  <div style={{ width: '32px', height: '32px', margin: '6px' }} />
                );
              })
            ) : (
              <div style={{ margin: '24px 0' }} />
            )}
          </div>
        ))}
    </div>
  );
};
