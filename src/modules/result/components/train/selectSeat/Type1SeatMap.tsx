import React from 'react';
import { some } from '../../../../../constants';
import { GREY, DARK_GREY } from '../../../../../colors';
import { Typography, Tooltip } from '@material-ui/core';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ReactComponent as IconSeat } from '../../../../../svg/ic_train_seat_bed_available.svg';
import Zoom from '@material-ui/core/Zoom';
import { getSeatColor } from '../../../utils';
import { SeatingInfo } from '../../../../booking/redux/trainBookingReducer';
import styled from 'styled-components';

const ItemSeat = styled.div`
  margin: 0 6px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  cursor: pointer;
`;

interface Props {
  col: number;
  seatData: some[];
  seatingInfo: SeatingInfo;
  onSelectSeat(seat: some): void;
}

export const Type1SeatMap: React.FC<Props> = props => {
  const { col, seatData, seatingInfo, onSelectSeat } = props;
  return (
    <div
      style={{
        border: `1px solid ${GREY}`,
        borderRadius: '8px',
        padding: '2px',
      }}
    >
      <div style={{ display: 'flex', marginTop: '40px', marginBottom: '8px' }}>
        <div
          style={{
            margin: '6px 24px 6px 0',
            width: '100px',
          }}
        />
        {Array(col)
          .fill(0)
          .map((_, index: number) => (
            <div
              key={index}
              style={{
                display: 'flex',
                justifyContent: 'center',
                width: '96px',
              }}
            >
              <FormattedMessage id="train.sectionNumber" values={{ num: index + 1 }} />
            </div>
          ))}
      </div>

      {seatData &&
        [...seatData].reverse().map((seats: some, floorIndex: number) => (
          <div
            key={floorIndex}
            style={{
              display: 'flex',
            }}
          >
            <Typography
              variant="body2"
              style={{
                margin: '6px 24px 6px 0',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                width: '100px',
              }}
            >
              <FormattedMessage id="train.floor" values={{ num: seatData.length - floorIndex }} />
            </Typography>
            <div style={{ border: `2px solid ${DARK_GREY}` }} />
            {seats.map((one: some, index: number) => {
              return one && one.seatTempID ? (
                <Tooltip
                  key={one.seatTempID}
                  TransitionComponent={Zoom}
                  title={
                    <span>
                      <FormattedNumber value={one.ticketPrice} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </span>
                  }
                  placement="top"
                >
                  <ItemSeat onClick={() => onSelectSeat(one)}>
                    <IconSeat
                      style={{
                        fill: getSeatColor(one, seatingInfo),
                        stroke: DARK_GREY,
                        flexShrink: 0,
                      }}
                      className="svgFillAll"
                    />
                    <Typography
                      variant="caption"
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      {one.seatNumber}
                    </Typography>
                  </ItemSeat>
                </Tooltip>
              ) : (
                one && <div key={index} style={{ border: `4px solid ${DARK_GREY}` }} />
              );
            })}
          </div>
        ))}
    </div>
  );
};
