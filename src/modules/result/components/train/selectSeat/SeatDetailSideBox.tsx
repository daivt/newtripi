import { Button, Card, RadioGroup, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { some } from '../../../../../constants';
import { SeatingInfo } from '../../../../booking/redux/trainBookingReducer';
import { TravellerOption } from './TravellerOption';
import { TravellerSelectedType } from './SelectSeatPopup';

interface Props {
  travellerSelected: some;
  seatingInfo: SeatingInfo;
  onSelectTraveller(data: { type: TravellerSelectedType; index: number }): void;
  onUpdateSeatingInfo(): void;
}

const SeatDetailSideBox: React.FC<Props> = props => {
  const { seatingInfo, travellerSelected, onSelectTraveller, onUpdateSeatingInfo } = props;

  return (
    <Card
      elevation={3}
      style={{
        width: '270px',
        display: 'flex',
        flexDirection: 'column',
        flexShrink: 0,
      }}
    >
      <div style={{ padding: '20px 28px 20px 10px' }}>
        <Typography variant="h6">
          <FormattedMessage id="train.seatDetail" />
        </Typography>
      </div>
      <div style={{ flex: 1 }}>
        <RadioGroup radioGroup="traveller">
          <TravellerOption
            travellerSelected={travellerSelected}
            travellers={seatingInfo.adults}
            travellerType="adult"
            onSelectTraveller={data => onSelectTraveller(data)}
          />
          <TravellerOption
            travellerSelected={travellerSelected}
            travellers={seatingInfo.children}
            travellerType="children"
            onSelectTraveller={data => onSelectTraveller(data)}
          />
          <TravellerOption
            travellerSelected={travellerSelected}
            travellers={seatingInfo.students}
            travellerType="student"
            onSelectTraveller={data => onSelectTraveller(data)}
          />
          <TravellerOption
            travellerSelected={travellerSelected}
            travellers={seatingInfo.seniors}
            travellerType="senior"
            onSelectTraveller={data => onSelectTraveller(data)}
          />
        </RadioGroup>
      </div>

      <div
        style={{
          boxShadow: '0px -2px 4px rgba(0, 0, 0, 0.2)',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: '60px',
          padding: '16px',
        }}
      >
        <Button
          variant="contained"
          size="large"
          style={{ width: '220px' }}
          color="secondary"
          onClick={onUpdateSeatingInfo}
        >
          <FormattedMessage id="apply" />
        </Button>
      </div>
    </Card>
  );
};

export default connect()(SeatDetailSideBox);
