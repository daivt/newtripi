import { Dialog, IconButton, Typography } from '@material-ui/core';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { DARK_GREY, GREY } from '../../../../../colors';
import { DESKTOP_WIDTH, some } from '../../../../../constants';
import { Type0SeatMap } from './Type0SeatMap';
import { Type1SeatMap } from './Type1SeatMap';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import SeatDetailSideBox from './SeatDetailSideBox';
import { connect } from 'react-redux';
import { AppState } from '../../../../../redux/reducers';
import { SeatingInfo } from '../../../../booking/redux/trainBookingReducer';
import { isEmpty } from 'lodash';
import { checkSeatSelected } from '../../../utils';
import styled from 'styled-components';

const TrainBox = styled.div`
  margin: 12px 0;
  display: flex;
  align-items: center;
  width: 100%;
`;

const SeatHeader = [
  {
    color: '#fff',
    text: 'train.slot.empty',
  },
  {
    color: GREY,
    text: 'train.slot.booked',
  },
  { color: '#B2DFDB', text: 'train.slot.pending' },
  { color: '#BA68C8', text: 'train.slot.other' },
];

export type TravellerSelectedType = 'adult' | 'children' | 'student' | 'senior';

function mapStateToProps(state: AppState) {
  return {
    adultCount: state.search.train.travellersInfo.adultCount,
    childCount: state.search.train.travellersInfo.childCount,
    studentCount: state.search.train.travellersInfo.studentCount,
    seniorCount: state.search.train.travellersInfo.seniorCount,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  open: boolean;
  message: string;
  seatData?: some;
  seatingInfo: SeatingInfo;
  onClose(): void;
  updateSeatingInfo(seatingInfo: SeatingInfo): void;
}

const SelectSeatPopup: React.FC<Props> = props => {
  const { open, seatData, message, seatingInfo, updateSeatingInfo } = props;

  const [tempSeatingInfo, setTempSeatingInfo] = React.useState<SeatingInfo>(seatingInfo);
  const [travellerSelected, setTravellerSelected] = React.useState<{
    type: TravellerSelectedType;
    index: number;
  }>({ type: 'adult', index: 0 });

  React.useEffect(() => {
    setTempSeatingInfo(seatingInfo);
    setTravellerSelected({
      type: seatingInfo.adults.length
        ? 'adult'
        : seatingInfo.children.length
        ? 'children'
        : seatingInfo.students.length
        ? 'student'
        : 'senior',
      index: 0,
    });
  }, [open, seatingInfo]);

  const onSelectSeat = (seat: some) => {
    const seatIndex = travellerSelected.index;

    if (travellerSelected.type === 'adult') {
      const seatingInformation: SeatingInfo = {
        ...tempSeatingInfo,
        adults: [
          ...tempSeatingInfo.adults.slice(0, seatIndex),
          isEmpty(tempSeatingInfo.adults[seatIndex]) ||
          tempSeatingInfo.adults[seatIndex].ticketId !== seat.ticketId
            ? seat
            : {},
          ...tempSeatingInfo.adults.slice(seatIndex + 1),
        ],
      };

      if (checkSeatSelected(seat, seatingInformation)) {
        setTempSeatingInfo(seatingInformation);
      }
    } else if (travellerSelected.type === 'children') {
      const seatingInformation: SeatingInfo = {
        ...tempSeatingInfo,
        children: [
          ...tempSeatingInfo.children.slice(0, seatIndex),
          isEmpty(tempSeatingInfo.children[seatIndex]) ||
          tempSeatingInfo.children[seatIndex].ticketId !== seat.ticketId
            ? seat
            : {},
          ...tempSeatingInfo.children.slice(seatIndex + 1),
        ],
      };
      if (checkSeatSelected(seat, seatingInformation)) {
        setTempSeatingInfo(seatingInformation);
      }
    } else if (travellerSelected.type === 'student') {
      const seatingInformation: SeatingInfo = {
        ...tempSeatingInfo,
        students: [
          ...tempSeatingInfo.students.slice(0, seatIndex),
          isEmpty(tempSeatingInfo.students[seatIndex]) ||
          tempSeatingInfo.students[seatIndex].ticketId !== seat.ticketId
            ? seat
            : {},
          ...tempSeatingInfo.students.slice(seatIndex + 1),
        ],
      };
      if (checkSeatSelected(seat, seatingInformation)) {
        setTempSeatingInfo(seatingInformation);
      }
    } else if (travellerSelected.type === 'senior') {
      const seatingInformation: SeatingInfo = {
        ...tempSeatingInfo,
        seniors: [
          ...tempSeatingInfo.seniors.slice(0, seatIndex),
          isEmpty(tempSeatingInfo.seniors[seatIndex]) ||
          tempSeatingInfo.seniors[seatIndex].ticketId !== seat.ticketId
            ? seat
            : {},
          ...tempSeatingInfo.seniors.slice(seatIndex + 1),
        ],
      };
      if (checkSeatSelected(seat, seatingInformation)) {
        setTempSeatingInfo(seatingInformation);
      }
    }
  };

  return (
    <Dialog
      fullScreen
      open={open}
      PaperProps={{
        style: {
          backgroundColor: 'transparent',
        },
      }}
    >
      <div
        style={{
          display: 'flex',
          minHeight: '64px',
          justifyContent: 'center',
          backgroundColor: '#000',
          opacity: 0.8,
          alignItems: 'center',
          minWidth: DESKTOP_WIDTH,
        }}
      >
        <div style={{ minWidth: DESKTOP_WIDTH, display: 'flex', justifyContent: 'flex-end' }}>
          <IconButton style={{ color: '#fff' }} onClick={() => props.onClose()}>
            <CloseOutlinedIcon />
          </IconButton>
        </div>
      </div>

      <div
        style={{
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          background: '#fff',
          minWidth: DESKTOP_WIDTH,
        }}
      >
        <div style={{ minWidth: DESKTOP_WIDTH }}>
          {seatData ? (
            <div style={{ padding: '16px 0', display: 'flex', margin: '0 12px' }}>
              <div style={{ marginRight: '30px', flex: 1, overflow: 'hidden' }}>
                <Typography variant="h5">
                  <FormattedMessage id="train.chooseSeat" />
                </Typography>

                <div style={{ display: 'flex', alignItems: 'center', paddingTop: '8px' }}>
                  <Typography variant="subtitle2">
                    <FormattedMessage
                      id="train.carNum"
                      values={{
                        num: seatData.seatArrived.carriage.carriageNo,
                      }}
                    />
                  </Typography>
                  &nbsp;-&nbsp;
                  <Typography variant="body2" color="textSecondary">
                    {seatData.seatArrived.carriage.detailCarriage}
                  </Typography>
                </div>

                <div style={{ display: 'flex', padding: '16px 0' }}>
                  {SeatHeader.map((item, index: number) => (
                    <div
                      key={index}
                      style={{ display: 'flex', minWidth: '170px', paddingRight: '16px' }}
                    >
                      <div
                        style={{
                          width: '24px',
                          height: '24px',
                          borderRadius: '4px',
                          backgroundColor: item.color,
                          border: `1px solid ${DARK_GREY}`,
                          marginRight: '8px',
                        }}
                      />
                      <FormattedMessage id={item.text} />
                    </div>
                  ))}
                </div>

                {seatData.seatArrived.mapCheck === 1 ? (
                  <Type1SeatMap
                    seatData={seatData.seatArrived.seatsIn1stFloor}
                    col={Math.floor(seatData.seatArrived.col / 3)}
                    seatingInfo={tempSeatingInfo}
                    onSelectSeat={seat => onSelectSeat(seat)}
                  />
                ) : seatData.seatArrived.seatsIn2ndFloor || seatData.seatArrived.seatsIn3rdFloor ? (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      flexDirection: 'column',
                      overflow: 'scroll',
                    }}
                  >
                    {seatData.seatArrived.seatsIn3rdFloor && (
                      <TrainBox>
                        <Typography variant="subtitle2" style={{ marginRight: '24px' }}>
                          <FormattedMessage id="train.floor" values={{ num: 3 }} />
                        </Typography>

                        <Type0SeatMap
                          seatingInfo={tempSeatingInfo}
                          hasFloor
                          seatData={seatData.seatArrived.seatsIn3rdFloor}
                          onSelectSeat={seat => onSelectSeat(seat)}
                        />
                      </TrainBox>
                    )}

                    {seatData.seatArrived.seatsIn2ndFloor && (
                      <TrainBox>
                        <Typography variant="subtitle2" style={{ marginRight: '24px' }}>
                          <FormattedMessage id="train.floor" values={{ num: 2 }} />
                        </Typography>

                        <Type0SeatMap
                          seatingInfo={tempSeatingInfo}
                          hasFloor
                          seatData={seatData.seatArrived.seatsIn2ndFloor}
                          onSelectSeat={seat => onSelectSeat(seat)}
                        />
                      </TrainBox>
                    )}

                    {seatData.seatArrived.seatsIn1stFloor && (
                      <TrainBox>
                        <Typography variant="subtitle2" style={{ marginRight: '24px' }}>
                          <FormattedMessage id="train.floor" values={{ num: 1 }} />
                        </Typography>

                        <Type0SeatMap
                          seatingInfo={tempSeatingInfo}
                          hasFloor
                          seatData={seatData.seatArrived.seatsIn1stFloor}
                          onSelectSeat={seat => onSelectSeat(seat)}
                        />
                      </TrainBox>
                    )}
                  </div>
                ) : (
                  <TrainBox>
                    <Type0SeatMap
                      seatingInfo={tempSeatingInfo}
                      seatData={seatData.seatArrived.seatsIn1stFloor}
                      onSelectSeat={seat => onSelectSeat(seat)}
                    />
                  </TrainBox>
                )}
              </div>

              <SeatDetailSideBox
                seatingInfo={tempSeatingInfo}
                travellerSelected={travellerSelected}
                onSelectTraveller={travellerSelected => setTravellerSelected(travellerSelected)}
                onUpdateSeatingInfo={() => updateSeatingInfo(tempSeatingInfo)}
              />
            </div>
          ) : message ? (
            <div style={{ margin: '40px', display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{message}</Typography>
            </div>
          ) : (
            <div style={{ margin: '40px', display: 'flex', justifyContent: 'center' }}>
              <LoadingIcon />
            </div>
          )}
        </div>
      </div>
    </Dialog>
  );
};

export default connect(mapStateToProps)(SelectSeatPopup);
