import { Radio, Typography } from '@material-ui/core';
import { isEmpty } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { LIGHT_GREY, LIGHT_BLUE, SECONDARY } from '../../../../../colors';
import { some } from '../../../../../constants';
import styled from 'styled-components';
import { fade } from '@material-ui/core/styles';
import { TravellerSelectedType } from './SelectSeatPopup';

const Traveller = styled.div`
  display: flex;
  flex: 1;
  cursor: pointer;
  border-bottom: 0.5px solid ${LIGHT_GREY};
  padding: 3px 10px;
  :hover {
    background-color: ${fade(LIGHT_BLUE, 0.2)};
  }
`;

interface Props {
  travellerSelected: some;
  travellers: some[];
  travellerType: TravellerSelectedType;
  onSelectTraveller(data: { type: TravellerSelectedType; index: number }): void;
}

export const TravellerOption: React.FC<Props> = props => {
  const { travellerSelected, travellers, travellerType, onSelectTraveller } = props;
  return (
    <>
      {travellers.map((one: some, index: number) => (
        <Traveller key={index} onClick={() => onSelectTraveller({ index, type: travellerType })}>
          <div style={{ flex: 1 }}>
            <Typography variant="body2">
              <FormattedMessage id={travellerType} />
              &nbsp;{travellers.length > 1 ? index + 1 : ''}
            </Typography>
            <Typography variant="caption" color="textSecondary">
              {isEmpty(one) ? (
                <FormattedMessage id="train.noChooseSeat" />
              ) : (
                <>
                  <FormattedMessage
                    id="train.seat"
                    values={{
                      num: one.seatNumber,
                    }}
                  />
                  &nbsp;-&nbsp;
                  <span style={{ color: SECONDARY }}>
                    <FormattedNumber value={one.ticketPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </span>
                </>
              )}
            </Typography>
          </div>
          <Radio
            checked={travellerSelected.index === index && travellerSelected.type === travellerType}
            color="secondary"
          />
        </Traveller>
      ))}
    </>
  );
};
