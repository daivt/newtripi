import { isEqual } from 'lodash';
import * as React from 'react';
import { some } from '../../../constants';

const THRESHOLD = 200;

interface ISmartResultItemProps {
  visible: boolean;
  height?: number;
}

const SmartResultItem: React.RefForwardingComponent<HTMLDivElement, React.PropsWithChildren<ISmartResultItemProps>> = (
  props,
  ref,
) => {
  const { children, visible, height } = props;

  return (
    <div style={{ height: height || undefined, display: 'block', overflow: 'hidden' }} ref={ref}>
      {// if height is not set, we will need to render to find out the height
      (visible || !height) && children}
    </div>
  );
};

function findVisibleBounds(refs: React.RefObject<HTMLDivElement>[]) {
  let low = 0;
  let hi = refs.length - 1;
  let start = 0;
  let end = refs.length;

  while (hi >= low) {
    const mid = Math.floor((hi + low) / 2);
    const elem = refs[mid].current;
    if (elem && elem.getBoundingClientRect().bottom >= 0 - THRESHOLD) {
      if (mid === 0) {
        start = mid;
        break;
      }
      const prevElem = refs[mid - 1].current;
      if (prevElem && prevElem.getBoundingClientRect().bottom < 0 - THRESHOLD) {
        start = mid;
        break;
      }
      hi = mid - 1;
    } else {
      low = mid + 1;
    }
  }

  for (let i = start + 1; i < refs.length; i += 1) {
    const elem = refs[i].current;
    if (elem && elem.getBoundingClientRect().top > window.innerHeight + THRESHOLD) {
      end = i;
      break;
    }
  }

  return { start, end };
}

export function useSmartRenderResults(items: some[], fixedHeight?: number) {
  const [visibleBounds, setVisibleBounds] = React.useState({ start: 0, end: fixedHeight ? 0 : items.length });
  const [itemHeights, setItemHeights] = React.useState(
    new Array(items.length).fill(fixedHeight, 0, items.length) as number[],
  );

  const prevItems = React.useRef(items);

  const itemRefs = React.useMemo(() => {
    const len = items.length;
    const refs: React.RefObject<HTMLDivElement>[] = new Array(len);
    for (let i = 0; i < len; i += 1) {
      refs[i] = React.createRef<HTMLDivElement>();
    }
    return refs;
  }, [items]);

  React.useEffect(() => {
    if (fixedHeight) {
      setItemHeights(new Array(items.length).fill(fixedHeight, 0, items.length));
    }
  }, [items, fixedHeight]);

  React.useEffect(() => {
    if (visibleBounds.end !== items.length && prevItems.current !== items) {
      setVisibleBounds({ ...visibleBounds, end: items.length });
    }
  }, [items, visibleBounds]);

  React.useLayoutEffect(() => {
    if (!fixedHeight) {
      const newItemHeights = new Array(itemRefs.length) as number[];
      for (let i = 0; i < itemRefs.length; i += 1) {
        const elem = itemRefs[i].current;
        if (elem) {
          newItemHeights[i] = elem.getBoundingClientRect().height;
        }
      }
      if (!isEqual(newItemHeights, itemHeights)) {
        setItemHeights(newItemHeights);
      }
    }
  }, [itemRefs, itemHeights, fixedHeight]);

  React.useEffect(() => {
    const newVisibleBounds = findVisibleBounds(itemRefs);
    if (!isEqual(newVisibleBounds, visibleBounds)) {
      setVisibleBounds(newVisibleBounds);
    }
  }, [itemHeights, itemRefs, visibleBounds]);

  React.useEffect(() => {
    const listener = () => {
      setVisibleBounds(findVisibleBounds(itemRefs));
    };
    window.addEventListener('scroll', listener);
    return () => {
      window.removeEventListener('scroll', listener);
    };
  }, [itemRefs]);

  React.useEffect(() => {
    const listener = () => {
      setItemHeights(old => new Array(old.length).fill(fixedHeight, 0, old.length));
    };
    window.addEventListener('resize', listener);
    return () => window.removeEventListener('resize', listener);
  }, [fixedHeight, items, itemRefs]);

  React.useEffect(() => {
    prevItems.current = items;
  }, [items]);

  return { itemRefs, itemHeights, visibleBounds };
}

export default React.forwardRef(SmartResultItem);
