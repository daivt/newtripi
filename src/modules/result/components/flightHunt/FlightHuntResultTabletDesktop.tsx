import { Container, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import { groupBy } from 'lodash';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { TABLET_WIDTH, SLASH_DATE_FORMAT } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconFlight } from '../../../../svg/ic_flight_itinerary_airplane.svg';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import FlightHuntSearch from '../../../search/components/FlightHuntSearch';
import { stringifyFlightHuntSearchParams } from '../../../search/utils';
import { setDates } from '../../redux/flightHuntResultReducer';
import FlightHuntCalendar from './FlightHuntCalendar';
import FlightHuntFilter from './FlightHuntFilter';
import FlightHuntInfoBox from './FlightHuntInfoBox';
import FlightHuntMinPriceBox from './FlightHuntMinPriceBox';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightHuntResultTabletDesktop: React.FunctionComponent<Props> = props => {
  const { departureData, lastParams, dispatch, returnData, filterParams, outboundDate, inboundDate, search } = props;
  const changeDate = React.useCallback(
    (newOutboundDate?: Moment, newInboundDate?: Moment) => {
      dispatch(setDates(newOutboundDate, newInboundDate));
      if (lastParams) {
        dispatch(
          push({
            search: stringifyFlightHuntSearchParams(lastParams, filterParams, newOutboundDate, newInboundDate),
          }),
        );
      }
    },
    [dispatch, lastParams, filterParams],
  );
  const handleChooseOutboundDate = React.useCallback(
    (date: Moment) => {
      const isAfterReturn = inboundDate ? date.isAfter(inboundDate) : false;
      if (!inboundDate) {
        changeDate(date);
      } else if (isAfterReturn) {
        changeDate(date);
      } else {
        changeDate(date, inboundDate);
      }
    },
    [changeDate, inboundDate],
  );

  const handleChooseInboundDate = React.useCallback(
    (date: Moment) => {
      const isBeforeDepart = outboundDate ? date.isBefore(outboundDate) : false;
      if (isBeforeDepart) {
        changeDate(undefined, date);
      } else if (outboundDate) {
        changeDate(outboundDate, date);
      } else {
        changeDate(undefined, date);
      }
    },
    [changeDate, outboundDate],
  );
  const dataStructureDeparture = departureData
    ? groupBy(departureData, d => moment(d.date, SLASH_DATE_FORMAT).format('YYYY/MM'))
    : undefined;
  const dataStructureReTurn = returnData
    ? groupBy(returnData, d => moment(d.date, SLASH_DATE_FORMAT).format('YYYY/MM'))
    : undefined;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <FlightHuntSearch home={false} />
      <Container style={{ flex: 1, marginBottom: '52px', display: 'flex', flexDirection: 'column' }}>
        <Typography variant="h5" style={{ margin: '20px 0px 16px 16px' }}>
          <FormattedMessage id="flightHunt.cheapTicketHunt" />
        </Typography>
        <FlightHuntFilter filterParams={filterParams.airlineIds} />
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ flex: 1 }}>
            <div>
              {(lastParams && lastParams.oneWay) || search.oneWay ? (
                <div>
                  <FlightHuntCalendar
                    data={dataStructureDeparture}
                    params={lastParams}
                    filterParams={filterParams}
                    chooseDate={outboundDate}
                    handleChoseDate={handleChooseOutboundDate}
                    header={
                      dataStructureDeparture &&
                      lastParams && (
                        <Typography variant="subtitle2" style={{ display: 'flex', alignItems: 'center' }}>
                          <FormattedMessage id="flightHunt.depart" />
                          :&nbsp;&nbsp;
                          {lastParams.origin && lastParams.origin.location}
                          &nbsp;&nbsp;
                          <IconFlight />
                          &nbsp;&nbsp;
                          {lastParams.destination && lastParams.destination.location}
                        </Typography>
                      )
                    }
                  />
                </div>
              ) : (
                <>
                  <FlightHuntCalendar
                    handleChoseDate={handleChooseOutboundDate}
                    data={dataStructureDeparture}
                    chooseDate={outboundDate}
                    header={
                      dataStructureDeparture &&
                      lastParams && (
                        <Typography variant="subtitle2" style={{ display: 'flex', alignItems: 'center' }}>
                          <FormattedMessage id="flightHunt.depart" />
                          :&nbsp;&nbsp;
                          {lastParams.origin && lastParams.origin.location}
                          &nbsp;&nbsp;
                          <IconFlight />
                          &nbsp;&nbsp;
                          {lastParams.destination && lastParams.destination.location}
                        </Typography>
                      )
                    }
                  />
                  <div style={{ marginTop: '32px' }}>
                    <FlightHuntCalendar
                      handleChoseDate={handleChooseInboundDate}
                      data={dataStructureReTurn}
                      chooseDate={inboundDate}
                      header={
                        dataStructureReTurn &&
                        lastParams && (
                          <Typography variant="subtitle2" style={{ display: 'flex', alignItems: 'center' }}>
                            <FormattedMessage id="flightHunt.return" />
                            :&nbsp;&nbsp;
                            {lastParams.destination && lastParams.destination.location}
                            &nbsp;&nbsp;
                            <IconFlight />
                            &nbsp;&nbsp;
                            {lastParams.origin && lastParams.origin.location}
                          </Typography>
                        )
                      }
                    />
                  </div>
                </>
              )}
            </div>
          </div>
          <div style={{ width: '330px', paddingLeft: '32px' }}>
            <FlightHuntMinPriceBox
              departureData={dataStructureDeparture}
              returnData={dataStructureReTurn}
              onClick={(turn, date) => {
                if (turn) {
                  handleChooseInboundDate(date);
                } else {
                  handleChooseOutboundDate(date);
                }
              }}
            />
            <FlightHuntInfoBox />
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return {
    departureData: state.result.flightHunt.departureData,
    returnData: state.result.flightHunt.returnData,
    lastParams: state.result.flightHunt.lastParams,
    filterParams: state.result.flightHunt.filterParams,
    outboundDate: state.result.flightHunt.outboundDate,
    inboundDate: state.result.flightHunt.inboundDate,
    search: state.search.flightHunt,
  };
}

export default connect(mapStateToProps)(FlightHuntResultTabletDesktop);
