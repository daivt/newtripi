import { ButtonBase, Typography } from '@material-ui/core';
import { Dictionary, minBy } from 'lodash';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DARK_GREY, GREY, HOVER_GREY } from '../../../../colors';
import { SLASH_DATE_FORMAT, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';

interface Props extends ReturnType<typeof mapStateToProps> {
  departureData?: Dictionary<some[]>;
  returnData?: Dictionary<some[]>;
  dispatch: Dispatch;
  onClick: (turn: boolean, date: Moment) => void;
}
class FlightHuntMinPriceBox extends React.PureComponent<Props> {
  render() {
    const { departureData, returnData, onClick, oneWay, lastParams } = this.props;
    const resultData: some = [];
    if (departureData) {
      let count = 0;
      Object.keys(departureData).forEach(k => {
        resultData[count] = {};
        resultData[count].departure = minBy(departureData[k], (item: some) => {
          return item.minPrice;
        });
        if (returnData) {
          resultData[count].return = minBy(returnData[k], (item: some) => {
            return item.minPrice;
          });
        }
        resultData[count].month = k;
        count += 1;
      });
    }

    resultData.sort((a: some, b: some) => {
      return moment(a.month, 'YYYY/MM').isBefore(moment(b.month, 'YYYY/MM'))
        ? -1
        : moment(a.month, 'YYYY/MM').isAfter(moment(b.month, 'YYYY/MM'))
        ? 1
        : 0;
    });
    return (
      <div>
        <Typography variant="subtitle2" style={{ marginLeft: '12px', marginBottom: '11px' }}>
          <FormattedMessage id="flightHunt.minPricePerMount" />
        </Typography>
        {lastParams && (
          <table
            style={{
              border: `1px solid ${GREY}`,
              borderRadius: '4px',
              width: '100%',
              borderSpacing: '0px',
              overflow: 'hidden',
            }}
          >
            <thead>
              <tr style={{ height: '32px' }}>
                <td style={{ textAlign: 'center' }}>
                  <Typography variant="body1" style={{ color: DARK_GREY }}>
                    <FormattedMessage id="flightHunt.monthStr" />
                  </Typography>
                </td>
                <td style={{ textAlign: 'end', paddingRight: '30px' }}>
                  <Typography variant="body1" style={{ color: DARK_GREY }}>
                    <FormattedMessage id="flightHunt.departStr" />
                  </Typography>
                </td>
                {!oneWay && (
                  <td style={{ textAlign: 'end', paddingRight: '30px' }}>
                    <Typography variant="body1" style={{ color: DARK_GREY }}>
                      <FormattedMessage id="flightHunt.returnStr" />
                    </Typography>
                  </td>
                )}
              </tr>
            </thead>
            <tbody>
              {resultData.map((v: some, index: number) => (
                <tr
                  key={v.month}
                  style={{
                    background: index % 2 === 0 ? HOVER_GREY : undefined,
                    height: '40px',
                    textAlign: 'center',
                    border: 'none',
                  }}
                >
                  <td>
                    <Typography variant="body1" color="textPrimary">
                      {moment(v.month, 'YYYY/MM').format('M')}
                    </Typography>
                  </td>
                  {v.departure ? (
                    <td style={{ cursor: 'pointer', textAlign: 'end', paddingRight: '15px' }} title={v.departure.date}>
                      <ButtonBase
                        onClick={() => {
                          onClick(false, moment(v.departure.date, SLASH_DATE_FORMAT));
                        }}
                      >
                        <Typography variant="body1" color="primary">
                          <FormattedNumber value={v.departure.minPrice} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      </ButtonBase>
                    </td>
                  ) : (
                    <td style={{ textAlign: 'end', paddingRight: '15px' }} title="">
                      <Typography variant="body1" color="primary">
                        - - - - - - - - - -
                      </Typography>
                    </td>
                  )}

                  {v.return ? (
                    <td style={{ cursor: 'pointer', textAlign: 'end', paddingRight: '15px' }} title={v.return.date}>
                      <ButtonBase
                        onClick={() => {
                          onClick(true, moment(v.return.date, SLASH_DATE_FORMAT));
                        }}
                      >
                        <Typography variant="body1" color="secondary">
                          <FormattedNumber value={v.return.minPrice} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      </ButtonBase>
                    </td>
                  ) : (
                    !oneWay && (
                      <td style={{ textAlign: 'end', paddingRight: '15px' }} title="">
                        <Typography variant="body1" color="secondary">
                          - - - - - - - - - -
                        </Typography>
                      </td>
                    )
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    oneWay: state.result.flightHunt.lastParams
      ? state.result.flightHunt.lastParams.oneWay
      : state.search.flightHunt.oneWay,
    lastParams: state.result.flightHunt.lastParams,
  };
};

export default connect(mapStateToProps)(FlightHuntMinPriceBox);
