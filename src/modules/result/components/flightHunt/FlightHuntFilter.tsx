import { Grid, Typography } from '@material-ui/core';
import { remove } from 'lodash';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CheckButton } from '../../../common/components/elements';
import { FlightHuntSupportedAirlines } from '../../constants';
import { setAirlines } from '../../redux/flightHuntResultReducer';

export interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  filterParams?: number[];
}

class FlightHuntFilter extends PureComponent<Props> {
  setAirline(aid: number) {
    const { filterParams, dispatch } = this.props;
    const filter = filterParams ? [...filterParams] : [];
    if (filter.indexOf(aid) === -1) {
      filter.push(aid);
    } else {
      remove(filter, id => aid === id);
    }
    dispatch(setAirlines(filter));
  }

  render() {
    const { filterParams, info } = this.props;
    const airlines = FlightHuntSupportedAirlines.map(item => {
      return info.flight && info.flight.airlines.find((airline: some) => airline.aid === item.aid);
    });
    return (
      <div>
        <Grid container style={{ marginTop: '16px', marginBottom: '16px' }}>
          <Grid
            item
            style={{
              marginLeft: '16px',
              marginRight: '16px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Typography variant="body1">
              <FormattedMessage id="flightHunt.filterBy" />
            </Typography>
          </Grid>
          {airlines.map(airline => (
            <Grid key={airline.aid} style={{ marginLeft: '10px' }}>
              <CheckButton
                style={{ padding: '5px 24px', height: '36px', borderRadius: '18px' }}
                size="large"
                active={filterParams ? filterParams.indexOf(airline.aid) !== -1 : false}
                onClick={() => this.setAirline(airline.aid)}
              >
                <Typography variant="body2">{airline.name}</Typography>
              </CheckButton>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    info: state.common.generalInfo,
  };
}

export default connect(mapStateToProps)(FlightHuntFilter);
