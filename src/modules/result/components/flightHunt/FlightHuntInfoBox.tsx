import { Button, Divider, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { PRIMARY } from '../../../../colors';
import { ROUTES, SLASH_DATE_FORMAT } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import MessageDialog from '../../../common/components/MessageDialog';
import { stringifyFlightHuntResultParams } from '../../../search/utils';

interface Props extends ReturnType<typeof mapStateToProps> {}

const FlightHuntInfoBox: React.FunctionComponent<Props> = props => {
  const { departureData, returnData, params, filterParams, outboundDate, inboundDate } = props;
  const [show, setShow] = React.useState<boolean>(false);
  const departPrice =
    departureData &&
    departureData.find(v => {
      const date = filterParams && outboundDate && outboundDate.format(SLASH_DATE_FORMAT);
      return v.date === date;
    });
  const returnPrice =
    returnData &&
    returnData.find(v => {
      const date = filterParams && inboundDate && inboundDate.format(SLASH_DATE_FORMAT);
      return v.date === date;
    });
  const checkContinueOneWay = params.oneWay && outboundDate && departureData;
  const checkContinueTwoWay =
    !params.oneWay &&
    inboundDate &&
    outboundDate &&
    outboundDate.isSameOrBefore(inboundDate) &&
    departureData &&
    returnData;
  return (
    <>
      {(outboundDate || inboundDate) && (
        <>
          <div
            style={{
              marginTop: '20px',
              padding: '0px 10px',
              border: `1px solid ${PRIMARY}`,
              borderRadius: '4px',
            }}
          >
            {outboundDate && (
              <div
                style={{
                  minHeight: '56px',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Typography
                  variant="body1"
                  style={{
                    lineHeight: '24px',
                    width: '25%',
                    fontWeight: 'bold',
                  }}
                >
                  <FormattedMessage id="flightHunt.depart" />
                </Typography>
                <Typography
                  variant="body1"
                  style={{
                    lineHeight: '24px',
                    width: '40%',
                    textAlign: 'center',
                  }}
                >
                  {outboundDate.format('L')}
                </Typography>

                {departPrice && (
                  <Typography
                    variant="body1"
                    style={{
                      lineHeight: '24px',
                      flex: 1,
                      textAlign: 'center',
                    }}
                    color="secondary"
                  >
                    <FormattedNumber value={departPrice.minPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                )}
              </div>
            )}

            {inboundDate && !params.oneWay && (
              <>
                <Divider />
                <div
                  style={{
                    minHeight: '56px',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Typography
                    variant="body1"
                    style={{
                      lineHeight: '24px',
                      width: '25%',
                      fontWeight: 'bold',
                    }}
                  >
                    <FormattedMessage id="flightHunt.return" />
                  </Typography>
                  <Typography
                    variant="body1"
                    style={{
                      lineHeight: '24px',
                      width: '40%',
                      textAlign: 'center',
                    }}
                  >
                    {inboundDate.format('L')}
                  </Typography>
                  {returnPrice && (
                    <Typography
                      variant="body1"
                      style={{
                        lineHeight: '24px',
                        flex: 1,
                        textAlign: 'center',
                      }}
                      color="secondary"
                    >
                      <FormattedNumber value={returnPrice.minPrice} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  )}
                </div>
              </>
            )}
          </div>
          {!params.oneWay && !returnPrice && departPrice && (
            <Typography variant="body1" color="error" style={{ lineHeight: '48px' }}>
              <FormattedMessage id="flightHunt.continueReturn" />
            </Typography>
          )}
          {!params.oneWay && returnPrice && !departPrice && (
            <Typography variant="body1" color="error" style={{ lineHeight: '48px' }}>
              <FormattedMessage id="flightHunt.continueDepart" />
            </Typography>
          )}
        </>
      )}

      {(checkContinueOneWay || checkContinueTwoWay) && (
        <a
          href={
            checkContinueTwoWay || checkContinueOneWay
              ? `${ROUTES.flight.flightResult}?${stringifyFlightHuntResultParams(
                  params,
                  filterParams,
                  outboundDate,
                  inboundDate,
                )}`
              : undefined
          }
          target="_blank"
          rel="noopener noreferrer"
          style={{
            textDecoration: 'inherit',
            color: 'inherit',
          }}
        >
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            style={{ height: '40px', marginTop: '20px' }}
            onClick={() => {
              if (params.oneWay ? !checkContinueOneWay : !checkContinueTwoWay) setShow(true);
            }}
          >
            <FormattedMessage id="flightHunt.ok" />
          </Button>
        </a>
      )}
      <MessageDialog
        show={show}
        message={
          <Typography variant="body1" style={{ textAlign: 'center' }}>
            <FormattedMessage id="flightHunt.warning" />
          </Typography>
        }
        onClose={() => setShow(false)}
      />
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    departureData: state.result.flightHunt.departureData,
    returnData: state.result.flightHunt.returnData,
    params: state.search.flightHunt,
    filterParams: state.result.flightHunt.filterParams,
    outboundDate: state.result.flightHunt.outboundDate,
    inboundDate: state.result.flightHunt.inboundDate,
  };
};
export default connect(mapStateToProps)(FlightHuntInfoBox);
