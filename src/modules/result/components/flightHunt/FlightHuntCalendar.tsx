import { Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import { Dictionary, find } from 'lodash';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREEN, GREY, HOVER_GREY, PRIMARY, RED } from '../../../../colors';
import { ROUTES, SLASH_DATE_FORMAT, some } from '../../../../constants';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { FlightHuntSearchState } from '../../../search/redux/flightHuntSearchReducer';
import { stringifyFlightHuntResultParams } from '../../../search/utils';
import { FlightHuntFilterState } from '../../redux/flightHuntResultReducer';

const { CalendarVietnamese } = require('date-chinese');

interface CellProps {
  hover: boolean;
  isFocus?: boolean;
  isBetween?: boolean;
  isBefore?: boolean;
}
const Cell = styled.td<CellProps>`
  height: 64px;
  border: 1px solid ${GREY};
  box-sizing: border-box;
  padding: 0px;
  cursor: ${props => (props.hover ? 'pointer' : undefined)};
  &:hover {
    background: ${props => (props.hover && !props.isFocus ? fade(`${PRIMARY}`, 0.2) : undefined)};
  }
  background: ${props =>
    props.isFocus ? PRIMARY : props.isBetween ? '#ffeaa4' : props.isBefore ? HOVER_GREY : 'white'};
  color: ${props => props.isFocus && 'white'};
`;
const Head = styled.td`
  height: 30px;
  text-align: center;
`;

interface Props {
  data?: Dictionary<some[]>;
  handleChoseDate: (chooseDate: moment.Moment) => void;
  chooseDate?: Moment;
  header?: React.ReactNode;
  params?: FlightHuntSearchState;
  filterParams?: FlightHuntFilterState;
}

interface State {
  dateObject: moment.Moment;
}

const cal = new CalendarVietnamese();
const getLunarDate = (year: number, month: number, day: number) => {
  cal.fromGregorian(year, month, day);
  const lunarYear = cal.yearFromEpochCycle();
  return {
    year: lunarYear,
    month: cal.month,
    day: cal.day,
  };
};

const FlightHuntCalendar: React.FC<Props> = props => {
  const { data, handleChoseDate, chooseDate, header, params, filterParams } = props;
  const [dateObject, setDateObject] = React.useState(
    chooseDate ? chooseDate.clone().startOf('month') : moment().startOf('month'),
  );

  React.useEffect(() => {
    setDateObject(chooseDate ? chooseDate.clone().startOf('month') : moment().startOf('month'));
  }, [chooseDate]);

  const getPrice = React.useCallback(
    (value: number) => {
      let price;
      const monthData = data ? data[dateObject.clone().format('YYYY/MM')] : [];
      if (monthData) {
        const dayData = find(monthData, item => {
          return item.date === moment(dateObject.clone().add(value - 1, 'days')).format(SLASH_DATE_FORMAT);
        });
        price = dayData && dayData.minPrice;
      }
      return price && Math.floor(price / 1000);
    },
    [data, dateObject],
  );
  const handleNextMonth = React.useCallback(() => {
    setDateObject(value => value.clone().add(1, 'months'));
  }, []);

  const handlePrevMonth = React.useCallback(() => {
    setDateObject(value => value.clone().subtract(1, 'months'));
  }, []);

  const renderCalendar = React.useMemo(() => {
    const monthData = (data && data[dateObject.format('YYYY/MM')]) || [];
    const totalCells: JSX.Element[] = [];
    let week: JSX.Element[] = [];
    const allWeeks: JSX.Element[][] = [];
    const firstDayOfMonth = moment(dateObject).startOf('month');
    let min = Number.MAX_SAFE_INTEGER;
    monthData.forEach(item => {
      if (!(item.minPrice === null)) {
        if (item.minPrice < min) {
          min = item.minPrice;
        }
      }
    });
    min = Math.floor(min / 1000);
    const max = Math.floor(
      monthData.reduce(
        (currentMax, p) => (p.minPrice > currentMax ? p.minPrice : currentMax),
        Number.MIN_SAFE_INTEGER,
      ) / 1000,
    );

    for (let d = 1; d <= firstDayOfMonth.day(); d += 1) {
      totalCells.push(<Cell isFocus={false} hover={false} key={`blank${d} `} />);
    }
    for (let d = 1; d <= moment(dateObject).daysInMonth(); d += 1) {
      const price = getPrice(d);
      const day = dateObject.clone().add(d - 1, 'days');
      const isChosenDate = chooseDate ? day.isSame(chooseDate, 'days') : false;
      totalCells.push(
        <Cell
          isFocus={isChosenDate}
          hover={price !== undefined}
          key={`day${d}`}
          onClick={() => {
            if (price) {
              handleChoseDate(day);
            }
          }}
        >
          <a
            href={
              price && params && filterParams && params.oneWay
                ? `${ROUTES.flight.flightResult}?${stringifyFlightHuntResultParams(params, filterParams, day)}`
                : undefined
            }
            target="_blank"
            rel="noopener noreferrer"
            style={{
              textDecoration: 'inherit',
              color: 'inherit',
            }}
          >
            <div style={{ height: '100%', paddingTop: '8px' }}>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                <>
                  <div style={{ paddingLeft: '12px' }}>
                    <Typography
                      style={{
                        color: isChosenDate ? 'white' : day.isSame(moment(), 'days') ? PRIMARY : price ? 'black' : GREY,
                        lineHeight: '24px',
                      }}
                      variant="subtitle1"
                    >
                      {d}
                    </Typography>
                  </div>
                  <div style={{ paddingRight: '12px' }}>
                    <Typography style={{ color: isChosenDate ? 'white' : GREY }} variant="caption">
                      {getLunarDate(firstDayOfMonth.year(), firstDayOfMonth.month() + 1, d).day === 1
                        ? `${getLunarDate(firstDayOfMonth.year(), firstDayOfMonth.month() + 1, d).day}/${
                            getLunarDate(firstDayOfMonth.year(), firstDayOfMonth.month() + 1, d).month
                          }`
                        : getLunarDate(firstDayOfMonth.year(), firstDayOfMonth.month() + 1, d).day}
                    </Typography>
                  </div>
                </>
              </div>
              <div style={{ paddingTop: '6px' }}>
                <>
                  <div style={{ display: 'flex', paddingLeft: '12px', height: '24px' }}>
                    {price ? (
                      <Typography
                        style={{
                          color: isChosenDate ? 'white' : price === min ? GREEN : price === max ? RED : GREY,
                        }}
                        variant="body2"
                      >
                        <FormattedNumber value={price} />
                        &nbsp;k
                      </Typography>
                    ) : (
                      <div />
                    )}
                  </div>
                </>
              </div>
            </div>
          </a>
        </Cell>,
      );
    }
    for (let d = 1; d <= (7 - ((moment(dateObject).daysInMonth() + firstDayOfMonth.day()) % 7)) % 7; d += 1) {
      totalCells.push(
        <Cell isFocus={false} hover={false} key={`blank${d + firstDayOfMonth.day()} `}>
          &nbsp;
        </Cell>,
      );
    }

    totalCells.forEach((day, index) => {
      if (index % 7 !== 0) {
        week.push(day);
      } else {
        allWeeks.push(week);
        week = [];
        week.push(day);
      }
      if (index === totalCells.length - 1) {
        allWeeks.push(week);
      }
    });
    return allWeeks;
  }, [chooseDate, data, dateObject, filterParams, getPrice, handleChoseDate, params]);
  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          border: `1px solid ${GREY}`,
          background: HOVER_GREY,
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            margin: '8px 16px',
            alignItems: 'center',
          }}
        >
          {header}
          <div style={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="flightHunt.month" values={{ num: dateObject.format('MM') }} />
            </Typography>
            {dateObject.isSameOrBefore(moment(), 'months') ? (
              <ChevronLeft style={{ marginLeft: '20px' }} color="disabled" />
            ) : (
              <ChevronLeft style={{ cursor: 'pointer', marginLeft: '20px' }} onClick={handlePrevMonth} />
            )}
            {dateObject.isSameOrAfter(moment().add(6, 'months'), 'months') ? (
              <ChevronRight style={{ marginLeft: '20px' }} color="disabled" />
            ) : (
              <ChevronRight style={{ cursor: 'pointer', marginLeft: '20px' }} onClick={handleNextMonth} />
            )}
          </div>
        </div>
        {!data ? (
          <div>
            <LoadingIcon
              style={{
                height: '350px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            />
          </div>
        ) : (
          <table style={{ borderCollapse: 'collapse', paddingTop: '10px', width: '100%' }}>
            <thead>
              <tr style={{ background: 'white' }}>
                {Array(7)
                  .fill(0)
                  .map((v, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <Head key={index}>
                      <Typography variant="body2">
                        {moment()
                          .day(index)
                          .format('ddd')}
                      </Typography>
                    </Head>
                  ))}
              </tr>
            </thead>
            <tbody>
              {renderCalendar.map((w, i) => {
                // eslint-disable-next-line react/no-array-index-key
                return <tr key={`week${i} `}>{w}</tr>;
              })}
            </tbody>
          </table>
        )}
      </div>
    </>
  );
};

export default FlightHuntCalendar;
