/* eslint-disable @typescript-eslint/camelcase */
import stringify from 'json-stable-stringify';
import { isEmpty } from 'lodash';
import moment from 'moment';
import { BLUE, GREY } from '../../colors';
import { some } from '../../constants';
import gym from '../../svg/facilityIcon/gym.svg';
import ic_airports_transfer from '../../svg/facilityIcon/ic_airports_transfer.svg';
import ic_alarm from '../../svg/facilityIcon/ic_alarm.svg';
import ic_baby_sitting from '../../svg/facilityIcon/ic_baby_sitting.svg';
import ic_balcony from '../../svg/facilityIcon/ic_balcony.svg';
import ic_bar from '../../svg/facilityIcon/ic_bar.svg';
import ic_beauty_salon from '../../svg/facilityIcon/ic_beauty_salon.svg';
import ic_bicycle from '../../svg/facilityIcon/ic_bicycle.svg';
import ic_braile from '../../svg/facilityIcon/ic_braile.svg';
import ic_bus from '../../svg/facilityIcon/ic_bus.svg';
import ic_call from '../../svg/facilityIcon/ic_call.svg';
import ic_car_2 from '../../svg/facilityIcon/ic_car_2.svg';
import ic_clothes_dryer from '../../svg/facilityIcon/ic_clothes_dryer.svg';
import ic_concierge from '../../svg/facilityIcon/ic_concierge.svg';
import ic_conditional from '../../svg/facilityIcon/ic_conditional.svg';
import ic_currency_change from '../../svg/facilityIcon/ic_currency_change.svg';
import ic_dailyCleaning from '../../svg/facilityIcon/ic_dailyCleaning.svg';
import ic_dinner from '../../svg/facilityIcon/ic_dinner.svg';
import ic_door_hanger_outline from '../../svg/facilityIcon/ic_door_hanger_outline.svg';
import ic_elevator from '../../svg/facilityIcon/ic_elevator.svg';
import ic_facilities_disabled_guests from '../../svg/facilityIcon/ic_facilities_disabled_guests.svg';
import ic_ferry_transfer from '../../svg/facilityIcon/ic_ferry_transfer.svg';
import ic_garden from '../../svg/facilityIcon/ic_garden.svg';
import ic_hand_money from '../../svg/facilityIcon/ic_hand_money.svg';
import ic_laundry_service from '../../svg/facilityIcon/ic_laundry_service.svg';
import ic_luggage_storage from '../../svg/facilityIcon/ic_luggage_storage.svg';
import ic_meeting_facilities from '../../svg/facilityIcon/ic_meeting_facilities.svg';
import ic_money from '../../svg/facilityIcon/ic_money.svg';
import ic_motobike from '../../svg/facilityIcon/ic_motobike.svg';
import ic_night_club from '../../svg/facilityIcon/ic_night_club.svg';
import ic_no_smorking from '../../svg/facilityIcon/ic_no_smorking.svg';
import ic_parking from '../../svg/facilityIcon/ic_parking.svg';
import ic_parquet_floors from '../../svg/facilityIcon/ic_parquet_floors.svg';
import ic_payment_jcb from '../../svg/facilityIcon/ic_payment_jcb.svg';
import ic_pet_face from '../../svg/facilityIcon/ic_pet_face.svg';
import ic_photocopy from '../../svg/facilityIcon/ic_photocopy.svg';
import ic_poolside_bar from '../../svg/facilityIcon/ic_poolside_bar.svg';
// facility icon
import ic_room_service24hour from '../../svg/facilityIcon/ic_room_service24hour.svg';
import ic_safety_deposit_boxes from '../../svg/facilityIcon/ic_safety_deposit_boxes.svg';
import ic_shops from '../../svg/facilityIcon/ic_shops.svg';
import ic_shutle_service from '../../svg/facilityIcon/ic_shutle_service.svg';
import ic_tiled_floor from '../../svg/facilityIcon/ic_tiled_floor.svg';
import ic_toilet_for_the_disabled from '../../svg/facilityIcon/ic_toilet_for_the_disabled.svg';
import ic_tours_service from '../../svg/facilityIcon/ic_tours_service.svg';
import ic_train_head from '../../svg/facilityIcon/ic_train_head.svg';
import ic_visa_card_payable from '../../svg/facilityIcon/ic_visa_card_payable.svg';
import ic_wash_basin_for_the_disabled from '../../svg/facilityIcon/ic_wash_basin_for_the_disabled.svg';
import ic_wifi from '../../svg/facilityIcon/ic_wifi.svg';
import ic_wine from '../../svg/facilityIcon/ic_wine.svg';
import restaurant from '../../svg/facilityIcon/restaurant.svg';
import sauna from '../../svg/facilityIcon/sauna.svg';
import spa from '../../svg/facilityIcon/spa.svg';
import swimming_pool from '../../svg/facilityIcon/swimming_pool.svg';
import { SeatingInfo } from '../booking/redux/trainBookingReducer';
import {
  FLIGHT_SEARCH_FILTER_PARAM_NAMES,
  HOTEL_ACTIVITY_DIRECT_PARAM,
  HOTEL_BOOK_PARAMS_NAMES,
  TOUR_BOOK_PARAMS_NAMES,
} from '../common/constants';
import { FlightSearchParamsState } from '../search/redux/flightSearchReducer';
import { HotelSearchParamsState } from '../search/redux/hotelSearchReducer';
import { TourSearchParamsState } from '../search/redux/tourSearchReducer';
import { parseHotelSearchParams } from '../search/utils';
import { PRICE_STEP, TECHNICAL_STOP, TRAIN_SEAT_STATUS } from './constants';
import { FlightFilterState } from './redux/flightResultReducer';
import { HotelFilterState } from './redux/hotelResultReducer';
import { TourFilterState } from './redux/tourResultReducer';

export function parseFlightSearchFilterParams(params: URLSearchParams): { airlineIds: number[] } {
  const airlineIdsStr = params.get(FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline);
  const airlineIds = airlineIdsStr ? JSON.parse(airlineIdsStr) : [];

  return { airlineIds };
}

export function filterAndSort(tickets: some[], filter: FlightFilterState, cmp?: (a: some, b: some) => number) {
  const filteredTickets = tickets.filter((ticket: some) => {
    if (filter.airline.length > 0) {
      if (!filter.airline.find(one => one === ticket.outbound.aid)) {
        return false;
      }
    }

    if (ticket.outbound.duration < filter.flightDuration[0] || ticket.outbound.duration > filter.flightDuration[1]) {
      return false;
    }

    if (filter.numStops.length > 0) {
      const stopCount = ticket.outbound.ticketdetail.numStops;
      if (filter.numStops.indexOf(stopCount) === -1) {
        return false;
      }
    }

    if (
      ticket.outbound.ticketdetail.bestGroupPriceAdultUnit < filter.price[0] ||
      ticket.outbound.ticketdetail.bestGroupPriceAdultUnit > filter.price[1]
    ) {
      return false;
    }

    if (filter.ticketClass.length > 0) {
      const find = filter.ticketClass.find(one => ticket.outbound.ticketdetail.ticketClassCode === one.code);
      if (!find) {
        return false;
      }
    }

    if (filter.timeLand.length > 0) {
      const find = filter.timeLand.find((one: some) => {
        const ticketTime = moment(ticket.outbound.arrivalTimeStr, 'HH:mm');
        const lowerTime = moment(one.value[0], 'HH:mm');
        const upperTime = moment(one.value[1], 'HH:mm');

        return !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime);
      });
      if (!find) {
        return false;
      }
    }

    if (filter.timeTakeOff.length > 0) {
      const find = filter.timeTakeOff.find((one: some) => {
        const ticketTime = moment(ticket.outbound.departureTimeStr, 'HH:mm');
        const lowerTime = moment(one.value[0], 'HH:mm');
        const upperTime = moment(one.value[1], 'HH:mm');

        return !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime);
      });
      if (!find) {
        return false;
      }
    }

    const transitDuration = getTransitDuration(ticket);

    if (transitDuration < filter.transitDuration[0] || transitDuration > filter.transitDuration[1]) {
      return false;
    }

    return true;
  });

  if (cmp) {
    return filteredTickets.sort(cmp);
  }

  return filteredTickets;
}

export const flightCheapestRoundtripCmp = (a: some, b: some) =>
  a.outbound.ticketdetail.bestGroupPriceAdultUnit - b.outbound.ticketdetail.bestGroupPriceAdultUnit;
export const flightCheapestCmp = (a: some, b: some) =>
  a.outbound.ticketdetail.priceAdultUnit - b.outbound.ticketdetail.priceAdultUnit;
export const flightFastestCmp = (a: some, b: some) => a.outbound.duration - b.outbound.duration;
export const flightTimeTakeOffCmp = (a: some, b: some) => a.outbound.departureTime - b.outbound.departureTime;

export function getMaxTransitDuration(tickets: some[]) {
  const value = tickets.reduce((prev, ticket) => Math.max(prev, getTransitDuration(ticket)), 0);
  return value;
}

function getTransitDuration(ticket: some) {
  let transitDuration = 0;
  if (ticket.outbound.transitTickets) {
    for (let i = 0; i < ticket.outbound.transitTickets.length; i += 1) {
      transitDuration += ticket.outbound.transitTickets[i].transitDuration;
    }
  }
  return transitDuration;
}

export function getMaxFlightDuration(tickets: some[]) {
  const value = tickets.reduce((prev, ticket) => Math.max(prev, ticket.outbound.duration), 0);
  return value;
}

export function getStops(ticket: some) {
  const value = [];
  if (ticket.outbound.transitTickets) {
    for (let i = 0; i < ticket.outbound.transitTickets.length; i += 1) {
      const transitTicket = ticket.outbound.transitTickets[i];
      const tstop = getTechnicalStop(transitTicket.polices);
      if (tstop) {
        value.push({ code: tstop, city: '', technical: true });
      }

      if (i !== ticket.outbound.transitTickets.length - 1) {
        value.push({ code: transitTicket.arrivalAirport, city: transitTicket.arrivalCity });
      }
    }
  } else {
    const tstop = getTechnicalStop(ticket.outbound.ticketdetail.polices);
    if (tstop) {
      value.push({ code: tstop, city: '' });
    }
  }
  return value;
}

export function removeFilterParams(search: string) {
  const params = new URLSearchParams(search);
  params.delete(FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline);
  return params.toString();
}

function getTechnicalStop(policies: some[]) {
  const stop = policies.find(policy => {
    if (policy.code === TECHNICAL_STOP) {
      return true;
    }
    return false;
  });
  if (stop) {
    const { moreInfo } = stop;
    if (moreInfo) {
      return moreInfo.airportCode;
    }
  }
  return null;
}
export function sameTourSearchParams(params1: TourSearchParamsState, params2: TourSearchParamsState) {
  return stringify(params1) === stringify(params2);
}

export function sameTourFilterSortByParams(
  params1: TourFilterState,
  sortBy1: string,
  params2: TourFilterState,
  sortBy2: string,
) {
  return stringify(params1) === stringify(params2) && sortBy1 === sortBy2;
}
export function sameHotelSearchParams(params1: HotelSearchParamsState, params2: HotelSearchParamsState) {
  return stringify(params1) === stringify(params2);
}

export function sameFlightSearchParams(params1: FlightSearchParamsState, params2: FlightSearchParamsState) {
  return stringify(params1) === stringify(params2);
}

export function sameFilterSortByParams(
  params1: HotelFilterState,
  sortBy1: string,
  params2: HotelFilterState,
  sortBy2: string,
) {
  return stringify(params1) === stringify(params2) && sortBy1 === sortBy2;
}

export function getScoreDescription(score: number) {
  if (score < 6) {
    return 'medium';
  }
  if (score < 7) {
    return 'satisfied';
  }
  if (score < 8) {
    return 'good';
  }
  if (score < 9) {
    return 'veryGood';
  }
  if (score <= 10) {
    return 'great';
  }

  return 'medium';
}
export function getInternetIcon(id: number) {
  let icon;
  switch (id) {
    case 0: // Quầy lễ tân 24h
      icon = ic_wifi;
      break;
    case 1: //  Quầy bar
      icon = ic_wifi;
      break;
    default:
  }
  return icon;
}
export function getFacilityIcon(id: number) {
  let icon;
  switch (id) {
    case 0: // Quầy lễ tân 24h
      icon = ic_room_service24hour;
      break;
    case 1: //  Quầy bar
      icon = ic_bar;
      break;
    case 2: // Dịch vụ giặt là
      icon = ic_laundry_service;
      break;
    case 3: // Hộp đêm
      icon = ic_night_club;
      break;
    case 4: // Dịch vụ phòng
      icon = ic_door_hanger_outline;
      break;
    case 5: // Dịch vụ đưa đón
      icon = ic_shutle_service;
      break;
    case 6: //  Bãi đỗ xe ô tô
      icon = ic_parking;
      break;
    case 7: // Thu đổi ngoại tệ
      icon = ic_currency_change;
      break;
    case 8: // Dịch vụ phòng 24h
      icon = ic_room_service24hour;
      break;
    case 9: // Thang máy
      icon = ic_elevator;
      break;
    case 10: // Két sắt
      icon = ic_safety_deposit_boxes;
      break;
    case 11: // Giữ hành lý
      icon = ic_luggage_storage;
      break;
    case 12: // Quầy bar cạnh bể bơi
      icon = ic_poolside_bar;
      break;
    case 13: //  Đưa đón sân bay/khách sạn
      icon = ic_airports_transfer;
      break;
    case 14: // Người vận chuyển hành lý
      icon = ic_concierge;
      break;
    case 15: // Cửa hàng lưu niệm
      icon = ic_shops;
      break;
    case 16: // Dịch vụ du lịch
      icon = ic_tours_service;
      break;
    case 17: // Thiết bị phòng họp
      icon = ic_meeting_facilities;
      break;
    case 18: // Dịch vụ trông trẻ
      icon = ic_baby_sitting;
      break;
    case 20: // Phòng ăn
      icon = restaurant;
      break;
    case 21: // Đưa đón bến phà
      icon = ic_ferry_transfer;
      break;
    case 22: // Đưa đón ga tàu
      icon = ic_train_head;
      break;
    case 23: // Cho thuê xe máy
      icon = ic_motobike;
      break;
    case 24: // Cho thuê xe đạp
      icon = ic_bicycle;
      break;
    case 25: // Cho thuê xe hơi
      icon = ic_car_2;
      break;
    case 26: // Trợ giúp đặc biệt
      icon = ic_call;
      break;
    case 27: // Báo thức
      icon = ic_alarm;
      break;
    case 28: // Báo thức
      icon = ic_dinner;
      break;
    case 29: // Rượu vang/Sâm panh.
      icon = ic_wine;
      break;
    case 30: // Dọn phòng hàng ngày
      icon = ic_dailyCleaning;
      break;
    case 31: // Ủi đồ
      icon = ic_laundry_service;
      break;
    case 32: // Giặt khô là hơi
      icon = ic_clothes_dryer;
      break;
    case 33: // Máy fax/Photocopy
      icon = ic_photocopy;
      break;
    case 34: // Tổ chức sự kiện/hội họp
      icon = ic_tiled_floor;
      break;
    case 35: // Bãi đỗ xe thu phí
      icon = ic_hand_money;
      break;
    case 36: // Bãi đỗ xe miễn phí
      icon = ic_bus;
      break;
    case 37: // Sân thượng
      icon = ic_parquet_floors;
      break;
    case 38: // Ban công
      icon = ic_balcony;
      break;
    case 39: // Sân vườn
      icon = ic_garden;
      break;
    case 40: // Điều hòa nhiệt độ
      icon = ic_conditional;
      break;
    case 41: // Phòng không hút thuốc
      icon = ic_no_smorking;
      break;
    case 42: // Lối vào cho nười đi xe lăn
      icon = ic_facilities_disabled_guests;
      break;
    case 43: // Hỗ trợ khiếm thị: Chữ nổi
      icon = ic_braile;
      break;
    case 44: // Toilet dành cho người khuyết tật
      icon = ic_toilet_for_the_disabled;
      break;
    case 45: // Bồn rửa mặt thấp hơn
      icon = ic_wash_basin_for_the_disabled;
      break;
    case 46: // Được phép mang vật nuôi
      icon = ic_pet_face;
      break;
    case 47: // Tiền mặt
      icon = ic_money;
      break;
    case 48: // Thẻ visa master card
      icon = ic_visa_card_payable;
      break;
    case 49: // Anmerican Express/Union Pay/JCB
      icon = ic_payment_jcb;
      break;
    default:
  }
  return icon;
}
export function getRelaxIcon(id: number) {
  let icon;
  switch (id) {
    case 0: //  Bể bơi ngoài trời
      icon = ic_tours_service;
      break;
    case 1: //  Bể bơi (trẻ em)
      icon = swimming_pool;
      break;
    case 2: // Phòng thể dục
      icon = gym;
      break;
    case 3: // Xông hơi
      icon = sauna;
      break;
    case 4: // Spa
      icon = spa;
      break;
    case 5: // Mát xa
      icon = spa;
      break;
    case 6: //  Sauna
      icon = sauna;
      break;
    case 7: // Hồ bơi trong nhà
      icon = swimming_pool;
      break;
    case 8: // Hồ bơi trên sân thượng
      icon = ic_poolside_bar;
      break;
    case 9: // Dịch vụ làm đẹp
      icon = ic_beauty_salon;
      break;
    default:
  }
  return icon;
}
export function parseHotelDetailParams(params: URLSearchParams) {
  const search = parseHotelSearchParams(params, true);
  const hotelIdStr = params.get(HOTEL_BOOK_PARAMS_NAMES.hotelId);
  if (!hotelIdStr) {
    throw new Error('No hotel ID');
  }
  const hotelId = parseInt(hotelIdStr, 10);
  if (!Number.isInteger(hotelId)) {
    throw new Error('Invalid hotel ID');
  }
  const direct = params.get(HOTEL_ACTIVITY_DIRECT_PARAM);
  return { hotelId, ...search, direct: direct && JSON.parse(direct) };
}

export function parseTourDetailParams(params: URLSearchParams) {
  const idStr = params.get(TOUR_BOOK_PARAMS_NAMES.tourId);
  if (!idStr) {
    throw new Error('no id');
  }
  const tourId = parseInt(idStr, 10);
  if (!Number.isInteger(tourId)) {
    throw new Error('Invalid id');
  }
  const direct = params.get(HOTEL_ACTIVITY_DIRECT_PARAM);
  return { tourId, direct: direct && JSON.parse(direct) };
}

export function getHotDealColor(id: number) {
  if (id === 1) {
    return BLUE;
  }
  if (id === 2) {
    return '#3A30C3';
  }
  if (id === 3) {
    return '#EE7A1C';
  }
  if (id === 4) {
    return '#EC2C04';
  }
  return '#D4160B';
}

export const trainEarliestCmp = (a: some, b: some) => a.departureDate - b.departureDate;
export const trainLatestCmp = (a: some, b: some) => b.departureDate - a.departureDate;
export const trainLongestCmp = (a: some, b: some) => b.arriveDate - b.departureDate - (a.arriveDate - a.departureDate);
export const trainShortestCmp = (a: some, b: some) => a.arriveDate - a.departureDate - (b.arriveDate - b.departureDate);

export function getSeatColor(seat: some, seatingInfo: SeatingInfo) {
  if (seat.seatType === 'GP' || seat.seatType === 'GPL') {
    return '#BA68C8';
  }

  if (
    seat.vnpayStatus === TRAIN_SEAT_STATUS.BOOKED ||
    seat.vnpayStatus === TRAIN_SEAT_STATUS.NOT_FOR_SELL ||
    seat.vnpayStatus === TRAIN_SEAT_STATUS.HOLDING
  ) {
    return GREY;
  }

  const selectedSeats = [
    ...seatingInfo.adults,
    ...seatingInfo.children,
    ...seatingInfo.seniors,
    ...seatingInfo.students,
  ];

  if (selectedSeats.findIndex(obj => !isEmpty(obj) && obj.ticketId === seat.ticketId) !== -1) {
    return '#B2DFDB';
  }

  return '#fff';
}

export function checkSeatSelected(seat: some, seatingInfo: SeatingInfo) {
  if (
    seat.vnpayStatus === TRAIN_SEAT_STATUS.BOOKED ||
    seat.vnpayStatus === TRAIN_SEAT_STATUS.NOT_FOR_SELL ||
    seat.vnpayStatus === TRAIN_SEAT_STATUS.HOLDING
  ) {
    return false;
  }

  const selectedSeats = [
    ...seatingInfo.adults,
    ...seatingInfo.children,
    ...seatingInfo.seniors,
    ...seatingInfo.students,
  ];

  const sameSelected = selectedSeats.filter(obj => !isEmpty(obj) && obj.ticketId === seat.ticketId);
  if (sameSelected.length > 1) {
    return false;
  }

  return true;
}

export function getCarriageDetail(carId: number, carriages: some[]) {
  return carriages.find(obj => obj.carriageTempId === carId);
}

export function validTrainBookingParams(seatingInfo: SeatingInfo) {
  const selectedSeats = [
    ...seatingInfo.adults,
    ...seatingInfo.children,
    ...seatingInfo.seniors,
    ...seatingInfo.students,
  ];

  const emptySeats = selectedSeats.filter(obj => isEmpty(obj));
  return emptySeats.length > 0;
}

export function roundTourFilterPrice(price: number) {
  return Math.ceil(price / PRICE_STEP) * PRICE_STEP;
}
