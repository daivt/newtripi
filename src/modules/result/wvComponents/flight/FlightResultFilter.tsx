import { Button, Dialog, IconButton, Slider, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import RefreshOutlinedIcon from '@material-ui/icons/RefreshOutlined';
import { remove } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { roundUp } from '../../../../utils';
import { CheckButton, PriceLabelComponent, TimeLabelComponent, StickyDiv } from '../../../common/components/elements';
import { durationMillisecondToHour } from '../../../common/utils';
import { SlideUp } from '../../../common/wvComponents/elements';
import Header from '../../../common/wvComponents/Header';
import { DEFAULT_FILTER, PRICE_STEP, TIME_TAKE_OFF_AND_LAND } from '../../constants';
import { FlightFilterState, setFilter } from '../../redux/flightResultReducer';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  isChangeItinerary?: boolean;
  open: boolean;
  onClose(): void;
}

const FlightResultFilter: React.FC<Props> = props => {
  const { open, onClose, maxFlightDuration, maxTransitDuration, flight, dispatch } = props;
  const [filter, setFilters] = React.useState<FlightFilterState>(props.filter);

  const setPrice = React.useCallback(
    (value: number[]) => {
      setFilters({ ...filter, price: value });
    },
    [filter],
  );

  const setTimeTakeOff = React.useCallback(
    (value: some, checked: boolean) => {
      const timeTakeOff = [...filter.timeTakeOff];
      if (checked) {
        timeTakeOff.push(value);
      } else {
        remove(timeTakeOff, obj => obj.id === value.id);
      }
      setFilters({ ...filter, timeTakeOff });
    },
    [filter],
  );

  const setFlightDuration = React.useCallback(
    (value: number[]) => {
      setFilters({ ...filter, flightDuration: value });
    },
    [filter],
  );

  const setTimeLand = React.useCallback(
    (value: some, checked: boolean) => {
      const timeLand = [...filter.timeLand];
      if (checked) {
        timeLand.push(value);
      } else {
        remove(timeLand, obj => obj.id === value.id);
      }
      setFilters({ ...filter, timeLand });
    },
    [filter],
  );

  const setTransitDuration = React.useCallback(
    (value: number[]) => {
      setFilters({ ...filter, transitDuration: value });
    },
    [filter],
  );

  const setAirline = React.useCallback(
    (value: some, checked: boolean) => {
      const airline = [...filter.airline];
      if (checked) {
        airline.push(value.id);
      } else {
        remove(airline, obj => obj === value.id);
      }
      setFilters({ ...filter, airline });
    },
    [filter],
  );

  const setNumStop = React.useCallback(
    (value: number, checked: boolean) => {
      const numStops = [...filter.numStops];
      if (checked) {
        numStops.push(value);
      } else {
        remove(numStops, obj => obj === value);
      }

      setFilters({ ...filter, numStops });
    },
    [filter],
  );

  const setDefault = React.useCallback(() => {
    setFilters(DEFAULT_FILTER);
  }, []);

  return (
    <Dialog
      open={open}
      onClose={onClose}
      closeAfterTransition
      fullScreen
      TransitionComponent={SlideUp}
      onEntered={() => setFilters(props.filter)}
    >
      <Header
        action={
          <IconButton onClick={onClose} style={{ padding: '6px', width: '40px', flexShrink: 0 }} color="inherit">
            <CloseIcon />
          </IconButton>
        }
        content={
          <div style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
            <div
              style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography variant="button">
                <FormattedMessage id="result.filter" />
              </Typography>
            </div>
            <IconButton onClick={setDefault} style={{ padding: '6px', width: '40px', flexShrink: 0 }} color="inherit">
              <RefreshOutlinedIcon />
            </IconButton>
          </div>
        }
      />
      <div style={{ display: 'flex', flexDirection: 'column', padding: '16px 8px' }}>
        <div style={{ display: 'flex' }}>
          <Typography style={{ display: 'flex', flex: 1 }} variant="subtitle2">
            <FormattedMessage id="filter.showResult.price" />
          </Typography>
          <span>
            <FormattedNumber value={filter.price[0]} />
            &nbsp;
            <FormattedMessage id="currency" />
            &nbsp; - &nbsp;
            <FormattedNumber value={filter.price[1]} />
            &nbsp;
            <FormattedMessage id="currency" />
          </span>
        </div>
        <div style={{ margin: '6px 2px 2px 6px' }}>
          <Slider
            key={JSON.stringify(filter.price)}
            defaultValue={filter.price.slice()}
            min={0}
            step={PRICE_STEP}
            max={flight && flight.filters ? roundUp(flight.filters.maxPrice, PRICE_STEP) : PRICE_STEP}
            onChangeCommitted={(e: any, value: any) => setPrice(value)}
            ValueLabelComponent={PriceLabelComponent}
          />
        </div>
        <div style={{ paddingTop: '12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="filter.numberStop" />
          </Typography>
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {flight &&
              flight.filters &&
              flight.filters.numStops &&
              flight.filters.numStops.map((item: number) => (
                <div style={{ padding: '6px' }} key={item}>
                  <CheckButton
                    active={filter.numStops.indexOf(item) !== -1}
                    onClick={() => setNumStop(item, filter.numStops.indexOf(item) === -1)}
                  >
                    <Typography variant="body2">
                      <FormattedMessage
                        id={item === 0 ? 'filter.numberStop.noStop' : 'filter.numberStop.stop'}
                        values={{ num: item }}
                      />
                    </Typography>
                  </CheckButton>
                </div>
              ))}
          </div>
        </div>
        <div style={{ paddingTop: '12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="filter.sortBy.takeOff" />
          </Typography>
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {TIME_TAKE_OFF_AND_LAND.map(item => (
              <div style={{ padding: '6px' }} key={item.id}>
                <CheckButton
                  active={filter.timeTakeOff.indexOf(item) !== -1}
                  onClick={() => setTimeTakeOff(item, filter.timeTakeOff.indexOf(item) === -1)}
                >
                  <Typography variant="body2">{item.text}</Typography>
                </CheckButton>
              </div>
            ))}
          </div>
        </div>
        <div style={{ paddingTop: '12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="filter.sortBy.land" />
          </Typography>
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {TIME_TAKE_OFF_AND_LAND.map(item => (
              <div style={{ padding: '6px' }} key={item.id}>
                <CheckButton
                  active={filter.timeLand.indexOf(item) !== -1}
                  onClick={() => setTimeLand(item, filter.timeLand.indexOf(item) === -1)}
                >
                  <Typography variant="body2">{item.text}</Typography>
                </CheckButton>
              </div>
            ))}
          </div>
        </div>
        <div style={{ display: 'flex', paddingTop: '12px' }}>
          <Typography style={{ display: 'flex', flex: 1 }} variant="subtitle2">
            <FormattedMessage id="filter.leg" />
          </Typography>
          <span>
            {filter.flightDuration.length ? durationMillisecondToHour(filter.flightDuration[0]) : '0h'}
            &nbsp; - &nbsp;
            {durationMillisecondToHour(filter.flightDuration.length ? filter.flightDuration[1] : maxFlightDuration)}
          </span>
        </div>
        <div style={{ margin: '6px 2px 2px 6px' }}>
          <Slider
            key={maxFlightDuration}
            defaultValue={[0, maxFlightDuration]}
            min={0}
            max={maxFlightDuration}
            onChangeCommitted={(e: any, value: any) => setFlightDuration(value)}
            ValueLabelComponent={TimeLabelComponent}
          />
        </div>
        <div style={{ paddingTop: '12px' }}>
          <div style={{ display: 'flex' }}>
            <Typography style={{ display: 'flex', flex: 1 }} variant="subtitle2">
              <FormattedMessage id="filter.timesTransit" />
            </Typography>
            <span>
              {filter.transitDuration.length ? durationMillisecondToHour(filter.transitDuration[0]) : '0h'}
              &nbsp; - &nbsp;
              {durationMillisecondToHour(
                filter.transitDuration.length ? filter.transitDuration[1] : maxTransitDuration,
              )}
            </span>
          </div>
          <div style={{ margin: '6px 2px 2px 6px' }}>
            <Slider
              key={maxTransitDuration}
              defaultValue={[0, maxTransitDuration]}
              min={0}
              max={maxTransitDuration}
              disabled={!maxTransitDuration}
              onChangeCommitted={(e: any, value: any) => setTransitDuration(value)}
              ValueLabelComponent={TimeLabelComponent}
            />
          </div>
        </div>
        <div style={{ paddingTop: '12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="filter.airline" />
          </Typography>
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {flight &&
              flight.airlines &&
              flight.airlines.map((item: some) => (
                <div style={{ padding: '6px' }} key={item.id}>
                  <CheckButton
                    active={filter.airline.indexOf(item.id) !== -1}
                    onClick={() => setAirline(item, filter.airline.indexOf(item.id) === -1)}
                  >
                    <Typography variant="body2">{item.name}</Typography>
                  </CheckButton>
                </div>
              ))}
          </div>
        </div>
      </div>
      <StickyDiv
        style={{
          bottom: 0,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
          minHeight: '50px',
          zIndex: 1,
          width: '100%',
          boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
        }}
      >
        <Button
          variant="contained"
          color="secondary"
          style={{
            width: '90%',
            height: '75%',
          }}
          onClick={() => {
            dispatch(setFilter(filter));
            onClose();
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="apply" />
          </Typography>
        </Button>
      </StickyDiv>
    </Dialog>
  );
};
function mapStateToProps(state: AppState) {
  let maxTransitDuration = 0;
  if (state.result.flight.data) {
    if (state.booking.flight.outbound.ticket) {
      maxTransitDuration = state.result.flight.maxTransitDuration.inbound;
    } else {
      maxTransitDuration = state.result.flight.maxTransitDuration.outbound;
    }
  }
  let maxFlightDuration = 0;
  if (state.result.flight.data) {
    if (state.booking.flight.outbound.ticket) {
      maxFlightDuration = state.result.flight.maxFlightDuration.inbound;
    } else {
      maxFlightDuration = state.result.flight.maxFlightDuration.outbound;
    }
  }
  return {
    maxTransitDuration,
    maxFlightDuration,
    flight: state.result.flight.data,
    filter: state.result.flight.filterParams,
  };
}

export default connect(mapStateToProps)(FlightResultFilter);
