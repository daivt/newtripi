import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE, GREY } from '../../../../colors';
import { StickyDiv } from '../../../common/components/elements';

interface Props {
  onOpenSortBox(): void;
  onOpenFilterBox(): void;
}

const FlightResultFooterBox: React.FC<Props> = props => {
  const { onOpenSortBox, onOpenFilterBox } = props;

  return (
    <StickyDiv
      style={{
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        color: BLUE,
        border: `0.5px solid ${GREY}`,
        boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
        minHeight: '40px',
        zIndex: 1,
      }}
    >
      <Button style={{ padding: '6px 24px', flex: 1 }} size="large" onClick={onOpenSortBox}>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="result.sortBy" />
        </Typography>
      </Button>

      <div style={{ backgroundColor: BLUE, margin: '3px 0', width: '1px', height: '32px' }} />

      <Button style={{ padding: '6px 24px', flex: 1 }} size="large" onClick={onOpenFilterBox}>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="result.filter" />
        </Typography>
      </Button>
    </StickyDiv>
  );
};

export default FlightResultFooterBox;
