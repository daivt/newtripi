import { push } from 'connected-react-router';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some, WV_ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setInbound, setOutbound } from '../../../booking/redux/flightBookingReducer';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../common/utils';
import { FLIGHT_SORT_BY } from '../../constants';
import { Typography } from '@material-ui/core';
import {
  filterAndSort,
  flightCheapestCmp,
  flightCheapestRoundtripCmp,
  flightFastestCmp,
  flightTimeTakeOffCmp,
} from '../../utils';
import { ResultSkeleton } from './FlightResultMobileTablet';
import FlightTicketItem from './FlightTicketItem';

export const STEP = 10;

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
    data: state.result.flight.data,
    sortBy: state.result.flight.sortBy,
    booking: state.booking.flight,
    filterParams: state.result.flight.filterParams,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    searchCompleted: state.result.flight.searchCompleted,
    searchRequest: state.result.flight.searchParams,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightResultBoxTwoWay: React.FC<Props> = props => {
  const {
    data,
    dispatch,
    booking,
    filterParams,
    getAirlineInfo,
    searchCompleted,
    getTicketClass,
    sortBy,
    searchRequest,
  } = props;

  const [maxDisplayOutbound, setMaxDisplayOutbound] = React.useState<number>(10);
  const [maxDisplayInbound, setMaxDisplayInbound] = React.useState<number>(10);

  if (!data || !data.outbound) {
    return (
      <div style={{ padding: 16 }}>
        {<ResultSkeleton />}
        {<ResultSkeleton />}
        {<ResultSkeleton />}
        {<ResultSkeleton />}
      </div>
    );
  }

  let matchingGroupId: some;

  if (booking.outbound.ticket) {
    matchingGroupId = booking.outbound.ticket.outbound.matchingGroupId;
  }
  const matchedInboundTickets = data.inbound.tickets.filter(
    (ticket: some) => ticket.outbound.matchingGroupId === matchingGroupId,
  );
  const filteredInboundTickets = filterAndSort(
    matchedInboundTickets,
    filterParams,
    sortBy === FLIGHT_SORT_BY.cheapest
      ? flightCheapestCmp
      : sortBy === FLIGHT_SORT_BY.fastest
      ? flightFastestCmp
      : sortBy === FLIGHT_SORT_BY.takeOff
      ? flightTimeTakeOffCmp
      : undefined,
  );
  const inboundTickets = filteredInboundTickets.filter((_: some, i: number) => i < maxDisplayInbound);
  const filteredOutboundTickets = filterAndSort(
    data.outbound.tickets,
    filterParams,
    sortBy === FLIGHT_SORT_BY.cheapest
      ? flightCheapestRoundtripCmp
      : sortBy === FLIGHT_SORT_BY.fastest
      ? flightFastestCmp
      : sortBy === FLIGHT_SORT_BY.takeOff
      ? flightTimeTakeOffCmp
      : undefined,
  );
  const outboundTickets = filteredOutboundTickets.filter((_: some, i: number) => i < maxDisplayOutbound);

  return (
    <div style={{ padding: 16, flex: 1, overflow: 'auto' }}>
      {booking.outbound.ticket ? (
        <InfiniteScroll
          pageStart={1}
          initialLoad={false}
          loadMore={(page: number) => setMaxDisplayInbound(page * STEP)}
          hasMore={maxDisplayInbound < filteredInboundTickets.length}
          loader={<ResultSkeleton key={-1} />}
          useWindow={true}
        >
          {inboundTickets.map(ticket => (
            <FlightTicketItem
              getAirlineInfo={getAirlineInfo}
              getTicketClass={getTicketClass}
              key={ticket.tid}
              ticket={ticket}
              onClick={() =>
                dispatch(
                  setInbound(
                    ticket,
                    data.requestId,
                    searchRequest
                      ? {
                          numAdults: searchRequest.travellerCountInfo.adultCount,
                          numChildren: searchRequest.travellerCountInfo.childCount,
                          numInfants: searchRequest.travellerCountInfo.babyCount,
                        }
                      : {},
                  ),
                )
              }
              getPrice={(ticket: some) => <FormattedNumber value={ticket.outbound.ticketdetail.priceAdultUnit} />}
              priceRemark={
                <Typography variant="caption">
                  <FormattedMessage id="result.roundTripPrice" />
                </Typography>
              }
            />
          ))}
        </InfiniteScroll>
      ) : (
        <InfiniteScroll
          pageStart={1}
          initialLoad={false}
          loadMore={(page: number) => setMaxDisplayOutbound(page * STEP)}
          hasMore={maxDisplayOutbound < filteredOutboundTickets.length}
          loader={<ResultSkeleton key={-1} />}
          useWindow={true}
        >
          {outboundTickets.map(ticket => (
            <FlightTicketItem
              getAirlineInfo={getAirlineInfo}
              getTicketClass={getTicketClass}
              key={ticket.tid}
              ticket={ticket}
              onClick={() => {
                dispatch(
                  setOutbound(
                    ticket,
                    data.requestId,
                    searchRequest
                      ? {
                          numAdults: searchRequest.travellerCountInfo.adultCount,
                          numChildren: searchRequest.travellerCountInfo.childCount,
                          numInfants: searchRequest.travellerCountInfo.babyCount,
                        }
                      : {},
                  ),
                );
                dispatch(
                  push({
                    pathname: WV_ROUTES.flight.flightResultInbound,
                    search: window.location.search,
                  }),
                );
              }}
              getPrice={(ticket: some) => <FormattedNumber value={ticket.outbound.ticketdetail.priceAdultUnit} />}
              priceRemark={
                <Typography variant="caption">
                  <FormattedMessage id="result.roundTripPrice" />
                </Typography>
              }
            />
          ))}
        </InfiniteScroll>
      )}

      {searchCompleted < 99 && (
        <>
          {<ResultSkeleton />}
          {<ResultSkeleton />}
          {<ResultSkeleton />}
          {<ResultSkeleton />}
        </>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(FlightResultBoxTwoWay);
