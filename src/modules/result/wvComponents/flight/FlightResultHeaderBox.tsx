import { Typography, IconButton } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';
import { goBack } from 'connected-react-router';
import { Dispatch } from 'redux';
import Header from '../../../common/wvComponents/Header';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import LensIcon from '@material-ui/icons/Lens';

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    result: state.result.flight,
  };
}
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  isOutbound: boolean;
  isTwoWay: boolean;
}
const FlightResultHeaderBox: React.FC<Props> = props => {
  const { result, dispatch, isOutbound, isTwoWay } = props;
  const searchParams = result.searchParams ? result.searchParams : null;
  const departureDate = searchParams ? searchParams.departureDate.format('L') : null;
  const returnDate = searchParams && searchParams.returnDate ? searchParams.returnDate.format(' - L') : null;
  const origin = searchParams ? searchParams.origin : '';
  const destination = searchParams ? searchParams.destination : '';
  const isShowOutBound = isTwoWay ? isOutbound : true;
  const travellerCountInfo = searchParams ? searchParams.travellerCountInfo : null;
  const numberTraveller = travellerCountInfo
    ? travellerCountInfo.adultCount + travellerCountInfo.childCount + travellerCountInfo.babyCount
    : '';
  return (
    <Header
      simple
      action={
        <IconButton
          onClick={() => dispatch(goBack())}
          style={{ padding: '6px', width: '40px', flexShrink: 0 }}
          color="inherit"
        >
          <ArrowBackIosRoundedIcon />
        </IconButton>
      }
      content={
        <div style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
          <div
            style={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Typography variant="button">
              {isShowOutBound ? (
                <div>
                  <FormattedMessage id="result.pickTicketOutBound" /> {origin ? origin.code : ''} -{' '}
                  {destination ? destination.code : ''}
                </div>
              ) : (
                <div>
                  <FormattedMessage id="result.pickTicketInBound" /> {destination ? destination.code : ' '} -{' '}
                  {origin ? origin.code : ' '}
                </div>
              )}
            </Typography>
            {departureDate ? (
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                {departureDate} {returnDate ? returnDate : ''} <LensIcon style={{ margin: 8, fontSize: 6 }} />{' '}
                {numberTraveller ? numberTraveller : ''} <FormattedMessage id="guest" />
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>
      }
    />
  );
};

export default connect(mapStateToProps)(FlightResultHeaderBox);
