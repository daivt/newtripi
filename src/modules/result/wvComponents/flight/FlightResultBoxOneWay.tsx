import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../common/utils';
import { FLIGHT_SORT_BY } from '../../constants';
import { filterAndSort, flightCheapestCmp, flightFastestCmp, flightTimeTakeOffCmp } from '../../utils';
import { ResultSkeleton } from './FlightResultMobileTablet';
import FlightTicketItem from './FlightTicketItem';
import { setOutbound } from '../../../booking/redux/flightBookingReducer';

const STEP = 10;

function mapStateToProps(state: AppState) {
  return {
    data: state.result.flight.data,
    filterParams: state.result.flight.filterParams,
    sortBy: state.result.flight.sortBy,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    searchCompleted: state.result.flight.searchCompleted,
    searchRequest: state.result.flight.searchParams,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightResultBoxOneWay: React.FC<Props> = props => {
  const {
    data,
    dispatch,
    searchRequest,
    filterParams,
    getAirlineInfo,
    searchCompleted,
    getTicketClass,
    sortBy,
  } = props;
  const [maxDisplay, setMaxDisplay] = React.useState<number>(10);

  if (!data || !data.tickets) {
    return (
      <div style={{ padding: 16 }}>
        {<ResultSkeleton />}
        {<ResultSkeleton />}
        {<ResultSkeleton />}
        {<ResultSkeleton />}
      </div>
    );
  }

  const filteredTickets = filterAndSort(
    data.tickets as some[],
    filterParams,
    sortBy === FLIGHT_SORT_BY.cheapest
      ? flightCheapestCmp
      : sortBy === FLIGHT_SORT_BY.fastest
      ? flightFastestCmp
      : sortBy === FLIGHT_SORT_BY.takeOff
      ? flightTimeTakeOffCmp
      : undefined,
  );
  const tickets = filteredTickets.filter((ticket: some, i: number) => i < maxDisplay);

  return (
    <div style={{ padding: 16, flex: 1, overflow: 'auto' }}>
      <InfiniteScroll
        pageStart={1}
        initialLoad={false}
        loadMore={(page: number) => setMaxDisplay(page * STEP)}
        hasMore={maxDisplay < filteredTickets.length}
        loader={<ResultSkeleton key={-1} />}
        useWindow={true}
      >
        {tickets.map((ticket: some) => (
          <FlightTicketItem
            getAirlineInfo={getAirlineInfo}
            getTicketClass={getTicketClass}
            key={ticket.tid}
            ticket={ticket}
            onClick={() =>
              dispatch(
                setOutbound(
                  ticket,
                  data.requestId,
                  searchRequest
                    ? {
                        numAdults: searchRequest.travellerCountInfo.adultCount,
                        numChildren: searchRequest.travellerCountInfo.childCount,
                        numInfants: searchRequest.travellerCountInfo.babyCount,
                      }
                    : {},
                ),
              )
            }
            getPrice={(ticket: some) => <FormattedNumber value={ticket.outbound.ticketdetail.priceAdultUnit} />}
          />
        ))}
      </InfiniteScroll>

      {searchCompleted < 99 && (
        <>
          {<ResultSkeleton />}
          {<ResultSkeleton />}
          {<ResultSkeleton />}
          {<ResultSkeleton />}
        </>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(FlightResultBoxOneWay);
