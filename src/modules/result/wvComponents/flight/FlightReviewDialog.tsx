import { Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { PageWrapper, SlideUp } from '../../../common/wvComponents/elements';
import Header from '../../../common/wvComponents/Header';

interface Props {
  open: boolean;
  onClose(): void;
}

const FlightReviewDialog: React.FC<Props> = props => {
  const { open, onClose } = props;

  return (
    <Dialog open={open} onClose={onClose} closeAfterTransition fullScreen TransitionComponent={SlideUp}>
      <PageWrapper>
        <Header
          simple
          action={
            <IconButton onClick={onClose} style={{ padding: '6px', width: '40px', flexShrink: 0 }} color="inherit">
              <CloseIcon />
            </IconButton>
          }
          content={
            <div style={{ display: 'flex', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Typography variant="button">
                <FormattedMessage id="result.filter" />
              </Typography>
            </div>
          }
        />
        <div>Filter box</div>
      </PageWrapper>
    </Dialog>
  );
};

export default FlightReviewDialog;
