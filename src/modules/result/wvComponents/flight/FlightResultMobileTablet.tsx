import React from 'react';
import { Card } from '@material-ui/core';
import FlightResultFooterBox from './FlightResultFooterBox';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import FlightResultSortBox from './FlightResultSortBox';
import FlightResultFilter from './FlightResultFilter';
import { PageWrapper } from '../../../common/wvComponents/elements';
import { AppState } from '../../../../redux/reducers';
import Skeleton from '@material-ui/lab/Skeleton';
import FlightResultBoxOneWay from '../../wvComponents/flight/FlightResultBoxOneWay';
import FlightResultBoxTwoWay from '../../wvComponents/flight/FlightResultBoxTwoWay';
import FlightResultHeaderBox from './FlightResultHeaderBox';

export const ResultSkeleton: React.FC = () => {
  return (
    <Card elevation={1} style={{ minHeight: '112px', padding: 8, display: 'flex', marginBottom: '12px' }}>
      <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Skeleton variant="circle" width={24} height={24} style={{ margin: 0 }} />
          <Skeleton variant="text" width={100} style={{ margin: '0 0 0 8px' }} />
        </div>
        <Skeleton variant="rect" width="80%" height={40} style={{ margin: '6px 0' }} />
        <Skeleton variant="text" width="65%" style={{ margin: 0 }} />
      </div>
      <div style={{ display: 'flex', alignItems: 'flex-end', flexDirection: 'column' }}>
        <Skeleton variant="text" width={90} style={{ margin: 0 }} />
        <Skeleton variant="text" width={70} style={{ margin: '6px 0' }} />
      </div>
    </Card>
  );
};

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    result: state.result.flight,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightResultMobileTablet: React.FC<Props> = props => {
  const { result, booking } = props;
  const [showSortBox, setShowSortBox] = React.useState(false);
  const [showFilterBox, setShowFilterBox] = React.useState(false);
  return (
    <PageWrapper>
      <FlightResultHeaderBox isOutbound={!booking.outbound.ticket} isTwoWay={!!(result.data && result.data.outbound)} />
      {result.data && result.data.outbound ? <FlightResultBoxTwoWay /> : <FlightResultBoxOneWay />}
      <FlightResultFooterBox
        onOpenSortBox={() => setShowSortBox(true)}
        onOpenFilterBox={() => setShowFilterBox(true)}
      />
      <FlightResultSortBox open={showSortBox} onClose={() => setShowSortBox(false)} />
      <FlightResultFilter open={showFilterBox} onClose={() => setShowFilterBox(false)} />
    </PageWrapper>
  );
};

export default connect(mapStateToProps)(FlightResultMobileTablet);
