import React from 'react';
import { Card, Typography, IconButton } from '@material-ui/core';
import moment from 'moment';
import styled from 'styled-components';
import { useIntl, FormattedMessage } from 'react-intl';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { getStops } from '../../utils';
import { BLUE, GREEN, GREY, DARK_GREY, HOVER_GREY } from '../../../../colors';
import { ReactComponent as IconSeat } from '../../../../svg/ic_flight_ticket_seat.svg';
import { ReactComponent as IconBaggage } from '../../../../svg/ic_flight_ticket_baggage.svg';
import { SLASH_DATE_FORMAT, some } from '../../../../constants';

interface Props {
  ticket: some;
  airlines?: some[];
  onClick(): void;
  buttonMsgId?: string;
  getAirlineInfo(id: number): some;
  getTicketClass(classCode: string): some | undefined;
  getPrice(ticket: some): React.ReactNode;
  priceRemark?: JSX.Element;
}
export const BoxInfoFlight = styled.div`
  min-width: 50px;
  display: flex;
  align-items: start;
  padding-left: 10px;
`;

const FlightTicketItem: React.FC<Props> = props => {
  const { ticket, onClick, getAirlineInfo, getTicketClass, getPrice, priceRemark } = props;
  const transitTickets = ticket.outbound.transitTickets ? ticket.outbound.transitTickets : null;

  const intl = useIntl();
  const arrivalDay = moment(ticket.outbound.arrivalDayStr, SLASH_DATE_FORMAT);
  const departureDay = moment(ticket.outbound.departureDayStr, SLASH_DATE_FORMAT);
  const diffDay = arrivalDay.diff(departureDay, 'days');

  const ticketClass = getTicketClass(ticket.outbound.ticketdetail.ticketClassCode);

  const price = getPrice(ticket);
  return (
    <Card
      elevation={1}
      style={{ minHeight: '112px', padding: '5px 8px 8px 8px', display: 'flex', marginBottom: '12px' }}
      onClick={onClick}
    >
      <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
        <div style={{ display: 'flex', alignItems: 'center', paddingBottom: 14 }}>
          <img alt="" style={{ width: 24 }} src={getAirlineInfo(ticket.outbound.aid).logo} />
          <Typography style={{ margin: 2 }} variant="caption">
            {getAirlineInfo(ticket.outbound.aid).name}
          </Typography>
        </div>
        <BoxInfoFlight>
          <div
            style={{
              display: 'flex',
              flexFlow: 'column',
              position: 'relative',
            }}
          >
            <Typography variant="subtitle2">
              <span>{ticket.outbound.departureTimeStr}</span>
            </Typography>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                background: HOVER_GREY,
                borderRadius: 2,
              }}
            >
              <Typography variant="caption" color="textSecondary">
                {ticket.outbound.departureAirport}
              </Typography>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              flex: 1,
              alignItems: 'center',
              height: 25,
              padding: '0 8px',
            }}
          >
            {getStops(ticket).map(item => (
              <React.Fragment key={item.code}>
                <div
                  style={{
                    flex: 1,
                  }}
                >
                  <div
                    style={{
                      borderBottom: `2px solid ${GREY}`,
                    }}
                  />
                </div>
                <div
                  style={{
                    position: 'relative',
                    display: 'flex',
                    alignItems: 'center',
                    flexFlow: 'column',
                  }}
                >
                  <div style={{ alignItems: 'center', display: 'flex', height: 20 }}>
                    <div
                      style={{
                        border: `2px solid ${GREY}`,
                        borderRadius: '50%',
                        backgroundColor: '#FFFFFF',
                        height: item.technical ? '3px' : '6px',
                        width: item.technical ? '3px' : '6px',
                        margin: '0 1px',
                      }}
                    />
                  </div>
                  <div
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      transform: 'translateY(100%)',
                      color: DARK_GREY,
                    }}
                  >
                    <Typography variant="caption">{item.code}</Typography>
                  </div>
                </div>
              </React.Fragment>
            ))}

            <div
              style={{
                flex: 1,
              }}
            >
              <div
                style={{
                  borderBottom: `2px solid ${GREY}`,
                }}
              />
            </div>
          </div>
          <div style={{ display: 'flex', flexFlow: 'column', position: 'relative' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="subtitle2">{ticket.outbound.arrivalTimeStr}</Typography>
              <Typography variant="caption">
                &nbsp;
                {diffDay ? `+${diffDay}d` : ''}
              </Typography>
            </div>
            <div
              style={{
                display: 'flex',
              }}
            >
              <Typography
                color="textSecondary"
                variant="caption"
                style={{
                  background: HOVER_GREY,
                  minWidth: 41,
                  textAlign: 'center',
                  borderRadius: 2,
                }}
              >
                {ticket.outbound.arrivalAirport}
              </Typography>
            </div>
          </div>
        </BoxInfoFlight>
        <div style={{ display: 'flex', paddingTop: 12, alignItems: 'center' }}>
          <IconSeat style={{ width: 20, height: 20 }} />
          <Typography variant="caption" color="textSecondary">
            {ticketClass ? (intl.locale.startsWith('vi') ? ticketClass.v_name : ticketClass.i_name) : ''}
          </Typography>
          <div style={{ paddingLeft: 12, display: 'flex' }}>
            <IconBaggage style={{ height: 20, width: 20 }} />
          </div>
          <Typography variant="caption" color="textSecondary">
            {ticket.outbound.ticketdetail.freeHandBaggage}
            kg
          </Typography>
          <div
            style={{
              marginLeft: 12,
              height: 16,
              minWidth: 16,
              overflow: 'hidden',
              alignItems: 'center',
              borderRadius: '2px',
              display: 'flex',
              color: 'white',
              backgroundColor: GREEN,
              fontWeight: 500,
              justifyContent: 'center',
            }}
          >
            {transitTickets ? transitTickets[0].detailTicketClass : ticket.outbound.ticketdetail.detailTicketClass}
          </div>
        </div>
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
        <div style={{ alignItems: 'flex-end', textAlign: 'end', flex: 1 }}>
          <Typography variant="subtitle2" color="secondary">
            {price || '---'}
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
          <span style={{ color: DARK_GREY }}>{priceRemark}</span>
        </div>
        <div
          style={{
            flex: 1,
            display: 'flex',
            alignItems: 'flex-end',
          }}
        >
          <IconButton style={{ color: BLUE, padding: 3 }}>
            <ArrowDropDownIcon />
          </IconButton>
        </div>
      </div>
    </Card>
  );
};

export default FlightTicketItem;
