import { Backdrop, Dialog, Divider, Typography, List, ListItem, ListItemText } from '@material-ui/core';
import React from 'react';
import { SlideUp } from '../../../common/wvComponents/elements';
import { FLIGHT_SORT_BY_DATA } from '../../../result/constants';
import { FormattedMessage } from 'react-intl';
import { PRIMARY } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { setSortBy } from '../../redux/flightResultReducer';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  open: boolean;
  onClose(): void;
}

const FlightResultSortBox: React.FC<Props> = props => {
  const { open, onClose, sortBy, dispatch } = props;
  const handleListItemClick = (value: string) => {
    dispatch(setSortBy(value));
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 300,
      }}
      TransitionComponent={SlideUp}
    >
      <div
        style={{
          position: 'fixed',
          bottom: 0,
          right: 0,
          outline: 0,
          left: 0,
          backgroundColor: '#fff',
        }}
      >
        <Typography variant="subtitle2" style={{ padding: 12 }}>
          <FormattedMessage id="filter.showResultBy" />
        </Typography>
        <Divider />
        <List style={{ padding: 0 }}>
          {FLIGHT_SORT_BY_DATA.map(item => (
            <ListItem
              style={{ backgroundColor: '#fff', padding: 12 }}
              key={item.id}
              button
              onClick={event => handleListItemClick(item.value)}
            >
              <ListItemText style={{ color: item.value === sortBy ? PRIMARY : '' }}>
                <FormattedMessage id={item.text} />
              </ListItemText>
            </ListItem>
          ))}
        </List>
      </div>
    </Dialog>
  );
};
function mapStateToProps(state: AppState) {
  return {
    sortBy: state.result.flight.sortBy,
  };
}

export default connect(mapStateToProps)(FlightResultSortBox);
