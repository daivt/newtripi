import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import HeaderGoBack from '../../../common/wvComponents/HeaderGoBack';
import HotelResultBox from './HotelResultBox';
import HotelResultUtilitySection from './HotelResultUtilitySection';

const Point = styled.div`
  width: 4px;
  height: 4px;
  border-radius: 4px;
  background: white;
`;

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const mapState2Props = (state: AppState) => {
  return {
    data: state.result.hotel.data,
    params: state.result.hotel.lastParams,
  };
};

const HotelResultMobileTablet: React.FC<Props> = props => {
  const { data, params } = props;
  return (
    <PageWrapper>
      <HeaderGoBack
        content={
          params && (
            <div style={{ textAlign: 'center' }}>
              <Typography variant="subtitle2">{params.searchParams.location.provinceName}</Typography>
              <Typography variant="caption" style={{ display: 'flex', alignItems: 'center' }} noWrap>
                {`${params.searchParams.checkIn.format('L')}-${params.searchParams.checkOut.format('L')}`}
                &nbsp;
                <Point />
                &nbsp;
                <FormattedMessage
                  id="result.totalPassengers"
                  values={{
                    num: params.searchParams.guestInfo.adultCount + params.searchParams.guestInfo.childCount,
                  }}
                />
                &nbsp;
                <Point />
                &nbsp;
                <FormattedMessage
                  id="search.guestInfo.roomCount"
                  values={{
                    roomCount: params.searchParams.guestInfo.roomCount,
                  }}
                />
              </Typography>
            </div>
          )
        }
        simple
      />
      <HotelResultBox data={data} />
      <HotelResultUtilitySection />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(HotelResultMobileTablet);
