import { Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import IconLocation from '@material-ui/icons/LocationOnOutlined';
import Rating from '@material-ui/lab/Rating';
import Skeleton from '@material-ui/lab/Skeleton';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY, PRIMARY, RED, SECONDARY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconDiscount } from '../../../../svg/ic_discount.svg';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import { getScoreDescription } from '../../utils';

const Wrapper = styled.div<{ skeleton: boolean }>`
  min-height: 165px;
  display: flex;
  border-radius: 4px;
  margin-bottom: 12px;
  border: 1px solid ${GREY};
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);
  background-color: #fff;
  :hover {
    background: ${props => (props.skeleton ? undefined : fade(SECONDARY, 0.05))};
  }
`;
interface Props {
  hotel?: some;
}
const HotelResultItem: React.FC<Props> = props => {
  const { hotel } = props;
  if (!hotel) {
    return (
      <Wrapper skeleton>
        <div style={{ width: '120px', position: 'relative' }}>
          <Skeleton
            variant="rect"
            style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          />
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', flex: 1, paddingLeft: '8px' }}>
          <Skeleton width="80%" />
          <Skeleton width="50%" />
        </div>
      </Wrapper>
    );
  }

  const { bestAgencyPrices } = hotel;
  const promotionInfo = bestAgencyPrices && bestAgencyPrices.length ? bestAgencyPrices[0].promotionInfo : undefined;

  return (
    <Wrapper skeleton={false}>
      <div style={{ width: '120px', position: 'relative' }}>
        <ProgressiveImage
          style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          src={hotel.logo}
          alt=""
        />
        {hotel.includeBreakfast && (
          <div
            style={{
              width: '88px',
              height: '24px',
              background: PRIMARY,
              position: 'absolute',
              top: 12,
              left: 0,
            }}
          >
            <Typography variant="caption" style={{ color: 'white' }}>
              &nbsp;
              <FormattedMessage id="result.hotel.includeBreakfast" />
            </Typography>
          </div>
        )}
        {hotel.numberOfRemainingRooms <= 3 && (
          <div
            style={{
              width: '120px',
              height: '24px',
              background: RED,
              position: 'absolute',
              bottom: 0,
              left: 0,
            }}
          >
            <Typography variant="caption" style={{ color: 'white' }}>
              &nbsp;
              <FormattedMessage id="result.hotel.remainingRoom" values={{ num: hotel.numberOfRemainingRooms }} />
            </Typography>
          </div>
        )}
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', flex: 1, paddingLeft: '8px' }}>
        <div style={{ minHeight: '38px', display: 'flex', alignItems: 'center' }}>
          <Typography
            variant="body2"
            style={{
              fontWeight: 'bold',
              display: '-webkit-box',
              WebkitLineClamp: 2,
              WebkitBoxOrient: 'vertical',
              overflow: 'hidden',
            }}
          >
            {hotel.name}
          </Typography>
        </div>
        <Rating value={hotel.starNumber} readOnly size="small" />
        <div style={{ display: 'flex', paddingTop: '4px', alignItems: 'center' }}>
          <div
            style={{
              width: '30px',
              height: '30px',
              backgroundColor: BLUE,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '4px',
            }}
          >
            <Typography variant="caption" style={{ color: '#fff' }}>
              <FormattedNumber value={hotel.overallScore / 10} />
            </Typography>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              flexDirection: 'column',
              paddingLeft: '4px',
            }}
          >
            <Typography variant="caption" style={{ color: BLUE, fontWeight: 500 }}>
              <FormattedMessage id={getScoreDescription(hotel.overallScore / 10)} />
            </Typography>
            <Typography variant="caption" color="textSecondary">
              <FormattedNumber value={hotel.numberOfReviews} maximumFractionDigits={1} minimumFractionDigits={1} />
              &nbsp;
              <FormattedMessage id="hotel.result.review" />
            </Typography>
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <IconLocation style={{ color: DARK_GREY, padding: '2px', width: '20px', height: '20px' }} />
          <Typography
            variant="caption"
            style={{
              paddingLeft: '6px',
              color: DARK_GREY,
              display: '-webkit-box',
              WebkitLineClamp: 1,
              WebkitBoxOrient: 'vertical',
              overflow: 'hidden',
            }}
          >
            {hotel.address}
          </Typography>
        </div>

        <div style={{ marginRight: '8px' }}>
          {promotionInfo && !!promotionInfo.discountPercentage && (
            <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
              <IconDiscount className="svgFill" style={{ fill: PRIMARY, width: '20px', height: '20px' }} />
              <Typography variant="caption">
                <FormattedMessage id="booking.hotel.saveAmount" values={{ amount: promotionInfo.discountPercentage }} />
              </Typography>
            </div>
          )}
          {hotel.minPrice < promotionInfo?.priceBeforePromotion && (
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Typography variant="body2" style={{ textDecoration: 'line-through', color: GREY }}>
                <FormattedNumber value={promotionInfo.priceBeforePromotion} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
          )}
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            <Typography variant="h6" style={{ color: SECONDARY, display: 'flex' }}>
              <FormattedNumber value={hotel.minPrice || 0} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </div>
          <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', minHeight: '26px' }}>
            <Typography variant="caption" color="textSecondary">
              <FormattedMessage id="result.hotel.includeTaxesFeesAndInvoices" />
            </Typography>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default HotelResultItem;
