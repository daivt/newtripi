import { Button as MUIButton, Dialog, IconButton, Typography, Slider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import RefreshIcon from '@material-ui/icons/Refresh';
import { findIndex, remove } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { DARK_GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { SlideUp } from '../../../common/wvComponents/elements';
import { renderStar } from '../../components/hotel/HotelResultFilter';
import { Button } from '../../components/hotel/HotelResultFilter.styles';
import { Facilities, FILTER_MAX_PRICE, HotelStars, HotelTypes, PRICE_STEP } from '../../constants';
import { setFilter } from '../../redux/hotelResultReducer';
import { PriceLabelComponent } from '../../../common/components/elements';

const TitleBar = styled.div`
  width: 100%;
  height: 48px;
  background: ${PRIMARY};
  display: flex;
`;

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  openFilterBox: boolean;
  onClose: () => void;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

function mapState2Props(state: AppState) {
  return {
    filter: state.result.hotel.filterParams,
    subLocations: state.result.hotel.data ? state.result.hotel.data.filters.subLocations : [],
  };
}

const HotelResultFilter: React.FC<Props> = props => {
  const { openFilterBox, filter, dispatch, onClose } = props;
  const [price, setPrice] = React.useState(filter.price);
  const [stars, setStars] = React.useState(filter.stars);
  const [subLocations, setSubLocations] = React.useState(filter.subLocations);
  const [selectedTypes, setSelectedTypes] = React.useState(filter.hotelTypes);
  const [facilities, setFacilities] = React.useState(filter.facilities);

  const resetFilter = React.useCallback(() => {
    setPrice(filter.price);
    setStars(filter.stars);
    setSubLocations(filter.subLocations);
    setSelectedTypes(filter.hotelTypes);
    setFacilities(filter.facilities);
  }, [filter]);

  return (
    <Dialog fullScreen open={openFilterBox} keepMounted={false} TransitionComponent={SlideUp}>
      <TitleBar>
        <IconButton style={{ position: 'absolute', color: 'white' }} size="medium" onClick={onClose}>
          <CloseIcon style={{ fontSize: '24px' }} />
        </IconButton>
        <Typography style={{ color: 'white', margin: 'auto' }} variant="body1">
          <FormattedMessage id="result.filter" />
        </Typography>
        <IconButton style={{ color: 'white' }} size="medium">
          <RefreshIcon style={{ fontSize: '24px' }} onClick={() => resetFilter()} />
        </IconButton>
      </TitleBar>
      <div style={{ display: 'flex', flexDirection: 'column', padding: '0px 20px' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            paddingTop: '16px',
          }}
        >
          <Typography variant="subtitle2">
            <FormattedMessage id="hotel.filter.roomPrice" />
          </Typography>
          <Typography variant="body2" color="textSecondary">
            <FormattedNumber value={filter.price[0]} />
            &nbsp;
            <FormattedMessage id="currency" />
            &nbsp;-&nbsp;
            <FormattedNumber value={filter.price[1]} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </div>
        <div style={{ position: 'relative', pointerEvents: 'visible', marginTop: '10px' }}>
          <Slider
            key={JSON.stringify(price)}
            defaultValue={price.slice()}
            min={0}
            max={FILTER_MAX_PRICE}
            step={PRICE_STEP}
            onChangeCommitted={(e: any, value: any) => setPrice(value)}
            ValueLabelComponent={PriceLabelComponent}
          />
        </div>
        <Typography variant="subtitle2" style={{ paddingBottom: '6px', marginTop: '10px' }}>
          <FormattedMessage id="hotel.filter.starRate" />
        </Typography>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {HotelStars.map(star => (
            <Button
              size="small"
              variant="outlined"
              selected={stars.indexOf(star.id) !== -1}
              key={star.id}
              onClick={() => {
                const newStars = [...stars];
                remove(newStars, id => id === star.id);
                if (newStars.length === stars.length) {
                  newStars.push(star.id);
                }
                setStars(newStars);
              }}
              style={{ marginRight: '4px' }}
            >
              {renderStar(star.id)}
            </Button>
          ))}
        </div>
        <Typography variant="subtitle2" style={{ marginTop: '10px' }}>
          <FormattedMessage id="hotel.filter.hotelType" />
        </Typography>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {HotelTypes.map(item => (
            <Button
              size="small"
              variant="outlined"
              selected={findIndex(selectedTypes, o => o === item.id) !== -1}
              key={item.id}
              onClick={() => {
                const types = [...selectedTypes];
                remove(types, id => id === item.id);
                if (types.length === selectedTypes.length) {
                  types.push(item.id);
                }
                setSelectedTypes(types);
              }}
              style={{ marginRight: '4px', padding: '5px 15px' }}
            >
              <Typography
                variant="body2"
                style={{
                  color: findIndex(selectedTypes, o => o === item.id) !== -1 ? 'white' : DARK_GREY,
                }}
              >
                <FormattedMessage id={item.name} />
              </Typography>
            </Button>
          ))}
        </div>
        <Typography variant="subtitle2" style={{ marginTop: '10px' }}>
          <FormattedMessage id="hotel.filter.area" />
        </Typography>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {props.subLocations.map((item: some, index: number) => (
            <Button
              size="small"
              variant="outlined"
              selected={subLocations.indexOf(item.ids[0]) !== -1}
              key={item.name}
              onClick={() => {
                const locations = [...subLocations];
                remove(locations, ids => item.ids.indexOf(ids) !== -1);
                if (locations.length === subLocations.length) {
                  locations.push(...item.ids);
                }
                setSubLocations(locations);
              }}
              style={{ marginRight: '4px', padding: '5px 15px' }}
            >
              <Typography
                variant="body2"
                style={{
                  color: subLocations.indexOf(item.ids[0]) !== -1 ? 'white' : DARK_GREY,
                }}
              >
                {item.name}
                &nbsp;&#40;
                <FormattedNumber value={item.numHotels} />
                &#41;
              </Typography>
            </Button>
          ))}
        </div>
        <Typography variant="subtitle2" style={{ marginTop: '10px' }}>
          <FormattedMessage id="hotel.filter.facility" />
        </Typography>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {Facilities.map((item: some) => (
            <Button
              size="small"
              variant="outlined"
              selected={facilities.indexOf(item.id) !== -1}
              key={item.id}
              onClick={() => {
                const selectedFacilities = [...facilities];
                remove(selectedFacilities, id => id === item.id);
                if (selectedFacilities.length === facilities.length) {
                  selectedFacilities.push(item.id);
                }
                setFacilities(selectedFacilities);
              }}
              style={{ marginRight: '4px', padding: '5px 15px' }}
            >
              <Typography
                variant="body2"
                style={{
                  color: facilities.indexOf(item.id) !== -1 ? 'white' : DARK_GREY,
                }}
              >
                <FormattedMessage id={item.name} />
              </Typography>
            </Button>
          ))}
        </div>

        <MUIButton
          variant="contained"
          color="secondary"
          style={{ width: '100%', marginTop: '40px' }}
          onClick={() => {
            dispatch(setFilter({ price, stars, subLocations, facilities, hotelTypes: selectedTypes }));
            onClose();
          }}
        >
          <FormattedMessage id="apply" />
        </MUIButton>
      </div>
    </Dialog>
  );
};

export default connect(mapState2Props)(injectIntl(HotelResultFilter));
