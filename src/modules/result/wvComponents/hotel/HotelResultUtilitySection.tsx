import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY } from '../../../../colors';
import { StickyDiv } from '../../../common/components/elements';
import HotelResultFilter from './HotelResultFilter';
import HotelResultSortBox from './HotelResultSortBox';

const FilterWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface Props {}

const HotelResultUtilitySection: React.FC<Props> = props => {
  const [openSortBox, setOpenSortBox] = React.useState(false);
  const [openFilterBox, setOpenFilterBox] = React.useState(false);
  return (
    <StickyDiv
      style={{
        minHeight: '40px',
        border: `0.5px solid ${GREY}`,
        boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
        bottom: 0,
        zIndex: 100,
        background: 'white',
        padding: '6px 30px',
        display: 'flex',
        flexDirection: 'row',
      }}
    >
      <FilterWrapper onClick={() => setOpenSortBox(true)}>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="result.sortBy" />
        </Typography>
      </FilterWrapper>
      <HotelResultSortBox openSortbox={openSortBox} onClose={() => setOpenSortBox(false)} />
      <FilterWrapper
        onClick={() => setOpenFilterBox(true)}
        style={{ borderLeft: `1px solid ${BLUE}`, borderRight: `1px solid ${BLUE}` }}
      >
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="result.filter" />
        </Typography>
      </FilterWrapper>
      <HotelResultFilter openFilterBox={openFilterBox} onClose={() => setOpenFilterBox(false)} />
      <FilterWrapper>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="result.map" />
        </Typography>
      </FilterWrapper>
    </StickyDiv>
  );
};

export default HotelResultUtilitySection;
