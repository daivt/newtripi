import { Modal, Slide, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { BLACK_TEXT, PRIMARY } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { SORT_BY } from '../../components/hotel/HotelResultSortBox';
import { setSortBy } from '../../redux/hotelResultReducer';

const ModalPaper = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  background: white;
  min-height: 200px;
  padding: 10px 20px;
`;
const Line = styled.div`
  height: 45px;
  display: flex;
  align-items: center;
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  openSortbox: boolean;
  onClose: () => void;
}

function mapStateToProps(state: AppState) {
  return {
    sortBy: state.result.hotel.sortBy,
  };
}

const HotelResultSortBox: React.FC<Props> = props => {
  const { openSortbox, sortBy, dispatch, onClose } = props;
  const onSelect = React.useCallback(
    value => {
      dispatch(setSortBy(value));
      onClose();
    },
    [dispatch, onClose],
  );
  return (
    <Modal open={openSortbox} keepMounted={false} onClose={onClose}>
      <Slide in={openSortbox} direction="up">
        <ModalPaper>
          <Line>
            <Typography variant="subtitle2" color="textSecondary">
              <FormattedMessage id="filter.showResultBy" />
            </Typography>
          </Line>
          {SORT_BY.map(item => (
            <Line key={item.id} onClick={() => onSelect(item.value)}>
              <Typography variant="body2" style={{ color: item.value === sortBy ? PRIMARY : BLACK_TEXT }}>
                <FormattedMessage id={item.text} />
              </Typography>
            </Line>
          ))}
        </ModalPaper>
      </Slide>
    </Modal>
  );
};

export default connect(mapStateToProps)(HotelResultSortBox);
