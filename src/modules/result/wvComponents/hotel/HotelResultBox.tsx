import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { PAGE_SIZE, ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setParams } from '../../../booking/redux/hotelBookingReducer';
import Link from '../../../common/components/Link';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../../common/constants';
import { stringifyHotelSearchState } from '../../../search/utils';
import SmartResultItem, { useSmartRenderResults } from '../../components/SmartResultItem';
import { searchMore } from '../../redux/hotelResultReducer';
import HotelResultItem from './HotelResultItem';

function mapStateToProps(state: AppState) {
  return {
    searching: state.result.hotel.searching,
    params: state.result.hotel.lastParams,
    pageOffset: state.result.hotel.pageOffset,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some | undefined;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const HotelResultBox: React.FC<Props> = props => {
  const { data, dispatch, params, pageOffset, searching } = props;
  const hotels = data ? (data.hotels as some[]) : null;

  const { itemRefs, itemHeights, visibleBounds } = useSmartRenderResults(hotels || []);

  return (
    <div style={{ marginTop: '15px', padding: '0px 15px', flex: 1 }}>
      {params && hotels ? (
        <InfiniteScroll
          pageStart={1}
          initialLoad={false}
          loadMore={() => {
            if (!searching) {
              dispatch(searchMore());
            }
          }}
          hasMore={data && data.totalResults > pageOffset * PAGE_SIZE}
          loader={<HotelResultItem key="loader" />}
          useWindow
        >
          {hotels.map((hotel, i) => (
            <SmartResultItem
              key={hotel.id}
              ref={itemRefs[i]}
              height={itemHeights[i]}
              visible={i >= visibleBounds.start && i < visibleBounds.end}
            >
              <Link
                to={{
                  pathname: `${ROUTES.hotel.hotelDetail}`,
                  search: `?${stringifyHotelSearchState(params.searchParams, undefined, undefined, true)}&${
                    HOTEL_BOOK_PARAMS_NAMES.hotelId
                  }=${hotel.id}`,
                  state: { backableToResult: true },
                }}
                onClick={() => {
                  dispatch(setParams({ ...params.searchParams, hotelId: hotel.id }));
                }}
                style={{ color: 'inherit' }}
              >
                <HotelResultItem hotel={hotel} />
              </Link>
            </SmartResultItem>
          ))}
        </InfiniteScroll>
      ) : (
        <>
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
        </>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(HotelResultBox);
