import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { DARK_GREY, GREEN } from '../../../../colors';
import { some, WV_ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setTourData } from '../../../booking/redux/tourBookingReducer';
import Link from '../../../common/components/Link';

const Line = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 30px;
`;

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  tourData: some;
}

const PickTourBox: React.FunctionComponent<Props> = props => {
  const { dispatch, tourData, router } = props;

  return (
    <div
      style={{
        display: 'flex',
        width: '100%',
        minHeight: '64px',
        padding: '8px 16px',
        boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
        background: 'white',
      }}
    >
      <Line style={{ alignItems: 'flex-start', flexDirection: 'column', flex: 1 }}>
        {tourData.originPrice > 0 && (
          <Typography variant="caption" style={{ textDecoration: 'line-through', color: DARK_GREY }}>
            <FormattedNumber value={tourData.originPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        )}
        <Typography variant="subtitle2" color="secondary">
          {tourData && tourData.price ? <FormattedNumber value={tourData.price} /> : 0}
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
        {tourData && tourData.bonusPoint > 0 && (
          <Typography variant="caption" style={{ color: GREEN }}>
            +
            <FormattedNumber value={tourData.bonusPoint} />
            &nbsp;
            <FormattedMessage id="booking.point" />
          </Typography>
        )}
      </Line>

      <Line style={{ justifyContent: 'flex-end' }}>
        {tourData && (
          <Link
            onClick={() => dispatch(setTourData(tourData))}
            to={{
              search: router.location.search,
              pathname: `${WV_ROUTES.tour.tourBookingInfo}`,
              state: { ...router.location.state, backableToDetail: true },
            }}
          >
            <Button size="small" variant="contained" color="secondary" style={{ minWidth: '130px', minHeight: '36px' }}>
              <FormattedMessage id="tour.continue" />
            </Button>
          </Link>
        )}
      </Line>
    </div>
  );
};

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(PickTourBox);
