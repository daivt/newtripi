import { Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { Extend } from '../../../components/hotel/HotelDetailTabs/styles';

interface Props {
  tour: some;
}

const ServiceBox: React.FunctionComponent<Props> = props => {
  const { tour } = props;
  const [extend, setExtend] = React.useState(false);
  return (
    <>
      {tour && tour.tourServices.length > 0 && (
        <>
          <Grid container spacing={2}>
            {tour.tourServices
              .slice(0, extend ? tour.tourServices.length : 7)
              .map((v: some, index: number) => (
                <Grid item xs={12} style={{ display: 'flex', alignItems: 'center' }} key={index}>
                  <img
                    style={{ marginRight: '12px', width: '24px', height: '24px' }}
                    src={v.iconUrl}
                    alt=""
                  />
                  <Typography variant="body2">{v.description}</Typography>
                </Grid>
              ))}
            {tour.tourServices.length > 8 && (
              <Grid item xs={12} style={{ display: 'flex', alignItems: 'center' }}>
                <Extend onClick={() => setExtend(!extend)}>
                  <FormattedMessage
                    id={extend ? 'hotel.result.info.collapse' : 'hotel.result.info.seeAll'}
                  />
                </Extend>
              </Grid>
            )}
          </Grid>
        </>
      )}
    </>
  );
};

export default ServiceBox;
