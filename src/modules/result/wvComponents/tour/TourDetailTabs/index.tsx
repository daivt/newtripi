import { Divider, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import TourBasicInfo from '../../../../common/components/Tour/TourBasicInfo';
// import TourBasicInfoBox from '../TourBasicInfoBox';
import CommonInfoBox from './CommonInfoBox';
import DetailActivityBox from './DetailActivityBox';
import FavoriteBox from './FavoriteBox';
import ReviewBox from './ReviewBox';
import ServiceBox from './ServiceBox';

const tabs = [
  { id: 'tour.commonInfo', content: <></> },
  { id: 'tour.reView', content: <></> },
  { id: 'tour.detailActivity', content: <></> },
  { id: 'tour.refund', content: <></> },
  { id: 'tour.moreInfo', content: <></> },
  { id: 'tour.favorite', content: <></> },
];

interface Props {
  tour: some;
}

const TourDetailTabs: React.FunctionComponent<Props> = props => {
  const { tour } = props;
  return (
    <div style={{ marginTop: '16px', marginBottom: '32px' }}>
      <div style={{ padding: '8px 16px' }}>
        <CommonInfoBox tour={tour} />
        <Divider style={{ margin: '16px 0px', color: '#f4f4f4' }} />
        <ServiceBox tour={tour} />
        <Divider style={{ margin: '16px 0px 8px 0px', color: '#f4f4f4' }} />
        <TourBasicInfo tour={tour} />
        <Divider style={{ margin: '8px 0px 16px 0px', color: '#f4f4f4' }} />
        <Typography id={tabs[1].id} variant="h6" style={{ margin: '0px 0px 8px 0px' }}>
          {tour.numberOfComments >= 0 && `${tour.numberOfComments} `}
          <FormattedMessage id={tabs[1].id} />
        </Typography>
        <ReviewBox tour={tour} />
        <DetailActivityBox tour={tour} tabs={tabs} />
      </div>
      <Typography id={tabs[5].id} variant="h6" style={{ margin: '16px' }}>
        <FormattedMessage id={tabs[5].id} />
      </Typography>
      <FavoriteBox tour={tour} />
    </div>
  );
};

export default TourDetailTabs;
