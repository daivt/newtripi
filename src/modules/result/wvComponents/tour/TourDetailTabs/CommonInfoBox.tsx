import { Grid, Typography } from '@material-ui/core';
import 'font-awesome/css/font-awesome.min.css';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE, DARK_GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { ReactComponent as IcClock } from '../../../../../svg/ic_clock.svg';
import { ReactComponent as IcHotDeal } from '../../../../../svg/ic_hot_deal.svg';

interface Props {
  tour: some;
}

const CommonInfoBox: React.FunctionComponent<Props> = props => {
  const { tour } = props;
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} style={{ display: 'flex', alignItems: 'center' }}>
          <IcClock />
          <Typography style={{ marginLeft: '12px' }} variant="body2">
            {tour.durationString}
          </Typography>
        </Grid>
        {tour.means.map((v: some) => (
          <Grid key={v.id} item xs={12} style={{ display: 'flex', alignItems: 'center' }}>
            <i
              style={{ fontSize: '20px', padding: '0px 4px', color: DARK_GREY }}
              className={v.vehicleIconURL}
            />
            <Typography style={{ marginLeft: '12px' }} variant="body2">
              {v.vehicleName}
            </Typography>
          </Grid>
        ))}
        <Grid item xs={12} style={{ display: 'flex', alignItems: 'center' }}>
          <IcClock />
          <Typography style={{ marginLeft: '12px', color: BLUE }} variant="body2">
            {tour.startedSelling ? (
              <FormattedMessage id="tour.startedSelling" />
            ) : (
              <FormattedMessage id="tour.startedSellingFrom" values={{ date: tour.openDate }} />
            )}
          </Typography>
        </Grid>
        {tour.instantBooking && (
          <Grid item xs={12} style={{ display: 'flex', alignItems: 'center' }}>
            <IcHotDeal />
            <Typography style={{ marginLeft: '12px' }} variant="body2">
              <FormattedMessage id="tour.detail.hotDeal" />
            </Typography>
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default CommonInfoBox;
