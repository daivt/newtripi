import * as React from 'react';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { skeleton } from '../../../constants';
import { fetchTourSuggest } from '../../../redux/tourResultReducer';
import TourResultItem from '../TourResultItem';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  tour: some;
}

interface State {
  suggest?: some[];
}

class FavoriteBox extends React.PureComponent<Props, State> {
  state: State = {};

  async componentDidMount() {
    const { dispatch, tour } = this.props;
    const json = await dispatch(fetchTourSuggest(tour.id));
    if (json.code === 200) {
      this.setState({
        suggest: json.data,
      });
    }
  }

  public render() {
    const { suggest } = this.state;
    return (
      <div style={{ overflow: 'hidden' }}>
        {suggest ? (
          <Slider
            slidesToShow={1}
            slidesToScroll={1}
            infinite={false}
            centerMode
            centerPadding="16px"
            className="center"
          >
            {suggest.map(tour => (
              <TourResultItem key={tour.id} data={tour} styleCSS={{ marginRight: 24, marginBottom: 8 }} />
            ))}
          </Slider>
        ) : (
          <Slider
            slidesToShow={1}
            slidesToScroll={1}
            infinite={false}
            centerMode
            centerPadding="16px"
            className="center"
          >
            {skeleton(2).map(v => (
              <TourResultItem key={v.id} data={v} styleCSS={{ marginRight: 24, marginBottom: 8 }} />
            ))}
          </Slider>
        )}
      </div>
    );
  }
}

export default connect()(FavoriteBox);
