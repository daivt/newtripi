import { Dialog, Typography, IconButton } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, HOVER_GREY, RED } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import { SlideUp } from '../../../../common/wvComponents/elements';
import PopupHeader from '../../../../common/wvComponents/PopupHeader';
import { setReviewImagesToShow } from '../../../redux/tourResultReducer';
import CommentCard from '../TourDetailImageBox/CommentCard';
import RatingBox from '../TourDetailImageBox/RatingBox';
import CloseIcon from '@material-ui/icons/Close';
import ReactImageGallery from 'react-image-gallery';
import { Image } from '../../../components/tour/TourDetailImageBox';
interface Props extends ReturnType<typeof mapStateToProps> {
  tour: some;
  dispatch: Dispatch;
}
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;
const ReviewBox: React.FunctionComponent<Props> = props => {
  const { tour, listImagesToShow, dispatch } = props;
  const [extend, setExtend] = React.useState(false);
  React.useEffect(() => {
    const escAction = (event: some) => {
      if (event.keyCode === 27) {
        setExtend(false);
        dispatch(setReviewImagesToShow());
      }
    };
    window.addEventListener('keyup', escAction);

    return () => window.removeEventListener('keyup', escAction);
  }, [dispatch]);
  return (
    <>
      {tour.comments.slice(0, 2).map((v: some, index: number) => {
        return <CommentCard key={index} data={v} style={{ justifyContent: 'flex-start' }} />;
      })}
      <Extend onClick={() => setExtend(!extend)}>
        <Typography variant="caption">
          <FormattedMessage id="tour.seeAllReview" />
        </Typography>
      </Extend>
      {listImagesToShow && (
        <Dialog
          open={!!listImagesToShow}
          TransitionComponent={SlideUp}
          fullScreen
          BackdropProps={{ style: { background: 'black' } }}
          PaperProps={{
            style: {
              margin: 0,
              background: 'transparent',
              maxHeight: '100%',
              height: '100%',
              maxWidth: '100%',
              borderRadius: 0,
            },
          }}
        >
          <IconButton
            style={{ alignSelf: 'flex-end' }}
            color="default"
            size="medium"
            onClick={() => dispatch(setReviewImagesToShow())}
          >
            <CloseIcon style={{ color: 'white' }} />
          </IconButton>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
              justifyContent: 'center',
              padding: '0px 8px',
            }}
          >
            <ReactImageGallery
              items={listImagesToShow.data.photos.map((v: some) => {
                return {
                  original: v.thumbLink as string,
                  thumbnail: v.thumbLink as string,
                };
              })}
              renderItem={item => <Image src={item.original} alt={item.originalTitle} />}
              showFullscreenButton={false}
              infinite={true}
              thumbnailPosition={'bottom'}
              showThumbnails={true}
              showIndex={true}
              startIndex={listImagesToShow.index}
            />
          </div>
        </Dialog>
      )}
      <Dialog
        open={extend}
        onClose={() => setExtend(false)}
        TransitionComponent={SlideUp}
        fullScreen
      >
        <PopupHeader
          onClose={() => setExtend(false)}
          title={
            <Typography
              variant="subtitle1"
              style={{ color: 'white', textAlign: 'center', flex: 1 }}
            >
              <FormattedMessage id="hotel.result.info.review" />
            </Typography>
          }
        />
        <Line
          style={{
            background: HOVER_GREY,
            minHeight: '48px',
            padding: '0px 16px',
          }}
        >
          <Line>
            <Line
              style={{
                background: BLUE,
                borderRadius: '4px',
                minWidth: '24px',
                height: '24px',
                padding: '0px 2px',
                justifyContent: 'center',
              }}
            >
              <Typography variant="subtitle1" style={{ color: 'white' }}>
                <FormattedNumber value={tour.expertRating} />
              </Typography>
            </Line>
            <Typography variant="caption" style={{ marginLeft: '8px' }}>
              {tour.numberOfComments >= 0 && `${tour.numberOfComments} `}
              <FormattedMessage id="tour.reView" />
            </Typography>
          </Line>
        </Line>

        <RatingBox tour={tour} />
      </Dialog>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return { listImagesToShow: state.result.tour.reviewImagesToShow };
};
export default connect(mapStateToProps)(ReviewBox);
