import { ButtonBase, Dialog, Radio, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../redux/reducers';
import { SlideUp } from '../../../common/wvComponents/elements';
import { TOUR_SORT_BY } from '../../constants';
import { setSortBy } from '../../redux/tourResultReducer';

const SORT_BY = [
  {
    id: 0,
    text: 'result.sortBy.default',
    value: TOUR_SORT_BY.default,
  },
  {
    id: 1,
    text: 'result.sortBy.priceDesc',
    value: TOUR_SORT_BY.expensive,
  },
  {
    id: 2,
    text: 'result.sortBy.priceAsc',
    value: TOUR_SORT_BY.cheap,
  },
];

function mapStateToProps(state: AppState) {
  return {
    sortBy: state.result.tour.sortBy,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  open: boolean;
  onClose(): void;
}

const TourResultSortPopup: React.FunctionComponent<Props> = props => {
  const { sortBy, dispatch, open, onClose } = props;

  return (
    <>
      <Dialog
        open={open}
        onClose={onClose}
        TransitionComponent={SlideUp}
        PaperProps={{
          style: {
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
            position: 'fixed',
            bottom: 0,
            left: 0,
            right: 0,
            borderRadius: '0px',
            minWidth: '100%',
            margin: 0,
            padding: '16px',
          },
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ textAlign: 'center' }}>
            <Typography variant="subtitle2" color="primary">
              <FormattedMessage id="result.sortBy.sortBy" />
            </Typography>
          </div>
          <div
            style={{
              backgroundColor: '#fff',
              marginTop: '8px',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {SORT_BY.map(item => (
              <ButtonBase
                key={item.id}
                style={{ minHeight: '40px', justifyContent: 'flex-start' }}
                onClick={() => {
                  dispatch(setSortBy(item.value));
                  onClose();
                }}
              >
                <Radio checked={sortBy === item.value} style={{ padding: 0, margin: '0px 8px' }} />
                <Typography
                  variant="body2"
                  color={sortBy === item.value ? 'primary' : 'textPrimary'}
                >
                  <FormattedMessage id={item.text} />
                </Typography>
              </ButtonBase>
            ))}
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default connect(mapStateToProps)(TourResultSortPopup);
