import { Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import ReactImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import { useIntl } from 'react-intl';
import Slider from 'react-slick';
import styled from 'styled-components';
import { RED } from '../../../../../colors';
import { some } from '../../../../../constants';
import ProgressiveImage from '../../../../common/components/ProgressiveImage';
import PickTourBox from '../PickTourBox';
import './override.css';

export const Image = styled.img`
  max-height: 375px;
  width: 100%;
  object-fit: contain;
  margin: auto;
  display: block;
  @media (min-height: 875px) {
    max-height: 720px;
  }
  @media (min-width: 1000px) {
    max-width: 650px;
  }
  @media (min-width: 1400px) {
    max-width: 900px;
  }
`;

interface Props {
  tourData: some;
}

const TourDetailImageBox: React.FunctionComponent<Props> = props => {
  const [showImageViewer, setShowImageViewer] = React.useState(false);
  const [startIndex, setIndex] = React.useState(0);
  const intl = useIntl();
  const { tourData } = props;
  const discountPrice =
    tourData.originPrice && tourData.originPrice > tourData.price
      ? (tourData.originPrice - tourData.price) / tourData.originPrice
      : 0;
  const images: some[] = tourData.images.map((v: some) => v);
  return (
    <div>
      <div style={{ overflow: 'hidden', width: '100%', position: 'relative' }}>
        <Slider
          key={`${showImageViewer}`}
          autoplay={!showImageViewer}
          initialSlide={startIndex}
          afterChange={(index: number) => setIndex(index)}
        >
          {images.map((v: some, index: number) => (
            <div
              key={index}
              onClick={() => {
                setShowImageViewer(true);
              }}
            >
              <ProgressiveImage
                src={v.imageURL}
                alt=""
                style={{
                  width: '100vw',
                  height: '225px',
                  objectFit: 'cover',
                }}
              />
            </div>
          ))}
        </Slider>
        {discountPrice >= 0.05 && (
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              bottom: '24px ',
              left: '-4px',
              borderRadius: '4px',
              minWidth: '60px',
              height: '32px',
              color: '#fff',
              background: RED,
            }}
          >
            <Typography variant="subtitle2">
              -
              {intl.formatNumber(discountPrice, {
                style: 'percent',
              })}
            </Typography>
          </div>
        )}
        <div style={{ position: 'absolute', bottom: 12, right: 16 }}>
          <Typography variant="caption" style={{ color: 'white' }}>
            [{startIndex + 1}/{images.length}]
          </Typography>
        </div>
      </div>
      <Dialog
        open={showImageViewer}
        BackdropProps={{ style: { background: 'black' } }}
        PaperProps={{
          style: {
            margin: 0,
            background: 'transparent',
            maxHeight: '100%',
            height: '100%',
            maxWidth: '100%',
            borderRadius: 0,
          },
        }}
        onClose={() => setShowImageViewer(false)}
      >
        <IconButton
          style={{ alignSelf: 'flex-end' }}
          color="default"
          size="medium"
          onClick={() => setShowImageViewer(!showImageViewer)}
        >
          <CloseIcon style={{ color: 'white' }} />
        </IconButton>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            justifyContent: 'center',
            padding: '0px 8px',
          }}
        >
          <ReactImageGallery
            items={tourData.images.map((v: some) => {
              return {
                original: v.imageURL as string,
                thumbnail: v.imageURL as string,
                originalTitle: v.desc as string,
                thumbnailTitle: v.desc as string,
              };
            })}
            renderItem={item => <ProgressiveImage src={item.original} alt={item.originalTitle} />}
            showFullscreenButton={false}
            infinite={true}
            showThumbnails={true}
            thumbnailPosition="bottom"
            showIndex={true}
            startIndex={startIndex}
          />
        </div>
        <PickTourBox tourData={tourData} />
      </Dialog>
    </div>
  );
};

export default TourDetailImageBox;
