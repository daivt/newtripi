import { Typography, Grid } from '@material-ui/core';
import IconStart from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import styled from 'styled-components';
import { GREY, PRIMARY } from '../../../../../colors';
import { some } from '../../../../../constants';
import default_image from '../../../../../svg/default_image.svg';
import { FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { setReviewImagesToShow } from '../../../redux/tourResultReducer';

const Line = styled.div`
  display: flex;
  align-items: center;
`;
const LIMIT_IMG = 2;
interface Props {
  data: some;
  style?: React.CSSProperties;
  dispatch: Dispatch;
}
const CommentCard: React.FunctionComponent<Props> = props => {
  const { data, style, dispatch } = props;
  const onClick = React.useCallback(
    (data: some, index: number) => {
      dispatch(setReviewImagesToShow({ data, index }));
    },
    [dispatch],
  );
  return (
    <div style={{ padding: '8px 0px', flexDirection: 'column' }}>
      <div>
        <Line>
          <Line
            style={{
              marginRight: '8px',
            }}
          >
            <img
              style={{
                width: '40px',
                height: '40px',
                objectFit: 'cover',
                borderRadius: '50%',
                border: `1px solid ${GREY}`,
              }}
              src={data.userAvatar ? data.userAvatar : default_image}
              alt=""
            />
          </Line>
          <div style={{ flex: 1, padding: '0px 16px 0px 4px' }}>
            {data.userName && (
              <Typography style={{ marginLeft: '8px' }} variant="body2">
                {data.userName}
              </Typography>
            )}
            <div style={{ display: 'flex', justifyContent: 'space-between', ...style }}>
              <Rating
                style={{ marginRight: '32px' }}
                size="small"
                icon={
                  <IconStart
                    style={{
                      color: PRIMARY,
                      height: '16px',
                      marginLeft: '-8px',
                    }}
                  />
                }
                value={data.score}
                max={Math.round(data.score)}
                readOnly={true}
              />
              <Typography variant="caption" color="textSecondary">
                {data.date}
              </Typography>
            </div>
          </div>
        </Line>
      </div>
      <div style={{ paddingTop: '8px' }}>
        <Typography color="textSecondary" variant="body2">
          {data.comment}
        </Typography>
        <Grid container spacing={2} style={{ maxWidth: '320px', width: '100%' }}>
          {data.photos.map((v: some, index: number) => (
            <Grid key={v.id} item xs={4}>
              {index < LIMIT_IMG ? (
                <img
                  style={{ width: '100%', height: '100%', cursor: 'pointer' }}
                  src={v.thumbLink}
                  alt=""
                  onClick={() => onClick && onClick(data, index)}
                />
              ) : index === LIMIT_IMG ? (
                <>
                  {data.photos.length - index - 1 > 0 ? (
                    <div
                      style={{
                        width: '100%',
                        height: '100%',
                        cursor: 'pointer',
                        position: 'relative',
                      }}
                      onClick={() => onClick && onClick(data, index)}
                    >
                      <div
                        style={{
                          position: 'absolute',
                          top: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          background:
                            'linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(image.png)',
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Typography style={{ color: 'white' }}>
                          [<FormattedNumber value={data.photos.length - index - 1} />]
                        </Typography>
                      </div>
                      <img style={{ width: '100%', height: '100%' }} src={v.thumbLink} alt="" />
                    </div>
                  ) : (
                    <img
                      onClick={() => onClick && onClick(data, index)}
                      style={{ width: '100%', height: '100%', cursor: 'pointer' }}
                      src={v.thumbLink}
                      alt=""
                    />
                  )}
                </>
              ) : (
                undefined
              )}
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default connect()(CommentCard);
