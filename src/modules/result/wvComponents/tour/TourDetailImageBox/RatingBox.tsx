import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NoDataResult from '../../../../common/components/NoDataResult';
import { fetchTourReviews } from '../../../redux/tourResultReducer';
import CommentCard from './CommentCard';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  tour: some;
}

interface State {
  reviews?: some[];
  totalReviews: number;
}

class RatingBox extends React.PureComponent<Props, State> {
  state: State = {
    totalReviews: 0,
  };
  async componentDidMount() {
    const { dispatch, tour } = this.props;
    const json = await dispatch(fetchTourReviews(tour.id, 1));
    if (json.code === 200) {
      this.setState({
        reviews: this.state.reviews
          ? this.state.reviews.concat(json.data.comments)
          : json.data.comments,
        totalReviews: json.data.total,
      });
    }
  }
  loadMore = async (page: number) => {
    const { dispatch, tour } = this.props;
    if (page > 25) {
      // safety valve
      return;
    }
    const json = await dispatch(fetchTourReviews(tour.id, page));
    if (json.code === 200) {
      this.setState({
        reviews: this.state.reviews
          ? this.state.reviews.concat(json.data.comments)
          : json.data.comments,
      });
    }
  };

  public render() {
    const { reviews, totalReviews } = this.state;
    return (
      <>
        <div style={{ flex: 1, overflow: 'auto' }}>
          {reviews ? (
            <InfiniteScroll
              pageStart={1}
              initialLoad={false}
              loadMore={this.loadMore}
              hasMore={reviews.length < totalReviews}
              loader={
                <LoadingIcon
                  key="loader"
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              }
              useWindow={false}
            >
              <div style={{ marginTop: '16px', marginLeft: '16px' }}>
                {reviews.map((v: some, index: number) => (
                  <CommentCard key={index} data={v} />
                ))}
              </div>
              {!reviews.length && (
                <NoDataResult id="tour.noComment" style={{ marginTop: '48px' }} />
              )}
            </InfiniteScroll>
          ) : (
            <LoadingIcon
              key="loader"
              style={{
                display: 'flex',
                height: '240px',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            />
          )}
        </div>
      </>
    );
  }
}

export default connect()(RatingBox);
