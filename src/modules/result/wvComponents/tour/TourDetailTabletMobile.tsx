import { Typography } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../constants';
import { PageWrapper } from '../../../common/wvComponents/elements';
import HeaderGoBack from '../../../common/wvComponents/HeaderGoBack';
import PickTourBox from './PickTourBox';
import TourDetailImageBox from './TourDetailImageBox';
import TourDetailSkeloton from './TourDetailSkeloton';
import TourDetailTabs from './TourDetailTabs';
import { StickyDiv } from '../../../common/components/elements';

export interface ITourDetailTabletMobileProps {
  tourData?: some;
}

const TourDetailTabletMobile: React.FC<ITourDetailTabletMobileProps> = props => {
  const { tourData } = props;
  const imageDivRef = React.useRef<HTMLDivElement>(null);
  return (
    <PageWrapper>
      {!tourData ? (
        <TourDetailSkeloton />
      ) : (
        <>
          <HeaderGoBack
            boundaryElementRef={imageDivRef}
            content={
              <Typography variant="subtitle2" noWrap color="inherit">
                {tourData.name}
              </Typography>
            }
          />
          <div ref={imageDivRef}>
            <TourDetailImageBox tourData={tourData} />
          </div>
          <div style={{ padding: '8px 16px', flex: 1 }}>
            <Typography variant="h6">{tourData.name}</Typography>
          </div>
          <TourDetailTabs tour={tourData} />
          <StickyDiv style={{ bottom: 0 }}>
            <PickTourBox tourData={tourData} />
          </StickyDiv>
        </>
      )}
    </PageWrapper>
  );
};

export default TourDetailTabletMobile;
