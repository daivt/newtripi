import styled from 'styled-components';

export const BoxColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

export const DropDownHeader = styled.div`
  cursor: pointer;
`;

export const ArrowStyle = styled.span<{ active: boolean }>`
  display: inline-block;
  margin-right: 6px;
  font-size: 10px;
  transition: all 300ms;
  transform: ${props => (!props.active ? 'rotate(0deg)' : 'rotate(180deg)')};
`;

export const DropDownStyle = {
  padding: '10px 0',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingLeft: '14px',
};

export const BoxContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-left: 8px;
`;

export const TourSubjectItem = styled.div`
  display: flex;
  flex-direction: column;
  :hover {
    cursor: pointer;
  }
`;
