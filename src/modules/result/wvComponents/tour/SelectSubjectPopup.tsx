/* eslint-disable @typescript-eslint/no-unused-expressions */
import { Button, ButtonBase, Checkbox, Collapse, Dialog, FormControlLabel, Radio, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { remove } from 'lodash';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DARK_GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import { SlideUp } from '../../../common/wvComponents/elements';
import PopupHeader from '../../../common/wvComponents/PopupHeader';
import { setFilter } from '../../redux/tourResultReducer';
import { StickyDiv } from '../../../common/components/elements';

const StyledFormControlLabel = withStyles({
  root: {
    width: '100%',
  },
})(FormControlLabel);
export interface TourSubjects {
  groupName: string;
  groupIcon: string;
  subjectList: some[];
}
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  open: boolean;
  onClose(): void;
  group: string;
  subjects: TourSubjects[];
}

const SelectSubjectPopup: React.FC<Props> = props => {
  const { subjects, dispatch, group, open, onClose, filter } = props;
  const [groupName, setGroup] = React.useState<string>(group);
  const [tourSubjects, setSubject] = React.useState<ReadonlyArray<number>>([]);
  React.useEffect(() => {
    setGroup(group);
  }, [group]);
  const tourSubjectSelected = React.useMemo(() => {
    return subjects.find((obj: some) => groupName && obj.groupName === groupName);
  }, [subjects, groupName]);
  React.useEffect(() => {
    if (tourSubjectSelected) {
      if (filter.groupName === tourSubjectSelected.groupName) {
        setSubject(filter.tourSubjects);
      } else {
        tourSubjectSelected && setSubject(tourSubjectSelected.subjectList.map((v: some) => v.id));
      }
    }
  }, [filter.groupName, filter.tourSubjects, tourSubjectSelected]);

  const setTourSubject = React.useCallback(
    (isChecked: boolean, subjectList: ReadonlyArray<number>, id: number) => {
      let tourSubjectsTemp = [...tourSubjects];
      if (tourSubjectsTemp.length === subjectList.length) {
        tourSubjectsTemp = [id];
      } else if (isChecked) {
        tourSubjectsTemp.push(id);
      } else if (tourSubjectsTemp.length > 1) {
        remove(tourSubjectsTemp, val => val === id);
      }

      setSubject(tourSubjectsTemp);
    },
    [tourSubjects],
  );
  const onSelectAllTourSubject = React.useCallback((isChecked: boolean, subjectList: ReadonlyArray<number>) => {
    setSubject(subjectList);
  }, []);
  return (
    <>
      <Dialog open={open} onClose={onClose} TransitionComponent={SlideUp} fullScreen>
        <div
          style={{
            background: '#fff',
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
          }}
        >
          <PopupHeader
            onClose={onClose}
            title={
              <Typography variant="subtitle1" style={{ color: 'white', textAlign: 'center', flex: 1 }}>
                <FormattedMessage id="tour.filter.tourSubject" />
              </Typography>
            }
          />
          <div style={{ flex: 1 }}>
            <div style={{ display: 'flex', flexDirection: 'column', marginTop: '8px' }}>
              <ButtonBase
                style={{
                  height: 48,
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  padding: '0px 16px',
                }}
              >
                <StyledFormControlLabel
                  label={
                    <Typography
                      variant="caption"
                      style={{
                        color: tourSubjects.length === 0 ? PRIMARY : DARK_GREY,
                        paddingLeft: '3px',
                      }}
                    >
                      <FormattedMessage id="all" />
                    </Typography>
                  }
                  labelPlacement="end"
                  onChange={(e, checked) => {
                    setGroup('');
                    setSubject([]);
                  }}
                  style={{ margin: 0 }}
                  checked={tourSubjects.length === 0}
                  control={<Radio color="secondary" style={{ padding: 0 }} />}
                />
              </ButtonBase>
            </div>
            {subjects.map((data: some, index: number) => {
              return (
                <div key={index} style={{ display: 'flex', flexDirection: 'column' }}>
                  <ButtonBase
                    key={index}
                    style={{
                      height: 48,
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      padding: '0px 16px',
                    }}
                    onClick={() => {
                      if (filter.groupName === data.groupName) {
                        setGroup(filter.groupName);
                        setSubject(filter.tourSubjects);
                      } else {
                        setGroup(data.groupName);
                        setSubject(data.subjectList.map((v: some) => v.id));
                      }
                    }}
                  >
                    <ProgressiveImage
                      src={data.groupIcon}
                      alt=""
                      style={{
                        width: '26px',
                      }}
                    />
                    <Typography variant="caption" style={{ textAlign: 'center', marginLeft: '8px' }}>
                      {data.groupName}
                    </Typography>
                  </ButtonBase>
                  <Collapse in={groupName === data.groupName}>
                    <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '30px' }}>
                      <ButtonBase style={{ justifyContent: 'flex-start', padding: '0px 8px' }}>
                        <StyledFormControlLabel
                          label={
                            <Typography
                              variant="caption"
                              style={{
                                color: tourSubjects.length === data.subjectList.length ? PRIMARY : DARK_GREY,
                                paddingLeft: '3px',
                              }}
                            >
                              <FormattedMessage id="all" />
                            </Typography>
                          }
                          labelPlacement="end"
                          onChange={(e, checked) =>
                            onSelectAllTourSubject(
                              checked,
                              data.subjectList.map((v: some) => v.id),
                            )
                          }
                          control={
                            <Checkbox
                              color="primary"
                              checked={tourSubjects.length === data.subjectList.length}
                              style={{ padding: '4px' }}
                            />
                          }
                        />
                      </ButtonBase>
                      {data.subjectList.map((item: some) => (
                        <ButtonBase key={item.id} style={{ justifyContent: 'flex-start', padding: '0px 8px' }}>
                          <StyledFormControlLabel
                            label={
                              <Typography
                                variant="caption"
                                style={{
                                  color:
                                    tourSubjects.indexOf(item.id) !== -1 &&
                                    tourSubjects.length !== data.subjectList.length
                                      ? PRIMARY
                                      : DARK_GREY,
                                  paddingLeft: '3px',
                                }}
                              >
                                {item.name}
                              </Typography>
                            }
                            labelPlacement="end"
                            value={item.id}
                            style={{ marginRight: 0 }}
                            onChange={(e, value) => setTourSubject(value, data.subjectList, item.id)}
                            control={
                              <Checkbox
                                color="primary"
                                checked={
                                  tourSubjects.indexOf(item.id) !== -1 &&
                                  tourSubjects.length !== data.subjectList.length
                                }
                                style={{ padding: '4px' }}
                              />
                            }
                          />
                        </ButtonBase>
                      ))}
                    </div>
                  </Collapse>
                </div>
              );
            })}
          </div>
          <StickyDiv
            style={{
              bottom: 0,
              minHeight: '64px',
              background: 'white',
              padding: '12px 24px',
              boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
            }}
          >
            <Button
              variant="contained"
              color="secondary"
              style={{ width: '100%' }}
              onClick={() => {
                onClose();
                dispatch(
                  setFilter({
                    ...filter,
                    tourSubjects,
                    groupName,
                  }),
                );
              }}
            >
              <FormattedMessage id="apply" />
            </Button>
          </StickyDiv>
        </div>
      </Dialog>
    </>
  );
};
function mapStateToProps(state: AppState) {
  return {
    filter: state.result.tour.filterParams,
  };
}
export default connect(mapStateToProps)(SelectSubjectPopup);
