import { Button, Divider } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Line, StickyDiv } from '../../../common/components/elements';

interface Props {}

const TourDetailSkeloton: React.FC<Props> = () => {
  return (
    <div style={{ flexDirection: 'column', display: 'flex', flex: 1 }}>
      <Skeleton variant="rect" height="225px" />
      <div style={{ padding: '8px 16px', flex: 1 }}>
        <Skeleton variant="text" width="100%" height="24px" />
        <Skeleton variant="text" width="60%" style={{ marginTop: '16px' }} />
        <Skeleton variant="text" width="50%" />
        <Skeleton variant="text" width="80%" />
        <Skeleton variant="text" width="70%" />
        <Divider style={{ margin: '16px 0px 8px 0px', color: '#f4f4f4' }} />
        <Skeleton variant="text" width="50%" />
        <Skeleton variant="text" width="30%" />
        <Divider style={{ margin: '8px 0px', color: '#f4f4f4' }} />
        <Line>
          <Skeleton variant="rect" width="32px" height="32px" />
          <div style={{ marginLeft: '8px' }}>
            <Skeleton variant="text" width="80px" height="8px" />
            <Skeleton variant="text" width="120px" height="8px" />
          </div>
        </Line>
        <Divider style={{ margin: '8px 0px 8px 0px', color: '#f4f4f4' }} />
        <Skeleton variant="text" width="70%" />
        <Skeleton variant="text" width="80%" />
        <Skeleton variant="text" width="60%" />
        <Skeleton variant="text" width="50%" />
        <Skeleton variant="text" width="45%" />
      </div>
      <StickyDiv
        style={{
          display: 'flex',
          width: '100%',
          minHeight: '64px',
          padding: '8px 16px',
          boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
          background: 'white',
          bottom: 0,
          alignItems: 'center',
        }}
      >
        <Line
          style={{
            flex: 1,
          }}
        >
          <Skeleton variant="text" width="80px" />
        </Line>
        <Line style={{ justifyContent: 'flex-end' }}>
          <Button
            size="small"
            variant="contained"
            color="secondary"
            style={{ minWidth: '130px', minHeight: '36px' }}
            disabled
          >
            <FormattedMessage id="tour.continue" />
          </Button>
        </Line>
      </StickyDiv>
    </div>
  );
};

export default TourDetailSkeloton;
