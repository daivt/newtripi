import { IconButton } from '@material-ui/core';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { TOUR_PAGE_SIZE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as ChangeValue } from '../../../../svg/change_value.svg';
import { Line, StickyDiv } from '../../../common/components/elements';
import { PageWrapper } from '../../../common/wvComponents/elements';
import HeaderGoBack from '../../../common/wvComponents/HeaderGoBack';
import TourSearch from '../../../search/wvComponents/TourSearch';
import { searchMore } from '../../redux/tourResultReducer';
import TourOptionBox from './TourOptionBox';
import TourResultBox from './TourResultBox';
import TourResultItem from './TourResultItem';
import TourResultTermFilterHeaderBox from './TourResultTermFilterHeaderBox';

interface ITourResultTermMobileProps extends ReturnType<typeof mapStateToProps> {
  input?: string;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TourResultTermMobile: React.FunctionComponent<ITourResultTermMobileProps> = props => {
  const { result, dispatch } = props;
  const { termResultData } = result;
  const { loading } = result;
  let totalLength = 0;
  if (termResultData) {
    totalLength = termResultData.data ? termResultData.data.total : 0;
  }
  const { pageOffset } = result;
  return (
    <PageWrapper>
      <HeaderGoBack
        content={
          <Line style={{ flex: 1 }}>
            <TourSearch type="term" />
            <IconButton>
              <ChangeValue />
            </IconButton>
          </Line>
        }
      />
      <StickyDiv
        style={{
          top: 51,
          zIndex: 100,
          background: 'white',
          boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.3), 0px 1px 1px rgba(0, 0, 0, 0.1)',
        }}
      >
        <TourResultTermFilterHeaderBox />
      </StickyDiv>
      <div
        style={{
          flex: 1,
        }}
      >
        <div style={{ padding: '0px 16px 8px 16px' }}>
          {loading && !termResultData?.data?.tours ? (
            <>
              <TourResultItem data={{ skeleton: true }} />
              <TourResultItem data={{ skeleton: true }} />
              <TourResultItem data={{ skeleton: true }} />
            </>
          ) : (
            <InfiniteScroll
              initialLoad={false}
              loadMore={() => {
                dispatch(searchMore());
              }}
              hasMore={totalLength - TOUR_PAGE_SIZE * pageOffset > 0}
              loader={<TourResultItem data={{ skeleton: true }} />}
              useWindow
              style={{ flex: 1 }}
            >
              <TourResultBox tours={termResultData?.data?.tours || []} />
            </InfiniteScroll>
          )}
        </div>
      </div>
      <StickyDiv
        style={{
          zIndex: 100,
          bottom: 0,
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <TourOptionBox />
      </StickyDiv>
    </PageWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
  };
}

export default connect(mapStateToProps)(TourResultTermMobile);
