import { Button, ButtonBase, Checkbox, Collapse, Dialog, FormControlLabel, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { remove } from 'lodash';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DARK_GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { SlideUp } from '../../../common/wvComponents/elements';
import PopupHeader from '../../../common/wvComponents/PopupHeader';
import { setFilter } from '../../redux/tourResultReducer';
import { StickyDiv } from '../../../common/components/elements';

const StyledFormControlLabel = withStyles({
  root: {
    width: '100%',
  },
  label: { flex: 1 },
})(FormControlLabel);

function getAllId(val: some) {
  let listIds: number[] = [val.id];
  if (val.subFilterPopularDestinations) {
    val.subFilterPopularDestinations.forEach((v: some) => {
      listIds = listIds.concat(getAllId(v));
    });
  }
  return listIds;
}
const OptionTree: React.FC<{
  data: some;
  destinations: ReadonlyArray<number>;
  onChange(isChecked: boolean, value: ReadonlyArray<number>): void;
}> = props => {
  const { onChange, data, destinations } = props;
  const [open, setOpen] = React.useState(destinations.indexOf(data.id) !== -1);
  React.useEffect(() => {
    setOpen(destinations.indexOf(data.id) !== -1);
  }, [data.id, destinations]);
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <ButtonBase
        style={{
          height: 48,
          justifyContent: 'flex-start',
          alignItems: 'center',
          padding: '0px 16px',
        }}
      >
        <StyledFormControlLabel
          label={
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography
                variant="caption"
                style={{
                  color: open ? PRIMARY : DARK_GREY,
                  paddingLeft: '3px',
                  flex: 1,
                  textAlign: 'start',
                }}
              >
                {data.name}
              </Typography>
              {data.subFilterPopularDestinations && data.subFilterPopularDestinations.length > 0 && (
                <Typography style={{ marginLeft: '20px' }}>
                  <span
                    style={{
                      display: 'inline-block',
                      marginLeft: '6px',
                      fontSize: '10px',
                      transition: 'all 300ms',
                      transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
                      cursor: 'pointer',
                    }}
                  >
                    &#9650;
                  </span>
                </Typography>
              )}
            </div>
          }
          labelPlacement="end"
          onChange={(e, checked) => {
            if (checked) {
              onChange(checked, [data.id]);
            } else {
              onChange(checked, getAllId(data));
            }
          }}
          style={{ margin: 0 }}
          control={<Checkbox color="secondary" checked={open} style={{ padding: '4px' }} />}
        />
      </ButtonBase>
      <Collapse in={open}>
        <div style={{ marginLeft: '30px' }}>
          {data.subFilterPopularDestinations &&
            data.subFilterPopularDestinations.map((v: some) => (
              <OptionTree key={v.id} data={v} onChange={onChange} destinations={destinations} />
            ))}
        </div>
      </Collapse>
    </div>
  );
};
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  open: boolean;
  onClose(): void;
}
const TourFilterDestinationPopup: React.FunctionComponent<Props> = props => {
  const { open, onClose, termResultData, dispatch, filter } = props;
  const [destinations, setDestination] = React.useState<ReadonlyArray<number>>(filter.destinations);
  React.useEffect(() => {
    setDestination(filter.destinations);
  }, [filter.destinations]);
  const selectLocation = React.useCallback(
    (isChecked: boolean, listId: ReadonlyArray<number>) => {
      let destinationsTemp = [...destinations];
      if (isChecked) {
        destinationsTemp = destinationsTemp.concat(listId);
      } else {
        remove(destinationsTemp, val => listId.indexOf(val) !== -1);
      }
      setDestination(destinationsTemp);
    },
    [destinations],
  );
  return (
    <Dialog open={open} onClose={onClose} TransitionComponent={SlideUp} fullScreen>
      <div
        style={{
          overflow: 'auto',
          display: 'flex',
          flexDirection: 'column',
          flex: 1,
        }}
      >
        <PopupHeader
          onClose={onClose}
          title={
            <Typography variant="subtitle1" style={{ color: 'white', textAlign: 'center', flex: 1 }}>
              <FormattedMessage id="tour.filter.chooseLocation" />
            </Typography>
          }
        />
        <div style={{ flex: 1 }}>
          {termResultData &&
            termResultData.data.filters.destinations.map((v: some) => (
              <OptionTree key={v.id} data={v} destinations={destinations} onChange={selectLocation} />
            ))}
        </div>
        <StickyDiv
          style={{
            bottom: 0,
            minHeight: '64px',
            background: 'white',
            padding: '12px 24px',
            boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
          }}
        >
          <Button
            variant="contained"
            color="secondary"
            style={{ width: '100%' }}
            onClick={() => {
              onClose();
              dispatch(setFilter({ ...filter, destinations }));
            }}
          >
            <FormattedMessage id="apply" />
          </Button>
        </StickyDiv>
      </div>
    </Dialog>
  );
};
function mapStateToProps(state: AppState) {
  return {
    termResultData: state.result.tour.termResultData,
    filter: state.result.tour.filterParams,
  };
}
export default connect(mapStateToProps)(TourFilterDestinationPopup);
