import { IconButton, Typography } from '@material-ui/core';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { TOUR_PAGE_SIZE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as ChangeValue } from '../../../../svg/change_value.svg';
import { Line } from '../../../common/components/elements';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import HeaderGoBack from '../../../common/wvComponents/HeaderGoBack';
import { searchMore } from '../../redux/tourResultReducer';
import TourOptionBox from './TourOptionBox';
import TourResultBox from './TourResultBox';
import TourResultFilterSubjectBox from './TourResultFilterSubjectBox';
import TourResultItem from './TourResultItem';

interface ITourResultLocationMobileProps extends ReturnType<typeof mapStateToProps> {
  input?: string;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TourResultLocationMobile: React.FunctionComponent<ITourResultLocationMobileProps> = props => {
  const { result, dispatch, input } = props;
  const { locationResultData, termResultData } = result;
  const { loading } = result;
  let totalLength = 0;
  if (locationResultData) {
    totalLength = locationResultData.total;
  }
  if (termResultData) {
    totalLength = termResultData.data ? termResultData.data.total : 0;
  }
  const { pageOffset } = result;

  const headerBoundaryElemRef = React.useRef<HTMLDivElement>(null);

  return (
    <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
      <HeaderGoBack
        content={
          <Line style={{ flex: 1 }}>
            <Typography variant="subtitle1" style={{ flex: 1, textAlign: 'center', color: 'white' }}>
              {input}
            </Typography>
            <IconButton>
              <ChangeValue />
            </IconButton>
          </Line>
        }
        boundaryElementRef={headerBoundaryElemRef}
      />
      <div
        style={{
          flex: 1,
        }}
      >
        <div ref={headerBoundaryElemRef} style={{ width: '100vw', height: '75vw' }}>
          <ProgressiveImage
            src={locationResultData?.destination.imageUrl}
            alt=""
            style={{ objectFit: 'cover', width: '100%', height: '100%' }}
          />
        </div>
        <div
          style={{
            margin: '-28px 16px 0px 16px',
          }}
        >
          <TourResultFilterSubjectBox />
        </div>
        <div style={{ padding: '8px 16px 8px 16px' }}>
          {loading && !locationResultData?.tours ? (
            <>
              <TourResultItem data={{ skeleton: true }} />
              <TourResultItem data={{ skeleton: true }} />
              <TourResultItem data={{ skeleton: true }} />
            </>
          ) : (
            <InfiniteScroll
              initialLoad={false}
              loadMore={() => {
                dispatch(searchMore());
              }}
              hasMore={totalLength - TOUR_PAGE_SIZE * pageOffset > 0}
              loader={<TourResultItem data={{ skeleton: true }} />}
              useWindow
            >
              <TourResultBox tours={locationResultData?.tours || []} />
            </InfiniteScroll>
          )}
        </div>
      </div>
      <div
        style={{
          position: 'sticky',
          bottom: 0,
          zIndex: 100,
        }}
      >
        <TourOptionBox />
      </div>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
  };
}

export default connect(mapStateToProps)(TourResultLocationMobile);
