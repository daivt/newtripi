import { ButtonBase, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import TourResultFilterPopup from './TourResultFilterPopup';
import TourResultSortPopup from './TourResultSortPopup';

interface ITourOptionBoxProps extends ReturnType<typeof mapStateToProps> {}

const TourOptionBox: React.FunctionComponent<ITourOptionBoxProps> = props => {
  const [openFilter, setOpenFilter] = React.useState(false);
  const [openSort, setOpenSort] = React.useState(false);
  return (
    <>
      <div
        style={{
          display: 'flex',
          flex: 1,
          boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
          background: 'white',
          overflow: 'hidden',
          minHeight: '40px',
        }}
      >
        <ButtonBase style={{ flex: 1 }} onClick={() => setOpenSort(true)}>
          <Typography variant="body2" style={{ color: BLUE }}>
            <FormattedMessage id="result.sortBy" />
          </Typography>
        </ButtonBase>
        <div style={{ border: `1px solid ${BLUE}`, margin: '6px 0px' }} />
        <ButtonBase style={{ flex: 1 }} onClick={() => setOpenFilter(true)}>
          <Typography variant="body2" style={{ color: BLUE }}>
            <FormattedMessage id="result.filter" />
          </Typography>
        </ButtonBase>
        <TourResultFilterPopup open={openFilter} onClose={() => setOpenFilter(false)} />
        <TourResultSortPopup open={openSort} onClose={() => setOpenSort(false)} />
      </div>
    </>
  );
};
function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}

export default connect(mapStateToProps)(TourOptionBox);
