import { Button, Modal, Slide, Slider } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { remove } from 'lodash';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CheckButton, PriceLabelComponent, StickyDiv } from '../../../common/components/elements';
import PopupHeader from '../../../common/wvComponents/PopupHeader';
import { PRICE_STEP } from '../../constants';
import { setFilter, TourFilterState } from '../../redux/tourResultReducer';
import { BoxColumn, BoxContent } from './TourFilter.styles';

function mapState2Props(state: AppState) {
  return {
    filterMaxPrice: state.result.tour.filterMaxPrice,
    filter: state.result.tour.filterParams,
    generalInformation: state.common.generalInfo.tour,
  };
}

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  locationResultData?: some;
  open: boolean;
  onClose(): void;
}

interface State {
  filter: TourFilterState;
}

const TourResultFilterPopup: React.FC<Props> = props => {
  const { generalInformation, filterMaxPrice, open, onClose, dispatch } = props;
  const [filter, setFilters] = React.useState<TourFilterState>(props.filter);
  React.useEffect(() => {
    setFilters(props.filter);
  }, [props.filter]);
  const setPrice = React.useCallback(
    (value: number) => {
      setFilters({ ...filter, maxPrice: value });
    },
    [filter],
  );
  const setActivityTag = React.useCallback(
    (activityId: number) => {
      const activityTags = [...filter.activityTags];
      remove(activityTags, id => id === activityId);
      if (activityTags.length === filter.activityTags.length) {
        activityTags.push(activityId);
      }
      setFilters({ ...filter, activityTags });
    },
    [filter],
  );
  const setTourType = React.useCallback(
    (tourId: number) => {
      const tourTypes = [...filter.tourTypes];
      remove(tourTypes, id => id === tourId);
      if (tourTypes.length === filter.tourTypes.length) {
        tourTypes.push(tourId);
      }
      setFilters({ ...filter, tourTypes });
    },
    [filter],
  );
  const setTourService = React.useCallback(
    (tourId: number) => {
      const tourServices = [...filter.tourServices];
      remove(tourServices, id => id === tourId);
      if (tourServices.length === filter.tourServices.length) {
        tourServices.push(tourId);
      }
      setFilters({ ...filter, tourServices });
    },
    [filter],
  );
  const setTransport = React.useCallback(
    (index: number) => {
      const transportations = [...filter.transportations];
      remove(transportations, id => id === index);
      if (transportations.length === filter.transportations.length) {
        transportations.push(index);
      }
      setFilters({ ...filter, transportations });
    },
    [filter],
  );

  return (
    <Modal open={open} onClose={onClose}>
      <Slide in={open} direction="up">
        <div
          style={{
            position: 'fixed',
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            background: '#fff',
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <PopupHeader
            onClose={onClose}
            title={
              <Typography variant="subtitle1" style={{ color: 'white', textAlign: 'center', flex: 1 }}>
                <FormattedMessage id="tour.filter.tourSubject" />
              </Typography>
            }
          />
          <div style={{ display: 'flex', flexDirection: 'column', flex: 1, padding: '12px 16px' }}>
            <BoxColumn>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  paddingTop: '16px',
                }}
              >
                <Typography variant="body2">
                  <FormattedMessage id="filter.showResult.price" />
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  0&nbsp;
                  <FormattedMessage id="currency" />
                  &nbsp;-&nbsp;
                  <FormattedNumber value={filter.maxPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
              <div
                style={{
                  margin: '12px 12px 0 12px',
                  position: 'relative',
                  pointerEvents: 'visible',
                }}
              >
                <Slider
                  key={`filter${filterMaxPrice}`}
                  defaultValue={filter.maxPrice}
                  min={0}
                  max={filterMaxPrice < 0 ? 0 : filterMaxPrice}
                  step={PRICE_STEP}
                  onChangeCommitted={(e: any, value: any) => setPrice(value)}
                  ValueLabelComponent={PriceLabelComponent}
                />
              </div>
            </BoxColumn>
            {generalInformation && (
              <>
                <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
                  <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                    <FormattedMessage id="tour.filter.activity" />
                  </Typography>
                  <BoxContent>
                    {generalInformation.activityTags.map((item: some) => (
                      <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                        <CheckButton
                          active={filter.activityTags.indexOf(item.id) !== -1}
                          style={{ padding: '0px 10px' }}
                          onClick={() => setActivityTag(item.id)}
                        >
                          <Typography variant="body2">{item.name}</Typography>
                        </CheckButton>
                      </div>
                    ))}
                  </BoxContent>
                </BoxColumn>

                <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
                  <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                    <FormattedMessage id="tour.filter.tourTypes" />
                  </Typography>
                  <BoxContent>
                    {generalInformation.tourTypes.map((item: some) => (
                      <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                        <CheckButton
                          active={filter.tourTypes.indexOf(item.id) !== -1}
                          style={{ padding: '0px 10px' }}
                          onClick={() => setTourType(item.id)}
                        >
                          <Typography variant="body2">{item.name}</Typography>
                        </CheckButton>
                      </div>
                    ))}
                  </BoxContent>
                </BoxColumn>

                <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
                  <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                    <FormattedMessage id="tour.filter.tourService" />
                  </Typography>
                  <BoxContent>
                    {generalInformation.tourServices.map((item: some) => (
                      <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                        <CheckButton
                          onClick={() => setTourService(item.id)}
                          active={filter.tourServices.indexOf(item.id) !== -1}
                          style={{ padding: '0px 10px' }}
                        >
                          <Typography variant="body2">{item.name}</Typography>
                        </CheckButton>
                      </div>
                    ))}
                  </BoxContent>
                </BoxColumn>

                <BoxColumn style={{ paddingTop: '16px', paddingBottom: '16px' }}>
                  <Typography variant="subtitle2" style={{ paddingBottom: '6px' }}>
                    <FormattedMessage id="tour.filter.transport" />
                  </Typography>
                  <BoxContent>
                    {generalInformation.transportations.map((item: some) => (
                      <div style={{ marginTop: '8px', display: 'flex', alignItems: 'center' }} key={item.id}>
                        <CheckButton
                          active={filter.transportations.indexOf(item.id) !== -1}
                          style={{ padding: '0px 10px' }}
                          onClick={() => setTransport(item.id)}
                        >
                          <Typography variant="body2">{item.name}</Typography>
                        </CheckButton>
                      </div>
                    ))}
                  </BoxContent>
                </BoxColumn>
              </>
            )}
          </div>
          <StickyDiv
            style={{
              bottom: 0,
              minHeight: '64px',
              background: 'white',
              padding: '12px 24px',
              boxShadow: '0px -1px 4px rgba(0, 0, 0, 0.25)',
            }}
          >
            <Button
              variant="contained"
              color="secondary"
              style={{ width: '100%' }}
              onClick={() => {
                dispatch(setFilter(filter));
                onClose();
              }}
            >
              <FormattedMessage id="apply" />
            </Button>
          </StickyDiv>
        </div>
      </Slide>
    </Modal>
  );
};

export default connect(mapState2Props)(TourResultFilterPopup);
