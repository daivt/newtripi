import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { some, WV_ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { setBookingParams } from '../../../booking/redux/tourBookingReducer';
import Link from '../../../common/components/Link';
import NoDataResult from '../../../common/components/NoDataResult';
import { TOUR_BOOK_PARAMS_NAMES } from '../../../common/constants';
import SmartResultItem, { useSmartRenderResults } from '../../components/SmartResultItem';
import TourResultItem from './TourResultItem';

interface ITourResultBoxProps extends ReturnType<typeof mapStateToProps> {
  tours?: some[];
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TourResultBox: React.FunctionComponent<ITourResultBoxProps> = props => {
  const { tours, result, dispatch } = props;
  const { loading } = result;
  const notFound = result.locationResultData
    ? result.locationResultData.notFound
    : result.termResultData && result.termResultData.data
    ? !result.termResultData.data.tours
    : true;

  const { itemRefs, itemHeights, visibleBounds } = useSmartRenderResults(tours || [], 348);

  return (
    <>
      <div style={{ margin: '10px 0' }}>
        {!loading && notFound && (
          <NoDataResult
            content={
              <Typography
                variant="body1"
                style={{
                  marginTop: '16px',
                  textAlign: 'center',
                }}
              >
                {result.message || <FormattedMessage id="tour.noDataByTerm" />}
              </Typography>
            }
            style={{ marginTop: '48px', marginBottom: '32px' }}
          />
        )}
        {tours?.map((tour, i) => (
          <SmartResultItem
            key={tour.id}
            ref={itemRefs[i]}
            height={itemHeights[i]}
            visible={i >= visibleBounds.start && i < visibleBounds.end}
          >
            <Link
              style={{ position: 'relative', display: 'block' }}
              to={{
                pathname: `${WV_ROUTES.tour.tourDetail}`,
                search: `?${TOUR_BOOK_PARAMS_NAMES.tourId}=${tour.id}`,
              }}
              onClick={() => dispatch(setBookingParams({ tourId: tour.id }))}
            >
              <TourResultItem data={tour} />
            </Link>
          </SmartResultItem>
        ))}
      </div>
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
  };
}

export default connect(mapStateToProps)(TourResultBox);
