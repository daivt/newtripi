import { ButtonBase, Typography } from '@material-ui/core';
import { groupBy } from 'lodash';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { Line } from '../../../common/components/elements';
import SelectSubjectPopup, { TourSubjects } from './SelectSubjectPopup';
import TourFilterDestinationPopup from './TourFilterDestinationPopup';

interface Props extends ReturnType<typeof mapStateToProps> {}

const TourResultTermFilterHeaderBox: React.FunctionComponent<Props> = props => {
  const { generalInformation, filter, termResultData } = props;
  const [openDestinationFilter, setOpenDes] = React.useState(false);
  const [openTourSubject, setOpenTourSubject] = React.useState(false);
  const subjects = React.useMemo((): TourSubjects[] => {
    const data: TourSubjects[] = [];
    const groupByName =
      termResultData &&
      groupBy(termResultData.data.filters.tourSubjects, (v: some) => {
        return v.groupName;
      });
    for (const property in groupByName) {
      if (groupByName.hasOwnProperty(property)) {
        data.push({
          groupName: property,
          groupIcon: groupByName[property][0].icon,
          subjectList: groupByName[property],
        });
      }
    }
    return data;
  }, [termResultData]);
  return (
    <div>
      <Line>
        <ButtonBase onClick={() => setOpenDes(true)} style={{ flex: 1, minHeight: '40px' }}>
          <Typography style={{ color: BLUE }} variant="body2">
            <FormattedMessage id="tour.filter.chooseLocation" />
            <span
              style={{
                display: 'inline-block',
                marginLeft: '6px',
                fontSize: '10px',
                transition: 'all 300ms',
                transform: openDestinationFilter ? 'rotate(0deg)' : 'rotate(180deg)',
                cursor: 'pointer',
              }}
            >
              &#9650;
            </span>
          </Typography>
        </ButtonBase>
        <ButtonBase onClick={() => setOpenTourSubject(true)} style={{ flex: 1, minHeight: '40px' }}>
          <Typography style={{ color: BLUE }} variant="body2">
            <FormattedMessage id="tour.filter.tourSubject" />

            <span
              style={{
                display: 'inline-block',
                marginLeft: '6px',
                fontSize: '10px',
                transition: 'all 300ms',
                transform: openTourSubject ? 'rotate(0deg)' : 'rotate(180deg)',
                cursor: 'pointer',
              }}
            >
              &#9650;
            </span>
          </Typography>
        </ButtonBase>
      </Line>
      {generalInformation && termResultData && (
        <SelectSubjectPopup
          open={openTourSubject}
          onClose={() => setOpenTourSubject(false)}
          group={filter.groupName}
          subjects={subjects}
        />
      )}
      <TourFilterDestinationPopup open={openDestinationFilter} onClose={() => setOpenDes(false)} />
    </div>
  );
};
function mapStateToProps(state: AppState) {
  return {
    termResultData: state.result.tour.termResultData,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}

export default connect(mapStateToProps)(TourResultTermFilterHeaderBox);
