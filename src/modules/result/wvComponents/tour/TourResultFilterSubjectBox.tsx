import { ButtonBase, Typography } from '@material-ui/core';
import IconSearch from '@material-ui/icons/Search';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DARK_GREY, GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconCheckAll } from '../../../../svg/ic_check_all.svg';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import TourSearch from '../../../search/wvComponents/TourSearch';
import { setFilter } from '../../redux/tourResultReducer';
import SelectSubjectPopup from './SelectSubjectPopup';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const TourResultFilterSubjectBox: React.FC<Props> = props => {
  const { generalInformation, dispatch, filter } = props;
  const [open, setOpen] = React.useState(false);
  const [groupName, setGroup] = React.useState<string>('');
  React.useEffect(() => {
    setGroup(filter.groupName);
  }, [filter.groupName, filter.tourSubjects]);
  if (!generalInformation) {
    return <></>;
  }
  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          borderRadius: '4px',
          paddingTop: '4px',
          overflow: 'hidden',
          boxShadow: '0px 4px 5px rgba(0, 0, 0, 0.2), 0px 1px 2px rgba(0, 0, 0, 0.1)',
          paddingBottom: '8px',
          background: 'white',
        }}
      >
        <TourSearch
          inputStyle={{ minHeight: '48px', borderRadius: '4px' }}
          icon={<IconSearch style={{ color: GREY }} />}
          type="location"
        />
        <Typography
          variant="subtitle2"
          style={{
            height: '30px',
            justifyContent: 'flex-start',
            padding: '0px 16px',
          }}
        >
          <FormattedMessage id="tour.filter.tourSubject" />
        </Typography>
        <ButtonBase
          style={{
            height: '30px',
            justifyContent: 'flex-start',
            padding: '0px 16px',
          }}
          onClick={() => {
            dispatch(
              setFilter({
                ...filter,
                tourSubjects: [],
                groupName: '',
              }),
            );
          }}
        >
          <IconCheckAll
            style={{
              width: '26px',
            }}
          />
          <Typography
            variant="caption"
            style={{
              color: filter.groupName === '' ? PRIMARY : DARK_GREY,
              paddingLeft: '8px',
            }}
          >
            <FormattedMessage id="all" />
          </Typography>
        </ButtonBase>
        {generalInformation.tourSubjects.map((data: some, index: number) => {
          return (
            <ButtonBase
              key={data.groupName}
              style={{
                height: '30px',
                justifyContent: 'flex-start',
                alignItems: 'center',
                padding: '0px 16px',
              }}
              onClick={() => {
                setGroup(data.groupName);
                setOpen(true);
              }}
            >
              <ProgressiveImage
                src={data.groupIcon}
                alt=""
                style={{
                  width: '26px',
                }}
              />
              <Typography
                variant="caption"
                style={{
                  marginLeft: '8px',
                  color: filter.groupName === data.groupName ? PRIMARY : DARK_GREY,
                }}
              >
                {data.groupName}
              </Typography>
            </ButtonBase>
          );
        })}
      </div>
      {generalInformation && open && (
        <SelectSubjectPopup
          open={open}
          onClose={() => setOpen(false)}
          group={groupName}
          subjects={generalInformation.tourSubjects}
        />
      )}
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    result: state.result.tour,
    generalInformation: state.common.generalInfo.tour,
    filter: state.result.tour.filterParams,
  };
}
export default connect(mapStateToProps)(TourResultFilterSubjectBox);
