import { replace } from 'connected-react-router';
import { map } from 'lodash';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { MESSAGE, REASON } from '../../common/components/SendHomeReasonDialog';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, FLIGHT_RESULT_CACHE_TIME, some, MY_TOUR, ROUTES } from '../../../constants';
import { makeCancelOldThunk, ThunkController } from '../../../utils';
import { resetBooking } from '../../booking/redux/flightBookingReducer';

import { SEND_HOME_REASONS } from '../../common/constants';
import { Airport } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { FlightSearchParamsState } from '../../search/redux/flightSearchReducer';
import { DEFAULT_FLIGHT_STATE, DEFAULT_FILTER } from '../constants';
import {
  getMaxFlightDuration,
  getMaxTransitDuration,
  parseFlightSearchFilterParams,
  sameFlightSearchParams,
} from '../utils';

export interface FlightFilterState {
  readonly price: ReadonlyArray<number>;
  readonly ticketClass: ReadonlyArray<some>;
  readonly numStops: ReadonlyArray<number>;
  readonly timeTakeOff: ReadonlyArray<some>;
  readonly timeLand: ReadonlyArray<some>;
  readonly flightDuration: ReadonlyArray<number>;
  readonly transitDuration: ReadonlyArray<number>;
  readonly airline: ReadonlyArray<number>;
}

interface ValidFlightSearchParamsState extends FlightSearchParamsState {
  readonly origin: Airport;
  readonly destination: Airport;
}

interface PairNumber {
  readonly inbound: number;
  readonly outbound: number;
}

export interface FlightResultState {
  readonly data?: Readonly<some>;
  readonly searchCompleted: number;
  readonly searchParams?: ValidFlightSearchParamsState;
  readonly searchTime: number;
  readonly authSearch: boolean;
  readonly filterParams: FlightFilterState;
  readonly sortBy: string;
  readonly maxTransitDuration: PairNumber;
  readonly maxFlightDuration: PairNumber;
}

export const setData = createAction(
  'result/flight/setData',
  resolve => (data?: some, params?: ValidFlightSearchParamsState, time?: number, auth: boolean = true) =>
    resolve({ data, params, time, auth }),
);

export const setFilter = createAction('result/flight/setFilter', resolve => (data: FlightFilterState) =>
  resolve({ data }),
);

export const setAirline = createAction('result/flight/setAirline', resolve => (airline: number[]) =>
  resolve({ airline }),
);

export const setSortBy = createAction('result/flight/setSortBy', resolve => (val: string) => resolve({ val }));

const setFilterPrice = createAction('result/flight/setFilterPrice', resolve => (price: number[]) => resolve({ price }));

const setFilterFlightDuration = createAction('result/flight/setFilterFlightDuration', resolve => (vals: number[]) =>
  resolve({ vals }),
);

const setMaxTransitDuration = createAction('result/flight/setMaxTransitDuration', resolve => (vals: PairNumber) =>
  resolve({ vals }),
);

const setMaxFlightDuration = createAction('result/flight/setMaxFlightDuration', resolve => (vals: PairNumber) =>
  resolve({ vals }),
);

const setSearchCompleted = createAction('result/flight/setSearchCompleted', resolve => (val: number) =>
  resolve({ val }),
);

export const search = makeCancelOldThunk(
  (controller: ThunkController, airlineId?: number, isOutbound?: boolean, bookingId?: number) => {
    return async (dispatch, getState) => {
      const state = getState();
      const searchState = state.search.flight;
      if (!searchState.origin || !searchState.destination) {
        return;
      }

      dispatch(resetBooking());

      if (
        state.result.flight.searchParams &&
        sameFlightSearchParams(searchState, state.result.flight.searchParams) &&
        new Date().getTime() - state.result.flight.searchTime < FLIGHT_RESULT_CACHE_TIME &&
        state.auth.auth === state.result.flight.authSearch
      ) {
        return;
      }

      let waitFor = null;
      let polling = true;
      dispatch(setData());
      dispatch(setFilter(DEFAULT_FILTER));
      dispatch(setSearchCompleted(0));

      const filterParams = parseFlightSearchFilterParams(new URLSearchParams(state.router.location.search));
      const { airlineIds } = filterParams;

      let i = 1;
      let n = 1;
      while (polling) {
        const json: some = await dispatch(
          fetchThunk(
            searchState.returnDate ? API_PATHS.searchTwoWayTickets : API_PATHS.searchOneWayTickets,
            'post',
            state.auth.auth,
            JSON.stringify({
              waitFor,
              from_airport: searchState.origin && searchState.origin.code,
              to_airport: searchState.destination && searchState.destination.code,
              depart: `${searchState.departureDate.format(DATE_FORMAT)}`,
              return: searchState.returnDate ? `${searchState.returnDate.format(DATE_FORMAT)}` : undefined,
              num_adults: `${searchState.travellerCountInfo.adultCount}`,
              num_childs: `${searchState.travellerCountInfo.childCount}`,
              num_infants: `${searchState.travellerCountInfo.babyCount}`,
              one_way: !searchState.returnDate ? '1' : '0',
              currency: 'VND',
              lang: state.intl.locale.substring(0, 2),
              filters: {
                airlines: airlineId ? [airlineId] : airlineIds,
                paging: { itemsPerPage: 200, page: 1 },
                sort: { priceUp: true },
                ticketClassCodes: map(state.search.flight.seatClass, 'code'),
              },
              sort: 'tripi_recommended',
              isOutbound: isOutbound || null,
              bookingId: bookingId || null,
            }),
          ),
        );
        if (controller.cancelled) {
          return;
        }

        if (json.code === 400) {
          dispatch(
            replace({
              pathname: MY_TOUR ? ROUTES.flight.flight : '/',
              search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}&${MESSAGE}=${json.message}`,
            }),
          );
          break;
        } else if (json.code !== 200) {
          dispatch(replace(MY_TOUR ? ROUTES.flight.flight : '/'));
          break;
        }

        dispatch(
          setData(
            json.data,
            { ...searchState, origin: searchState.origin, destination: searchState.destination },
            new Date().getTime(),
            state.auth.auth,
          ),
        );
        dispatch(
          setFilterPrice([0, searchState.returnDate ? json.data.filters.maxGroupPrice : json.data.filters.maxPrice]),
        );
        dispatch(
          setMaxTransitDuration({
            outbound: json.data.tickets
              ? getMaxTransitDuration(json.data.tickets)
              : getMaxTransitDuration(json.data.outbound.tickets),
            inbound: json.data.inbound ? getMaxTransitDuration(json.data.inbound.tickets) : 0,
          }),
        );

        dispatch(
          setMaxFlightDuration({
            outbound: json.data.tickets
              ? json.data.filters.maxDuration
              : getMaxFlightDuration(json.data.outbound.tickets),
            inbound: json.data.inbound ? getMaxFlightDuration(json.data.inbound.tickets) : 0,
          }),
        );

        waitFor = json.data.waitFor as string;
        polling = json.data.polling as boolean;
        if (i === 1) {
          n = 1 + waitFor.split(',').length;
        }
        const searchPercent = (100 * i) / n;

        dispatch(setSearchCompleted(searchPercent < 100 ? searchPercent : 99));
        i += 1;
      }
      dispatch(setSearchCompleted(100));
    };
  },
);

const actions = {
  setData,
  setFilter,
  setSortBy,
  setFilterPrice,
  setFilterFlightDuration,
  setMaxTransitDuration,
  setMaxFlightDuration,
  setSearchCompleted,
  setAirline,
};

type ActionT = ActionType<typeof actions>;

export default function flightReducer(
  state: FlightResultState = DEFAULT_FLIGHT_STATE,
  action: ActionT,
): FlightResultState {
  switch (action.type) {
    case getType(setData):
      return {
        ...state,
        data: action.payload.data,
        searchParams: action.payload.params,
        searchTime: action.payload.time || 0,
        authSearch: action.payload.auth,
      };
    case getType(setFilter):
      return {
        ...state,
        filterParams: action.payload.data,
      };
    case getType(setSortBy):
      return { ...state, sortBy: action.payload.val };
    case getType(setFilterPrice):
      return {
        ...state,
        filterParams: { ...state.filterParams, price: action.payload.price },
      };
    case getType(setFilterFlightDuration):
      return {
        ...state,
        filterParams: { ...state.filterParams, flightDuration: action.payload.vals },
      };
    case getType(setMaxTransitDuration):
      return {
        ...state,
        maxTransitDuration: action.payload.vals,
      };
    case getType(setMaxFlightDuration):
      return {
        ...state,
        maxFlightDuration: action.payload.vals,
      };
    case getType(setSearchCompleted):
      return {
        ...state,
        searchCompleted: action.payload.val,
      };
    case getType(setAirline):
      return {
        ...state,
        filterParams: { ...state.filterParams, airline: action.payload.airline },
      };
    default:
      return state;
  }
}
