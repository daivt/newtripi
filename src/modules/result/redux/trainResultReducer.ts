import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { makeCancelOldThunk, ThunkController } from '../../../utils';
import { fetchThunk } from '../../common/redux/thunks';
import { TrainSearchParamsState } from '../../search/redux/trainSearchReducer';
import { parseTrainSearchState } from '../../search/utils';
import { DEFAULT_TRAIN_RESULT_STATE } from '../constants';

export interface TrainResultState {
  loading: boolean;
  trainData?: some;
  outboundSortBy: string;
  inboundSortBy: string;
  searchParams?: TrainSearchParamsState;
  searchTime: number;
}

export const setData = createAction(
  'result/train/setData',
  resolve => (searchParams: TrainSearchParamsState, searchTime: number) => resolve({ searchParams, searchTime }),
);

export const setLoading = createAction('result/train/setLoading', resolve => (val: boolean) => resolve({ val }));

export const setTrainData = createAction('result/train/setTrainData', resolve => (data: some) => resolve({ data }));

export const setOutboundSortBy = createAction('result/train/setOutboundSortBy', resolve => (val: string) =>
  resolve({ val }),
);

export const setInboundSortBy = createAction('result/train/setInboundSortBy', resolve => (val: string) =>
  resolve({ val }),
);

export const search = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const state = getState();

    let searchState;
    try {
      searchState = parseTrainSearchState(new URLSearchParams(state.router.location.search));
    } catch (err) {}
    searchState = searchState || state.search.train;
    dispatch(setLoading(true));

    const json = await dispatch(
      fetchThunk(
        API_PATHS.trainSearch,
        'post',
        true,
        JSON.stringify({
          adt: searchState.travellersInfo.adultCount,
          chd: searchState.travellersInfo.childCount,
          std: searchState.travellersInfo.studentCount,
          old: searchState.travellersInfo.seniorCount,
          departureDate: searchState.departureDate.format('YYYY-MM-DD'),
          returnDate: searchState.returnDate ? searchState.returnDate.format('YYYY-MM-DD') : undefined,
          fromPoint: searchState.origin ? searchState.origin.trainStationCode : '',
          toPoint: searchState.destination ? searchState.destination.trainStationCode : '',
          orders: [
            {
              order: 'DESC',
              orderBy: 'duration',
            },
          ],
          seatType: searchState.seatClass.map(obj => obj.code).join(','),
          tripType: searchState.returnDate ? 2 : 1,
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setTrainData(json.data));
    }
    dispatch(setData(searchState, new Date().getTime()));
    dispatch(setLoading(false));
  };
});

const actions = { setData, setLoading, setTrainData, setOutboundSortBy, setInboundSortBy };

type ActionT = ActionType<typeof actions>;

function reducer(state: TrainResultState = DEFAULT_TRAIN_RESULT_STATE, action: ActionT): TrainResultState {
  switch (action.type) {
    case getType(setData):
      return {
        ...state,
        searchTime: action.payload.searchTime,
        searchParams: action.payload.searchParams,
      };
    case getType(setLoading):
      return { ...state, loading: action.payload.val };
    case getType(setOutboundSortBy):
      return { ...state, outboundSortBy: action.payload.val };
    case getType(setInboundSortBy):
      return { ...state, inboundSortBy: action.payload.val };
    case getType(setTrainData):
      return { ...state, trainData: action.payload.data };
    default:
      return state;
  }
}

export default reducer;
