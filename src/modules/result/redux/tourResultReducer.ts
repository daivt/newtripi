import { replace } from 'connected-react-router';
import moment from 'moment';
import querystring from 'querystring';
import { ThunkAction } from 'redux-thunk';
import { Action, ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, some, TOUR_PAGE_SIZE } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { makeCancelOldThunk, ThunkController } from '../../../utils';
import { resetBooking } from '../../booking/redux/tourBookingReducer';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../common/constants';
import { fetchThunk } from '../../common/redux/thunks';
import { isTermInput, Term, TourSearchParamsState } from '../../search/redux/tourSearchReducer';
import { parseTourSearchParams } from '../../search/utils';
import { DEFAULT_TOUR_STATE, TOUR_SORT_BY } from '../constants';
import { roundTourFilterPrice, sameTourFilterSortByParams, sameTourSearchParams } from '../utils';

export interface TourFilterState {
  readonly maxPrice: number;
  readonly groupName: string;
  readonly destinations: ReadonlyArray<number>;
  readonly tourTypes: ReadonlyArray<number>;
  readonly activityTags: ReadonlyArray<number>;
  readonly tourSubjects: ReadonlyArray<number>;
  readonly tourServices: ReadonlyArray<number>;
  readonly transportations: ReadonlyArray<number>;
}
export interface TourLastParams {
  search: TourSearchParamsState;
  filterParams: TourFilterState;
  sortBy: string;
}
export interface TourResultState {
  loading: boolean;
  termResultData?: { data: some; input: Term };
  locationResultData?: some;
  sortBy: string;
  tourData?: some;
  pageOffset: number;
  filterParams: TourFilterState;
  lastParams?: TourLastParams;
  filterMaxPrice: number;
  reviewImagesToShow?: { data: some; index: number };
  message?: string;
}

export const setPage = createAction('/result/tour/setPage', resolve => (val: number) => resolve({ val }));
export const setReviewImagesToShow = createAction(
  '/result/tour/reviewImagesToShow',
  resolve => (val?: { data: some; index: number }) => resolve({ val }),
);
export const setLoading = createAction('/result/tour/setLoading', resolve => (val: boolean) => resolve({ val }));
export const setMessage = createAction('/result/tour/setMessage', resolve => (val?: string) => resolve({ val }));

export const setLastParams = createAction('/result/tour/setLastParams', resolve => (data: TourLastParams) =>
  resolve({ data }),
);

export const setTermResultData = createAction(
  'result/tour/setTermResultData',
  resolve => (obj?: { data: some; term: string; params: TourSearchParamsState }) => resolve({ obj }),
);

export const setLocationResultData = createAction(
  'result/tour/setLocationResultData',
  resolve => (obj?: { data: some; params: TourSearchParamsState }) => resolve({ obj }),
);

export const addTermResultData = createAction(
  'result/tour/addTermResultData',
  resolve => (obj?: { data: some; term: string; params: TourSearchParamsState }) => resolve({ obj }),
);

export const addLocationResultData = createAction(
  'result/tour/addLocationResultData',
  resolve => (obj?: { data: some; params: TourSearchParamsState }) => resolve({ obj }),
);

export const setSortBy = createAction('result/tour/setSortBy', resolve => (val: string, skipSaga: boolean = false) =>
  resolve({ val, skipSaga }),
);

export const setTourDetail = createAction('result/tour/setTourDetail', resolve => (data?: some) => resolve({ data }));

export const setFilter = createAction(
  'result/tour/setFilter',
  resolve => (data: TourFilterState, skipSaga: boolean = false) => resolve({ data, skipSaga }),
);

export const setFilterMaxPrice = createAction('result/tour/setFilterMaxPrice', resolve => (val: number) =>
  resolve({ val }),
);

export const search = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    dispatch(setMessage());
    const state = getState();
    let searchState;
    try {
      searchState = parseTourSearchParams(new URLSearchParams(state.router.location.search));
    } catch (err) {}
    searchState = searchState || state.search.tour;
    const result = state.result.tour;
    if (
      result.lastParams &&
      sameTourSearchParams(searchState, result.lastParams.search) &&
      sameTourFilterSortByParams(
        result.filterParams,
        result.sortBy,
        result.lastParams.filterParams,
        result.lastParams.sortBy,
      )
    ) {
      return;
    }
    const { filterParams } = state.result.tour;
    const { sortBy } = state.result.tour;
    const pageOffset = 1;

    dispatch(resetBooking());

    const usePriceFilter = filterParams.maxPrice !== -1;
    if (isTermInput(searchState.input)) {
      const { term } = searchState.input;

      dispatch(setTermResultData());
      dispatch(setLocationResultData());
      if (!usePriceFilter) {
        dispatch(setFilterMaxPrice(-1));
      }
      dispatch(setLoading(true));

      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.searchTourByTerm}`,
          'post',
          true,
          JSON.stringify({
            filters: {
              ...filterParams,
              pageOffset,
              pageSize: TOUR_PAGE_SIZE,
              priceUp: sortBy === TOUR_SORT_BY.cheap ? 1 : 0,
              sortForVNA: sortBy === TOUR_SORT_BY.default ? 1 : 0,
            },
            tour: { term, destinationId: searchState.input.locationId },
          }),
        ),
      );
      dispatch(setLoading(false));
      if (controller.cancelled) {
        return;
      }

      if (json.code === 200 || json.code === '200') {
        dispatch(setPage(1));

        if (json.data) {
          dispatch(
            addTermResultData({
              term,
              params: searchState,
              data: json.data,
            }),
          );
          dispatch(
            setLastParams({
              search: searchState,
              sortBy,
              filterParams: {
                ...filterParams,
                maxPrice: usePriceFilter ? filterParams.maxPrice : roundTourFilterPrice(json.data.maxPrice),
              },
            }),
          );
          if (!usePriceFilter) {
            dispatch(
              setFilter({ ...getState().result.tour.filterParams, maxPrice: roundTourFilterPrice(json.data.maxPrice) }),
            );
            dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
          }
          if (usePriceFilter && json.data.maxPrice !== state.result.tour.filterMaxPrice) {
            dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
          }
        }
      }
    } else {
      const id = searchState.input.locationId;

      dispatch(setTermResultData());
      dispatch(setLocationResultData());
      if (!usePriceFilter) {
        dispatch(setFilterMaxPrice(-1));
      }
      dispatch(setLoading(true));

      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.searchTourByLocation}`,
          'post',
          true,
          JSON.stringify({
            id,
            filters: {
              ...filterParams,
              pageOffset,
              pageSize: TOUR_PAGE_SIZE,
              priceUp: sortBy === TOUR_SORT_BY.cheap ? 1 : 0,
              sortForVNA: sortBy === TOUR_SORT_BY.default ? 1 : 0,
            },
          }),
        ),
      );
      dispatch(setLoading(false));
      if (controller.cancelled) {
        return;
      }

      if (json.code === 200) {
        dispatch(setPage(1));
        dispatch(addLocationResultData({ data: json.data, params: searchState }));
        dispatch(
          setLastParams({
            search: searchState,
            sortBy,
            filterParams: {
              ...filterParams,
              maxPrice: usePriceFilter ? filterParams.maxPrice : roundTourFilterPrice(json.data.maxPrice),
            },
          }),
        );
        dispatch(setMessage(json.message));
        if (!usePriceFilter) {
          dispatch(
            setFilter({ ...getState().result.tour.filterParams, maxPrice: roundTourFilterPrice(json.data.maxPrice) }),
          );
          dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
        }
        if (usePriceFilter && json.data.maxPrice !== state.result.tour.filterMaxPrice) {
          dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
        }
      }
    }
  };
});

export function searchMore(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    let searchState;
    try {
      searchState = parseTourSearchParams(new URLSearchParams(state.router.location.search));
    } catch (err) {}
    searchState = searchState || state.search.tour;
    const { filterParams } = state.result.tour;
    const { sortBy } = state.result.tour;
    const pageOffset = state.result.tour.pageOffset + 1;
    dispatch(resetBooking());
    const usePriceFilter = filterParams.maxPrice !== -1;
    if (isTermInput(searchState.input)) {
      const { term } = searchState.input;
      if (!usePriceFilter) {
        dispatch(setFilterMaxPrice(-1));
      }
      dispatch(setLoading(true));
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.searchTourByTerm}`,
          'post',
          true,
          JSON.stringify({
            filters: {
              ...filterParams,
              pageOffset,
              pageSize: TOUR_PAGE_SIZE,
              priceUp: sortBy === TOUR_SORT_BY.cheap ? 1 : 0,
              sortForVNA: sortBy === TOUR_SORT_BY.default ? 1 : 0,
            },
            tour: { term },
          }),
        ),
      );
      dispatch(setLoading(false));
      if (json.code === 400) {
        dispatch(
          replace({
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}`,
          }),
        );
        return;
      }
      if (json.code !== 200) {
        dispatch(replace('/'));
        return;
      }
      if (json.code === 200 && json.data) {
        dispatch(setPage(pageOffset));
        dispatch(addTermResultData({ term, params: searchState, data: json.data }));
        if (!usePriceFilter) {
          dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
          dispatch(
            setFilter({ ...getState().result.tour.filterParams, maxPrice: roundTourFilterPrice(json.data.maxPrice) }),
          );
        }
      }
    } else {
      const id = searchState.input.locationId;
      if (!usePriceFilter) {
        dispatch(setFilterMaxPrice(-1));
      }
      dispatch(setLoading(true));
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.searchTourByLocation}`,
          'post',
          true,
          JSON.stringify({
            id,
            filters: {
              ...filterParams,
              pageOffset,
              pageSize: TOUR_PAGE_SIZE,
              priceUp: sortBy === TOUR_SORT_BY.cheap ? 1 : 0,
              sortForVNA: sortBy === TOUR_SORT_BY.default ? 1 : 0,
            },
          }),
        ),
      );
      dispatch(setLoading(false));
      if (json.code === 400) {
        dispatch(
          replace({
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}`,
          }),
        );
        return;
      }
      if (json.code !== 200) {
        dispatch(replace('/'));
        return;
      }
      if (json.code === 200) {
        dispatch(setPage(pageOffset));
        dispatch(addLocationResultData({ data: json.data, params: searchState }));
        if (!usePriceFilter) {
          dispatch(setFilterMaxPrice(roundTourFilterPrice(json.data.maxPrice)));
          dispatch(
            setFilter({ ...getState().result.tour.filterParams, maxPrice: roundTourFilterPrice(json.data.maxPrice) }),
          );
        }
      }
    }
  };
}
export const fetchTourDetail = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const { params } = getState().booking.tour;
    if (!params) {
      return;
    }

    dispatch(setTourDetail());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.tourDetail}?id=${params.tourId}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${params.direct}`,
        'get',
        true,
      ),
    );
    if (controller.cancelled) {
      return;
    }
    if (json.code === 200) {
      dispatch(setTourDetail(json.data));
    }
  };
});

export function fetchTourReviews(
  tourId: number,
  pageOffset: number,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const searchStr = querystring.stringify({
      pageOffset,
      tourId,
      language: state.intl.locale,
      pageSize: 8,
    });
    const json = await dispatch(fetchThunk(`${API_PATHS.getTourReviews}?${searchStr}`, 'get'));
    return json;
  };
}

export function fetchTourSuggest(tourId: number): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getTourSuggest}`,
        'post',
        true,
        JSON.stringify({
          tourId,
          fromDate: moment().format(DATE_FORMAT),
          page: 1,
          size: 4,
        }),
      ),
    );
    return json;
  };
}

export const fetchPackages = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const booking = getState().booking.tour;

    if (!booking.params) {
      return null;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.tourPackage}`,
        'post',
        true,
        JSON.stringify({
          tourId: booking.params.tourId,
          departureDate: booking.date ? booking.date.format(DATE_FORMAT) : null,
        }),
      ),
    );

    if (controller.cancelled) {
      return null;
    }

    return json;
  };
});

const actions = {
  setLastParams,
  setTermResultData,
  setLocationResultData,
  addTermResultData,
  addLocationResultData,
  setSortBy,
  setTourDetail,
  setFilter,
  setFilterMaxPrice,
  setLoading,
  setReviewImagesToShow,
  setMessage,
  setPage,
};

type ActionT = ActionType<typeof actions>;

function reducer(state: TourResultState = DEFAULT_TOUR_STATE, action: ActionT): TourResultState {
  switch (action.type) {
    case getType(setReviewImagesToShow):
      return {
        ...state,
        reviewImagesToShow: action.payload.val,
      };
    case getType(setPage):
      return {
        ...state,
        pageOffset: action.payload.val,
      };
    case getType(setTermResultData):
      if (action.payload.obj) {
        return {
          ...state,
          termResultData: {
            data: action.payload.obj.data,
            input: { term: action.payload.obj.term },
          },
          locationResultData: undefined,
          message: '',
        };
      }
      return { ...state, termResultData: undefined };
    case getType(setLocationResultData):
      if (action.payload.obj) {
        return {
          ...state,
          locationResultData: action.payload.obj.data,
          termResultData: undefined,
        };
      }
      return { ...state, locationResultData: undefined };
    case getType(addTermResultData):
      if (action.payload.obj) {
        return {
          ...state,
          termResultData: {
            data: state.termResultData
              ? {
                  ...state.termResultData.data,
                  tours: state.termResultData.data.tours.concat(action.payload.obj.data.tours),
                }
              : action.payload.obj.data,
            input: { term: action.payload.obj.term },
          },
          locationResultData: undefined,
          message: '',
        };
      }
      return { ...state, termResultData: undefined };
    case getType(addLocationResultData):
      if (action.payload.obj) {
        return {
          ...state,
          locationResultData: {
            ...action.payload.obj.data,
            tours: state.locationResultData
              ? state.locationResultData.tours.concat(action.payload.obj.data.tours)
              : action.payload.obj.data.tours,
          },
          termResultData: undefined,
        };
      }
      return { ...state, locationResultData: undefined };
    case getType(setSortBy):
      return { ...state, sortBy: action.payload.val };
    case getType(setTourDetail):
      return { ...state, tourData: action.payload.data };
    case getType(setFilter):
      return { ...state, filterParams: action.payload.data };
    case getType(setLastParams):
      return { ...state, lastParams: action.payload.data };
    case getType(setFilterMaxPrice):
      return { ...state, filterMaxPrice: action.payload.val };
    case getType(setLoading):
      return { ...state, loading: action.payload.val };
    case getType(setMessage):
      return { ...state, message: action.payload.val };

    default:
      return state;
  }
}

export default reducer;
