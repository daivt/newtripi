import { combineReducers } from 'redux';
import {
  DEFAULT_FLIGHT_STATE,
  DEFAULT_HOTEL_STATE,
  DEFAULT_TOUR_STATE,
  DEFAULT_TRAIN_RESULT_STATE,
} from '../constants';
import flightHuntReducer, {
  DEFAULT_FLIGHT_HUNT_STATE,
  FlightHuntResultState,
} from './flightHuntResultReducer';
import flightReducer, { FlightResultState } from './flightResultReducer';
import hotelReducer, { HotelResultState } from './hotelResultReducer';
import rewardReducer, { DEFAULT_REWARDS_STATE, RewardResultState } from './rewardResultReducer';
import tourReducer, { TourResultState } from './tourResultReducer';
import trainReducer, { TrainResultState } from './trainResultReducer';

export interface ResultState {
  flight: FlightResultState;
  train: TrainResultState;
  hotel: HotelResultState;
  tour: TourResultState;
  flightHunt: FlightHuntResultState;
  reward: RewardResultState;
}

export const DEFAULT_RESULT_STATE = {
  flight: DEFAULT_FLIGHT_STATE,
  train: DEFAULT_TRAIN_RESULT_STATE,
  hotel: DEFAULT_HOTEL_STATE,
  tour: DEFAULT_TOUR_STATE,
  flightHunt: DEFAULT_FLIGHT_HUNT_STATE,
  reward: DEFAULT_REWARDS_STATE,
};

export default combineReducers({
  flight: flightReducer,
  train: trainReducer,
  hotel: hotelReducer,
  tour: tourReducer,
  flightHunt: flightHuntReducer,
  reward: rewardReducer,
});
