import { replace } from 'connected-react-router';
import { delay, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { ActionType, getType } from 'typesafe-actions';
import { AppState } from '../../../redux/reducers';
import { setParams } from '../../booking/redux/hotelBookingReducer';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../common/constants';
import {
  stringifyFlightHuntSearchParams,
  stringifyHotelSearchState,
  stringifyTourSearchState,
} from '../../search/utils';
import { setAirlines } from './flightHuntResultReducer';
import { setFilter as hotelSetFilter, setSortBy as hotelSetSortBy } from './hotelResultReducer';
import { setFilter as tourSetFilter, setSortBy as tourSetSortBy } from './tourResultReducer';

const FILTER_CHANGE_DELAY_PERIOD = 1000;
export function* watchHotelFilterSortingChange() {
  function* replaceUrl(action: ActionType<typeof hotelSetFilter> | ActionType<typeof hotelSetSortBy>) {
    const state: AppState = yield select();
    if (!action.payload.skipSaga && state.result.hotel.lastParams) {
      yield delay(FILTER_CHANGE_DELAY_PERIOD);
      yield put(
        replace({
          search: stringifyHotelSearchState(
            state.result.hotel.lastParams.searchParams,
            state.result.hotel.filterParams,
            state.result.hotel.sortBy,
            false,
          ),
        }),
      );
    }
  }

  yield takeLatest([getType(hotelSetFilter), getType(hotelSetSortBy)], replaceUrl);
}

export function* watchHotelDetailParamsChange() {
  function* replaceUrl(action: ActionType<typeof setParams>) {
    const { params } = action.payload;
    if (!action.payload.skipSaga) {
      yield put(
        replace({
          search: `?${stringifyHotelSearchState(params, undefined, undefined, true)}&${
            HOTEL_BOOK_PARAMS_NAMES.hotelId
          }=${params.hotelId}`,
        }),
      );
    }
  }

  yield takeEvery(getType(setParams), replaceUrl);
}

export function* watchTourFilterSortByChange() {
  function* replaceUrl(action: ActionType<typeof tourSetFilter> | ActionType<typeof tourSetSortBy>) {
    const state: AppState = yield select();
    if (!action.payload.skipSaga && state.result.tour.lastParams) {
      yield delay(FILTER_CHANGE_DELAY_PERIOD);
      yield put(
        replace({
          search: stringifyTourSearchState(
            state.result.tour.lastParams.search,
            state.result.tour.filterParams,
            state.result.tour.sortBy,
          ),
        }),
      );
    }
  }

  yield takeLatest([getType(tourSetFilter), getType(tourSetSortBy)], replaceUrl);
}

export function* watchFlightHuntAirlinesChange() {
  function* replaceUrl(action: ActionType<typeof setAirlines>) {
    const state: AppState = yield select();
    if (state.result.flightHunt.lastParams) {
      yield delay(FILTER_CHANGE_DELAY_PERIOD);
      yield put(
        replace({
          search: stringifyFlightHuntSearchParams(
            {
              ...state.result.flightHunt.lastParams,
            },
            {
              ...state.result.flightHunt.filterParams,
            },
          ),
        }),
      );
    }
  }

  yield takeLatest(getType(setAirlines), replaceUrl);
}
