import { replace } from 'connected-react-router';
import querystring from 'querystring';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, HOTEL_RESULT_CACHE_TIME, PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { makeCancelOldThunk, ThunkController } from '../../../utils';
import { HotelBookingParams } from '../../booking/redux/hotelBookingReducer';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { Location } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { HotelSearchParamsState } from '../../search/redux/hotelSearchReducer';
import { parseHotelSearchParams } from '../../search/utils';
import { DEFAULT_HOTEL_STATE } from '../constants';
import { sameFilterSortByParams, sameHotelSearchParams } from '../utils';

function convert(search: ValidHotelSearchParamsState, filterParams: HotelFilterState, sortBy: string) {
  return {
    sortBy,
    adults: search.guestInfo.adultCount,
    children: search.guestInfo.childCount,
    childrenAges: search.guestInfo.childrenAges,
    rooms: search.guestInfo.roomCount,
    checkIn: search.checkIn.format(DATE_FORMAT),
    checkOut: search.checkOut.format(DATE_FORMAT),
    provinceId: search.location.provinceId,
    districtId: search.location.districtId,
    streetId: search.location.streetId,
    filters: {
      subLocationIds: filterParams.subLocations,
      priceMax: filterParams.price[1],
      priceMin: filterParams.price[0],
      stars: filterParams.stars,
      types: filterParams.hotelTypes,
      relaxes: filterParams.facilities,
      services: [],
    },
  };
}

export interface ValidHotelSearchParamsState extends HotelSearchParamsState {
  location: Location;
}

export interface HotelFilterState {
  price: number[];
  stars: number[];
  subLocations: some[];
  hotelTypes: number[];
  facilities: string[];
}

export interface HotelResultState {
  data?: Readonly<some>;
  searching: boolean;
  sortBy: string;
  lastParams?: {
    searchParams: ValidHotelSearchParamsState;
    filter: HotelFilterState;
    sortBy: string;
  };
  searchTime: number;
  authSearch: boolean;
  filterParams: HotelFilterState;
  pageOffset: number;
}

export const setData = createAction(
  'result/hotel/setData',
  resolve => (
    searchParams?: ValidHotelSearchParamsState,
    filter?: HotelFilterState,
    sortBy?: string,
    data?: some,
    time?: number,
    auth: boolean = true,
  ) => resolve({ data, searchParams, filter, sortBy, time, auth }),
);
export const setPage = createAction('result/hotel/setPage', resolve => (val: number) => resolve({ val }));
export const setSortBy = createAction('result/hotel/setSortBy', resolve => (val: string, skipSaga?: boolean) =>
  resolve({ val, skipSaga }),
);

export const addData = createAction('result/hotel/addData', resolve => (data: some) => resolve({ data }));

export const setSearching = createAction('result/hotel/setSearching', resolve => (val: boolean) => resolve({ val }));

export const setFilter = createAction(
  'result/hotel/setFilter',
  resolve => (data: HotelFilterState, skipSaga?: boolean) => resolve({ data, skipSaga }),
);

export function searchMore(pageSize = PAGE_SIZE): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();

    if (!state.search.hotel.location) {
      return;
    }
    const search: ValidHotelSearchParamsState = {
      ...state.search.hotel,
      location: state.search.hotel.location,
    };

    const result = state.result.hotel;
    const pageOffset = state.result.hotel.pageOffset + 1;

    dispatch(setSearching(true));
    const json = await dispatch(
      fetchThunk(
        API_PATHS.searchHotel,
        'post',
        true,
        JSON.stringify({
          pageOffset,
          ...convert(search, result.filterParams, result.sortBy),
          size: pageSize,
        }),
      ),
    );
    dispatch(setSearching(false));

    if (json.code === 400) {
      dispatch(
        replace({
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}`,
        }),
      );
      return;
    }
    if (json.code !== 200) {
      dispatch(replace('/'));
      return;
    }
    dispatch(setPage(pageOffset));
    dispatch(addData(json.data));
  };
}

export const search = makeCancelOldThunk((controller: ThunkController, pageSize = PAGE_SIZE) => {
  return async (dispatch, getState) => {
    const state = getState();
    if (!state.search.hotel.location) {
      return;
    }

    let searchState;
    try {
      searchState = parseHotelSearchParams(new URLSearchParams(state.router.location.search));
    } catch (err) {}
    searchState = searchState || {
      ...state.search.hotel,
      location: state.search.hotel.location,
    };

    const result = state.result.hotel;
    if (
      result.lastParams &&
      sameHotelSearchParams(searchState, result.lastParams.searchParams) &&
      sameFilterSortByParams(result.filterParams, result.sortBy, result.lastParams.filter, result.lastParams.sortBy) &&
      new Date().getTime() - state.result.hotel.searchTime < HOTEL_RESULT_CACHE_TIME &&
      result.authSearch === state.auth.auth
    ) {
      return;
    }

    dispatch(setSearching(true));
    dispatch(setPage(1));
    dispatch(setData());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.searchHotel,
        'post',
        true,
        JSON.stringify({
          pageOffset: 1,
          ...convert(searchState, result.filterParams, result.sortBy),
          size: pageSize,
        }),
      ),
    );
    if (controller.cancelled) {
      return;
    }

    dispatch(setSearching(false));

    if (json.code === 400) {
      dispatch(
        replace({
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}`,
        }),
      );
      return;
    }
    if (json.code !== 200) {
      dispatch(replace('/'));
      return;
    }

    dispatch(
      setData(
        { ...searchState, location: searchState.location },
        result.filterParams,
        result.sortBy,
        json.data,
        new Date().getTime(),
        state.auth.auth,
      ),
    );
  };
});

export function fetchHotelDetails(
  params: HotelBookingParams,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.hotelDetail}`,
        'post',
        true,
        JSON.stringify({
          adults: params.guestInfo.adultCount,
          children: params.guestInfo.childCount,
          numRooms: params.guestInfo.roomCount,
          hotelId: params.hotelId,
          checkIn: params.checkIn.format(DATE_FORMAT),
          checkOut: params.checkOut.format(DATE_FORMAT),
          direct: params.direct,
        }),
      ),
    );
    return json;
  };
}

export function fetchHotelReviews(
  hotelId: number,
  pageOffset: number,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const searchStr = querystring.stringify({
      pageOffset,
      language: state.intl.locale,
      pageSize: PAGE_SIZE,
      rootHotelId: hotelId,
    });
    const reviewJson = await dispatch(fetchThunk(`${API_PATHS.getHotelReviews}?${searchStr}`, 'get'));
    return reviewJson;
  };
}

export function fetchroomsData(params: HotelBookingParams): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.hotelPrice}`,
        'post',
        true,
        JSON.stringify({
          hotelId: params.hotelId,
          adults: params.guestInfo.adultCount,
          children: params.guestInfo.childCount,
          numRooms: params.guestInfo.roomCount,
          checkIn: params.checkIn.format(DATE_FORMAT),
          checkOut: params.checkOut.format(DATE_FORMAT),
          childrenAges: params.guestInfo.childrenAges,
        }),
      ),
    );
    return json;
  };
}

const actions = {
  setData,
  addData,
  setSearching,
  setFilter,
  setSortBy,
  setPage,
};

type ActionT = ActionType<typeof actions>;

function reducer(state: HotelResultState = DEFAULT_HOTEL_STATE, action: ActionT): HotelResultState {
  switch (action.type) {
    case getType(setPage):
      return {
        ...state,
        pageOffset: action.payload.val,
      };
    case getType(setData):
      return {
        ...state,
        data: action.payload.data,
        lastParams:
          action.payload.filter && action.payload.searchParams && action.payload.sortBy
            ? {
                filter: action.payload.filter,
                searchParams: action.payload.searchParams,
                sortBy: action.payload.sortBy,
              }
            : undefined,
        searchTime: action.payload.time || 0,
        authSearch: action.payload.auth,
      };
    case getType(addData):
      if (state.data) {
        const current = state.data.hotels as some[];
        const hotels = current.concat(action.payload.data.hotels);
        return { ...state, data: { ...state.data, hotels } };
      }
      return state;
    case getType(setSortBy):
      return { ...state, sortBy: action.payload.val };
    case getType(setSearching):
      return { ...state, searching: action.payload.val };
    case getType(setFilter):
      return {
        ...state,
        filterParams: action.payload.data,
      };
    default:
      return state;
  }
}

export default reducer;
