import { push } from 'connected-react-router';
import { AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some, PAGE_SIZE } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { setRewardsCategories } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { REWARDS_SORT_BY } from '../constants';

export interface RewardResultState {
  data?: some;
}

export const setData = createAction('result/reward/setData', resolve => (data?: some) => resolve({ data }));

export function fetchRewards(size: number = 1): ThunkAction<Promise<some>, AppState, null, AnyAction> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(API_PATHS.getRewardsProgram, 'post', true, JSON.stringify({ size })));
    if (json.code === 200) {
      if (size === 1) {
        dispatch(setRewardsCategories(json.data.categories));
      } else {
        return json;
      }
    } else {
      dispatch(
        push({
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }),
      );
    }
    return json;
  };
}
export function fetchMyRewards(
  page: number,
  size?: number,
  id?: number,
  objectType?: string,
): ThunkAction<Promise<some>, AppState, null, AnyAction> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getRewardsHistory,
        'post',
        true,
        JSON.stringify({ id, objectType, page, size: size || PAGE_SIZE }),
      ),
    );
    if (json.code === 200) {
      return json.data;
    }
    return null;
  };
}
export function fetchRewardsByCategory(
  categoryId: number,
  sortBy: string | null,
  hasPurchase: boolean = false,
  page: number = 1,
): ThunkAction<Promise<some>, AppState, null, AnyAction> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getRewardsProgramByCategory,
        'post',
        true,
        JSON.stringify({
          page,
          categoryId,
          hasPurchase: hasPurchase || null,
          order: sortBy || REWARDS_SORT_BY.common,
          size: PAGE_SIZE,
        }),
      ),
    );
    return json;
  };
}

const actions = { setData };

type ActionT = ActionType<typeof actions>;

export const DEFAULT_REWARDS_STATE = {};

function reducer(state: RewardResultState = DEFAULT_REWARDS_STATE, action: ActionT): RewardResultState {
  switch (action.type) {
    case getType(setData):
      return { ...state, data: action.payload.data };
    default:
      return state;
  }
}

export default reducer;
