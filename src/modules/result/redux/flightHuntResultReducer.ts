import moment, { Moment } from 'moment';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some, C_DATE_FORMAT } from '../../../constants';
import { makeCancelOldThunk, ThunkController } from '../../../utils';
import { Airport, TravellerCountInfo } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { FlightHuntSearchState } from '../../search/redux/flightHuntSearchReducer';

export interface ValidFlightHuntSearchParams extends FlightHuntSearchState {
  origin: Airport;
  destination: Airport;
  travellerCountInfo: TravellerCountInfo;
  oneWay: boolean;
}

export interface FlightHuntFilterState {
  readonly airlineIds?: number[];
}

export interface FlightHuntResultState {
  departureData?: some[];
  returnData?: some[];
  lastParams?: ValidFlightHuntSearchParams;
  filterParams: FlightHuntFilterState;
  outboundDate?: Moment;
  inboundDate?: Moment;
}
export const setLastParams = createAction(
  'result/flightHunt/setLastParams',
  resolve => (lastParams: ValidFlightHuntSearchParams) => resolve({ lastParams }),
);

export const search = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const searchState = getState().search.flightHunt;

    if (!searchState.origin || !searchState.destination) {
      return;
    }
    dispatch(setDataDepart());
    dispatch(setDataReturn());
    if (searchState.oneWay) {
      dispatch(fetchDataDepart());
    } else {
      dispatch(fetchDataDepart());
      dispatch(fetchDataReturn());
    }
  };
});
const fetchDataDepart = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const searchState = getState().search.flightHunt;
    const filter = getState().result.flightHunt.filterParams;

    if (!searchState.origin || !searchState.destination) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.flightHuntSearch,
        'post',
        true,
        JSON.stringify({
          adults: searchState.travellerCountInfo.adultCount,
          children: searchState.travellerCountInfo.childCount,
          infants: searchState.travellerCountInfo.babyCount,
          fromAirport: searchState.origin.code,
          toAirport: searchState.destination.code,
          groupByDate: true,
          fromDate: moment()
            .startOf('day')
            .format(C_DATE_FORMAT),
          toDate: moment()
            .add(6, 'months')
            .format(C_DATE_FORMAT),
          filters: filter.airlineIds ? { airlineIds: filter.airlineIds } : undefined,
        }),
      ),
    );
    if (controller.cancelled) {
      return;
    }
    if (json.code === 200 || json.code === '200') {
      dispatch(setLastParams({ ...searchState, origin: searchState.origin, destination: searchState.destination }));
      dispatch(setDataDepart(json.data));
    }
  };
});
const fetchDataReturn = makeCancelOldThunk((controller: ThunkController) => {
  return async (dispatch, getState) => {
    const searchState = getState().search.flightHunt;
    const filter = getState().result.flightHunt.filterParams;

    if (!searchState.origin || !searchState.destination) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.flightHuntSearch,
        'post',
        true,
        JSON.stringify({
          adults: searchState.travellerCountInfo.adultCount,
          children: searchState.travellerCountInfo.childCount,
          infants: searchState.travellerCountInfo.babyCount,
          fromAirport: searchState.destination.code,
          toAirport: searchState.origin.code,
          groupByDate: true,
          fromDate: moment()
            .startOf('day')
            .format(C_DATE_FORMAT),
          toDate: moment()
            .add(6, 'months')
            .format(C_DATE_FORMAT),
          filters: filter.airlineIds ? { airlineIds: filter.airlineIds } : undefined,
        }),
      ),
    );
    if (controller.cancelled) {
      return;
    }
    if (json.code === 200 || json.code === '200') {
      dispatch(setLastParams({ ...searchState, origin: searchState.origin, destination: searchState.destination }));
      dispatch(setDataReturn(json.data));
    }
  };
});
export const setDataDepart = createAction('result/flightHunt/setDataDepart', resolve => (data?: some[]) =>
  resolve({ data }),
);
export const setDataReturn = createAction('result/flightHunt/setDataReturn', resolve => (data?: some[]) =>
  resolve({ data }),
);
export const setDates = createAction(
  'result/flightHunt/setDates',
  resolve => (outboundDate?: Moment, inboundDate?: Moment) => resolve({ outboundDate, inboundDate }),
);
export const setFilterParams = createAction(
  'result/flightHunt/setFilterParams',
  resolve => (filterParams: FlightHuntFilterState) => resolve({ filterParams }),
);
export const setAirlines = createAction('result/flightHunt/setAirlines', resolve => (val?: number[]) =>
  resolve({ val }),
);

const actions = {
  setDataDepart,
  setDataReturn,
  setDates,
  setAirlines,
  setFilterParams,
  setLastParams,
};

type ActionT = ActionType<typeof actions>;

export const DEFAULT_FLIGHT_HUNT_STATE = {
  filterParams: { airlineIds: [] },
};

function reducer(state: FlightHuntResultState = DEFAULT_FLIGHT_HUNT_STATE, action: ActionT): FlightHuntResultState {
  switch (action.type) {
    case getType(setDataDepart):
      return { ...state, departureData: action.payload.data };
    case getType(setDataReturn):
      return { ...state, returnData: action.payload.data };
    case getType(setDates):
      return {
        ...state,
        outboundDate: action.payload.outboundDate,
        inboundDate: action.payload.inboundDate,
      };
    case getType(setFilterParams):
      return {
        ...state,
        filterParams: action.payload.filterParams,
      };
    case getType(setAirlines):
      return {
        ...state,
        filterParams: { ...state.filterParams, airlineIds: action.payload.val },
      };
    case getType(setLastParams):
      return {
        ...state,
        lastParams: action.payload.lastParams,
      };
    default:
      return state;
  }
}

export default reducer;
