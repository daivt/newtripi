import { push } from 'connected-react-router';
import stringify from 'json-stable-stringify';
import * as React from 'react';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { TABLET_WIDTH, WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { stringifyParams } from '../../booking/utils';
import { search } from '../redux/flightResultReducer';
import { removeFilterParams } from '../utils';
import FlightResultMobileTablet from '../wvComponents/flight/FlightResultMobileTablet';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

class FlightResult extends React.PureComponent<Props> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal, filter } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      dispatch(search());
    }
    if (stringify(filter) !== stringify(prevProps.filter)) {
      window.scrollTo({ top: 0 });
    }
  }

  goToBooking = () => {
    const { booking, dispatch } = this.props;
    if (booking.tid) {
      const dest = `${WV_ROUTES.flight.flightBookingInfo}?${stringifyParams(booking.tid)}`;
      dispatch(push(dest, { backableToResult: true }));
    }
  };

  render() {
    return (
      <MediaQuery minWidth={TABLET_WIDTH}>
        {match => {
          if (match) {
            return <FlightResultMobileTablet />;
          }
          return <FlightResultMobileTablet />;
        }}
      </MediaQuery>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    params: state.result.flight.searchParams,
    filter: state.result.flight.filterParams,
    fetchSignal: removeFilterParams(`?${state.router.location.search}&${state.auth.auth}`),
  };
}

export default connect(mapStateToProps)(FlightResult);
