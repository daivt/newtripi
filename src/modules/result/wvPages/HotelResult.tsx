import * as React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { MIN_TABLET_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { search } from '../redux/hotelResultReducer';
import HotelResultMobileTablet from '../wvComponents/hotel/HotelResultMobileTablet';

interface IHotelResultProps extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const HotelResult: React.FunctionComponent<IHotelResultProps> = props => {
  const { dispatch, params, sortBy, filterParams } = props;
  React.useEffect(() => {
    dispatch(search());
  }, [dispatch, sortBy, filterParams]);
  return (
    <>
      <Helmet>
        <title>
          {params
            ? `${params.searchParams.location.provinceName}: ${params.searchParams.checkIn.format(
                'L',
              )} - ${params.searchParams.checkOut.format('L')}`
            : 'Hotel result'}
        </title>
      </Helmet>
      <MediaQuery minWidth={MIN_TABLET_WIDTH}>
        {match => {
          if (match) {
            return <HotelResultMobileTablet />;
          }
          return <HotelResultMobileTablet />;
        }}
      </MediaQuery>
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    params: state.result.hotel.lastParams,
    sortBy: state.result.hotel.sortBy,
    filterParams: state.result.hotel.filterParams,
  };
}

export default connect(mapStateToProps)(HotelResult);
