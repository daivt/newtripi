import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { isTermInput } from '../../search/redux/tourSearchReducer';
import { search } from '../redux/tourResultReducer';
import TourResultTermMobile from '../wvComponents/tour/TourResultTermMobile';
import { WV_ROUTES } from '../../../constants';
import { matchPath } from 'react-router';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
class TourResultTerm extends PureComponent<Props> {
  async componentDidMount() {
    await this.props.dispatch(search());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, fetchSignal, router } = this.props;
    if (fetchSignal !== prevProps.fetchSignal) {
      if (matchPath(WV_ROUTES.tour.tourResultTerm, router.location.pathname)) {
        dispatch(search());
      }
    }
  }

  render() {
    const { intl, locationResultData, termResultData, search } = this.props;
    let input: string = isTermInput(search.input) ? search.input.term : search.input.locationName;
    if (locationResultData) {
      input = locationResultData.destination.name;
    }
    if (termResultData) {
      input = termResultData.input.term;
    }
    return (
      <>
        <Helmet>
          <title>
            {`${intl.formatMessage({ id: 'tour' })}: ${
              locationResultData ? locationResultData.destination.name : ''
            }${termResultData ? termResultData.input.term : ''}`}
          </title>
        </Helmet>
        <TourResultTermMobile input={input} />
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    fetchSignal: `${state.router.location.search}&${state.auth.auth}`,
    locationResultData: state.result.tour.locationResultData,
    termResultData: state.result.tour.termResultData,
    search: state.search.tour,
    router: state.router,
  };
}

export default connect(mapStateToProps)(injectIntl(TourResultTerm));
