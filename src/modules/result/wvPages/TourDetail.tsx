import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { matchPath, RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { TABLET_WIDTH, WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchTourDetail } from '../redux/tourResultReducer';
import TourDetailTabletMobile from '../wvComponents/tour/TourDetailTabletMobile';

interface Props extends ReturnType<typeof mapStateToProps>, RouteComponentProps<{ id: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class TourDetail extends PureComponent<Props, State> {
  state: State = {};

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchTourDetail());
  }

  componentDidUpdate(prevProps: Props) {
    const { dispatch, location } = this.props;
    if (location.search !== prevProps.location.search) {
      if (matchPath(WV_ROUTES.tour.tourDetail, location.pathname)) {
        dispatch(fetchTourDetail());
      }
    }
  }

  render() {
    const { tourData } = this.props;

    return (
      <MediaQuery minWidth={TABLET_WIDTH}>
        {match => {
          if (match) {
            return <TourDetailTabletMobile tourData={tourData} />;
          }
          return <TourDetailTabletMobile tourData={tourData} />;
        }}
      </MediaQuery>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    tourData: state.result.tour.tourData,
    booking: state.booking.tour,
    location: state.router.location,
  };
}

export default connect(mapStateToProps)(TourDetail);
