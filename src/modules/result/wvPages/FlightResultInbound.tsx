import { push, replace } from 'connected-react-router';
import stringify from 'json-stable-stringify';
import * as React from 'react';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { FLIGHT_RESULT_INBOUND_CACHE_TIME, ROUTES, TABLET_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { stringifyParams } from '../../booking/utils';
import { DEFAULT_FILTER } from '../constants';
import { setFilter } from '../redux/flightResultReducer';
import { sameFlightSearchParams } from '../utils';
import FlightResultMobileTablet from '../wvComponents/flight/FlightResultMobileTablet';
import { parseFlightSearchParams } from '../../search/utils';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

class FlightResultInbound extends React.PureComponent<Props> {
  componentDidMount() {
    const { dispatch, booking, result, auth } = this.props;
    const params = result.searchParams;
    if (
      !params ||
      !sameFlightSearchParams(params, parseFlightSearchParams(new URLSearchParams(window.location.search))) ||
      !booking.outbound.ticket ||
      new Date().getTime() - result.searchTime > FLIGHT_RESULT_INBOUND_CACHE_TIME ||
      result.authSearch !== auth
    ) {
      dispatch(replace({ pathname: ROUTES.flight.flightResult, search: window.location.search }));
    } else {
      dispatch(setFilter({ ...DEFAULT_FILTER, price: result.filterParams.price }));
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { result } = this.props;
    if (stringify(result.filterParams) !== stringify(prevProps.result.filterParams)) {
      window.scrollTo({ top: 0 });
    }
  }

  componentWillUnmount() {
    const { result, dispatch } = this.props;
    dispatch(setFilter({ ...DEFAULT_FILTER, price: result.filterParams.price }));
  }

  goToBooking = () => {
    const { booking, dispatch } = this.props;
    if (booking.tid) {
      const dest = `${ROUTES.flight.flightBookingInfo}?${stringifyParams(booking.tid)}`;
      dispatch(push(dest, { backableToResultTwoWay: true }));
    }
  };

  render() {
    return (
      <MediaQuery minWidth={TABLET_WIDTH}>
        {match => {
          if (match) {
            return <FlightResultMobileTablet />;
          }
          return <FlightResultMobileTablet />;
        }}
      </MediaQuery>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    result: state.result.flight,
    search: state.search.flight,
    auth: state.auth.auth,
  };
}

export default connect(mapStateToProps)(FlightResultInbound);
