import { Typography } from '@material-ui/core';
import { remove } from 'lodash';
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import iconDelete from '../../../svg/ic_delete_ticket_alert.svg';
import ConfirmDialog from '../../common/components/ConfirmDialog';
import { fetchThunk } from '../../common/redux/thunks';
import { TicketAlertTabletDesktop } from '../components/TicketAlertTabletDesktop';
import { setTicketAlertPersonalInformation } from '../redux/ticketAlertReducer';

function mapState2Props(state: AppState) {
  return {
    ticketAlertPersonalInformation: state.ticketAlert.ticketAlertPersonalInformation,
  };
}

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  ticketAlertActive: some[];
  ticketAlertExpired: some[];
  isLoading: boolean;
  showDialog: boolean;
  ticketSelected?: some;
}

class TicketAlert extends React.PureComponent<Props, State> {
  state: State = {
    ticketAlertActive: [],
    ticketAlertExpired: [],
    isLoading: false,
    showDialog: false,
  };

  async fetchTicketAlert() {
    const { dispatch, ticketAlertPersonalInformation } = this.props;
    if (!ticketAlertPersonalInformation) {
      return;
    }

    this.setState({ isLoading: true });
    const ticketStatus = ticketAlertPersonalInformation.ticketStatus;
    const json = await dispatch(
      fetchThunk(`${API_PATHS.ticketAlert}?page=1&pageSize=100`, 'get', true),
    );

    if (json.code === 200) {
      this.setState({
        ticketAlertActive: json.data.filter(
          (item: some) => item.status === ticketStatus.ACTIVE.code,
        ),
        ticketAlertExpired: json.data.filter(
          (item: some) => item.status !== ticketStatus.ACTIVE.code,
        ),
        isLoading: false,
      });
    }
  }

  async componentDidMount() {
    const { dispatch, ticketAlertPersonalInformation } = this.props;

    if (!ticketAlertPersonalInformation) {
      await dispatch(fetchThunk(API_PATHS.ticketAlertGeneralInformation, 'get', true)).then(json =>
        dispatch(setTicketAlertPersonalInformation(json.data)),
      );
    }

    this.fetchTicketAlert();
  }

  async onDelete() {
    const { dispatch } = this.props;
    const { ticketSelected } = this.state;

    if (!ticketSelected) {
      return;
    }

    const json = await dispatch(
      fetchThunk(`${API_PATHS.ticketAlert}/${ticketSelected.id}`, 'delete', true),
    );

    this.setState({ showDialog: false });
    if (json.code === 200) {
      const ticketAlertActive = [...this.state.ticketAlertActive];
      const ticketAlertExpired = [...this.state.ticketAlertExpired];
      remove(ticketAlertActive, one => one.id === ticketSelected.id);
      remove(ticketAlertExpired, one => one.id === ticketSelected.id);

      this.setState({
        ticketAlertActive,
        ticketAlertExpired,
      });
    }
  }

  render() {
    const { intl } = this.props;
    const { ticketAlertActive, ticketAlertExpired, isLoading, showDialog } = this.state;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'discountNotify' })}</title>
        </Helmet>
        <TicketAlertTabletDesktop
          isLoading={isLoading}
          ticketAlertActive={ticketAlertActive}
          ticketAlertExpired={ticketAlertExpired}
          onDelete={ticketItem => this.setState({ showDialog: true, ticketSelected: ticketItem })}
        />

        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" />
              <Typography variant="body1">
                <FormattedMessage id="ticketAlert.confirmDeleteTicket" />
              </Typography>
            </div>
          }
          show={showDialog}
          cancelMessageId="ignore"
          confirmMessageId="delete"
          onCancel={() => this.setState({ showDialog: false })}
          onAccept={() => this.onDelete()}
        />
      </>
    );
  }
}

export default connect(mapState2Props)(injectIntl(TicketAlert));
