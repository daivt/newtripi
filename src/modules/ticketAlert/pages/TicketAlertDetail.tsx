import { Typography } from '@material-ui/core';
import { goBack, push } from 'connected-react-router';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ROUTES, some, DATE_FORMAT, C_DATE_FORMAT } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import iconDelete from '../../../svg/ic_delete_ticket_alert.svg';
import ConfirmDialog from '../../common/components/ConfirmDialog';
import { fetchThunk } from '../../common/redux/thunks';
import {
  FlightSearchParamsState,
  setParams as setFlightSearchParams,
} from '../../search/redux/flightSearchReducer';
import { stringifyFlightSearchStateAndFilterParams } from '../../search/utils';
import { TicketAlertDetailTabletDesktop } from '../components/TicketAlertDetailTabletDesktop';
import { fetchHotTickets } from '../redux/ticketAlertReducer';

function mapState2Props(state: AppState) {
  return {
    ticketAlertInfo: state.ticketAlert.ticketAlertInfo,
    ticketAlertPersonalInformation: state.ticketAlert.ticketAlertPersonalInformation,
  };
}
interface Props
  extends WrappedComponentProps,
    ReturnType<typeof mapState2Props>,
    RouteComponentProps<{ ticketId: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  showDialog: boolean;
  outboundTickets: some[];
  inboundTickets: some[];
}

class TicketAlertDetail extends PureComponent<Props, State> {
  state: State = {
    showDialog: false,
    outboundTickets: [],
    inboundTickets: [],
  };

  async componentDidMount() {
    const { dispatch, ticketAlertInfo } = this.props;
    if (!ticketAlertInfo) {
      dispatch(goBack());
      return;
    }

    const params = {
      fromDate: moment(ticketAlertInfo.outboundFromDate, DATE_FORMAT).format(C_DATE_FORMAT),
      toDate: moment(ticketAlertInfo.outboundToDate, DATE_FORMAT).format(C_DATE_FORMAT),
      fromAirport: ticketAlertInfo.fromAirport.code,
      toAirport: ticketAlertInfo.toAirport.code,
      adults: ticketAlertInfo.numAdults,
      children: ticketAlertInfo.numChildren,
      infants: ticketAlertInfo.numInfants,
      groupByDate: true,
    };

    const outboundTickets = await dispatch(fetchHotTickets(params));
    let inboundTickets: some[] = [];

    if (ticketAlertInfo.roundTrip) {
      params.fromDate = moment(ticketAlertInfo.inboundFromDate, DATE_FORMAT).format(C_DATE_FORMAT);
      params.toDate = moment(ticketAlertInfo.inboundToDate, DATE_FORMAT).format(C_DATE_FORMAT);
      params.toAirport = ticketAlertInfo.fromAirport.code;
      params.fromAirport = ticketAlertInfo.toAirport.code;

      inboundTickets = await dispatch(fetchHotTickets(params));
    }

    this.setState({
      outboundTickets: outboundTickets.filter(item => item.minPrice),
      inboundTickets: inboundTickets.filter(item => item.minPrice),
    });
  }

  onEdit() {
    const { ticketAlertInfo, dispatch } = this.props;

    if (!ticketAlertInfo) {
      return;
    }

    dispatch(push(ROUTES.flight.ticketAlertCreate));
  }

  async onDelete() {
    const { dispatch, ticketAlertInfo } = this.props;
    if (!ticketAlertInfo) {
      return;
    }

    const json = await dispatch(
      fetchThunk(`${API_PATHS.ticketAlert}/${ticketAlertInfo.id}`, 'delete', true),
    );

    if (json.code === 200) {
      dispatch(goBack());
    }
  }

  onSeeAllResult(search: FlightSearchParamsState) {
    const { dispatch } = this.props;
    const params = stringifyFlightSearchStateAndFilterParams(search);

    dispatch(setFlightSearchParams(search));
    dispatch(push({ pathname: `${ROUTES.flight.flightResult}`, search: `?${params}` }));
  }

  render() {
    const { intl, ticketAlertInfo, ticketAlertPersonalInformation } = this.props;
    const { showDialog, outboundTickets, inboundTickets } = this.state;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'discountNotify' })}</title>
        </Helmet>
        <TicketAlertDetailTabletDesktop
          ticketAlertInfo={ticketAlertInfo}
          outboundTickets={outboundTickets}
          inboundTickets={inboundTickets}
          onDelete={() => this.setState({ showDialog: true })}
          onEdit={() => this.onEdit()}
          onSeeAllResult={params => this.onSeeAllResult(params)}
          ticketAlertPersonalInformation={ticketAlertPersonalInformation}
        />

        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" />
              <Typography variant="body1">
                <FormattedMessage id="ticketAlert.confirmDeleteTicket" />
              </Typography>
            </div>
          }
          show={showDialog}
          cancelMessageId="ignore"
          confirmMessageId="delete"
          onCancel={() => this.setState({ showDialog: false })}
          onAccept={() => this.onDelete()}
        />
      </>
    );
  }
}

export default connect(mapState2Props)(injectIntl(TicketAlertDetail));
