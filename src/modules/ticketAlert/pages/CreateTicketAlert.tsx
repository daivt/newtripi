import { Typography } from '@material-ui/core';
import { go, goBack } from 'connected-react-router';
import moment from 'moment';
import React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { TicketAlertInfo, TicketAlertValidation } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { TicketAlertCreateTabletDesktop } from '../components/TicketAlertCreateTabletDesktop';
import { defaultTicketAlertInfo } from '../constants';
import { validateTicketAlertInfo, validTicketAlertInfo, getValidMessage } from '../utils';
import { setTicketAlertPersonalInformation } from '../redux/ticketAlertReducer';
import { toArray } from 'lodash';

function mapState2Props(state: AppState) {
  return {
    ticketAlertInfo: state.ticketAlert.ticketAlertInfo,
    ticketAlertPersonalInformation: state.ticketAlert.ticketAlertPersonalInformation,
  };
}

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  info: TicketAlertInfo;
  validation: TicketAlertValidation;
  errorMessage?: string;
  loading?: boolean;
  ticktock: boolean;
}

class CreateTicketAlert extends React.PureComponent<Props, State> {
  state: State = {
    info: this.props.ticketAlertInfo
      ? {
          id: this.props.ticketAlertInfo.id,
          fromAirport: this.props.ticketAlertInfo.fromAirport,
          toAirport: this.props.ticketAlertInfo.toAirport,
          navigator: this.props.ticketAlertInfo.navigator,
          notifyChannels: this.props.ticketAlertInfo.notifyChannels,
          roundTrip: this.props.ticketAlertInfo.roundTrip,
          numAdults: this.props.ticketAlertInfo.numAdults,
          numChildren: this.props.ticketAlertInfo.numChildren,
          numInfants: this.props.ticketAlertInfo.numInfants,
          totalExpectedPrice: this.props.ticketAlertInfo.totalExpectedPrice
            ? this.props.ticketAlertInfo.totalExpectedPrice
            : null,
          outboundTimeSegments: this.props.ticketAlertInfo.outboundTimeSegments
            ? this.props.ticketAlertInfo.outboundTimeSegments
            : [],
          inboundTimeSegments: this.props.ticketAlertInfo.inboundTimeSegments
            ? this.props.ticketAlertInfo.inboundTimeSegments
            : [],
          outboundFromDate: moment(this.props.ticketAlertInfo.outboundFromDate, DATE_FORMAT),
          outboundToDate: moment(this.props.ticketAlertInfo.outboundToDate, DATE_FORMAT),
          inboundFromDate: this.props.ticketAlertInfo.inboundFromDate
            ? moment(this.props.ticketAlertInfo.inboundFromDate, DATE_FORMAT)
            : null,
          inboundToDate: this.props.ticketAlertInfo.inboundToDate
            ? moment(this.props.ticketAlertInfo.inboundToDate, DATE_FORMAT)
            : null,
        }
      : defaultTicketAlertInfo,
    validation: {
      fromAirport: true,
      toAirport: true,
      outboundDate: true,
      outboundDateOver: '',
      inboundDate: true,
      inboundDateOver: '',
    },
    ticktock: false,
  };

  async componentDidMount() {
    const { dispatch, ticketAlertPersonalInformation } = this.props;
    const { info } = this.state;

    if (!ticketAlertPersonalInformation) {
      const json = await dispatch(fetchThunk(API_PATHS.ticketAlertGeneralInformation, 'get', true));
      dispatch(setTicketAlertPersonalInformation(json.data));

      if (!info.id) {
        const notifyChannels = toArray(json.data.notifyChannels).map(obj => obj.code);
        this.setState({ info: { ...info, notifyChannels } });
      }
    } else {
      if (!info.id) {
        const notifyChannels = toArray(ticketAlertPersonalInformation.notifyChannels).map(
          obj => obj.code,
        );

        this.setState({ info: { ...info, notifyChannels } });
      }
    }
  }

  async onCreateOrEdit() {
    const { dispatch, intl } = this.props;
    const { info, ticktock } = this.state;

    const validation: TicketAlertValidation = validateTicketAlertInfo(info);

    this.setState({ validation, loading: true });

    if (!validTicketAlertInfo(validation)) {
      const messageId = getValidMessage(validation);
      this.setState({
        loading: false,
        ticktock: !ticktock,
        errorMessage: messageId ? intl.formatMessage({ id: messageId }) : '',
      });
      return;
    }

    if (!info.toAirport || !info.fromAirport) {
      this.setState({ loading: false, ticktock: !ticktock });
      return;
    }

    const params = {
      ...info,
      fromAirport: info.fromAirport.code,
      toAirport: info.toAirport.code,
      outboundFromDate: info.outboundFromDate
        ? moment(info.outboundFromDate).format(DATE_FORMAT)
        : null,
      outboundToDate: info.outboundToDate ? moment(info.outboundToDate).format(DATE_FORMAT) : null,
      inboundFromDate: info.inboundFromDate
        ? moment(info.inboundFromDate).format(DATE_FORMAT)
        : null,
      inboundToDate: info.inboundToDate ? moment(info.inboundToDate).format(DATE_FORMAT) : null,
    };

    if (!params.id) {
      const json = await dispatch(
        fetchThunk(API_PATHS.ticketAlert, 'post', true, JSON.stringify(params)),
      );

      if (json.code === 200) {
        dispatch(goBack());
      } else {
        this.setState({ errorMessage: json.message });
      }
    } else {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.ticketAlert}/${params.id}`, 'put', true, JSON.stringify(params)),
      );

      if (json.code === 200) {
        dispatch(go(-2));
      } else {
        this.setState({ errorMessage: json.message });
      }
    }
    this.setState({ loading: false });
  }

  render() {
    const { intl, dispatch } = this.props;
    const { info, validation, errorMessage, loading, ticktock } = this.state;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'discountNotify' })}</title>
        </Helmet>

        <TicketAlertCreateTabletDesktop
          loading={loading}
          ticktock={ticktock}
          info={info}
          updateInfo={info => this.setState({ info })}
          validation={validation}
          updateValidation={validation => this.setState({ validation })}
          cancel={() => dispatch(goBack())}
          onCreateOrEdit={() => this.onCreateOrEdit()}
        />

        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '32px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}

export default connect(mapState2Props)(injectIntl(CreateTicketAlert));
