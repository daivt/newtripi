import { TicketAlertInfo } from '../common/models';

export const defaultTicketAlertInfo: TicketAlertInfo = {
  navigator: 'NOTIFY',
  notifyChannels: [],
  roundTrip: false,
  numAdults: 1,
  numChildren: 0,
  numInfants: 0,
  totalExpectedPrice: 0,
  outboundTimeSegments: [],
  inboundTimeSegments: [],
  outboundFromDate: null,
  outboundToDate: null,
  inboundFromDate: null,
  inboundToDate: null,
};
