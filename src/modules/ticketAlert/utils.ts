import { TicketAlertInfo, TicketAlertValidation } from '../common/models';
import { forIn } from 'lodash';

export function validateTicketAlertInfo(info: TicketAlertInfo): TicketAlertValidation {
  const outboundDateOver =
    info.outboundFromDate &&
    info.outboundToDate &&
    info.outboundToDate.diff(info.outboundFromDate, 'days') > 7
      ? 'ticketAlert.selectDateOver7Day'
      : '';

  const inboundDateOver =
    info.roundTrip &&
    info.inboundFromDate &&
    info.inboundToDate &&
    info.inboundToDate.diff(info.inboundFromDate, 'days') > 7
      ? 'ticketAlert.selectDateOver7Day'
      : '';

  const outboundDateError =
    info.roundTrip &&
    info.outboundToDate &&
    info.inboundFromDate &&
    info.inboundFromDate.isSameOrBefore(info.outboundToDate, 'days')
      ? 'ticketAlert.outboundDateMoreThanInboundDate'
      : '';

  return {
    outboundDateOver,
    inboundDateOver: inboundDateOver ? inboundDateOver : outboundDateError,
    fromAirport:
      !!info.fromAirport && (info.toAirport ? info.toAirport.code !== info.fromAirport.code : true),
    toAirport:
      !!info.toAirport && (info.fromAirport ? info.toAirport.code !== info.fromAirport.code : true),
    outboundDate: !!info.outboundFromDate && !!info.outboundToDate,
    inboundDate: info.roundTrip ? !!info.inboundFromDate && !!info.inboundToDate : true,
  };
}

export function validTicketAlertInfo(validation: TicketAlertValidation) {
  return (
    validation.fromAirport &&
    validation.toAirport &&
    validation.outboundDate &&
    validation.inboundDate &&
    !validation.outboundDateOver &&
    !validation.inboundDateOver
  );
}

export function getValidMessage(validation: TicketAlertValidation) {
  let result = '';
  forIn(validation, (value, key) => {
    if (typeof value === 'string' && value) {
      result = value;
      return;
    }
  });

  return result;
}
