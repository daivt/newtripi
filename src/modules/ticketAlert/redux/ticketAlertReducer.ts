import { AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';

export interface TicketAlertState {
  ticketAlertInfo?: some;
  ticketAlertPersonalInformation?: some;
}

export const setTicketAlertInfo = createAction(
  '/ticketAlert/setTicketAlertInfo',
  resolve => (data: some) => resolve({ data }),
);

export const resetTicketAlertInfo = createAction('/ticketAlert/resetTicketAlertInfo');

export const setTicketAlertPersonalInformation = createAction(
  '/ticketAlert/setTicketAlertPersonalInformation',
  resolve => (data: some) => resolve({ data }),
);

export function fetchHotTickets(
  params: some,
): ThunkAction<Promise<some[]>, AppState, null, AnyAction> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(API_PATHS.searchHotTickets, 'post', true, JSON.stringify(params)),
    );

    if (json.code === 200 || json.code === '200') {
      return json.data;
    }

    return [];
  };
}

const actions = {
  setTicketAlertInfo,
  resetTicketAlertInfo,
  setTicketAlertPersonalInformation,
};

type ActionT = ActionType<typeof actions>;

export default function reducer(state: TicketAlertState = {}, action: ActionT): TicketAlertState {
  switch (action.type) {
    case getType(resetTicketAlertInfo):
      return {
        ...state,
        ticketAlertInfo: undefined,
      };
    case getType(setTicketAlertInfo):
      return {
        ...state,
        ticketAlertInfo: action.payload.data,
      };
    case getType(setTicketAlertPersonalInformation):
      return {
        ...state,
        ticketAlertPersonalInformation: action.payload.data,
      };
    default:
      return state;
  }
}
