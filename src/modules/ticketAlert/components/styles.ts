import styled from 'styled-components';
import { GREY, PRIMARY } from '../../../colors';
import { Button } from '@material-ui/core';
import { MUI_THEME } from '../../../setupTheme';

export const GuestItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid ${GREY};
  margin-left: 12px;
`;

export const BoxButton = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-width: 130px;
  padding-right: 16px;
`;

export const MUIButton = styled(Button)<{ selected: boolean }>`
  &&& {
    background-color: ${props => (props.selected ? props.theme.primary : undefined)};
    box-shadow: none;
    text-transform: none;
    color: ${props => (props.selected ? '#fff' : PRIMARY)};
    border-color: ${PRIMARY};
    font-size: ${MUI_THEME.typography.body2.fontSize};
    border-radius: 16px;
    max-height: 32px;
    min-width: 120px;
    margin-top: 8px;
  }
`;
