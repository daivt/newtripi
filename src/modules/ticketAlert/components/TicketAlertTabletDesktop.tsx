import { Container } from '@material-ui/core';
import React from 'react';
import { some, TABLET_WIDTH } from '../../../constants';
import { PageWrapper } from '../../common/components/elements';
import Footer from '../../common/components/Footer';
import Header from '../../common/components/Header';
import TicketAlertBreadCrumbs from '../components/TicketAlertBreadCrumbs';
import TicketAlertMainBox from '../components/TicketAlertMainBox';

interface Props {
  isLoading: boolean;
  ticketAlertActive: some[];
  ticketAlertExpired: some[];
  onDelete(ticketSelected: some): void;
}

export const TicketAlertTabletDesktop: React.FC<Props> = props => {
  const { isLoading, ticketAlertActive, ticketAlertExpired, onDelete } = props;

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <TicketAlertBreadCrumbs />
        <TicketAlertMainBox
          isLoading={isLoading}
          ticketAlertActive={ticketAlertActive}
          ticketAlertExpired={ticketAlertExpired}
          onDelete={ticketItem => onDelete(ticketItem)}
        />
      </Container>
      <Footer />
    </PageWrapper>
  );
};
