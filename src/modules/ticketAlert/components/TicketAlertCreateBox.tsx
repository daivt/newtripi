import { Button, Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import LoadingButton from '../../common/components/LoadingButton/index';
import { TicketAlertInfo, TicketAlertValidation } from '../../common/models';
import TicketAlertForm from './TicketAlertForm';

interface Props {
  info: TicketAlertInfo;
  updateInfo(ticketAlertInfo: TicketAlertInfo): void;
  validation: TicketAlertValidation;
  updateValidation(validation: TicketAlertValidation): void;
  cancel(): void;
  onCreateOrEdit(): void;
  loading?: boolean;
  ticktock: boolean;
}

export const TicketAlertCreateBox: React.FC<Props> = props => {
  const {
    cancel,
    onCreateOrEdit,
    info,
    updateInfo,
    validation,
    updateValidation,
    loading,
    ticktock,
  } = props;
  return (
    <>
      <Container style={{ paddingTop: '18px' }}>
        <Typography variant="h5">
          <FormattedMessage
            id={!info.id ? 'ticketAlert.createNewAlert' : 'ticketAlert.editAlert'}
          />
        </Typography>
      </Container>

      <TicketAlertForm
        ticketAlertInfo={info}
        updateInfo={updateInfo}
        validation={validation}
        updateValidation={updateValidation}
        ticktock={ticktock}
      />

      <Container
        style={{
          paddingTop: '18px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: '72px',
        }}
      >
        <div>
          <Button
            size="large"
            variant="outlined"
            style={{ width: '190px' }}
            onClick={() => cancel()}
          >
            <Typography variant="button" color="textSecondary">
              <FormattedMessage id="ignore" />
            </Typography>
          </Button>
        </div>

        <div style={{ padding: '16px' }}>
          <LoadingButton
            size="large"
            variant="contained"
            color="secondary"
            style={{ width: '190px' }}
            disabled={loading}
            loading={loading}
            onClick={() => onCreateOrEdit()}
          >
            <div style={{ position: 'relative', width: '100%' }}>
              <Typography variant="button">
                {!info.id ? (
                  <FormattedMessage id="ticketAlert.createAlert" />
                ) : (
                  <FormattedMessage id="ticketAlert.editAlert" />
                )}
              </Typography>
            </div>
          </LoadingButton>
        </div>
      </Container>
    </>
  );
};
