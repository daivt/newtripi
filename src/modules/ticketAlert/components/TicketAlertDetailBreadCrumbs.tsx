import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, push } from 'connected-react-router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

interface Props {
  dispatch: Dispatch;
}

const TicketAlertDetailBreadCrumbs: React.FC<Props> = props => {
  return (
    <div style={{ paddingTop: '20px' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => props.dispatch(push('/'))}
        >
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => props.dispatch(go(-1))}
        >
          <FormattedMessage id="discountNotify" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="ticketAlert.detail" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

export default connect()(TicketAlertDetailBreadCrumbs);
