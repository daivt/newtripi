import { Button, Card, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { DARK_GREY, GREY } from '../../../colors';
import { some, DATE_FORMAT } from '../../../constants';
import calenderBack from '../../../svg/calender_back.svg';
import calenderNext from '../../../svg/calender_next.svg';
import iconAirPlane from '../../../svg/ic_airplane_horizontal.svg';
import iconLeftRight from '../../../svg/ic_left_right_arrow.svg';

interface Props {
  ticketAlert: some;
  isExpired: boolean;
  onDelete(ticketAlert: some): void;
  onViewDetail(ticketAlert: some): void;
}

export const TicketAlertItem: React.FC<Props> = props => {
  const { ticketAlert, isExpired } = props;
  const numPassengers = ticketAlert.numAdults + ticketAlert.numChildren + ticketAlert.numInfants;

  return (
    <Card
      style={{
        border: `1px solid ${GREY}`,
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        borderRadius: '4px',
        padding: '10px 22px 18px 18px',
        marginTop: '16px',
      }}
    >
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography variant="h5" style={{ fontWeight: 'normal' }}>
            {ticketAlert.fromAirport.name}
          </Typography>
          {ticketAlert.roundTrip ? (
            <img src={iconLeftRight} alt="" style={{ margin: '0 24px' }} />
          ) : (
            <img src={iconAirPlane} alt="" style={{ margin: '0 24px' }} />
          )}
          <Typography variant="h5" style={{ fontWeight: 'normal' }}>
            {ticketAlert.toAirport.name}
          </Typography>
        </div>

        <Typography variant="button" color="textSecondary">
          <FormattedMessage id={isExpired ? 'ticketAlert.expired' : 'ticketAlert.active'} />
        </Typography>
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-between', paddingTop: '12px' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ display: 'flex', alignItems: 'center', minWidth: '200px' }}>
            {!!ticketAlert.totalExpectedPrice && (
              <Typography variant="body2">
                <FormattedNumber value={ticketAlert.totalExpectedPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            )}

            {!!ticketAlert.totalExpectedPrice && !!numPassengers && (
              <div
                style={{
                  width: '7px',
                  height: '7px',
                  borderRadius: '50%',
                  backgroundColor: DARK_GREY,
                  marginLeft: '12px',
                  marginRight: '8px',
                }}
              />
            )}

            {!!numPassengers && (
              <Typography variant="body2">
                <FormattedMessage
                  id="result.totalPassengers"
                  values={{
                    num: numPassengers,
                  }}
                />
              </Typography>
            )}
          </div>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img src={calenderNext} alt="" style={{ margin: '0 8px' }} />
            <Typography variant="body2" style={{ display: 'flex', flexWrap: 'wrap' }}>
              <FormattedMessage id="ticketAlert.departureDateRange" />
              :&nbsp;
              <span>
                {moment(ticketAlert.outboundFromDate, DATE_FORMAT).format('DD/MM')} -&nbsp;
                {moment(ticketAlert.outboundToDate, DATE_FORMAT).format('DD/MM')}
              </span>
            </Typography>
          </div>

          {ticketAlert.inboundFromDate && (
            <div style={{ display: 'flex', alignItems: 'center', paddingLeft: '32px' }}>
              <img src={calenderBack} alt="" style={{ margin: '0 8px' }} />
              <Typography variant="body2" style={{ display: 'flex', flexWrap: 'wrap' }}>
                <FormattedMessage id="ticketAlert.returnDateRange" />
                :&nbsp;
                <span>
                  {moment(ticketAlert.inboundFromDate, DATE_FORMAT).format('DD/MM')} -&nbsp;
                  {moment(ticketAlert.inboundToDate, DATE_FORMAT).format('DD/MM')}
                </span>
              </Typography>
            </div>
          )}
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ paddingRight: '20px' }}>
            <Button
              variant="outlined"
              style={{ width: '80px' }}
              onClick={() => props.onDelete(ticketAlert)}
            >
              <Typography variant="button" color="textSecondary">
                <FormattedMessage id="delete" />
              </Typography>
            </Button>
          </div>

          <Button
            variant="contained"
            color="secondary"
            style={{ width: '160px' }}
            onClick={() => props.onViewDetail(ticketAlert)}
          >
            <Typography variant="button">
              <FormattedMessage id="ticketAlert.seeDetail" />
            </Typography>
          </Button>
        </div>
      </div>
    </Card>
  );
};
