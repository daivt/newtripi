import { Button, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import moment, { Moment } from 'moment';
import React from 'react';
import { DayPickerRangeController, FocusedInputShape, isInclusivelyAfterDay } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREY, PRIMARY, RED } from '../../../colors';
import calendarSvg from '../../../svg/calendar.svg';
import { renderMonthText } from '../../common/utils';
import { VIBRATE_KEYFRAME } from '../../search/constants';

const InputContainer = styled.div<{ isFocused: boolean; error: boolean; disable: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  opacity: ${props => (props.disable ? 0.5 : 1)};
  width: 100%;
  background-color: white;
  box-shadow: ${props =>
    props.isFocused
      ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem`
      : props.error
      ? `${fade(RED, 0.25)} 0 0 0 0.2rem`
      : ''};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : '')};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: ${props => (props.error ? '5' : '0')};
  cursor: pointer;
`;

interface Props {
  startDate: Moment | null;
  endDate: Moment | null;
  onChange(startDate: Moment | null, endDate: Moment | null): void;
  disable: boolean;
  error: boolean;
}

interface State {
  isFocused: boolean;
  focusedInput: FocusedInputShape;
  startDate: Moment | null;
  endDate: Moment | null;
}

function isDayHighlighted(day: Moment) {
  return moment().format('L') === day.format('L') ? (
    <span style={{ fontWeight: 'bold', color: PRIMARY }}>{day.format('D')}</span>
  ) : (
    day.format('D')
  );
}

class TicketAlertDatePicker extends React.PureComponent<Props, State> {
  state: State = {
    isFocused: false,
    focusedInput: 'startDate' as FocusedInputShape,
    startDate: this.props.startDate ? moment(this.props.startDate) : null,
    endDate: this.props.endDate ? moment(this.props.endDate) : null,
  };

  parent = React.createRef<HTMLDivElement>();
  firefoxWorkaroundHelper = React.createRef<HTMLButtonElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }

    const { startDate, endDate } = this.props;
    this.setState({ startDate, endDate, isFocused: false });
  };

  render() {
    const { isFocused, startDate, endDate } = this.state;
    const { onChange, disable, error } = this.props;

    return (
      <div
        style={{
          padding: '8px',
          margin: '-8px',
          borderRadius: '4px',
          background: isFocused ? 'white' : 'transparent',
          transition: 'all 300ms',
          position: 'relative',
          outline: 'none',
        }}
        tabIndex={-1}
        ref={this.parent}
        onBlur={this.onBlur}
        onFocus={e => {
          e.target !== this.parent.current &&
            this.firefoxWorkaroundHelper.current &&
            this.firefoxWorkaroundHelper.current.focus();
          /* need this check for oneWay checkbox */ e.target === e.currentTarget && this.setState({ isFocused: true });
        }}
      >
        <button
          onFocus={e => e.stopPropagation()}
          ref={this.firefoxWorkaroundHelper}
          style={{ position: 'absolute', opacity: 0, zIndex: -1 }}
        ></button>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <InputContainer
            onClick={() => (!disable ? this.setState({ isFocused: true }) : {})}
            disable={disable}
            isFocused={isFocused}
            error={error}
          >
            <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
              <img alt="" src={calendarSvg} />
            </div>
            <div
              style={{
                height: '24px',
                margin: '7px 1px',
                flex: 1,
              }}
            >
              {!startDate && !endDate && <FormattedMessage id="nothingYet" />}
              {startDate && `${startDate.format('L')}  -  `}
              {endDate ? endDate.format('L') : startDate ? <FormattedMessage id="nothingYet" /> : ''}
            </div>
          </InputContainer>
        </div>
        <div
          style={{
            opacity: isFocused ? 1 : 0,
            position: 'absolute',
            transition: 'opacity 300ms',
            visibility: isFocused ? 'visible' : 'hidden',
            zIndex: 100,
            right: 0,
            left: 0,
            marginTop: '6px',
            background: '#fff',
          }}
        >
          <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '16px' }}>
            <DayPickerRangeController
              renderMonthText={renderMonthText}
              hideKeyboardShortcutsPanel
              startDate={startDate || null}
              endDate={endDate || null}
              renderDayContents={day => isDayHighlighted(day)}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              onDatesChange={({ startDate, endDate }) => {
                const startDate_ = startDate || moment();
                this.setState({ startDate: startDate_, endDate: endDate || null });
              }}
              focusedInput={this.state.focusedInput}
              onFocusChange={focusedInput =>
                this.setState({
                  focusedInput: focusedInput || 'startDate',
                })
              }
              daySize={36}
              numberOfMonths={2}
            />
          </div>
          <div
            style={{
              margin: '22px',
              display: 'flex',
              justifyContent: 'flex-end',
            }}
          >
            <Button
              size="large"
              variant="contained"
              color="secondary"
              onClick={() => {
                this.setState({ isFocused: false });
                onChange(startDate, endDate);
              }}
            >
              <Typography variant="button">
                <FormattedMessage id="done" />
              </Typography>
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default TicketAlertDatePicker;
