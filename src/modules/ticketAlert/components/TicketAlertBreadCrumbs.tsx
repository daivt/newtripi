import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { push } from 'connected-react-router';

interface Props {
  dispatch: Dispatch;
}

const TicketAlertBreadCrumbs: React.FC<Props> = props => {
  return (
    <div style={{ paddingTop: '20px' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => props.dispatch(push('/'))}
        >
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="discountNotify" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

export default connect()(TicketAlertBreadCrumbs);
