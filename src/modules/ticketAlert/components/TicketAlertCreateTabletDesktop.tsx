import React from 'react';
import { PageWrapper } from '../../common/components/elements';
import Header from '../../common/components/Header';
import { Container } from '@material-ui/core';
import TicketAlertCreateBreadCrumbs from './TicketAlertCreateBreadCrumbs';
import { TicketAlertCreateBox } from './TicketAlertCreateBox';
import Footer from '../../common/components/Footer';
import { TicketAlertInfo, TicketAlertValidation } from '../../common/models';
import { TABLET_WIDTH } from '../../../constants';
import TicketAlertEditBreadCrumbs from './TicketAlertEditBreadCrumbs';

interface Props {
  info: TicketAlertInfo;
  updateInfo(ticketAlertInfo: TicketAlertInfo): void;
  validation: TicketAlertValidation;
  updateValidation(validation: TicketAlertValidation): void;
  cancel(): void;
  onCreateOrEdit(): void;
  loading?: boolean;
  ticktock: boolean;
}

export const TicketAlertCreateTabletDesktop: React.FC<Props> = props => {
  const {
    loading,
    info,
    validation,
    updateInfo,
    updateValidation,
    cancel,
    onCreateOrEdit,
    ticktock,
  } = props;

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        {info.id ? <TicketAlertEditBreadCrumbs /> : <TicketAlertCreateBreadCrumbs />}
        <div>
          <TicketAlertCreateBox
            loading={loading}
            ticktock={ticktock}
            info={info}
            updateInfo={info => updateInfo(info)}
            validation={validation}
            updateValidation={validation => updateValidation(validation)}
            cancel={() => cancel()}
            onCreateOrEdit={() => onCreateOrEdit()}
          />
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};
