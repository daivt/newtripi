import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedDate, FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREY, LIGHT_BLUE, SECONDARY } from '../../../colors';
import { some, SLASH_DATE_FORMAT } from '../../../constants';
import iconAirPlane from '../../../svg/ic_airplane_horizontal.svg';
import { subHeader2 } from '../../common/components/elements';

const TicketItem = styled.div<{ isSelected: boolean }>`
  background: ${props => (props.isSelected ? LIGHT_BLUE : '#ffffff')};
  color: ${props => (props.isSelected ? '#fff' : BLACK_TEXT)};
  border: 1px solid ${GREY};
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 4px;
  display: flex;
  margin-top: 12px;
  padding: 16px 20px 8px 16px;
`;

interface Props {
  ticket: some;
  ticketAlertInfo: some;
  onSelect(): void;
  outbound: boolean;
  isSelected: boolean;
}

export const TicketAlertDetailItem: React.FC<Props> = props => {
  const { ticket, ticketAlertInfo, outbound, isSelected } = props;
  return (
    <TicketItem isSelected={isSelected}>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography variant="body2" style={{ textTransform: 'capitalize' }}>
          <FormattedDate value={moment(ticket.date, SLASH_DATE_FORMAT).toDate()} weekday="short" />
          ,&nbsp;
          {ticket.date}
        </Typography>
        <div style={{ display: 'flex', alignItems: 'center', paddingTop: '12px' }}>
          <Typography variant="body2" style={subHeader2}>
            {outbound ? ticketAlertInfo.fromAirport.code : ticketAlertInfo.toAirport.code}
          </Typography>

          <img
            src={iconAirPlane}
            alt=""
            style={{
              margin: '0 22px',
              transform: outbound ? 'rotate(0deg)' : 'rotate(180deg)',
              filter: isSelected ? 'brightness(0) invert(1)' : undefined,
            }}
          />

          <Typography variant="body2" style={subHeader2}>
            {outbound ? ticketAlertInfo.toAirport.code : ticketAlertInfo.fromAirport.code}
          </Typography>
        </div>
      </div>
      <div>
        <div style={{ display: 'flex', alignItems: 'center', marginLeft: '24px' }}>
          <img src={ticket.airlineLogo} alt="" style={{ width: '32px' }} />
          <Typography variant="body2" style={{ marginLeft: '12px' }}>
            {ticket.airlineName}
          </Typography>
        </div>
      </div>

      <div style={{ display: 'flex', flex: 1, alignItems: 'flex-end', flexDirection: 'column' }}>
        <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
          <Typography variant="subtitle1" style={{ color: isSelected ? '#fff' : SECONDARY }}>
            <FormattedNumber value={ticket.minPrice} />
          </Typography>
          &nbsp;
          <Typography variant="body2" style={{ color: isSelected ? '#fff' : SECONDARY }}>
            <FormattedMessage id="currency" />
          </Typography>
        </div>

        <Button
          variant="contained"
          color="secondary"
          style={{ width: '100px', background: isSelected ? '#fff' : SECONDARY }}
          onClick={() => props.onSelect()}
        >
          <Typography variant="button" style={{ color: isSelected ? SECONDARY : '#fff' }}>
            <FormattedMessage id={isSelected ? 'deselect' : 'select'} />
          </Typography>
        </Button>
      </div>
    </TicketItem>
  );
};
