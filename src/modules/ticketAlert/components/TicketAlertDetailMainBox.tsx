import { Button, Container, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { BLUE } from '../../../colors';
import { some, DATE_FORMAT, SLASH_DATE_FORMAT } from '../../../constants';
import { Airport } from '../../common/models';
import { FlightSearchParamsState } from '../../search/redux/flightSearchReducer';
import { TicketAlertDetailItem } from './TicketAlertDetailItem';
import { orderBy, toArray } from 'lodash';
import { durationMinuteToHour } from '../../common/utils';
import PassengerCount from '../../common/components/PassengerCount';

interface Props extends WrappedComponentProps {
  ticketAlertInfo?: some;
  outboundTickets: some[];
  inboundTickets: some[];
  ticketAlertPersonalInformation?: some;
  onSeeAllResult(params: FlightSearchParamsState): void;
}

interface State {
  outboundTicket?: some;
  inboundTicket?: some;
  seeAllOutbound?: boolean;
  seeAllInbound?: boolean;
}

const ITEM_SHOW_DEFAULT = 3;

function renderTimeSegments(timeSegments: some[], ticketTimeSegments: string[]) {
  const data: string[] = [];

  timeSegments.forEach((item: some) => {
    if (ticketTimeSegments && ticketTimeSegments.indexOf(item.code) !== -1) {
      data.push(
        `${durationMinuteToHour(item.startMinute as number)} - ${durationMinuteToHour(
          item.endMinute as number,
        )}`,
      );
    }
  });

  return data.join(', ');
}

export default injectIntl(
  class TicketAlertDetailMainBox extends PureComponent<Props, State> {
    state: State = {};

    seeAllResult() {
      const { onSeeAllResult, ticketAlertInfo } = this.props;
      const { outboundTicket, inboundTicket } = this.state;

      if (!ticketAlertInfo || !outboundTicket || (ticketAlertInfo.roundTrip && !inboundTicket)) {
        return;
      }

      const params: FlightSearchParamsState = {
        origin: ticketAlertInfo.fromAirport as Airport,
        destination: ticketAlertInfo.toAirport as Airport,
        departureDate: moment(outboundTicket.date, SLASH_DATE_FORMAT),
        returnDate: inboundTicket ? moment(inboundTicket.date, SLASH_DATE_FORMAT) : undefined,
        travellerCountInfo: {
          adultCount: ticketAlertInfo.numAdults,
          childCount: ticketAlertInfo.numChildren,
          babyCount: ticketAlertInfo.numInfants,
        },
        seatClass: [],
      };

      onSeeAllResult(params);
    }

    render() {
      const {
        ticketAlertInfo,
        ticketAlertPersonalInformation,
        outboundTickets,
        inboundTickets,
      } = this.props;
      const { outboundTicket, inboundTicket, seeAllInbound, seeAllOutbound } = this.state;
      if (!ticketAlertInfo || !ticketAlertPersonalInformation) {
        return <div />;
      }

      const timeSegments = ticketAlertPersonalInformation
        ? orderBy(toArray(ticketAlertPersonalInformation.timeSegments), 'startMinute')
        : [];

      return (
        <Container>
          <Typography variant="body2" style={{ paddingTop: '8px' }}>
            <FormattedMessage id="ticketAlert.departureDateRange" />
            :&nbsp;
            {moment(ticketAlertInfo.outboundFromDate, DATE_FORMAT).format('L')} - &nbsp;
            {moment(ticketAlertInfo.outboundToDate, DATE_FORMAT).format('L')}
          </Typography>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Typography variant="body2">
              <FormattedMessage id="ticketAlert.timeSegments" />: &nbsp;
              {renderTimeSegments(timeSegments, ticketAlertInfo.outboundTimeSegments)}
            </Typography>
          </div>

          {ticketAlertInfo.roundTrip && (
            <>
              <Typography variant="body2">
                <FormattedMessage id="ticketAlert.departureDateRange" />
                :&nbsp;
                {moment(ticketAlertInfo.inboundFromDate, DATE_FORMAT).format('L')} - &nbsp;
                {moment(ticketAlertInfo.inboundToDate, DATE_FORMAT).format('L')}
              </Typography>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2">
                  <FormattedMessage id="ticketAlert.timeSegments" />: &nbsp;
                  {renderTimeSegments(timeSegments, ticketAlertInfo.inboundTimeSegments)}
                </Typography>
              </div>
            </>
          )}

          <Typography variant="body2">
            <FormattedMessage id="ticketAlert.passengerCount" />
            :&nbsp;
            <PassengerCount
              adults={ticketAlertInfo.numAdults}
              children={ticketAlertInfo.numChildren}
              babies={ticketAlertInfo.numInfants}
            />
          </Typography>

          {!!ticketAlertInfo.totalExpectedPrice && (
            <Typography variant="body2">
              <FormattedMessage id="ticketAlert.totalPrice" />
              :&nbsp;
              <FormattedNumber value={ticketAlertInfo.totalExpectedPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          )}

          {!!ticketAlertInfo.notifyChannels.length && (
            <Typography variant="body2">
              <FormattedMessage id="ticketAlert.receiveNotifyVia" />
              :&nbsp;
              <span style={{ textTransform: 'capitalize' }}>
                {ticketAlertInfo.notifyChannels.join(', ').toLowerCase()}
              </span>
            </Typography>
          )}

          <div style={{ display: 'flex', paddingTop: '20px', marginBottom: '32px' }}>
            <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
              <Typography variant="subtitle1">
                <FormattedMessage id="ticketAlert.outboundGoodPrice" />
              </Typography>
              <div style={{ marginLeft: '-32px', marginRight: '32px' }}>
                {outboundTickets.map(
                  (one: some, index: number) =>
                    one.minPrice &&
                    (!seeAllOutbound ? index < ITEM_SHOW_DEFAULT : true) && (
                      <div key={index}>
                        <TicketAlertDetailItem
                          ticket={one}
                          ticketAlertInfo={ticketAlertInfo}
                          outbound={true}
                          onSelect={() => {
                            this.setState({
                              outboundTicket: one !== outboundTicket ? one : undefined,
                            });
                          }}
                          isSelected={one === outboundTicket}
                        />
                      </div>
                    ),
                )}
              </div>
              {!seeAllOutbound && outboundTickets.length > ITEM_SHOW_DEFAULT && (
                <Typography
                  variant="body2"
                  style={{
                    color: BLUE,
                    display: 'flex',
                    justifyContent: 'flex-end',
                    marginRight: '32px',
                    marginTop: '12px',
                    cursor: 'pointer',
                  }}
                  onClick={() => this.setState({ seeAllOutbound: true })}
                >
                  <FormattedMessage id="ticketAlert.seeAllOutboundDateRange" />
                </Typography>
              )}
            </div>
            <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
              {ticketAlertInfo.roundTrip && (
                <>
                  <Typography variant="subtitle1" style={{ paddingLeft: '32px' }}>
                    <FormattedMessage id="ticketAlert.inboundGoodPrice" />
                  </Typography>
                  {inboundTickets.map(
                    (one: some, index: number) =>
                      one.minPrice &&
                      (!seeAllInbound ? index < ITEM_SHOW_DEFAULT : true) && (
                        <div key={index}>
                          <TicketAlertDetailItem
                            ticket={one}
                            ticketAlertInfo={ticketAlertInfo}
                            outbound={false}
                            onSelect={() =>
                              this.setState({
                                inboundTicket: one !== inboundTicket ? one : undefined,
                              })
                            }
                            isSelected={one === inboundTicket}
                          />
                        </div>
                      ),
                  )}

                  {!seeAllInbound && inboundTickets.length > ITEM_SHOW_DEFAULT && (
                    <Typography
                      variant="body2"
                      style={{
                        color: BLUE,
                        display: 'flex',
                        justifyContent: 'flex-end',
                        marginRight: '32px',
                        marginTop: '12px',
                        cursor: 'pointer',
                      }}
                      onClick={() => this.setState({ seeAllInbound: true })}
                    >
                      <FormattedMessage id="ticketAlert.seeAllInboundDateRange" />
                    </Typography>
                  )}
                </>
              )}
            </div>
          </div>

          <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '32px' }}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              disabled={
                ticketAlertInfo.roundTrip ? !outboundTicket || !inboundTicket : !outboundTicket
              }
              onClick={() => this.seeAllResult()}
            >
              <Typography variant="button">
                <FormattedMessage id="ticketAlert.seeAll" />
              </Typography>
            </Button>
          </div>
        </Container>
      );
    }
  },
);
