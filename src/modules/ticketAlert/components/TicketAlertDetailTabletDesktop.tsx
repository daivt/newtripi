import React from 'react';
import { PageWrapper } from '../../common/components/elements';
import { Container } from '@material-ui/core';
import Footer from '../../common/components/Footer';
import Header from '../../common/components/Header';
import TicketAlertDetailBreadCrumbs from './TicketAlertDetailBreadCrumbs';
import { TicketAlertDetailHeader } from './TicketAlertDetailHeader';
import TicketAlertDetailMainBox from './TicketAlertDetailMainBox';
import { some, TABLET_WIDTH } from '../../../constants';
import { FlightSearchParamsState } from '../../search/redux/flightSearchReducer';

interface Props {
  ticketAlertInfo?: some;
  outboundTickets: some[];
  inboundTickets: some[];
  ticketAlertPersonalInformation?: some;
  onDelete(): void;
  onEdit(): void;
  onSeeAllResult(params: FlightSearchParamsState): void;
}

export const TicketAlertDetailTabletDesktop: React.FC<Props> = props => {
  const {
    ticketAlertInfo,
    outboundTickets,
    inboundTickets,
    onDelete,
    onEdit,
    onSeeAllResult,
    ticketAlertPersonalInformation,
  } = props;

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <TicketAlertDetailBreadCrumbs />
        <Container>
          <TicketAlertDetailHeader
            ticketAlertInfo={ticketAlertInfo}
            onDelete={() => onDelete()}
            onEdit={() => onEdit()}
          />
        </Container>
        <TicketAlertDetailMainBox
          ticketAlertInfo={ticketAlertInfo}
          outboundTickets={outboundTickets}
          inboundTickets={inboundTickets}
          onSeeAllResult={params => onSeeAllResult(params)}
          ticketAlertPersonalInformation={ticketAlertPersonalInformation}
        />
      </Container>
      <Footer />
    </PageWrapper>
  );
};
