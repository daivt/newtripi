import { Checkbox, Container, FormControlLabel, IconButton, Typography } from '@material-ui/core';
import Button, { ButtonProps } from '@material-ui/core/Button';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import { orderBy, remove, toArray } from 'lodash';
import React, { PureComponent } from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { BLUE, GREY, PRIMARY } from '../../../colors';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import airportSvg from '../../../svg/airport.svg';
import iconBellBlack from '../../../svg/ic_bell_black.svg';
import iconFlightDown from '../../../svg/ic_flight_down.svg';
import iconFlightUp from '../../../svg/ic_flight_up.svg';
import { FreeTextField } from '../../booking/components/Form';
import Swap from '../../common/components/Swap';
import { Airport, TicketAlertInfo, TicketAlertValidation } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { durationMinuteToHour, validTelephoneRegex } from '../../common/utils';
import AsyncSelect from '../../search/components/AsyncSelect';
import { NotableAirports } from '../../search/constants';
import { validTravellerCount } from '../../search/utils';
import { BoxButton, GuestItem } from './styles';
import TicketAlertDatePicker from './TicketAlertDatePicker';

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  ticketAlertInfo: TicketAlertInfo;
  ticktock: boolean;
  updateInfo(ticketAlertInfo: TicketAlertInfo): void;
  validation: TicketAlertValidation;
  updateValidation(validation: TicketAlertValidation): void;
}

export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2">
          {option.location} ({option.code})
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}

const CustomButton = (props: ButtonProps & { active: boolean }) => {
  const { active, ...rest } = props;
  return (
    <Button
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active ? PRIMARY : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        padding: '5px 8px',
        textTransform: 'none',
        color: active ? 'white' : '#757575',
        minWidth: '120px',
        borderColor: active ? PRIMARY : undefined,
        opacity: rest.disabled ? 0.5 : 1,
      }}
      {...rest}
    >
      {props.children}
    </Button>
  );
};

interface State {
  switchAirport: boolean;
}

class TicketAlertForm extends PureComponent<Props, State> {
  state: State = {
    switchAirport: false,
  };

  onSelectOutboundTimeSegments(value: string) {
    const { ticketAlertInfo, updateInfo } = this.props;
    const outboundTimeSegments = [...ticketAlertInfo.outboundTimeSegments];
    remove(outboundTimeSegments, id => id === value);

    if (outboundTimeSegments.length === ticketAlertInfo.outboundTimeSegments.length) {
      outboundTimeSegments.push(value);
    }
    updateInfo({ ...ticketAlertInfo, outboundTimeSegments });
  }

  onSelectInboundTimeSegments(value: string) {
    const { ticketAlertInfo, updateInfo } = this.props;
    const inboundTimeSegments = [...ticketAlertInfo.inboundTimeSegments];
    remove(inboundTimeSegments, id => id === value);

    if (inboundTimeSegments.length === ticketAlertInfo.inboundTimeSegments.length) {
      inboundTimeSegments.push(value);
    }
    updateInfo({ ...ticketAlertInfo, inboundTimeSegments });
  }

  onSelectNotifyChanel(isChecked: boolean, value: string) {
    const { ticketAlertInfo, updateInfo } = this.props;
    const notifyChannels = [...ticketAlertInfo.notifyChannels];

    if (isChecked) {
      notifyChannels.push(value);
    } else {
      remove(notifyChannels, id => id === value);
    }

    updateInfo({ ...ticketAlertInfo, notifyChannels });
  }

  render() {
    const {
      dispatch,
      ticketAlertPersonalInformation,
      ticketAlertInfo,
      updateInfo,
      intl,
      validation,
      updateValidation,
      ticktock,
    } = this.props;

    const { switchAirport } = this.state;

    const timeSegments = ticketAlertPersonalInformation
      ? orderBy(toArray(ticketAlertPersonalInformation.timeSegments), 'startMinute')
      : [];

    return (
      <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
        <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
          <Container style={{ display: 'flex', alignItems: 'flex-end' }}>
            <AsyncSelect<Airport>
              key={`fromAirport-${ticktock}${switchAirport}`}
              error={!validation.fromAirport}
              minimizedWidth="215px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightUp} />}
              defaultValue={ticketAlertInfo.fromAirport ? getLabel(ticketAlertInfo.fromAirport) : ''}
              title={<FormattedMessage id="search.chooseOrigin" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseOrigin' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              onSelect={fromAirport => {
                updateInfo({ ...ticketAlertInfo, fromAirport });
                updateValidation({ ...validation, fromAirport: true });
              }}
            />
            <div
              style={{
                padding: '8px 0',
                width: '0px',
                overflow: 'visible',
                position: 'relative',
                zIndex: 10,
                display: 'flex',
                justifyContent: 'center',
                height: '40px',
                boxSizing: 'content-box',
                alignItems: 'center',
              }}
            >
              <Swap
                onClick={async () => {
                  const { fromAirport } = ticketAlertInfo;
                  updateInfo({
                    ...ticketAlertInfo,
                    fromAirport: ticketAlertInfo.toAirport,
                    toAirport: fromAirport,
                  });
                  this.setState(state => ({ switchAirport: !state.switchAirport }));
                }}
              />
            </div>
            <AsyncSelect<Airport>
              key={`toAirport-${ticktock}${switchAirport}`}
              error={!validation.toAirport}
              minimizedWidth="215px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightDown} />}
              defaultValue={ticketAlertInfo.toAirport ? getLabel(ticketAlertInfo.toAirport) : ''}
              title={<FormattedMessage id="search.chooseDestination" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseDestination' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              onSelect={toAirport => {
                updateInfo({ ...ticketAlertInfo, toAirport });
                updateValidation({ ...validation, toAirport: true });
              }}
            />
          </Container>
          <Container>
            <Typography variant="subtitle2" style={{ paddingTop: '12px' }}>
              <FormattedMessage id="ticketAlert.guestCountInfo" />
            </Typography>
            <GuestItem>
              <div style={{ display: 'flex', alignItems: 'center', padding: '10px 0' }}>
                <Typography variant="body1">
                  <FormattedMessage id="search.travellerInfo.adult" /> -&nbsp;
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  <FormattedMessage id="search.travellerInfo.adultDef" />
                </Typography>
              </div>
              <BoxButton>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults - 1,
                      ticketAlertInfo.numChildren,
                      ticketAlertInfo.numInfants,
                    )
                  }
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numAdults: ticketAlertInfo.numAdults - 1,
                    })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults - 1,
                        ticketAlertInfo.numChildren,
                        ticketAlertInfo.numInfants,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <Typography variant="body2">{ticketAlertInfo.numAdults}</Typography>
                <IconButton
                  size="small"
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numAdults: ticketAlertInfo.numAdults + 1,
                    })
                  }
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults + 1,
                      ticketAlertInfo.numChildren,
                      ticketAlertInfo.numInfants,
                    )
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults + 1,
                        ticketAlertInfo.numChildren,
                        ticketAlertInfo.numInfants,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </BoxButton>
            </GuestItem>
            <GuestItem>
              <div style={{ display: 'flex', alignItems: 'center', padding: '10px 0' }}>
                <Typography variant="body1">
                  <FormattedMessage id="search.travellerInfo.children" /> -&nbsp;
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  <FormattedMessage id="search.travellerInfo.childrenDef" />
                </Typography>
              </div>
              <BoxButton>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults,
                      ticketAlertInfo.numChildren - 1,
                      ticketAlertInfo.numInfants,
                    )
                  }
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numChildren: ticketAlertInfo.numChildren - 1,
                    })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults,
                        ticketAlertInfo.numChildren - 1,
                        ticketAlertInfo.numInfants,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <Typography variant="body2">{ticketAlertInfo.numChildren}</Typography>
                <IconButton
                  size="small"
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numChildren: ticketAlertInfo.numChildren + 1,
                    })
                  }
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults,
                      ticketAlertInfo.numChildren + 1,
                      ticketAlertInfo.numInfants,
                    )
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults,
                        ticketAlertInfo.numChildren + 1,
                        ticketAlertInfo.numInfants,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </BoxButton>
            </GuestItem>
            <GuestItem>
              <div style={{ display: 'flex', alignItems: 'center', padding: '10px 0' }}>
                <Typography variant="body1">
                  <FormattedMessage id="search.travellerInfo.baby" /> -&nbsp;
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  <FormattedMessage id="search.travellerInfo.babyDef" />
                </Typography>
              </div>
              <BoxButton>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults,
                      ticketAlertInfo.numChildren,
                      ticketAlertInfo.numInfants - 1,
                    )
                  }
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numInfants: ticketAlertInfo.numInfants - 1,
                    })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults,
                        ticketAlertInfo.numChildren,
                        ticketAlertInfo.numInfants - 1,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <Typography variant="body2">{ticketAlertInfo.numInfants}</Typography>
                <IconButton
                  size="small"
                  onClick={() =>
                    updateInfo({
                      ...ticketAlertInfo,
                      numInfants: ticketAlertInfo.numInfants + 1,
                    })
                  }
                  disabled={
                    !validTravellerCount(
                      ticketAlertInfo.numAdults,
                      ticketAlertInfo.numChildren,
                      ticketAlertInfo.numInfants + 1,
                    )
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '22px',
                      color: !validTravellerCount(
                        ticketAlertInfo.numAdults,
                        ticketAlertInfo.numChildren,
                        ticketAlertInfo.numInfants + 1,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </BoxButton>
            </GuestItem>
            <div style={{ paddingTop: '20px' }}>
              <FreeTextField
                optional
                style={{ margin: 0 }}
                text={`${ticketAlertInfo.totalExpectedPrice ? ticketAlertInfo.totalExpectedPrice : ''}`}
                header={intl.formatMessage({ id: 'ticketAlert.priceTotal' })}
                placeholder={intl.formatMessage({ id: 'ticketAlert.enterPriceExpect' })}
                update={price =>
                  updateInfo({
                    ...ticketAlertInfo,
                    totalExpectedPrice: parseInt(price, 10),
                  })
                }
                regex={validTelephoneRegex}
              />
            </div>
          </Container>
          <Container>
            <Typography variant="subtitle2" style={{ paddingTop: '22px' }}>
              <FormattedMessage id="ticketAlert.doYouWantReceiveNotify" />
            </Typography>
            <div>
              {ticketAlertPersonalInformation &&
                toArray(ticketAlertPersonalInformation.notifyChannels).map((item: some) => (
                  <div key={item.code}>
                    <FormControlLabel
                      style={{ outline: 'none', margin: '0' }}
                      label={<Typography variant="body2">{item.code}</Typography>}
                      labelPlacement="end"
                      onChange={(e, isCheck) => this.onSelectNotifyChanel(isCheck, item.code)}
                      control={
                        <Checkbox color="primary" checked={ticketAlertInfo.notifyChannels.indexOf(item.code) !== -1} />
                      }
                    />
                  </div>
                ))}

              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  marginLeft: '12px',
                  marginTop: '10px',
                  paddingTop: '12px',
                  borderTop: `1px solid ${GREY}`,
                }}
              >
                <img src={iconBellBlack} alt="" />
                <Typography variant="body2" style={{ paddingLeft: '12px' }}>
                  <FormattedMessage
                    id="ticketAlert.receiveNotifyOnThisPC"
                    values={{
                      text: (
                        <span style={{ color: BLUE }}>
                          <FormattedMessage id="ticketAlert.showNow" />
                        </span>
                      ),
                    }}
                  />
                </Typography>
              </div>
            </div>
          </Container>
        </div>
        <div style={{ flex: 1 }}>
          <div style={{ display: 'flex', flexDirection: 'column', padding: '8px 16px' }}>
            <Typography variant="body2" style={{ height: '30px', paddingLeft: '8px' }}>
              <FormattedMessage id="ticketAlert.departureDateRange" />
            </Typography>
            <TicketAlertDatePicker
              key={`outboundDate${ticktock}`}
              error={!validation.outboundDate || !!validation.outboundDateOver}
              disable={false}
              startDate={ticketAlertInfo.outboundFromDate}
              endDate={ticketAlertInfo.outboundToDate}
              onChange={(startDate, endDate) => {
                updateInfo({
                  ...ticketAlertInfo,
                  outboundFromDate: startDate,
                  outboundToDate: endDate,
                });
                updateValidation({ ...validation, outboundDate: true, outboundDateOver: '' });
              }}
            />
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                paddingTop: '8px',
              }}
            >
              {timeSegments.map(item => (
                <CustomButton
                  key={item.code}
                  active={ticketAlertInfo.outboundTimeSegments.indexOf(item.code) !== -1}
                  onClick={() => this.onSelectOutboundTimeSegments(item.code)}
                >
                  <Typography variant="body2" style={{ lineHeight: '19px' }}>
                    {durationMinuteToHour(item.startMinute as number)} -&nbsp;
                    {durationMinuteToHour(item.endMinute as number)}
                  </Typography>
                </CustomButton>
              ))}
            </div>
          </div>
          <div style={{ padding: '8px 16px' }}>
            <FormControlLabel
              style={{ outline: 'none', margin: '0' }}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="ticketAlert.returnDateRange" />
                </Typography>
              }
              labelPlacement="end"
              control={
                <Checkbox
                  onChange={event =>
                    updateInfo({
                      ...ticketAlertInfo,
                      roundTrip: event.currentTarget.checked,
                    })
                  }
                  color="primary"
                  checked={ticketAlertInfo.roundTrip}
                />
              }
            />
            <TicketAlertDatePicker
              key={`inboundDate${ticktock}`}
              error={ticketAlertInfo.roundTrip && (!validation.inboundDate || !!validation.inboundDateOver)}
              disable={!ticketAlertInfo.roundTrip}
              startDate={ticketAlertInfo.inboundFromDate}
              endDate={ticketAlertInfo.inboundToDate}
              onChange={(startDate, endDate) => {
                updateInfo({
                  ...ticketAlertInfo,
                  inboundFromDate: startDate,
                  inboundToDate: endDate,
                });
                updateValidation({ ...validation, inboundDate: true, inboundDateOver: '' });
              }}
            />
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                paddingTop: '8px',
              }}
            >
              {timeSegments.map(item => (
                <CustomButton
                  key={item.code}
                  active={ticketAlertInfo.inboundTimeSegments.indexOf(item.code) !== -1}
                  disabled={!ticketAlertInfo.roundTrip}
                  onClick={() => this.onSelectInboundTimeSegments(item.code)}
                >
                  <Typography variant="body2" style={{ lineHeight: '19px' }}>
                    {durationMinuteToHour(item.startMinute as number)} -&nbsp;
                    {durationMinuteToHour(item.endMinute as number)}
                  </Typography>
                </CustomButton>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapState2Props(state: AppState) {
  return {
    ticketAlert: state.ticketAlert,
    ticketAlertPersonalInformation: state.ticketAlert.ticketAlertPersonalInformation,
  };
}
export default connect(mapState2Props)(injectIntl(TicketAlertForm));
