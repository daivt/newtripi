import { Button, Card, Container, Typography, ButtonBase } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { push } from 'connected-react-router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Dispatch } from 'redux';
import { BLUE, SECONDARY } from '../../../colors';
import { ROUTES, some } from '../../../constants';
import { resetTicketAlertInfo, setTicketAlertInfo } from '../redux/ticketAlertReducer';
import { TicketAlertItem } from './TicketAlertItem';
import iconNoResult from '../../../svg/ic_contact_no_result.svg';
import iconWarning from '../../../svg/ic_warning.svg';

interface Props {
  dispatch: Dispatch;
  onDelete(ticketItem: some): void;
  isLoading: boolean;
  ticketAlertActive: some[];
  ticketAlertExpired: some[];
}

const MAX_TICKET_ALERT = 5;

function renderTicketAlertSkeleton() {
  return (
    <Card
      style={{
        height: '100px',
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        borderRadius: '4px',
        padding: '10px 22px 18px 18px',
        marginTop: '12px',
      }}
    >
      <Skeleton width="30%" height="30px" variant="text" style={{ margin: 0 }}></Skeleton>
      <div
        style={{
          display: 'flex',
          paddingTop: '12px',
        }}
      >
        <Skeleton width="168px" />
        <Skeleton width="30%" style={{ marginLeft: '32px' }} />
      </div>
    </Card>
  );
}

const TicketAlertMainBox: React.FC<Props> = props => {
  const { ticketAlertActive, ticketAlertExpired, isLoading, dispatch, onDelete } = props;
  const [showExpiredTicket, setShowExpiredTicket] = React.useState(false);

  const onViewDetail = React.useCallback(
    (ticketAlertInfo: some) => {
      dispatch(setTicketAlertInfo(ticketAlertInfo));
      dispatch(push(ROUTES.flight.ticketAlertDetail.setUrl(ticketAlertInfo.id)));
    },
    [dispatch],
  );

  return (
    <Container>
      <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '12px' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <Typography variant="h5">
            <FormattedMessage
              id={showExpiredTicket ? 'ticketAlert.notifyExpired' : 'discountNotify'}
            />
          </Typography>
          {ticketAlertActive.length < MAX_TICKET_ALERT ? (
            <Link
              to={ROUTES.flight.ticketAlertCreate}
              onClick={() => {
                dispatch(resetTicketAlertInfo());
              }}
              style={{ textDecoration: 'none' }}
            >
              <Button size="large" color="secondary" variant="contained">
                <Typography variant="button">
                  <FormattedMessage id="ticketAlert.createNewAlert" />
                </Typography>
              </Button>
            </Link>
          ) : (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <img src={iconWarning} style={{ width: '24px', height: '24px' }} alt="" />
              <Typography variant="body2" style={{ color: SECONDARY, paddingLeft: '12px' }}>
                <FormattedMessage
                  id="ticketAlert.maxTicketAlertDescription"
                  values={{ num: MAX_TICKET_ALERT }}
                />
              </Typography>
            </div>
          )}
        </div>
        {!showExpiredTicket && (
          <Typography variant="body2">
            <FormattedMessage id="ticketAlert.systemAutoNotice" />
          </Typography>
        )}
      </div>

      {showExpiredTicket
        ? ticketAlertExpired.map(one => (
            <TicketAlertItem
              key={one.id}
              isExpired={true}
              ticketAlert={one}
              onDelete={ticketItem => onDelete(ticketItem)}
              onViewDetail={ticketItem => onViewDetail(ticketItem)}
            />
          ))
        : ticketAlertActive.map(one => (
            <TicketAlertItem
              key={one.id}
              isExpired={false}
              ticketAlert={one}
              onDelete={ticketItem => onDelete(ticketItem)}
              onViewDetail={ticketItem => onViewDetail(ticketItem)}
            />
          ))}

      {isLoading ? (
        <>
          {renderTicketAlertSkeleton()}
          {renderTicketAlertSkeleton()}
          {renderTicketAlertSkeleton()}
        </>
      ) : (
        <>
          {!showExpiredTicket ? (
            !ticketAlertActive.length ? (
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  marginTop: '72px',
                }}
              >
                <img src={iconNoResult} alt="" />
                <Typography variant="body2">
                  <FormattedMessage id="ticketAlert.noResult" />
                </Typography>

                <Typography variant="body2">
                  <FormattedMessage
                    id="ticketAlert.clickHereToCreate"
                    values={{
                      text: (
                        <Link
                          to={ROUTES.flight.ticketAlertCreate}
                          onClick={() => {
                            dispatch(resetTicketAlertInfo());
                          }}
                          style={{ textDecoration: 'none', color: BLUE }}
                        >
                          <FormattedMessage id="ticketAlert.clickHere" />
                        </Link>
                      ),
                    }}
                  />
                </Typography>

                {!!ticketAlertExpired.length && (
                  <Typography
                    variant="body2"
                    style={{ color: BLUE, cursor: 'pointer', padding: '22px 0px' }}
                    onClick={() => setShowExpiredTicket(true)}
                  >
                    <FormattedMessage id="ticketAlert.seeTicketExpired" />
                  </Typography>
                )}
              </div>
            ) : (
              <ButtonBase style={{ borderRadius: '4px', margin: '22px 0' }}>
                <Typography
                  variant="body2"
                  style={{ color: BLUE, cursor: 'pointer', padding: '3px 16px' }}
                  onClick={() => setShowExpiredTicket(true)}
                >
                  <FormattedMessage id="ticketAlert.seeTicketExpired" />
                </Typography>
              </ButtonBase>
            )
          ) : !ticketAlertExpired.length ? (
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                marginTop: '72px',
              }}
            >
              <img src={iconNoResult} alt="" />
              <Typography variant="body2">
                <FormattedMessage id="ticketAlert.noResult" />
              </Typography>

              {ticketAlertActive.length < MAX_TICKET_ALERT && (
                <Typography variant="body2">
                  <FormattedMessage
                    id="ticketAlert.clickHereToCreate"
                    values={{
                      text: (
                        <Link
                          to={ROUTES.flight.ticketAlertCreate}
                          onClick={() => {
                            dispatch(resetTicketAlertInfo());
                          }}
                          style={{ textDecoration: 'none', color: BLUE }}
                        >
                          <FormattedMessage id="ticketAlert.clickHere" />
                        </Link>
                      ),
                    }}
                  />
                </Typography>
              )}

              {!!ticketAlertActive.length && (
                <Typography
                  variant="body2"
                  style={{ color: BLUE, cursor: 'pointer', padding: '22px 0px' }}
                  onClick={() => setShowExpiredTicket(false)}
                >
                  <FormattedMessage id="ticketAlert.seeTicketActive" />
                </Typography>
              )}
            </div>
          ) : (
            <ButtonBase style={{ borderRadius: '4px', margin: '22px 0' }}>
              <Typography
                variant="body2"
                style={{ color: BLUE, cursor: 'pointer', padding: '3px 16px' }}
                onClick={() => setShowExpiredTicket(false)}
              >
                <FormattedMessage id="ticketAlert.seeTicketActive" />
              </Typography>
            </ButtonBase>
          )}
        </>
      )}
    </Container>
  );
};

export default connect()(TicketAlertMainBox);
