import React from 'react';
import { some } from '../../../constants';
import { Typography, Button } from '@material-ui/core';
import iconAirPlane from '../../../svg/ic_airplane_horizontal.svg';
import iconLeftRight from '../../../svg/ic_left_right_arrow.svg';
import { FormattedMessage } from 'react-intl';
import { GREY } from '../../../colors';

interface Props {
  ticketAlertInfo?: some;
  onDelete(): void;
  onEdit(): void;
}

export const TicketAlertDetailHeader: React.FC<Props> = props => {
  const { ticketAlertInfo, onDelete, onEdit } = props;

  if (!ticketAlertInfo) {
    return <div />;
  }

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        paddingTop: '22px',
        paddingBottom: '12px',
        borderBottom: `1px solid ${GREY}`,
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Typography variant="h5">{ticketAlertInfo.fromAirport.name}</Typography>
        {ticketAlertInfo.roundTrip ? (
          <img src={iconLeftRight} alt="" style={{ margin: '0 24px' }} />
        ) : (
          <img src={iconAirPlane} alt="" style={{ margin: '0 24px' }} />
        )}
        <Typography variant="h5">{ticketAlertInfo.toAirport.name}</Typography>
      </div>

      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ paddingRight: '20px' }}>
          <Button
            variant="outlined"
            style={{ width: '80px' }}
            size="large"
            onClick={() => onDelete()}
          >
            <Typography variant="button" color="textSecondary">
              <FormattedMessage id="delete" />
            </Typography>
          </Button>
        </div>

        <Button
          variant="contained"
          color="secondary"
          style={{ width: '160px' }}
          size="large"
          onClick={() => onEdit()}
        >
          <Typography variant="button">
            <FormattedMessage id="ticketAlert.editAlert" />
          </Typography>
        </Button>
      </div>
    </div>
  );
};
