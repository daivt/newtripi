import { Container } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { PRIMARY } from '../../../colors';
import { ReactComponent as LogoTripi } from '../../../svg/logo_tripi.svg';
import { BoxHeader, HeaderContainer, SubHeader } from '../../common/components/Header.styles';
import LanguageSelecter from '../../intl/components/LanguageSelecter';

class AuthHeader extends PureComponent {
  render() {
    return (
      <HeaderContainer>
        <BoxHeader light>
          <SubHeader light>
            <Container
              style={{
                height: '66px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Link to="/">
                <LogoTripi fill={PRIMARY} />
              </Link>
              <div
                style={{
                  alignItems: 'center',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  flexShrink: 0,
                  flexGrow: 1,
                }}
              >
                <LanguageSelecter />
              </div>
            </Container>
          </SubHeader>
        </BoxHeader>
      </HeaderContainer>
    );
  }
}

export default AuthHeader;
