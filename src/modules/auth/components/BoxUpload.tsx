import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import IconDelete from '@material-ui/icons/HighlightOffOutlined';
import React from 'react';
import { useDropzone } from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DARK_GREY, GREY } from '../../../colors';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconSquare } from '../../../svg/ic_square_upload.svg';
import { IMAGE_ALLOW_TYPE } from '../../common/constants';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  multiple: boolean;
  imageUrl?: string;
  onDrop(file: File[]): void;
  removePhoto(): void;
}

const BoxUpload: React.FunctionComponent<Props> = props => {
  const { getRootProps, getInputProps } = useDropzone({
    noKeyboard: true,
    multiple: props.multiple,
    onDrop: acceptedFiles => props.onDrop(acceptedFiles),
    accept: IMAGE_ALLOW_TYPE,
  });

  return (
    <div
      style={{
        minWidth: '348px',
        minHeight: '286px',
        border: `1px solid ${GREY}`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        position: 'relative',
        cursor: 'pointer',
      }}
    >
      <div
        {...getRootProps()}
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          width: '100%',
          position: 'relative',
        }}
      >
        <input {...getInputProps()} />
        {props.imageUrl ? (
          <>
            <div style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, zIndex: 1 }}>
              <img style={{ width: '100%', height: '100%', objectFit: 'contain' }} src={props.imageUrl} alt="" />
            </div>
          </>
        ) : (
          <>
            <div>
              <IconSquare />
            </div>
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="signUp.dragAndDropImage" />
            </Typography>
          </>
        )}
      </div>
      {props.imageUrl && (
        <div style={{ position: 'absolute', top: 0, right: 0, zIndex: 2 }}>
          <IconButton
            onClick={props.removePhoto}
            style={{ padding: '6px', margin: '2px', background: 'rgba(255, 255, 255, 0.3)' }}
          >
            <IconDelete style={{ color: DARK_GREY }} />
          </IconButton>
        </div>
      )}
    </div>
  );
};

export default connect()(BoxUpload);
