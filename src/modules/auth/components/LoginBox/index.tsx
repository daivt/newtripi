import * as React from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../redux/reducers';
import { setLoginErrorMsg } from '../../redux/authReducer';
import ConfirmOtpCode from './ConfirmOtpCode';
import LoginComponent from './LoginComponent';
import ConfirmSendOTPBox from './ConfirmSendOTPBox';

type Phase = 'login' | 'sendOTP' | 'confirmOTP';

const mapState2Props = (state: AppState) => {
  return {};
};
export interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onPage?: boolean;
}

export interface State {
  loginId: string;
  password: string;
}

const LoginBox: React.FunctionComponent<Props> = props => {
  const { dispatch, onPage } = props;
  const [loginId, setId] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [phase, setPhase] = React.useState<Phase>('login');

  React.useEffect(() => {
    dispatch(setLoginErrorMsg());
  }, [dispatch]);

  return (
    <>
      {phase === 'login' && (
        <LoginComponent
          loginId={loginId}
          password={password}
          onChange={(newLoginId, newPassword) => {
            setId(newLoginId);
            setPassword(newPassword);
          }}
          onPage={onPage}
          requireOTP={() => setPhase('sendOTP')}
        />
      )}
      {phase === 'sendOTP' && <ConfirmSendOTPBox loginId={loginId} nextPhase={() => setPhase('confirmOTP')} />}
      {phase === 'confirmOTP' && <ConfirmOtpCode loginId={loginId} password={password} />}
    </>
  );
};

export default connect(mapState2Props)(injectIntl(LoginBox));
