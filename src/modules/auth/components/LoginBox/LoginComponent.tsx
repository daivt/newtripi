import { Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE, DARK_BLUE, RED } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { BootstrapInput } from '../../../common/components/elements';
import LoadingButton from '../../../common/components/LoadingButton';
import { NewDeviceLoginError } from '../../constants';
import { AuthDialog, setAuthDialog, setLoginErrorMsg } from '../../redux/authReducer';
import { login } from '../../redux/authThunks';

const mapState2Props = (state: AppState) => {
  return {
    authenticating: state.auth.authenticating,
    loginErrorMsg: state.auth.loginErrorMsg,
  };
};
export interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onPage?: boolean;
  onChange(loginId: string, password: string): void;
  loginId: string;
  password: string;
  requireOTP(): void;
}

const LoginComponent: React.FunctionComponent<Props> = props => {
  const { dispatch, authenticating, loginErrorMsg, intl, onPage, onChange, loginId, password, requireOTP } = props;
  React.useEffect(() => {
    dispatch(setLoginErrorMsg());
  }, [dispatch]);
  const logIn = React.useCallback(
    async (e: any) => {
      e.preventDefault();
      const res = await dispatch(login(loginId, password));
      if (res === NewDeviceLoginError) {
        requireOTP();
      }
    },
    [dispatch, loginId, password, requireOTP],
  );
  return (
    <div style={{ padding: '12px 108px' }}>
      <Typography variant="h5" style={{ fontWeight: 'normal' }}>
        <FormattedMessage id="auth.loginTripiVia" />
      </Typography>
      <div style={{ width: '425px' }}>
        <form onSubmit={logIn}>
          <div style={{ marginTop: '25px' }}>
            <Typography style={{ marginLeft: '10px' }} variant="body2">
              <FormattedMessage id="auth.telOrEmail" />
            </Typography>
            <BootstrapInput
              fullWidth
              value={loginId}
              onChange={e => onChange(e.target.value, password)}
              placeholder={intl.formatMessage({ id: 'auth.telOrEmailPlaceholder' })}
            />
          </div>
          <div style={{ marginTop: '25px' }}>
            <Typography style={{ marginLeft: '10px' }} variant="body2">
              <FormattedMessage id="auth.password" />
            </Typography>
            <BootstrapInput
              fullWidth
              type="password"
              value={password}
              onChange={e => onChange(loginId, e.target.value)}
              placeholder="******"
            />
          </div>
          <div style={{ textAlign: 'center' }}>
            <div style={{ padding: '5px', color: RED, marginBottom: '-8px' }}>
              <Typography variant="body2">{loginErrorMsg ? <span>{loginErrorMsg}</span> : <>&nbsp;</>}</Typography>
            </div>
            <div
              style={{
                color: DARK_BLUE,
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Typography
                variant="body2"
                onClick={() =>
                  onPage ? dispatch(push(ROUTES.forgotPassword)) : dispatch(setAuthDialog(AuthDialog.forgotPassword))
                }
                style={{ cursor: 'pointer' }}
              >
                <FormattedMessage id="auth.forgotPassword" />
              </Typography>
            </div>
            <LoadingButton
              size="large"
              type="submit"
              style={{ width: '260px', marginTop: '15px' }}
              variant="contained"
              color="secondary"
              loading={authenticating}
              onClick={logIn}
            >
              <FormattedMessage id="signIn" />
            </LoadingButton>
            <div style={{ marginTop: '15px' }}>
              <Typography variant="body2">
                <FormattedMessage id="auth.dontHaveAccount" />
                &nbsp;
                <Link to={ROUTES.signUp} style={{ color: BLUE }}>
                  <FormattedMessage id="signUp" />
                </Link>
              </Typography>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default connect(mapState2Props)(injectIntl(LoginComponent));
