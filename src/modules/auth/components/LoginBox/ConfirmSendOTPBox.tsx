import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { GREY, RED } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import LoadingButton from '../../../common/components/LoadingButton';
import { requestLoginOTP } from '../../redux/authThunks';
import { setLoginErrorMsg } from '../../redux/authReducer';

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  loginId: string;
  nextPhase(): void;
}

const ConfirmSendOTPBox: React.FunctionComponent<Props> = props => {
  const { loginId, loginErrorMsg, dispatch, nextPhase } = props;
  const [fetching, setFetching] = React.useState(false);
  const sendNewDeviceLoginOTP = React.useCallback(async () => {
    try {
      setFetching(true);
      await dispatch(requestLoginOTP(loginId));
      await nextPhase();
      setFetching(false);
    } catch (e) {}
  }, [dispatch, loginId, nextPhase]);
  React.useEffect(() => {
    dispatch(setLoginErrorMsg());
  }, [dispatch]);
  return (
    <div style={{ padding: '0px 24px 34px 24px' }}>
      <Typography variant="h6">
        <FormattedMessage id="auth.verifyDevice" />
      </Typography>
      <div style={{ width: '600px' }}>
        <Typography variant="subtitle2" style={{ margin: '16px 0px' }}>
          <FormattedMessage id="auth.verifyDeviceNote" />
        </Typography>
        <Typography variant="body1" color="textSecondary">
          <FormattedMessage id="auth.verifyDeviceNote2" />
        </Typography>
        <div
          style={{
            border: `1px  solid ${GREY}`,
            borderRadius: '4px',
            boxSizing: 'border-box',
            display: 'flex',
            alignItems: 'center',
            padding: '4px 12px',
            height: '40px',
            overflow: 'hidden',
            marginTop: '12px',
          }}
        >
          <Typography variant="body1">{loginId.replace(/.{7}/, '*******')}</Typography>
        </div>
        <div style={{ padding: '5px', color: RED, marginBottom: '24px', marginTop: '12px' }}>
          <Typography variant="body2">{loginErrorMsg ? <span>{loginErrorMsg}</span> : <>&nbsp;</>}</Typography>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <LoadingButton
            size="large"
            type="submit"
            style={{ minWidth: '170px', height: '40px' }}
            variant="contained"
            color="secondary"
            loading={fetching}
            onClick={sendNewDeviceLoginOTP}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="auth.sendOtp" />
            </Typography>
          </LoadingButton>
        </div>
      </div>
    </div>
  );
};
const mapState2Props = (state: AppState) => {
  return {
    loginErrorMsg: state.auth.loginErrorMsg,
  };
};
export default connect(mapState2Props)(ConfirmSendOTPBox);
