import { Typography } from '@material-ui/core';
import moment, { now } from 'moment';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { RED } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { FreeTextField } from '../../../booking/components/Form';
import LoadingButton from '../../../common/components/LoadingButton';
import { validTelephoneRegex } from '../../../common/utils';
import { setLoginErrorMsg } from '../../redux/authReducer';
import { login, requestLoginOTP } from '../../redux/authThunks';

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  loginId: string;
  password: string;
}

const ConfirmOtpCode: React.FunctionComponent<Props> = props => {
  const { loginId, password, dispatch, authenticating, loginErrorMsg, intl, reSendOtp } = props;
  const [otp, setOTP] = React.useState('');
  const [fetching, setFetching] = React.useState(false);
  const [countTime, setCountTime] = React.useState(now());
  const [valid, setValid] = React.useState(true);
  const [ticktock, setTick] = React.useState(false);
  const seconds = React.useMemo((): number => moment(reSendOtp).diff(moment(countTime), 'seconds'), [
    countTime,
    reSendOtp,
  ]);
  React.useEffect(() => {
    dispatch(setLoginErrorMsg());
  }, [dispatch]);
  React.useEffect(() => {
    if (seconds <= 0) {
      return undefined;
    }
    const id = setTimeout(() => setCountTime(now()), 1000);
    return () => clearTimeout(id);
  }, [dispatch, seconds]);

  const sendNewDeviceLoginOTP = React.useCallback(async () => {
    setFetching(true);
    await dispatch(requestLoginOTP(loginId));
    setCountTime(now());
    setFetching(false);
  }, [dispatch, loginId]);
  const callLogin = React.useCallback(
    async (e: any) => {
      e.preventDefault();
      setTick(tick => !tick);
      setValid(!!otp.trim());
      if (otp.trim()) {
        await dispatch(login(loginId, password, otp));
        setValid(!!loginErrorMsg);
      }
    },
    [dispatch, loginId, password, otp, loginErrorMsg],
  );
  return (
    <div style={{ padding: '0px 24px 34px 24px' }}>
      <Typography variant="h6">
        <FormattedMessage id="auth.verifyDevice" />
      </Typography>
      <div style={{ width: '600px' }}>
        <form onSubmit={callLogin}>
          <Typography variant="body1" color="textSecondary" style={{ marginTop: '16px' }}>
            <FormattedMessage id="auth.verifyDeviceNote3" />
          </Typography>
          <FreeTextField
            key={`otp${ticktock}`}
            style={{ marginTop: '48px' }}
            valid={valid}
            text={otp}
            update={text => {
              dispatch(setLoginErrorMsg());
              setOTP(text);
              setValid(true);
            }}
            regex={validTelephoneRegex}
            placeholder={intl.formatMessage({ id: 'auth.otp' })}
            autoFocus
          />
          <Typography variant="caption" color="textSecondary" style={{ marginLeft: '8px' }}>
            &#40;&nbsp;
            {seconds}
            ~&nbsp;
            <FormattedMessage id="seconds" />
            &#41;
          </Typography>
          <div style={{ padding: '5px', color: RED, marginBottom: '24px', marginTop: '12px' }}>
            <Typography variant="body2">{loginErrorMsg ? <span>{loginErrorMsg}</span> : <>&nbsp;</>}</Typography>
          </div>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <LoadingButton
              size="large"
              type="submit"
              style={{ minWidth: '170px', height: '40px', marginRight: '16px' }}
              variant="contained"
              color="secondary"
              loading={authenticating}
              onClick={callLogin}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="done" />
              </Typography>
            </LoadingButton>
            <LoadingButton
              size="large"
              type="submit"
              style={{ minWidth: '170px', height: '40px', opacity: seconds > 0 ? 0.5 : 1 }}
              variant="outlined"
              loading={fetching}
              onClick={sendNewDeviceLoginOTP}
              disabled={seconds > 0}
            >
              <Typography variant="subtitle2" color="textSecondary">
                <FormattedMessage id="auth.reSendOtp" />
              </Typography>
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};
const mapState2Props = (state: AppState) => {
  return {
    authenticating: state.auth.authenticating,
    loginErrorMsg: state.auth.loginErrorMsg,
    reSendOtp: state.auth.reSendOTP,
  };
};
export default connect(mapState2Props)(injectIntl(ConfirmOtpCode));
