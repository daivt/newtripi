import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { AuthState } from '../redux/authReducer';

const mapStateToProps = (state: AppState) => ({
  email: '',
});
export interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AuthState, null, Action<string>>;
  // email: string;
}

class ResetLinkSent extends React.PureComponent<Props> {
  public render() {
    const { email } = this.props;
    return (
      <div style={{ padding: '40px 40px', width: '640px' }}>
        <Typography variant="h5">
          <FormattedMessage id="auth.mailTitle" />
        </Typography>
        <Typography variant="body2" style={{ marginTop: '8px' }}>
          <FormattedMessage id="auth.mailHint" />
        </Typography>
        <div style={{ color: '#635f5fde', display: 'flex', justifyContent: 'center' }}>{email}</div>
        <div style={{ marginTop: '16px' }}>
          <div style={{ display: 'flex', justifyContent: 'space-around' }}>
            <Button
              style={{ width: '200px', height: '40px', textAlign: 'center' }}
              variant="contained"
              color="secondary"
            >
              <FormattedMessage id="auth.email" />
            </Button>
            <div style={{ alignSelf: 'center' }}>
              <FormattedMessage id="auth.mailResend" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(ResetLinkSent);
