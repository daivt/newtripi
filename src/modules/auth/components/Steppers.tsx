import Step from '@material-ui/core/Step';
import StepConnector from '@material-ui/core/StepConnector';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  connectorActive: {
    '& $connectorLine': {
      borderColor: theme.palette.primary.main,
      borderTopStyle: 'dashed',
      borderTopWidth: 'thin',
    },
  },
  connectorCompleted: {
    '& $connectorLine': {
      borderColor: theme.palette.primary.main,
      borderTopStyle: 'dashed',
      borderTopWidth: 'thin',
    },
  },
  connectorDisabled: {
    '& $connectorLine': {
      borderColor: theme.palette.text.secondary,
      borderTopStyle: 'dashed',
      borderTopWidth: 'thin',
    },
  },
  connectorLine: {
    transition: theme.transitions.create('border-color'),
  },
  iconContainer: {
    transform: 'scale(1.5)',
  },
}));

interface Props extends WrappedComponentProps {
  activeStep: number;
  getSteps(): string[];
}

const Steppers: React.FunctionComponent<Props> = props => {
  const classes = useStyles();
  const steps = props.getSteps();

  const connector = (
    <StepConnector
      classes={{
        active: classes.connectorActive,
        completed: classes.connectorCompleted,
        disabled: classes.connectorDisabled,
        line: classes.connectorLine,
      }}
    />
  );

  return (
    <div className={classes.root}>
      <Stepper
        style={{ background: 'transparent' }}
        alternativeLabel
        activeStep={props.activeStep}
        connector={connector}
      >
        {steps.map(label => (
          <Step key={label}>
            <StepLabel
              classes={{
                iconContainer: classes.iconContainer,
              }}
            >
              <span>{props.intl.formatMessage({ id: label })}</span>
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  );
};

export default injectIntl(Steppers);
