import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import IconDelete from '@material-ui/icons/HighlightOffOutlined';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { DARK_GREY, GREY, RED } from '../../../colors';
import { AppState } from '../../../redux/reducers';
import { FreeTextField } from '../../booking/components/Form';
import { snackbarSetting } from '../../common/components/elements';
import { AgencyInformation, AgencyInformationValidation } from '../../common/models';
import { PHOTO_TYPE } from '../constants';
import { uploadFile } from '../redux/authThunks';
import BoxUpload from './BoxUpload';

const Box = styled.div`
  background-color: #fff;
  width: 768px;
  display: flex;
  flex-direction: column;
  padding: 40px 24px 0;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14);
  margin-bottom: 32px;
`;

export interface IAgencyInformationBoxProps extends WrappedComponentProps, WithSnackbarProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  agencyInfo: AgencyInformation;
  agencyInfoValidate: AgencyInformationValidation;
  update(agencyInformation: AgencyInformation, validation?: AgencyInformationValidation): void;
  updatePhoto(data: string, type: string): void;
  ticktock: boolean;
}

export interface IAgencyInformationBoxState {}

class AgencyInformationBox extends React.PureComponent<IAgencyInformationBoxProps, IAgencyInformationBoxState> {
  state: IAgencyInformationBoxState = {};

  async uploadPhoto(files: File[], photoType: string) {
    const { dispatch, updatePhoto } = this.props;
    const json = await dispatch(uploadFile(files[0]));
    const { enqueueSnackbar, closeSnackbar } = this.props;

    if (json.code === 200) {
      updatePhoto(json.photo.link, photoType);
    } else if (json.code === 400) {
      const object = JSON.parse(json.message);
      const message = object[0].value;
      enqueueSnackbar(
        message,
        snackbarSetting(message || <FormattedMessage id="copied" />, key => closeSnackbar(key)),
      );
    }
  }

  removePhoto(photoType: string, index: number = 0) {
    const { update, agencyInfo } = this.props;

    if (photoType === PHOTO_TYPE.FRONT_IDENTITY_CARD) {
      update({ ...agencyInfo, frontIdentityCard: '' });
    } else if (photoType === PHOTO_TYPE.BACK_IDENTITY_CARD) {
      update({ ...agencyInfo, backIdentityCard: '' });
    } else {
      const businessProfiles = [...agencyInfo.businessProfiles];
      businessProfiles.splice(index, 1);

      update({
        ...agencyInfo,
        businessProfiles,
      });
    }
  }

  public render() {
    const { agencyInfo, agencyInfoValidate, update, intl, ticktock } = this.props;

    return (
      <Box>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant="h6">
            <FormattedMessage id="signUp.verifyInfoPartner" />
          </Typography>

          <Typography variant="body1" style={{ color: DARK_GREY, paddingTop: '16px' }}>
            <FormattedMessage id="signUp.provideFullInfor" />
          </Typography>

          <Typography variant="h6" style={{ paddingTop: '20px' }}>
            <FormattedMessage id="signUp.passport" />
          </Typography>

          <div style={{ width: '50%', padding: '8px 0' }}>
            <FreeTextField
              key={`identityCard${ticktock}`}
              text={agencyInfo.identityCard}
              valid={agencyInfoValidate.identityCard}
              header={intl.formatMessage({ id: 'signUp.passportLabel' })}
              placeholder={intl.formatMessage({ id: 'signUp.passportExample' })}
              update={value =>
                update({ ...agencyInfo, identityCard: value }, { ...agencyInfoValidate, identityCard: true })
              }
            />
          </div>

          <div style={{ display: 'flex', justifyContent: 'space-between', paddingTop: '32px' }}>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="body1" style={{ paddingLeft: '16px' }}>
                <FormattedMessage id="signUp.passportImageFront" />
              </Typography>

              <BoxUpload
                multiple={false}
                imageUrl={agencyInfo.frontIdentityCard}
                onDrop={files => this.uploadPhoto(files, PHOTO_TYPE.FRONT_IDENTITY_CARD)}
                removePhoto={() => this.removePhoto(PHOTO_TYPE.FRONT_IDENTITY_CARD)}
              />
            </div>

            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="body1" style={{ paddingLeft: '16px' }}>
                <FormattedMessage id="signUp.passportImageBackSide" />
              </Typography>

              <BoxUpload
                multiple={false}
                imageUrl={agencyInfo.backIdentityCard}
                onDrop={files => this.uploadPhoto(files, PHOTO_TYPE.BACK_IDENTITY_CARD)}
                removePhoto={() => this.removePhoto(PHOTO_TYPE.BACK_IDENTITY_CARD)}
              />
            </div>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '20px' }}>
            <Typography variant="h6">
              <FormattedMessage id="signUp.bankAccount" />
            </Typography>

            <Typography variant="body1" style={{ paddingTop: '12px', paddingBottom: '10px', color: RED }}>
              <FormattedMessage id="signUp.bankAccountNote" />
            </Typography>

            <div style={{ width: '50%' }}>
              <FreeTextField
                key={`accountName${ticktock}`}
                text={agencyInfo.accountName}
                valid={agencyInfoValidate.accountName}
                header={intl.formatMessage({ id: 'signUp.accountName' })}
                placeholder={intl.formatMessage({ id: 'signUp.accountNameExample' })}
                update={value =>
                  update({ ...agencyInfo, accountName: value }, { ...agencyInfoValidate, identityCard: true })
                }
              />
            </div>

            <div style={{ width: '50%', padding: '28px 0' }}>
              <FreeTextField
                key={`accountNumber${ticktock}`}
                text={agencyInfo.accountNumber}
                valid={agencyInfoValidate.accountNumber}
                header={intl.formatMessage({ id: 'signUp.accountNumber' })}
                placeholder={intl.formatMessage({ id: 'signUp.passportExample' })}
                update={value =>
                  update({ ...agencyInfo, accountNumber: value }, { ...agencyInfoValidate, identityCard: true })
                }
              />
            </div>

            <div style={{ width: '50%' }}>
              <FreeTextField
                key={`bankCode${ticktock}`}
                text={agencyInfo.bankCode}
                valid={agencyInfoValidate.bankCode}
                header={intl.formatMessage({ id: 'signUp.bank' })}
                placeholder={intl.formatMessage({ id: 'signUp.bankCodeExample' })}
                update={value =>
                  update({ ...agencyInfo, bankCode: value }, { ...agencyInfoValidate, identityCard: true })
                }
              />
            </div>
          </div>

          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              paddingTop: '40px',
              marginBottom: agencyInfo.businessProfiles.length ? '32px' : 0,
            }}
          >
            <Typography variant="h6">
              <FormattedMessage id="signUp.businessRecord" />
            </Typography>
            <Typography variant="body1" style={{ paddingTop: '8px', color: DARK_GREY }}>
              <FormattedMessage id="signUp.informationPermission" />
            </Typography>

            <Typography variant="body1" style={{ paddingTop: '8px', color: DARK_GREY }}>
              <FormattedMessage id="signUp.permissionOne" />
            </Typography>

            <Typography variant="body1" style={{ paddingTop: '24px', color: DARK_GREY }}>
              <FormattedMessage id="signUp.permissionTwo" />
            </Typography>

            <Typography variant="body1" style={{ paddingTop: '24px', color: DARK_GREY }}>
              <FormattedMessage id="signUp.permissionThree" />
            </Typography>

            <div style={{ width: '50%', paddingTop: '8px', paddingBottom: '16px' }}>
              <BoxUpload
                multiple={false}
                removePhoto={() => {}}
                onDrop={files => this.uploadPhoto(files, PHOTO_TYPE.BUSINESS_PROFILE)}
              />
            </div>
            <div
              style={{
                display: 'flex',
                flexWrap: 'wrap',
              }}
            >
              {agencyInfo.businessProfiles.map((item: string, index: number) => (
                <div
                  key={item}
                  style={{
                    width: '150px',
                    height: '150px',
                    margin: '4px',
                    border: `1px solid ${GREY}`,
                    position: 'relative',
                  }}
                >
                  <img
                    style={{
                      width: '100%',
                      height: '100%',
                    }}
                    src={item}
                    alt=""
                  />

                  <div style={{ position: 'absolute', top: 0, right: 0, zIndex: 2 }}>
                    <IconButton
                      onClick={() => this.removePhoto(PHOTO_TYPE.BUSINESS_PROFILE, index)}
                      style={{ padding: '2px', background: 'rgba(255, 255, 255, 0.3)' }}
                    >
                      <IconDelete style={{ color: DARK_GREY }} />
                    </IconButton>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Box>
    );
  }
}

export default connect()(withSnackbar(injectIntl(AgencyInformationBox)));
