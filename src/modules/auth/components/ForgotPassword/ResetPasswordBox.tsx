import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { FreeTextField } from '../../../booking/components/Form';
import LoadingButton from '../../../common/components/LoadingButton/index';
import MessageDialog from '../../../common/components/MessageDialog';
import NewPasswordBox from '../../../common/components/NewPasswordBox';
import { MAX_OTP_SECONDS } from '../../constants';
import { AuthDialog, setAuthDialog } from '../../redux/authReducer';
import { forgotPassword, sendForgotPasswordOTP } from '../../redux/authThunks';
import { SecondsDown } from '../SecondsDown';
import { Box } from './styles';

export interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  usePage?: boolean;
  phone: string;
}

export interface State {
  otp: string;
  password: string;
  rePassword: string;
  valid: {
    password: boolean;
    rePassword: boolean;
    otp: boolean;
  };
  loading: boolean;
  message?: string;
  showDialog: boolean;
  ticktock: boolean;
  counting: boolean;
}

class ResetPasswordBox extends React.PureComponent<Props, State> {
  state: State = {
    otp: '',
    password: '',
    rePassword: '',
    valid: {
      password: true,
      rePassword: true,
      otp: true,
    },
    loading: false,
    showDialog: false,
    ticktock: false,
    counting: true,
  };

  stopCounting = () => {
    this.setState({ counting: false });
  };

  async resetPassword() {
    const { password, rePassword, otp } = this.state;
    const { dispatch, phone, usePage } = this.props;

    this.setState({
      loading: false,
      valid: {
        otp: otp.length > 4,
        password: password.length >= 6,
        rePassword: rePassword === password,
      },
    });

    const valid = password.length >= 6 && password === rePassword && otp.trim().length > 4;

    if (!valid) {
      return;
    }

    const json = await dispatch(
      forgotPassword({
        password,
        otp,
        user_name: phone,
      }),
    );

    this.setState({ loading: false });

    if (json.code === 200) {
      if (usePage) {
        dispatch(replace(ROUTES.loginAfterResetPass));
      } else {
        dispatch(setAuthDialog(AuthDialog.loginAfterResetPass));
      }
    } else {
      this.setState({ message: json.message, showDialog: true });
    }
  }

  async resendOTP() {
    const { dispatch, phone } = this.props;
    const response = await dispatch(sendForgotPasswordOTP(phone));
    if (response.message) {
      this.setState({ message: response.message, showDialog: true, counting: true });
    }
  }

  public render() {
    const { intl, usePage, phone } = this.props;
    const { otp, password, rePassword, valid, loading, message, showDialog, ticktock, counting } = this.state;

    return (
      <Box usePage={usePage || false} style={{ marginBottom: '32px' }}>
        <div>
          <Typography variant="h5" style={{ fontWeight: 'normal' }}>
            <FormattedMessage id="auth.otpTitle" />
          </Typography>
          <Typography variant="body2" style={{ marginTop: '8px' }} color="textSecondary">
            <FormattedMessage id="auth.otpMes" values={{ phone: <span style={{ fontWeight: 500 }}>{phone}</span> }} />
            &nbsp;
          </Typography>
          <div style={{ marginTop: '16px', marginBottom: '32px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ flex: 1 }}>
                <form>
                  <FreeTextField
                    key={`otp${ticktock}`}
                    text={otp}
                    valid={valid.otp}
                    header={intl.formatMessage({ id: 'auth.otp' })}
                    placeholder={intl.formatMessage({ id: 'signUp.otpExample' })}
                    update={newOtp => {
                      this.setState({
                        otp: newOtp,
                        valid: {
                          ...valid,
                          otp: true,
                        },
                      });
                    }}
                  />
                </form>
              </div>

              <div style={{ display: 'flex', alignItems: 'flex-end', paddingLeft: '24px' }}>
                <Button disabled={counting} style={{ padding: '11px 0' }} onClick={() => this.resendOTP()}>
                  <Typography variant="button" color="textSecondary" style={{ padding: '0 16px' }}>
                    <FormattedMessage id="auth.otpResend" />
                  </Typography>
                </Button>
              </div>
            </div>

            <div style={{ padding: '10px 2px' }}>
              {counting && <SecondsDown num={MAX_OTP_SECONDS} onCountingCompleted={this.stopCounting} />}
            </div>
          </div>
        </div>
        <Typography variant="h5" style={{ fontWeight: 'normal' }}>
          <FormattedMessage id="auth.changePassword" />
        </Typography>
        <div style={{ width: '425px' }}>
          <NewPasswordBox
            ticktock={ticktock}
            password={password}
            confirmPassword={rePassword}
            passwordValidation={valid.password}
            confirmPasswordValidation={valid.rePassword}
            updatePassword={(newPassword, validPassword) => {
              this.setState({
                password: newPassword,
                valid: { ...valid, password: validPassword },
              });
            }}
            updateConfirmPassword={(newRePassword, validRePassword) => {
              this.setState({
                rePassword: newRePassword,
                valid: { ...valid, rePassword: validRePassword },
              });
            }}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              paddingTop: '44px',
              paddingBottom: '32px',
              marginRight: '20px',
            }}
          >
            <LoadingButton
              style={{ width: '260px' }}
              variant="contained"
              color="secondary"
              size="large"
              loading={loading}
              onClick={() => {
                this.resetPassword();
                this.setState({ ticktock: !ticktock });
              }}
            >
              <Typography variant="button">
                <FormattedMessage id="auth.changePassword" />
              </Typography>
            </LoadingButton>
          </div>
        </div>

        <MessageDialog
          show={!!showDialog}
          onClose={() => this.setState({ showDialog: false })}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '10px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{message}</Typography>
            </div>
          }
        />
      </Box>
    );
  }
}

export default connect()(injectIntl(ResetPasswordBox));
