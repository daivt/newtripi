import { InputAdornment, Typography } from '@material-ui/core';
import IconWarning from '@material-ui/icons/WarningOutlined';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { FreeTextField } from '../../../booking/components/Form';
import LoadingButton from '../../../common/components/LoadingButton/index';
import { validTelephoneRegex, validVietnamTelephoneRegex } from '../../../common/utils';
import { AuthDialog, setAuthDialog } from '../../redux/authReducer';
import { FORGOT_PASSWORD_STEP } from '../../utils';
import { Box } from './styles';
import { sendForgotPasswordOTP } from '../../redux/authThunks';

interface State {
  telephone: string;
  valid: { telephone: boolean };
  loading: boolean;
  message: string;
  ticktock: boolean;
}

export interface IForgotPassWordBoxProps extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  usePage?: boolean;
  update(phone: string, step: number): void;
}

class ForgotPasswordBox extends React.PureComponent<IForgotPassWordBoxProps, State> {
  state: State = {
    telephone: '',
    valid: { telephone: true },
    loading: false,
    message: '',
    ticktock: false,
  };

  submit = async () => {
    const { telephone } = this.state;
    const { dispatch, usePage, update } = this.props;

    const validTel = validVietnamTelephoneRegex.test(telephone);
    this.setState({ loading: true, valid: { telephone: validTel } });

    if (!validTel) {
      this.setState({ loading: false });
      return;
    }

    const json = await dispatch(sendForgotPasswordOTP(telephone));
    this.setState({ loading: false });
    if (json.code === 200) {
      update(telephone, FORGOT_PASSWORD_STEP.RESET_PASSWORD);
      if (!usePage) {
        dispatch(setAuthDialog(AuthDialog.forgotPasswordREPASS));
      }
    } else {
      this.setState({ message: json.message });
    }
  };

  public render() {
    const { telephone, valid, loading, message, ticktock } = this.state;
    const { intl, usePage } = this.props;
    return (
      <Box usePage={usePage || false}>
        <Typography variant="h5" style={{ fontWeight: 'normal' }}>
          <FormattedMessage id="auth.forgotPassword" />
        </Typography>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant="subtitle2" style={{ paddingTop: '36px', paddingBottom: '8px', fontWeight: 'normal' }}>
            <FormattedMessage id="auth.resetPasswordByTel" />
          </Typography>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage id="auth.pleaseEnterPhoneNumber" />
          </Typography>
          <div style={{ paddingTop: '8px' }}>
            <FreeTextField
              key={`telephone${ticktock}`}
              text={telephone}
              valid={valid.telephone}
              header={intl.formatMessage({ id: 'telephone' })}
              placeholder={intl.formatMessage({ id: 'signUp.phoneExample' })}
              update={value => {
                this.setState({
                  telephone: value,
                  valid: {
                    telephone: true,
                  },
                  message: '',
                });
              }}
              endAdornment={
                <InputAdornment position="end">
                  {!valid.telephone ? <IconWarning color="error" style={{ paddingRight: '6px' }} /> : <></>}
                </InputAdornment>
              }
              regex={validTelephoneRegex}
            />
            <Typography variant="caption" color="error" style={{ paddingLeft: '8px' }}>
              {message}
            </Typography>
          </div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
              paddingTop: '48px',
              paddingBottom: '32px',
            }}
          >
            <LoadingButton
              size="large"
              style={{ width: '260px' }}
              variant="contained"
              color="secondary"
              onClick={() => {
                this.submit();
                this.setState({ ticktock: !ticktock });
              }}
              loading={loading}
            >
              <Typography variant="button">
                <FormattedMessage id="auth.changePassword" />
              </Typography>
            </LoadingButton>
            <div style={{ marginTop: '16px' }}>
              <Typography variant="body2" style={{ color: '#757575' }}>
                <FormattedMessage id="auth.dontHaveAccount" />
                &nbsp;
                <Link to={ROUTES.signUp} style={{ color: BLUE }}>
                  <FormattedMessage id="signUp" />
                </Link>
              </Typography>
            </div>
          </div>
        </div>
      </Box>
    );
  }
}

export default injectIntl(connect()(ForgotPasswordBox));
