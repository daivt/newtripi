import { Dialog, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { AuthDialog, setAuthDialog } from '../redux/authReducer';
import ForgotPasswordBox from './ForgotPassword/ForgotPasswordBox';
import ResetPasswordBox from './ForgotPassword/ResetPasswordBox';
import LoginAfterResetPasswordBox from './LoginAfterResetPasswordBox';
import LoginBox from './LoginBox';
import VerifyAgencyDialog from './VerifyAgencyDialog';

interface IAuthDialogsProps extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  authDialog?: AuthDialog;
  close(): void;
}

const mapStateToProps = (state: AppState) => ({});

interface State {
  phone: string;
}

class AuthDialogs extends React.PureComponent<IAuthDialogsProps, State> {
  state: State = {
    phone: '',
  };

  componentDidMount() {
    const { authDialog, dispatch } = this.props;
    const { phone } = this.state;
    if (authDialog === AuthDialog.forgotPasswordREPASS && !phone) {
      dispatch(setAuthDialog(AuthDialog.forgotPassword));
    }
  }

  render() {
    const { authDialog, close } = this.props;
    const { phone } = this.state;

    return (
      <Dialog open={authDialog !== undefined} maxWidth="md">
        <div
          style={{
            position: 'sticky',
            display: 'flex',
            justifyContent: 'flex-end',

            top: '0px',
            zIndex: 2,
          }}
        >
          <IconButton
            style={{
              marginRight: '8px',
              marginTop: '8px',
            }}
            color="default"
            size="small"
            onClick={close}
          >
            <CloseIcon />
          </IconButton>
        </div>
        {authDialog === AuthDialog.login && <LoginBox />}
        {authDialog === AuthDialog.loginAfterResetPass && (
          <div
            style={{
              background: 'white',
              width: '640px',
              height: '440px',
              boxShadow:
                '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
              paddingTop: '40px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <LoginAfterResetPasswordBox />
          </div>
        )}
        {authDialog === AuthDialog.forgotPassword && (
          <ForgotPasswordBox update={(newPhone, activeStep) => this.setState({ phone: newPhone })} />
        )}
        {authDialog === AuthDialog.forgotPasswordREPASS && <ResetPasswordBox phone={phone} />}
        {authDialog === AuthDialog.verifyAgency && <VerifyAgencyDialog />}
      </Dialog>
    );
  }
}

export default connect(mapStateToProps)(AuthDialogs);
