import { Button, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { GREY, RED, DARK_GREY } from '../../../colors';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as SubmitInprocesSvg } from '../../../svg/ic_signup_submit_inprocess.svg';
import { ReactComponent as WarningSvg } from '../../../svg/ic_signup_warning.svg';
import { closeAuthDialog } from '../redux/authReducer';

const SUBMIT_STATUS = {
  NO_SUBMIT: 'noSubmit',
  SUBMITTED: 'submitted',
  SUBMIT_ERROR: 'submitError',
};

const Box = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 16px 0px;
`;

const NoSubmitBox = (props: { dispatch: Dispatch }) => {
  const { dispatch } = props;
  return (
    <div>
      <div style={{ padding: '16px 0px', display: 'flex', justifyContent: 'center' }}>
        <WarningSvg />
      </div>
      <Typography style={{ textAlign: 'center' }} variant="body2" color="textSecondary">
        <FormattedMessage id="signUp.accountNotVerify" />
      </Typography>

      <Box>
        <Button
          style={{
            height: '40px',
            width: '196px',
            border: `1px solid ${GREY}`,
            marginRight: '16px',
          }}
          size="large"
          onClick={() => dispatch(closeAuthDialog())}
        >
          <Typography color="textPrimary" variant="button">
            <FormattedMessage id="ignore" />
          </Typography>
        </Button>
        <Button
          style={{
            height: '40px',
            width: '196px',
            marginLeft: '16px',
          }}
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => {
            dispatch(closeAuthDialog());
            dispatch(push(ROUTES.verifyAgency));
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="signUp.verifyNow" />
          </Typography>
        </Button>
      </Box>
    </div>
  );
};

const SubmitErrorBox = (props: { dispatch: Dispatch; message: string }) => {
  const { dispatch, message } = props;
  return (
    <div>
      <Typography variant="h5">
        <FormattedMessage id="signUp.submitRequestInValid" />
      </Typography>
      <div style={{ padding: '16px 0px', display: 'flex', justifyContent: 'center' }}>
        <WarningSvg />
      </div>
      <Typography variant="body2" color="textSecondary">
        <FormattedMessage id="signUp.pleaseCheckInfoBelow" />
      </Typography>

      <Typography variant="body2" style={{ color: RED, paddingLeft: '8px' }}>
        <span style={{ whiteSpace: 'pre-wrap' }} dangerouslySetInnerHTML={{ __html: message }} />
      </Typography>

      <Typography variant="body2" color="textSecondary" style={{ paddingTop: '32px' }}>
        <FormattedMessage id="signUp.updateToContinue" />
      </Typography>
      <Box>
        <Button
          style={{
            height: '40px',
            width: '196px',
            border: `1px solid ${GREY}`,
            marginRight: '16px',
          }}
          size="large"
          onClick={() => dispatch(closeAuthDialog())}
        >
          <Typography color="textPrimary" variant="button">
            <FormattedMessage id="ignore" />
          </Typography>
        </Button>
        <Button
          style={{
            height: '40px',
            width: '196px',
            marginLeft: '16px',
          }}
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => {
            dispatch(closeAuthDialog());
            dispatch(push(ROUTES.verifyAgency));
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="signUp.updateInformation" />
          </Typography>
        </Button>
      </Box>
    </div>
  );
};

const SubmitInProcessBox = (props: { dispatch: Dispatch }) => {
  const { dispatch } = props;
  return (
    <div>
      <Typography variant="h5">
        <FormattedMessage id="signUp.submitInProcess" />
      </Typography>
      <div style={{ padding: '16px 0px', display: 'flex', justifyContent: 'center' }}>
        <SubmitInprocesSvg />
      </div>
      <Typography style={{ textAlign: 'center' }} variant="body2" color="textSecondary">
        <FormattedMessage id="signUp.weSentResultViaEmail" />
      </Typography>

      <Box>
        <Button
          style={{
            height: '40px',
            width: '196px',
            border: `1px solid ${GREY}`,
            marginRight: '16px',
          }}
          size="large"
          onClick={() => {
            dispatch(closeAuthDialog());
            dispatch(push(ROUTES.verifyAgency));
          }}
        >
          <Typography style={{ color: DARK_GREY }} variant="button">
            <FormattedMessage id="signUp.updateInformation" />
          </Typography>
        </Button>
        <Button
          style={{
            height: '40px',
            width: '196px',
            marginLeft: '16px',
          }}
          variant="contained"
          color="primary"
          size="large"
          onClick={() => dispatch(closeAuthDialog())}
        >
          <Typography variant="button" style={{ color: 'white' }}>
            <FormattedMessage id="ok" />
          </Typography>
        </Button>
      </Box>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    userData: state.account.userData,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

interface State {
  message: string;
  submitStatus: string;
}

class VerifyAgencyDialog extends React.PureComponent<Props, State> {
  state: State = {
    message: '',
    submitStatus: SUBMIT_STATUS.NO_SUBMIT,
  };

  componentDidMount() {
    const { userData } = this.props;

    if (userData) {
      if (userData.bookerRequest && userData.bookerRequest.message) {
        this.setState({
          submitStatus: SUBMIT_STATUS.SUBMIT_ERROR,
          message: userData.bookerRequest.message,
        });
      }
      if (userData.bookerRequest && !userData.bookerRequest.message) {
        this.setState({ submitStatus: SUBMIT_STATUS.SUBMITTED });
      }
    }
  }

  render() {
    const { dispatch } = this.props;
    const { message, submitStatus } = this.state;

    return (
      <div style={{ padding: '0px 24px', width: '640px' }}>
        {submitStatus === SUBMIT_STATUS.NO_SUBMIT && <NoSubmitBox dispatch={dispatch} />}
        {submitStatus === SUBMIT_STATUS.SUBMIT_ERROR && (
          <SubmitErrorBox dispatch={dispatch} message={message} />
        )}
        {submitStatus === SUBMIT_STATUS.SUBMITTED && <SubmitInProcessBox dispatch={dispatch} />}
      </div>
    );
  }
}

export default connect(mapStateToProps)(VerifyAgencyDialog);
