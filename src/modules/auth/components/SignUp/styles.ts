import styled from 'styled-components';

export const Box = styled.div`
  background-color: #fff;
  width: 640px;
  display: flex;
  flex-direction: column;
  padding: 40px 24px 0;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
`;
