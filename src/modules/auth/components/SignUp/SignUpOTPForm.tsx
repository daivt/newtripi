import { FormHelperText } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { Link } from 'react-router-dom';
import { BLUE, DARK_GREY, PRIMARY, SECONDARY } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { FreeTextField } from '../../../booking/components/Form';
import { subtitle3Styles } from '../../../common/components/elements';
import LoadingButton from '../../../common/components/LoadingButton';
import { SignUpInformation, SignUpInformationValidation } from '../../../common/models/index';
import { validTelephoneRegex } from '../../../common/utils';
import { Box } from './styles';

export interface ISignUpOTPFormProps extends WrappedComponentProps {
  signUpInfo: SignUpInformation;
  validation: SignUpInformationValidation;
  loading: boolean;
  update(signUpInfo: SignUpInformation, validation?: SignUpInformationValidation): void;
  sendSignUpOTP(): void;
}

export default injectIntl(
  class SignUpOTPForm extends React.PureComponent<ISignUpOTPFormProps, { ticktock: boolean }> {
    state = { ticktock: false };

    public render() {
      const { intl, signUpInfo, update, sendSignUpOTP, validation, loading } = this.props;
      return (
        <Box style={{ marginBottom: '30px' }}>
          <Typography variant="h5" style={{ fontWeight: 'normal' }}>
            <FormattedMessage id="signUp.register" />
          </Typography>
          <Typography variant="body2" style={{ color: DARK_GREY, paddingTop: '8px' }}>
            <FormattedMessage
              id="signUp.verifyPhoneNumber"
              values={{
                text: (
                  <>
                    <span style={{ fontWeight: 'bold', color: PRIMARY }}>
                      <FormattedMessage id="signUp.tripi" />
                      &nbsp;
                    </span>
                    <span style={{ fontWeight: 'bold', color: SECONDARY }}>
                      <FormattedMessage id="signUp.partner" />
                    </span>
                  </>
                ),
              }}
            />
          </Typography>
          <div style={{ paddingTop: '8px', display: 'flex' }}>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              <FormattedMessage id="signUp.seeMoreInfo" />
              &nbsp;
            </Typography>
            <a
              href="https://partner.tripi.vn"
              target="_blank"
              rel="noopener noreferrer"
              style={{
                textDecoration: 'inherit',
                color: 'inherit',
              }}
            >
              <Typography variant="body2" style={{ color: BLUE, cursor: 'pointer' }}>
                <FormattedMessage id="signUp.here" />
              </Typography>
            </a>
          </div>

          <Typography variant="body2" style={{ color: DARK_GREY, paddingTop: '8px' }}>
            <FormattedMessage id="signUp.pleaseEnterYourPhoneNumber" />
          </Typography>

          <div style={{ paddingTop: '8px' }}>
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <div style={{ width: '316px' }}>
                <FreeTextField
                  key={`telephone${this.state.ticktock}`}
                  text={signUpInfo.phone}
                  valid={validation.phone}
                  header={intl.formatMessage({ id: 'telephone' })}
                  placeholder={intl.formatMessage({ id: 'signUp.phoneExample' })}
                  update={value => update({ ...signUpInfo, phone: value }, { ...validation, phone: true })}
                  regex={validTelephoneRegex}
                />
              </div>
              <div style={{ paddingLeft: '18px' }}>
                <LoadingButton
                  variant="contained"
                  color="secondary"
                  size="large"
                  style={{ padding: '10px 24px' }}
                  loading={loading}
                  onClick={() => {
                    sendSignUpOTP();
                    this.setState(state => ({ ticktock: !state.ticktock }));
                  }}
                >
                  <Typography variant="button">
                    <FormattedMessage id="signUp.sendOTP" />
                  </Typography>
                </LoadingButton>
              </div>
            </div>
            <Typography variant="caption" style={{ color: DARK_GREY, paddingLeft: '12px' }}>
              <FormattedMessage id="signUp.usePhoneNumberToLogin" />
            </Typography>
            <FormHelperText error>{!validation.phone && <FormattedMessage id="auth.invalidTelRegex" />}</FormHelperText>
          </div>

          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingBottom: '32px',
              paddingTop: '16px',
            }}
          >
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              <span style={subtitle3Styles}>
                <FormattedMessage id="signUp.alreadyAccount" />
                &nbsp;
                <Link to={ROUTES.login} style={{ color: BLUE }}>
                  <FormattedMessage id="signIn" />
                </Link>
              </span>
            </Typography>
          </div>
        </Box>
      );
    }
  },
);
