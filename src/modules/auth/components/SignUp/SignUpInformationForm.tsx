import { Button, FormHelperText } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { BLACK_TEXT, DARK_GREY, PRIMARY, SECONDARY } from '../../../../colors';
import { FreeTextField } from '../../../booking/components/Form';
import LoadingButton from '../../../common/components/LoadingButton/index';
import { SignUpInformation, SignUpInformationValidation } from '../../../common/models';
import { MAX_OTP_SECONDS } from '../../constants';
import { SecondsDown } from '../SecondsDown';
import { Box } from './styles';

interface Props extends WrappedComponentProps {
  loading: boolean;
  counting: boolean;
  signUpInfo: SignUpInformation;
  validation: SignUpInformationValidation;
  update(signUpInfo: SignUpInformation, validation?: SignUpInformationValidation): void;
  signUp(): void;
  resendOTP(): void;
  onCountingCompleted(): void;
}

const SignUpInformationForm: React.FunctionComponent<Props> = props => {
  const { intl, signUpInfo, update, signUp, validation, loading, resendOTP, counting, onCountingCompleted } = props;
  const [ticktock, setTick] = React.useState<boolean>(false);

  return (
    <div>
      <Box>
        <Typography variant="h5" style={{ fontWeight: 'normal' }}>
          <FormattedMessage id="signUp.enterInfo" />
        </Typography>

        <div style={{ paddingTop: '28px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FreeTextField
              key={`name${ticktock}`}
              text={signUpInfo.name}
              valid={validation.name}
              header={intl.formatMessage({ id: 'signUp.fullName' })}
              placeholder={intl.formatMessage({ id: 'signUp.fullNameExample' })}
              update={value => update({ ...signUpInfo, name: value }, { ...validation, name: true })}
            />
          </div>
        </div>
        <div style={{ paddingTop: '28px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FreeTextField
              key={`email${ticktock}`}
              text={signUpInfo.email}
              valid={validation.email}
              header={intl.formatMessage({ id: 'signUp.emailAddress' })}
              placeholder={intl.formatMessage({ id: 'signUp.emailExample' })}
              update={value => update({ ...signUpInfo, email: value }, { ...validation, email: true })}
            />
          </div>
        </div>
        <div style={{ paddingTop: '28px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FreeTextField
              key={`password${ticktock}`}
              text={signUpInfo.password}
              valid={validation.password}
              header={intl.formatMessage({ id: 'signUp.password' })}
              placeholder="********"
              update={value => update({ ...signUpInfo, password: value }, { ...validation, password: true })}
              type="password"
            />
          </div>
          <FormHelperText error>
            {!validation.password && <FormattedMessage id="auth.passwordLengthError" />}
          </FormHelperText>
        </div>
        <div style={{ paddingTop: '28px', paddingBottom: '36px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FreeTextField
              key={`rePassword${ticktock}`}
              text={signUpInfo.rePassword}
              valid={validation.rePassword}
              header={intl.formatMessage({ id: 'signUp.rePassword' })}
              placeholder="********"
              update={value => update({ ...signUpInfo, rePassword: value }, { ...validation, rePassword: true })}
              type="password"
            />
          </div>
          {!validation.rePassword && (
            <Typography variant="caption" color="error">
              <FormattedMessage id="signUp.confirmPasswordNotCorrect" />
            </Typography>
          )}
        </div>

        <div>
          <Typography variant="h5" style={{ fontWeight: 'normal' }}>
            <FormattedMessage id="signUp.enterOTP" />
          </Typography>
          <Typography variant="body2" style={{ color: DARK_GREY, paddingTop: '8px' }}>
            <FormattedMessage
              id="signUp.verifyPhoneNumber"
              values={{
                text: (
                  <>
                    <span style={{ color: PRIMARY }}>
                      <FormattedMessage id="signUp.tripi" />
                      &nbsp;
                    </span>
                    <span style={{ color: SECONDARY }}>
                      <FormattedMessage id="signUp.partner" />
                    </span>
                  </>
                ),
              }}
            />
          </Typography>
          <Typography variant="body2" style={{ color: DARK_GREY, paddingTop: '8px' }}>
            <FormattedMessage
              id="signUp.weSentOTPCodeToYourPhoneNumber"
              values={{
                phone: <span style={{ fontWeight: 'bold', color: BLACK_TEXT }}>{signUpInfo.phone}</span>,
              }}
            />
          </Typography>
          <div style={{ paddingTop: '16px' }}>
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <div style={{ width: '316px' }}>
                <FreeTextField
                  key={`otp${ticktock}`}
                  text={signUpInfo.otp}
                  valid={validation.otp}
                  header={intl.formatMessage({ id: 'signUp.otp' })}
                  placeholder={intl.formatMessage({ id: 'signUp.otpExample' })}
                  update={value => update({ ...signUpInfo, otp: value }, { ...validation, otp: true })}
                />
              </div>
              <div style={{ paddingLeft: '18px' }}>
                <Button
                  disabled={counting}
                  style={{ padding: '10px', width: '150px', borderRadius: '4px' }}
                  onClick={() => resendOTP()}
                >
                  <Typography variant="subtitle2" color="textSecondary">
                    <FormattedMessage id="signUp.resendOTP" />
                  </Typography>
                </Button>
              </div>
            </div>
            <div style={{ padding: '10px 2px' }}>
              {counting && <SecondsDown num={MAX_OTP_SECONDS} onCountingCompleted={onCountingCompleted} />}
            </div>
          </div>
        </div>
      </Box>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          marginBottom: '32px',
        }}
      >
        <Typography variant="body2" style={{ padding: '16px 0' }}>
          <FormattedMessage
            id="signUp.verifyAccount"
            values={{
              text: (
                <>
                  <span style={{ color: PRIMARY }}>
                    <FormattedMessage id="signUp.tripi" />
                    &nbsp;
                  </span>
                  <span style={{ color: SECONDARY }}>
                    <FormattedMessage id="signUp.partner" />
                  </span>
                </>
              ),
            }}
          />
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <LoadingButton
            variant="contained"
            color="secondary"
            size="large"
            onClick={() => {
              signUp();
              setTick(!ticktock);
            }}
            loading={loading}
          >
            <Typography variant="button">
              <FormattedMessage id="signUp.verifyInfoPartner" />
            </Typography>
          </LoadingButton>
        </div>
      </div>
    </div>
  );
};

export default injectIntl(SignUpInformationForm);
