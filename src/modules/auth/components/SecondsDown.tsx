import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { DARK_GREY } from '../../../colors';

interface Props {
  num: number;
  onCountingCompleted(): void;
  format?: (seconds: number) => React.ReactNode;
}

export const SecondsDown = (props: Props) => {
  const { onCountingCompleted, format } = props;
  const [seconds, setSeconds] = useState(props.num);
  useEffect(() => {
    let id = 0;
    if (seconds > 0) {
      id = setTimeout(() => setSeconds(seconds - 1), 1000);
    } else {
      onCountingCompleted();
    }
    return () => clearTimeout(id);
  }, [seconds, onCountingCompleted]);

  return (
    <>
      {!format ? (
        <div>
          <Typography variant="caption" style={{ color: DARK_GREY, paddingLeft: '12px' }}>
            {seconds}
            &nbsp;~&nbsp;
            <FormattedMessage id="seconds" />
          </Typography>
        </div>
      ) : (
        format(seconds)
      )}
    </>
  );
};
