import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { AppState } from '../../../redux/reducers';
import AuthHeader from '../components/AuthHeader';
import ForgotPasswordBox from '../components/ForgotPassword/ForgotPasswordBox';
import ResetPasswordBox from '../components/ForgotPassword/ResetPasswordBox';
import Steppers from '../components/Steppers';
import { FORGOT_PASSWORD_STEP, getForgotPasswordSteps } from '../utils';
import Footer from '../../common/components/Footer';
import Container from '@material-ui/core/Container';

function mapState2Props(state: AppState) {
  return {};
}

interface Props extends ReturnType<typeof mapState2Props> {}

interface State {
  activeStep: number;
  phone: string;
  otp: string;
}

class ForgotPassword extends PureComponent<Props, State> {
  state: State = {
    activeStep: FORGOT_PASSWORD_STEP.SEND_OTP,
    phone: '',
    otp: '',
  };

  render() {
    const { activeStep, phone } = this.state;

    return (
      <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <AuthHeader />
        <Steppers activeStep={activeStep} getSteps={getForgotPasswordSteps} />
        <Container
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyItems: 'center',
            alignItems: 'center',
            flex: 1,
          }}
        >
          {activeStep === FORGOT_PASSWORD_STEP.SEND_OTP && (
            <ForgotPasswordBox
              usePage
              update={(phone, activeStep) => this.setState({ phone, activeStep })}
            />
          )}

          {activeStep === FORGOT_PASSWORD_STEP.RESET_PASSWORD && (
            <ResetPasswordBox usePage phone={phone} />
          )}
        </Container>
        <Footer />
      </div>
    );
  }
}

export default connect(mapState2Props)(ForgotPassword);
