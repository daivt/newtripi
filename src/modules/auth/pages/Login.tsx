import React from 'react';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import Footer from '../../common/components/Footer';
import LoadingIcon from '../../common/components/LoadingIcon';
import AuthHeader from '../components/AuthHeader';
import LoginBox from '../components/LoginBox';
import { validateAccessToken } from '../redux/authThunks';
const mapStateToProps = (state: AppState) => ({ validatingToken: state.auth.validatingToken });
interface LoginProps extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface LoginState {}

class Login extends React.PureComponent<LoginProps, LoginState> {
  constructor(props: LoginProps) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.props.dispatch(validateAccessToken());
  }

  render() {
    const { validatingToken } = this.props;
    return (
      <div>
        <AuthHeader />
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '32px',
            marginBottom: '164px',
          }}
        >
          <div
            style={{
              background: 'white',
              width: '640px',
              height: '440px',
              boxShadow:
                '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
              paddingTop: '40px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {validatingToken ? <LoadingIcon /> : <LoginBox onPage />}
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default connect(mapStateToProps)(Login);
