import React from 'react';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import Footer from '../../common/components/Footer';
import AuthHeader from '../components/AuthHeader';
import { validateAccessToken } from '../redux/authThunks';
import LoginAfterResetPasswordBox from '../components/LoginAfterResetPasswordBox';

const mapStateToProps = (state: AppState) => ({ authenticating: state.auth.authenticating });

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class LoginAfterResetPassword extends React.PureComponent<Props, State> {
  componentDidMount() {
    this.props.dispatch(validateAccessToken());
  }

  render() {
    return (
      <div>
        <AuthHeader />
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '32px',
            marginBottom: '164px',
          }}
        >
          <div
            style={{
              background: 'white',
              width: '640px',
              height: '440px',
              boxShadow:
                '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
              paddingTop: '40px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <LoginAfterResetPasswordBox onPage />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default connect(mapStateToProps)(LoginAfterResetPassword);
