import { Button, Typography } from '@material-ui/core';
import { goBack, push, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { DARK_GREY, PRIMARY, SECONDARY, BLUE } from '../../../colors';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconAppStore } from '../../../svg/ic_app_store.svg';
import { ReactComponent as IconGooglePlay } from '../../../svg/ic_google_play.svg';
import LoadingButton from '../../common/components/LoadingButton/index';
import MessageDialog from '../../common/components/MessageDialog';
import { AgencyInformation, AgencyInformationValidation } from '../../common/models';
import { validAgencyInformationValidation } from '../../common/models/index';
import AgencyInformationBox from '../components/AgencyInformationBox';
import AuthHeader from '../components/AuthHeader';
import Steppers from '../components/Steppers';
import { verifyAgencyInformation } from '../redux/authThunks';
import { SIGN_UP_STEP, validAgency, validateAgencyInfo, getSignUpSteps } from '../utils';
import { PHOTO_TYPE } from '../constants';
import { NewTabLink } from '../../common/components/NewTabLink';

export const Box = styled.div`
  background-color: #fff;
  width: 640px;
  display: flex;
  flex-direction: column;
  padding: 40px 24px 0;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
`;

const mapState2Props = (state: AppState) => {
  return {
    userData: state.account.userData,
  };
};

export interface IVerifyAgencyProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  agencyInfo: AgencyInformation;
  agencyInfoValidation: AgencyInformationValidation;
  errorMessage?: string;
  showDialog?: boolean;
  isVerified?: boolean;
  loading: boolean;
  ticktock: boolean;
}

class VerifyAgency extends React.Component<IVerifyAgencyProps, State> {
  state: State = {
    agencyInfo: {
      bankCode:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.bankCode
          : '',
      businessProfiles:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.businessProfiles
          : [],
      identityCard:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.identityCard
          : '',
      frontIdentityCard:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.frontIdentityCard
          : '',
      accountName:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.accountName
          : '',
      accountNumber:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.accountNumber
          : '',
      backIdentityCard:
        this.props.userData && this.props.userData.bookerRequest
          ? this.props.userData.bookerRequest.detail.backIdentityCard
          : '',
    },
    agencyInfoValidation: validAgencyInformationValidation,
    loading: false,
    ticktock: false,
  };

  componentDidMount() {
    const { userData, dispatch } = this.props;
    if (userData && userData.moduleBooker) {
      dispatch(replace('/'));
    }
  }

  async verifyAgencyInformation() {
    const { agencyInfo } = this.state;
    const agencyInfoValidation = validateAgencyInfo(agencyInfo);
    this.setState({
      agencyInfoValidation,
      loading: true,
    });

    if (!validAgency(agencyInfoValidation)) {
      this.setState({ loading: false });
      return;
    }

    const json = await this.props.dispatch(verifyAgencyInformation(agencyInfo));

    if (json.code === 200) {
      this.setState({ isVerified: true, loading: false });
    } else {
      this.setState({ errorMessage: json.message, showDialog: true, loading: false });
    }
  }

  updatePhoto(imageUrl: string, photoType: string) {
    const { agencyInfo } = this.state;

    if (photoType === PHOTO_TYPE.FRONT_IDENTITY_CARD) {
      this.setState({ agencyInfo: { ...agencyInfo, frontIdentityCard: imageUrl } });
    } else if (photoType === PHOTO_TYPE.BACK_IDENTITY_CARD) {
      this.setState({ agencyInfo: { ...agencyInfo, backIdentityCard: imageUrl } });
    } else {
      this.setState({
        agencyInfo: { ...agencyInfo, businessProfiles: [...agencyInfo.businessProfiles, imageUrl] },
      });
    }
  }

  render() {
    const { dispatch } = this.props;
    const {
      agencyInfo,
      agencyInfoValidation,
      errorMessage,
      showDialog,
      isVerified,
      loading,
      ticktock,
    } = this.state;
    return (
      <div>
        <AuthHeader />
        {!isVerified ? (
          <>
            <Steppers activeStep={SIGN_UP_STEP.VERIFY_PARTNER} getSteps={getSignUpSteps} />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyItems: 'center',
                alignItems: 'center',
              }}
            >
              <AgencyInformationBox
                ticktock={ticktock}
                agencyInfo={agencyInfo}
                agencyInfoValidate={agencyInfoValidation}
                update={(agencyInfo, agencyInfoValidation) => {
                  this.setState({ agencyInfo });
                  if (!!agencyInfoValidation) {
                    this.setState({ agencyInfoValidation });
                  }
                }}
                updatePhoto={(imageUrl: string, type: string) => this.updatePhoto(imageUrl, type)}
              />

              <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '32px' }}>
                <div>
                  <Button
                    style={{ width: '196px' }}
                    variant="outlined"
                    color="secondary"
                    size="large"
                    onClick={() => dispatch(goBack())}
                  >
                    <Typography variant="button">
                      <FormattedMessage id="back" />
                    </Typography>
                  </Button>
                </div>
                <div style={{ paddingLeft: '32px' }}>
                  <LoadingButton
                    style={{ width: '196px' }}
                    variant="contained"
                    color="secondary"
                    size="large"
                    onClick={() => {
                      this.verifyAgencyInformation();
                      this.setState({ ticktock: !ticktock });
                    }}
                    loading={loading}
                  >
                    <Typography variant="button">
                      <FormattedMessage id="continue" />
                    </Typography>
                  </LoadingButton>
                </div>
              </div>
            </div>
          </>
        ) : (
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyItems: 'center',
              alignItems: 'center',
              paddingTop: '32px',
            }}
          >
            <Box>
              <Typography variant="h5">
                <FormattedMessage id="signUp.verifyInProcess" />
              </Typography>
              <Typography variant="body1" style={{ color: DARK_GREY, padding: '8px 0' }}>
                <FormattedMessage
                  id="signUp.canAccessAfterVerifyAccount"
                  values={{
                    text: (
                      <>
                        <span style={{ fontWeight: 'bold', color: PRIMARY }}>
                          <FormattedMessage id="signUp.tripi" />
                          &nbsp;
                        </span>
                        <span style={{ fontWeight: 'bold', color: SECONDARY }}>
                          <FormattedMessage id="signUp.partner" />
                        </span>
                      </>
                    ),
                  }}
                />
              </Typography>
              <Typography variant="body1" style={{ color: DARK_GREY }}>
                <FormattedMessage id="signUp.youCanDownloadApp" />
              </Typography>
              <div
                style={{
                  padding: '24px 54px',
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <NewTabLink href="https://play.google.com/store/apps/details?id=com.tripi.booker">
                  <IconGooglePlay />
                </NewTabLink>
                <NewTabLink href="https://apps.apple.com/us/app/tripi-partner-b2b-travel-deals/id1356994818">
                  <IconAppStore />
                </NewTabLink>
              </div>
              <Typography variant="body1" style={{ color: DARK_GREY }}>
                <FormattedMessage
                  id="signUp.viewInfoTripiPartner"
                  values={{
                    textLink: (
                      <a
                        style={{ color: BLUE, textDecoration: 'none' }}
                        href="https://partner.tripi.vn/"
                      >
                        <FormattedMessage id="signUp.clickHere" />
                      </a>
                    ),
                  }}
                />
              </Typography>

              <div style={{ display: 'flex', justifyContent: 'center', margin: '32px 0' }}>
                <Button
                  style={{ width: '135px' }}
                  variant="contained"
                  color="primary"
                  size="large"
                  onClick={() => dispatch(push('/'))}
                >
                  <Typography variant="button">
                    <FormattedMessage id="ok" />
                  </Typography>
                </Button>
              </div>
            </Box>
          </div>
        )}
        <MessageDialog
          show={!!showDialog}
          onClose={() => this.setState({ showDialog: false })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(VerifyAgency);
