import { Container, Typography } from '@material-ui/core';
import { push, replace } from 'connected-react-router';
import * as React from 'react';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import Footer from '../../common/components/Footer';
import MessageDialog from '../../common/components/MessageDialog';
import { SignUpInformation, validSignUpInformationValidation } from '../../common/models';
import { SignUpInformationValidation } from '../../common/models/index';
import AuthHeader from '../components/AuthHeader';
import SignUpInformationForm from '../components/SignUp/SignUpInformationForm';
import SignUpOTPForm from '../components/SignUp/SignUpOTPForm';
import Steppers from '../components/Steppers';
import { getSignUpOPT, signUp } from '../redux/authThunks';
import {
  getSignUpSteps,
  SIGN_UP_STEP,
  valid,
  validateSignUpInfo,
  validateSignUpPhoneInfo,
} from '../utils';
import { closeAuthDialog } from '../redux/authReducer';

const mapState2Props = (state: AppState) => {
  return {
    userData: state.account.userData,
    auth: state.auth.auth,
  };
};

export interface ISignUpProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  activeStep: number;
  errorMessage?: string;
  showDialog?: boolean;
  info: SignUpInformation;
  infoValidation: SignUpInformationValidation;
  loading: boolean;
  counting: boolean;
}

class SignUp extends React.Component<ISignUpProps, State> {
  state: State = {
    activeStep: SIGN_UP_STEP.SEND_OTP,
    info: {
      phone: '',
      otp: '',
      name: '',
      email: '',
      password: '',
      rePassword: '',
    },
    infoValidation: validSignUpInformationValidation,
    loading: false,
    counting: true,
  };

  componentDidMount() {
    const { dispatch, auth } = this.props;

    dispatch(closeAuthDialog());
    if (auth) {
      dispatch(replace('/'));
    }
  }

  sendSignUpOTP = async () => {
    const { info, infoValidation } = this.state;
    const validate = validateSignUpPhoneInfo(info, infoValidation);
    this.setState({ infoValidation: validate, loading: true });

    if (!valid(validate)) {
      this.setState({ loading: false });
      return;
    }

    const json = await this.props.dispatch(getSignUpOPT(info.phone));
    this.setState({ loading: false });
    if (json.code === 200) {
      this.setState({ activeStep: SIGN_UP_STEP.ENTER_INFORMATION });
    } else {
      this.setState({ errorMessage: json.message, showDialog: true });
    }
  };

  resendOTP = async () => {
    const { info } = this.state;
    const json = await this.props.dispatch(getSignUpOPT(info.phone));
    if (json.code === 200) {
      this.setState({ errorMessage: json.message, showDialog: true, counting: true });
    }
  };

  stopCounting = () => {
    this.setState({ counting: false });
  };

  signUp = async () => {
    const { dispatch } = this.props;
    const { info } = this.state;
    const infoValidation = validateSignUpInfo(info);
    this.setState({ infoValidation, loading: true });

    if (!valid(infoValidation)) {
      this.setState({ loading: false });
      return;
    }

    const json = await this.props.dispatch(signUp(info));

    if (json.code === 200) {
      this.setState({ loading: false });
      dispatch(push(ROUTES.verifyAgency));
      return;
    }

    this.setState({ errorMessage: json.message, showDialog: true, loading: false });
  };

  render() {
    const {
      activeStep,
      info,
      errorMessage,
      showDialog,
      infoValidation,
      loading,
      counting,
    } = this.state;
    return (
      <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <AuthHeader />
        <Steppers activeStep={activeStep} getSteps={getSignUpSteps} />
        <Container
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyItems: 'center',
            alignItems: 'center',
            flex: 1,
          }}
        >
          {activeStep === SIGN_UP_STEP.SEND_OTP && (
            <SignUpOTPForm
              signUpInfo={info}
              validation={infoValidation}
              update={(info, validation) => {
                this.setState({ info });
                if (!!validation) {
                  this.setState({ infoValidation: validation });
                }
              }}
              sendSignUpOTP={this.sendSignUpOTP}
              loading={loading}
            />
          )}

          {activeStep === SIGN_UP_STEP.ENTER_INFORMATION && (
            <SignUpInformationForm
              loading={loading}
              counting={counting}
              signUpInfo={info}
              validation={infoValidation}
              update={(info, validation) => {
                this.setState({ info });
                if (!!validation) {
                  this.setState({ infoValidation: validation });
                }
              }}
              signUp={this.signUp}
              resendOTP={this.resendOTP}
              onCountingCompleted={this.stopCounting}
            />
          )}
        </Container>
        <MessageDialog
          show={!!showDialog}
          onClose={() => this.setState({ showDialog: false })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
        <Footer />
      </div>
    );
  }
}

export default connect(mapState2Props)(SignUp);
