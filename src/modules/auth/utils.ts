import { some } from '../../constants';
import {
  AgencyInformation,
  AgencyInformationValidation,
  SignUpInformation,
  SignUpInformationValidation,
} from '../common/models';
import { validEmailRegex, validIdRegex, validVietnamTelephoneRegex } from '../common/utils';

export const SIGN_UP_STEP = {
  SEND_OTP: 0,
  ENTER_INFORMATION: 1,
  VERIFY_PARTNER: 2,
};

export const FORGOT_PASSWORD_STEP = {
  SEND_OTP: 0,
  RESET_PASSWORD: 1,
};

export function validateSignUpPhoneInfo(
  info: SignUpInformation,
  validation: SignUpInformationValidation,
): SignUpInformationValidation {
  return {
    ...validation,
    phone: validVietnamTelephoneRegex.test(info.phone),
  };
}

export function validateSignUpOTPInfo(
  info: SignUpInformation,
  validation: SignUpInformationValidation,
): SignUpInformationValidation {
  return {
    ...validation,
    otp: info.otp.length > 4,
  };
}

export function validateSignUpInfo(info: SignUpInformation): SignUpInformationValidation {
  return {
    phone: true,
    otp: info.otp.length > 4,
    name: info.name ? !!info.name.trim() : false,
    email: validEmailRegex.test(info.email),
    password: info.password.length >= 6,
    rePassword: info.rePassword.length >= 6 && info.password === info.rePassword,
  };
}

export function valid(validation: SignUpInformationValidation) {
  return (
    validation.phone &&
    validation.otp &&
    validation.email &&
    validation.name &&
    validation.password &&
    validation.rePassword
  );
}

export function validateAgencyInfo(agencyInfo: AgencyInformation): AgencyInformationValidation {
  return {
    businessProfiles: true,
    frontIdentityCard: true,
    backIdentityCard: true,
    identityCard: validIdRegex.test(agencyInfo.identityCard),
    bankCode: agencyInfo.bankCode ? !!agencyInfo.bankCode.trim() : false,
    accountName: agencyInfo.accountName ? !!agencyInfo.accountName.trim() : false,
    accountNumber: agencyInfo.accountNumber ? validIdRegex.test(agencyInfo.accountNumber) : false,
  };
}

export function validAgency(validation: AgencyInformationValidation) {
  return (
    validation.businessProfiles &&
    validation.frontIdentityCard &&
    validation.backIdentityCard &&
    validation.identityCard &&
    validation.bankCode &&
    validation.accountNumber &&
    validation.accountName
  );
}

export function getSignUpSteps() {
  return ['telephone', 'signUp.enterInfo', 'signUp.verifyInfoPartner'];
}

export function getForgotPasswordSteps() {
  return ['auth.forgotPassword', 'auth.changePassword'];
}

export function shouldDisplayVerifyAgencyDialog(userData: some) {
  if (!userData.bookerRequest && userData.moduleBooker) {
    return false;
  }
  return true;
}
