import { ActionType, createAction, getType } from 'typesafe-actions';
import { DEVICE_RID_KEY, some } from '../../../constants';
import { setUserData } from '../../account/redux/accountReducer';

export enum AuthDialog {
  login,
  loginAfterResetPass,
  forgotPassword,
  forgotPasswordOTP,
  forgotPasswordEMAIL,
  forgotPasswordREPASS,
  signUp,
  verifyAgency,
}
export interface AuthState {
  readonly auth: boolean;
  readonly authenticating: boolean;
  readonly validatingToken: boolean;
  readonly loginErrorMsg?: string;
  readonly authDialog?: AuthDialog;
  readonly userData?: Readonly<some>;
  readonly fingerPrint: string;
  readonly reSendOTP?: number;
}

export const inAction = createAction('auth/in', resolve => (skipSaga: boolean) => resolve({ skipSaga }));
export const out = createAction('auth/out');

export const setAuthenticating = createAction('auth/setAuthenticating', resolve => (val: boolean) => resolve({ val }));

export const setValidatingToken = createAction('auth/setValidatingToken', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setAuthDialog = createAction('auth/setAuthDialog', resolve => (val: AuthDialog, dest?: string) =>
  resolve({ val, dest }),
);

export const closeAuthDialog = createAction('auth/closeAuthDialog');

export const setResendOTP = createAction('auth/setResendOTP', resolve => (val?: number) => resolve({ val }));
export const setLoginErrorMsg = createAction('auth/setLoginErrorMsg', resolve => (val?: string) => resolve({ val }));

const actions = {
  in_: inAction,
  out,
  setAuthenticating,
  setValidatingToken,
  setAuthDialog,
  setUserData,
  closeAuthDialog,
  setLoginErrorMsg,
  setResendOTP,
};

type ActionT = ActionType<typeof actions>;

export const defaultSignUpState = {
  email: '',
  password: '',
  repassword: '',
  telephone: '',
  username: '',
};

const key = `${DEVICE_RID_KEY}_v2019-11-08`;
let deviceRid = `${new Date().valueOf()}-${Math.random()}`;
const value = localStorage.getItem(key);
if (value === null) {
  localStorage.setItem(key, deviceRid);
} else {
  deviceRid = value;
}

export default function reducer(
  state: AuthState = {
    authenticating: false,
    validatingToken: false,
    auth: false,
    fingerPrint: deviceRid,
  },
  action: ActionT,
): AuthState {
  switch (action.type) {
    case getType(inAction):
      return { ...state, auth: true };
    case getType(out):
      return { ...state, auth: false };
    case getType(setLoginErrorMsg):
      return { ...state, loginErrorMsg: action.payload.val };
    case getType(setAuthenticating):
      return { ...state, authenticating: action.payload.val };
    case getType(setValidatingToken):
      return { ...state, validatingToken: action.payload.val };
    case getType(setAuthDialog):
      return { ...state, authDialog: action.payload.val };
    case getType(closeAuthDialog):
      return { ...state, authDialog: undefined };
    case getType(setResendOTP):
      return { ...state, reSendOTP: action.payload.val };
    default:
      return state;
  }
}
