import { put, select } from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';
import { DINOGO, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { shouldDisplayVerifyAgencyDialog } from '../utils';
import { AuthDialog, closeAuthDialog, inAction, setAuthDialog } from './authReducer';

export function* loggedInFollowUp(action: ActionType<typeof inAction>) {
  const state: AppState = yield select();
  const { userData } = state.account;
  if (userData && !action.payload.skipSaga) {
    if (
      !DINOGO &&
      shouldDisplayVerifyAgencyDialog(userData) &&
      [ROUTES.verifyAgency, ROUTES.inviteMember].indexOf(state.router.location.pathname) === -1
    ) {
      yield put(setAuthDialog(AuthDialog.verifyAgency));
    } else {
      yield put(closeAuthDialog());
    }
  }
}
