import { push, replace } from 'connected-react-router';
import { get, remove, set } from 'js-cookie';
import moment from 'moment';
import { batch } from 'react-redux';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ROUTES, some } from '../../../constants';
import { AppState, clearStoreAfterLogout } from '../../../redux/reducers';
import { loadParamsToStore } from '../../../utils';
import { fetchBookerInfo, setUserData } from '../../account/redux/accountReducer';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { AgencyInformation } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { ACCESS_TOKEN, NewDeviceLoginError } from '../constants';
import { inAction, out, setAuthenticating, setLoginErrorMsg, setResendOTP, setValidatingToken } from './authReducer';

export function authIn(userData: some, skipSaga: boolean = false): ThunkAction<void, AppState, null, Action<string>> {
  return (dispatch, getState) => {
    const state = getState();
    dispatch(setUserData(userData));
    if (!state.auth.auth) {
      dispatch(inAction(skipSaga));
      dispatch(fetchBookerInfo());
    }
  };
}

export function validateAccessToken(periodic = false): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    let prevAccessToken = get(ACCESS_TOKEN);
    let first = true;
    const fn = async (force = false) => {
      const accessToken = get(ACCESS_TOKEN);
      const state = getState();
      if (accessToken) {
        if (first || prevAccessToken !== accessToken || force) {
          first = false;
          dispatch(setValidatingToken(true));
          try {
            const json = await dispatch(fetchThunk(`${API_PATHS.validateAccessToken}`, 'get', true));
            if (json.code === 200) {
              dispatch(authIn(json.data));
              prevAccessToken = accessToken;
            } else if (getState().auth.auth) {
              dispatch(out());
              remove(ACCESS_TOKEN);
              dispatch(setUserData());
              dispatch(
                push({
                  pathname: '/',
                  search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidAccessToken)}`,
                }),
              );
            }
          } finally {
            dispatch(setValidatingToken(false));
          }
        }
      } else if (state.auth.auth) {
        dispatch(out());
      }
    };

    if (periodic) {
      setInterval(fn, 1000);
    } else {
      fn(true);
    }
  };
}

export function login(
  loginId: string,
  password: string,
  otp?: string,
): ThunkAction<Promise<number | string | null>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    dispatch(setAuthenticating(true));
    try {
      const json = await dispatch(
        fetchThunk(API_PATHS.login, 'post', false, JSON.stringify({ password, otp, user_name: loginId })),
      );
      if (json.code === 200) {
        loadParamsToStore(dispatch);
        set(ACCESS_TOKEN, json.access_token);
        dispatch(authIn(json.data));
        return null;
      }
      if (json.code === NewDeviceLoginError) {
        return NewDeviceLoginError;
      }
      dispatch(setLoginErrorMsg(json.message));
      return json.messsage;
    } finally {
      dispatch(setAuthenticating(false));
    }
  };
}

export function requestLoginOTP(loginId: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(`${API_PATHS.sendNewDeviceLoginOTP}?phoneNumber=${loginId}`, 'get', false));
    if (json.code === 200) {
      dispatch(
        setResendOTP(
          moment()
            .add(2, 'minutes')
            .valueOf(),
        ),
      );
    }
    dispatch(setLoginErrorMsg(json.message));
  };
}
export function logout(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    remove(ACCESS_TOKEN);
    batch(() => {
      const { location } = getState().router;
      if (location.pathname === ROUTES.management) {
        dispatch(replace({ ...location, search: undefined }));
      }
      dispatch(out());
      dispatch(clearStoreAfterLogout());
      loadParamsToStore(dispatch, new URLSearchParams(getState().router.location.search));
    });
  };
}

export function getSignUpOPT(phone: string): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.sendSignUpOTP,
          'post',
          false,
          JSON.stringify({
            phone,
          }),
        ),
      );
      return json;
    } finally {
    }
  };
}

export function uploadFile(file: File): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const formData = new FormData();
    formData.append('file', file);
    const json = await dispatch(fetchThunk(API_PATHS.upload, 'post', true, formData));
    return json;
  };
}

export function updateAgencyInformation(
  agencyInformation: AgencyInformation,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(
        fetchThunk(API_PATHS.updateAgencyInformation, 'post', true, JSON.stringify(agencyInformation)),
      );
      return json;
    } finally {
    }
  };
}

export function verifyAgencyInformation(
  agencyInformation: AgencyInformation,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(
        fetchThunk(API_PATHS.verifyAgencyInformation, 'post', true, JSON.stringify(agencyInformation)),
      );
      return json;
    } finally {
    }
  };
}

export function signUp(data: some): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(fetchThunk(API_PATHS.signUpV2, 'post', false, JSON.stringify(data)));
      if (json.code === 200) {
        set(ACCESS_TOKEN, json.access_token);
        dispatch(authIn(json.data, true));
      }
      return json;
    } finally {
    }
  };
}

export function sendForgotPasswordOTP(phone: string): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.sendForgotPasswordOTP,
          'post',
          false,
          JSON.stringify({
            phone,
          }),
        ),
      );
      return json;
    } finally {
    }
  };
}

export function forgotPassword(params: some): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    try {
      const json = await dispatch(fetchThunk(API_PATHS.forgotPasswordV2, 'post', false, JSON.stringify(params)));
      return json;
    } finally {
    }
  };
}
