export const ACCESS_TOKEN = 'ACCESS_TOKEN';
export const USER_ID = 'fuser_id';
export const PHOTO_TYPE = {
  FRONT_IDENTITY_CARD: 'frontIdentityCard',
  BACK_IDENTITY_CARD: 'backIdentityCard',
  BUSINESS_PROFILE: 'businessProfiles',
};
export const NewDeviceLoginError = 415;
export const MAX_OTP_SECONDS = 120;
