import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ModuleType, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';
import PaymentSuccessDesktop from '../components/PaymentSuccessDesktop';

const mapStateToProps = (state: AppState) => {
  return {
    stateRouter: state.router.location.state,
    router: state.router,
  };
};

interface Props extends RouteComponentProps<{ moduleType: ModuleType; bookingId: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const PartnerPaymentSuccess: React.FC<Props> = props => {
  const { dispatch, match } = props;

  const { bookingId, moduleType } = match.params;
  const [loading, setLoading] = React.useState(false);
  const [messageJson, setMessageJson] = React.useState<some>();
  const [bookingInfo, setBookingInfo] = React.useState<some>();

  const fetchData = React.useCallback(async () => {
    setLoading(true);
    const fetchDisplayResult = dispatch(
      fetchThunk(API_PATHS.checkoutSuccess, 'post', false, JSON.stringify({ url: window.location.href })),
    );

    const fetchBookingInfo = dispatch(
      fetchThunk(
        moduleType === 'hotel' ? API_PATHS.getHotelBookingInformation : API_PATHS.getFlightBookingInformation,
        'post',
        false,
        JSON.stringify({ bookingId }),
      ),
    );

    const displayResultJson = await fetchDisplayResult;
    const bookingInfoJson = await fetchBookingInfo;

    setLoading(false);
    setMessageJson(displayResultJson);
    if (bookingInfoJson?.data) {
      setBookingInfo(bookingInfoJson.data);
    }
  }, [dispatch, bookingId, moduleType]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <PaymentSuccessDesktop
      loading={loading}
      moduleType={moduleType}
      messageJson={messageJson}
      bookingInfo={bookingInfo}
    />
  );
};

export default connect(mapStateToProps)(PartnerPaymentSuccess);
