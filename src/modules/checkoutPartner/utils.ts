import { PRIMARY, SECONDARY, ORANGE } from '../../colors';
import { some, MY_TOUR } from '../../constants';

export function getMainColor(data: some) {
  return {
    primary: data?.mainColorCode ?? PRIMARY,
    secondary: MY_TOUR ? ORANGE : SECONDARY,
  };
}
