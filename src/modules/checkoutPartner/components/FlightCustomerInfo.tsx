import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../constants';
import { getMainColor } from '../utils';
import { Row } from './styles';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

const TravellerInfoBox: React.FC<{
  travellerCount: number;
  travellerType: string;
  ageCategory: string;
  guests: some[];
}> = props => {
  const { travellerCount, travellerType, ageCategory, guests } = props;

  return (
    <div>
      <Typography variant="body2" style={{ fontWeight: 500, textTransform: 'lowercase' }}>
        {travellerCount}&nbsp;
        <FormattedMessage id={travellerType} />
      </Typography>

      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {guests
          .filter((item: some) => item.ageCategory === ageCategory)
          .map((one: some) => (
            <Typography variant="body2" style={{ flex: 3, display: 'flex', alignItems: 'flex-start' }}>
              <ArrowRightIcon style={{ margin: '0 6px' }} fontSize="small" />
              {one.fullName}&nbsp;-&nbsp;
              <FormattedMessage id={one.gender === 'M' ? 'male' : 'female'} />
              {ageCategory !== 'adult' && <>&nbsp;-&nbsp;{one.dob}</>}
            </Typography>
          ))}
      </div>
    </div>
  );
};

interface Props {
  bookingInfo?: some;
}

const FlightCustomerInfo = (props: Props) => {
  const { bookingInfo } = props;
  const colors = getMainColor(bookingInfo?.caAccount);

  return (
    <div style={{ border: `1px solid ${colors.primary}`, width: '100%', maxWidth: '780px' }}>
      <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
        <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
          <FormattedMessage id="booking.travellersInfo" />
        </Typography>
      </div>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.totalTraveller" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold', textTransform: 'lowercase' }}>
          {bookingInfo?.numGuests}&nbsp;
          <FormattedMessage id="search.travellerInfo.travellers" />
        </Typography>
      </Row>

      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="ticketAlert.detail" />
        </Typography>
        <div style={{ flex: 3 }}>
          {!!bookingInfo?.numAdults && (
            <TravellerInfoBox
              travellerCount={bookingInfo?.numAdults}
              travellerType="adult"
              ageCategory="adult"
              guests={bookingInfo.guests}
            />
          )}
          {!!bookingInfo?.numChildren && (
            <TravellerInfoBox
              travellerCount={bookingInfo?.numChildren}
              travellerType="children"
              ageCategory="children"
              guests={bookingInfo.guests}
            />
          )}
          {!!bookingInfo?.numInfants && (
            <TravellerInfoBox
              travellerCount={bookingInfo?.numInfants}
              travellerType="baby"
              ageCategory="infant"
              guests={bookingInfo.guests}
            />
          )}
        </div>
      </Row>
    </div>
  );
};

export default FlightCustomerInfo;
