import React from 'react';
import { Container } from '@material-ui/core';
import { PageWrapper } from '../../common/components/elements';
import LoadingIcon from '../../common/components/LoadingIcon';
import { ModuleType, some } from '../../../constants';
import HotelBookingInfo from './HotelBookingInfo';
import PaymentSuccessHeader from './PaymentSuccessHeader';
import FlightBookingInfo from './FlightBookingInfo';

interface Props {
  loading: boolean;
  moduleType: ModuleType;
  messageJson?: some;
  bookingInfo?: some;
}

const PaymentSuccessDesktop: React.FC<Props> = props => {
  const { loading, moduleType, messageJson, bookingInfo } = props;

  return (
    <div style={{ minHeight: '100vh' }}>
      <PageWrapper>
        <Container
          style={{
            flex: 1,
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          {loading ? (
            <LoadingIcon style={{ flex: 1 }} />
          ) : (
            <>
              <PaymentSuccessHeader bookingInfo={bookingInfo} />
              {moduleType === 'hotel' && <HotelBookingInfo messageJson={messageJson} bookingInfo={bookingInfo} />}
              {moduleType === 'flight' && <FlightBookingInfo messageJson={messageJson} bookingInfo={bookingInfo} />}
            </>
          )}
        </Container>
      </PageWrapper>
    </div>
  );
};

export default PaymentSuccessDesktop;
