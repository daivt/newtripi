import styled from 'styled-components';

export const Row = styled.div`
  display: flex;
  align-items: center;
  padding: 3px 8px;
  border-top: 1px solid #c6e3e1;
`;
