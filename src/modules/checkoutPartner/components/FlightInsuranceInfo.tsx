import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import { some, MY_TOUR } from '../../../constants';
import { getMainColor } from '../utils';
import { Row } from './styles';
import { SECONDARY, ORANGE, PRIMARY } from '../../../colors';

const secondary = MY_TOUR ? PRIMARY : SECONDARY;

interface Props {
  bookingInfo: some;
}

const FlightInsuranceInfo = (props: Props) => {
  const { bookingInfo } = props;
  const { guests } = bookingInfo;
  const colors = getMainColor(bookingInfo?.caAccount);

  return (
    <div style={{ border: `1px solid ${colors.primary}`, width: '100%', maxWidth: '780px', marginTop: '24px' }}>
      <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
        <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
          <FormattedMessage id="payment.insuranceInfo" />
        </Typography>
      </div>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.packageName" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold', color: secondary }}>
          {guests[0]?.insuranceInfo?.insurancePackageCode}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.activateDate" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {guests[0]?.insuranceInfo?.fromDate}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.deactivateDate" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {guests[0]?.insuranceInfo?.toDate}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.personInsurance" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {bookingInfo?.numGuests}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="booking.priceDetails" />
        </Typography>
        <div style={{ display: 'flex', flexDirection: 'column', flex: 3 }}>
          {guests.map((one: some) => (
            <Typography variant="body2" style={{ display: 'flex', alignItems: 'flex-start', flexWrap: 'wrap' }}>
              <span style={{ display: 'flex', alignItems: 'center' }}>
                <ArrowRightIcon style={{ marginRight: '3px' }} fontSize="small" />
                {one.fullName}&nbsp;-&nbsp;
              </span>
              <FormattedMessage id={one.gender === 'M' ? 'male' : 'female'} />
              &nbsp;-&nbsp;
              <strong>
                <FormattedNumber value={bookingInfo?.insuranceAmount / bookingInfo?.numGuests} />
                &nbsp;
                <FormattedMessage id="currency" />
              </strong>
            </Typography>
          ))}
        </div>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.totalPrice" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold', color: ORANGE }}>
          <FormattedNumber value={bookingInfo?.insuranceAmount} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Row>
    </div>
  );
};

export default FlightInsuranceInfo;
