import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import { some, MY_TOUR } from '../../../constants';
import { getMainColor } from '../utils';
import { Row } from './styles';
import { SECONDARY, ORANGE, PRIMARY } from '../../../colors';

const secondary = MY_TOUR ? PRIMARY : SECONDARY;
interface Props {
  isInbound?: boolean;
  bookingInfo: some;
  ticketDetail: some;
}

const FlightTicketInfo = (props: Props) => {
  const { isInbound, bookingInfo, ticketDetail } = props;
  const colors = getMainColor(bookingInfo?.caAccount);

  return (
    <div style={{ border: `1px solid ${colors.primary}`, width: '100%', maxWidth: '780px', marginTop: '24px' }}>
      <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
        <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
          <FormattedMessage
            id="payment.flightTicketInfo"
            values={{
              text: (
                <Typography variant="button" component="span" style={{ textTransform: 'lowercase' }}>
                  <FormattedMessage id={isInbound ? 'inbound' : 'outbound'} />
                </Typography>
              ),
            }}
          />
        </Typography>
      </div>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="home.flightTotal" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold', color: secondary }}>
          {ticketDetail.flightNumber}&nbsp;
          <Typography variant="body2" component="span" style={{ flex: 3, fontWeight: 'bold' }} color="textPrimary">
            -&nbsp;{ticketDetail.agencyName}
          </Typography>
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="filter.ticketClass" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {ticketDetail.ticketClass}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.departure" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {`${ticketDetail.departureTimeStr} ${ticketDetail.departureDayStr} - ${ticketDetail.departureAirport} - ${ticketDetail.departureAirportName}`}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.arrival" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
          {`${ticketDetail.arrivalTimeStr} ${ticketDetail.arrivalDayStr} - ${ticketDetail.arrivalAirport} - ${ticketDetail.arrivalAirportName}`}
        </Typography>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="booking.priceDetails" />
        </Typography>
        <div style={{ display: 'flex', flexDirection: 'column', flex: 3 }}>
          {!!bookingInfo.numAdults && (
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Typography
                variant="body2"
                style={{ minWidth: '100px', marginRight: '24px', display: 'flex', alignItems: 'flex-start' }}
              >
                <ArrowRightIcon style={{ marginRight: '3px' }} fontSize="small" />
                <FormattedMessage id="adult" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                <Typography
                  variant="body2"
                  style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', minWidth: '120px' }}
                >
                  <FormattedNumber value={ticketDetail.priceAdult} /> x {bookingInfo.numAdults} =
                </Typography>
                <Typography variant="body2" style={{ display: 'flex', alignItems: 'flex-start', fontWeight: 'bold' }}>
                  &nbsp;
                  <FormattedNumber value={ticketDetail.priceAdult * bookingInfo.numAdults} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            </div>
          )}

          {!!bookingInfo.numChildren && (
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Typography
                variant="body2"
                style={{ minWidth: '100px', marginRight: '24px', display: 'flex', alignItems: 'flex-start' }}
              >
                <ArrowRightIcon style={{ marginRight: '3px' }} fontSize="small" />
                <FormattedMessage id="children" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                <Typography
                  variant="body2"
                  style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', minWidth: '120px' }}
                >
                  <FormattedNumber value={ticketDetail.priceChild} /> x {bookingInfo.numChildren} =
                </Typography>
                <Typography variant="body2" style={{ display: 'flex', alignItems: 'flex-start', fontWeight: 'bold' }}>
                  &nbsp;
                  <FormattedNumber value={ticketDetail.priceChild * bookingInfo.numChildren} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            </div>
          )}

          {!!bookingInfo.numInfants && (
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Typography
                variant="body2"
                style={{ minWidth: '100px', marginRight: '24px', display: 'flex', alignItems: 'flex-start' }}
              >
                <ArrowRightIcon style={{ marginRight: '3px' }} fontSize="small" />
                <FormattedMessage id="baby" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                <Typography
                  variant="body2"
                  style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', minWidth: '120px' }}
                >
                  <FormattedNumber value={ticketDetail.priceInfant} /> x {bookingInfo.numInfants} =
                </Typography>
                <Typography variant="body2" style={{ display: 'flex', alignItems: 'flex-start', fontWeight: 'bold' }}>
                  &nbsp;
                  <FormattedNumber value={ticketDetail.priceInfant * bookingInfo.numInfants} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            </div>
          )}
        </div>
      </Row>
      <Row>
        <Typography variant="body2" style={{ flex: 1, flexShrink: 0, marginRight: '16px', fontWeight: 'bold' }}>
          <FormattedMessage id="payment.totalPrice" />
        </Typography>
        <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold', color: ORANGE }}>
          <FormattedNumber value={ticketDetail.totalPrice} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Row>
    </div>
  );
};

export default FlightTicketInfo;
