import { Divider, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedHTMLMessage, FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../constants';
import { getMainColor } from '../utils';
import FlightCustomerInfo from './FlightCustomerInfo';
import FlightInsuranceInfo from './FlightInsuranceInfo';
import FlightTicketInfo from './FlightTicketInfo';
import { Row } from './styles';
import { RED } from '../../../colors';

interface Props {
  messageJson?: some;
  bookingInfo?: some;
}

const FlightBookingInfo: React.FC<Props> = props => {
  const { messageJson, bookingInfo } = props;
  const colors = getMainColor(bookingInfo?.caAccount);

  return (
    <div style={{ flex: 1, display: 'flex', flexDirection: 'column', alignItems: 'center', width: '100%' }}>
      <Typography
        variant="subtitle1"
        style={{
          margin: '12px 0',
          textAlign: 'center',
          lineHeight: '24px',
          color: messageJson?.code !== 200 ? RED : colors.primary,
        }}
      >
        {messageJson?.message}
      </Typography>

      <Typography variant="subtitle2" style={{ textAlign: 'center', color: colors.primary }}>
        <FormattedHTMLMessage
          id="payment.tripiSupportText"
          values={{
            chanel: bookingInfo?.caAccount?.fullName,
            phone: bookingInfo?.caAccount?.hotline,
          }}
        />
      </Typography>
      <Typography variant="subtitle2" style={{ textAlign: 'center', color: colors.primary }}>
        <FormattedHTMLMessage
          id="payment.tripiReadyServeYou"
          values={{
            chanel: bookingInfo?.caAccount?.fullName,
          }}
        />
      </Typography>

      <Divider style={{ width: '80%', margin: '16px 0' }} />

      <Typography variant="h5" style={{ margin: '24px 0', textAlign: 'center' }}>
        <FormattedMessage id="payment.flightBookingDetail" />
      </Typography>

      <FlightCustomerInfo bookingInfo={bookingInfo} />
      {bookingInfo && <FlightTicketInfo bookingInfo={bookingInfo} ticketDetail={bookingInfo.tickets.outbound} />}
      {bookingInfo && bookingInfo.tickets?.inbound?.aid && (
        <FlightTicketInfo bookingInfo={bookingInfo} ticketDetail={bookingInfo.tickets.inbound} />
      )}
      {bookingInfo && bookingInfo?.insuranceAmount > 0 && <FlightInsuranceInfo bookingInfo={bookingInfo} />}

      <div style={{ border: `1px solid ${colors.primary}`, width: '100%', maxWidth: '780px', margin: '24px 0 32px' }}>
        <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
          <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
            <FormattedMessage id="payment.paymentInfo" />
          </Typography>
        </div>
        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.personPayment" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            {bookingInfo?.contactInfo?.fullName}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="email" />
          </Typography>
          <Typography
            variant="body2"
            style={{ flex: 3, fontWeight: 'bold', display: 'flex', alignItems: 'center', wordBreak: 'break-word' }}
          >
            {bookingInfo?.contactInfo?.email}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="telephone" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            {bookingInfo?.contactInfo?.phone1}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.bookingDate" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            {bookingInfo?.created}
          </Typography>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.totalPrice" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            <FormattedNumber value={bookingInfo?.totalPrice || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.paymentMethodFee" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            <FormattedNumber value={bookingInfo?.paymentMethodFee || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="mobile.promotion" />
          </Typography>
          <Typography variant="body2" style={{ flex: 3, fontWeight: 'bold' }}>
            <FormattedNumber value={bookingInfo?.discount || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>

        <Row>
          <Typography variant="subtitle2" style={{ flex: 2, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.finalPrice" />
          </Typography>
          <Typography variant="subtitle2" style={{ flex: 3, fontWeight: 'bold', color: colors.secondary }}>
            <FormattedNumber value={bookingInfo?.finalPrice || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
      </div>
    </div>
  );
};

export default FlightBookingInfo;
