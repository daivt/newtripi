import React from 'react';
import { some } from '../../../constants';

interface Props {
  bookingInfo?: some;
}

const PaymentSuccessHeader: React.FC<Props> = props => {
  const { bookingInfo } = props;

  return (
    <div
      style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', margin: '16px 0', width: '100%' }}
    >
      {bookingInfo?.caAccount?.caAccount?.id === 17 ? (
        <div style={{ display: 'flex', flex: 1 }}>
          <a href="https://mytour.vn/">
            <img src={bookingInfo?.caAccount?.logo} alt="" style={{ maxHeight: '32px' }} />
          </a>
        </div>
      ) : (
        <div style={{ display: 'flex', flex: 1 }}>
          <img src={bookingInfo?.caAccount?.logo} alt="" style={{ maxHeight: '32px' }} />
        </div>
      )}

      {/* <div style={{ display: 'flex', flex: 1, justifyContent: 'center', margin: '3px 16px' }}>
        <div style={{ display: 'flex', flexDirection: 'column', minWidth: '180px' }}>
          <Typography variant="caption">
            <FormattedMessage id="payment.workingDay" />
          </Typography>
          <Typography variant="caption">
            <FormattedHTMLMessage id="payment.workingDayDescription" />
          </Typography>
        </div>
      </div>
      <div style={{ display: 'flex', flex: 1, justifyContent: 'center', margin: '3px 16px' }}>
        <div style={{ display: 'flex', flexDirection: 'column', minWidth: '180px' }}>
          <Typography variant="caption">
            <FormattedMessage id="payment.contact" />
          </Typography>
          <Typography variant="caption" style={{ fontWeight: 'bold' }}>
            {`${bookingInfo?.caAccount?.phoneNumber} - ${bookingInfo?.caAccount?.hotline}`}
          </Typography>
        </div>
      </div> */}
    </div>
  );
};

export default PaymentSuccessHeader;
