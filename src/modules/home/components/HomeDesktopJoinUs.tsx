import { ButtonBase } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import Slider from 'react-slick';
import styled from 'styled-components';
import { ROUTES, some } from '../../../constants';
import DanTriLogo from '../../../svg/dantri_logo.svg';
import ICTNewsLogo from '../../../svg/ict_group_logo.svg';
import icNext from '../../../svg/ic_next.svg';
import icPrev from '../../../svg/ic_prev.svg';
import VNExpressLogo from '../../../svg/vn_express_logo.svg';
import Link from '../../common/components/Link';
import { HOME_DESKTOP_IMAGE } from '../constants';

const Wrapper = styled.div`
  background-color: ${props => props.theme.primary};
`;

interface Props {}

const hotNew = [
  {
    id: 0,
    website: 'home.hotNewWebsite1',
    content: 'home.hotNewContent1',
    imageUrl: DanTriLogo,
  },
  {
    id: 1,
    website: 'home.hotNewWebsite2',
    content: 'home.hotNewContent2',
    imageUrl: ICTNewsLogo,
  },
  {
    id: 2,
    website: 'home.hotNewWebsite3',
    content: 'home.hotNewContent3',
    imageUrl: VNExpressLogo,
  },
];

const Arrow = ({ img, className, style, onClick }: some) => {
  const replaceClass = className.replace('slick-arrow', '');
  return (
    <div className={replaceClass} style={{ ...style, zIndex: 100 }}>
      <ButtonBase onClick={onClick}>
        <img alt="" src={img} />
      </ButtonBase>
    </div>
  );
};

class HomeDesktopJoinUs extends PureComponent<Props, {}> {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      nextArrow: <Arrow img={icNext} />,
      prevArrow: <Arrow img={icPrev} />,
    };

    return (
      <>
        <Wrapper>
          <Container
            style={{
              display: 'flex',
              flexFlow: 'column',
            }}
          >
            <div style={{ height: '300px', overflow: 'visible', textAlign: 'center' }}>
              <Slider {...settings}>
                {hotNew.map(item => (
                  <div key={item.id}>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: '300px',
                        width: '100%',
                      }}
                    >
                      <img
                        style={{
                          height: '170px',
                          width: '170px',
                        }}
                        src={item.imageUrl}
                        alt=""
                      />
                      <div
                        style={{
                          display: 'flex',
                          flexFlow: 'column',
                          color: '#fff',
                          maxWidth: '500px',
                          marginLeft: '100px',
                          textAlign: 'left',
                          paddingBottom: '12px',
                        }}
                      >
                        <span
                          style={{
                            fontSize: '20px',
                            lineHeight: '170%',
                            fontWeight: 500,
                          }}
                        >
                          <FormattedMessage id={item.content} />
                        </span>
                        <FormattedMessage id={item.website} />
                      </div>
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          </Container>
        </Wrapper>
        <div style={{ backgroundColor: '#fff' }}>
          <Container style={{ display: 'flex', flexDirection: 'row', height: '350px' }}>
            <div style={{ flex: 1 }}>
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  justifyContent: 'space-between',
                  padding: '65px 0 50px',
                  height: '100%',
                }}
              >
                <Typography variant="h4" style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>
                  <FormattedMessage id="home.joinUs" />
                </Typography>
                <Typography variant="body1" style={{ textAlign: 'justify', paddingRight: 12 }}>
                  <FormattedMessage id="home.joinUsDescription" />
                </Typography>
                <div>
                  <Link to={ROUTES.signUp}>
                    <Button color="secondary" variant="contained" size="large" style={{ width: '200px' }}>
                      <FormattedMessage id="home.joinNow" />
                    </Button>
                  </Link>
                </div>
              </div>
            </div>

            <div style={{ flex: 1 }}>
              <img
                style={{
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover',
                }}
                src={HOME_DESKTOP_IMAGE.joinUs}
                alt=""
              />
            </div>
          </Container>
        </div>
      </>
    );
  }
}

export default HomeDesktopJoinUs;
