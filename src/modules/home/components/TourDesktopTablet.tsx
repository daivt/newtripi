import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { some, TABLET_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { PageWrapper } from '../../common/components/elements';
import Footer from '../../common/components/Footer';
import Header from '../../common/components/Header';
import TourSearch from '../../search/components/TourSearch';
import PopularDestinationsBox from './common/PopularDestinationsBox';
import PromotionBox from './tour/PromotionBox';
import TourFindRecentBox from './tour/TourFindRecentBox';
interface Props extends ReturnType<typeof mapStateToProps> {
  promotionData: some[];
}

interface State {
  popularDestinations: some[];
}
const POPULAR_DESTINATION = [
  { id: 0, skeleton: true },
  { id: 1, skeleton: true },
  { id: 2, skeleton: true },
  { id: 3, skeleton: true },
  { id: 4, skeleton: true },
  { id: 5, skeleton: true },
  { id: 6, skeleton: true },
  { id: 7, skeleton: true },
];
class TourDesktopTablet extends PureComponent<Props, State> {
  render() {
    const { promotionData, notableLocationsTour } = this.props;

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light={true} />
        <TourSearch home={false} />
        <Container style={{ flex: 1 }}>
          <TourFindRecentBox />
          <Typography style={{ marginTop: '16px' }} variant="h5">
            <FormattedMessage id="hotel.popularDestinations" />
          </Typography>
          <PopularDestinationsBox
            module="tour"
            data={notableLocationsTour.length > 0 ? notableLocationsTour : POPULAR_DESTINATION}
          />
          <PromotionBox data={promotionData} />
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}
const mapStateToProps = (state: AppState) => {
  return {
    notableLocationsTour: state.common.notableLocationsTour,
  };
};
export default connect(mapStateToProps)(TourDesktopTablet);
