import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import IconAmadeus from '../../../svg/ic_home_desktop_partner_amadeus.svg';
import IconBamboo from '../../../svg/ic_home_desktop_partner_bamboo.svg';
import IconExpedia from '../../../svg/ic_home_desktop_partner_expedia.svg';
import IconFlcGroup from '../../../svg/ic_home_desktop_partner_flc_group.svg';
import IconGalileo from '../../../svg/ic_home_desktop_partner_galileo.svg';
import IconHotelbeds from '../../../svg/ic_home_desktop_partner_hotelbeds.svg';
import IconJetstar from '../../../svg/ic_home_desktop_partner_jetstar.svg';
import IconKlook from '../../../svg/ic_home_desktop_partner_klook.svg';
import IconSabre from '../../../svg/ic_home_desktop_partner_sabre.svg';
import IconSunGroup from '../../../svg/ic_home_desktop_partner_sun_group.svg';
import IconVietjetAir from '../../../svg/ic_home_desktop_partner_vietjet_air.svg';
import IconVietnamAirline from '../../../svg/ic_home_desktop_partner_vietnam_airlines.svg';
import IconVinGroup from '../../../svg/ic_home_desktop_partner_vin_group.svg';

const partners = [
  {
    id: 0,
    icon: IconVietnamAirline,
  },
  {
    id: 1,
    icon: IconJetstar,
  },
  {
    id: 2,
    icon: IconVietjetAir,
  },
  {
    id: 3,
    icon: IconBamboo,
  },

  {
    id: 4,
    icon: IconAmadeus,
  },

  {
    id: 5,
    icon: IconGalileo,
  },
  {
    id: 6,
    icon: IconSabre,
  },
  {
    id: 7,
    icon: null,
  },
  {
    id: 8,
    icon: IconSunGroup,
  },
  {
    id: 9,
    icon: IconVinGroup,
  },
  {
    id: 10,
    icon: IconFlcGroup,
  },
  {
    id: 11,
    icon: null,
  },
  {
    id: 12,
    icon: IconExpedia,
  },
  {
    id: 13,
    icon: IconHotelbeds,
  },
  {
    id: 14,
    icon: IconKlook,
  },
];

class HomeDesktopPartner extends PureComponent {
  render() {
    return (
      <div style={{ backgroundColor: '#fff' }}>
        <Container style={{ display: 'flex', minHeight: '430px' }}>
          <div style={{ flex: 1, display: 'flex' }}>
            <div style={{ paddingTop: '100px' }}>
              <Typography variant="h4" style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>
                <FormattedMessage id="home.connectedPartner" values={{ number: <FormattedNumber value={18000} /> }} />
              </Typography>
              <div style={{ maxWidth: '500px', paddingTop: '42px', textAlign: 'justify' }}>
                <Typography variant="body1" style={{ color: '#757575' }}>
                  <FormattedMessage id="home.connectedPartnerDescription" />
                </Typography>
              </div>
            </div>
          </div>
          <div
            style={{
              flex: 1,
              display: 'flex',
              flexWrap: 'wrap',
              paddingTop: '100px',
            }}
          >
            {partners.map(partner => (
              <div
                key={partner.id}
                style={{
                  width: '130px',
                  textAlign: 'center',
                  marginRight: '20px',
                }}
              >
                {partner.icon && <img src={partner.icon} alt="" />}
              </div>
            ))}
          </div>
        </Container>
      </div>
    );
  }
}

export default HomeDesktopPartner;
