import { Backdrop, withStyles } from '@material-ui/core';
import Tab, { TabProps } from '@material-ui/core/Tab';
import Tabs_ from '@material-ui/core/Tabs';
import React, { PureComponent } from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { ReactComponent as FlightSvg } from '../../../svg/Flight.svg';
import { ReactComponent as HotelSvg } from '../../../svg/Hotel.svg';
import { ReactComponent as TicketHuntSvg } from '../../../svg/TicketHunt.svg';
import { ReactComponent as TourSvg } from '../../../svg/TourismActivities.svg';
import { ReactComponent as TrainSvg } from '../../../svg/Train.svg';
import FlightHuntSearch from '../../search/components/FlightHuntSearch';
import FlightsSearch from '../../search/components/FlightSearch';
import HotelSearch from '../../search/components/HotelSearch';
import TourSearch from '../../search/components/TourSearch';
import TrainSearch from '../../search/components/TrainSearch';
import styles from './Tabs.module.scss';
import { TAB_SCROLL_TOP_HEIGHT } from '../constants';

const tabs = [
  {
    img: <FlightSvg />,
    id: 'flight',
    content: <FlightsSearch home />,
  },
  {
    img: <HotelSvg />,
    id: 'hotel',
    content: <HotelSearch home />,
  },
  {
    img: <TourSvg />,
    id: 'tourTitle',
    content: <TourSearch />,
  },
  {
    img: <TrainSvg />,
    id: 'trainTickets',
    content: <TrainSearch home />,
  },
  {
    img: <TicketHuntSvg />,
    id: 'ticketHunt',
    content: <FlightHuntSearch home />,
  },
];

const Container = styled.div`
  max-width: 1200px;
  margin: 10px;
  background: #ffffff;
  box-sizing: border-box;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.25);
  border-radius: 4px 4px 4px 4px;
  position: relative;
  z-index: 99;
`;

const TabContentContainer = styled.div`
  border-top: 1px solid #bdbdbd;
  border-radius: 0 0 4px 4px;
  background: white;
`;

const CustomTabs = withStyles(theme => ({
  root: {},
  indicator: {
    backgroundColor: theme.palette.primary.main,
    height: 5,
  },
}))(Tabs_);

const CustomTab = withStyles(theme => ({
  root: {
    fontSize: '1.05em',
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    opacity: 1,
    padding: '16px 0',
  },
  selected: {
    background: 'linear-gradient(360deg, #f5f5f5 0%, rgba(245, 245, 245, 0) 94.81%)',
  },
}))((props: TabProps) => <Tab {...props} />);

interface Props extends WrappedComponentProps {}

interface State {
  currentTabIndex: number;
}

class Tabs extends PureComponent<Props, State> {
  transitionClassNames: CSSTransition.CSSTransitionClassNames = {};

  contentDivRef = React.createRef<HTMLDivElement>();

  state = {
    currentTabIndex: -1,
  };

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.state.currentTabIndex !== -1 && prevState.currentTabIndex === -1) {
      window.scrollTo({ top: TAB_SCROLL_TOP_HEIGHT, behavior: 'smooth' });
    }
  }

  render() {
    const { currentTabIndex } = this.state;
    const { intl } = this.props;

    return (
      <div style={{ position: 'relative', zIndex: currentTabIndex === -1 ? undefined : 1250 }}>
        <Backdrop
          open={currentTabIndex !== -1}
          onClick={() => {
            this.transitionClassNames.exitActive = styles.exitActive;
            this.transitionClassNames.exit = styles.exit;
            this.setState({ currentTabIndex: -1 });
          }}
        />
        <Container>
          <CustomTabs
            variant="fullWidth"
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => {
              const enter =
                this.state.currentTabIndex === -1
                  ? styles.enter
                  : val > this.state.currentTabIndex
                  ? styles.slideLeft
                  : val < this.state.currentTabIndex
                  ? styles.slideRight
                  : undefined;
              this.transitionClassNames = {
                enter,
                enterActive: styles.enterActive,
                exit: styles.exitSlide,
                exitActive:
                  val > this.state.currentTabIndex
                    ? styles.exitLeftActive
                    : val < this.state.currentTabIndex
                    ? styles.exitRightActive
                    : undefined,
              };
              this.setState({ currentTabIndex: val });
            }}
          >
            {tabs.map(tab => (
              <CustomTab
                icon={<div style={{ height: '30px', display: 'flex', alignItems: 'flex-end' }}>{tab.img}</div>}
                label={intl.formatMessage({ id: tab.id })}
                key={tab.id}
              />
            ))}
          </CustomTabs>
          <div style={{ position: 'relative', borderRadius: '0 0 4px 4px' }} ref={this.contentDivRef}>
            {tabs.map((tab, i) => (
              <CSSTransition
                key={tab.id}
                unmountOnExit
                classNames={this.transitionClassNames}
                timeout={300}
                in={i === currentTabIndex}
                onEntering={() => {
                  if (this.contentDivRef.current) {
                    this.contentDivRef.current.className = styles.overflowHidden;
                  }
                }}
                onEntered={() => {
                  if (this.contentDivRef.current) {
                    this.contentDivRef.current.className = '';
                  }
                }}
              >
                <TabContentContainer>{tab.content}</TabContentContainer>
              </CSSTransition>
            ))}
          </div>
        </Container>
      </div>
    );
  }
}

export default injectIntl(Tabs);
