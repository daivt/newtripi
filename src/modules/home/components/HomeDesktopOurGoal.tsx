import React, { PureComponent } from 'react';
import { Typography, Container } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { HOME_DESKTOP_IMAGE } from '../constants';

class HomeDesktopOurGoal extends PureComponent {
  render() {
    return (
      <Container>
        <div style={{ display: 'flex' }}>
          <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end' }}>
            <img
              style={{
                width: '100%',
                objectFit: 'cover',
              }}
              src={HOME_DESKTOP_IMAGE.ourGoalImage}
              alt=""
            />
          </div>
          <div style={{ flex: 1 }}>
            <div style={{ paddingTop: '130px', paddingLeft: '85px' }}>
              <Typography variant="h4" style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>
                <FormattedMessage id="home.ourTarget" />
              </Typography>
              <div style={{ maxWidth: '500px', paddingTop: '42px', textAlign: 'justify' }}>
                <Typography variant="body1" style={{ color: '#757575' }}>
                  <FormattedMessage id="home.ourTargetContent2" />
                </Typography>
              </div>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default HomeDesktopOurGoal;
