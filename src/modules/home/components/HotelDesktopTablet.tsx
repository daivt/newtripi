import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { some, TABLET_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { PageWrapper } from '../../common/components/elements';
import Footer from '../../common/components/Footer';
import Header from '../../common/components/Header';
import HotelSearch from '../../search/components/HotelSearch';
import PopularDestinationsBox from './common/PopularDestinationsBox';
import HotelPartner from './hotel/HotelPartner';
import HotelRecentSearchSlider from './hotel/HotelRecentSearchSlider';

interface Props extends ReturnType<typeof mapStateToProps> {}

interface State {}
class HotelDesktopTablet extends PureComponent<Props, State> {
  render() {
    const { popularDestinations } = this.props;
    let dataTmp: some[] = [];
    if (popularDestinations.length === 0) {
      dataTmp = [
        { id: 0, skeleton: true },
        { id: 1, skeleton: true },
        { id: 2, skeleton: true },
        { id: 3, skeleton: true },
        { id: 4, skeleton: true },
        { id: 5, skeleton: true },
        { id: 6, skeleton: true },
        { id: 7, skeleton: true },
      ];
    } else if (popularDestinations.length > 8) {
      dataTmp = popularDestinations.slice(0, 8);
    }

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <HotelSearch home={false} />
        <Container style={{ flex: 1 }}>
          <HotelRecentSearchSlider />
          <Typography style={{ marginTop: '16px' }} variant="h5">
            <FormattedMessage id="hotel.popularDestinations" />
          </Typography>
          <PopularDestinationsBox module="hotel" data={dataTmp} />
          <HotelPartner />
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    popularDestinations: state.common.notableLocationsHotel,
  };
}

export default connect(mapStateToProps)(HotelDesktopTablet);
