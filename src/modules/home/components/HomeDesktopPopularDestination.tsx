import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import IconLocation from '../../../svg/ic_home_desktop_location.svg';
import { fetchThunk } from '../../common/redux/thunks';

export interface PopularDestination {
  id: number;
  imageUrl: string;
  name: string;
  highlights: some[];
  bestPrice: number;
  destinationId: number;
  numberTours: number;
  priority: number;
}

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  popularDestinations: PopularDestination[];
}

const ImageBox = styled.div`
  padding-top: 30px;
  position: relative;
  background-size: cover;
`;

function renderDescription(popularDestination: PopularDestination) {
  return (
    <div
      style={{
        position: 'absolute',
        bottom: 0,
        left: 0,
        display: 'flex',
        flexFlow: 'column',
        background: 'rgba(0,0,0,0.3)',
        padding: '0 10px',
        borderRadius: '10px',
        margin: '10px',
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center', paddingBottom: '6px' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <img src={IconLocation} alt="" />
        </div>
        <div style={{ display: 'flex', flexFlow: 'column' }}>
          <Typography
            style={{
              color: '#fff',
            }}
            variant="h6"
          >
            {popularDestination.name}
          </Typography>
        </div>
      </div>

      <div style={{ display: 'flex', flexFlow: 'column', paddingLeft: '40px' }}>
        {popularDestination.highlights.map(item => (
          <Typography
            style={{
              color: '#fff',
              paddingBottom: '12px',
            }}
            variant="body1"
            key={item.id}
          >
            {item.highlight}
          </Typography>
        ))}
      </div>
    </div>
  );
}

class HomeDesktopPopularDestination extends PureComponent<Props, State> {
  state = {
    popularDestinations: [] as PopularDestination[],
  };

  async componentDidMount() {
    const json = await this.props.dispatch(fetchThunk(API_PATHS.popularDestinations));

    if (json && json.data) {
      this.setState({ popularDestinations: json.data });
    }
  }

  render() {
    const { popularDestinations } = this.state;

    return (
      <div
        style={{
          display: 'flex',
          flexFlow: 'column',
          backgroundColor: '#fff',
        }}
      >
        <Container>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Typography
              variant="h4"
              style={{
                paddingTop: '72px',
                paddingBottom: '12px',
                textTransform: 'uppercase',
                fontWeight: 'bold',
              }}
            >
              <FormattedMessage id="home.discoverPopularDistinations" />
            </Typography>
          </div>

          {popularDestinations.length > 4 && (
            <>
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  flexWrap: 'wrap',
                }}
              >
                <div style={{ display: 'flex', flex: 1 }}>
                  <ImageBox
                    style={{
                      display: 'flex',
                      flex: 1,
                      height: '570px',
                    }}
                  >
                    <img
                      style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                      src={popularDestinations[0].imageUrl}
                      alt=""
                    />
                    {renderDescription(popularDestinations[0])}
                  </ImageBox>
                  <div style={{ display: 'flex', flex: 1, marginLeft: '30px' }}>
                    <ImageBox
                      style={{
                        width: '270px',
                        height: '570px',
                        display: 'flex',
                        flex: 1,
                      }}
                    >
                      <img
                        style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                        src={popularDestinations[1].imageUrl}
                        alt=""
                      />
                      {renderDescription(popularDestinations[1])}
                    </ImageBox>

                    <ImageBox
                      style={{
                        marginLeft: '30px',
                        width: '270px',
                        height: '570px',
                        display: 'flex',
                        flex: 1,
                      }}
                    >
                      <img
                        style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                        src={popularDestinations[2].imageUrl}
                        alt=""
                      />
                      {renderDescription(popularDestinations[2])}
                    </ImageBox>
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  flexWrap: 'wrap',
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                  }}
                >
                  <ImageBox
                    style={{
                      height: '270px',
                      display: 'flex',
                      flex: 1,
                    }}
                  >
                    <img
                      style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                      src={popularDestinations[3].imageUrl}
                      alt=""
                    />
                    {renderDescription(popularDestinations[3])}
                  </ImageBox>

                  <ImageBox
                    style={{
                      marginLeft: '30px',
                      height: '270px',
                      display: 'flex',
                      flex: 1,
                    }}
                  >
                    <img
                      style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                      src={popularDestinations[4].imageUrl}
                      alt=""
                    />
                    {renderDescription(popularDestinations[4])}
                  </ImageBox>
                </div>
              </div>
            </>
          )}

          <div style={{ display: 'flex', paddingTop: '70px' }}>
            <div style={{ flex: 1 }}>
              <div style={{ maxWidth: '500px' }}>
                <Typography
                  variant="h4"
                  style={{
                    paddingTop: '24px',
                    textTransform: 'uppercase',
                    fontWeight: 'bold',
                    lineHeight: '150%',
                  }}
                >
                  <FormattedMessage id="home.customerProgram" />
                </Typography>
                <Typography
                  variant="body1"
                  style={{ color: '#757575', paddingTop: '48px', lineHeight: '19px', textAlign: 'justify' }}
                >
                  <FormattedMessage id="home.customerProgramDescription1" />
                </Typography>
                <Typography variant="body1" style={{ color: '#757575', paddingTop: '16px', lineHeight: '19px' }}>
                  <FormattedMessage id="home.customerProgramDescription2" />
                </Typography>
                <Typography variant="body1" style={{ color: '#757575', paddingTop: '16px', lineHeight: '19px' }}>
                  <FormattedMessage id="home.customerProgramDescription3" />
                </Typography>
              </div>
            </div>

            <div style={{ flex: 1 }}>
              <img
                src="https://tripiweb.s3-ap-southeast-1.amazonaws.com/homepage/ic_home_desktop_card_member.svg"
                alt=""
              />
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default connect()(HomeDesktopPopularDestination);
