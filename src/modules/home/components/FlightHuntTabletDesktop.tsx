import * as React from 'react';
import { TABLET_WIDTH } from '../../../constants';
import Header from '../../common/components/Header';
import FlightHuntSearch from '../../search/components/FlightHuntSearch';
import { Container, Typography } from '@material-ui/core';
import Footer from '../../common/components/Footer';
import { PageWrapper } from '../../common/components/elements';
import { FormattedMessage } from 'react-intl';

interface IFlightHuntTabletDesktopProps {}

const FlightHuntTabletDesktop: React.FunctionComponent<IFlightHuntTabletDesktopProps> = props => {
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <FlightHuntSearch home={false} />
      <Container style={{ flex: 1 }}>
        <div style={{ textAlign: 'center', margin: '50px 0' }}>
          <Typography variant="h6">
            <FormattedMessage id="flightHunt.landingMsg" />
          </Typography>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default FlightHuntTabletDesktop;
