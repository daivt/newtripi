import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { ROUTES } from '../../../../constants';
import Link from '../../../common/components/Link';
import { isTermInput, setParams } from '../../../search/redux/tourSearchReducer';
import { parseTourSearchParams } from '../../../search/utils';

interface Props {
  dispatch: Dispatch;
  url: string;
}

const TourRecentCard: React.FunctionComponent<Props> = props => {
  const { url, dispatch } = props;
  const params = parseTourSearchParams(new URLSearchParams(url));
  const getLink = React.useCallback(() => {
    return {
      pathname: `${ROUTES.tour.tourResult}`,
      search: `?${url}`,
    };
  }, [url]);
  return (
    <Link
      to={getLink()}
      onClick={() => dispatch(setParams(params))}
      style={{ marginRight: '24px' }}
      title={isTermInput(params.input) ? params.input.term : params.input.locationName}
    >
      <Button
        variant="outlined"
        style={{ padding: '6px 14px', borderRadius: '50px', width: '142px' }}
      >
        <Typography variant="body2" noWrap={true}>
          {isTermInput(params.input) ? params.input.term : params.input.locationName}
        </Typography>
      </Button>
    </Link>
  );
};

export default connect()(TourRecentCard);
