import { Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../constants';
import styled from 'styled-components';
import { NewTabLink } from '../../../common/components/NewTabLink';
interface Props {
  data: some[];
}
const HEIGHT = '233px';
const ImageBox = styled.div`
  position: relative;
  background-size: cover;
  height: ${HEIGHT};
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 4px;
  overflow: hidden;
`;
const PromotionBox: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <div>
      {data.length > 0 && (
        <div style={{ marginBottom: '24px' }}>
          <Typography variant="h4" style={{ fontSize: 'normal', padding: '8px 0px 16px 0px' }}>
            <FormattedMessage id="tour.promotion" />
          </Typography>
          <Grid container spacing={3}>
            {data.map((v: some, index: number) => (
              <Grid key={index} item xs={4} style={{ cursor: 'pointer' }}>
                <NewTabLink href={v.linkShare}>
                  <ImageBox>
                    <img
                      title={v.shortDescription}
                      style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                      src={v.thumb}
                      alt=""
                    />
                  </ImageBox>
                </NewTabLink>
              </Grid>
            ))}
          </Grid>
        </div>
      )}
    </div>
  );
};

export default PromotionBox;
