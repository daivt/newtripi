import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import iconDelete from '../../../../svg/icon_delete_blue.svg';
import { clearTourSearchHistory, getTourSearchHistory } from '../../../search/utils';
import { slideSettings } from '../common/Slider/setting';
import TourRecentCard from './TourRecentCard';

interface Props {
  dispatch: Dispatch;
}

interface State {
  recentSearchData: string[];
}

class TourFindRecentBox extends PureComponent<Props, State> {
  state: State = {
    recentSearchData: [],
  };

  componentDidMount() {
    const recentSearchData = getTourSearchHistory();
    this.setState({ recentSearchData });
  }

  render() {
    const { recentSearchData } = this.state;

    return (
      <>
        {!!recentSearchData.length && (
          <div style={{ marginTop: '10px', marginBottom: '35px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Typography variant="h5" style={{ padding: '10px 0' }}>
                <FormattedMessage id="home.searchRecent" />
              </Typography>

              <div
                style={{
                  display: 'flex',
                  flex: 1,
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  margin: '12px 0px',
                }}
              >
                <Button
                  style={{ borderRadius: '4px' }}
                  onClick={() => {
                    clearTourSearchHistory();
                    this.setState({ recentSearchData: [] });
                  }}
                >
                  <div style={{ display: 'flex', alignItems: 'center', padding: '3px 6px' }}>
                    <img src={iconDelete} alt="" />
                    <Typography variant="body2" style={{ color: BLUE, paddingLeft: '6px' }}>
                      <FormattedMessage id="home.deleteSearchHistory" />
                    </Typography>
                  </div>
                </Button>
              </div>
            </div>

            <div style={{ margin: '0 36px' }}>
              <Slider {...slideSettings(7)}>
                {recentSearchData.map((item: string, index: number) => (
                  <TourRecentCard url={item} key={index} />
                ))}
              </Slider>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default connect()(TourFindRecentBox);
