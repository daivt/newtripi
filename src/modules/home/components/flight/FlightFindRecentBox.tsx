import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY, HOVER_GREY } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import iconDelete from '../../../../svg/icon_delete_blue.svg';
import IconAirPlaneDown from '../../../../svg/ic_flight_ticket_airplane_down.svg';
import IconAirPlaneUp from '../../../../svg/ic_flight_ticket_airplane_up.svg';
import { RawLink } from '../../../common/components/Link';
import { FlightSearchParamsState, setParams as setFlightSearchParams } from '../../../search/redux/flightSearchReducer';
import {
  addFlightSearchToHistory,
  clearFlightSearchHistory,
  getFlightSearchHistory,
  parseFlightSearchParams,
} from '../../../search/utils';
import { slideSettings } from '../common/Slider/setting';

const BoxItem = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  border-radius: 4px;
  padding: 10px;
  border: 1px solid ${GREY};
  margin: 3px;
  outline: none;
  cursor: pointer;
  :hover {
    background: ${HOVER_GREY};
  }
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

interface State {
  searchHistory: string[];
}

interface PropCustom {
  dispatch: Dispatch;
  search: string;
  index: number;
}

const CustomSlide: React.FC<PropCustom> = props => {
  const { dispatch, search, index } = props;
  const onClick = React.useCallback(
    (params: FlightSearchParamsState) => {
      dispatch(setFlightSearchParams(params));
      addFlightSearchToHistory(params);
    },
    [dispatch],
  );

  const params = parseFlightSearchParams(new URLSearchParams(search));
  return (
    <RawLink to={{ pathname: `${ROUTES.flight.flightResult}`, search: `?${search}` }} onClick={() => onClick(params)}>
      <BoxItem {...props} key={index} style={{}}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '60px',
          }}
        >
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img alt="" style={{ paddingRight: '4px' }} src={IconAirPlaneUp} />
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {params.origin && (
                <>
                  {params.origin.location} ({params.origin.code})
                </>
              )}
            </Typography>
          </div>
          <div style={{ display: 'flex', width: '28px', justifyContent: 'center', flex: 1 }}>
            <div style={{ borderLeft: `1px solid ${BLUE}`, height: '100%' }} />
          </div>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img alt="" style={{ paddingRight: '4px' }} src={IconAirPlaneDown} />
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {params.destination && (
                <>
                  {params.destination.location} ({params.destination.code})
                </>
              )}
            </Typography>
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'center', paddingTop: '5px' }}>
          <Typography noWrap variant="body2" color="textSecondary" style={{ flexShrink: 0 }}>
            {moment(params.departureDate.valueOf()).format('L')}
            {params.returnDate && <>&nbsp;- {moment(params.returnDate.valueOf()).format('L')}</>}
          </Typography>

          <div
            style={{
              width: '4px',
              height: '4px',
              borderRadius: '4px',
              marginLeft: '12px',
              marginRight: '6px',
              background: DARK_GREY,
            }}
          />
          <Typography noWrap variant="body2" color="textSecondary">
            <FormattedMessage
              id="result.totalPassengers"
              values={{
                num:
                  params.travellerCountInfo.adultCount +
                  params.travellerCountInfo.babyCount +
                  params.travellerCountInfo.childCount,
              }}
            />
          </Typography>
        </div>
      </BoxItem>
    </RawLink>
  );
};

class FlightFindRecentBox extends PureComponent<Props, State> {
  state: State = {
    searchHistory: [],
  };

  componentDidMount() {
    const searchHistory = getFlightSearchHistory();
    this.setState({ searchHistory });
  }

  render() {
    const { searchHistory } = this.state;

    if (!searchHistory.length) {
      return <div />;
    }

    return (
      <div style={{ marginBottom: '40px' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography variant="h5" style={{ padding: '10px 0' }}>
            <FormattedMessage id="home.searchRecent" />
          </Typography>

          <div
            style={{
              display: 'flex',
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              margin: '12px 20px',
            }}
          >
            <Button
              style={{ borderRadius: '4px' }}
              onClick={() => {
                clearFlightSearchHistory();
                this.setState({ searchHistory: [] });
              }}
            >
              <div style={{ display: 'flex', alignItems: 'center', padding: '3px 6px' }}>
                <img src={iconDelete} alt="" />
                <Typography variant="body2" style={{ color: BLUE, paddingLeft: '6px' }}>
                  <FormattedMessage id="home.deleteSearchHistory" />
                </Typography>
              </div>
            </Button>
          </div>
        </div>

        <div style={{ margin: '0 36px' }}>
          <Slider {...slideSettings()}>
            {searchHistory.map((item: string, index: number) => (
              <CustomSlide key={index} index={index} search={item} dispatch={this.props.dispatch} />
            ))}
          </Slider>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return { locale: state.intl.locale };
}

export default connect(mapStateToProps)(FlightFindRecentBox);
