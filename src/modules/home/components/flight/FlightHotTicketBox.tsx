import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import Skeleton from '@material-ui/lab/Skeleton';
import { push } from 'connected-react-router';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { GREY, HOVER_GREY, ORANGE } from '../../../../colors';
import { some, ROUTES, DATE_FORMAT, MY_TOUR } from '../../../../constants';
import iconAirplane from '../../../../svg/ic_airplane_horizontal.svg';
import IconBamboo from '../../../../svg/ic_home_desktop_partner_bamboo.svg';
import IconJetstar from '../../../../svg/ic_home_desktop_partner_jetstar.svg';
import IconVietjetAir from '../../../../svg/ic_home_desktop_partner_vietjet_air.svg';
import IconVietnamAirline from '../../../../svg/ic_home_desktop_partner_vietnam_airlines.svg';
import IconQatarAirline from '../../../../svg/ic_qatar_airline.svg';
import IconSingaporeAirline from '../../../../svg/ic_singapore_airline.svg';
import { FlightSearchParamsState, setParams as setFlightSearchParams } from '../../../search/redux/flightSearchReducer';
import { Airport } from '../../../common/models';
import { stringifyFlightSearchStateAndFilterParams, addFlightSearchToHistory } from '../../../search/utils';

const partners = [IconVietnamAirline, IconVietjetAir, IconJetstar, IconBamboo, IconQatarAirline, IconSingaporeAirline];

const TicketItem = styled.div`
  border-top: 1px solid ${GREY};
  display: flex;
  align-items: center;
  min-height: 80px;
  background: #fff;
  padding-right: 20px;
  margin-left: 12px;
  :hover {
    background: ${HOVER_GREY};
  }
`;

function renderHotTicketSkeleton(hasBorder: boolean = false) {
  return (
    <TicketItem style={{ borderBottom: hasBorder ? `1px solid ${GREY}` : undefined }}>
      <div style={{ width: '100px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="60px" height="40px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="130px" height="50px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="130px" height="50px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="180px" height="50px" />
      </div>

      <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end' }}>
        <Skeleton variant="text" width="200px" height="30px" />
      </div>
    </TicketItem>
  );
}

interface Props {
  dispatch: Dispatch;
  hotTickets: some[];
  currentLocation?: some;
  currentDestination?: some;
  loading: boolean;
}

class FlightHotTicketBox extends PureComponent<Props> {
  searchFlight(ticket: some) {
    const { dispatch } = this.props;

    const search: FlightSearchParamsState = {
      origin: {
        code: ticket.fromAirport,
        name: ticket.fromAirportName,
        location: ticket.fromAirportName,
      } as Airport,
      destination: {
        code: ticket.toAirport,
        name: ticket.toAirportName,
        location: ticket.toAirportName,
      } as Airport,
      departureDate: moment(ticket.departureTime, DATE_FORMAT),
      travellerCountInfo: {
        adultCount: 1,
        childCount: 0,
        babyCount: 0,
      },
      seatClass: [],
    };

    const params = stringifyFlightSearchStateAndFilterParams(search);

    addFlightSearchToHistory(search);
    dispatch(setFlightSearchParams(search));
    dispatch(push({ pathname: `${ROUTES.flight.flightResult}`, search: `?${params}` }));
  }

  render() {
    const { hotTickets, currentLocation, currentDestination, loading } = this.props;
    return (
      <div>
        {!loading ? (
          hotTickets.map((ticket: some, index: number) => (
            <TicketItem
              key={ticket.id}
              style={{
                borderBottom: index === hotTickets.length - 1 ? `1px solid ${GREY}` : undefined,
              }}
            >
              {ticket.airlineLogo && (
                <div
                  style={{
                    minWidth: '105px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <img src={ticket.airlineLogo} alt="" style={{ width: '72px' }} />
                </div>
              )}
              <div style={{ width: '180px' }}>
                <Typography variant="body2" style={{ fontWeight: 500 }}>
                  {currentLocation ? currentLocation.name : ''} ({ticket.fromAirport})
                </Typography>
                <Typography variant="caption" color="textSecondary">
                  {ticket.fromAirportName}
                </Typography>
              </div>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingRight: '20px',
                }}
              >
                <img src={iconAirplane} alt="" />
              </div>
              <div style={{ width: '180px' }}>
                <Typography variant="body2" style={{ fontWeight: 500 }}>
                  {currentDestination ? currentDestination.destinationName : ''} ({ticket.toAirport})
                </Typography>
                <Typography variant="caption" color="textSecondary">
                  {ticket.toAirportName}
                </Typography>
              </div>

              <div>
                <Typography variant="body2">
                  <FormattedMessage
                    id="flightHunt.departDate"
                    values={{
                      num: moment(ticket.departureTime, DATE_FORMAT).format('L'),
                    }}
                  />
                </Typography>
                <Typography variant="caption" color="textSecondary">
                  <FormattedMessage id="home.oneWayTicket" />
                </Typography>
              </div>
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <Typography variant="h5" style={{ color: ORANGE }}>
                  <FormattedNumber value={ticket.totalPrice} />
                  &nbsp;
                  <Typography variant="body1" component="span">
                    <FormattedMessage id="currency" />
                  </Typography>
                </Typography>

                <Button
                  style={{
                    marginLeft: '30px',
                  }}
                  color="secondary"
                  size="large"
                  variant="contained"
                  onClick={() => this.searchFlight(ticket)}
                >
                  <Typography variant="button">
                    <FormattedMessage id="home.bookingNow" />
                  </Typography>
                </Button>
              </div>
            </TicketItem>
          ))
        ) : (
          <>
            {renderHotTicketSkeleton()}
            {renderHotTicketSkeleton()}
            {renderHotTicketSkeleton(true)}
          </>
        )}

        <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '20px' }}>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage id="booking.includeTaxesAndFees" />
          </Typography>
        </div>

        <Typography variant="subtitle2" style={{ textAlign: 'center' }}>
          <FormattedMessage id={MY_TOUR ? 'home.mytourPartner' : 'home.tripiPartner'} />
        </Typography>

        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            margin: '20px',
          }}
        >
          {partners.map((item, index: number) => (
            <img src={item} key={index} alt="" />
          ))}
        </div>
      </div>
    );
  }
}

export default connect()(FlightHotTicketBox);
