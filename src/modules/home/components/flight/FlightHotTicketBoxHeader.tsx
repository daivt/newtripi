import { Button } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import { Moment } from 'moment';
import React, { PureComponent } from 'react';
import { FormattedDate, FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import calendarSvg from '../../../../svg/calendar.svg';
import iconRefresh from '../../../../svg/icon_refresh_blue.svg';
import iconPin from '../../../../svg/ic_pin.svg';
import { BootstrapInput } from '../../../common/components/elements';
import AsyncSelect from '../../../search/components/AsyncSelect';

interface Props extends WrappedComponentProps {
  locations: some[];
  currentLocation?: some;
  topDestinations: some[];
  currentDestination?: some;
  month: Moment;
  onSelectCurrentLocation(location: some): void;
  onSelectDestination(data: some): void;
  onRefreshData(): void;
  onSelectMonth(date: Moment): void;
}

interface State {
  months: Moment[];
}

function getLabel(option: some) {
  return `${option.name}`;
}

function renderOptionLocation(option: some) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2">{option.name}</Typography>
    </div>
  );
}

class FlightHotTicketBoxHeader extends PureComponent<Props, State> {
  state: State = {
    months: [],
  };

  componentDidMount() {
    const months = [0, 1, 2].map(num => this.props.month.clone().add(num, 'months'));
    this.setState({ months });
  }

  getLabelMonth = () => {
    const { month } = this.props;
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img alt="" style={{ width: '24px', height: '24px' }} src={calendarSvg} />
        <Typography variant="body2" style={{ paddingLeft: '8px' }}>
          <FormattedDate value={month.toDate()} month="long" />
        </Typography>
      </div>
    );
  };

  getLabelTopDestination = () => {
    const { currentDestination } = this.props;

    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img alt="" style={{ width: '24px', height: '24px' }} src={iconPin} />
        <Typography variant="body2" style={{ paddingLeft: '8px' }}>
          {currentDestination ? currentDestination.destinationName : ''}
        </Typography>
      </div>
    );
  };

  render() {
    const {
      intl,
      locations,
      currentLocation,
      topDestinations,
      currentDestination,
      onSelectCurrentLocation,
      onSelectDestination,
      onRefreshData,
      month,
      onSelectMonth,
    } = this.props;

    const { months } = this.state;

    return (
      <div style={{ display: 'flex' }}>
        <div style={{ minWidth: '250px' }}>
          <AsyncSelect<some>
            key={currentLocation ? currentLocation.name : 'HAN'}
            error={false}
            minimizedWidth="100%"
            loadOptions={async (str: string) => {
              return locations.filter(
                item =>
                  item.name.toLowerCase().indexOf(str.toLocaleLowerCase()) !== -1 ||
                  item.nameNoAccent.toLowerCase().indexOf(str.toLocaleLowerCase()) !== -1,
              );
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconPin} />}
            title={<FormattedMessage id="home.youAreStaying" />}
            optionHeader={<FormattedMessage id="home.selectYourLocation" />}
            focusOptionHeader={<FormattedMessage id="home.selectYourLocation" />}
            placeholder={intl.formatMessage({ id: 'home.locationEx' })}
            defaultValue={currentLocation ? currentLocation.name : ''}
            focusOptions={locations}
            getLabel={getLabel}
            renderOption={renderOptionLocation}
            dropdownMaxHeight="300px"
            onSelect={location => location && onSelectCurrentLocation(location)}
          />
        </div>

        <div
          style={{
            minWidth: '250px',
            margin: '8px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
          }}
        >
          <Typography variant="body2" style={{ paddingBottom: '2px' }}>
            <FormattedMessage id="home.yourDestination" />
          </Typography>

          <Select
            value={topDestinations.findIndex(
              obj => currentDestination && obj.toAirport === currentDestination.toAirport,
            )}
            renderValue={this.getLabelTopDestination}
            onChange={event => onSelectDestination(topDestinations[event.target.value as number])}
            input={<BootstrapInput style={{ background: '#fff', height: '40px' }} />}
            fullWidth
          >
            {topDestinations.map((item, index: number) => (
              <MenuItem key={index} value={index}>
                <Typography variant="body2">{item.destinationName}</Typography>
              </MenuItem>
            ))}
          </Select>
        </div>

        <div
          style={{
            minWidth: '200px',
            margin: '8px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
          }}
        >
          <Typography variant="body2" style={{ paddingBottom: '2px' }}>
            <FormattedMessage id="search.date" />
          </Typography>

          <Select
            value={months.findIndex(obj => obj.month() === month.month())}
            renderValue={this.getLabelMonth}
            onChange={event => onSelectMonth(months[event.target.value as number])}
            input={<BootstrapInput style={{ background: '#fff', height: '40px' }} />}
            fullWidth
          >
            {months.map((item, index: number) => (
              <MenuItem key={index} value={index}>
                <Typography variant="body2">
                  <FormattedDate value={item.toDate()} month="long" />
                </Typography>
              </MenuItem>
            ))}
          </Select>
        </div>

        <div
          style={{
            display: 'flex',
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            margin: '12px 20px',
          }}
        >
          <Button onClick={() => onRefreshData()}>
            <div style={{ display: 'flex', alignItems: 'center', padding: '3px 6px' }}>
              <img src={iconRefresh} alt="" />
              <Typography variant="body2" style={{ color: BLUE, paddingLeft: '6px' }}>
                <FormattedMessage id="home.newestResult" />
              </Typography>
            </div>
          </Button>
        </div>
      </div>
    );
  }
}

export default injectIntl(FlightHotTicketBoxHeader);
