import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { Moment } from 'moment';
import { TABLET_WIDTH, some } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import FlightSearch from '../../../search/components/FlightSearch';
import FlightFindRecentBox from './FlightFindRecentBox';
import FlightHotTicketBox from './FlightHotTicketBox';
import SelectLocationDialog from '../common/SelectLocationDialog';
import FlightHotTicketBoxHeader from './FlightHotTicketBoxHeader';

interface Props {
  hasLocation?: boolean;
  locations: some[];
  currentLocation?: some;
  topDestinations: some[];
  currentDestination?: some;
  hotTickets: some[];
  month: Moment;
  loading: boolean;
  onSelectLocation(location?: some): void;
  onSelectCurrentLocation(location: some): void;
  onSelectDestination(data: some): void;
  onRefreshData(): void;
  onSelectMonth(date: Moment): void;
}

export default class FlightHotTicketTabletDesktop extends PureComponent<Props> {
  render() {
    const {
      hasLocation,
      hotTickets,
      locations,
      currentLocation,
      topDestinations,
      currentDestination,
      onSelectLocation,
      onSelectCurrentLocation,
      onSelectDestination,
      onRefreshData,
      month,
      onSelectMonth,
      loading,
    } = this.props;
    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <FlightSearch />
        <Container style={{ flex: 1 }}>
          {hasLocation ? (
            <div style={{ padding: '22px 0', display: 'flex', flexDirection: 'column' }}>
              <FlightFindRecentBox />
              <Typography variant="h5" style={{ padding: '10px 0' }}>
                <FormattedMessage id="home.bestTicketPrice" />
              </Typography>

              <FlightHotTicketBoxHeader
                locations={locations}
                currentLocation={currentLocation}
                topDestinations={topDestinations}
                currentDestination={currentDestination}
                month={month}
                onSelectCurrentLocation={location => onSelectCurrentLocation(location)}
                onSelectDestination={data => onSelectDestination(data)}
                onRefreshData={onRefreshData}
                onSelectMonth={date => onSelectMonth(date)}
              />

              <FlightHotTicketBox
                hotTickets={hotTickets}
                currentLocation={currentLocation}
                currentDestination={currentDestination}
                loading={loading}
              />
            </div>
          ) : (
            <SelectLocationDialog
              show={!hasLocation}
              onClose={location => onSelectLocation(location)}
              locations={locations}
            />
          )}
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}
