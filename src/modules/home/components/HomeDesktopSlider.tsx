import { ButtonBase } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconNext } from '../../../svg/ic_next.svg';
import { ReactComponent as IconPrev } from '../../../svg/ic_prev.svg';
import ProgressiveImage from '../../common/components/ProgressiveImage';
import { fetchThunk } from '../../common/redux/thunks';
import { SLIDE_HEIGHT } from '../constants';

const Arrow = ({ img, className, style, onClick }: some) => {
  return (
    <div className={className} style={{ ...style }}>
      <ButtonBase onClick={onClick}>{img}</ButtonBase>
    </div>
  );
};

interface IHomeSliderProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  data: some[];
}

class HomeDesktopSlider extends React.PureComponent<IHomeSliderProps, State> {
  state = {
    data: [],
  };

  async componentDidMount() {
    const json = await this.props.dispatch(fetchThunk(API_PATHS.breakingNews));
    this.setState({ data: json.data });
  }

  public render() {
    const { data } = this.state;
    return (
      <div style={{ overflow: 'hidden', height: SLIDE_HEIGHT }}>
        <Slider
          autoplay
          arrows
          dots
          nextArrow={<Arrow img={<IconNext />} />}
          prevArrow={<Arrow img={<IconPrev />} />}
          customPaging={() => (
            <div style={{ padding: '8px 0', background: 'transparent' }}>
              <div
                style={{
                  height: '4px',
                  width: '50px',
                  borderRadius: '2px',
                  borderWidth: '3px',
                }}
              />
            </div>
          )}
        >
          {data &&
            data
              .filter((one: some) => one.active && one.activeDesktop)
              .map((one: some) => (
                <div key={one.thumb}>
                  {one.action === 'link' ? (
                    <a href={one.content}>
                      <ProgressiveImage
                        src={one.thumb}
                        alt=""
                        style={{
                          height: SLIDE_HEIGHT,
                          width: '100%',
                          objectFit: 'cover',
                        }}
                      />
                    </a>
                  ) : (
                    <ProgressiveImage
                      src={one.thumb}
                      alt=""
                      style={{
                        height: SLIDE_HEIGHT,
                        width: '100%',
                        objectFit: 'cover',
                      }}
                    />
                  )}
                </div>
              ))}
        </Slider>
      </div>
    );
  }
}

export default connect()(HomeDesktopSlider);
