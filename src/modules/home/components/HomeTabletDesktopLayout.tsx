import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { TABLET_WIDTH } from '../../../constants';
import Header from '../../common/components/Header';
import HomeDesktopSlider from './HomeDesktopSlider';
import HomeDesktopOurGoal from './HomeDesktopOurGoal';
import HomeDesktopConnectedAgents from './HomeDesktopConnectedAgents';
import HomeDesktopPartner from './HomeDesktopPartner';
import HomeDesktopProduct from './HomeDesktopProduct';
import HomeDesktopPopularDestination from './HomeDesktopPopularDestination';
import HomeDesktopJoinUs from './HomeDesktopJoinUs';
import Tabs from './Tabs';
import Footer from '../../common/components/Footer';
import { AppState } from '../../../redux/reducers';
import { SEARCH_TAB_TOP_HEIGHT } from '../constants';

const CoverContainer = styled.div`
  position: relative;
  overflow: visible;
  text-align: center;
`;

const SearchTabsContainer = styled.div`
  position: absolute;
  top: ${SEARCH_TAB_TOP_HEIGHT}px;
  left: 0;
  right: 0;
  text-align: center;
`;

interface Props extends ReturnType<typeof mapStateToProps> {}

class HomeTabletDesktopLayout extends PureComponent<Props> {
  render() {
    const { auth } = this.props;
    return (
      <div style={{ minWidth: TABLET_WIDTH }}>
        <Header light={false} />
        <CoverContainer>
          <HomeDesktopSlider />
          <SearchTabsContainer>
            <div style={{ margin: 'auto', maxWidth: '1200px' }}>
              <Tabs />
            </div>
          </SearchTabsContainer>
        </CoverContainer>
        {auth ? (
          <>
            <div style={{ height: '70px' }} />
            <HomeDesktopOurGoal />
            <HomeDesktopPartner />
            <HomeDesktopProduct />
            <HomeDesktopPopularDestination />
          </>
        ) : (
          <>
            <HomeDesktopConnectedAgents />
            <HomeDesktopOurGoal />
            <HomeDesktopPartner />
            <HomeDesktopProduct />
            <HomeDesktopPopularDestination />
            <HomeDesktopJoinUs />
          </>
        )}
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return { auth: state.auth.auth };
}

export default connect(mapStateToProps)(HomeTabletDesktopLayout);
