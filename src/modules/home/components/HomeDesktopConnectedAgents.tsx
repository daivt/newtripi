import { Button, Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import IconFlight from '../../../svg/ic_home_desktop_flight.svg';
import IconHotel from '../../../svg/ic_home_desktop_hotel.svg';
import IconPartner from '../../../svg/ic_home_desktop_partner.svg';
import IconRevenue from '../../../svg/ic_home_desktop_revenue.svg';
import IconRevenueIncrease from '../../../svg/ic_home_desktop_revenue_increase.svg';
import IconTourActivity from '../../../svg/ic_home_desktop_tour_activity.svg';
import Link from '../../common/components/Link';
import { ROUTES } from '../../../constants';

const statistics = [
  {
    id: 0,
    icon: IconRevenueIncrease,
    value: (
      <>
        <FormattedNumber value={15} />%
      </>
    ),
    description: 'home.profitIncrease',
  },
  {
    id: 1,
    icon: IconRevenue,
    value: (
      <>
        <FormattedNumber value={15000000} />
        <FormattedMessage id="currency" />
      </>
    ),
    description: 'home.revenueMonthly',
  },
  {
    id: 2,
    icon: IconPartner,
    value: <FormattedNumber value={18358} />,
    description: 'home.partnersAllCountry',
  },
  {
    id: 3,
    icon: IconFlight,
    value: <FormattedNumber value={8500} />,
    description: 'home.flightTotal',
  },
  {
    id: 4,
    icon: IconTourActivity,
    value: <FormattedNumber value={1530} />,
    description: 'home.tourActivity',
  },
  {
    id: 5,
    icon: IconHotel,
    value: <FormattedNumber value={78000} />,
    description: 'home.hotels',
  },
];

class HomeDesktopConnectedAgents extends PureComponent {
  render() {
    return (
      <div
        style={{
          minHeight: '740px',
          backgroundColor: '#fff',
        }}
      >
        <Container>
          <Typography
            variant="h4"
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingTop: '115px',
            }}
          >
            <span style={{ textTransform: 'uppercase', fontWeight: 'bold', textAlign: 'center' }}>
              <FormattedMessage id="home.smartTravelBusiness" />
            </span>
          </Typography>
          <Typography variant="body1" style={{ color: '#757575', textAlign: 'center', padding: '16px 90px' }}>
            <FormattedMessage
              id="home.smartTravelBusinessDescription"
              values={{ number: <FormattedNumber value={18000} /> }}
            />
          </Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingTop: '48px',
              paddingBottom: '44px',
            }}
          >
            <Link to={ROUTES.signUp}>
              <Button
                size="large"
                color="primary"
                variant="contained"
                style={{
                  width: '350px',
                  height: '60px',
                  fontSize: '20px',
                  textTransform: 'uppercase',
                }}
              >
                <FormattedMessage id="home.becomePartner" />
              </Button>
            </Link>
          </div>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'space-evenly',
            }}
          >
            {statistics.map(statistic => (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '80px',
                  minWidth: '355px',
                  padding: '80px 0',
                }}
                key={statistic.id}
              >
                <div style={{ paddingRight: '24px' }}>
                  <img src={statistic.icon} alt="" />
                </div>
                <div style={{ display: 'flex', flexFlow: 'column', justifyContent: 'center' }}>
                  <Typography variant="h4">{statistic.value}</Typography>
                  <Typography variant="body1">
                    <FormattedMessage id={statistic.description} />
                  </Typography>
                </div>
              </div>
            ))}
          </div>
        </Container>
      </div>
    );
  }
}

export default HomeDesktopConnectedAgents;
