import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { HOME_DESKTOP_IMAGE } from '../constants';
import { BLUE } from '../../../colors';

class HomeDesktopProduct extends PureComponent {
  render() {
    return (
      <div style={{ backgroundColor: 'rgba(79, 195, 247, 0.1)' }}>
        <Container>
          <div
            style={{
              display: 'flex',
              paddingTop: '30px',
              justifyContent: 'center',
              flexDirection: 'row',
            }}
          >
            <div style={{ flex: 1, position: 'relative' }}>
              <img
                style={{
                  width: '100%',
                  height: '100%',
                  position: 'absolute',
                  bottom: 0,
                }}
                src={HOME_DESKTOP_IMAGE.productPhoneImage}
                alt=""
              />
            </div>
            <div
              style={{
                paddingTop: '100px',
                paddingLeft: '50px',
                paddingBottom: '76px',
                justifyContent: 'space-between',
                display: 'flex',
                flexFlow: 'column',
                flex: 1,
              }}
            >
              <Typography variant="h4" style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>
                <FormattedMessage id="home.ourProduct" />
              </Typography>
              <div style={{ display: 'flex', paddingTop: '38px' }}>
                <div style={{ width: '100px', height: '100px', position: 'relative' }}>
                  <img
                    style={{
                      height: '100%',
                    }}
                    src={HOME_DESKTOP_IMAGE.PartnerLogo}
                    alt=""
                  />
                </div>
                <div style={{ paddingLeft: '30px' }}>
                  <Typography variant="h5">
                    <FormattedMessage id="home.partner" />
                  </Typography>
                  <Typography
                    variant="body1"
                    style={{ color: '#757575', paddingTop: '12px', lineHeight: '19px' }}
                  >
                    <FormattedMessage id="home.partnerProductDescription" />
                  </Typography>
                  <a
                    href={'https://partner.tripi.vn'}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{
                      textDecoration: 'inherit',
                      color: 'inherit',
                    }}
                  >
                    <Typography
                      variant="body2"
                      style={{ color: BLUE, paddingTop: '12px', lineHeight: '19px' }}
                    >
                      <FormattedMessage id="seeMore" />
                    </Typography>
                  </a>
                </div>
              </div>
              <div style={{ display: 'flex', paddingTop: '38px' }}>
                <div style={{ width: '100px', height: '100px', position: 'relative' }}>
                  <img
                    style={{
                      height: '100%',
                    }}
                    src={HOME_DESKTOP_IMAGE.OliveLogo}
                    alt=""
                  />
                </div>
                <div style={{ paddingLeft: '30px' }}>
                  <Typography variant="h5">
                    <FormattedMessage id="home.olive" />
                  </Typography>
                  <Typography
                    variant="body1"
                    style={{ color: '#757575', paddingTop: '12px', lineHeight: '19px' }}
                  >
                    <FormattedMessage id="home.oliveDescription" />
                  </Typography>
                  <a
                    href={'https://olive.tripi.vn'}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{
                      textDecoration: 'inherit',
                      color: 'inherit',
                    }}
                  >
                    <Typography
                      variant="body2"
                      style={{ color: BLUE, paddingTop: '12px', lineHeight: '19px' }}
                    >
                      <FormattedMessage id="seeMore" />
                    </Typography>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default HomeDesktopProduct;
