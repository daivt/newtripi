import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import IconAgoda from '../../../../svg/ic_home_desktop_partner_agoda.svg';
import IconBooking from '../../../../svg/ic_home_desktop_partner_booking.com.svg';
import IconHotel from '../../../../svg/ic_home_desktop_partner_hotels.com.svg';
import IconOrbitz from '../../../../svg/ic_home_desktop_partner_orbitz.svg';
import IconTravelocity from '../../../../svg/ic_home_desktop_partner_travelocity.svg';

interface Props {}

const partners = [
  {
    id: 0,
    icon: IconAgoda,
  },
  {
    id: 1,
    icon: IconBooking,
  },
  {
    id: 2,
    icon: IconHotel,
  },
  {
    id: 3,
    icon: IconOrbitz,
  },
  {
    id: 4,
    icon: IconTravelocity,
  },
];

const HotelPartner: React.FunctionComponent<Props> = props => {
  return (
    <div>
      <Typography variant="subtitle2" style={{ marginTop: '48px', display: 'flex', justifyContent: 'center' }}>
        <FormattedMessage id="home.partners" />
      </Typography>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginTop: '24px',
          alignItems: 'center',
          marginBottom: '32px',
        }}
      >
        {partners.map(item => (
          <div
            key={item.id}
            style={{
              textAlign: 'center',
            }}
          >
            <img src={item.icon} alt="" />
          </div>
        ))}
      </div>
    </div>
  );
};

export default HotelPartner;
