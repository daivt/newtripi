import { Typography } from '@material-ui/core';
import * as React from 'react';
import styled from 'styled-components';
import { GREY, PRIMARY } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import icDoor from '../../../../svg/ic_door.svg';
import { ReactComponent as IcHotel } from '../../../../svg/ic_hotel.svg';
import { ReactComponent as IcPin } from '../../../../svg/ic_pin.svg';
import icTraveller from '../../../../svg/ic_traveller.svg';
import { HOTEL_BOOK_PARAMS_NAMES, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../../common/constants';
import { parseHotelSearchParams, stringifyHotelSearchState } from '../../../search/utils';

const HoverDiv = styled.div`
  :hover {
    background-color: rgba(0, 0, 0, 0.03);
  }
`;
interface RecentSearchBoxProps {
  data: string;
}

const RecentSearchBox: React.FunctionComponent<RecentSearchBoxProps> = props => {
  const { data } = props;
  const { location, checkIn, checkOut, guestInfo } = parseHotelSearchParams(new URLSearchParams(data));
  const link =
    location.hotelId === -1
      ? `${ROUTES.hotel.hotelResult}?${stringifyHotelSearchState({
          location,
          checkIn,
          checkOut,
          guestInfo,
        })}`
      : `${ROUTES.hotel.hotelDetail}?${stringifyHotelSearchState(
          {
            location,
            checkIn,
            checkOut,
            guestInfo,
          },
          undefined,
          undefined,
          true,
        )}&${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${location.hotelId}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${true}`;
  return (
    <a
      href={link}
      target="_blank"
      rel="noopener noreferrer"
      style={{
        textDecoration: 'inherit',
        color: 'inherit',
      }}
    >
      <HoverDiv
        style={{
          boxSizing: 'border-box',
          border: `1px solid ${GREY}`,
          borderRadius: '4px',
          padding: '8px',
          margin: '0 3px',
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {location.hotelId === -1 ? (
            <IcPin style={{ stroke: PRIMARY, flexShrink: 0 }} className="svgFill" />
          ) : (
            <IcHotel style={{ stroke: PRIMARY, flexShrink: 0 }} className="svgFill" />
          )}
          <Typography variant="subtitle2" noWrap style={{ marginLeft: '16px' }}>
            {location ? location.name : ''}
          </Typography>
        </div>
        <div style={{ marginTop: '16px', display: 'flex' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img src={icDoor} alt="" />
            <Typography variant="body1" style={{ marginLeft: '8px' }}>
              {guestInfo.roomCount}
            </Typography>
          </div>
          <div style={{ display: 'flex', alignItems: 'center', marginLeft: '32px' }}>
            <img src={icTraveller} alt="" />
            <Typography variant="body1" style={{ marginLeft: '8px' }}>
              {guestInfo.adultCount + guestInfo.childCount}
            </Typography>
          </div>
        </div>
        <div style={{ marginTop: '16px' }}>
          <Typography variant="body2">{`${checkIn.format('L')} - ${checkOut.format('L')}`}</Typography>
        </div>
      </HoverDiv>
    </a>
  );
};

export default RecentSearchBox;
