import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { DARK_GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import AsyncSelect from '../../../search/components/AsyncSelect/index';
import { TopDestinations } from '../../constants';

interface Props extends WrappedComponentProps {
  show: boolean;
  onClose(location?: some): void;
  locations: some[];
}

interface State {
  locationSelected?: some;
}

function getLabel(option: some) {
  return `${option.name}`;
}

function renderOptionLocation(option: some) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2">{option.name}</Typography>
    </div>
  );
}

class SelectLocationDialog extends PureComponent<Props, State> {
  state: State = {};

  render() {
    const { show, onClose, locations, intl } = this.props;
    const { locationSelected } = this.state;

    return (
      <Dialog
        open={show}
        PaperProps={{
          style: {
            textAlign: 'center',
            overflowY: 'visible',
          },
        }}
        maxWidth="sm"
      >
        <div style={{ padding: '20px' }}>
          <div style={{ padding: '8px' }}>
            <Typography variant="h5">
              <FormattedMessage id="home.whereAreYou" />
            </Typography>
            <Typography variant="body2" style={{ paddingTop: '8px' }}>
              <FormattedMessage id="home.pleaseProvideYourLocation" />
            </Typography>
          </div>

          <div>
            <AsyncSelect<some>
              error={false}
              minimizedWidth="100%"
              loadOptions={async (str: string) => {
                return locations.filter(
                  item =>
                    item.name.toLowerCase().indexOf(str.toLocaleLowerCase()) !== -1 ||
                    item.nameNoAccent.toLowerCase().indexOf(str.toLocaleLowerCase()) !== -1,
                );
              }}
              icon={<div />}
              optionHeader={<FormattedMessage id="m.setup.choosedAirplane" />}
              focusOptionHeader={<FormattedMessage id="home.selectYourLocation" />}
              placeholder={intl.formatMessage({ id: 'home.locationEx' })}
              defaultValue={locationSelected ? locationSelected.name : ''}
              focusOptions={locations}
              getLabel={getLabel}
              renderOption={renderOptionLocation}
              dropdownMaxHeight="300px"
              onSelect={location => this.setState({ locationSelected: location })}
            />
          </div>

          <div style={{ display: 'flex', justifyContent: 'space-between', padding: '8px' }}>
            {TopDestinations.map(item => {
              const isSelected = locationSelected && item.id === locationSelected.id;
              return (
                <Button
                  key={item.id}
                  variant="outlined"
                  style={{
                    margin: '0 3px',
                    padding: '2px 16px',
                    borderRadius: '32px',
                    backgroundColor: isSelected ? PRIMARY : 'transparent',
                    color: isSelected ? '#fff' : DARK_GREY,
                    borderColor: isSelected ? PRIMARY : undefined,
                  }}
                  size="medium"
                  onClick={() => this.setState({ locationSelected: item })}
                >
                  <Typography variant="body2">{item.name}</Typography>
                </Button>
              );
            })}
          </div>

          <Button
            style={{ marginTop: '32px', marginBottom: '12px', minWidth: '160px' }}
            size="large"
            variant="contained"
            color="secondary"
            onClick={() => onClose(locationSelected)}
          >
            <Typography variant="button">
              <FormattedMessage id="accept" />
            </Typography>
          </Button>
        </div>
      </Dialog>
    );
  }
}

export default injectIntl(SelectLocationDialog);
