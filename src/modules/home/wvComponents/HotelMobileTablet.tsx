import React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../../../redux/reducers';
import HotelSearch from '../../search/wvComponents/HotelSearch';

interface Props extends ReturnType<typeof mapStateToProps> {}

const HotelMobileTablet: React.FC<Props> = props => {
  return (
    <div>
      <HotelSearch />
      {/* <Container style={{ flex: 1 }}>
        <HotelRecentSearchSlider />
        <Typography style={{ marginTop: '16px' }} variant="h5">
          <FormattedMessage id="hotel.popularDestinations" />
        </Typography>
        <PopularDestinationsBox module="hotel" data={popularDestinations} />
        <HotelPartner />
      </Container>
      <Footer /> */}
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    popularDestinations: state.common.notableLocationsHotel,
  };
}

export default connect(mapStateToProps)(HotelMobileTablet);
