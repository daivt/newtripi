import { Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import Header from '../../.../../../common/wvComponents/Header';
import { GREY } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import icPin from '../../../../svg/ic_pin.svg';
import TourSearch from '../../../search/wvComponents/TourSearch';
import PolularDestinationsBox from '../common/PopularDestinationsBox';

interface Props extends ReturnType<typeof mapStateToProps> {}

interface State {}
class TourMobile extends PureComponent<Props, State> {
  render() {
    const { notableLocationsTour } = this.props;

    return (
      <>
        <Header action={<div />} content={<div />} />
        <div
          style={{
            padding: '24px 16px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <TourSearch
            icon={<img src={icPin} alt="" style={{ filter: 'brightness(1) invert(1)' }} />}
            inputStyle={{
              borderBottom: `1px solid ${GREY}`,
              borderRadius: '0px',
              background: 'none',
            }}
          />
          <Typography variant="h6" style={{ marginTop: '24px' }}>
            <FormattedMessage id="hotel.popularDestinations" />
          </Typography>
          <PolularDestinationsBox module={'tour'} data={notableLocationsTour} />
        </div>
      </>
    );
  }
}
const mapStateToProps = (state: AppState) => {
  return {
    notableLocationsTour: state.common.notableLocationsTour,
  };
};
export default connect(mapStateToProps)(TourMobile);
