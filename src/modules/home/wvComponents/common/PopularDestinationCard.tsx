import { Card, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { ROUTES, some, WV_ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import { Location } from '../../../common/models';
import { DEFAULT_HOTEL_FILTER_STATE, DEFAULT_TOUR_FILTER_STATE } from '../../../result/constants';
import { setFilter as setHotelFilter } from '../../../result/redux/hotelResultReducer';
import { setFilter as setTourFilter } from '../../../result/redux/tourResultReducer';
import { getDefaultHotelSearchState, setParams as setHotelParams } from '../../../search/redux/hotelSearchReducer';
import { setParams as setTourParams } from '../../../search/redux/tourSearchReducer';
import { stringifyHotelSearchState, stringifyTourSearchState } from '../../../search/utils';

const HEIGHT = '195px';
const LinkBox = styled(Link)`
  position: relative;
  background-size: cover;
  height: ${HEIGHT};
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 4px;
  overflow: hidden;
  display: block;
`;

const mapStateToProps = (state: AppState) => ({
  hotelSearchState: state.search.hotel,
});

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
  module: string;
  dispatch: Dispatch;
}

const PopularDestinationsCard: React.FunctionComponent<Props> = props => {
  const { data, dispatch, module } = props;
  const location = data as Location;

  const getLink = React.useCallback(() => {
    if (module === 'tour') {
      return {
        pathname: `${WV_ROUTES.tour.tourResultLocation}`,
        search: `?${stringifyTourSearchState({
          input: { locationId: data.locationId, locationName: data.name },
        })}`,
      };
    }
    if (module === 'hotel') {
      const params = stringifyHotelSearchState({ ...getDefaultHotelSearchState(), location });
      return {
        pathname: `${ROUTES.hotel.hotelResult}`,
        search: `?${params}`,
      };
    }
    return {
      pathname: '/',
    };
  }, [module, data, location]);

  const setParamsToStore = React.useCallback(() => {
    if (module === 'tour') {
      dispatch(setTourFilter(DEFAULT_TOUR_FILTER_STATE, true));
      dispatch(setTourParams({ input: { locationId: data.locationId, locationName: data.name } }));
    }
    if (module === 'hotel') {
      dispatch(setHotelFilter(DEFAULT_HOTEL_FILTER_STATE, true));
      dispatch(setHotelParams({ ...getDefaultHotelSearchState(), location }));
    }
  }, [module, data, location, dispatch]);

  if (data.skeleton) {
    return (
      <Card style={{ height: HEIGHT, display: 'flex', flexDirection: 'column' }} elevation={2}>
        <Skeleton height={HEIGHT} variant="rect" />
        <div
          style={{
            position: 'relative',
            bottom: 0,
            left: 0,
            right: 0,
            display: 'flex',
            background: 'white',
            minHeight: '72px',
            alignItems: 'center',
            padding: '8px 16px',
          }}
        >
          <Skeleton variant="text" width="80%" />
        </div>
      </Card>
    );
  }

  return (
    <LinkBox
      to={getLink()}
      onClick={() => {
        setParamsToStore();
      }}
      title={data.name || data.provinceName}
    >
      <ProgressiveImage
        style={{ width: '100%', height: '100%', objectFit: 'cover' }}
        src={data.imageUrl || data.thumb}
        alt=""
      />
      <div
        style={{
          position: 'absolute',
          bottom: 10,
          left: 8,
          right: 0,
          display: 'flex',
        }}
      >
        <Typography
          style={{
            color: '#fff',
            fontWeight: 'normal',
          }}
          variant="h6"
        >
          {data.name || data.provinceName}
        </Typography>
      </div>
    </LinkBox>
  );
};

export default connect(mapStateToProps)(PopularDestinationsCard);
