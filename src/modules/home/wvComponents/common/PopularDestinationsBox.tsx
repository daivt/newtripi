import { Grid } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../constants';
import PopularDestinationsCard from './PopularDestinationCard';

interface Props {
  data: some[];
  module: string;
}

const PolularDestinationsBox: React.FunctionComponent<Props> = props => {
  const { data, module } = props;
  return (
    <div style={{ padding: '16px 0px' }}>
      <Grid container spacing={3}>
        {data.map(v => (
          <Grid
            key={`${v.id}-${v.hotelId}-${v.provinceId}-${v.name}`}
            item
            xs={12}
            sm={6}
            style={{ cursor: 'pointer' }}
          >
            <PopularDestinationsCard module={module} data={v} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default PolularDestinationsBox;
