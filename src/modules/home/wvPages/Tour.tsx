import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import TourMobile from '../wvComponents/tour/TourMobile';
interface Props extends WrappedComponentProps {}

interface State {}
class Tour extends PureComponent<Props, State> {
  render() {
    const { intl } = this.props;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'home.findAndCompareTourBooking' })}</title>
        </Helmet>
        <TourMobile />
      </>
    );
  }
}

export default injectIntl(Tour);
