import * as React from 'react';
import Helmet from 'react-helmet';
import MediaQuery from 'react-responsive';
import { useIntl } from 'react-intl';
import HotelMobileTablet from '../wvComponents/HotelMobileTablet';
import { MIN_TABLET_WIDTH } from '../../../constants';

interface IHotelProps {}

const Hotel: React.FunctionComponent<IHotelProps> = props => {
  const intl = useIntl();
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'home.findAndCompareHotelBooking' })}</title>
      </Helmet>
      <MediaQuery minWidth={MIN_TABLET_WIDTH}>
        {match => {
          if (match) {
            return <HotelMobileTablet />;
          }
          return <HotelMobileTablet />;
        }}
      </MediaQuery>
    </>
  );
};

export default Hotel;
