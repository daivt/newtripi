import React, { PureComponent } from 'react';
import styled from 'styled-components';
import HomeHeader from '../../common/wvComponents/HomeHeader';
import HomeWebViewSlider from '../../common/wvComponents/HomeWebViewSlider';
import Promotion from '../../common/wvComponents/Promotion';
import Tabs from '../../common/wvComponents/Tabs';

const TabsContainer = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  justify-content: center;
  padding: 0 5px;
  width: 100%;
  margin-top: -70px;
  margin-bottom: 10px;
`;

const PromotionContainer = styled.div`
  text-align: center;
`;
class Home extends PureComponent {
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <HomeHeader />
        <div>
          <HomeWebViewSlider />
          <TabsContainer>
            <Tabs />
          </TabsContainer>
        </div>
        <PromotionContainer>
          <Promotion />
        </PromotionContainer>
      </div>
    );
  }
}
export default Home;
