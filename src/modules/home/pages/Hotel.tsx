import React, { PureComponent } from 'react';
import HotelDesktopTablet from '../components/HotelDesktopTablet';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';

interface Props extends WrappedComponentProps {}

class Hotel extends PureComponent<Props> {
  render() {
    const { intl } = this.props;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'home.findAndCompareHotelBooking' })}</title>
        </Helmet>
        <HotelDesktopTablet />
      </>
    );
  }
}

export default injectIntl(Hotel);
