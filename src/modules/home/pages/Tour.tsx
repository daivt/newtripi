import querystring from 'querystring';
import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';
import TourDesktopTablet from '../components/TourDesktopTablet';
interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  promotionData: some[];
}
class Tour extends PureComponent<Props, State> {
  state: State = { promotionData: [] };
  componentDidMount() {
    this.props
      .dispatch(
        fetchThunk(
          `${API_PATHS.getPromotionPrograms}?${querystring.stringify({ module: 'activity' })}`,
          /*module có 3 loại hotel/activity/flight*/
          'post',
          true,
          '{}',
        ),
      )
      .then(json => this.setState({ promotionData: json.data }));
  }
  render() {
    const { intl } = this.props;
    const { promotionData } = this.state;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'home.findAndCompareTourBooking' })}</title>
        </Helmet>
        <TourDesktopTablet promotionData={promotionData} />
      </>
    );
  }
}

export default connect()(injectIntl(Tour));
