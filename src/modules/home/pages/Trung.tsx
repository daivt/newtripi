import { AppBar, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import Tabs from '../components/Tabs';

class Trung extends PureComponent {
  render() {
    return (
      <div>
        <FormattedMessage id="test" />
        <div>
          <AppBar>
            <Typography>abc</Typography>
          </AppBar>
          <Tabs />
        </div>
      </div>
    );
  }
}

export default Trung;
