import React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { isAndroid, isIOS, isMobileOnly } from 'react-device-detect';
import SendHomeReasonDialog from '../../common/components/SendHomeReasonDialog';
import HomeTabletDesktopLayout from '../components/HomeTabletDesktopLayout';
import { WEBVIEW } from '../../../constants';

interface Props extends WrappedComponentProps {}

const Home: React.FC<Props> = props => {
  const { intl } = props;

  if (!WEBVIEW && isMobileOnly) {
    if (isAndroid && isMobileOnly) {
      window.location.replace('https://play.google.com/store/apps/details?id=com.tripi.booker');
    } else if (isIOS && isMobileOnly) {
      window.location.replace('https://apps.apple.com/vn/app/tripi-partner-b2b-travel-deals/id1356994818');
    }
    return <div />;
  }
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'title' })}</title>
      </Helmet>
      <SendHomeReasonDialog />
      <HomeTabletDesktopLayout />
    </>
  );
};

export default injectIntl(Home);
