import * as React from 'react';
import { Helmet } from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import FlightHuntTabletDesktop from '../components/FlightHuntTabletDesktop';

interface IFlightHuntProps extends WrappedComponentProps {}

const FlightHunt: React.FunctionComponent<IFlightHuntProps> = props => {
  const { intl } = props;
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'flightHunt.cheapTicketHunt' })}</title>
      </Helmet>
      <FlightHuntTabletDesktop />
    </>
  );
};

export default injectIntl(FlightHunt);
