import moment, { Moment } from 'moment';
import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { CURRENT_LOCATION, C_DATE_FORMAT, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';
import FlightHotTicketTabletDesktop from '../components/flight/FlightHotTicketTabletDesktop';
import SendHomeReasonDialog from '../../common/components/SendHomeReasonDialog';

interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  hasLocation?: boolean;
  currentLocation?: some;
  locations: some[];
  topDestinations: some[];
  currentDestination?: some;
  hotTickets: some[];
  month: Moment;
  loading: boolean;
}

class Flight extends PureComponent<Props, State> {
  state: State = {
    locations: [],
    hotTickets: [],
    topDestinations: [],
    hasLocation: true,
    month: moment(),
    loading: true,
  };

  async componentDidMount() {
    const currentLocationStr = localStorage.getItem(CURRENT_LOCATION) || false;
    const { dispatch } = this.props;

    const locationsJson = await dispatch(fetchThunk(API_PATHS.getListLocationsInformation));
    this.setState({ locations: locationsJson.data });

    if (!currentLocationStr) {
      this.setState({ hasLocation: false });
      return;
    }

    const currentLocation = JSON.parse(currentLocationStr);
    this.setState({ currentLocation });
    this.fetchTopDestination(currentLocation);
  }

  async onSelectLocation(location?: some) {
    if (!location) {
      return;
    }

    localStorage.setItem(CURRENT_LOCATION, JSON.stringify(location));
    this.setState({ currentLocation: location, hasLocation: true });
    this.fetchTopDestination(location);
  }

  async onSelectDestination(data: some) {
    this.setState({ currentDestination: data });
    this.searchHotTicket(data);
  }

  onRefreshData = () => {
    const { currentDestination, loading } = this.state;

    if (currentDestination && !loading) {
      this.searchHotTicket(currentDestination);
    }
  };

  onSelectMonth(month: Moment) {
    const { currentDestination } = this.state;
    this.setState({ month });
    if (currentDestination) {
      this.searchHotTicket(currentDestination, month);
    }
  }

  async searchHotTicket(selectedLocation: some, date: Moment | undefined = undefined) {
    const { dispatch } = this.props;

    const month = date || this.state.month;
    this.setState({ loading: true });

    const params = {
      fromDate: month.startOf('months').format(C_DATE_FORMAT),
      toDate: month.endOf('months').format(C_DATE_FORMAT),
      fromAirport: selectedLocation.fromAirport,
      toAirport: selectedLocation.toAirport,
      adults: 1,
      children: 0,
      infants: 0,
      groupByDate: false,
    };

    const json = await dispatch(fetchThunk(API_PATHS.searchHotTickets, 'post', false, JSON.stringify(params)));

    if (json.code === 200 || json.code === '200') {
      this.setState({ hotTickets: json.data, loading: false });
      return;
    }

    this.setState({ loading: false });
  }

  async fetchTopDestination(location: some) {
    const { dispatch } = this.props;

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getTopDestinations}?fromAirport=${location.airportCode}&size=5`),
    );

    if (json.code === 200 || json.code === '200') {
      this.setState({ topDestinations: json.data, currentDestination: json.data[0] });
      if (!json.data.length) {
        this.setState({ hotTickets: [] });
        return;
      }

      this.searchHotTicket(json.data[0]);
    }
  }

  render() {
    const {
      hasLocation,
      locations,
      currentLocation,
      topDestinations,
      hotTickets,
      currentDestination,
      month,
      loading,
    } = this.state;

    const { intl } = this.props;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'home.findAndCompareFlightTicket' })}</title>
        </Helmet>
        <SendHomeReasonDialog />
        <FlightHotTicketTabletDesktop
          hasLocation={hasLocation}
          locations={locations}
          currentLocation={currentLocation}
          topDestinations={topDestinations}
          currentDestination={currentDestination}
          hotTickets={hotTickets}
          month={month}
          loading={loading}
          onSelectLocation={location => this.onSelectLocation(location)}
          onSelectCurrentLocation={location => this.onSelectLocation(location)}
          onSelectDestination={data => this.onSelectDestination(data)}
          onRefreshData={this.onRefreshData}
          onSelectMonth={date => this.onSelectMonth(date)}
        />
      </>
    );
  }
}

export default connect()(injectIntl(Flight));
