import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { TABLET_WIDTH, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchThunk } from '../../common/redux/thunks';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import Header from '../../common/components/Header';
import TrainSearch from '../../search/components/TrainSearch';
import { PageWrapper } from '../../common/components/elements';
import { Container } from '@material-ui/core';
import TrainFindRecentBox from '../components/train/TrainFindRecentBox';
interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  hasLocation?: boolean;
  locations: some[];
}

class Train extends PureComponent<Props, State> {
  state: State = {
    locations: [],
    hasLocation: true,
  };

  async componentDidMount() {
    const { dispatch } = this.props;

    const locationsJson = await dispatch(fetchThunk(API_PATHS.getListLocationsInformation));
    this.setState({ locations: locationsJson.data });
  }

  render() {
    const { hasLocation } = this.state;
    const { intl } = this.props;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'train.findTrainTickets' })}</title>
        </Helmet>
        <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
          <Header light={true} />
          <TrainSearch />
          <Container style={{ flex: 1 }}>
            {hasLocation ? (
              <div style={{ padding: '22px 0', display: 'flex', flexDirection: 'column' }}>
                <TrainFindRecentBox />
              </div>
            ) : (
              <div></div>
            )}
          </Container>
        </PageWrapper>
      </>
    );
  }
}

export default connect()(injectIntl(Train));
