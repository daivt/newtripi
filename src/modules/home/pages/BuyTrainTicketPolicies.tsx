import * as React from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { Container } from '@material-ui/core';

interface IBuyTrainTicketPoliciesProps extends WrappedComponentProps {}

const BuyTrainTicketPolicies: React.FunctionComponent<IBuyTrainTicketPoliciesProps> = props => {
  const [html, setHtml] = React.useState('');

  React.useEffect(() => {
    const read = async () => {
      const content = await fetch('/static/train_policy.xml');
      setHtml(await content.text());
    };
    read();
  }, []);

  return (
    <>
      <Container style={{ flex: 1 }}>
        <div style={{ padding: '10px 0' }} dangerouslySetInnerHTML={{ __html: html }}></div>
      </Container>
    </>
  );
};

export default injectIntl(BuyTrainTicketPolicies);
