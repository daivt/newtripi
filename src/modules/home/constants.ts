import PartnerLogo from '../../svg/partner_logo.svg';
import OliveLogo from '../../svg/olive_logo.svg';

export const HOME_DESKTOP_IMAGE = {
  PartnerLogo,
  OliveLogo,
  ourGoalImage: 'https://tripiweb.s3-ap-southeast-1.amazonaws.com/homepage/Rectangle-14.1.png',
  productPhoneImage: 'https://storage.googleapis.com/tripi-assets/images/Mobile_mockup%20(1).png',
  joinUs: 'https://tripiweb.s3-ap-southeast-1.amazonaws.com/homepage/Rectangle-14.3.png',
};

export const TopDestinations = [
  {
    airportCode: 'HAN',
    id: 11,
    latitude: 21.028333,
    longitude: 105.853333,
    name: 'Hà Nội',
    nameNoAccent: 'ha noi',
  },
  {
    airportCode: 'SGN',
    id: 33,
    latitude: 10.769444,
    longitude: 106.681944,
    name: 'Hồ Chí Minh',
    nameNoAccent: 'ho chi minh',
  },
  {
    airportCode: 'DAD',
    id: 50,
    latitude: 16.031944,
    longitude: 108.220556,
    name: 'Đà Nẵng',
    nameNoAccent: 'da nang',
  },
  {
    airportCode: 'HPH',
    id: 3,
    latitude: 20.866389,
    longitude: 106.6825,
    name: 'Hải Phòng',
    nameNoAccent: 'hai phong',
  },
  {
    airportCode: 'VCA',
    id: 38,
    latitude: 10.032415,
    longitude: 105.784092,
    name: 'Cần Thơ',
    nameNoAccent: 'can tho',
  },
];

export const SLIDE_HEIGHT = 450;
export const TAB_SCROLL_TOP_HEIGHT = 350;
export const SEARCH_TAB_TOP_HEIGHT = 400;
