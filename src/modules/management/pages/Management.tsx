import { push } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Dispatch } from 'redux';
import { AppState } from '../../../redux/reducers';
import ManagementTabletDesktop from '../components/ManagementTabletDesktop';
import { CURRENT_PARAM_NAME } from '../constants';

export interface IManagementProps
  extends WrappedComponentProps,
    ReturnType<typeof mapState2Props>,
    RouteComponentProps {
  dispatch: Dispatch;
}

class Management extends React.PureComponent<IManagementProps> {
  componentDidUpdate(prevProps: IManagementProps) {
    if (this.props.location.search !== prevProps.location.search) {
      window.scrollTo(0, 0);
    }
  }

  public render() {
    const { intl, router, dispatch, userData } = this.props;
    const params = new URLSearchParams(router.location.search);
    const current = params.get(CURRENT_PARAM_NAME) || 'm.general';
    return (
      <>
        <Helmet
          titleTemplate={`${intl.formatMessage({ id: 'm.title' })}: ${intl.formatMessage({
            id: current,
          })} - %s`}
          defaultTitle={`${intl.formatMessage({ id: 'm.title' })}: ${intl.formatMessage({
            id: current,
          })}`}
        />
        <ManagementTabletDesktop
          userData={userData}
          current={current}
          changeItem={newCurrent => dispatch(push({ search: `?${CURRENT_PARAM_NAME}=${newCurrent}` }))}
        />
      </>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return { router: state.router, userData: state.account.userData };
};

export default connect(mapState2Props)(injectIntl(withRouter(Management)));
