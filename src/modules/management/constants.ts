import moment from 'moment';
import { Airport, TrainCustomersGroup } from '../common/models';
import { BLACK_TEXT, GREEN, LIGHT_BLUE, RED, SECONDARY, YELLOW } from '../../colors';
import { DATE_FORMAT } from '../../constants';
import { NotableAirports } from './../search/constants';

export const ACTION_PARAM_NAME = 'action';
export const BOOKING_ID_PARAM_NAME = 'bookingId';
export type EXPORT_BILL = 'exportInvoice';
export const CURRENT_PARAM_NAME = 'current';
export const RECHARGE_PARAM_NAME = 'action';
export type ORDER_REQUEST_DETAIL = 'orderRequestDetail';

export type FlightManagementBookingItemActionType =
  | 'detail'
  | 'changeBaggage'
  | 'refund'
  | 'splitCode'
  | 'payBaggage'
  | 'changeItinerary'
  | 'payChangeItinerary'
  | 'payBookingHolding'
  | EXPORT_BILL
  | ORDER_REQUEST_DETAIL;

export type ListOfStaffActionType = 'detail' | 'flight' | 'hotel' | 'activity' | 'train' | 'invite';
export type ListOfCustomerInfomationType = 'add' | 'edit';
export type ListOfCustomerActionType =
  | 'transactionHistory'
  | 'detail'
  | 'delete'
  | ListOfCustomerInfomationType;
export type ProfileInfoActionType = 'smartOTP';

export type Detail = 'detail';
export type ProductType = 'Flight' | 'Hotel' | 'Train' | 'Activity';
export type RefundType = 'outbound' | 'inbound' | 'all';

export function getStatusColor(status: string) {
  if (status === 'success' || status === 'completed') {
    return GREEN;
  }
  if (status === 'fail') {
    return RED;
  }
  if (status === 'waiting') {
    return LIGHT_BLUE;
  }
  if (status === 'holding') {
    return SECONDARY;
  }
  if (status === 'pending') {
    return SECONDARY;
  }
  if (status === 'refunded') {
    return YELLOW;
  }

  return BLACK_TEXT;
}
export const TAB_PARAM_NAME = 'tab';
export const MAX_PERCENT = 1000;
export const MAX_PRICE = 1000000000;
export type PAY_LATER = 'payLater';
export type NotificationActionType = 'detail';
export type HotelManagementBookingItemActionType =
  | 'detail'
  | EXPORT_BILL
  | PAY_LATER
  | ORDER_REQUEST_DETAIL;
export type TourManagementBookingItemActionType = 'detail' | PAY_LATER;
export type AccumulateActionType = 'pointHistory';
export function showMessage(message: string) {}

export const filterByData = [
  {
    id: 0,
    text: 'today',
    fromDate: moment().format(DATE_FORMAT),
    toDate: moment().format(DATE_FORMAT),
  },
  {
    id: 1,
    text: 'm.filterBy.yesterday',
    fromDate: moment()
      .subtract(1, 'days')
      .format(DATE_FORMAT),
    toDate: moment()
      .subtract(1, 'days')
      .format(DATE_FORMAT),
  },
  {
    id: 2,
    text: 'm.filterBy.week',
    fromDate: moment()
      .subtract(6, 'days')
      .format(DATE_FORMAT),
    toDate: moment().format(DATE_FORMAT),
  },
  {
    id: 3,
    text: 'm.filterBy.thisMonth',
    fromDate: moment()
      .startOf('months')
      .format(DATE_FORMAT),
    toDate: moment().format(DATE_FORMAT),
  },
  {
    id: 4,
    text: 'm.filterBy.lastMonth',
    fromDate: moment()
      .subtract(1, 'months')
      .startOf('month')
      .format(DATE_FORMAT),
    toDate: moment()
      .subtract(1, 'months')
      .endOf('month')
      .format(DATE_FORMAT),
  },
  {
    id: 5,
    text: 'm.filterBy.90Days',
    fromDate: moment()
      .subtract(89, 'days')
      .format(DATE_FORMAT),
    toDate: moment().format(DATE_FORMAT),
  },
  {
    id: 6,
    text: 'm.filterBy.allTime',
    fromDate: '01-01-2017',
    toDate: moment().format(DATE_FORMAT),
  },
  {
    id: 7,
    text: 'm.filterBy.custom',
    fromDate: '',
    toDate: '',
  },
];

export const NotableAirportsSetingPrice: Airport[] = [
  { code: 'DOMESTIC', location: 'Nội địa', name: 'Nội địa' },
  { code: 'INTERNATIONAL', location: 'Quốc tế', name: 'Quốc tế' },
  ...NotableAirports,
];

export const AllTrainCustomersGroup: TrainCustomersGroup[] = [
  { customerGroupId: 1, ageGroupName: 'search.trainTravellersInfo.adult', discountPercent: 100 },
  { customerGroupId: 2, ageGroupName: 'search.trainTravellersInfo.children', discountPercent: 75 },
  { customerGroupId: 12, ageGroupName: 'search.trainTravellersInfo.student', discountPercent: 90 },
  { customerGroupId: 13, ageGroupName: 'search.trainTravellersInfo.senior', discountPercent: 85 },
];
