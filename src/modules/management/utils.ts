import { sumBy } from 'lodash';
import { some } from '../../constants';
import { calculateTax } from '../../utils';
import { computePaymentFees } from '../booking/utils';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../common/constants';
import {
  CustomerInfo,
  CustomerInfoValidation,
  ProfileInfo,
  ProfileInfoValidation,
} from '../common/models';
import { validEmailRegex, validTelephoneRegex } from '../common/utils';
import { ChangeItineraryState, FlightOrderState } from './redux/flightOrderReducer';
import { TourOrderState } from './redux/tourOrderReducer';
import { HotelOrderState } from './redux/hotelOrderReducer';

export interface FlightSetupParamsError {
  fromAirportCode: boolean;
  toAirportCode: boolean;
  airlineId: boolean;
  price: boolean;
}
export interface FightSetupData {
  airlineId?: number;
  fromAirportCode?: string;
  id?: number;
  markupAmount?: number;
  markupPercent?: number;
  toAirportCode?: string;
  fromAirportName?: string;
  logoUrl?: string;
  title?: string;
  toAirportName?: string;
}
export interface ExportInvoiceParams {
  readonly benefitPackageSetting?: some;
  readonly companyName?: string;
  readonly companyAddress?: string;
  readonly note?: string;
  readonly recipientAddress?: string;
  readonly recipientEmail?: string;
  readonly recipientName?: string;
  readonly recipientPhone?: string;
  readonly taxIdNumber?: string;
}
export interface ExportInvoiceValidation {
  readonly companyName: boolean;
  readonly companyAddress: boolean;
  readonly recipientAddress: boolean;
  readonly recipientEmail: boolean;
  readonly recipientName: boolean;
  readonly recipientPhone: boolean;
  readonly taxIdNumber: boolean;
}

export function validateExportInvoiceParams(params: ExportInvoiceParams): ExportInvoiceValidation {
  const companyName = params.companyName ? !!params.companyName.trim() : false;
  const companyAddress = params.companyAddress ? !!params.companyAddress.trim() : false;
  const recipientName = params.recipientName ? !!params.recipientName.trim() : false;
  const recipientAddress = params.recipientAddress ? !!params.recipientAddress.trim() : false;
  const recipientEmail = params.recipientEmail
    ? validEmailRegex.test(params.recipientEmail.trim())
    : false;
  const recipientPhone = params.recipientPhone ? !!params.recipientPhone.trim() : false;
  const taxIdNumber = params.taxIdNumber ? !!params.taxIdNumber.trim() : false;

  return {
    companyAddress,
    companyName,
    recipientAddress,
    recipientEmail,
    recipientName,
    recipientPhone,
    taxIdNumber,
  };
}
export function isValidExportInvoice(validation: ExportInvoiceValidation): boolean {
  if (
    validation.companyAddress &&
    validation.companyName &&
    validation.recipientAddress &&
    validation.recipientEmail &&
    validation.recipientName &&
    validation.recipientPhone &&
    validation.taxIdNumber
  ) {
    return true;
  }
  return false;
}
export function validateFlightSetupParams(params: FightSetupData): FlightSetupParamsError {
  let airlineId = false;
  if (!params.title) {
    airlineId = true;
  }
  let fromAirportCode = false;
  if (!params.fromAirportCode) {
    fromAirportCode = true;
  }
  let toAirportCode = false;
  if (!params.toAirportCode) {
    toAirportCode = true;
  }
  if (params.toAirportCode === params.fromAirportCode) {
    toAirportCode = true;
    fromAirportCode = true;
  }
  if (
    (params.toAirportCode === 'DOMESTIC' || params.toAirportCode === 'INTERNATIONAL') &&
    (params.fromAirportCode === 'DOMESTIC' || params.fromAirportCode === 'INTERNATIONAL')
  ) {
    toAirportCode = false;
    fromAirportCode = false;
  }
  if (params.toAirportCode === 'DOMESTIC') {
    toAirportCode = false;
  }

  const price = false;

  return { airlineId, fromAirportCode, toAirportCode, price };
}

export function computeBaggagePayable(oldGuestInfo: some[], newGuestInfo: some[]) {
  const oldPrice =
    sumBy(oldGuestInfo, one => (one.outboundBaggage ? one.outboundBaggage.price : 0)) +
    sumBy(oldGuestInfo, one => (one.inboundBaggage ? one.inboundBaggage.price : 0));
  const newPrice =
    sumBy(newGuestInfo, one => (one.outboundBaggage ? one.outboundBaggage.price : 0)) +
    sumBy(newGuestInfo, one => (one.inboundBaggage ? one.inboundBaggage.price : 0));
  const totalPrice = newPrice - oldPrice;
  const totalTax = calculateTax(totalPrice);

  return {
    totalPrice,
    totalTax,
    totalPriceNoTax: totalPrice - totalTax,
  };
}

export function computeChangeItineraryPayable(changeItineraryData: ChangeItineraryState) {
  const data = changeItineraryData.isInboundSelected
    ? changeItineraryData.inboundChangingPrice
    : changeItineraryData.outboundChangingPrice;

  return {
    changingFee: data ? data.changingFee : 0,
    ticketPriceChangeAmount: data ? data.ticketPriceChangeAmount : 0,
    totalAmount: data ? data.totalAmount : 0,
  };
}

export function computeHoldingBookingPayable(flightOrder: FlightOrderState) {
  const { flightOrderDetail } = flightOrder;

  if (!flightOrderDetail) {
    return {
      originAmount: 0,
      discountAmount: 0,
      pointToAmount: 0,
      paymentFee: 0,
      total: 0,
    };
  }

  const originAmount = flightOrderDetail.grandTotal;

  const discountAmount =
    flightOrder.promotion && flightOrder.promotion.price
      ? originAmount - flightOrder.promotion.price
      : flightOrderDetail.discount;

  const pointToAmount = flightOrder.usePointPayment
    ? flightOrder.pointPaymentData &&
      flightOrder.selectedPaymentMethod &&
      flightOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? flightOrder.pointUsing * flightOrder.pointPaymentData.pointFactor
      : 0
    : 0;

  const total = originAmount - discountAmount;

  const paymentFee = flightOrder.selectedPaymentMethod
    ? computePaymentFees(flightOrder.selectedPaymentMethod, total)
    : 0;

  return {
    originAmount,
    discountAmount,
    pointToAmount,
    paymentFee,
    total,
  };
}

export function validateCustomerInfo(customerInfo: CustomerInfo): CustomerInfoValidation {
  return {
    firstName: !!customerInfo.firstName,
    lastName: !!customerInfo.lastName,
    phone: validTelephoneRegex.test(customerInfo.phone),
  };
}
export function validateProfileInfo(info: ProfileInfo): ProfileInfoValidation {
  return {
    gender: !!info.gender,
    name: !!info.name.trim().length,
    phone: info.phone.trim().length > 4,
    email: validEmailRegex.test(info.email),
    address: !!info.address.trim().length,
    dateOfBirth: true,
    country: true,
  };
}

export function validProfileInfo(validation: ProfileInfoValidation) {
  return (
    validation.gender &&
    validation.name &&
    validation.address &&
    validation.phone &&
    validation.email &&
    validation.dateOfBirth
  );
}

export function getFilteredMenuItems<T extends some>(menuItems: T[], userData?: some) {
  return menuItems.filter(item => {
    if (item.msgId !== 'm.priceSetup' && item.msgId !== 'm.staff') {
      return true;
    }
    if (userData) {
      if (item.msgId === 'm.priceSetup') {
        if (userData.isAgencyManager) {
          return true;
        }
      } else if (item.msgId === 'm.staff') {
        if (userData.isAgencyManager && userData.agencyLevel > 0) {
          return true;
        }
      }
    }
    return false;
  });
}

export function getTicketStatus(data: some, ticket: some) {
  if (ticket.ticketStatus === 'voided') {
    return 'cancelByProvider';
  }

  if (data.paymentStatus === 'fail') {
    return 'm.noBookingCode';
  }

  return '';
}
export function computeTourHoldingBookingPayable(tourOrder: TourOrderState) {
  const originPrice = tourOrder.data
    ? tourOrder.data.finalPrice + (tourOrder.data.discount || 0)
    : 0;

  const pointToAmount = tourOrder.usePointPayment
    ? tourOrder.pointPaymentData &&
      tourOrder.selectedPaymentMethod &&
      tourOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? tourOrder.pointUsing * tourOrder.pointPaymentData.pointFactor
      : 0
    : 0;

  const discountAmount =
    tourOrder.data && tourOrder.data.discount > 0
      ? tourOrder.data.discount
      : tourOrder.promotion && tourOrder.promotion.originPrice && tourOrder.promotion.price
      ? tourOrder.promotion.originPrice - tourOrder.promotion.price
      : 0;
  const finalPrice = originPrice - discountAmount - pointToAmount;
  const paymentFee = tourOrder.selectedPaymentMethod
    ? computePaymentFees(tourOrder.selectedPaymentMethod, finalPrice)
    : 0;

  return {
    originPrice,
    discountAmount,
    pointToAmount,
    paymentFee,
    finalPrice,
  };
}
export function computeHotelHoldingBookingPayable(hotelOrder: HotelOrderState) {
  const originPrice = hotelOrder.data
    ? hotelOrder.data.finalPrice + (hotelOrder.data.discount || 0)
    : 0;

  const pointToAmount = hotelOrder.usePointPayment
    ? hotelOrder.pointPaymentData &&
      hotelOrder.selectedPaymentMethod &&
      hotelOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? hotelOrder.pointUsing * hotelOrder.pointPaymentData.pointFactor
      : 0
    : 0;

  const discountAmount =
    hotelOrder.data && hotelOrder.data.discount > 0
      ? hotelOrder.data.discount
      : hotelOrder.promotion && hotelOrder.promotion.originPrice && hotelOrder.promotion.price
      ? hotelOrder.promotion.originPrice - hotelOrder.promotion.price
      : 0;
  const finalPrice = originPrice - discountAmount - pointToAmount;
  const paymentFee = hotelOrder.selectedPaymentMethod
    ? computePaymentFees(hotelOrder.selectedPaymentMethod, finalPrice)
    : 0;
  return {
    originPrice,
    discountAmount,
    pointToAmount,
    paymentFee,
    finalPrice,
  };
}
