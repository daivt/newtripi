import { replace } from 'connected-react-router';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { computePaymentFees, getPointPaymentData } from '../../booking/utils';
import { fetchThunk } from '../../common/redux/thunks';
import { ACTION_PARAM_NAME, HotelManagementBookingItemActionType } from '../constants';
import { computeHotelHoldingBookingPayable } from '../utils';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../common/constants';

export interface HotelOrderState {
  paymentMethods?: ReadonlyArray<some>;
  selectedPaymentMethod?: Readonly<some>;
  promotionCode: string;
  promotion?: Readonly<some>;
  usePointPayment: boolean;
  pointUsing: number;
  pointPaymentData?: Readonly<some>;
  paying: boolean;
  data?: some;
  payResponseMessage?: string;
}

export const setData = createAction('booking/hotelOrder/setData', resolve => (data: some[]) =>
  resolve({ data }),
);
export const setPaymentMethods = createAction(
  'booking/hotelOrder/setPaymentMethods',
  resolve => (data?: some[]) => resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'booking/hotelOrder/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction(
  'booking/hotelOrder/setPointPaymentData',
  resolve => (data?: some) => resolve({ data }),
);

export const setUsePointPayment = createAction(
  'booking/hotelOrder/setUsePointPayment',
  resolve => (val: boolean) => resolve({ val }),
);

export const setPointUsing = createAction(
  'booking/hotelOrder/setPointUsing',
  resolve => (val: number) => resolve({ val }),
);

export const setPromotionCode = createAction(
  'booking/hotelOrder/setPromotionCode',
  resolve => (data: string) => resolve({ data }),
);

export const setPromotion = createAction(
  'booking/hotelOrder/setPromotion',
  resolve => (data?: some) => resolve({ data }),
);

export const startPaying = createAction('booking/hotelOrder/startPaying');
export const endPaying = createAction('booking/hotelOrder/endPaying');

export const resetBooking = createAction('booking/hotelOrder/resetBooking');

export const setPayResponseMessage = createAction(
  'management/orders/hotel/setPayResponseMessage',
  resolve => (val?: string) => resolve({ val }),
);

export function fetchDataDetail(
  bookingId: number,
  module: string,
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    if (!bookingId) {
      return;
    }
    const json = await dispatch(
      fetchThunk(`${API_PATHS.getHotelBookingDetail}?id=${bookingId}`, 'get', true),
    );
    if (json.code === 200) {
      dispatch(setData(json.data));
      dispatch(fetchPaymentMethods(bookingId, module));
      if (json.data.promotionCode) {
        dispatch(setPromotionCode(json.data.promotionCode));
      }
    }
  };
}
export function fetchPaymentMethods(
  bookingId: number,
  module: string,
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const router = state.router;
    if (!bookingId || !module) {
      return;
    }

    const params = {
      bookingId,
      module,
    };

    const json = await dispatch(
      fetchThunk(API_PATHS.getPaymentMethodOfHoldingBooking, 'post', true, JSON.stringify(params)),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      const paramsUrl = new URLSearchParams(router.location.search);
      paramsUrl.set(ACTION_PARAM_NAME, 'detail' as HotelManagementBookingItemActionType);
      dispatch(replace({ search: `${paramsUrl.toString()}` }));
    }
  };
}

export function fetchPointPayment(): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().management.hotelOrder;
    if (!booking.data) {
      return;
    }
    const payableNumber = computeHotelHoldingBookingPayable(booking);
    const originAmount = payableNumber.originPrice;
    const priceAfter = payableNumber.finalPrice;
    const paymentMethods = getState().management.hotelOrder.paymentMethods;
    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: paymentMethods
            ? paymentMethods.map((v: some) => {
                return {
                  originAmount: originAmount + computePaymentFees(v, originAmount),
                  amount: priceAfter + computePaymentFees(v, originAmount),
                  code: v.code,
                };
              })
            : [],
          module: 'hotel',
          usePromoCode: payableNumber.discountAmount > 0 ? true : false,
        }),
      ),
    );
    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}
export function checkPromotion(code: string): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const hotelOrder = getState().management.hotelOrder;
    const data = hotelOrder.data;
    if (!hotelOrder.paymentMethods || !data) {
      return;
    }
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          departureDate: data.departureDate,
          numAdults: data.numAdults,
          numBabies: data.numBabies,
          numChildren: data.numChildren,
          numInfants: data.numInfants,
          originPrice: data.finalPrice + (data.discount || 0),
          originPoint: 0,
          productType: 'hotel',
          module: 'hotel',
        }),
      ),
    );

    dispatch(setPromotionCode(code));
    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
      dispatch(fetchPointPayment());
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    return json;
  };
}

export function fetchRewardsHistoryHotelOrder(
  search: string = '',
  page: number = 1,
): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const hotelOrder = getState().management.hotelOrder;
    const data = hotelOrder.data;
    if (!hotelOrder.paymentMethods || !data) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search ? search : '',
          info: {
            departureDate: data.departureDate,
            numAdults: data.numAdults,
            numBabies: data.numBabies,
            numChildren: data.numChildren,
            numInfants: data.numInfants,
            originPrice: data.finalPrice + (data.discount || 0),
            productType: 'hotel',
          },
          module: 'hotel',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}

export function hotelPayLater(
  smartOTPPass?: string,
): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const hotelOrder = state.management.hotelOrder;
    const { selectedPaymentMethod, data } = hotelOrder;

    if (!selectedPaymentMethod || !data) {
      return;
    }
    dispatch(startPaying());

    const json = await dispatch(
      fetchThunk(
        API_PATHS.paymentForHoldingBooking,
        'post',
        true,
        JSON.stringify({
          promotionCode: hotelOrder.promotionCode,
          point:
            hotelOrder.usePointPayment && selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
              ? hotelOrder.pointUsing
              : null,
          module: 'hotel',
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          bookingId: data.id,
          creditPassword: smartOTPPass,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );

    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      if (json.data) {
        const paymentLink = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

const actions = {
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  resetBooking,
  setData,
  setPayResponseMessage,
};

type ActionT = ActionType<typeof actions>;

export const DEFAULT_HOTEL_STATE: HotelOrderState = {
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
};

export default function reducer(
  state: HotelOrderState = DEFAULT_HOTEL_STATE,
  action: ActionT,
): HotelOrderState {
  switch (action.type) {
    case getType(resetBooking):
      return DEFAULT_HOTEL_STATE;
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setData):
      return { ...state, data: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    case getType(setPayResponseMessage):
      return { ...state, payResponseMessage: action.payload.val };
    default:
      return state;
  }
}
