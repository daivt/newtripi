import { replace } from 'connected-react-router';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { computePaymentFees, getPointPaymentData } from '../../booking/utils';
import { fetchThunk } from '../../common/redux/thunks';
import { ACTION_PARAM_NAME, TourManagementBookingItemActionType } from '../constants';
import { computeTourHoldingBookingPayable } from '../utils';
import { PAYMENT_TRIPI_CREDIT_CODE } from './../../common/constants';

export interface TourOrderState {
  paymentMethods?: ReadonlyArray<some>;
  selectedPaymentMethod?: Readonly<some>;
  promotionCode: string;
  promotion?: Readonly<some>;
  usePointPayment: boolean;
  pointUsing: number;
  pointPaymentData?: Readonly<some>;
  paying: boolean;
  data?: some;
  payMessage?: string;
}

export const setData = createAction('booking/tourOrder/setData', resolve => (data: some[]) =>
  resolve({ data }),
);
export const setTourPayLaterMessage = createAction(
  'booking/tourOrder/setPayMessage',
  resolve => (data?: string) => resolve({ data }),
);
export const setPaymentMethods = createAction(
  'booking/tourOrder/setPaymentMethods',
  resolve => (data?: some[]) => resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'booking/tourOrder/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction(
  'booking/tourOrder/setPointPaymentData',
  resolve => (data?: some) => resolve({ data }),
);

export const setUsePointPayment = createAction(
  'booking/tourOrder/setUsePointPayment',
  resolve => (val: boolean) => resolve({ val }),
);

export const setPointUsing = createAction(
  'booking/tourOrder/setPointUsing',
  resolve => (val: number) => resolve({ val }),
);

export const setPromotionCode = createAction(
  'booking/tourOrder/setPromotionCode',
  resolve => (data: string) => resolve({ data }),
);

export const setPromotion = createAction(
  'booking/tourOrder/setPromotion',
  resolve => (data?: some) => resolve({ data }),
);

export const startPaying = createAction('booking/tourOrder/startPaying');

export const endPaying = createAction('booking/tourOrder/endPaying');

export const resetBooking = createAction('booking/tourOrder/resetBooking');
export function fetchDataDetail(
  bookingId: number,
  module: string,
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    if (!bookingId) {
      return;
    }
    const json = await dispatch(
      fetchThunk(`${API_PATHS.getTourBookingDetail}?bookingId=${bookingId}`, 'get', true),
    );
    if (json.code === 200) {
      dispatch(setData(json.data));
      dispatch(fetchPaymentMethods(bookingId, module));
      if (json.data.promotionCode) {
        dispatch(setPromotionCode(json.data.promotionCode));
      }
    }
  };
}
export function fetchPaymentMethods(
  bookingId: number,
  module: string,
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const router = state.router;
    if (!bookingId || !module) {
      return;
    }

    const params = {
      bookingId,
      module,
    };

    const json = await dispatch(
      fetchThunk(API_PATHS.getPaymentMethodOfHoldingBooking, 'post', true, JSON.stringify(params)),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      const paramsUrl = new URLSearchParams(router.location.search);
      paramsUrl.set(ACTION_PARAM_NAME, 'detail' as TourManagementBookingItemActionType);
      dispatch(replace({ search: `${paramsUrl.toString()}` }));
    }
  };
}

export function fetchPointPayment(): ThunkAction<
  Promise<some | undefined>,
  AppState,
  null,
  Action<string>
> {
  return async (dispatch, getState) => {
    const booking = getState().management.tourOrder;
    if (!booking.data) {
      return;
    }
    const payableNumber = computeTourHoldingBookingPayable(booking);
    const originAmount = payableNumber.originPrice;
    const priceAfter = payableNumber.finalPrice;
    const paymentMethods = getState().management.tourOrder.paymentMethods;
    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: paymentMethods
            ? paymentMethods.map((v: some) => {
                return {
                  originAmount: originAmount + computePaymentFees(v, originAmount),
                  amount: priceAfter + computePaymentFees(v, originAmount),
                  code: v.code,
                };
              })
            : [],
          module: 'tour',
          usePromoCode: payableNumber.discountAmount > 0 ? true : false,
        }),
      ),
    );
    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}
export function checkPromotion(
  code: string,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const tourOrder = getState().management.tourOrder;
    const data = tourOrder.data;
    if (!tourOrder.paymentMethods || !data) {
      return;
    }
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          departureDate: data.departureDate,
          numAdults: data.numAdults,
          numBabies: data.numBabies,
          numChildren: data.numChildren,
          numInfants: data.numInfants,
          originPrice: data.finalPrice + (data.discount || 0),
          originPoint: 0,
          productType: 'tour',
          module: 'tour',
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));
    return json;
  };
}

export function fetchRewardsHistoryTourOrder(
  search: string = '',
  page: number = 1,
): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const tourOrder = getState().management.tourOrder;
    const data = tourOrder.data;
    if (!tourOrder.paymentMethods || !data) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search ? search : '',
          info: {
            departureDate: data.departureDate,
            numAdults: data.numAdults,
            numBabies: data.numBabies,
            numChildren: data.numChildren,
            numInfants: data.numInfants,
            originPrice: data.finalPrice + (data.discount || 0),
            productType: 'tour',
          },
          module: 'tour',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}
export function tourPayLatterPay(
  smartOTPPass?: string,
): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const tourOrder = getState().management.tourOrder;
    const { selectedPaymentMethod, data } = tourOrder;

    if (!selectedPaymentMethod || !data) {
      return;
    }
    dispatch(startPaying());

    const json = await dispatch(
      fetchThunk(
        API_PATHS.paymentForHoldingBooking,
        'post',
        true,
        JSON.stringify({
          creditPassword: smartOTPPass,
          promotionCode: tourOrder.promotionCode,
          point:
            tourOrder.usePointPayment && selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
              ? tourOrder.pointUsing
              : 0,
          module: 'tour',
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          bookingId: data.id,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );

    if (json.code === 200 || json.code === '200') {
      if (json.data) {
        const paymentLink = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      setTourPayLaterMessage(json.message);
    }
    dispatch(endPaying());
  };
}
const actions = {
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  resetBooking,
  setData,
  setTourPayLaterMessage,
};

type ActionT = ActionType<typeof actions>;

export const DEFAULT_TOUR_STATE: TourOrderState = {
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
};

export default function reducer(
  state: TourOrderState = DEFAULT_TOUR_STATE,
  action: ActionT,
): TourOrderState {
  switch (action.type) {
    case getType(resetBooking):
      return DEFAULT_TOUR_STATE;
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setData):
      return { ...state, data: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(setTourPayLaterMessage):
      return { ...state, payMessage: action.payload.data };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    default:
      return state;
  }
}
