/* eslint-disable consistent-return */
import { replace } from 'connected-react-router';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { computePaymentFees, getPointPaymentData, initPointPayment } from '../../booking/utils';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { fetchThunk } from '../../common/redux/thunks';
import { computeHoldingBookingPayable } from '../utils';

export interface ChangeItineraryState {
  isInboundSelected: boolean;
  inbound?: some;
  outbound?: some;
  inboundChangingPrice?: some;
  outboundChangingPrice?: some;
}

export interface FlightOrderState {
  flightOrderDetail?: some;
  guests: some[];
  refundData?: some;
  splitCodeData?: some;
  changeItineraryData: ChangeItineraryState;
  totalPrice: number;
  paymentMethods?: ReadonlyArray<some>;
  selectedPaymentMethod?: Readonly<some>;
  promotionCode: string;
  promotion?: Readonly<some>;
  usePointPayment: boolean;
  pointUsing: number;
  pointPaymentData?: Readonly<some>;
  paying: boolean;
  payResponseMessage?: string;
}

export const setTotalPrice = createAction('management/orders/flight/setTotalPrice', resolve => (val: number) =>
  resolve({ val }),
);

export const setFlightOrderDetail = createAction(
  'management/orders/flight/setFlightOrderDetail',
  resolve => (data: some) => resolve({ data }),
);

export const setGuests = createAction('management/orders/flight/setGuests', resolve => (data: some[]) =>
  resolve({ data }),
);

export const setRefundData = createAction('management/orders/flight/setRefundData', resolve => (data: some) =>
  resolve({ data }),
);

export const setSplitCodeData = createAction('management/orders/flight/setSplitCodeData', resolve => (data: some) =>
  resolve({ data }),
);

export const setChangeItineraryData = createAction(
  'management/orders/flight/setChangeItineraryData',
  resolve => (data: ChangeItineraryState) => resolve({ data }),
);

export const setPaymentMethods = createAction('management/orders/flight/setPaymentMethods', resolve => (data: some[]) =>
  resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'management/orders/flight/setSelectedPaymentMethod',
  resolve => (data: some) => resolve({ data }),
);

export const setUsePointPayment = createAction(
  'management/orders/flight/setUsePointPayment',
  resolve => (val: boolean) => resolve({ val }),
);

export const setPointUsing = createAction('management/orders/flight/setPointUsing', resolve => (val: number) =>
  resolve({ val }),
);

export const setPromotionCode = createAction('management/orders/flight/setPromotionCode', resolve => (data: string) =>
  resolve({ data }),
);

export const setPromotion = createAction('management/orders/flight/setPromotion', resolve => (data?: some) =>
  resolve({ data }),
);

export const setPointPaymentData = createAction(
  'management/orders/flight/setPointPaymentData',
  resolve => (data?: some) => resolve({ data }),
);

export const startPaying = createAction('management/orders/flight/startPaying');
export const endPaying = createAction('management/orders/flight/endPaying');

export const setPayResponseMessage = createAction(
  'management/orders/flight/setPayResponseMessage',
  resolve => (val?: string) => resolve({ val }),
);

export function fetchPaymentMethodsForAddingBaggages(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { flightOrderDetail } = getState().management.flightOrder;
    if (!flightOrderDetail) {
      return;
    }

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getPaymentMethodsForAddingBaggages}?bookingId=${flightOrderDetail.id}`, 'get', true),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
    }
  };
}

export function fetchPaymentMethodsForChangingItinerary(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { flightOrderDetail } = getState().management.flightOrder;
    if (!flightOrderDetail) {
      return;
    }

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getPaymentMethodsForAddingBaggages}?bookingid=${flightOrderDetail.id}`, 'get', true),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
    }
  };
}

export function fetchPointPaymentForBaggageAndChangeItinerary(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const pointPaymentData = initPointPayment;
    dispatch(setPointUsing(pointPaymentData.min));
    dispatch(setPointPaymentData(pointPaymentData));
  };
}

export function fetchPointPayment(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { flightOrder } = getState().management;
    const { flightOrderDetail, paymentMethods } = flightOrder;

    if (!flightOrderDetail) {
      return;
    }

    const payable = computeHoldingBookingPayable(flightOrder);
    const { originAmount } = payable;
    const amount = payable.total;

    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: paymentMethods
            ? paymentMethods.map(obj => ({
                originAmount: originAmount + computePaymentFees(obj, originAmount),
                amount: amount + computePaymentFees(obj, originAmount),
                code: obj.code,
              }))
            : [],
          module: 'flight',
          usePromoCode: originAmount !== amount,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }
    return json;
  };
}

const checkPromotionOld = (code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> => {
  return async (dispatch, getState) => {
    const { flightOrder } = getState().management;
    const { flightOrderDetail } = flightOrder;

    if (!flightOrderDetail) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          originPrice: flightOrderDetail.finalPrice,
          outboundFarePrice: flightOrderDetail.outbound.farePrice,
          inboundFarePrice: flightOrderDetail.isTwoWay ? flightOrderDetail.inbound.farePrice : undefined,
          numChildren: flightOrderDetail.numChildren,
          numAdults: flightOrderDetail.numAdults,
          numInfants: flightOrderDetail.numInfants,
          productType: 'flight',
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));

    return json;
  };
};

function checkPromotionNew(code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { flightOrder } = getState().management;
    const { flightOrderDetail } = flightOrder;

    if (!flightOrderDetail) {
      return;
    }

    // originPoint: computePoints(booking),
    // data: { ...genFlightPayParams(booking) },
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          module: 'flight',
          originPoint: flightOrderDetail.bonusPoint,
          bookingId: flightOrderDetail.id,
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}

// export const checkPromotion = DEV || TEST ? checkPromotionNew : checkPromotionOld;
export const checkPromotion = checkPromotionNew;

export function fetchPaymentMethodsForHoldingBooking(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { flightOrderDetail } = getState().management.flightOrder;
    if (!flightOrderDetail) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.getPaymentMethodOfHoldingBooking,
        'post',
        true,
        JSON.stringify({
          module: 'flight',
          bookingId: flightOrderDetail.id,
        }),
      ),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
    }
  };
}

export function flightPayHolding(smartOTPPass?: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { flightOrder } = state.management;
    const { selectedPaymentMethod, flightOrderDetail } = flightOrder;

    if (!selectedPaymentMethod || !flightOrderDetail) {
      return;
    }

    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.paymentForHoldingBooking,
        'post',
        true,
        JSON.stringify({
          creditPassword: smartOTPPass,
          promotionCode: flightOrder.promotionCode,
          point: flightOrder.usePointPayment ? flightOrder.pointUsing : 0,
          module: 'flight',
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          bookingId: flightOrderDetail.id,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );

    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      if (json.data) {
        const paymentLink = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

export function flightBaggagesPay(smartOTPPass?: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { flightOrder } = state.management;
    const { selectedPaymentMethod, flightOrderDetail } = flightOrder;

    if (!selectedPaymentMethod || !flightOrderDetail) {
      return;
    }

    const guests = flightOrder.guests.map((guest: some) => ({
      guestId: guest.id,
      outboundBaggageId: guest.outboundBaggage ? guest.outboundBaggage.id : null,
      inboundBaggageId: guest.inboundBaggage ? guest.inboundBaggage.id : null,
    }));

    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.addBaggagesForFlightBooking,
        'post',
        true,
        JSON.stringify({
          guests,
          creditPassword: smartOTPPass,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          bookingId: flightOrderDetail.id,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200) {
      if (json.data.paymentLink) {
        const { paymentLink } = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

export function flightItineraryChangingPay(
  smartOTPPass?: string,
): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { flightOrder } = state.management;
    const { selectedPaymentMethod, flightOrderDetail, changeItineraryData } = flightOrder;

    if (!selectedPaymentMethod || !flightOrderDetail || !changeItineraryData) {
      return;
    }

    const tickets = {
      outbound: changeItineraryData.outbound
        ? {
            agencyId: changeItineraryData.outbound.aid,
            requestId: changeItineraryData.outbound.requestId,
            ticketId: changeItineraryData.outbound.ticketId,
          }
        : undefined,
      inbound: changeItineraryData.inbound
        ? {
            agencyId: changeItineraryData.inbound.aid,
            requestId: changeItineraryData.inbound.requestId,
            ticketId: changeItineraryData.inbound.ticketId,
          }
        : undefined,
    };

    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.changeFlightItinerary,
        'post',
        true,
        JSON.stringify({
          tickets,
          creditPassword: smartOTPPass,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          bookingId: flightOrderDetail.id,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200) {
      if (json.data.paymentLink) {
        const { paymentLink } = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

const actions = {
  setTotalPrice,
  setFlightOrderDetail,
  setGuests,
  setRefundData,
  setSplitCodeData,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setChangeItineraryData,
  setPointPaymentData,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  setPayResponseMessage,
};

type ActionT = ActionType<typeof actions>;

export const DEFAULT_FLIGHT_STATE: FlightOrderState = {
  changeItineraryData: { isInboundSelected: false },
  guests: [],
  totalPrice: 0,
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
};

export default function reducer(state: FlightOrderState = DEFAULT_FLIGHT_STATE, action: ActionT): FlightOrderState {
  switch (action.type) {
    case getType(setTotalPrice):
      return { ...state, totalPrice: action.payload.val };
    case getType(setFlightOrderDetail):
      return { ...DEFAULT_FLIGHT_STATE, flightOrderDetail: action.payload.data };
    case getType(setGuests):
      return { ...state, guests: action.payload.data };
    case getType(setRefundData):
      return { ...state, refundData: action.payload.data };
    case getType(setSplitCodeData):
      return { ...state, splitCodeData: action.payload.data };
    case getType(setChangeItineraryData):
      return { ...state, changeItineraryData: action.payload.data };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setPayResponseMessage):
      return { ...state, payResponseMessage: action.payload.val };
    default:
      return state;
  }
}
