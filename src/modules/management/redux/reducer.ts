import flightOrderReducer, { FlightOrderState, DEFAULT_FLIGHT_STATE } from './flightOrderReducer';
import { combineReducers } from 'redux';
import tourOrderReducer, { TourOrderState, DEFAULT_TOUR_STATE } from './tourOrderReducer';
import hotelOrderReducer, { HotelOrderState, DEFAULT_HOTEL_STATE } from './hotelOrderReducer';

export interface ManagementState {
  flightOrder: FlightOrderState;
  hotelOrder: HotelOrderState;
  tourOrder: TourOrderState;
}

export const DEFAULT_MANAGEMENT_STATE: ManagementState = {
  flightOrder: DEFAULT_FLIGHT_STATE,
  hotelOrder: DEFAULT_HOTEL_STATE,
  tourOrder: DEFAULT_TOUR_STATE,
};

export default combineReducers({
  flightOrder: flightOrderReducer,
  hotelOrder: hotelOrderReducer,
  tourOrder: tourOrderReducer,
});
