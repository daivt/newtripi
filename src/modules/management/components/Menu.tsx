import { ButtonBase, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREEN, LIGHT_GREY } from '../../../colors';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import notificationIcon from '../../../svg/bell_red.svg';
import loyaltyIcon from '../../../svg/ic_sidebar_loyalty.svg';
import billIcon from '../../../svg/mng_bill.svg';
import customerIcon from '../../../svg/mng_customer.svg';
import generalIcon from '../../../svg/mng_general.svg';
import logoutIcon from '../../../svg/mng_logout.svg';
import orderIcon from '../../../svg/mng_order.svg';
import peopleIcon from '../../../svg/mng_people.svg';
import priceSetupIcon from '../../../svg/mng_price_setup.svg';
import profileIcon from '../../../svg/mng_profile.svg';
import { logout } from '../../auth/redux/authThunks';
import { Line } from '../../result/components/hotel/HotelDetailTabs/styles';
import { getFilteredMenuItems } from '../utils';
import Accumulate from './Accumulate';
import Customer from './Customer';
import General from './General';
import Invoice from './Invoice';
import Notification from './Notification';
import Orders from './Orders';
import PriceSetup from './PriceSetup';
import Profile from './Profile';
import Staff from './Staff';

export const MENU_ITEMS = [
  { icon: generalIcon, msgId: 'm.general', Content: General },
  { icon: notificationIcon, msgId: 'm.notification', Content: Notification },
  { icon: orderIcon, msgId: 'm.orders', Content: Orders },
  { icon: priceSetupIcon, msgId: 'm.priceSetup', Content: PriceSetup },
  { icon: billIcon, msgId: 'm.invoiceManagement', Content: Invoice },
  { icon: loyaltyIcon, msgId: 'm.accumulate', Content: Accumulate },
  { icon: customerIcon, msgId: 'm.customers', Content: Customer },
  { icon: peopleIcon, msgId: 'm.staff', Content: Staff },
  { icon: profileIcon, msgId: 'm.profile', Content: Profile },
];

function mapState2Props(state: AppState) {
  return {
    userData: state.account.userData,
  };
}
interface IMenuProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  currentId: string;
  onClick(current: string): void;
}

const Menu: React.FunctionComponent<IMenuProps> = props => {
  const { dispatch, currentId, onClick, userData } = props;
  return (
    <>
      {getFilteredMenuItems(MENU_ITEMS, userData).map((one, i) => (
        <ButtonBase
          disabled={one.msgId === currentId}
          key={one.msgId}
          onClick={() => onClick(one.msgId)}
          style={{ background: currentId === one.msgId ? LIGHT_GREY : undefined }}
        >
          <MediaQuery minWidth={DESKTOP_WIDTH}>
            {match => {
              if (match) {
                return (
                  <Line
                    style={{
                      height: '48px',
                      padding: '0 31px',
                      display: 'flex',
                      justifyContent: 'flex-start',
                      width: '100%',
                      minWidth: '270px',
                    }}
                  >
                    <img alt="" src={one.icon} style={{ marginRight: '12px', width: '25px', height: '25px' }} />
                    <Typography color={one.msgId === currentId ? 'textPrimary' : 'textSecondary'}>
                      <FormattedMessage id={one.msgId} />
                    </Typography>
                    {one.msgId === 'm.myReward' && userData && (
                      <div
                        style={{
                          display: 'flex',
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'flex-end',
                        }}
                      >
                        <Typography
                          variant="body2"
                          style={{
                            color: '#fff',
                            width: '28px',
                            height: '28px',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: '50%',
                            backgroundColor: GREEN,
                          }}
                        >
                          {userData.loyaltyInfo.rewardNum <= 99 ? userData.loyaltyInfo.rewardNum : '99+'}
                        </Typography>
                      </div>
                    )}
                  </Line>
                );
              }
              return (
                <Line
                  style={{
                    height: '48px',
                    padding: '0 15px',
                    display: 'flex',
                    justifyContent: 'flex-start',
                  }}
                >
                  <img alt="" src={one.icon} style={{ width: '25px', height: '25px' }} />
                </Line>
              );
            }}
          </MediaQuery>
        </ButtonBase>
      ))}
      <div
        style={{
          display: 'flex',
          alignItems: 'flex-end',
          justifyContent: 'center',
          flex: 1,
          width: '100%',
        }}
      >
        <ButtonBase
          style={{
            width: '100%',
          }}
          onClick={() => {
            dispatch(logout());
          }}
        >
          <MediaQuery minWidth={DESKTOP_WIDTH}>
            {match => {
              if (match) {
                return (
                  <Line
                    style={{
                      height: '48px',
                      padding: '0 31px',
                    }}
                  >
                    <img alt="" src={logoutIcon} style={{ marginRight: '12px', width: '25px', height: '25px' }} />
                    <Typography>
                      <FormattedMessage id="signOut" />
                    </Typography>
                  </Line>
                );
              }
              return (
                <Line
                  style={{
                    height: '48px',
                    padding: '0 15px',
                  }}
                >
                  <img alt="" src={logoutIcon} style={{ width: '25px', height: '25px' }} />
                </Line>
              );
            }}
          </MediaQuery>
        </ButtonBase>
      </div>
    </>
  );
};

export default connect(mapState2Props)(Menu);
