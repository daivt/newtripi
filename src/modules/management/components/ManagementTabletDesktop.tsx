import { Container } from '@material-ui/core';
import * as React from 'react';
import { LIGHT_GREY } from '../../../colors';
import { some, TABLET_WIDTH } from '../../../constants';
import { PageWrapper } from '../../common/components/elements';
import Footer from '../../common/components/Footer';
import Header from '../../common/components/Header';
import { getFilteredMenuItems } from '../utils';
import Menu, { MENU_ITEMS } from './Menu';

interface Props {
  userData?: some;
  current: string;
  changeItem(current: string): void;
}

const ManagementTabletDesktop: React.FunctionComponent<Props> = props => {
  const { current, changeItem, userData } = props;

  const currentItem = getFilteredMenuItems(MENU_ITEMS, userData).find(item => item.msgId === current) || MENU_ITEMS[0];

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1, display: 'flex', padding: 0 }}>
        <div style={{ borderRight: `2px solid ${LIGHT_GREY}`, background: 'white' }}>
          <div
            style={{
              position: 'sticky',
              top: 67,
              alignSelf: 'flex-start',
              paddingTop: '30px',
              paddingBottom: '10px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              minHeight: 'calc(100vh - 85px)',
            }}
          >
            <Menu currentId={current} onClick={clicked => changeItem(clicked)} />
          </div>
        </div>
        <div style={{ flex: 1 }}>
          <currentItem.Content />
        </div>
      </Container>
      <Footer compact />
    </PageWrapper>
  );
};

export default ManagementTabletDesktop;
