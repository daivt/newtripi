import * as React from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../constants';
import AccumulateInfo from './AccumulateInfo';
import PointHistory from './PointHistory';

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
    data: state.account.userData,
  };
}

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: Dispatch;
}

const Accumulate: React.FunctionComponent<Props> = props => {
  const { search } = props;
  const params = new URLSearchParams(search);
  const action = params.get(ACTION_PARAM_NAME);

  return (
    <>
      <div style={{ padding: '30px 30px', display: action ? 'none' : undefined }}>
        <AccumulateInfo />
      </div>
      {action === 'pointHistory' && <PointHistory />}
    </>
  );
};

export default connect(mapState2Props)(injectIntl(Accumulate));
