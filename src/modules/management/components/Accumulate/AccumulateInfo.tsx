import { Button, Tab, Tabs, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { GREY, PRIMARY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icDocument from '../../../../svg/ic_document.svg';
import icGift from '../../../../svg/mng_gift.svg';
import { Line } from '../../../common/components/elements';
import Link from '../../../common/components/Link';
import LoadingIcon from '../../../common/components/LoadingIcon';
import ProgressiveImage from '../../../common/components/ProgressiveImage';
import { AccumulateActionType, ACTION_PARAM_NAME } from '../../constants';
import { ROUTES } from './../../../../constants';
import { fetchThunk } from './../../../common/redux/thunks';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const seeHistory = (search: string) => {
  const params = new URLSearchParams(search);
  params.set(ACTION_PARAM_NAME, 'pointHistory' as AccumulateActionType);
  return params.toString();
};

const AccumulateInfo: React.FunctionComponent<Props> = props => {
  const { dispatch, search, userData } = props;
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [tab, setTab] = React.useState(0);

  React.useEffect(() => {
    const fetch = async () => {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getMyLoyaltyInfo}`, 'post', true, JSON.stringify({})),
      );
      if (json.code === 200) {
        setData(json.data);
        setTab(json.data.title.no);
      }
    };
    fetch();
  }, [dispatch]);

  if (!data) {
    return <LoadingIcon />;
  }

  return (
    <div>
      <Line style={{ justifyContent: 'space-between' }}>
        <Typography variant="h5">
          <FormattedMessage id="m.accumulate" />
        </Typography>
        <Link
          to={{
            pathname: ROUTES.management,
            search: seeHistory(search),
            state: { backableToAccumulateInfo: true },
          }}
        >
          <Button variant="contained" color="secondary" size="large">
            <Typography variant="body1">
              <FormattedMessage id="m.accumulate.history" />
            </Typography>
          </Button>
        </Link>
      </Line>
      <div
        style={{ marginTop: '20px', marginBottom: '28px', width: '620px', wordBreak: 'break-word' }}
      >
        <Typography variant="body2" style={{ marginTop: '16px' }}>
          <FormattedMessage id="m.accumulate.note" />
        </Typography>
      </div>
      <Line style={{ marginLeft: '16px', alignItems: 'flex-start' }}>
        <div style={{ position: 'relative', padding: '12px', width: '344px' }}>
          <ProgressiveImage
            src={data.title.cardBackground}
            alt=""
            style={{ position: 'absolute', top: 0, left: 0, zIndex: -1, width: '344px' }}
          />
          <Line>
            {userData && userData.profilePhoto ? (
              <img
                src={userData.profilePhoto}
                alt=""
                style={{ borderRadius: '50%', height: '40px', width: '40px', objectFit: 'cover' }}
              />
            ) : (
              <div
                style={{
                  background: GREY,
                  borderRadius: '50%',
                  height: '40px',
                  width: '40px',
                  textAlign: 'center',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                }}
              >
                <span>
                  {data.name &&
                    data.name
                      .split(' ')
                      .map((v: string) => v.substring(0, 1).toUpperCase())
                      .join('')}
                </span>
              </div>
            )}
            <Typography variant="body1" style={{ marginLeft: '8px', color: 'white' }}>
              {data.name}
            </Typography>
          </Line>
          <Line style={{ justifyContent: 'center' }}>
            <Typography
              variant="h5"
              style={{ marginTop: '20px', color: 'white', fontWeight: 'normal' }}
            >
              {data.title.card.formattedCardNumber}
            </Typography>
          </Line>
        </div>
        <div style={{ marginLeft: '40px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Typography variant="body1">
              <FormattedMessage id="m.accumulate.currentRank" />
              :&nbsp;
              {data.title.name}
            </Typography>
            <img src={data.title.icon} alt="" style={{ marginLeft: '12px', height: '24px' }} />
          </div>
          <Typography variant="body1" style={{ marginTop: '8px' }}>
            <FormattedMessage id="m.accumulate.pendingPoint" />
            :&nbsp;
            <FormattedNumber value={data.pendingPoint} />
            &nbsp;
            <FormattedMessage id="point" />
          </Typography>
          <div style={{ display: 'flex', marginTop: '8px' }}>
            <Typography variant="body1">
              <FormattedMessage id="account.pointCanUse" />
              :&nbsp;
            </Typography>
            <Typography variant="body1" style={{ color: PRIMARY }}>
              <FormattedNumber value={data.point} />
              &nbsp;
              <FormattedMessage id="point" />
            </Typography>
          </div>
          {data.ranks[data.title.no + 1] && (
            <Typography variant="body2" style={{ color: GREY, marginTop: '8px' }}>
              <FormattedMessage
                id="m.accumulate.nextRank"
                values={{ point: data.remainingPoint, rank: data.ranks[data.title.no + 1].name }}
              />
            </Typography>
          )}
          <div style={{ display: 'flex', marginTop: '30px' }}>
            <div style={{ display: 'flex' }}>
              <Link to={{ pathname: ROUTES.reward.myRewards }}>
                <Button
                  variant="outlined"
                  size="large"
                  style={{
                    display: 'flex',
                    minWidth: '200px',
                    borderRadius: '99px',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <img src={icGift} alt="" />
                  <Typography variant="button" color="textSecondary">
                    <FormattedMessage id="m.myReward" />
                  </Typography>
                </Button>
              </Link>
              <Link to={{ pathname: ROUTES.reward.rewards }}>
                <Button
                  variant="outlined"
                  size="large"
                  style={{
                    marginLeft: '20px',
                    display: 'flex',
                    minWidth: '200px',
                    borderRadius: '99px',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <img src={icDocument} alt="" />
                  <Typography variant="button" color="textSecondary">
                    <FormattedMessage id="m.listReward" />
                  </Typography>
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </Line>
      <div>
        <Tabs
          value={tab}
          onChange={(event, newValue) => setTab(newValue)}
          variant="fullWidth"
          scrollButtons="on"
          aria-label="icon label tabs"
          style={{ marginTop: '100px', borderBottom: `1px solid ${GREY}` }}
          TabIndicatorProps={{ style: { background: data.ranks[tab].color } }}
        >
          {data.ranks.map((item: some) => (
            <Tab
              key={item.id}
              label={item.name}
              icon={<img style={{ width: '36px', height: '36px' }} src={item.icon} alt="" />}
            />
          ))}
        </Tabs>
        <div style={{ width: '100%', marginTop: '20px' }}>
          {data.ranks[tab].webDescription.map((item: any) => (
            <Typography key={item} variant="body2">
              -&nbsp;{item}
            </Typography>
          ))}
        </div>
      </div>
    </div>
  );
};
function mapStateToProps(state: AppState) {
  return {
    search: state.router.location.search,
    userData: state.account.userData,
  };
}
export default connect(mapStateToProps)(AccumulateInfo);
