import * as React from 'react';
import { Typography, Breadcrumbs } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { FormattedMessage } from 'react-intl';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { goBack, replace } from 'connected-react-router';
import { ACTION_PARAM_NAME } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const AccumulateBreadcrumb: React.FunctionComponent<Props> = props => {
  const { state, dispatch, router } = props;
  const backToEmployeeList = () => {
    const backableToList = state && state.backableToAccumulateInfo;

    if (backableToList) {
      dispatch(goBack());
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  };
  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => backToEmployeeList()}
        >
          <FormattedMessage id="m.accumulate" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.accumulate.pointHistory" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state, router: state.router };
}

export default connect(mapStateToProps)(AccumulateBreadcrumb);
