import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREY, HOVER_GREY, PRIMARY, SECONDARY } from '../../../../colors';
import { some } from '../../../../constants';

const ItemDiv = styled.div`
  width: 100%;
  padding: 14px 10px;
  border-bottom: 1px solid ${GREY};
  :hover {
    background: ${HOVER_GREY};
  }
`;

interface Props {
  info: some;
}

const PointInfoCard: React.FunctionComponent<Props> = props => {
  const { info } = props;
  return (
    <ItemDiv>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant="body2" style={{ fontWeight: 'bold' }}>
          {info.title}
        </Typography>
        <Typography
          variant="body2"
          style={{ marginRight: '40px', color: info.isUsed ? SECONDARY : PRIMARY }}
        >
          {info.isUsed ? '-' : '+'} <FormattedNumber value={info.point} />
          &nbsp;
          <FormattedMessage id="point" />
        </Typography>
      </div>
      <Typography variant="body2" style={{ color: GREY }}>
        <FormattedMessage id="m.bookingDate" />
        :&nbsp;
        {info.time}
      </Typography>
    </ItemDiv>
  );
};

export default PointInfoCard;
