import { Button, MenuItem, Select, Tab, Tabs, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE, GREY } from '../../../../colors';
import { some, PAGE_SIZE, DATE_FORMAT, ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { BootstrapInput } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../common/redux/thunks';
import { filterByData } from '../../constants';
import ChooseDateDialog from '../common/ChooseDateDialog';
import AccumulateBreadcrumb from './AccumulateBreadcrumb';
import PointInfoCard from './PointInfoCard';
import Helmet from 'react-helmet';
import { ReactComponent as IconNoHistories } from '../../../../svg/ic_contact_no_result.svg';
import { RawLink } from '../../../common/components/Link';

const pointState = [null, false, true];

const getPointMessage = (tab: number) => {
  switch (tab) {
    case 0:
      return 'm.accumulate.totalPoint';
    case 1:
      return 'm.accumulate.totalPointBonus';
    case 2:
      return 'm.accumulate.totalPointUsed';
    default:
      return 'm.accumulate.totalPoint';
  }
};

interface PointHistoryFilterParams {
  size: number;
  isUsed: boolean | null;
  startDate: string;
  endDate: string;
}

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const PointHistory: React.FunctionComponent<Props> = props => {
  const { dispatch, intl } = props;
  const [sortBy, setSortBy] = React.useState(5);
  const [openChooseDate, setOpenChooseDate] = React.useState(false);
  const [page, setPage] = React.useState(1);
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [tab, setTab] = React.useState(0);
  const [totalPoint, setTotalPoint] = React.useState<number>(0);
  const [params, setParams] = React.useState<PointHistoryFilterParams>({
    isUsed: null,
    size: PAGE_SIZE,
    startDate: moment()
      .subtract(89, 'days')
      .format(DATE_FORMAT),
    endDate: moment().format(DATE_FORMAT),
  });

  const loadMore = React.useCallback(async () => {
    setLoading(true);

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getBonusPointHistories}`,
        'post',
        true,
        JSON.stringify({ ...params, page: page + 1 }),
      ),
    );
    if (json.code === 200) {
      setPage(page + 1);
      setData(b => b && b.concat(json.data.list));
      setTotalPoint(json.data.totalPoint);
    }
    setLoading(false);
  }, [params, dispatch, page]);

  React.useEffect(() => {
    const fetch = async () => {
      setLoading(true);

      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getBonusPointHistories}`,
          'post',
          true,
          JSON.stringify({ ...params, page: 1 }),
        ),
      );
      if (json.code === 200) {
        setData(json.data.list);
        setTotal(json.data.total);
        setTotalPoint(json.data.totalPoint);
      }
      setLoading(false);
    };
    fetch();
  }, [dispatch, params]);

  if (!data) {
    return <LoadingIcon style={{ marginTop: '40px' }} />;
  }

  return (
    <div style={{ padding: '18px 30px' }}>
      <Helmet>
        <title>{intl.formatMessage({ id: 'm.accumulate.pointHistory' })}</title>
      </Helmet>
      <AccumulateBreadcrumb />
      <Typography variant="h5" style={{ marginTop: '32px' }}>
        <FormattedMessage id="m.accumulate.pointHistory" />
      </Typography>
      <Typography variant="body1" style={{ marginTop: '8px' }}>
        <FormattedMessage id={getPointMessage(tab)} />
        :&nbsp;
        <FormattedNumber value={totalPoint} />
        &nbsp;
        <FormattedMessage id="point" />
      </Typography>
      <div
        style={{
          background: 'white',
          width: '200px',
          marginTop: '20px',
        }}
      >
        <Select
          value={sortBy}
          onChange={event => {
            setSortBy(event.target.value as number);
            const target = filterByData.find(v => v.id === (event.target.value as number));
            if (target && target.id !== 7) {
              setParams({ ...params, startDate: target.fromDate, endDate: target.toDate });
            } else {
              setOpenChooseDate(true);
            }
          }}
          input={<BootstrapInput />}
          fullWidth
        >
          {filterByData.map(item => (
            <MenuItem key={item.id} value={item.id}>
              <Typography variant="body2">
                {sortBy !== 7 ? (
                  <FormattedMessage id={item.text} />
                ) : item.id === 7 ? (
                  `${params.startDate.split('-').join('/')} - ${params.endDate
                    .split('-')
                    .join('/')}`
                ) : (
                  <FormattedMessage id={item.text} />
                )}
              </Typography>
            </MenuItem>
          ))}
        </Select>
      </div>

      <div style={{ marginTop: '20px', width: '570px' }}>
        <Tabs
          value={tab}
          onChange={(event, newValue) => {
            setPage(1);
            setTab(newValue);
            setParams({ ...params, isUsed: pointState[newValue] });
          }}
          variant="fullWidth"
          scrollButtons="on"
          aria-label="icon label tabs"
          indicatorColor="primary"
          textColor="primary"
          style={{ borderBottom: `1px solid ${GREY}` }}
        >
          <Tab label={intl.formatMessage({ id: 'all' })} />
          <Tab label={intl.formatMessage({ id: 'm.accumulate.pointBonus' })} />
          <Tab label={intl.formatMessage({ id: 'm.accumulate.pointUse' })} />
        </Tabs>
        <div style={{ width: '100%' }}>
          {loading ? (
            <LoadingIcon style={{ marginTop: '40px' }} />
          ) : (
            <div>
              {data.length > 0 ? (
                <div>
                  {data.map((item: some, index: any) => (
                    <PointInfoCard key={index} info={item}></PointInfoCard>
                  ))}
                </div>
              ) : (
                <div>
                  {tab === 2 ? (
                    <div
                      style={{
                        marginTop: '60px',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}
                    >
                      <Typography variant="body2">
                        <FormattedMessage id="m.accumulate.usePoint" />
                      </Typography>
                      <RawLink to={{ pathname: ROUTES.reward.rewards }}>
                        <Button
                          style={{ marginTop: '20px', textAlign: 'center' }}
                          variant="contained"
                          color="secondary"
                        >
                          <FormattedMessage id="m.accumulate.exchangeReward" />
                        </Button>
                      </RawLink>
                    </div>
                  ) : (
                    <div
                      style={{
                        marginTop: '60px',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}
                    >
                      <IconNoHistories />
                      <Typography variant="body2">
                        <FormattedMessage id="m.accumulate.noPointHistories" />
                      </Typography>
                    </div>
                  )}
                </div>
              )}

              {total - PAGE_SIZE * page > 0 && (
                <div style={{ textAlign: 'center', margin: '24px 0px' }}>
                  <Button onClick={loadMore}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage
                        id="result.displayMore"
                        values={{ num: total - PAGE_SIZE * page }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
            </div>
          )}
        </div>
      </div>

      {openChooseDate && (
        <ChooseDateDialog
          open={openChooseDate}
          onClose={() => setOpenChooseDate(false)}
          params={{ fromDate: params.startDate, toDate: params.endDate }}
          onChange={chooseDateParams => {
            setParams({
              ...params,
              startDate: chooseDateParams.fromDate,
              endDate: chooseDateParams.toDate,
            });
            setOpenChooseDate(false);
          }}
        />
      )}
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

export default connect(mapStateToProps)(injectIntl(PointHistory));
