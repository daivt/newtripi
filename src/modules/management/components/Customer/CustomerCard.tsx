import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY, HOVER_GREY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import Link from '../../../common/components/Link';
import { ACTION_PARAM_NAME, ListOfCustomerActionType } from '../../constants';

const Row = styled.tr`
  &:hover {
    background: ${HOVER_GREY};
  }
  height: 56px;
  border-top: 1px solid ${GREY};
  border-bottom: 1px solid ${GREY};
`;

interface Props {
  data: some;
  index: number;
  search: string;
}

const seeDetail = (id: number, search: string) => {
  const params = new URLSearchParams(search);
  params.set(ACTION_PARAM_NAME, 'detail' as ListOfCustomerActionType);
  params.set('customerId', `${id}`);
  return params.toString();
};

const CustomerCard: React.FunctionComponent<Props> = props => {
  const { data, index } = props;

  return (
    <Row>
      <>
        <td style={{ width: '10%', textAlign: 'center' }}>
          <Typography variant="body2">{index}</Typography>
        </td>
        <td style={{ width: '30%' }}>
          <Typography variant="body2">
            {data.lastName}&nbsp;{data.firstName}
          </Typography>
        </td>
        <td style={{ width: '15%' }}>
          <Typography variant="body2">{data.phone}</Typography>
        </td>
        <td style={{ width: '30%' }}>
          <Typography variant="body2" style={{ color: BLUE }}>
            {data.email}
          </Typography>
        </td>
        <td style={{ width: '15%' }}>
          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${seeDetail(data.id, props.search)}`,
              state: { backableToList: true },
            }}
          >
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage id="m.detail" />
            </Typography>
          </Link>
        </td>
      </>
    </Row>
  );
};

export default CustomerCard;
