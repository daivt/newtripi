import { Button, InputAdornment, Typography } from '@material-ui/core';
import { debounce, find } from 'lodash';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { PAGE_SIZE, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icExport from '../../../../svg/ic_export.svg';
import icSearch from '../../../../svg/ic_search.svg';
import { FreeTextField } from '../../../booking/components/Form';
import { snackbarSetting } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import MessageDialog from '../../../common/components/MessageDialog';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, ListOfCustomerActionType } from '../../constants';
import CustomerCard from './CustomerCard';
import CustomerDetail from './CustomerDetail';
import CustomerInfomationDialog from './CustomerInfomationDialog';
import CustomerTransactionHistory from './CustomerTransactionHistory';
import ExportListDialog from './ExportListDialog';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

const Customer: React.FunctionComponent<Props> = props => {
  const { dispatch, intl } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(1);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [message, setMessage] = React.useState('');
  const [search, setSearch] = React.useState<string>('');
  const searchState = props.router.location.search;
  const [openExportDialog, setOpenExportDialog] = React.useState(false);
  const params = new URLSearchParams(searchState);
  const action = params.get(ACTION_PARAM_NAME) as ListOfCustomerActionType | null;
  const customerId = parseInt(params.get('customerId') || '0', 10) || 0;
  const findData = find(data, o => {
    return o.id === customerId;
  });

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [changedCustomer, setChangedCustomer] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getContactList}`,
        'post',
        true,
        JSON.stringify({
          filters: { name: search },
          paging: { page: page + 1, pageSize: PAGE_SIZE },
        }),
      ),
    );
    if (json.code === 200) {
      setData(b => b && b.concat(json.data));
      setPage(page + 1);
    } else {
      setMessage(json.message);
    }
    setLoading(false);
  }, [dispatch, page, search]);

  const fetchData = React.useCallback(
    async (filters: some, page: number) => {
      setData([]);
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getContactList}`,
          'post',
          true,
          JSON.stringify({ filters, paging: { page, pageSize: PAGE_SIZE } }),
        ),
      );
      if (json.code === 200) {
        setData(json.data);
        setTotal(json.total);
      } else {
        setTotal(0);
        setMessage(json.message);
      }
      setLoading(false);
    },
    [dispatch],
  );
  React.useEffect(() => {
    fetchData({}, 1);
  }, [fetchData, changedCustomer]);

  const sendListToEmail = React.useCallback(
    async (email: String) => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.exportContacts}`,
          'post',
          true,
          JSON.stringify({
            email,
          }),
        ),
      );
      if (json.code === 200) {
        enqueueSnackbar(
          json.message,
          snackbarSetting(json.message, key => closeSnackbar(key)),
        );
      } else {
        setMessage(json.message);
      }
      setOpenExportDialog(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );
  const onChangeInput = React.useCallback(
    debounce(
      (text: string) => {
        fetchData({ name: text }, page);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );
  const inputChange = React.useCallback(
    (text: string) => {
      setSearch(text);
      onChangeInput(text);
    },
    [onChangeInput],
  );

  return (
    <div style={{ padding: '30px 20px 42px 30px' }}>
      <div style={{ display: action ? 'none' : undefined }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.customers" />
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            style={{ width: '160px' }}
            size="large"
            onClick={() => setOpen(true)}
          >
            <FormattedMessage id="m.addNew" />
          </Button>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <FreeTextField
            inputStyle={{ width: '270px', background: 'white' }}
            text={search}
            update={inputChange}
            placeholder={intl.formatMessage({ id: 'contact.enterCustomerName' })}
            header={<></>}
            optional
            endAdornment={
              <InputAdornment position="end">
                <div style={{ padding: '8px' }}>
                  <img
                    src={icSearch}
                    alt=""
                    style={{
                      filter: 'brightness(1) invert(1)',
                      width: '24px',
                      height: '24px',
                      color: 'black',
                    }}
                  />
                </div>
              </InputAdornment>
            }
          />
        </div>
        <div>
          {data && (
            <>
              <table style={{ width: '100%', borderCollapse: 'collapse', marginTop: '12px' }}>
                <tbody>
                  {data.map((one, index) => (
                    <CustomerCard key={one.id} data={one} index={index + 1} search={searchState} />
                  ))}
                </tbody>
              </table>
              {loading && <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />}
              {!data.length && !loading && (
                <NoDataResult
                  id={!!search ? 'contact.noResult' : 'm.customers.noCustomers'}
                  linkText={!search ? 'm.customers.addCustomerText' : undefined}
                  style={{ marginTop: '48px' }}
                  action={() => setOpen(true)}
                />
              )}
            </>
          )}
          {!loading && total - PAGE_SIZE * page > 0 && (
            <div style={{ textAlign: 'center', margin: '24px 0px' }}>
              <Button onClick={loadMore}>
                <Typography style={{ color: BLUE }}>
                  <FormattedMessage
                    id="result.displayMore"
                    values={{ num: total - PAGE_SIZE * page }}
                  />
                </Typography>
              </Button>
            </div>
          )}
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            marginTop: '16px',
            alignItems: 'center',
          }}
        >
          {data && data.length > 0 && (
            <Button
              style={{ display: 'flex', cursor: 'pointer' }}
              onClick={() => setOpenExportDialog(true)}
            >
              <img src={icExport} alt="" />
              &nbsp;
              <Typography variant="body2">
                <FormattedMessage id="m.customers.export" />
              </Typography>
            </Button>
          )}
        </div>
      </div>
      <MessageDialog
        show={!!message}
        onClose={() => setMessage('')}
        message={
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Typography variant="body1">{message}</Typography>
          </div>
        }
      />

      <ExportListDialog
        open={openExportDialog}
        onClose={() => {
          setOpenExportDialog(false);
        }}
        onClick={sendListToEmail}
      />

      {action === 'detail' && findData && (
        <CustomerDetail customerData={findData} onChange={object => setChangedCustomer(object)} />
      )}
      {action === 'transactionHistory' && <CustomerTransactionHistory customerId={customerId} />}
      <CustomerInfomationDialog
        open={open}
        onClose={() => setOpen(false)}
        onChange={object => setChangedCustomer(object)}
        action={'add'}
      />
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(Customer));
