import {
  Button,
  Dialog,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { isEqual } from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { GREY, HOVER_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { DateField, FreeTextField, GenderField } from '../../../booking/components/Form';
import { snackbarSetting } from '../../../common/components/elements';
import {
  CustomerInfo,
  CustomerInfoValidation,
  validCustomerInfoValidation,
} from '../../../common/models';
import { fetchThunk } from '../../../common/redux/thunks';
import { ListOfCustomerInfomationType } from '../../constants';
import { validateCustomerInfo } from '../../utils';
import { validTelephoneRegex } from '../../../common/utils';

export interface ICustomerInfomationDialogProps
  extends WrappedComponentProps,
    ReturnType<typeof mapStateToProps> {
  open: boolean;
  onClose(): void;
  data?: some;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onChange(object: string): void;
  action: ListOfCustomerInfomationType;
}

const CustomerInfomationDialog: React.FunctionComponent<ICustomerInfomationDialogProps> = props => {
  const { open, onClose, intl } = props;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [birthDay, setBirthDay] = React.useState('');
  const [expireDay, setExpireDay] = React.useState('');
  const [customerInfoValidation, setCustomerInfoValidation] = React.useState({
    firstName: true,
    lastName: true,
    phone: true,
  } as CustomerInfoValidation);
  const [customerData, setCustomerData] = React.useState<CustomerInfo>({
    gender: 'f',
    firstName: '',
    lastName: '',
    phone: '',
  });

  const submitData = React.useCallback(async () => {
    const { dispatch, onChange, onClose, action } = props;
    const submitData = {
      ...customerData,
      firstName: customerData.firstName.trim(),
      lastName: customerData.lastName.trim(),
      dateOfBirth: birthDay ? moment(birthDay, 'L').valueOf() : undefined,
      passportExpiry: expireDay ? moment(expireDay, 'L').valueOf() : undefined,
    };

    const validateData = validateCustomerInfo(submitData);
    setCustomerInfoValidation(validateData);

    if (isEqual(validateData, validCustomerInfoValidation)) {
      const json = await dispatch(
        fetchThunk(
          action === 'edit' ? `${API_PATHS.updateContact}` : `${API_PATHS.addContact}`,
          'post',
          true,
          JSON.stringify(submitData),
        ),
      );
      if (json.code === 200) {
        enqueueSnackbar(
          json.message,
          snackbarSetting(json.message, key => closeSnackbar(key)),
        );
        onClose();
        if (submitData) {
          onChange(JSON.stringify(submitData));
        }
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(json.message, key => closeSnackbar(key)),
        );
      }
    }
  }, [customerData, props, closeSnackbar, enqueueSnackbar, birthDay, expireDay]);
  React.useEffect(() => {
    const { data } = props;
    if (data) {
      setCustomerData({ ...data, gender: data.gender.toLowerCase() } as CustomerInfo);
    } else {
      setCustomerData({
        gender: 'f',
        firstName: '',
        lastName: '',
        phone: '',
      });
      setBirthDay('');
      setExpireDay('');
    }
  }, [props]);

  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="sm"
      PaperProps={{
        style: {
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          minWidth: '840px',
          padding: '20px 10px 20px 30px',
        },
      }}
      onClose={onClose}
      keepMounted={false}
    >
      <Typography variant="h5" style={{ alignSelf: 'center' }}>
        <FormattedMessage id="m.customerInfomations" />
      </Typography>
      <div style={{ marginTop: '30px' }}>
        <GenderField
          gender={customerData.gender}
          update={gender => setCustomerData({ ...customerData, gender })}
          oneLine
        />
      </div>
      <div style={{ display: 'flex' }}>
        <FreeTextField
          text={customerData.lastName}
          placeholder={intl.formatMessage({ id: 'familyNameExD' })}
          update={lastName => setCustomerData({ ...customerData, lastName })}
          header={intl.formatMessage({ id: 'familyName' })}
          inputStyle={{ width: '240px' }}
          valid={customerInfoValidation.lastName}
        />
        <FreeTextField
          text={customerData.firstName}
          placeholder={intl.formatMessage({ id: 'givenNameExD' })}
          update={firstName => setCustomerData({ ...customerData, firstName })}
          header={intl.formatMessage({ id: 'givenName' })}
          inputStyle={{ width: '240px' }}
          valid={customerInfoValidation.firstName}
        />
        <DateField
          disableFuture
          header={intl.formatMessage({ id: 'birthday' })}
          date={
            birthDay
              ? birthDay
              : customerData.dateOfBirth
              ? moment(customerData.dateOfBirth).format('L')
              : ''
          }
          update={birthDay => setBirthDay(birthDay)}
          valid={true}
          optional
        />
      </div>
      <Typography variant="button" style={{ marginTop: '24px' }}>
        <FormattedMessage id="contactInfo" />
      </Typography>
      <div style={{ display: 'flex', marginTop: '16px' }}>
        <FreeTextField
          text={customerData.phone}
          placeholder={intl.formatMessage({ id: 'telephoneEx' })}
          update={phone => setCustomerData({ ...customerData, phone })}
          header={intl.formatMessage({ id: 'telephone' })}
          inputStyle={{ width: '240px' }}
          regex={validTelephoneRegex}
          valid={customerInfoValidation.phone}
        />
        <FreeTextField
          text={customerData.email || ''}
          placeholder={intl.formatMessage({ id: 'emailEx' })}
          update={email => setCustomerData({ ...customerData, email })}
          header={intl.formatMessage({ id: 'email' })}
          inputStyle={{ width: '240px' }}
          optional
        />
        <FreeTextField
          text={customerData.address || ''}
          placeholder={intl.formatMessage({ id: 'addressEx' })}
          update={address => setCustomerData({ ...customerData, address })}
          header={intl.formatMessage({ id: 'address' })}
          inputStyle={{ width: '240px' }}
          optional
        />
      </div>
      <div style={{ display: 'flex', marginTop: '16px' }}>
        <FreeTextField
          text={customerData.city || ''}
          placeholder={intl.formatMessage({ id: 'provinceEx' })}
          update={city => setCustomerData({ ...customerData, city })}
          header={intl.formatMessage({ id: 'province' })}
          inputStyle={{ width: '240px' }}
          optional
          style={{ flex: 1 }}
        />
        <FreeTextField
          text={customerData.country || ''}
          placeholder={intl.formatMessage({ id: 'countryEx' })}
          update={country => setCustomerData({ ...customerData, country })}
          header={intl.formatMessage({ id: 'country' })}
          inputStyle={{ width: '240px' }}
          optional
          style={{ flex: 1 }}
        />
        <div style={{ flex: 1, marginRight: '20px' }} />
      </div>
      <ExpansionPanel
        style={{ marginTop: '20px', background: HOVER_GREY, border: 'none', boxShadow: 'none' }}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography variant="subtitle2">
            <FormattedMessage id="personalId" />
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails
          style={{ background: 'white', paddingTop: '16px', padding: 'unset' }}
        >
          <FreeTextField
            text={customerData.passport || ''}
            placeholder={intl.formatMessage({ id: 'personalIdEx' })}
            update={passport => setCustomerData({ ...customerData, passport })}
            header={intl.formatMessage({ id: 'number' })}
            inputStyle={{ width: '240px' }}
            optional
            style={{ flex: 1 }}
          />
          <DateField
            disablePast
            header={intl.formatMessage({ id: 'expiredTime' })}
            date={
              expireDay
                ? expireDay
                : customerData.passportExpiry
                ? moment(customerData.passportExpiry).format('L')
                : ''
            }
            update={expireDay => setExpireDay(expireDay)}
            valid={true}
            optional
          />
          <div style={{ flex: 1, marginRight: '20px' }} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <div style={{ display: 'flex', marginTop: '30px', alignSelf: 'center' }}>
        <Button
          style={{ marginRight: '20px', width: '160px', height: '40px' }}
          variant="contained"
          color="secondary"
          onClick={submitData}
        >
          <Typography variant="button" style={{ color: 'white' }}>
            <FormattedMessage id={'update'} />
          </Typography>
        </Button>
        <Button style={{ width: '160px', height: '40px' }} variant="outlined" onClick={onClose}>
          <Typography variant="button" style={{ color: GREY }}>
            <FormattedMessage id={'ignore'} />
          </Typography>
        </Button>
      </div>
    </Dialog>
  );
};

function mapStateToProps(state: AppState) {
  return {};
}
export default connect(mapStateToProps)(injectIntl(CustomerInfomationDialog));
