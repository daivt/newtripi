import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE, PRIMARY, SECONDARY } from '../../../../colors';
import { some, PAGE_SIZE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icNoResult from '../../../../svg/ic_contact_no_result.svg';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../common/redux/thunks';
import CustomerTransactionHistoryBreadscrumb from './CustomerTransactionHistoryBreadscrumb';
import TransactionCard from './TransactionCard';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  customerId: number;
}
function mapStateToProps(state: AppState) {
  return {};
}

const CustomerTransactionHistory: React.FunctionComponent<Props> = props => {
  const { intl, dispatch, customerId } = props;
  const [data, setData] = React.useState<some | undefined>(undefined);
  // const [message, setMessage] = React.useState('');
  const [size, setSize] = React.useState(PAGE_SIZE);

  const fetchData = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getContactTransactionHistory}`,
        'post',
        true,
        JSON.stringify({ contactId: customerId }),
      ),
    );
    if (json.code === 200) {
      setData(json.data);
    } else {
      // setMessage(json.message);
    }
  }, [dispatch, customerId]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);
  return (
    <div>
      <Helmet>
        <title>
          {intl.formatMessage({ id: 'm.title' })}:{' '}
          {intl.formatMessage({ id: 'm.customers.transactionHistories' })}
        </title>
      </Helmet>
      <CustomerTransactionHistoryBreadscrumb />
      {data ? (
        <div
          style={{
            marginTop: '32px',
          }}
        >
          <Typography variant="h5">
            <FormattedMessage id="m.customers.transactionHistories" />
          </Typography>
          {data.summaries.totalOrders > 0 ? (
            <>
              <Typography variant="body2" style={{ marginTop: '16px' }}>
                <FormattedMessage id="m.totalOrders" />
                :&nbsp;
                {data.summaries.totalOrders}
              </Typography>
              <div style={{ display: 'flex', marginTop: '12px', alignItems: 'center' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.totalSales" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: SECONDARY }}>
                  <FormattedNumber value={data.summaries.totalSales} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
              <div style={{ display: 'flex', marginTop: '12px', alignItems: 'center' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.totalProfit" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: SECONDARY }}>
                  <FormattedNumber value={data.summaries.totalMarkupAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
              <div style={{ display: 'flex', alignItems: 'center', marginTop: '12px' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.totalBonusPoint" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: PRIMARY }}>
                  <FormattedNumber value={data.summaries.totalBonusPoint} />
                  &nbsp;
                  <FormattedMessage id="point" />
                </Typography>
              </div>
              <div style={{ marginTop: '24px' }}>
                {data.transactions.slice(0, size).map((item: some) => (
                  <TransactionCard key={item.bookingId} data={item} />
                ))}
              </div>
              {data.transactions.length > size && (
                <div style={{ width: '530px', marginTop: '24px', textAlign: 'center' }}>
                  <Button onClick={() => setSize(size + PAGE_SIZE)}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage
                        id="result.displayMore"
                        values={{ num: data.transactions.length - size }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
            </>
          ) : (
            <div style={{ width: '100%', marginTop: '75px', textAlign: 'center' }}>
              <img src={icNoResult} alt="" />
              <Typography variant="subtitle2">
                <FormattedMessage id="m.customers.noTransaction" />
              </Typography>
            </div>
          )}
        </div>
      ) : (
        <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />
      )}
    </div>
  );
};

export default connect()(injectIntl(CustomerTransactionHistory));
