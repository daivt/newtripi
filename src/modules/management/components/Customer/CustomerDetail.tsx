import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE, GREY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icDelete from '../../../../svg/delete.svg';
import iconDelete from '../../../../svg/ic_delete_ticket_alert.svg';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import Link from '../../../common/components/Link';
import LoadingIcon from '../../../common/components/LoadingIcon';
import MessageDialog from '../../../common/components/MessageDialog';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, ListOfCustomerActionType } from '../../constants';
import CustomerDetailBreadscrumb from './CustomerDetailBreadscrumb';
import CustomerInfomationDialog from './CustomerInfomationDialog';

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

export interface ICustomerDetailProps
  extends WrappedComponentProps,
    ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onChange(object: string): void;
  customerData: some;
}

const seeTransactionHistories = (id: number, search: string) => {
  const params = new URLSearchParams(search);
  params.set(ACTION_PARAM_NAME, 'transactionHistory' as ListOfCustomerActionType);
  params.set('customerId', `${id}`);
  return params.toString();
};

const CustomerDetail: React.FunctionComponent<ICustomerDetailProps> = props => {
  const { intl, dispatch, router, onChange, customerData } = props;
  const [confirm, setConfirm] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [dialogAction, setDialogAction] = React.useState<ListOfCustomerActionType>('detail');
  const [openCustomerInfomationDialog, setOpenCustomerInfomationDialog] = React.useState(false);

  const deleteCustomer = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.deleteContact}?id=${customerData.id}`,
        'post',
        true,
        JSON.stringify({}),
      ),
    );
    if (json.code === 200) {
      setMessage(intl.formatMessage({ id: 'm.customers.deleteCustomerSuccess' }));
      setDialogAction('delete');
      setConfirm(false);
    } else {
      setMessage(json.message);
    }
  }, [dispatch, intl, customerData]);

  const onCloseMessageDialog = React.useCallback(async () => {
    setMessage('');
    if (dialogAction === 'delete') {
      const { router, dispatch, onChange } = props;
      onChange(JSON.stringify(customerData));
      const params = new URLSearchParams(router.location.search);
      params.delete('customerId');
      params.delete(ACTION_PARAM_NAME);
      dispatch(replace({ search: params.toString() }));
    }
  }, [dialogAction, props, customerData]);
  return (
    <div>
      <Helmet>
        <title>
          {intl.formatMessage({ id: 'm.title' })}: {intl.formatMessage({ id: 'm.order.customer' })}
          &nbsp;
          {customerData ? `${customerData.lastName} ${customerData.firstName}` : ''}
        </title>
      </Helmet>
      <CustomerDetailBreadscrumb />
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: '32px',
        }}
      >
        <Typography variant="h5">
          <FormattedMessage id="m.order.customerInfo" />
        </Typography>
        <div>
          <Button
            size="large"
            color="secondary"
            variant="contained"
            style={{ minWidth: '170px' }}
            onClick={() => setOpenCustomerInfomationDialog(true)}
          >
            <FormattedMessage id="m.customers.editInfomation" />
          </Button>
          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${seeTransactionHistories(customerData.id, router.location.search)}`,
              state: { backableToDetail: true },
            }}
          >
            <Button
              size="large"
              color="secondary"
              variant="contained"
              style={{ minWidth: '170px', marginLeft: '20px' }}
            >
              <FormattedMessage id="m.customers.transactionHistories" />
            </Button>
          </Link>
        </div>
      </div>
      {customerData ? (
        <div
          style={{
            marginTop: '10px',
            boxSizing: 'border-box',
            borderRadius: '4px',
            border: `1px solid ${GREY}`,
            background: 'white',
            padding: '20px 20px',
          }}
        >
          <Typography variant="subtitle2">
            <FormattedMessage id="m.customers.basicInfomation" />
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="fullName" />
            :&nbsp;
            {`${customerData.lastName} ${customerData.firstName}`}
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="gender" />
            :&nbsp;
            {customerData.gender.toLowerCase() === 'm' ? (
              <FormattedMessage id="male" />
            ) : (
              <FormattedMessage id="female" />
            )}
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="birthday" />
            :&nbsp;
            {customerData.dateOfBirth && moment(customerData.dateOfBirth).format('L')}
          </Typography>
          <Typography variant="subtitle2" style={{ marginTop: '20px' }}>
            <FormattedMessage id="m.contactInfo" />
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="telephone" />
            :&nbsp;
            {customerData.phone}
          </Typography>
          <div style={{ display: 'flex', marginTop: '10px' }}>
            <Typography variant="body2">
              <FormattedMessage id="email" />
              :&nbsp;
            </Typography>
            <Typography variant="body2" style={{ color: BLUE }}>
              {customerData.email}
            </Typography>
          </div>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="address" />
            :&nbsp;
            {customerData.address}
          </Typography>
          <Typography variant="subtitle2" style={{ marginTop: '20px' }}>
            <FormattedMessage id="m.customers.idCard" />
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="m.customers.idNumber" />
            :&nbsp;
            {customerData.passport}
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="expiredTime" />
            :&nbsp;
            {customerData.passportExpiry && moment(customerData.passportExpiry).format('L')}
          </Typography>
        </div>
      ) : (
        <LoadingIcon
          style={{
            height: '300px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      )}
      <div
        style={{ display: 'flex', marginTop: '10px', cursor: 'pointer' }}
        onClick={() => setConfirm(true)}
      >
        <img src={icDelete} alt="" />
        <Typography variant="body2" style={{ marginLeft: '10px' }}>
          <FormattedMessage id="m.customers.deleteCustomer" />
        </Typography>
      </div>
      <MessageDialog
        show={!!message}
        onClose={onCloseMessageDialog}
        message={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              padding: '18px',
              textAlign: 'center',
            }}
          >
            <Typography variant="body1">{message}</Typography>
          </div>
        }
      />
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <img src={iconDelete} alt="" style={{ height: '117px' }} />
            <Typography variant="h6">
              <FormattedMessage id="m.customers.confirm" />
            </Typography>
          </div>
        }
        show={confirm}
        cancelMessageId="ignore"
        confirmMessageId="ok"
        onAccept={deleteCustomer}
        onCancel={() => setConfirm(false)}
      />
      <CustomerInfomationDialog
        open={openCustomerInfomationDialog}
        onClose={() => setOpenCustomerInfomationDialog(false)}
        data={customerData}
        onChange={onChange}
        action={'edit'}
      />
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(CustomerDetail));
