import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { goBack, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const CustomerDetailBreadscrumb: React.FunctionComponent<Props> = props => {
  const backToCustomerList = React.useCallback(() => {
    const { state, dispatch, router } = props;
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(goBack());
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    params.delete('customerId');
    dispatch(replace({ search: params.toString() }));
  }, [props]);
  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToCustomerList()}
      >
        <FormattedMessage id="m.customers.listCustomers" />
      </Typography>
      <Typography variant="body2" color="secondary">
        <FormattedMessage id="m.customerInfomations" />
      </Typography>
    </Breadcrumbs>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(CustomerDetailBreadscrumb);
