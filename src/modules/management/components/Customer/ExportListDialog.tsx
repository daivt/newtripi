import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { FreeTextField } from '../../../booking/components/Form';
import { validEmailRegex } from '../../../common/utils';

export interface IExportListDialogProps extends WrappedComponentProps {
  open: boolean;
  onClose(): void;
  onClick(email: string): void;
}

const ExportListDialog: React.FunctionComponent<IExportListDialogProps> = props => {
  const { open, onClose, onClick, intl } = props;
  const [email, setEmail] = React.useState('');
  const [valid, setValid] = React.useState(true);
  const [ticktock, setTick] = React.useState(true);
  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="sm"
      PaperProps={{
        style: {
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          width: '400px',
          minHeight: '320px',
          padding: '20px 30px 28px 30px',
        },
      }}
      onClose={onClose}
      keepMounted={false}
    >
      <IconButton
        style={{ position: 'absolute', padding: '0px', top: '8px', right: '8px' }}
        size="small"
        onClick={onClose}
      >
        <IconClose />
      </IconButton>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography variant="h5">
          <FormattedMessage id="m.customers.export" />
        </Typography>
        <Typography
          variant="body2"
          style={{
            marginTop: '30px',
            marginBottom: '20px',
            paddingRight: '50px',
          }}
        >
          <FormattedMessage id="m.customers.exportCaption" />
        </Typography>
        <FreeTextField
          key={`${ticktock}`}
          style={{ width: '100%' }}
          text={email}
          optional={true}
          header={intl.formatMessage({ id: 'm.customers.enterEmail' })}
          placeholder={intl.formatMessage({ id: 'signUp.emailExample' })}
          update={text => {
            setEmail(text);
            setValid(true);
          }}
          valid={valid}
        />

        <Button
          style={{ width: '170px', marginTop: '30px' }}
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => {
            const valid = validEmailRegex.test(email);
            setValid(valid);
            setTick(!ticktock);
            if (valid) {
              onClick(email);
            }
          }}
        >
          <Typography variant="button">
            <FormattedMessage id={'m.customers.send'} />
          </Typography>
        </Button>
      </div>
    </Dialog>
  );
};
export default injectIntl(ExportListDialog);
