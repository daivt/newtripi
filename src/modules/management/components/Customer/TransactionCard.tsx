import * as React from 'react';
import { GREY, SECONDARY, DARK_GREY } from '../../../../colors';
import { some, ROUTES } from '../../../../constants';
import Flight from '../../../../svg/Flight.svg';
import Hotel from '../../../../svg/Hotel.svg';
import Activity from '../../../../svg/TourismActivities.svg';
import Train from '../../../../svg/Train.svg';
import { Typography, Button } from '@material-ui/core';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import {
  BOOKING_ID_PARAM_NAME,
  ACTION_PARAM_NAME,
  FlightManagementBookingItemActionType,
  HotelManagementBookingItemActionType,
  TourManagementBookingItemActionType,
  CURRENT_PARAM_NAME,
  TAB_PARAM_NAME,
} from '../../constants';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
}

const TransactionCard: React.FunctionComponent<Props> = props => {
  const { data, router } = props;
  const getLink = React.useCallback(() => {
    const { data } = props;
    const tabParam =
      data.module === 'flight'
        ? 'flight'
        : data.module === 'hotel'
        ? 'hotel'
        : data.module === 'activity'
        ? 'tour'
        : '';
    const actionParam =
      data.module === 'flight'
        ? ('detail' as FlightManagementBookingItemActionType)
        : data.module === 'hotel'
        ? ('detail' as HotelManagementBookingItemActionType)
        : data.module === 'activity'
        ? ('detail' as TourManagementBookingItemActionType)
        : '';
    const params = new URLSearchParams(router.location.search);
    params.set(CURRENT_PARAM_NAME, 'm.orders');
    params.set(TAB_PARAM_NAME, tabParam);
    params.set(BOOKING_ID_PARAM_NAME, data.bookingId);
    params.set(ACTION_PARAM_NAME, actionParam);
    return `${ROUTES.management}?${params.toString()}`;
  }, [props, router.location.search]);

  return (
    <div
      style={{
        display: 'flex',
        marginTop: '10px',
        border: `0.5px solid ${GREY}`,
        width: '530px',
        minHeight: '90px',
        borderRadius: '4px',
        boxSizing: 'border-box',
        padding: '13px 10px 18px 13px',
      }}
    >
      <img
        src={
          data.module === 'flight'
            ? Flight
            : data.module === 'hotel'
            ? Hotel
            : data.module === 'train'
            ? Train
            : data.module === 'activity'
            ? Activity
            : ''
        }
        style={{ width: '33px', height: '33px' }}
        alt=""
      />
      <div style={{ marginLeft: '24px', width: '100%' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            {data.module === 'flight' ? (
              <div>
                <Typography variant="subtitle2">
                  <FormattedMessage id="flight" />
                  &nbsp;
                  {data.fromAirport} - {data.toAirport}
                </Typography>
                <Typography variant="body2" style={{ color: DARK_GREY, marginTop: '4px' }}>
                  <FormattedMessage id="m.bookingDate" />
                  &nbsp;
                  {data.bookingTime}
                </Typography>
              </div>
            ) : data.module === 'hotel' ? (
              <div>
                <Typography variant="subtitle2">
                  <FormattedMessage id="hotel" />
                  &nbsp;
                  {data.hotelName}
                </Typography>
                <Typography variant="body2" style={{ color: DARK_GREY, marginTop: '4px' }}>
                  <FormattedMessage id="m.bookingDate" />
                  &nbsp;
                  {data.bookingTime}
                </Typography>
              </div>
            ) : data.module === 'train' ? (
              <div>
                <Typography variant="subtitle2">
                  <FormattedMessage id="trainTickets" />
                  &nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: DARK_GREY, marginTop: '4px' }}>
                  <FormattedMessage id="m.bookingDate" />
                  &nbsp;
                  {data.bookingTime}
                </Typography>
              </div>
            ) : data.module === 'activity' ? (
              <div>
                <Typography variant="subtitle2">
                  <FormattedMessage id="tour" />
                  &nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: DARK_GREY, marginTop: '4px' }}>
                  <FormattedMessage id="m.bookingDate" />
                  &nbsp;
                  {data.bookingTime}
                </Typography>
              </div>
            ) : (
              ''
            )}
          </div>
          <div style={{ textAlign: 'right' }}>
            <Typography variant="body2" style={{ color: SECONDARY }}>
              <FormattedNumber value={data.price} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
            <a
              href={getLink()}
              target="_blank"
              rel="noopener noreferrer"
              style={{
                textDecoration: 'inherit',
                color: 'inherit',
              }}
            >
              <Button
                size="large"
                color="secondary"
                variant="contained"
                style={{ minWidth: '100px' }}
              >
                <FormattedMessage id="m.detail" />
              </Button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return { router: state.router };
};
export default connect(mapStateToProps)(TransactionCard);
