import * as React from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { some, DATE_FORMAT } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { fetchThunk } from './../../../common/redux/thunks';
import moment from 'moment';
import { Typography, Divider } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import RequestCard from './RequestCard';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  bookingId: number;
  bookModule: string;
}

const OrderRequestList: React.FunctionComponent<Props> = props => {
  const { dispatch, bookingId, bookModule, search, state } = props;
  const [data, setData] = React.useState<some | undefined>(undefined);

  React.useEffect(() => {
    const fetch = async () => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.bookerRequest}`,
          'post',
          true,
          JSON.stringify({
            filters: {
              bookingId,
              modules: [bookModule],
              fromDate: '01-01-2018',
              toDate: moment().format(DATE_FORMAT),
            },
            paging: {
              page: 1,
              pageSize: 500,
            },
          }),
        ),
      );
      if (json.code === 200) {
        setData(json.data);
      }
    };
    fetch();
  }, [dispatch, bookingId, bookModule]);

  if (!data) {
    return <LoadingIcon />;
  }

  return (
    <div style={{}}>
      {!!data && data.requests.length > 0 && (
        <>
          <Divider style={{ marginTop: '16px' }} />
          <div style={{ marginTop: '16px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="m.orders.listRequests" />
            </Typography>
          </div>

          <table style={{ width: '100%', borderCollapse: 'collapse', marginTop: '12px' }}>
            <tbody>
              {data.requests.map((one: some) => (
                <RequestCard key={one.id} data={one} search={search} state={state} />
              ))}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};
function mapStateToProps(state: AppState) {
  return {
    search: state.router.location.search,
    state: state.router.location.state,
  };
}
export default connect(mapStateToProps)(OrderRequestList);
