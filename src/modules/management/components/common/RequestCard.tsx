import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY, HOVER_GREY, RED, SECONDARY, PRIMARY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import Link from '../../../common/components/Link';
import { ACTION_PARAM_NAME, ORDER_REQUEST_DETAIL } from '../../constants';
import { ReactComponent as InvoiceIcon } from '../../../../svg/invoice_icon.svg';
import { ReactComponent as RefundIcon } from '../../../../svg/refund_icon.svg';

const Row = styled.tr`
  &:hover {
    background: ${HOVER_GREY};
  }
  height: 56px;
  border-bottom: 1px solid ${GREY};
`;

interface Props {
  data: some;
  search: string;
  state: some;
}

const seeInvoiceDetail = (id: number, search: string) => {
  const params = new URLSearchParams(search);
  params.set(ACTION_PARAM_NAME, 'orderRequestDetail' as ORDER_REQUEST_DETAIL);
  return params.toString();
};

export const getInvoiceStatus = (status: string) => {
  if (status === 'open') {
    return (
      <Typography variant="body2" style={{ color: BLUE }}>
        <FormattedMessage id="m.invoice.open" />
      </Typography>
    );
  }
  if (status === 'cancel') {
    return (
      <Typography variant="body2" style={{ color: RED }}>
        <FormattedMessage id="m.invoice.cancel" />
      </Typography>
    );
  }
  if (status === 'handling') {
    return (
      <Typography variant="body2" style={{ color: SECONDARY }}>
        <FormattedMessage id="m.invoice.handling" />
      </Typography>
    );
  }
  if (status === 'completed') {
    return (
      <Typography variant="body2" style={{ color: PRIMARY }}>
        <FormattedMessage id="m.invoice.completed" />
      </Typography>
    );
  }
};

const RequestCard: React.FunctionComponent<Props> = props => {
  const { data, state } = props;
  return (
    <Row>
      <>
        <td style={{ width: '30%' }}>
          <div style={{ display: 'flex', paddingLeft: '16px' }}>
            {data.type === 'vat_invoice' && (
              <>
                <InvoiceIcon style={{ marginRight: '16px' }} />
                <Typography variant="body2">
                  <FormattedMessage id="m.exportInvoice" />
                </Typography>
              </>
            )}
            {data.type === 'refund' && (
              <>
                <RefundIcon style={{ marginRight: '16px' }} />
                <Typography variant="body2">
                  <FormattedMessage id="m.invoice.refund" />
                </Typography>
              </>
            )}
          </div>
        </td>
        <td style={{ width: '25%' }}>
          <Typography variant="body2">{data.requestTime}</Typography>
        </td>

        <td style={{ width: '25%' }}>{getInvoiceStatus(data.status)}</td>
        <td style={{ width: '20%' }}>
          {data.type === 'vat_invoice' && !(data.status === 'cancel') && (
            <Typography variant="body2" style={{ color: BLUE }}>
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${seeInvoiceDetail(data.id, props.search)}`,
                  state: { ...state, backableToOrderDetail: true, requestData: data },
                }}
              >
                <FormattedMessage id="m.detail" />
              </Link>
            </Typography>
          )}
        </td>
      </>
    </Row>
  );
};

export default RequestCard;
