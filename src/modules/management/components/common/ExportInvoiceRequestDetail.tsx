import { IconButton, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconDelete } from '../../../../svg/ic_delete_ticket_alert.svg';
import { ReactComponent as IconInfo } from '../../../../svg/ic_payment_info.svg';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import LoadingButton from '../../../common/components/LoadingButton';
import MessageDialog from '../../../common/components/MessageDialog';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, HotelManagementBookingItemActionType } from '../../constants';
import { getInvoiceStatus } from './RequestCard';

const Line = styled.div`
  min-height: 36px;
  display: flex;
  align-items: center;
  width: 100%;
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  breadcrumb: React.ReactNode;
}

function mapStateToProps(state: AppState) {
  return {
    state: state.router.location.state,
    router: state.router,
  };
}

const ExportInvoiceRequestDetail: React.FC<Props> = props => {
  const { state, breadcrumb, dispatch, router } = props;
  const data = { ...state.requestData };
  const [openCancelDialog, setOpenCancelDialog] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [openPackageInfoDialog, setOpenPackageInfoDialog] = React.useState(false);

  const handleCancelRequest = () => {
    setOpenCancelDialog(true);
  };
  const sendCancelRequest = async () => {
    setLoading(true);
    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'detail' as HotelManagementBookingItemActionType);
    const json = await dispatch(
      fetchThunk(`${API_PATHS.cancelBookerRequest}?id=${data.id}`, 'get', true),
    );
    if (json.code === 200) {
      dispatch(replace({ search: `${params.toString()}` }));
    }
  };
  return (
    <div>
      {breadcrumb}
      <Typography variant="h5" style={{ padding: '16px 0px' }}>
        <FormattedMessage id="m.invoice" />
      </Typography>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="m.requestId" />
          :&nbsp;
          {data.id}
        </Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="m.invoice.timeRequest" />
          :&nbsp;
          {data.requestTime}
        </Typography>
      </Line>
      {data.vatInvoiceDetail && (
        <>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.invoice.customerName" />
              :&nbsp;
              {data.vatInvoiceDetail.companyName}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.invoice.taxNumber" />
              :&nbsp;
              {data.vatInvoiceDetail.taxIdNumber}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.invoice.companyAddress" />
              :&nbsp;
              {data.vatInvoiceDetail.companyAddress}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.invoice.reciever" />
              :&nbsp;
              {data.vatInvoiceDetail.recipientName}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.invoice.address" />
              :&nbsp;
              {data.vatInvoiceDetail.recipientAddress}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="telephone" />
              :&nbsp;
              {data.vatInvoiceDetail.recipientPhone}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="m.email" />
              :&nbsp;
            </Typography>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.vatInvoiceDetail.recipientEmail}
            </Typography>
          </Line>
          {data.vatInvoiceDetail.benefitPackage && (
            <div>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.invoice.package" />
                  :&nbsp;
                  {data.vatInvoiceDetail.benefitPackage.title}
                </Typography>
                <IconButton
                  style={{ padding: '0px', marginLeft: '10px' }}
                  onClick={() => setOpenPackageInfoDialog(true)}
                >
                  <IconInfo />
                </IconButton>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="note" />
                  :&nbsp;
                  {data.vatInvoiceDetail.note}
                </Typography>
              </Line>
            </div>
          )}
        </>
      )}
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="m.status" />
          :&nbsp;
        </Typography>
        {getInvoiceStatus(data.status)}
      </Line>
      {data.status === 'open' && (
        <LoadingButton
          loading={loading}
          variant="contained"
          color="secondary"
          style={{ marginTop: '10px' }}
          onClick={handleCancelRequest}
        >
          <FormattedMessage id="m.invoice.cancelRequest" />
        </LoadingButton>
      )}
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <IconDelete style={{ margin: 'auto' }} />
            <Typography variant="body1" color="textSecondary">
              <FormattedMessage id="m.invoice.cancelRequestWarning" />
            </Typography>
          </div>
        }
        show={openCancelDialog}
        cancelMessageId="ignore"
        confirmMessageId="ok"
        onAccept={sendCancelRequest}
        onCancel={() => setOpenCancelDialog(false)}
      />
      {data.benefitPackageSetting && (
        <MessageDialog
          show={!!openPackageInfoDialog}
          onClose={() => setOpenPackageInfoDialog(false)}
          header={<FormattedMessage id="m.packageInfo" />}
          message={
            <div style={{ paddingLeft: '16px' }}>
              <Typography variant="body2" style={{ fontWeight: 'bold' }}>
                <FormattedMessage id="m.invoice.package" />:
              </Typography>
              <Typography variant="body2">{data.benefitPackageSetting.title}</Typography>
              <Typography variant="body2" style={{ fontWeight: 'bold', marginTop: '12px' }}>
                <FormattedMessage id="m.description" />:
              </Typography>
              <Typography variant="body2">{data.benefitPackageSetting.feesDescription}</Typography>
              <Typography variant="body2" style={{ fontWeight: 'bold', marginTop: '12px' }}>
                <FormattedMessage id="m.packageInfo" />:
              </Typography>
              <Typography variant="body2">{data.benefitPackageSetting.noteMessage}</Typography>
            </div>
          }
          buttonMessageId={'accept'}
        />
      )}
    </div>
  );
};
export default connect(mapStateToProps)(ExportInvoiceRequestDetail);
