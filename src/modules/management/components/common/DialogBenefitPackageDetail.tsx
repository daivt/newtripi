import * as React from 'react';
import { Dialog, IconButton } from '@material-ui/core';
import { some } from '../../../../constants';
import IconClose from '@material-ui/icons/Clear';
interface Props {
  open: boolean;
  data: some;
  onClose(): void;
}

const DialogBenefitPackageDetail: React.FunctionComponent<Props> = props => {
  const { open, data, onClose } = props;
  return (
    <Dialog open={open} maxWidth="md" onClose={onClose} PaperProps={{}}>
      <div style={{ position: 'absolute', top: '8px', left: '8px' }}>
        <IconButton style={{ padding: '8px' }} size="small" onClick={onClose}>
          <IconClose />
        </IconButton>
      </div>
    </Dialog>
  );
};

export default DialogBenefitPackageDetail;
