import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import * as React from 'react';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import { GREY } from '../../../../colors';
import { Line } from '../../../common/components/elements';
import { DATE_FORMAT } from '../../../../constants';
import { renderMonthText } from '../../../common/utils';

interface ChoseDateParams {
  fromDate: string;
  toDate: string;
}
interface Props {
  open: boolean;
  onClose(): void;
  params: {
    fromDate: string;
    toDate: string;
  };
  onChange(params: ChoseDateParams): void;
}

const ChooseDateDialog: React.FunctionComponent<Props> = props => {
  const { onClose, open, onChange } = props;
  const [params, setParams] = React.useState<ChoseDateParams>(props.params);
  const [focusedInput, setFocusedInput] = React.useState<FocusedInputShape>('endDate');
  const valid =
    params.fromDate &&
    params.toDate &&
    moment(params.fromDate, DATE_FORMAT).isSameOrBefore(moment(params.toDate, DATE_FORMAT));
  return (
    <Dialog open={open} onClose={onClose}>
      <div>
        <Line style={{ justifyContent: 'center', margin: '16px' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.dashboard.aboutDay" />
          </Typography>
          <IconButton
            style={{
              position: 'absolute',
              padding: '0px',
              top: '8px',
              right: '8px',
            }}
            color="default"
            size="medium"
            onClick={onClose}
          >
            <CloseIcon style={{ color: GREY }} />
          </IconButton>
        </Line>
        <DayPickerRangeController
          daySize={36}
          renderMonthText={renderMonthText}
          numberOfMonths={2}
          startDate={params.fromDate ? moment(params.fromDate, DATE_FORMAT) : null}
          endDate={params.toDate ? moment(params.toDate, DATE_FORMAT) : null}
          onDatesChange={({ startDate, endDate }) => {
            setParams({
              toDate: endDate ? endDate.format(DATE_FORMAT) : '',
              fromDate: startDate ? startDate.format(DATE_FORMAT) : '',
            });
          }}
          onFocusChange={val => setFocusedInput(val || 'startDate')}
          focusedInput={focusedInput}
          minimumNights={0}
          isOutsideRange={date =>
            moment()
              .endOf('day')
              .isBefore(date)
          }
        />
      </div>
      <div
        style={{
          textAlign: 'end',
        }}
      >
        <Button
          style={{
            textTransform: 'none',
            marginTop: '8px',
            marginRight: '12px',
            marginBottom: '8px',
          }}
          size="large"
          color="secondary"
          variant="contained"
          disabled={!valid}
          onClick={() => {
            if (valid) {
              onChange(params);
            }
          }}
        >
          <FormattedMessage id="done" />
        </Button>
      </div>
    </Dialog>
  );
};

export default ChooseDateDialog;
