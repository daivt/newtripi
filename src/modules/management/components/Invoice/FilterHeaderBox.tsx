import { Typography } from '@material-ui/core';
import lodash from 'lodash';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { InvoiceParams, InvoiceStatusParams } from '.';
import { some } from '../../../../constants';
import { FreeTextField } from '../../../booking/components/Form';
import { CheckButton } from '../../../common/components/elements';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import SelectDateRange from '../Orders/SelectDateRange';
const FILTER = [
  { id: 'all', value: null },
  { id: 'm.invoice.open', value: ['open'] },
  { id: 'm.invoice.handling', value: ['handling'] },
  { id: 'm.invoice.completed', value: ['completed'] },
];
interface Props extends WrappedComponentProps {
  onChange: (params: InvoiceParams) => void;
  loading: boolean;
  params: InvoiceParams;
}

const FilterHeaderBox: React.FunctionComponent<Props> = props => {
  const { onChange, loading, params, intl } = props;
  const [input, setInput] = React.useState('');
  const setPaymentStatuses = React.useCallback<(obj: some, params: InvoiceParams) => void>(
    (obj: some, params: InvoiceParams) => {
      let status: InvoiceStatusParams[] | null;
      if (obj.id === 'all') {
        status = null;
      } else if (params.status && params.status.join(',').includes(obj.value.join(','))) {
        if (params.status.join(',') !== obj.value.join(',')) {
          status = params.status.filter(x => !obj.value.includes(x));
        } else {
          status = obj.value;
        }
      } else {
        status = params.status ? params.status.concat(obj.value) : obj.value;
      }
      const newParams = { ...params, status };
      onChange(newParams);
    },
    [onChange],
  );
  const onChangeInput = React.useCallback(
    lodash.debounce(
      (text: string) => {
        onChange({ ...params, term: text });
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [onChange, params],
  );

  const inputChange = React.useCallback(
    (text: string) => {
      setInput(text);
      onChangeInput(text);
    },
    [onChangeInput],
  );
  return (
    <div style={{ width: '100%' }}>
      <div style={{ display: 'flex', alignItems: 'center', padding: '20px 0' }}>
        <Typography variant="body2" style={{ marginRight: '9px' }}>
          <FormattedMessage id="m.status" />
        </Typography>
        {FILTER.map(obj => (
          <CheckButton
            loadingColor="primary"
            loading={loading}
            key={obj.id}
            active={
              params.status && obj.value
                ? params.status.join(',').includes(obj.value.join(','))
                : !params.status && !obj.value
                ? true
                : false
            }
            style={{ marginRight: '12px' }}
            onClick={() => setPaymentStatuses(obj, params)}
          >
            <Typography variant="body2">
              <FormattedMessage id={obj.id} />
            </Typography>
          </CheckButton>
        ))}
      </div>
      <Line style={{ width: '682px' }}>
        <FreeTextField
          style={{ marginRight: '30px' }}
          text={input}
          update={inputChange}
          header={<FormattedMessage id="m.invoice.findBusiness" />}
          optional
          placeholder={intl.formatMessage({ id: 'm.invoice.findBusinessPH' })}
          inputStyle={{ background: 'white' }}
        />
        <SelectDateRange
          checkInDate={params.startRequestTime || undefined}
          checkOutDate={params.endRequestTime || undefined}
          onChange={(checkInDate: string, checkOutDate: string) => {
            onChange({ ...params, startRequestTime: checkInDate, endRequestTime: checkOutDate });
          }}
          header={'m.invoice.timeRequest'}
          placeholder="m.orders.aboutDay"
        />
      </Line>
    </div>
  );
};
export default injectIntl(FilterHeaderBox);
