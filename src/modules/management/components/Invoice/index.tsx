import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { PAGE_SIZE, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { Line } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, Detail } from '../../constants';
import FilterHeaderBox from './FilterHeaderBox';
import InvoiceCard from './InvoiceCard';
import InvoiceDetail from './InvoiceDetail';

export type InvoiceStatusParams = 'open' | 'handling' | 'completed';
export interface InvoiceParams {
  endBookingTime: string | null;
  endRequestTime: string | null;
  module: string | null;
  pageSize: number;
  startBookingTime: string | null;
  startRequestTime: string | null;
  status: InvoiceStatusParams[] | null;
  term: string | null;
}
interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

const Invoice: React.FunctionComponent<Props> = props => {
  const { dispatch, router } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [params, setParams] = React.useState<InvoiceParams>({
    endBookingTime: null,
    endRequestTime: null,
    module: null,
    pageSize: PAGE_SIZE,
    startBookingTime: null,
    startRequestTime: null,
    status: null,
    term: null,
  });
  const [page, setPage] = React.useState(1);
  const [total, setTotal] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const searchState = router.location.search;
  const searchParams = new URLSearchParams(searchState);
  const action = searchParams.get(ACTION_PARAM_NAME) as Detail | null;
  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getVatinvoice}`,
        'post',
        true,
        JSON.stringify({ ...params, page: page + 1 }),
      ),
    );
    if (json.code === 200) {
      setData(b => b && b.concat(json.data));
      setPage(page + 1);
    }
    setLoading(false);
  }, [page, dispatch, params]);
  React.useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      setData([]);
      setPage(1);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getVatinvoice}`,
          'post',
          true,
          JSON.stringify({ ...params, page: 1 }),
        ),
      );
      if (json.code === 200) {
        setData(json.data);
        setTotal(json.total);
      } else {
        setTotal(0);
      }
      setLoading(false);
    };
    fetch();
  }, [dispatch, params]);

  return (
    <div style={{ padding: '30px 20px 42px 30px' }}>
      <div style={{ display: action ? 'none' : undefined }}>
        <Line style={{ justifyContent: 'space-between' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.invoiceManagement" />
          </Typography>
        </Line>
        <Line>
          <FilterHeaderBox
            params={params}
            loading={loading}
            onChange={params => setParams(params)}
          />
        </Line>
        {data ? (
          <div style={{ marginTop: '20px' }}>
            <table
              style={{
                width: '100%',
                borderCollapse: 'collapse',
              }}
            >
              <tbody>
                {data.map((one, index) => (
                  <InvoiceCard data={one} search={searchState} key={one.id} />
                ))}
              </tbody>
            </table>
            {!data.length && !loading && (
              <NoDataResult id="m.noData" style={{ marginTop: '48px' }} />
            )}
            {!loading && total - PAGE_SIZE * page > 0 && (
              <div style={{ textAlign: 'center', marginTop: '23px' }}>
                <Button onClick={loadMore}>
                  <Typography style={{ color: BLUE }}>
                    <FormattedMessage
                      id="result.displayMore"
                      values={{ num: total - PAGE_SIZE * page }}
                    />
                  </Typography>
                </Button>
              </div>
            )}
            {loading && <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />}
          </div>
        ) : (
          <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />
        )}
      </div>
      {action === 'detail' && <InvoiceDetail />}
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(Invoice));
