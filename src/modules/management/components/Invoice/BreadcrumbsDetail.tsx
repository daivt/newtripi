import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const BreadcrumbDetail: React.FunctionComponent<Props> = props => {
  const { state, dispatch, router } = props;

  const backToInvoiceList = React.useCallback(() => {
    const backableToInvoiceList = state ? state.backableToInvoiceList : 0;

    if (backableToInvoiceList) {
      dispatch(go(-backableToInvoiceList));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  }, [state, dispatch, router]);

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => backToInvoiceList()}
        >
          <FormattedMessage id="m.invoiceManagement" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.invoice.detail" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(BreadcrumbDetail);
