import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { InvoiceStatusParams } from '.';
import { GREY, HOVER_GREY, BLUE } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import Link from '../../../common/components/Link';
import { ACTION_PARAM_NAME, Detail } from '../../constants';

const Row = styled.tr`
  &:hover {
    background: ${HOVER_GREY};
  }
  height: 86px;
  border-top: 1px solid ${GREY};
  border-bottom: 1px solid ${GREY};
  padding: '0px 12px';
`;

interface Props {
  data: some;
  search: string;
}

const InvoiceCard: React.FunctionComponent<Props> = props => {
  const { data, search } = props;
  const status: InvoiceStatusParams = data.status;
  const seeDetail = React.useCallback(() => {
    const params = new URLSearchParams(search);
    params.set(ACTION_PARAM_NAME, 'detail' as Detail);
    return params.toString();
  }, [search]);
  return (
    <Row>
      <>
        <td style={{ width: '65%', paddingLeft: '12px' }}>
          <Typography variant="subtitle2">{data.companyName}</Typography>
          <Typography variant="caption">
            <FormattedMessage id="m.invoice.timeRequest" />
            :&nbsp;{data.requestTime}
          </Typography>
        </td>
        <td style={{ width: '20%', textAlign: 'center' }}>
          <Typography
            variant="body2"
            color={
              status === 'completed' ? 'primary' : status === 'handling' ? 'secondary' : 'error'
            }
          >
            <FormattedMessage
              id={
                status === 'completed'
                  ? 'm.invoice.completed'
                  : status === 'handling'
                  ? 'm.invoice.handling'
                  : 'm.invoice.open'
              }
            />
          </Typography>
        </td>
        <td style={{ width: '15%', textAlign: 'center' }}>
          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${seeDetail()}`,
              state: { detailDataInvoice: data, backableToInvoiceList: 1 },
            }}
          >
            <Button variant="text">
              <Typography variant="body2" style={{ color: BLUE }}>
                <FormattedMessage id="m.viewDetail" />
              </Typography>
            </Button>
          </Link>
        </td>
      </>
    </Row>
  );
};

export default InvoiceCard;
