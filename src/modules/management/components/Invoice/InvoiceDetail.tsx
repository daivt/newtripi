import { Button, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, GREY } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { Line } from '../../../common/components/elements';
import { NewTabLink } from '../../../common/components/NewTabLink';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  CURRENT_PARAM_NAME,
  FlightManagementBookingItemActionType,
  TAB_PARAM_NAME,
} from '../../constants';
import BreadcrumbsDetail from './BreadcrumbsDetail';
const Row = styled.tr`
  height: 42px;
`;
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const InvoiceDetail: React.FunctionComponent<Props> = props => {
  const { router } = props;
  const state = router.location.state;
  const data = state && state.detailDataInvoice;
  const backToInvoicePage = React.useCallback(() => {
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    return params.toString();
  }, [router]);
  const getLink = React.useCallback(() => {
    if (data.module === 'flight') {
      const params = new URLSearchParams(router.location.search);
      params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
      params.set(BOOKING_ID_PARAM_NAME, `${data.bookingId}`);
      params.set(CURRENT_PARAM_NAME, 'm.orders');
      params.set(TAB_PARAM_NAME, data.module);
      return `${ROUTES.management}?${params.toString()}`;
    }
    if (data.module === 'hotel') {
      const params = new URLSearchParams(router.location.search);
      params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
      params.set(BOOKING_ID_PARAM_NAME, `${data.bookingId}`);
      params.set(CURRENT_PARAM_NAME, 'm.orders');
      params.set(TAB_PARAM_NAME, data.module);
      return `${ROUTES.management}?${params.toString()}`;
    }
    return `${ROUTES.management}`;
  }, [data, router.location.search]);
  if (!state || !data) {
    return (
      <Redirect
        to={{
          pathname: ROUTES.management,
          search: `?${backToInvoicePage()}`,
        }}
      />
    );
  }
  return (
    <div>
      <Helmet>
        <title>{data.companyName}</title>
      </Helmet>
      <BreadcrumbsDetail />
      <Typography variant="h5" style={{ padding: '16px 0px' }}>
        <FormattedMessage id="m.invoice.detail" />
      </Typography>
      <table>
        <tbody>
          <Row>
            <td style={{ width: '150px' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.invoice.companyName" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.companyName}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="m.invoice.taxNumber" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.taxIdNumber}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="m.invoice.address" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.companyAddress}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="m.invoice.reciever" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.recipientName}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="m.invoice.address" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.recipientAddress}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="telephone" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2">{data.recipientPhone}</Typography>
            </td>
          </Row>
          <Row>
            <td>
              <Typography variant="body2">
                <FormattedMessage id="email" />:
              </Typography>
            </td>
            <td>
              <Typography variant="body2" style={{ color: BLUE }}>
                {data.recipientEmail}
              </Typography>
            </td>
          </Row>
          {data.benefitPackage && (
            <Row>
              <td>
                <Typography variant="body2">
                  <FormattedMessage id="m.invoice.package" />:
                </Typography>
              </td>
              <td>
                <Typography variant="body2" color="secondary">
                  {data.benefitPackage.title}
                </Typography>
              </td>
            </Row>
          )}
          {data.note && (
            <Row>
              <td>
                <Typography variant="body2">
                  <FormattedMessage id="note" />:
                </Typography>
              </td>
              <td>
                <Typography variant="body2">{data.note}</Typography>
              </td>
            </Row>
          )}
        </tbody>
      </table>
      <Typography variant="subtitle1" style={{ marginTop: '16px', marginBottom: '8px' }}>
        <FormattedMessage id="m.invoice.list" />
      </Typography>
      {data.outboundAttachLinks && data.outboundAttachLinks.length > 0 && (
        <>
          <Typography variant="subtitle2" style={{ marginTop: '16px', marginBottom: '8px' }}>
            <FormattedMessage id="m.invoice.outbound" />
          </Typography>
          {data.outboundAttachLinks.map((v: string, index: number) => (
            <div
              style={{ padding: '8px 0px', borderBottom: `1px solid ${GREY}`, width: '500px' }}
              key={v}
            >
              <NewTabLink href={v}>
                <Button variant="text" style={{ padding: '4px 0px' }}>
                  <Line style={{ justifyContent: 'space-between', width: '100px' }}>
                    <Typography variant="body2" style={{ color: BLUE }}>
                      <FormattedMessage id="m.invoice" /> &nbsp;
                      {index + 1}
                    </Typography>
                    <NavigateNextIcon fontSize="small" style={{ color: BLUE }} />
                  </Line>
                </Button>
              </NewTabLink>
            </div>
          ))}
        </>
      )}
      {data.inboundAttachLinks && data.inboundAttachLinks.length > 0 && (
        <>
          <Typography variant="subtitle2" style={{ marginTop: '32px', marginBottom: '8px' }}>
            <FormattedMessage id="m.invoice.inbound" />
          </Typography>
          {data.inboundAttachLinks.map((v: string, index: number) => (
            <div
              style={{ padding: '8px 0px', borderBottom: `1px solid ${GREY}`, width: '500px' }}
              key={v}
            >
              <NewTabLink href={v}>
                <Button variant="text" style={{ padding: '4px 0px' }}>
                  <Line style={{ justifyContent: 'space-between', width: '100px' }}>
                    <Typography variant="body2" style={{ color: BLUE }}>
                      <FormattedMessage id="m.invoice" /> &nbsp;
                      {index + 1}
                    </Typography>
                    <NavigateNextIcon fontSize="small" style={{ color: BLUE }} />
                  </Line>
                </Button>
              </NewTabLink>
            </div>
          ))}
        </>
      )}
      <NewTabLink href={getLink()}>
        <Button
          variant="contained"
          color="secondary"
          style={{ width: '170px', height: '40px', boxShadow: 'none', marginTop: '32px' }}
        >
          <Typography variant="subtitle2" style={{ color: 'white' }}>
            <FormattedMessage id="m.orderDetail" />
          </Typography>
        </Button>
      </NewTabLink>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

export default connect(mapStateToProps)(InvoiceDetail);
