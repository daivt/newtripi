import { FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { DateField, FreeTextField } from '../../../booking/components/Form';
import { ProfileInfo, ProfileInfoValidation } from '../../../common/models';
import { fetchAllCountries } from '../../../common/redux/reducer';
import { validTelephoneRegex } from '../../../common/utils';
import AsyncSelect from '../../../search/components/AsyncSelect';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  ticktock: boolean;
  profileInfo: ProfileInfo;
  profileInfoValidation: ProfileInfoValidation;
  updateProfileInfo(profileInfo: ProfileInfo, profileInfoValidation?: ProfileInfoValidation): void;
}

function getLabel(option: some) {
  return `${option.name}`;
}

function renderOptionLocation(option: some) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2">{option.name}</Typography>
    </div>
  );
}

const ProfileInfoForm: React.FC<Props> = props => {
  const {
    intl,
    profileInfo,
    updateProfileInfo,
    profileInfoValidation,
    dispatch,
    ticktock,
    allCountries,
  } = props;

  React.useEffect(() => {
    dispatch(fetchAllCountries());
  }, [dispatch]);

  return (
    <div style={{ maxWidth: '600px' }}>
      <div style={{ display: 'flex', alignItems: 'center', paddingTop: '8px' }}>
        <Typography variant="body2" style={{ marginRight: '24px' }}>
          <FormattedMessage id="gender" />
        </Typography>
        <RadioGroup row>
          <FormControlLabel
            value="m"
            control={<Radio size="small" />}
            checked={profileInfo.gender === 'm'}
            onChange={(e, checked) => checked && updateProfileInfo({ ...profileInfo, gender: 'm' })}
            label={<Typography variant="body2">{intl.formatMessage({ id: 'male' })}</Typography>}
          />
          <FormControlLabel
            style={{ paddingLeft: '10px' }}
            value="f"
            control={<Radio />}
            checked={profileInfo.gender === 'f'}
            onChange={(e, checked) => checked && updateProfileInfo({ ...profileInfo, gender: 'f' })}
            label={<Typography variant="body2">{intl.formatMessage({ id: 'female' })}</Typography>}
          />
        </RadioGroup>

        {!profileInfoValidation.gender && (
          <Typography variant="body2" color="error">
            <FormattedMessage id="m.profile.youNeedChooseGender" />
          </Typography>
        )}
      </div>

      <div style={{ paddingTop: '18px', paddingBottom: '12px', display: 'flex' }}>
        <div style={{ flex: 1, paddingRight: '20px' }}>
          <FreeTextField
            key={`name${ticktock}`}
            text={profileInfo.name}
            header={
              <span style={{ marginLeft: '-12px' }}>
                <FormattedMessage id="m.fullName" />
              </span>
            }
            valid={profileInfoValidation.name}
            inputStyle={{ height: '40px', background: '#fff' }}
            placeholder={intl.formatMessage({ id: 'tour.booking.fullNameEx' })}
            update={value =>
              updateProfileInfo(
                { ...profileInfo, name: value },
                { ...profileInfoValidation, name: true },
              )
            }
          />
        </div>
        <div style={{ flex: 1 }}>
          <Typography variant="body2" style={{ paddingBottom: '4px' }}>
            <FormattedMessage id="birthday" />
          </Typography>
          <DateField
            disableFuture
            key={`dateOfBirth${ticktock}`}
            date={profileInfo.dateOfBirth}
            inputStyle={{ height: '40px', background: '#fff' }}
            update={birthday =>
              updateProfileInfo(
                { ...profileInfo, dateOfBirth: birthday },
                { ...profileInfoValidation, dateOfBirth: true },
              )
            }
            valid={profileInfoValidation.dateOfBirth}
          />
        </div>
      </div>
      {/* <div>
        <FormControlLabel
          label={
            <Typography variant="body2">
              <FormattedMessage id="m.profile.companyRepresentative" />
            </Typography>
          }
          labelPlacement="end"
          value={profileInfo.isAgencyManager}
          onChange={(e, value) => updateProfileInfo({ ...profileInfo, isAgencyManager: value })}
          control={<Checkbox color="secondary" checked={profileInfo.isAgencyManager} />}
        />
        <FreeTextField
          key={`companyName${ticktock}`}
          inputStyle={{ height: '40px', background: '#fff' }}
          text={profileInfo.companyName}
          placeholder={intl.formatMessage({ id: 'm.profile.companyNameEx' })}
          update={value => updateProfileInfo({ ...profileInfo, companyName: value })}
        />
      </div> */}

      <div style={{ paddingTop: '12px', display: 'flex', justifyContent: 'space-between' }}>
        <div style={{ flex: 1, paddingRight: '8px' }}>
          <FreeTextField
            key={`address${ticktock}`}
            header={
              <span style={{ marginLeft: '-12px' }}>
                <FormattedMessage id="address" />
              </span>
            }
            inputStyle={{ height: '40px', background: '#fff' }}
            text={profileInfo.address}
            valid={profileInfoValidation.address}
            placeholder={intl.formatMessage({ id: 'auth.exampleEmail' })}
            update={value =>
              updateProfileInfo(
                { ...profileInfo, address: value },
                { ...profileInfoValidation, address: true },
              )
            }
          />
        </div>

        <div style={{ flex: 1, paddingRight: '8px' }}>
          <Typography variant="body2" style={{ marginLeft: '12px' }}>
            <FormattedMessage id="country" />
          </Typography>

          <AsyncSelect<some>
            error={false}
            minimizedWidth="100%"
            loadOptions={async (str: string) => {
              return [
                ...allCountries.filter(
                  obj => obj.name.toLowerCase().indexOf(str.toLocaleLowerCase()) !== -1,
                ),
              ];
            }}
            icon={<></>}
            optionHeader={''}
            focusOptionHeader={''}
            placeholder={''}
            defaultValue={profileInfo.countryName}
            focusOptions={allCountries}
            getLabel={getLabel}
            renderOption={renderOptionLocation}
            dropdownMaxHeight="300px"
            onSelect={country =>
              country && updateProfileInfo({ ...profileInfo, country, countryName: country.name })
            }
          />
        </div>
      </div>

      <div style={{ padding: '16px 0', display: 'flex', justifyContent: 'space-between' }}>
        <div style={{ flex: 1, paddingRight: '20px' }}>
          <FreeTextField
            key={`email${ticktock}`}
            header={
              <span style={{ marginLeft: '-12px' }}>
                <FormattedMessage id="m.email" />
              </span>
            }
            inputStyle={{ height: '40px', background: '#fff' }}
            text={profileInfo.email}
            valid={profileInfoValidation.email}
            placeholder={intl.formatMessage({ id: 'auth.exampleEmail' })}
            update={value =>
              updateProfileInfo(
                { ...profileInfo, email: value },
                { ...profileInfoValidation, email: true },
              )
            }
          />
        </div>

        <div style={{ flex: 1 }}>
          <FreeTextField
            key={`phone${ticktock}`}
            header={
              <span style={{ marginLeft: '-12px' }}>
                <FormattedMessage id="telephone" />
              </span>
            }
            inputStyle={{ height: '40px', background: '#fff' }}
            text={profileInfo.phone}
            valid={profileInfoValidation.phone}
            placeholder={intl.formatMessage({ id: 'telephoneEx' })}
            update={value => {}}
            inputProps={{ readOnly: true }}
            regex={validTelephoneRegex}
          />
        </div>
      </div>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { allCountries: state.common.allCountries };
}

export default connect(mapStateToProps)(injectIntl(ProfileInfoForm));
