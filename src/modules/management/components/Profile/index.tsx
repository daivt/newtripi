import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import { get } from 'js-cookie';
import { isEmpty } from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import {
  FormattedHTMLMessage,
  FormattedMessage,
  WrappedComponentProps,
  injectIntl,
} from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { DATE_FORMAT, ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import iconDeletePopup from '../../../../svg/ic_delete_ticket_alert.svg';
import { setUserData } from '../../../account/redux/accountReducer';
import { ACCESS_TOKEN } from '../../../auth/constants';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { snackbarSetting } from '../../../common/components/elements';
import LoadingButton from '../../../common/components/LoadingButton/index';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { defaultProfileInfoValidation, ProfileInfo } from '../../../common/models';
import { fetchThunk } from '../../../common/redux/thunks';
import { validateProfileInfo, validProfileInfo } from '../../utils';
import { AvatarBox } from './AvatarBox';
import ChangePasswordDialog from './ChangePasswordDialog';
import ProfileInfoForm from './ProfileInfoForm';
import { ACTION_PARAM_NAME, ProfileInfoActionType } from '../../constants';
import SmartOTP from './SmartOTP';
import Link from '../../../common/components/Link';

function mapState2Props(state: AppState) {
  return {
    userData: state.account.userData,
    router: state.router,
  };
}

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const Profile: React.FC<Props> = props => {
  const { userData, dispatch, intl, router } = props;

  const [loading, setLoading] = React.useState<boolean>(false);
  const [ticktock, setTicktock] = React.useState(false);
  const [showChangePassword, setShowChangePassword] = React.useState<boolean>(false);
  const [showDeleteAvatar, setShowDeleteAvatar] = React.useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [profileInfo, setProfileInfo] = React.useState<ProfileInfo>({
    gender:
      props.userData && props.userData.gender ? props.userData.gender.toLowerCase() : undefined,
    name: props.userData && props.userData.name ? props.userData.name : '',
    isAgencyManager: props.userData ? props.userData.isAgencyManager : false,
    companyName: '',
    country: props.userData && props.userData.country ? props.userData.country : {},
    countryName: props.userData && props.userData.countryName ? props.userData.countryName : '',
    address: props.userData && props.userData.address ? props.userData.address : '',
    email: props.userData && props.userData.emailInfo ? props.userData.emailInfo : '',
    phone: props.userData && props.userData.phoneInfo ? props.userData.phoneInfo : '',
    dateOfBirth:
      props.userData && props.userData.dateOfBirth
        ? moment(props.userData.dateOfBirth).format('L')
        : '',
  });

  const [profileInfoValidation, setProfileInfoValidation] = React.useState(
    defaultProfileInfoValidation,
  );

  const search = props.router.location.search;
  const params = new URLSearchParams(search);

  const action = params.get(ACTION_PARAM_NAME) as ProfileInfoActionType | null;

  const updateProfile = React.useCallback(async () => {
    const validation = validateProfileInfo(profileInfo);
    setProfileInfoValidation(validation);

    if (!validProfileInfo(validation)) {
      setTicktock(v => !v);
      return;
    }

    const birthday = moment(profileInfo.dateOfBirth, 'L', true);

    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.updateProfile,
        'post',
        true,
        JSON.stringify({
          profile_photo: profileInfo.profilePhoto ? profileInfo.profilePhoto : undefined,
          dateOfBirth: birthday.isValid() ? birthday.format(DATE_FORMAT) : undefined,
          country: !isEmpty(profileInfo.country) ? profileInfo.country.code : null,
          name: profileInfo.name,
          gender: profileInfo.gender ? profileInfo.gender.toUpperCase() : 'M',
          return_user_info: true,
          emailInfo: profileInfo.email,
          address: profileInfo.address,
          access_token: get(ACCESS_TOKEN) || '',
        }),
      ),
    );

    setLoading(false);
    if (json.code === 200) {
      dispatch(setUserData(json.data));
    }

    enqueueSnackbar(
      intl.formatMessage({ id: 'm.profile.updateSuccess' }),
      snackbarSetting(intl.formatMessage({ id: 'm.profile.updateSuccess' }), key =>
        closeSnackbar(key),
      ),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, intl, profileInfo]);

  const deleteAvatar = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.updateProfile,
        'post',
        true,
        JSON.stringify({
          profile_photo: null,
          return_user_info: true,
          access_token: get(ACCESS_TOKEN) || '',
        }),
      ),
    );

    setLoading(false);
    setShowDeleteAvatar(false);
    if (json.code === 200) {
      dispatch(setUserData(json.data));
    }

    enqueueSnackbar(
      intl.formatMessage({ id: 'm.profile.deleteAvatarSuccess' }),
      snackbarSetting(intl.formatMessage({ id: 'm.profile.deleteAvatarSuccess' }), key =>
        closeSnackbar(key),
      ),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, intl]);

  const goToSmartOTP = React.useCallback(() => {
    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'smartOTP' as ProfileInfoActionType);
    return params.toString();
  }, [router.location.search]);

  if (!userData) {
    return (
      <div style={{ padding: '32px' }}>
        <LoadingIcon />
      </div>
    );
  }

  if (action === 'smartOTP') {
    return <SmartOTP userData={userData} />;
  }

  return (
    <div style={{ padding: '30px 20px 42px 30px' }}>
      <Typography variant="h5">
        <FormattedMessage id="m.profile.update" />
      </Typography>

      <div
        style={{
          marginTop: '40px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <AvatarBox
          userData={userData}
          profileInfo={profileInfo}
          dispatch={dispatch}
          updateProfileInfo={profileInfo => setProfileInfo(profileInfo)}
          showConfirmDialog={() => setShowDeleteAvatar(true)}
        />

        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${goToSmartOTP()}`,
            }}
          >
            <Button
              variant="outlined"
              color="secondary"
              size="large"
              style={{ width: '140px', marginRight: '16px' }}
            >
              <Typography variant="button">
                <FormattedMessage id="smartOTP" />
              </Typography>
            </Button>
          </Link>

          <Button
            variant="contained"
            color="secondary"
            size="large"
            style={{ width: '200px' }}
            onClick={() => setShowChangePassword(true)}
          >
            <Typography variant="button">
              <FormattedMessage id="m.profile.changePassword" />
            </Typography>
          </Button>
        </div>
      </div>

      <ProfileInfoForm
        ticktock={ticktock}
        profileInfo={profileInfo}
        profileInfoValidation={profileInfoValidation}
        updateProfileInfo={(profileInfo, profileInfoValidation) => {
          setProfileInfo(profileInfo);
          profileInfoValidation && setProfileInfoValidation(profileInfoValidation);
        }}
      />

      <LoadingButton
        loading={loading}
        variant="contained"
        size="large"
        color="secondary"
        style={{ width: '170px', margin: '16px 0 72px' }}
        onClick={updateProfile}
      >
        <Typography variant="button">
          <FormattedMessage id="save" />
        </Typography>
      </LoadingButton>

      <ChangePasswordDialog
        dispatch={dispatch}
        show={showChangePassword}
        onClose={() => setShowChangePassword(false)}
        updateSuccess={message => {
          setTimeout(() => {
            dispatch(replace(ROUTES.login));
          }, 2000);
          enqueueSnackbar(
            message,
            snackbarSetting(message, key => {
              closeSnackbar(key);
            }),
          );
          setShowChangePassword(false);
        }}
      />

      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <img src={iconDeletePopup} alt="" />
            <Typography variant="body1" style={{ paddingTop: '16px' }}>
              <FormattedHTMLMessage id="m.profile.deleteAvatarContent" />
            </Typography>
          </div>
        }
        show={showDeleteAvatar}
        cancelMessageId="ignore"
        confirmMessageId="delete"
        onCancel={() => setShowDeleteAvatar(false)}
        onAccept={() => deleteAvatar()}
      />
    </div>
  );
};

export default connect(mapState2Props)(injectIntl(Profile));
