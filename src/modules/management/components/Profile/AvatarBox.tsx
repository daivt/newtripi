import { Avatar, ButtonBase, CircularProgress, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useDropzone } from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import iconDelete from '../../../../svg/delete.svg';
import iconCamera from '../../../../svg/ic_camera.svg';
import { uploadFile } from '../../../auth/redux/authThunks';
import { ProfileInfo } from '../../../common/models';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  userData: some;
  profileInfo: ProfileInfo;
  updateProfileInfo(profileInfo: ProfileInfo): void;
  showConfirmDialog(): void;
}

export const AvatarBox: React.FC<Props> = props => {
  const { userData, dispatch, profileInfo, updateProfileInfo, showConfirmDialog } = props;
  const { enqueueSnackbar } = useSnackbar();
  const { getRootProps, getInputProps } = useDropzone({
    noKeyboard: true,
    multiple: false,
    onDrop: acceptedFiles => uploadPhoto(acceptedFiles),
  });
  const [uploading, setUploading] = React.useState(false);

  const uploadPhoto = React.useCallback(
    async (files: File[]) => {
      setUploading(true);
      try {
        const json = await dispatch(uploadFile(files[0]));

        if (json.code === 200) {
          updateProfileInfo({ ...profileInfo, profilePhoto: json.photo.link });
        } else {
          const arr = JSON.parse(json.message);
          enqueueSnackbar(arr[0].value, {
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          });
        }
      } catch (err) {
        enqueueSnackbar('File too large', {
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        });
      }
      setUploading(false);
    },
    [dispatch, profileInfo, updateProfileInfo, enqueueSnackbar],
  );

  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <div style={{ position: 'relative', display: 'flex' }}>
        <Avatar
          alt={userData.name}
          src={profileInfo.profilePhoto ? profileInfo.profilePhoto : userData.profilePhoto}
          style={{ width: '128px', height: '128px' }}
        >
          <span style={{ fontSize: '42px' }}>
            {!profileInfo.profilePhoto &&
              !userData.profilePhoto &&
              userData.name
                .split(' ')
                .map((v: string) => v.substring(0, 1).toUpperCase())
                .join('')}
          </span>
        </Avatar>
        {uploading && (
          <div
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '50%',
              background: 'rgba(0, 0, 0, 0.3)',
            }}
          >
            <CircularProgress size={32} style={{ color: '#fff' }} />
          </div>
        )}
      </div>

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          paddingLeft: '10px',
        }}
      >
        <ButtonBase
          style={{
            borderRadius: '4px',
          }}
          {...getRootProps()}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              padding: '4px 16px',
              borderRadius: '4px',
            }}
          >
            <input {...getInputProps()} />
            <img src={iconCamera} style={{ width: '24px', height: '24px' }} alt="" />
            <Typography variant="body1" style={{ color: BLUE, paddingLeft: '10px' }}>
              <FormattedMessage id="m.profile.changeAvatar" />
            </Typography>
          </div>
        </ButtonBase>

        <ButtonBase
          style={{
            borderRadius: '4px',
            marginTop: '10px',
          }}
          onClick={() => showConfirmDialog()}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              padding: '4px 16px',
              borderRadius: '4px',
            }}
          >
            <img src={iconDelete} style={{ width: '24px', height: '24px' }} alt="" />
            <Typography variant="body1" color="textSecondary" style={{ paddingLeft: '10px' }}>
              <FormattedMessage id="m.profile.deleteAvatar" />
            </Typography>
          </div>
        </ButtonBase>
      </div>
    </div>
  );
};
