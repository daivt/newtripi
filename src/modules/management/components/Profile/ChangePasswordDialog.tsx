import { Button, Dialog, IconButton, Typography, FormHelperText } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../redux/reducers';
import { FreeTextField } from '../../../booking/components/Form';
import LoadingButton from '../../../common/components/LoadingButton/index';
import NewPasswordBox from '../../../common/components/NewPasswordBox';
import { fetchThunk } from '../../../common/redux/thunks';
import { API_PATHS } from '../../../../API';
import { ACCESS_TOKEN } from '../../../auth/constants';
import { get } from 'js-cookie';

interface ChangePasswordParams {
  oldPassword: string;
  password: string;
  confirmPassword: string;
}

interface ChangePasswordValidation {
  oldPassword: boolean;
  password: boolean;
  confirmPassword: boolean;
}

interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  show: boolean;
  onClose(): void;
  updateSuccess(message: string): void;
}

const defaultValidation: ChangePasswordValidation = {
  oldPassword: true,
  password: true,
  confirmPassword: true,
};

const ChangePasswordDialog: React.FC<Props> = props => {
  const { dispatch, show, onClose, intl, updateSuccess } = props;
  const [ticktock, setTick] = React.useState(false);
  const [params, setParams] = React.useState<ChangePasswordParams>({
    oldPassword: '',
    password: '',
    confirmPassword: '',
  });

  const [validation, setValidation] = React.useState<ChangePasswordValidation>(defaultValidation);
  const [loading, setLoading] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');

  const onChangePassword = React.useCallback(async () => {
    const validation: ChangePasswordValidation = {
      oldPassword: params.oldPassword.length >= 6,
      password: params.password.length >= 6,
      confirmPassword: params.password === params.confirmPassword,
    };

    setValidation(validation);

    if (!validation.password || !validation.oldPassword || !validation.confirmPassword) {
      setTick(!ticktock);

      return;
    }

    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.changePasswordNew,
        'post',
        true,
        JSON.stringify({
          password: params.password,
          old_password: params.oldPassword,
          access_token: get(ACCESS_TOKEN) || '',
        }),
      ),
    );
    setLoading(false);

    if (json.code === 200) {
      updateSuccess(json.message);
    } else if (json.code === 400) {
      setErrorMessage(json.message);
    }
  }, [
    dispatch,
    params.confirmPassword,
    params.oldPassword,
    params.password,
    updateSuccess,
    ticktock,
  ]);

  return (
    <Dialog open={show} fullWidth maxWidth="xs" onEnter={() => setValidation(defaultValidation)}>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <div style={{ display: 'flex', justifyContent: 'flex-end', flex: 1 }}>
            <IconButton style={{ margin: '5px' }} size="small" onClick={onClose}>
              <IconClose />
            </IconButton>
          </div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.profile.changePassword" />
          </Typography>
        </div>

        <div style={{ padding: '30px 12px 30px 30px' }}>
          <FreeTextField
            type="password"
            text={params.oldPassword}
            valid={validation.oldPassword}
            header={intl.formatMessage({ id: 'm.profile.oldPassword' })}
            placeholder="******"
            update={value => {
              setParams({ ...params, oldPassword: value });
              setValidation({ ...validation, oldPassword: true });
            }}
          />

          <FormHelperText error={true} style={{ paddingBottom: '18px' }}>
            {errorMessage && <Typography variant="caption">{errorMessage}</Typography>}
          </FormHelperText>

          <NewPasswordBox
            ticktock={ticktock}
            password={params.password}
            confirmPassword={params.confirmPassword}
            passwordValidation={validation.password}
            confirmPasswordValidation={validation.confirmPassword}
            updatePassword={(value, valid) => {
              setParams({ ...params, password: value });
              setValidation({ ...validation, password: valid });
            }}
            updateConfirmPassword={(value, valid) => {
              setParams({ ...params, confirmPassword: value });
              setValidation({ ...validation, confirmPassword: valid });
            }}
          />
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '0 30px 28px',
          }}
        >
          <LoadingButton
            loading={loading}
            style={{ width: '170px' }}
            variant="contained"
            color="secondary"
            size="large"
            onClick={onChangePassword}
          >
            <Typography variant="button">
              <FormattedMessage id="update" />
            </Typography>
          </LoadingButton>
          <Button style={{ width: '170px' }} variant="outlined" size="large" onClick={onClose}>
            <Typography variant="button" color="textSecondary">
              <FormattedMessage id="ignore" />
            </Typography>
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

export default injectIntl(ChangePasswordDialog);
