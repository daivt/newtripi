import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import { go, replace } from 'connected-react-router';
import { useSnackbar } from 'notistack';
import React, { useState, useCallback, useEffect } from 'react';
import {
  FormattedHTMLMessage,
  FormattedMessage,
  WrappedComponentProps,
  injectIntl,
} from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { PRIMARY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconCheckOutSuccess } from '../../../../../svg/ic_check_success.svg';
import { validateAccessToken } from '../../../../auth/redux/authThunks';
import { FreeTextField } from '../../../../booking/components/Form';
import { fetchThunk } from '../../../../common/redux/thunks';
import { validOTPRegex } from '../../../../common/utils';
import { ACTION_PARAM_NAME } from '../../../constants';
import SmartOTPBreadCrumbs from './SmartOTPBreadCrumbs';
import LoadingButton from '../../../../common/components/LoadingButton/index';

const CHANGE_SMART_OTP_STEP = {
  ENTER_PASSWORD: 'ENTER_PASSWORD',
  ENTER_VERIFY_CODE: 'ENTER_VERIFY_CODE',
  SUCCESS: 'SUCCESS',
};

const MAX_OTP_LENGTH = 4;

const StepIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 32px;
  height: 32px;
  background-color: ${PRIMARY};
  border-radius: 32px;
  margin-right: 12px;
  font-weight: 500;
  color: #fff;
`;

interface SmartOTPInfo {
  password: string;
  confirmPassword: string;
  otp: string;
}

interface SmartOTPInfoValidation {
  password: boolean;
  confirmPassword: boolean;
  otp: boolean;
}

const defaultInfo: SmartOTPInfo = {
  password: '',
  confirmPassword: '',
  otp: '',
};

const defaultValidation: SmartOTPInfoValidation = {
  password: true,
  confirmPassword: true,
  otp: true,
};

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  userData: some;
}

const SmartOTP: React.FC<Props> = props => {
  const { userData, dispatch, intl, router } = props;
  const [step, setStep] = useState(CHANGE_SMART_OTP_STEP.ENTER_PASSWORD);
  const [seconds, setSeconds] = useState(0);
  const [showDialog, setShowDialog] = useState(false);
  const [tick, setTick] = useState(false);
  const [loading, setLoading] = useState(false);
  const [info, setInfo] = useState<SmartOTPInfo>(defaultInfo);
  const [validation, setValidation] = useState<SmartOTPInfoValidation>(defaultValidation);
  const { enqueueSnackbar } = useSnackbar();

  const useCreditPassword = userData.useCreditPassword;

  const sendCreditPasswordOtp = useCallback(async () => {
    const validation: SmartOTPInfoValidation = {
      password: info.password.length === 4,
      confirmPassword: info.confirmPassword === info.password,
      otp: true,
    };

    setValidation(validation);
    setTick(v => !v);

    if (!validation.confirmPassword || !validation.password) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        useCreditPassword
          ? API_PATHS.sendChangeCreditPasswordOtp
          : API_PATHS.sendCreateCreditPasswordOtp,
        'get',
        true,
      ),
    );

    if (json.code === 200) {
      setSeconds(120);
      setStep(CHANGE_SMART_OTP_STEP.ENTER_VERIFY_CODE);
    }
  }, [dispatch, info.confirmPassword, info.password, useCreditPassword]);

  const onChangeCreatePassword = useCallback(async () => {
    const validation: SmartOTPInfoValidation = {
      password: true,
      confirmPassword: true,
      otp: info.otp.length >= 6,
    };

    setValidation(validation);
    setTick(v => !v);

    if (!validation.otp) {
      return;
    }

    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        useCreditPassword ? API_PATHS.changeCreditPassword : API_PATHS.createNewCreditPassword,
        'post',
        true,
        JSON.stringify({
          password: info.password,
          otp: info.otp,
        }),
      ),
    );
    setLoading(false);
    if (json.code === 200) {
      setStep(CHANGE_SMART_OTP_STEP.SUCCESS);
    } else {
      enqueueSnackbar(json.message, {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      });
    }
  }, [dispatch, enqueueSnackbar, info.otp, info.password, useCreditPassword]);

  useEffect(() => {
    if (seconds <= 0) {
      return;
    }
    const id = setInterval(() => setSeconds(seconds - 1), 1000);
    return () => clearInterval(id);
  }, [seconds]);

  return (
    <div style={{ padding: '30px 20px 42px 30px' }}>
      <SmartOTPBreadCrumbs useCreditPassword={useCreditPassword} />

      <Typography variant="h5" style={{ paddingTop: '16px' }}>
        <FormattedMessage id={useCreditPassword ? 'smartOTP' : 'm.profile.createSmartOTP'} />
      </Typography>
      <Typography variant="subtitle2" color="textSecondary" style={{ paddingTop: '16px' }}>
        <FormattedMessage
          id={
            useCreditPassword
              ? 'm.profile.createNewSmartOTPDescription'
              : 'm.profile.youNeedCreateSmartOTPForPaymentOnline'
          }
        />
      </Typography>
      <Typography variant="body2" color="textSecondary" style={{ paddingTop: '16px' }}>
        <FormattedHTMLMessage
          id={useCreditPassword ? 'm.profile.recoverSmartOTP' : 'm.profile.smartOTPDescription'}
        />
      </Typography>

      <div style={{ display: 'flex', alignItems: 'center', paddingTop: '16px' }}>
        <StepIcon>1</StepIcon>
        <Typography variant="body2" color="textSecondary">
          <FormattedMessage
            id={useCreditPassword ? 'm.profile.createNewCode' : 'm.profile.createNewSmartOTP'}
          />
        </Typography>
      </div>

      <div style={{ display: 'flex', alignItems: 'center', paddingTop: '16px' }}>
        <StepIcon>2</StepIcon>
        <Typography variant="body2" color="textSecondary">
          <FormattedMessage id="m.profile.enterCodeVerifyAndComplete" />
        </Typography>
      </div>

      <div style={{ display: 'flex', paddingTop: '36px' }}>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          style={{ width: '150px', marginRight: '16px' }}
          onClick={() => {
            setStep(CHANGE_SMART_OTP_STEP.ENTER_PASSWORD);
            setInfo(defaultInfo);
            setValidation(defaultValidation);
            setShowDialog(true);
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="continue" />
          </Typography>
        </Button>

        <Button
          variant="outlined"
          size="large"
          style={{ width: '150px' }}
          onClick={() => dispatch(go(-1))}
        >
          <Typography variant="button" color="textSecondary">
            <FormattedMessage id="ignore" />
          </Typography>
        </Button>
      </div>

      <Dialog
        open={showDialog}
        onClose={() => setShowDialog(false)}
        PaperProps={{
          style: {
            width: '450px',
            position: 'relative',
            padding: '24px',
          },
        }}
      >
        <div style={{ position: 'absolute', right: 0, top: 0 }}>
          <IconButton style={{ padding: '8px' }} size="small" onClick={() => setShowDialog(false)}>
            <IconClose />
          </IconButton>
        </div>

        {step !== CHANGE_SMART_OTP_STEP.SUCCESS && (
          <Typography variant="h6">
            <FormattedMessage
              id={useCreditPassword ? 'm.profile.changeSmartOTP' : 'm.profile.createSmartOTP'}
            />
          </Typography>
        )}

        {step === CHANGE_SMART_OTP_STEP.ENTER_PASSWORD && (
          <div style={{ padding: '8px' }}>
            <Typography variant="body1" color="textSecondary">
              <FormattedMessage
                id={useCreditPassword ? 'm.profile.enterNewSmartOTP' : 'm.profile.enterSmartOTP'}
              />
            </Typography>

            <FreeTextField
              key={`password${tick}`}
              style={{ width: '100%', marginTop: '24px' }}
              text={info.password}
              valid={validation.password}
              header={intl.formatMessage({ id: 'm.profile.enterSmartOTPInclude4Number' })}
              optional
              placeholder="****"
              update={password => {
                password.length <= MAX_OTP_LENGTH && setInfo({ ...info, password });
                setValidation({ ...validation, password: true });
              }}
              type="password"
              regex={validOTPRegex}
            />
            <FreeTextField
              key={`confirmPassword${tick}`}
              style={{ width: '100%', marginTop: '36px' }}
              text={info.confirmPassword}
              valid={validation.confirmPassword}
              header={intl.formatMessage({ id: 'm.profile.enterConfirmOTP' })}
              optional
              placeholder="****"
              update={confirmPassword => {
                confirmPassword.length <= MAX_OTP_LENGTH && setInfo({ ...info, confirmPassword });
                setValidation({ ...validation, confirmPassword: true });
              }}
              type="password"
              regex={validOTPRegex}
            />
            <div
              style={{
                display: 'flex',
                paddingTop: '36px',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ width: '150px', marginRight: '16px' }}
                onClick={() => sendCreditPasswordOtp()}
              >
                <Typography variant="button">
                  <FormattedMessage id="continue" />
                </Typography>
              </Button>

              <Button
                variant="outlined"
                size="large"
                style={{ width: '150px' }}
                onClick={() => setShowDialog(false)}
              >
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="ignore" />
                </Typography>
              </Button>
            </div>
          </div>
        )}

        {step === CHANGE_SMART_OTP_STEP.ENTER_VERIFY_CODE && (
          <div style={{ padding: '8px' }}>
            <Typography variant="body1" color="textSecondary">
              <FormattedMessage id="m.profile.enterVerifyCodeSendYourPhone" />
            </Typography>

            <div
              style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' }}
            >
              <FreeTextField
                key={`otp${tick}`}
                style={{ width: '100%', marginTop: '24px' }}
                text={info.otp}
                valid={validation.otp}
                header={intl.formatMessage({ id: 'm.profile.enterVerifyCode' })}
                placeholder="123456"
                update={otp => {
                  setInfo({ ...info, otp });
                  setValidation({ ...validation, otp: true });
                }}
              />

              <Button
                style={{
                  width: '115px',
                }}
                variant="contained"
                color="secondary"
                size="large"
                onClick={() => !seconds && sendCreditPasswordOtp()}
              >
                <Typography variant="subtitle2" style={{ padding: '3px 0' }}>
                  {seconds ? seconds : <FormattedMessage id="auth.mailResend" />}
                </Typography>
              </Button>
            </div>

            <div
              style={{
                display: 'flex',
                paddingTop: '36px',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <LoadingButton
                variant="contained"
                color="secondary"
                size="large"
                loading={loading}
                style={{ width: '150px', marginRight: '16px' }}
                onClick={() => onChangeCreatePassword()}
              >
                <Typography variant="button">
                  <FormattedMessage id="done" />
                </Typography>
              </LoadingButton>

              <Button
                variant="outlined"
                size="large"
                style={{ width: '150px' }}
                onClick={() => setShowDialog(false)}
              >
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="ignore" />
                </Typography>
              </Button>
            </div>
          </div>
        )}

        {step === CHANGE_SMART_OTP_STEP.SUCCESS && (
          <div
            style={{
              padding: '8px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <IconCheckOutSuccess style={{ width: '100px', height: '85px', margin: '32px 0' }} />
            <Typography variant="body1">
              <FormattedMessage
                id={
                  useCreditPassword
                    ? 'm.profile.changeSmartOTPSuccess'
                    : 'm.profile.crateSmartOTPSuccess'
                }
              />
            </Typography>

            <div
              style={{
                display: 'flex',
                paddingTop: '36px',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ width: '220px' }}
                onClick={() => {
                  const params = new URLSearchParams(router.location.search);
                  params.delete(ACTION_PARAM_NAME);
                  dispatch(validateAccessToken());
                  dispatch(replace({ search: params.toString() }));
                }}
              >
                <Typography variant="button">
                  <FormattedMessage id="m.profile.backToProfileDetail" />
                </Typography>
              </Button>
            </div>
          </div>
        )}
      </Dialog>
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(SmartOTP));
