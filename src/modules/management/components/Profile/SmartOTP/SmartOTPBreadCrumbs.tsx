import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../../constants';
import { replace } from 'connected-react-router';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  useCreditPassword: boolean;
}

const SmartOTPBreadCrumbs: React.FunctionComponent<Props> = props => {
  const { router, dispatch, useCreditPassword } = props;
  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => {
            const params = new URLSearchParams(router.location.search);
            params.delete(ACTION_PARAM_NAME);
            dispatch(replace({ search: params.toString() }));
          }}
        >
          <FormattedMessage id="m.profile" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id={useCreditPassword ? 'smartOTP' : 'm.profile.createSmartOTP'} />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router };
}

export default connect(mapStateToProps)(SmartOTPBreadCrumbs);
