import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  text: string;
}

const NotificationDetailBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state, dispatch, router, text } = props;
  const backToList = React.useCallback(() => {
    const backToList = state && state.backToList;
    if (backToList) {
      dispatch(go(-backToList));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    params.delete('id');
    dispatch(replace({ search: params.toString() }));
  }, [state, dispatch, router]);

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography variant="body2" color="textPrimary" style={{ cursor: 'pointer' }} onClick={() => backToList()}>
          <FormattedMessage id="m.notification" />
        </Typography>
        <Typography variant="body2" color="secondary">
          {text}
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(NotificationDetailBreadcrumbs);
