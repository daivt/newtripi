import * as React from 'react';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { ROUTES } from '../../../../constants';
import { CURRENT_PARAM_NAME, ACTION_PARAM_NAME } from '../../constants';
import { Typography, Button } from '@material-ui/core';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps, FormattedMessage } from 'react-intl';
import icDelete from '../../../../svg/delete.svg';
import iconDelete from '../../../../svg/ic_delete_ticket_alert.svg';
import { Line, snackbarSetting } from '../../../common/components/elements';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { go, replace } from 'connected-react-router';
import { fetchThunk } from '../../../common/redux/thunks';
import { API_PATHS } from '../../../../API';
import { Action } from 'typesafe-actions';
import { ThunkDispatch } from 'redux-thunk';
import { useSnackbar } from 'notistack';
import { fetchNotification } from '../../../account/redux/accountReducer';
import NotificationDetailBreadcrumbs from './NotificationDetailBreadcrumbs';
import { NewTabLink } from '../../../common/components/NewTabLink';

const mapStateToProps = (state: AppState) => {
  return {
    router: state.router,
  };
};
interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDeactivate(id: number): void;
  onRead(id: number): void;
}

const NotificationDetail: React.FunctionComponent<Props> = props => {
  const { router, intl, dispatch, onDeactivate, onRead } = props;
  const [showDelete, setShowDelete] = React.useState(false);
  const data = router.location.state && router.location.state.notificationDataDetail;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const backToList = React.useCallback(() => {
    const backToList = router.location.state && router.location.state.backToList;
    if (backToList) {
      dispatch(go(-1));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    params.delete('id');
    dispatch(replace({ search: params.toString() }));
  }, [dispatch, router]);

  const onDelete = React.useCallback(async () => {
    if (data) {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.deleteNotification}`, 'post', true, JSON.stringify({ id: data.id })),
      );
      if (json.code === 200) {
        onDeactivate(data.id);
        dispatch(fetchNotification());
        backToList();
      }
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
    }
  }, [backToList, closeSnackbar, data, dispatch, enqueueSnackbar, onDeactivate]);

  React.useEffect(() => {
    if (!data?.read && data) {
      onRead(data.id);
    }
  }, [data, onRead]);

  if (!data) {
    return <Redirect to={`${ROUTES.management}?${CURRENT_PARAM_NAME}=m.general`} />;
  }
  return (
    <div>
      <Helmet>
        <title>{`${intl.formatMessage({ id: 'm.notification' })} - ${data.message ? data.message.title : ''}`}</title>
      </Helmet>
      {data.banner && data.banner.desktopFile && (
        <img src={data.banner.desktopFile} alt="" style={{ width: '100%', height: '320px', objectFit: 'cover' }} />
      )}
      <div style={{ padding: '30px 20px 30px 30px' }}>
        <Line style={{ justifyContent: 'space-between' }}>
          <NotificationDetailBreadcrumbs text={data.message ? data.message.title : ''} />
          <Button variant="text" style={{ padding: '0px' }} onClick={() => setShowDelete(true)}>
            <img src={icDelete} alt="" />
            &nbsp;
            <Typography variant="subtitle2" color="textSecondary">
              <FormattedMessage id="m.notification.delete" />
            </Typography>
          </Button>
        </Line>
        <Typography variant="h5" style={{ padding: '16px 0px' }}>
          {data.message ? data.message.title : ''}
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {data.lastExecuteTime}
        </Typography>
        <Typography variant="subtitle2" style={{ padding: '16px 0px' }}>
          {data.message ? data.message.description : ''}
        </Typography>
        <div style={{ marginBottom: 24 }}>
          {data.message && data.message.content && <div dangerouslySetInnerHTML={{ __html: data.message.content }} />}
        </div>
        {data.landingPageUrl && (
          <NewTabLink href={data.landingPageUrl} style={{ marginTop: 24 }}>
            <Button variant="contained" color="secondary">
              <Typography variant="button">
                <FormattedMessage id="m.viewDetail" />
              </Typography>
            </Button>
          </NewTabLink>
        )}
      </div>
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <img src={iconDelete} alt="" style={{ height: '117px' }} />
            <Typography variant="h6">
              <FormattedMessage id="m.notification.deleteConfirm" />
            </Typography>
          </div>
        }
        show={showDelete}
        cancelMessageId="ignore"
        confirmMessageId="ok"
        onAccept={() => {
          onDelete();
          setShowDelete(false);
        }}
        onCancel={() => {
          setShowDelete(false);
        }}
      />
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(NotificationDetail));
