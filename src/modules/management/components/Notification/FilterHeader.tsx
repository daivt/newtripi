import { InputAdornment, Typography } from '@material-ui/core';
import * as React from 'react';
import lodash from 'lodash';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { ParamsNotification } from '.';
import { FreeTextField } from '../../../booking/components/Form';
import { CheckButton, Line } from '../../../common/components/elements';
import icSearch from '../../../../svg/ic_search.svg';
const FILTER = [
  { id: 'all', value: null },
  { id: 'm.notification.marketing', value: 'MARKETING' },
  { id: 'm.notification.system', value: 'SYSTEM' },
  { id: 'm.notification.creditChange', value: 'CREDIT_CHANGING' },
  { id: 'm.notification.exportInvoice', value: 'EXPORT_INVOICE' },
  { id: 'm.notification.advertising', value: 'ADVERTISING' },
  { id: 'm.notification.loyalty', value: 'LOYALTY' },
];
interface Props extends WrappedComponentProps {
  loading: boolean;
  params: ParamsNotification;
  onChange(params: ParamsNotification): void;
}
const FilterHeader: React.FunctionComponent<Props> = props => {
  const { loading, onChange, params, intl } = props;
  const [term, setTerm] = React.useState('');
  const onChangeInput = React.useCallback(
    lodash.debounce(
      (text: string) => {
        onChange({ ...params, term: text });
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [onChange, params],
  );

  const inputChange = React.useCallback(
    (text: string) => {
      setTerm(text);
      onChangeInput(text);
    },
    [onChangeInput],
  );
  return (
    <div>
      <Line style={{ marginTop: '20px', marginBottom: '8px' }}>
        <Typography variant="body2" style={{ marginRight: '16px', alignSelf: 'start' }}>
          <FormattedMessage id="m.notification.filter" />
        </Typography>
        <Line style={{ display: 'flex', flexWrap: 'wrap', marginRight: '50px' }}>
          {FILTER.map(obj => (
            <CheckButton
              loadingColor="primary"
              loading={loading}
              key={obj.id}
              active={params.type === obj.value}
              style={{ marginRight: '12px', marginBottom: '12px' }}
              onClick={() => onChange({ ...params, type: obj.value })}
            >
              <Typography variant="body2">
                <FormattedMessage id={obj.id} />
              </Typography>
            </CheckButton>
          ))}
        </Line>
      </Line>
      <FreeTextField
        inputStyle={{ width: '270px', background: 'white' }}
        text={term}
        update={inputChange}
        placeholder={intl.formatMessage({ id: 'm.notification.initText' })}
        header={intl.formatMessage({ id: 'm.notification.search' })}
        optional
        endAdornment={
          <InputAdornment position="end">
            <div style={{ padding: '8px' }}>
              <img
                src={icSearch}
                alt=""
                style={{
                  filter: 'brightness(1) invert(1)',
                  width: '24px',
                  height: '24px',
                  color: 'black',
                }}
              />
            </div>
          </InputAdornment>
        }
      />
    </div>
  );
};

export default injectIntl(FilterHeader);
