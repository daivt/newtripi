import { Button, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../API';
import { BLUE, GREY } from '../../../../colors';
import { PAGE_SIZE, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icDelete from '../../../../svg/delete.svg';
import iconDelete from '../../../../svg/ic_delete_ticket_alert.svg';
import NotificationCard from '../../../account/component/NotificationCard';
import { fetchNotification } from '../../../account/redux/accountReducer';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { snackbarSetting } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, NotificationActionType } from '../../constants';
import FilterHeader from './FilterHeader';
import NotificationDetail from './NotificationDetail';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
export interface ParamsNotification {
  type: string | null;
  term: string | null;
  pageSize: number;
}
const Notification: React.FunctionComponent<Props> = props => {
  const { dispatch, searchState } = props;
  const [data, setData] = React.useState<some | undefined>();
  const [params, setParams] = React.useState<ParamsNotification>({
    type: null,
    term: null,
    pageSize: PAGE_SIZE,
  });
  const [loading, setLoading] = React.useState<boolean>(false);
  const [page, setPage] = React.useState(1);
  const [total, setTotal] = React.useState(0);
  const [showDelete, setShowDelete] = React.useState(false);
  const [deactivateId, setDeactivateId] = React.useState(0);
  const paramsUrl = new URLSearchParams(searchState);
  const action = paramsUrl.get(ACTION_PARAM_NAME) as NotificationActionType | null;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(`${API_PATHS.getNotificationList}`, 'post', true, JSON.stringify({ ...params, page: page + 1 })),
    );
    if (json.code === 200) {
      setData({
        ...data,
        notificationList: data ? data.notificationList.concat(json.data.notificationList) : json.data.notificationList,
      });
      setPage(page + 1);
    }
    setLoading(false);
  }, [dispatch, params, page, data]);
  const fetch = React.useCallback(async () => {
    setLoading(true);
    setData(undefined);
    setPage(1);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getNotificationList}`,
        'post',
        true,
        JSON.stringify({ ...params, page: 1, pageSize: PAGE_SIZE }),
      ),
    );
    if (json.code === 200) {
      setData(json.data);
      setTotal(json.data.total);
    } else {
      setTotal(0);
    }
    setLoading(false);
  }, [dispatch, params]);
  const readAll = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(`${API_PATHS.readAllNotification}`, 'post', true, '{}'));
    if (json.code === 200) {
      fetch();
      dispatch(fetchNotification());
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, fetch]);
  const deleteAll = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(`${API_PATHS.deleteAllNotification}`, 'post', true, JSON.stringify({ type: params.type })),
    );
    if (json.code === 200) {
      fetch();
      dispatch(fetchNotification());
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, fetch, params.type]);
  React.useEffect(() => {
    fetch();
  }, [fetch, params, deactivateId]);
  const markRead = React.useCallback(
    (id: number) =>
      setData(data => ({
        ...data,
        notificationList:
          data &&
          data.notificationList.map((v: some) => {
            if (v.id === id) {
              return { ...v, read: true };
            }
            return v;
          }),
        numberNotiUnread: data && data.numberNotiUnread > 0 ? data.numberNotiUnread - 1 : 0,
      })),
    [],
  );
  return (
    <div>
      <div style={{ padding: '30px 20px 30px 30px', display: action ? 'none' : undefined }}>
        <Typography variant="h5">
          <FormattedMessage id="m.notification" />
        </Typography>
        <FilterHeader onChange={params => setParams(params)} params={params} loading={loading} />
        {data ? (
          <>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                padding: '12px 0px',
                justifyContent: 'space-between',
                borderBottom: `1px solid ${GREY}`,
                minHeight: '48px',
              }}
            >
              <div>
                {data.numberNotiUnread > 0 && (
                  <Button variant="text" style={{ padding: '0px' }} onClick={() => data.numberNotiUnread && readAll()}>
                    <Typography variant="body2" style={{ color: BLUE }}>
                      <FormattedMessage id="home.markReadAll" values={{ num: data.numberNotiUnread }} />
                    </Typography>
                  </Button>
                )}
              </div>
              {!params.term && !params.type && (
                <Button variant="text" style={{ padding: '0px' }} onClick={() => setShowDelete(true)}>
                  <img src={icDelete} alt="" />
                  &nbsp;
                  <Typography variant="subtitle2" color="textSecondary">
                    <FormattedMessage id="m.notification.deleteAll" />
                  </Typography>
                </Button>
              )}
            </div>
            {data.notificationList.map((val: some, index: number) => (
              <NotificationCard key={index} data={val} />
            ))}
            {!data.notificationList.length && !loading && (
              <NoDataResult id="m.notification.notFound" style={{ marginTop: '48px' }} />
            )}
            {!loading && total - PAGE_SIZE * page > 0 && (
              <div style={{ textAlign: 'center', marginTop: '23px' }}>
                <Button onClick={loadMore}>
                  <Typography style={{ color: BLUE }}>
                    <FormattedMessage id="result.displayMore" values={{ num: total - PAGE_SIZE * page }} />
                  </Typography>
                </Button>
              </div>
            )}
            {loading && <LoadingIcon style={{ textAlign: 'center', height: '160px' }} />}
          </>
        ) : (
          <LoadingIcon style={{ textAlign: 'center', height: '240px' }} />
        )}
        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" style={{ height: '117px' }} />
              <Typography variant="h6">
                <FormattedMessage id="m.notification.deleteAllConfirm" />
              </Typography>
            </div>
          }
          show={showDelete}
          cancelMessageId="ignore"
          confirmMessageId="ok"
          onAccept={() => {
            deleteAll();
            setShowDelete(false);
          }}
          onCancel={() => {
            setShowDelete(false);
          }}
        />
      </div>
      {action === 'detail' && <NotificationDetail onDeactivate={id => setDeactivateId(id)} onRead={markRead} />}
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    searchState: state.router.location.search,
  };
};
export default connect(mapStateToProps)(Notification);
