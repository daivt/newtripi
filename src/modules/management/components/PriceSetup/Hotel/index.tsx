import { Button, Radio, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { AppState } from '../../../../../redux/reducers';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../../common/redux/thunks';
import HotelCard from './HotelCard';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
interface HotelSetupData {
  hotelStar: number;
  id: number;
  percent: number;
  userId: number;
  value: number;
}
type settingType = 'generalSetting' | 'specificSetting';
const Hotel: React.FunctionComponent<Props> = props => {
  const { dispatch, userData } = props;
  const [option, setOption] = React.useState<settingType | undefined>();
  const [data, setData] = React.useState<HotelSetupData[] | undefined>();
  const [generalData, setGeneral] = React.useState<HotelSetupData | undefined>();
  const [showConfirm, setShowConfirm] = React.useState<boolean>(false);
  const [updatedId, setUpdatedID] = React.useState<boolean>(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const fetch = React.useCallback(async () => {
    setData(undefined);
    const json = await dispatch(fetchThunk(`${API_PATHS.getHotelMarkup}`, 'get', true));
    if (json.code === 200) {
      setData(
        json.data.length > 0
          ? json.data
          : [
              { hotelStar: 1, userId: userData && userData.id, percent: 0, value: 0 },
              { hotelStar: 2, userId: userData && userData.id, percent: 0, value: 0 },
              { hotelStar: 3, userId: userData && userData.id, percent: 0, value: 0 },
              { hotelStar: 4, userId: userData && userData.id, percent: 0, value: 0 },
              { hotelStar: 5, userId: userData && userData.id, percent: 0, value: 0 },
            ],
      );
      if (
        json.data.filter(
          (v: HotelSetupData) =>
            v.percent === json.data[0].percent && v.value === json.data[0].value,
        ).length === json.data.length
      ) {
        setOption('generalSetting');
        setGeneral(json.data.length > 0 ? json.data[0] : { hotelStar: 1, percent: 0, value: 0 });
      } else {
        setOption('specificSetting');
        setGeneral({ ...json.data[0], value: 0, percent: 0 });
      }
    }
  }, [dispatch, userData]);
  const update = React.useCallback(
    async (markups: HotelSetupData[]) => {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.changeHotelMarkup}`, 'post', true, JSON.stringify({ markups })),
      );
      if (json.code === 200) {
        setUpdatedID(!updatedId);
      }
      enqueueSnackbar(json.message, snackbarSetting(json.message, key => closeSnackbar(key)));
    },
    [dispatch, updatedId, enqueueSnackbar, closeSnackbar],
  );
  React.useEffect(() => {
    fetch();
  }, [fetch, dispatch, updatedId]);
  if (!data || !generalData) {
    return (
      <LoadingIcon
        style={{
          margin: '140px 0px',
        }}
      />
    );
  }
  return (
    <div style={{ width: '590px', padding: '20px 0px' }}>
      <div style={{ display: 'flex' }}>
        <Radio
          checked={option === 'generalSetting'}
          onChange={e => setOption('generalSetting')}
          style={{ alignSelf: 'flex-start', padding: '8px' }}
        />
        <div>
          <Typography variant="subtitle1">
            <FormattedMessage id="m.generalSetupTitle" />
          </Typography>
          <Typography
            variant="body2"
            color={option === 'generalSetting' ? 'textPrimary' : 'textSecondary'}
          >
            <FormattedMessage id="m.setup.appliesAll" />
          </Typography>
          <HotelCard
            percent={generalData.percent}
            value={generalData.value}
            disabled={option === 'specificSetting'}
            onChange={(percent, value) => {
              setGeneral({
                ...generalData,
                percent,
                value,
              });
            }}
          />
        </div>
      </div>

      <div style={{ marginTop: '42px', marginBottom: '30px' }}>
        <div style={{ display: 'flex' }}>
          <Radio
            checked={option === 'specificSetting'}
            onChange={e => setOption('specificSetting')}
            style={{ alignSelf: 'flex-start', padding: '8px' }}
          />
          <div>
            <Typography variant="subtitle1">
              <FormattedMessage id="m.privateSetupTitle" />
            </Typography>
            <Typography
              variant="body2"
              color={option === 'specificSetting' ? 'textPrimary' : 'textSecondary'}
            >
              <FormattedMessage id="m.setup.appliesSpecific" />
            </Typography>
            {data.map((val: HotelSetupData) => (
              <div style={{ display: 'flex' }} key={val.id}>
                <HotelCard
                  starNumber={val.hotelStar}
                  percent={val.percent}
                  value={val.value}
                  disabled={option === 'generalSetting'}
                  onChange={(percent, value) => {
                    setData(
                      data.map(v => {
                        if (val.id === v.id) {
                          return { ...v, percent, value };
                        }
                        return v;
                      }),
                    );
                  }}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
      <Typography variant="body2">
        <FormattedMessage id="m.setup.noteHotel" />
      </Typography>
      <Button
        variant="contained"
        color="secondary"
        style={{ marginTop: '20px', width: '169px', height: '40' }}
        onClick={() => setShowConfirm(true)}
      >
        <FormattedMessage id="save" />
      </Button>
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <Typography variant="body1">
              <FormattedMessage id="m.setup.confirmSave" />
            </Typography>
          </div>
        }
        show={showConfirm}
        cancelMessageId="notSave"
        confirmMessageId="save"
        onCancel={() => setShowConfirm(false)}
        onAccept={() => {
          if (option === 'generalSetting') {
            update(
              data.map(v => {
                return { ...v, percent: generalData.percent, value: generalData.value };
              }),
            );
          } else if (option === 'specificSetting') {
            update(data);
          }
          setShowConfirm(false);
        }}
      />
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    userData: state.account.userData,
  };
};
export default connect(mapStateToProps)(Hotel);
