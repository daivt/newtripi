import { InputAdornment, Typography } from '@material-ui/core';
import Start from '@material-ui/icons/StarBorderRounded';
import StartColor from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY } from '../../../../../colors';
import ic_add from '../../../../../svg/add.svg';
import { NumberTexField } from '../../../../booking/components/Form';
import { validTelephoneRegex } from '../../../../common/utils';
import { MAX_PERCENT, MAX_PRICE } from '../../../constants';
interface IHotelCardProps {
  disabled: boolean;
  percent: number;
  value: number;
  onChange: (percent: number, value: number) => void;
  starNumber?: number;
}

const HotelCard: React.FunctionComponent<IHotelCardProps> = props => {
  const { disabled, onChange, percent, value, starNumber } = props;
  return (
    <div style={{ display: 'flex', marginTop: '16px', alignItems: 'flex-end' }}>
      <div
        style={{
          width: '156px',
          minHeight: '40px',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        {starNumber ? (
          <Rating
            icon={
              disabled ? (
                <Start color="disabled" style={{ height: '20px' }} />
              ) : (
                <StartColor color="secondary" style={{ height: '20px' }} />
              )
            }
            value={null}
            max={starNumber}
            readOnly
          />
        ) : (
          <>
            {disabled ? (
              <Start color="disabled" style={{ height: '20px', marginRight: '8px' }} />
            ) : (
              <StartColor color="secondary" style={{ height: '20px', marginRight: '8px' }} />
            )}
            <Typography variant="body1" color={disabled ? 'textSecondary' : 'secondary'}>
              <FormattedMessage id="all" />
            </Typography>
          </>
        )}
      </div>

      <NumberTexField
        disabled={disabled}
        style={{ margin: '0px', flex: 0.75 }}
        text={percent}
        header={<FormattedMessage id="m.setup.value" />}
        update={text => {
          onChange(text < MAX_PERCENT ? text : MAX_PERCENT, value);
        }}
        optional
        endAdornment={
          <InputAdornment position="start" style={{ color: GREY }}>
            %
          </InputAdornment>
        }
      />
      <div style={{ width: '70px', textAlign: 'center', alignSelf: 'flex-end' }}>
        <img src={ic_add} alt="" />
      </div>
      <NumberTexField
        disabled={disabled}
        style={{ margin: '0px' }}
        text={value}
        header={<></>}
        regex={validTelephoneRegex}
        update={text => onChange(percent, text < MAX_PRICE ? text : MAX_PRICE)}
        optional
        endAdornment={
          <InputAdornment position="start" style={{ color: GREY }}>
            <FormattedMessage id="currency" />
          </InputAdornment>
        }
      />
    </div>
  );
};

export default HotelCard;
