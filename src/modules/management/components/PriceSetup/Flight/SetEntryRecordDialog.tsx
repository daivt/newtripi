import { Button, Dialog, IconButton, InputAdornment, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { GREY } from '../../../../../colors';
import { AppState } from '../../../../../redux/reducers';
import ic_add from '../../../../../svg/add.svg';
import airportSvg from '../../../../../svg/airport.svg';
import iconFlightDown from '../../../../../svg/ic_flight_down.svg';
import iconFlightUp from '../../../../../svg/ic_flight_up.svg';
import { NumberTexField } from '../../../../booking/components/Form';
import Swap from '../../../../common/components/Swap';
import { Airport } from '../../../../common/models';
import { fetchThunk } from '../../../../common/redux/thunks';
import { validTelephoneRegex } from '../../../../common/utils';
import AsyncSelect from '../../../../search/components/AsyncSelect';
import { MAX_PERCENT, MAX_PRICE, NotableAirportsSetingPrice } from '../../../constants';
import { validateFlightSetupParams, FlightSetupParamsError, FightSetupData } from '../../../utils';

interface Props extends WrappedComponentProps {
  data: FightSetupData;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  open: boolean;
  onClose(): void;
  onSubmit(data: FightSetupData): void;
  airport: Airline[];
}

export function getLabel(option: Airport) {
  return `${option.name} (${option.code})`;
}
export function getLabelAirline(option: Airline) {
  return `${option.value}`;
}
export interface Airline {
  key?: number;
  value: string;
}
export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2">
          {option.name} ({option.code})
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.location}
        </Typography>
      </div>
    </div>
  );
}
export function renderOptionAirline(option: Airline) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2" color="primary">
          {option.value}
        </Typography>
      </div>
    </div>
  );
}
const SetEntryRecordDialog: React.FunctionComponent<Props> = props => {
  const { dispatch, open, onClose, intl, onSubmit, airport } = props;
  const [data, setData] = React.useState<FightSetupData>(props.data);
  const [error, setError] = React.useState<FlightSetupParamsError>({
    fromAirportCode: false,
    toAirportCode: false,
    airlineId: false,
    price: false,
  });
  const [ticktock, setTick] = React.useState<boolean>(true);
  const [ticktock2, setTick2] = React.useState<boolean>(true);
  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="xs"
      style={{
        margin: '48px',
      }}
      PaperProps={{
        style: {
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          background: 'white',
          padding: '20px 18px 30px 18px',
          minWidth: '530px',
          overflowY: 'unset',
          alignSelf: 'flex-start',
          maxHeight: '100%',
        },
      }}
    >
      <div>
        <IconButton
          style={{ padding: '8px', position: 'absolute', top: '10px', right: '10px' }}
          size="small"
          onClick={onClose}
        >
          <IconClose />
        </IconButton>
        <Typography variant="h6" style={{ textAlign: 'center', marginBottom: '24px' }}>
          <FormattedMessage id="m.privateSetupTitle" />
        </Typography>
        <div
          style={{
            display: 'flex',
            minWidth: '430px',
            height: '86px',
            alignItems: 'flex-end',
          }}
        >
          <AsyncSelect<Airline>
            key={`airlineId${ticktock}`}
            error={error.airlineId}
            minimizedWidth="100%"
            loadOptions={async (str: string) => {
              if (str.trim()) {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirlines(str)));
                return json.data;
              }
              return airport;
            }}
            icon={<div />}
            title={<FormattedMessage id="m.setup.choosedAirplane" />}
            optionHeader={<FormattedMessage id="m.setup.choosedAirplane" />}
            focusOptionHeader={<FormattedMessage id="m.setup.popularAirplane" />}
            placeholder={intl.formatMessage({ id: 'm.setup.palcehoderAirplane' })}
            focusOptions={airport}
            defaultValue={data.title}
            getLabel={getLabelAirline}
            renderOption={renderOptionAirline}
            dropdownMaxHeight="300px"
            onSelect={airport => {
              setError({ ...error, airlineId: false, fromAirportCode: false });
              setData({
                ...data,
                airlineId: airport && airport.key !== -1 ? airport.key : undefined,
                title: airport ? getLabelAirline(airport) : '',
              });
            }}
          />
        </div>
        <div
          style={{
            display: 'flex',
            minWidth: '430px',
            height: '86px',
            alignItems: 'flex-end',
            position: 'relative',
          }}
          key={`date${ticktock}${ticktock2}`}
        >
          <AsyncSelect<Airport>
            error={error.fromAirportCode}
            minimizedWidth="224px"
            loadOptions={async (str: string) => {
              if (str.trim()) {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightUp} />}
            title={<FormattedMessage id="search.chooseOrigin" />}
            optionHeader={<FormattedMessage id="search.airports" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseOrigin' })}
            focusOptions={NotableAirportsSetingPrice}
            getLabel={getLabel}
            renderOption={renderOption}
            defaultValue={
              data.fromAirportName && data.fromAirportCode
                ? getLabel({
                    location: '',
                    code: data.fromAirportCode,
                    name: data.fromAirportName,
                  })
                : ''
            }
            dropdownMaxHeight="280px"
            onSelect={airport => {
              setError({ ...error, fromAirportCode: false, toAirportCode: false });
              setData({
                ...data,
                fromAirportCode: airport ? airport.code : undefined,
                fromAirportName: airport ? airport.name : '',
              });
            }}
          />
          <div
            style={{
              padding: '8px 0',
              width: '0px',
              overflow: 'visible',
              position: 'relative',
              zIndex: 10,
              display: 'flex',
              justifyContent: 'center',
              height: '40px',
              boxSizing: 'content-box',
              alignItems: 'center',
            }}
          >
            <Swap
              onClick={() => {
                setData({
                  ...data,
                  fromAirportCode: data.toAirportCode,
                  fromAirportName: data.toAirportName,
                  toAirportName: data.fromAirportName,
                  toAirportCode: data.fromAirportCode,
                });
                setError({
                  ...error,
                  fromAirportCode: error.toAirportCode,
                  toAirportCode: error.fromAirportCode,
                });
                setTick2(!ticktock2);
              }}
            />
          </div>
          <AsyncSelect<Airport>
            error={error.toAirportCode}
            minimizedWidth="224px"
            loadOptions={async (str: string) => {
              if (str.trim()) {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightDown} />}
            title={<FormattedMessage id="search.chooseDestination" />}
            optionHeader={<FormattedMessage id="search.airports" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseDestination' })}
            focusOptions={NotableAirportsSetingPrice}
            getLabel={getLabel}
            defaultValue={
              data.toAirportName && data.toAirportCode
                ? getLabel({
                    location: '',
                    code: data.toAirportCode,
                    name: data.toAirportName,
                  })
                : ''
            }
            renderOption={renderOption}
            dropdownMaxHeight="280px"
            onSelect={airport => {
              setError({ ...error, toAirportCode: false });
              setData({
                ...data,
                toAirportName: airport ? airport.name : '',
                toAirportCode: airport ? airport.code : undefined,
              });
            }}
          />
        </div>
        <div
          style={{ display: 'flex', justifyItems: 'flex-end', height: '86px', padding: '8px 12px' }}
        >
          <NumberTexField
            key={`price1${ticktock}`}
            valid={!error.price}
            style={{ margin: '0px' }}
            text={data.markupPercent}
            header={<FormattedMessage id="m.setup.value" />}
            regex={validTelephoneRegex}
            update={text => {
              setError({ ...error, price: false });
              setData({
                ...data,
                markupPercent: text < MAX_PERCENT ? text : MAX_PERCENT,
              });
            }}
            optional
            endAdornment={<InputAdornment position="start">%</InputAdornment>}
          />
          <div style={{ width: '70px', textAlign: 'center', alignSelf: 'flex-end' }}>
            <img src={ic_add} alt="" />
          </div>
          <NumberTexField
            key={`price2${ticktock}`}
            valid={!error.price}
            style={{ margin: '0px' }}
            text={data.markupAmount}
            header={<></>}
            regex={validTelephoneRegex}
            update={text => {
              setError({ ...error, price: false });
              setData({
                ...data,
                markupAmount: text < MAX_PRICE ? text : MAX_PRICE,
              });
            }}
            optional
            endAdornment={
              <InputAdornment position="start" style={{ color: GREY }}>
                <FormattedMessage id="currency" />
              </InputAdornment>
            }
          />
        </div>
        <div style={{ marginTop: '40px', display: 'flex', justifyContent: 'center' }}>
          <Button
            variant="contained"
            color="secondary"
            style={{ height: '40px', width: '160px', marginRight: '20px' }}
            onClick={() => {
              const error = validateFlightSetupParams(data);
              if (error.airlineId || error.fromAirportCode || error.price || error.toAirportCode) {
                setError(error);
                setTick(!ticktock);
              } else {
                onSubmit(data);
              }
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="update" />
            </Typography>
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            style={{ height: '40px', width: '160px' }}
            onClick={onClose}
          >
            <Typography variant="body2">
              <FormattedMessage id="ignore" />
            </Typography>
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

export default connect()(injectIntl(SetEntryRecordDialog));
