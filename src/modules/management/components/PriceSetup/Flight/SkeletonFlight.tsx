import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { GREY } from '../../../../../colors';
const Card = () => (
  <div
    style={{
      boxShadow:
        '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
      borderRadius: '4px',
      background: 'white',
      width: '100%',
      height: '105px',
      padding: '12px',
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'column',
    }}
  >
    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
      <div style={{ display: 'flex' }}>
        <Skeleton height={20} width={30} />
        <Skeleton height={20} width={104} style={{ marginLeft: '8px' }} />
      </div>{' '}
      <div style={{ display: 'flex' }}>
        <Skeleton height={20} width={152} />
        <Skeleton
          style={{
            minWidth: '24px',
            minHeight: '24px',
            height: '24px',
            alignSelf: 'center',
            padding: 0,
            marginLeft: '20px',
          }}
        />
      </div>
    </div>
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <div style={{ display: 'flex', alignItems: 'center', flex: 1 }}>
        <Skeleton height={20} width={132} />
        <div style={{ display: 'flex', alignItems: 'center', flex: 1, padding: '0px 1px' }}>
          <div style={{ border: `1px solid  ${GREY}`, flex: 1, margin: '0 10px' }} />
          <Skeleton height={24} width={24} variant="circle" />
          <div style={{ border: `1px solid  ${GREY}`, flex: 1, margin: '0 10px' }} />
        </div>
        <Skeleton height={20} width={132} />
      </div>
      <Skeleton
        style={{
          minWidth: '24px',
          minHeight: '24px',
          height: '24px',
          alignSelf: 'center',
          padding: 0,
          marginLeft: '20px',
        }}
      />
    </div>
  </div>
);

interface Props {}

const SketetonFlight: React.FunctionComponent<Props> = props => {
  return (
    <div style={{ width: '670px', padding: '20px 0px' }}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div
          style={{
            width: '320px',
            height: '90px',
            borderRadius: '4px',
            background: 'white',
            display: 'flex',
            padding: '20px 20px 20px 10px',
            border: `1px solid ${GREY}`,
          }}
        >
          <Skeleton width={48} height={48} style={{ alignSelf: 'center' }} />
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between ',
              flex: 1,
              marginLeft: '12px',
            }}
          >
            <Skeleton width={52} style={{ margin: '0px' }} />
            <Skeleton width={104} style={{ margin: '0px' }} />
          </div>
          <Skeleton
            style={{
              minWidth: '24px',
              minHeight: '24px',
              height: '24px',
              alignSelf: 'center',
              padding: 0,
            }}
          />
        </div>
        <div
          style={{
            width: '320px',
            height: '90px',
            borderRadius: '4px',
            background: 'white',
            display: 'flex',
            padding: '20px 20px 20px 10px',
            border: `1px solid ${GREY}`,
          }}
        >
          <Skeleton width={48} height={48} style={{ alignSelf: 'center' }} />
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between ',
              flex: 1,
              marginLeft: '12px',
            }}
          >
            <Skeleton width={52} style={{ margin: '0px' }} />
            <Skeleton width={104} style={{ margin: '0px' }} />
          </div>
          <Skeleton
            style={{
              minWidth: '24px',
              minHeight: '24px',
              height: '24px',
              alignSelf: 'center',
              padding: 0,
            }}
          />
        </div>
      </div>
      <Skeleton
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginTop: '32px',
          marginRight: '12px',
        }}
        height={23}
        width={162}
      />
      {Array(3)
        .fill(0)
        .map((v, index) => (
          <div key={index} style={{ marginTop: '10px' }}>
            <Card />
          </div>
        ))}
    </div>
  );
};

export default SketetonFlight;
