import { Button, Checkbox, IconButton, Typography } from '@material-ui/core';
import { pick } from 'lodash';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import iconDelete from '../../../../../svg/ic_delete_ticket_alert.svg';
import { ReactComponent as Website } from '../../../../../svg/website.svg';
import { ReactComponent as Pin } from '../../../../../svg/ic_pin.svg';
import menu_dot from '../../../../../svg/menu_dot.svg';
import profile_edit from '../../../../../svg/profile_edit_secondary.svg';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import { snackbarSetting } from '../../../../common/components/elements';
import { fetchThunk } from '../../../../common/redux/thunks';
import FlightCard from './FlightCard';
import SetEntryDefaultDialog, { Airline } from './SetEntryDefaultDialog';
import SetEntryRecordDialog from './SetEntryRecordDialog';
import SketetonFlight from './SkeletonFlight';
import { FightSetupData } from '../../../utils';
export interface Props {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
export interface State {
  data?: FightSetupData[];
}

type typeData = 'CommonPriceSetting' | 'EntrySettingDialog';
type typeMarkup = 'DOMESTIC' | 'INTERNATIONAL';
function defaultMarkup(type: typeMarkup) {
  return {
    airlineId: null,
    fromAirportCode: 'DOMESTIC',
    markupAmount: 0,
    markupPercent: 0,
    toAirportCode: type === 'DOMESTIC' ? 'DOMESTIC' : 'INTERNATIONAL',
  };
}
const Flight: React.FunctionComponent<Props> = props => {
  const [data, setData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [deletionList, setDeletionList] = React.useState<number[]>([]);
  const [priceDialogData, setPriceDialogData] = React.useState<FightSetupData | undefined>();
  const [type, setType] = React.useState<typeData | undefined>();
  const [option, setOption] = React.useState<boolean>(false);
  const [showConfirm, setShowConfirm] = React.useState<boolean>(false);
  const [deactivateId, setDeactivateId] = React.useState<number[]>([]);
  const [updatedId, setUpdatedId] = React.useState<boolean>(false);
  const [airport, setAirport] = React.useState<Airline[]>([]);
  const { dispatch } = props;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const fetch = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(fetchThunk(`${API_PATHS.getFlightMarkupOfBooker}`, 'get', true));
    if (json.code === 200) {
      setData({
        ...json.data,
        domesticMarkup: json.data.domesticMarkup || defaultMarkup('DOMESTIC'),
        internationalMarkup: json.data.internationalMarkup || defaultMarkup('INTERNATIONAL'),
      });
      setOption(false);
    }
    setLoading(false);
  }, [dispatch]);

  const update = React.useCallback(
    async (params: some) => {
      const json = await dispatch(
        fetchThunk(API_PATHS.saveFlightMarkupOfBooker, 'post', true, JSON.stringify(params)),
      );
      if (json.code === 200) {
        setUpdatedId(!updatedId);
        setOption(false);
        setDeletionList([]);
      }
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
    },
    [dispatch, updatedId, closeSnackbar, enqueueSnackbar],
  );
  const deleteFn = React.useCallback(async () => {
    if (deletionList.length > 0) {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.deleteFlightMarkupOfBooker}?id=${deletionList.join('%2C')}`,
          'delete',
          true,
        ),
      );
      if (json.code === 200) {
        setDeactivateId(deletionList);
        setDeletionList([]);
      }
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
    }
  }, [dispatch, deletionList, closeSnackbar, enqueueSnackbar]);
  React.useEffect(() => {
    fetch();
  }, [fetch, dispatch, deactivateId, updatedId]);
  React.useEffect(() => {
    const fetchdata = async () => {
      const json = await dispatch(fetchThunk(API_PATHS.searchAirlines('')));
      setAirport([{ key: undefined, value: 'Tất cả' }, ...json.data]);
    };
    fetchdata();
  }, [dispatch]);
  if (!data || loading) {
    return <SketetonFlight />;
  }
  return (
    <div style={{ width: '670px', padding: '20px 0px' }}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div
          style={{
            width: '320px',
            height: '90px',
            borderRadius: '4px',
            background: 'white',
            display: 'flex',
            padding: '20px 20px 20px 10px',
            border: `1px solid ${GREY}`,
          }}
        >
          <Pin style={{ width: '48px', height: '48px' }} />
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between ',
              flex: 1,
              marginLeft: '12px',
            }}
          >
            <Typography variant="subtitle2">
              {(data.domesticMarkup.toAirportCode as typeMarkup) === 'DOMESTIC' ? (
                <FormattedMessage id="m.domestic" />
              ) : (
                <FormattedMessage id="m.international" />
              )}
            </Typography>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
                <FormattedNumber value={data.domesticMarkup.markupPercent} />
                %&nbsp;+&nbsp;
              </Typography>
              <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
                <FormattedNumber value={data.domesticMarkup.markupAmount} />
                <FormattedMessage id="currency" />
              </Typography>
            </div>
          </div>
          <Button
            variant="text"
            style={{
              minWidth: '24px',
              minHeight: '24px',
              height: '24px',
              alignSelf: 'center',
              padding: 0,
            }}
            onClick={() => {
              setPriceDialogData(data.domesticMarkup);
              setType('CommonPriceSetting');
            }}
          >
            <img src={profile_edit} alt="" />
          </Button>
        </div>
        <div
          style={{
            width: '320px',
            height: '90px',
            borderRadius: '4px',
            background: 'white',
            display: 'flex',
            padding: '20px 20px 20px 10px',
            border: `1px solid ${GREY}`,
          }}
        >
          <Website style={{ width: '48px' }} />
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              flex: 1,
              marginLeft: '12px',
            }}
          >
            <Typography variant="subtitle2">{data.internationalMarkup.title}</Typography>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
                <FormattedNumber value={data.internationalMarkup.markupPercent} />
                %&nbsp;+&nbsp;
              </Typography>
              <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
                <FormattedNumber value={data.internationalMarkup.markupAmount} />
                <FormattedMessage id="currency" />
              </Typography>
            </div>
          </div>
          <Button
            variant="text"
            style={{
              minWidth: '24px',
              minHeight: '24px',
              height: '24px',
              alignSelf: 'center',
              padding: 0,
            }}
            onClick={() => {
              setPriceDialogData(data.internationalMarkup);
              setType('CommonPriceSetting');
            }}
          >
            <img src={profile_edit} alt="" />
          </Button>
        </div>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginTop: '32px',
          marginRight: '12px',
        }}
      >
        <Typography variant="h6">
          <FormattedMessage id="m.privateSetupTitle" />
          {data.additionalMarkup.length > 0 && ` (${data.additionalMarkup.length})`}
        </Typography>
        {data.additionalMarkup.length > 0 && (
          <>
            {option ? (
              <Checkbox
                style={{ padding: 0 }}
                color="secondary"
                checked={deletionList.length === data.additionalMarkup.length}
                onChange={() => {
                  if (deletionList.length === data.additionalMarkup.length) {
                    setDeletionList([]);
                  } else {
                    setDeletionList(data.additionalMarkup.map((v: some) => v.id));
                  }
                }}
              />
            ) : (
              <IconButton style={{ padding: '0px' }} onClick={() => setOption(true)}>
                <img src={menu_dot} alt="" style={{ height: '27px' }} />
              </IconButton>
            )}
          </>
        )}
      </div>

      {data.additionalMarkup.length > 0 ? (
        data.additionalMarkup.map((v: FightSetupData) => (
          <div key={v.id} style={{ marginTop: '10px' }}>
            <FlightCard
              data={v}
              check={deletionList.some((val: number) => val === v.id)}
              onClick={() => {
                setPriceDialogData(v);
                setType('EntrySettingDialog');
              }}
              onCheck={() => {
                if (deletionList.some((val: number) => val === v.id)) {
                  setDeletionList(deletionList.filter((val: number) => val !== v.id));
                } else {
                  v.id && setDeletionList([...deletionList, v.id]);
                }
              }}
              showOption={option}
            />
          </div>
        ))
      ) : (
        <div style={{ marginTop: '16px', width: '400px' }}>
          <Typography variant="body2" color="error">
            <FormattedMessage id="m.setup.note1" />
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.setup.note2" />
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.setup.note3" />
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.setup.note4" />
          </Typography>
        </div>
      )}
      <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px' }}>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setPriceDialogData({ markupAmount: 0, markupPercent: 0 });
            setType('EntrySettingDialog');
          }}
          style={{ width: '144px', height: '40px' }}
        >
          <Typography variant="subtitle2">
            <FormattedMessage id="m.setup.add" />
          </Typography>
        </Button>
        {option && data.additionalMarkup.length > 0 && (
          <div style={{ display: 'flex' }}>
            <Button
              variant="outlined"
              onClick={() => {
                setOption(false);
                setDeletionList([]);
              }}
              style={{ width: '144px', height: '40px', marginRight: '20px' }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="cancel" />
              </Typography>
            </Button>
            <Button
              variant="outlined"
              color="secondary"
              disabled={deletionList.length === 0}
              onClick={() => {
                if (deletionList.length > 0) setShowConfirm(true);
              }}
              style={{ width: '144px', height: '40px' }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="delete" />
                {deletionList.length > 0 && `(${deletionList.length})`}
              </Typography>
            </Button>
          </div>
        )}
      </div>
      {priceDialogData && (
        <>
          {type === 'EntrySettingDialog' && (
            <SetEntryRecordDialog
              airport={airport}
              data={priceDialogData}
              open={!!priceDialogData}
              onClose={() => setPriceDialogData(undefined)}
              onSubmit={data => {
                const v = pick(data, [
                  'id',
                  'airlineId',
                  'fromAirportCode',
                  'markupAmount',
                  'markupPercent',
                  'toAirportCode',
                ]);
                setPriceDialogData(undefined);
                update(v);
              }}
            />
          )}
          {type === 'CommonPriceSetting' && (
            <SetEntryDefaultDialog
              data={priceDialogData}
              open={!!priceDialogData}
              onClose={() => setPriceDialogData(undefined)}
              onSubmit={data => {
                const v = pick(data, [
                  'id',
                  'airlineId',
                  'fromAirportCode',
                  'markupAmount',
                  'markupPercent',
                  'toAirportCode',
                ]);
                setPriceDialogData(undefined);
                update(v);
              }}
            />
          )}
        </>
      )}
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <img src={iconDelete} alt="" style={{ height: '117px' }} />
            <Typography variant="body1">
              <FormattedMessage id="m.setup.confirm" />
            </Typography>
          </div>
        }
        show={showConfirm}
        cancelMessageId="ignore"
        confirmMessageId="delete"
        onCancel={() => setShowConfirm(false)}
        onAccept={() => {
          deleteFn();
          setShowConfirm(false);
        }}
      />
    </div>
  );
};
export default connect()(Flight);
