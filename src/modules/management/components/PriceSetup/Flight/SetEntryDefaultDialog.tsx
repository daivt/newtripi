import { Button, Dialog, IconButton, InputAdornment, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREY } from '../../../../../colors';
import { AppState } from '../../../../../redux/reducers';
import ic_add from '../../../../../svg/add.svg';
import airportSvg from '../../../../../svg/airport.svg';
import { NumberTexField } from '../../../../booking/components/Form';
import { Airport } from '../../../../common/models';
import { validTelephoneRegex } from '../../../../common/utils';
import { MAX_PERCENT, MAX_PRICE } from '../../../constants';
import { FightSetupData } from '../../../utils';

interface Props {
  data: FightSetupData;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  open: boolean;
  onClose(): void;
  onSubmit(data: FightSetupData): void;
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}
export function getLabelAirline(option: Airline) {
  return `${option.value}`;
}
export interface Airline {
  key: number;
  value: string;
}
export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2">
          {option.location} ({option.code})
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}
export function renderOptionAirline(option: Airline) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2" color="primary">
          {option.value}
        </Typography>
      </div>
    </div>
  );
}
const SetEntryDefaultDialog: React.FunctionComponent<Props> = props => {
  const { open, onClose, onSubmit } = props;
  const [data, setData] = React.useState<FightSetupData>(props.data);
  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="xs"
      style={{
        margin: '48px',
      }}
      PaperProps={{
        style: {
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          background: 'white',
          padding: '20px 18px 30px 18px',
          minWidth: '530px',
          overflowY: 'unset',
          alignSelf: 'flex-start',
          maxHeight: '100%',
        },
      }}
    >
      <div>
        <IconButton
          style={{ padding: '8px', position: 'absolute', top: '10px', right: '10px' }}
          size="small"
          onClick={onClose}
        >
          <IconClose />
        </IconButton>
        <Typography variant="h6" style={{ textAlign: 'center', marginBottom: '24px' }}>
          <FormattedMessage
            id={
              data.fromAirportCode === data.toAirportCode
                ? 'm.setup.domestic'
                : 'm.setup.international'
            }
          />
        </Typography>
        <div
          style={{ display: 'flex', justifyItems: 'flex-end', height: '86px', padding: '8px 12px' }}
        >
          <NumberTexField
            style={{ margin: '0px' }}
            text={data.markupPercent}
            header={<FormattedMessage id="m.setup.value" />}
            regex={validTelephoneRegex}
            update={text => {
              setData({
                ...data,
                markupPercent: text < MAX_PERCENT ? text : MAX_PERCENT,
              });
            }}
            optional
            endAdornment={<InputAdornment position="start">%</InputAdornment>}
          />
          <div style={{ width: '70px', textAlign: 'center', alignSelf: 'flex-end' }}>
            <img src={ic_add} alt="" />
          </div>
          <NumberTexField
            style={{ margin: '0px' }}
            text={data.markupAmount}
            header={<></>}
            regex={validTelephoneRegex}
            update={text => {
              setData({
                ...data,
                markupAmount: text < MAX_PRICE ? text : MAX_PRICE,
              });
            }}
            optional
            endAdornment={
              <InputAdornment position="start" style={{ color: GREY }}>
                <FormattedMessage id="currency" />
              </InputAdornment>
            }
          />
        </div>
        <div style={{ marginTop: '40px', display: 'flex', justifyContent: 'center' }}>
          <Button
            variant="contained"
            color="secondary"
            style={{ height: '40px', width: '160px', marginRight: '20px' }}
            onClick={() => {
              onSubmit(data);
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="update" />
            </Typography>
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            style={{ height: '40px', width: '160px' }}
            onClick={onClose}
          >
            <Typography variant="body2">
              <FormattedMessage id="ignore" />
            </Typography>
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

export default connect()(SetEntryDefaultDialog);
