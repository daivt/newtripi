import { Button, Checkbox, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { GREY, HOVER_GREY } from '../../../../../colors';
import { ReactComponent as Plane } from '../../../../../svg/ic_flight_itinerary_airplane.svg';
import { ReactComponent as Profile } from '../../../../../svg/profile_edit_secondary.svg';
import { Line } from '../../../../common/components/elements';
import { FightSetupData } from '../../../utils';
interface Props {
  check: boolean;
  showOption: boolean;
  data: FightSetupData;
  onCheck(): void;
  onClick(): void;
}

const FlightCard: React.FunctionComponent<Props> = props => {
  const { check, data, onCheck, onClick, showOption } = props;
  return (
    <div
      style={{
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        borderRadius: '4px',
        background: check ? HOVER_GREY : 'white',
        width: '100%',
        height: '105px',
        padding: '12px',
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'column',
      }}
    >
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <div style={{ display: 'flex' }}>
          <img src={data.logoUrl} alt="" style={{ height: '20px' }} />
          <Typography variant="body1" style={{ marginLeft: '8px' }}>
            {data.title}
          </Typography>
        </div>
        <div style={{ display: 'flex' }}>
          <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
            <FormattedNumber value={data.markupPercent || 0} />
            %&nbsp;+&nbsp;
          </Typography>
          <Typography variant="h6" color="secondary" style={{ wordBreak: 'break-all' }}>
            <FormattedNumber value={data.markupAmount || 0} />
            <FormattedMessage id="currency" />
          </Typography>
          <Button
            variant="text"
            style={{
              minWidth: '24px',
              minHeight: '24px',
              height: '24px',
              alignSelf: 'center',
              padding: 0,
              marginLeft: '20px',
            }}
            onClick={onClick}
          >
            <Profile />
          </Button>
        </div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
          <Line style={{ width: 'calc(50% - 12px)' }}>
            <div style={{ maxWidth: '230px', wordBreak: 'break-word' }}>
              <Typography variant="subtitle2">
                {data.fromAirportName}({data.fromAirportCode})
              </Typography>
            </div>
            <div style={{ border: `1px solid  ${GREY}`, flex: 1, marginLeft: '10px' }} />
          </Line>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              padding: '0px 5px',
              flex: 1,
            }}
          >
            <Plane />
          </div>
          <Line style={{ width: 'calc(50% - 12px)' }}>
            <div style={{ border: `1px solid  ${GREY}`, flex: 1, marginRight: '10px' }} />
            <div style={{ maxWidth: '230px', wordBreak: 'break-word', textAlign: 'end' }}>
              <Typography variant="subtitle2">
                {data.toAirportName}({data.toAirportCode})
              </Typography>
            </div>
          </Line>
        </div>
        <Checkbox
          color="secondary"
          style={{ padding: 0 }}
          checked={check}
          onChange={onCheck}
          disabled={!showOption}
        />
      </div>
    </div>
  );
};

export default FlightCard;
