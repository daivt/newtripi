import { Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { ROUTES, ServiceType } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CustomTab, CustomTabs } from '../../../common/components/elements';
import { TAB_PARAM_NAME } from '../../constants';
import Flight from './Flight';
import Hotel from './Hotel';
import { Redirect } from 'react-router';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}
const LIST: { id: ServiceType; content: React.ReactNode }[] = [
  { id: 'flight', content: <Flight /> },
  { id: 'hotel', content: <Hotel /> },
];
const PriceSetup: React.FunctionComponent<Props> = props => {
  const { search } = props;
  const params = new URLSearchParams(search);
  const currentTab = params.get(TAB_PARAM_NAME);

  if (!currentTab) {
    params.set(TAB_PARAM_NAME, 'flight');
    return (
      <Redirect
        to={{
          pathname: ROUTES.management,
          search: params.toString(),
        }}
      />
    );
  }

  let tab = 0;
  if (currentTab) {
    tab = LIST.findIndex(obj => obj.id === currentTab) || 0;
  }
  return (
    <div style={{ padding: '30px 30px' }}>
      <div>
        <Typography variant="h5">
          <FormattedMessage id="m.priceSetup" />
        </Typography>
      </div>
      <div>
        <CustomTabs
          value={tab}
          onChange={(e, val) => {
            const params = new URLSearchParams(props.search);
            params.set('tab', LIST[val].id);
            props.dispatch(replace(`${ROUTES.management}?${params.toString()}`));
          }}
        >
          {LIST.map(v => (
            <CustomTab
              key={v.id}
              label={
                <Typography variant="body2">
                  <FormattedMessage id={v.id} />
                </Typography>
              }
            />
          ))}
        </CustomTabs>
        {LIST[tab].content}
      </div>
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    search: state.router.location.search,
  };
};
export default connect(mapStateToProps)(PriceSetup);
