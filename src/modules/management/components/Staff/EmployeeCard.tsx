import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY, HOVER_GREY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import Link from '../../../common/components/Link';
import { ACTION_PARAM_NAME, ListOfStaffActionType } from '../../constants';

const Row = styled.tr`
  &:hover {
    background: ${HOVER_GREY};
  }
  height: 56px;
  border-top: 1px solid ${GREY};
  border-bottom: 1px solid ${GREY};
`;

interface Props {
  data: some;
  index: number;
  search: string;
}

const EmployeCard: React.FunctionComponent<Props> = props => {
  const { data, index, search } = props;
  const seeDetail = React.useCallback(
    (id: number) => {
      const params = new URLSearchParams(search);
      params.set(ACTION_PARAM_NAME, 'detail' as ListOfStaffActionType);
      params.set('memberId', `${id}`);
      return params.toString();
    },
    [search],
  );
  const seeDetailInvite = React.useCallback(() => {
    const params = new URLSearchParams(search);
    params.set(ACTION_PARAM_NAME, 'invite' as ListOfStaffActionType);
    return params.toString();
  }, [search]);
  return (
    <Row>
      {data.status === 'waiting' ? (
        <>
          <td style={{ width: '10%', textAlign: 'center' }}>
            <Typography variant="body2" color="secondary">
              {index}
            </Typography>
          </td>
          <td style={{ width: '30%' }}>
            <Typography variant="body2" color="secondary">
              {data.subUserInfo && data.subUserInfo.name}
            </Typography>
          </td>
          <td style={{ width: '15%' }}>
            <Typography variant="body2" color="secondary">
              {data.subUserInfo && data.subUserInfo.phoneInfo}
            </Typography>
          </td>
          <td style={{ width: '30%' }}>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.subUserInfo && data.subUserInfo.emailInfo}
            </Typography>
          </td>
          <td style={{ width: '15%' }}>
            <Link
              to={{
                pathname: ROUTES.management,
                search: `?${seeDetailInvite()}`,
                state: { backableToList: true, employeeData: data },
              }}
            >
              <Typography variant="body2" color="secondary">
                <FormattedMessage id="m.employee.waitingConfirm" />
              </Typography>
            </Link>
          </td>
        </>
      ) : (
        <>
          <td style={{ width: '10%', textAlign: 'center' }}>
            <Typography variant="body2">{index}</Typography>
          </td>
          <td style={{ width: '30%' }}>
            <Typography variant="body2">{data.name}</Typography>
          </td>
          <td style={{ width: '15%' }}>
            <Typography variant="body2">{data.phoneInfo}</Typography>
          </td>
          <td style={{ width: '30%' }}>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.emailInfo}
            </Typography>
          </td>
          <td style={{ width: '15%' }}>
            <Link
              to={{
                pathname: ROUTES.management,
                search: `?${seeDetail(data.id)}`,
                state: { backableToList: true, employeeData: data },
              }}
            >
              <Typography variant="body2" style={{ color: BLUE }}>
                <FormattedMessage id="m.detail" />
              </Typography>
            </Link>
          </td>
        </>
      )}
    </Row>
  );
};

export default EmployeCard;
