import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, ListOfStaffActionType } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  type: ListOfStaffActionType;
}

const BookingDetailBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state, dispatch, router, type } = props;
  const backToEmployeeList = React.useCallback(() => {
    const backableToList = state && state.backableToList;
    if (backableToList) {
      dispatch(go(-2));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    params.delete('memberId');
    dispatch(replace({ search: params.toString() }));
  }, [state, dispatch, router]);
  const backToEmployeeDetail = React.useCallback(() => {
    const backableToDetail = state && state.backableToDetail;
    if (backableToDetail) {
      dispatch(go(-1));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    params.delete('memberId');
    dispatch(replace({ search: params.toString() }));
  }, [state, dispatch, router]);
  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => backToEmployeeList()}
        >
          <FormattedMessage id="m.employee" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => backToEmployeeDetail()}
        >
          <FormattedMessage id="m.employeeInfo" />
        </Typography>
        <Typography variant="body2" color="secondary">
          {type === 'flight' ? (
            <FormattedMessage id="flight" />
          ) : type === 'hotel' ? (
            <FormattedMessage id="hotel" />
          ) : (
            undefined
          )}
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(BookingDetailBreadcrumbs);
