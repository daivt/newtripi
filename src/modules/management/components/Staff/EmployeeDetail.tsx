import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import moment from 'moment';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../API';
import { BLUE, DARK_GREY } from '../../../../colors';
import { DATE_FORMAT, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import ic_delete from '../../../../svg/delete.svg';
import iconDelete from '../../../../svg/ic_delete_ticket_alert.svg';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { Line, snackbarSetting } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import MessageDialog from '../../../common/components/MessageDialog';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME } from '../../constants';
import BookingCard from './BookingCard';
import EmployeeDetailBreadcrumb from './EmployeeDetailBreadcrumb';

const Td = styled.td`
  padding: 0;
  padding-top: 12px;
`;

interface Props
  extends WrappedComponentProps,
    WithSnackbarProps,
    ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDeactivate(id: number): void;
}

interface State {
  data?: some;
  errorMessage: string;
  confirm: boolean;
  numberPage: number;
}
const HEADER = [
  { width: '10%' },
  { id: 'm.employee.produce', width: '15%' },
  { id: 'm.employee.bookingTime', width: '25%' },
  { id: 'm.sales', width: '20%' },
  { id: 'm.profit', width: '20%' },
  { id: 'm.employee.action', width: '10%' },
];
const PAGE_SIZE = 8;
class EmployeeDetail extends React.PureComponent<Props, State> {
  state: State = {
    errorMessage: '',
    confirm: false,
    numberPage: 1,
  };

  async componentDidMount() {
    const { router, dispatch } = this.props;
    const params = new URLSearchParams(router.location.search);
    const memberId = parseInt(params.get('memberId') || '0', 10) || 0;
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getBookingListOfMember}`,
        'post',
        true,
        JSON.stringify({ memberId, fromDate: '01-01-2010', toDate: moment().format(DATE_FORMAT) }),
      ),
    );

    if (json.code === 200) {
      this.setState({ data: json.data });
    }
  }
  onDelete = async () => {
    const { dispatch, onDeactivate, router, enqueueSnackbar, closeSnackbar } = this.props;
    const params = new URLSearchParams(router.location.search);
    const memberId = parseInt(params.get('memberId') || '0', 10) || 0;
    const json = await dispatch(
      fetchThunk(`${API_PATHS.removeMember}`, 'post', true, JSON.stringify({ memberId })),
    );
    if (json.code === 200) {
      onDeactivate(memberId);
      this.setState({ confirm: false });
      enqueueSnackbar(
        json.message,
        snackbarSetting(json.message, key => closeSnackbar(key)),
      );
      const searchState = router.location.search;
      const params = new URLSearchParams(searchState);
      params.delete(ACTION_PARAM_NAME);
      dispatch(
        replace({
          search: params.toString(),
          state: { ...router.location.state, employeeData: undefined },
        }),
      );
    } else {
      this.setState({ errorMessage: json.message });
    }
  };
  public render() {
    const { router } = this.props;
    const { data, errorMessage, confirm, numberPage } = this.state;
    const searchState = router.location.search;

    const employeeData = router.location.state && router.location.state.employeeData;
    if (!data) {
      return (
        <div style={{ padding: '40px' }}>
          <LoadingIcon />
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>{employeeData ? employeeData.name : ''}</title>
        </Helmet>
        <EmployeeDetailBreadcrumb />
        <Typography variant="h5" style={{ padding: '16px 0px' }}>
          <FormattedMessage id="m.employeeInfo" />
        </Typography>
        <div style={{ display: 'flex', alignItems: 'flex-start' }}>
          <div style={{ width: '50%' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="m.employee.history" />
            </Typography>
            <table>
              <tbody>
                <tr>
                  <Td style={{ paddingTop: '20px' }}>
                    <Typography variant="body1">
                      <FormattedMessage id="m.totalOrders" />
                      :&nbsp;
                    </Typography>
                  </Td>
                  <Td style={{ paddingTop: '20px', textAlign: 'end' }}>
                    <Typography variant="body1">{data.summaries.totalOrders}</Typography>
                  </Td>
                </tr>
                <tr>
                  <Td>
                    <Typography variant="body1">
                      <FormattedMessage id="m.totalSales" />
                      :&nbsp;
                    </Typography>
                  </Td>
                  <Td style={{ textAlign: 'end' }}>
                    <Typography variant="body1" color="secondary">
                      <FormattedNumber value={data.summaries.totalSales} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Td>
                </tr>
                <tr>
                  <Td>
                    <Typography variant="body1">
                      <FormattedMessage id="m.totalProfit" />
                      :&nbsp;
                    </Typography>
                  </Td>
                  <Td style={{ textAlign: 'end' }}>
                    <Typography variant="body1" color="secondary">
                      <FormattedNumber value={data.summaries.totalMarkupAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Td>
                </tr>
                <tr>
                  <Td>
                    <Typography variant="body1">
                      <FormattedMessage id="m.totalBonusPoint" />
                      :&nbsp;
                    </Typography>
                  </Td>
                  <Td style={{ textAlign: 'end' }}>
                    <Typography variant="body1" color="primary">
                      <FormattedNumber value={data.summaries.totalBonusPoint} />
                      &nbsp;
                      <FormattedMessage id="point" />
                    </Typography>
                  </Td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style={{ width: '50%' }}>
            {employeeData && (
              <>
                <Typography variant="subtitle2">
                  <FormattedMessage id="m.contactInfo" />
                </Typography>
                <table>
                  <tbody>
                    <tr>
                      <Td style={{ paddingTop: '20px' }}>
                        <Line>
                          <Typography variant="body1">
                            <FormattedMessage id="m.fullName" />
                            :&nbsp;{employeeData.name}
                          </Typography>
                        </Line>
                      </Td>
                    </tr>
                    <tr>
                      <Td>
                        <Line>
                          <Typography variant="body1">
                            <FormattedMessage id="telephone" />
                            :&nbsp; {employeeData.phoneInfo}
                          </Typography>
                        </Line>
                      </Td>
                    </tr>
                    <tr>
                      <Td>
                        <Line>
                          <Typography variant="body1">
                            <FormattedMessage id="m.email" />
                            :&nbsp;
                          </Typography>
                          <Typography variant="body1" style={{ color: BLUE }}>
                            {employeeData.emailInfo}
                          </Typography>
                        </Line>
                      </Td>
                    </tr>
                  </tbody>
                </table>
              </>
            )}
            <Line style={{ marginTop: '12px' }}>
              <Button
                variant="text"
                style={{ padding: '4px' }}
                onClick={() => this.setState({ confirm: true })}
              >
                <img src={ic_delete} alt="" style={{ width: '24px', height: '24px' }} />
                &nbsp;&nbsp;
                <Typography variant="body1">
                  <FormattedMessage id="m.employee.delete" />
                </Typography>
              </Button>
            </Line>
          </div>
        </div>
        <div style={{ marginTop: '24px' }}>
          {data.bookings.length > 0 ? (
            <table style={{ width: '100%', borderCollapse: 'collapse', marginTop: '12px' }}>
              <thead>
                <tr style={{ height: '56px' }}>
                  {HEADER.map((v, index) => (
                    <td style={{ width: v.width }} key={index}>
                      {v.id && (
                        <Typography variant="body2" style={{ color: DARK_GREY }}>
                          <FormattedMessage id={v.id} />
                        </Typography>
                      )}
                    </td>
                  ))}
                </tr>
              </thead>
              <tbody>
                {data.bookings.slice(0, PAGE_SIZE * numberPage).map((v: some, index: number) => (
                  <BookingCard key={index} data={v} search={searchState} />
                ))}
              </tbody>
            </table>
          ) : (
            <NoDataResult id="m.noTransaction" style={{ marginTop: '48px' }} />
          )}
          {PAGE_SIZE * numberPage < data.bookings.length && (
            <div style={{ textAlign: 'center', marginTop: '23px' }}>
              <Button
                onClick={() => {
                  this.setState({ numberPage: numberPage + 1 });
                }}
              >
                <Typography style={{ color: BLUE }}>
                  <FormattedMessage
                    id="result.displayMore"
                    values={{ num: data.bookings.length - PAGE_SIZE * numberPage }}
                  />
                </Typography>
              </Button>
            </div>
          )}
        </div>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" style={{ height: '117px' }} />
              <Typography variant="h6">
                <FormattedMessage id="m.employee.confirm" />
              </Typography>
            </div>
          }
          show={confirm}
          cancelMessageId="ignore"
          confirmMessageId="ok"
          onAccept={() => {
            this.onDelete();
          }}
          onCancel={() => {
            this.setState({ confirm: false });
          }}
        />
      </div>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(injectIntl(withSnackbar(EmployeeDetail)));
