import { Dialog, IconButton, Typography, Button } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { FreeTextField } from '../../../booking/components/Form';
import { validTelephoneRegex, validVietnamTelephoneRegex } from '../../../common/utils';

export interface IMessageDialogProps extends WrappedComponentProps {
  open: boolean;
  onClose(): void;
  onClick(username: string): void;
}

const AddNewEmployeeDialog: React.FunctionComponent<IMessageDialogProps> = props => {
  const { open, onClose, onClick, intl } = props;
  const [phone, setPhone] = React.useState('');
  const [valid, setValid] = React.useState(true);
  const [ticktock, setTick] = React.useState(true);

  React.useEffect(() => {
    setPhone('');
    setValid(true);
    setTick(true);
  }, [open]);

  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="sm"
      PaperProps={{
        style: {
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          width: '400px',
          minHeight: '320px',
          padding: '20px 30px 28px 30px',
        },
      }}
      keepMounted
      onClose={onClose}
    >
      <IconButton
        style={{ position: 'absolute', padding: '0px', top: '8px', right: '8px' }}
        size="small"
        onClick={onClose}
      >
        <IconClose />
      </IconButton>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Typography variant="h5">
          <FormattedMessage id="m.employee.addNew" />
        </Typography>
        <Typography
          variant="body2"
          style={{ textAlign: 'center', marginTop: '30px', marginBottom: '24px' }}
        >
          <FormattedMessage id="m.employee.note" />
        </Typography>
        <FreeTextField
          key={`${ticktock}`}
          style={{ width: '100%' }}
          text={phone}
          header={intl.formatMessage({ id: 'telephone' })}
          placeholder={intl.formatMessage({ id: 'telephoneEx' })}
          update={text => {
            setPhone(text);
            setValid(true);
          }}
          regex={validTelephoneRegex}
          valid={valid}
        />

        <Button
          style={{ width: '170px', marginTop: '30px' }}
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => {
            const valid = validVietnamTelephoneRegex.test(phone);
            setValid(valid);
            setTick(!ticktock);
            if (valid) {
              onClick(phone);
            }
          }}
        >
          <Typography variant="button">
            <FormattedMessage id={'m.employee.sendInvite'} />
          </Typography>
        </Button>
      </div>
    </Dialog>
  );
};
export default injectIntl(AddNewEmployeeDialog);
