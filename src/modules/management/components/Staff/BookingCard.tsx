import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY, HOVER_GREY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import Flight from '../../../../svg/Flight.svg';
import Hotel from '../../../../svg/Hotel.svg';
import Activity from '../../../../svg/TourismActivities.svg';
import Train from '../../../../svg/Train.svg';
import Link from '../../../common/components/Link';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  ListOfStaffActionType,
  ProductType,
} from '../../constants';

const Row = styled.tr`
  &:hover {
    background: ${HOVER_GREY};
  }
  height: 56px;
  border-top: 1px solid ${GREY};
  border-bottom: 1px solid ${GREY};
`;

interface Props {
  data: some;
  search: string;
}

const BookingCard: React.FunctionComponent<Props> = props => {
  const { data } = props;
  const seeBookingDetail = (id: number) => {
    const params = new URLSearchParams(props.search);
    params.set(BOOKING_ID_PARAM_NAME, `${id}`);
    const productType = data.productType as ProductType;
    params.set(ACTION_PARAM_NAME, (productType === 'Flight'
      ? 'flight'
      : productType === 'Hotel'
      ? 'hotel'
      : productType === 'Activity'
      ? 'activity'
      : productType === 'Train'
      ? 'train'
      : undefined) as ListOfStaffActionType);
    return params.toString();
  };
  const productType = data.productType as ProductType;
  return (
    <Row>
      <>
        <td
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: 'inherit',
          }}
        >
          <img
            src={
              productType === 'Flight'
                ? Flight
                : productType === 'Hotel'
                ? Hotel
                : productType === 'Train'
                ? Train
                : productType === 'Activity'
                ? Activity
                : ''
            }
            style={{ width: '33px', height: '33px' }}
            alt=""
          />
        </td>
        <td>
          <Typography variant="body2">{data.productType}</Typography>
        </td>
        <td>
          <Typography variant="body2" style={{ color: DARK_GREY }}>
            {data.bookingTime}
          </Typography>
        </td>
        <td>
          <Typography variant="body2" color="secondary">
            <FormattedNumber value={data.totalAmount} />
            &nbsp; <FormattedMessage id="currency" />
          </Typography>
        </td>
        <td>
          <Typography variant="body2" color="primary">
            <FormattedNumber value={data.totalMarkupAmount} />
            &nbsp; <FormattedMessage id="currency" />
          </Typography>
        </td>
        <td>
          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${seeBookingDetail(data.bookingId)}`,
              state: { backableToDetail: true },
            }}
          >
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage id="m.detail" />
            </Typography>
          </Link>
        </td>
      </>
    </Row>
  );
};

export default BookingCard;
