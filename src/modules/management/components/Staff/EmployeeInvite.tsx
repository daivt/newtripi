import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import ic_delete from '../../../../svg/delete.svg';
import iconDelete from '../../../../svg/ic_delete_ticket_alert.svg';
import { CopyTextField } from '../../../booking/components/Form';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import { Line, snackbarSetting } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME } from '../../constants';
import EmployeeDetailBreadcrumb from './EmployeeDetailBreadcrumb';

interface Props extends WrappedComponentProps, WithSnackbarProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDeactivate(id: number): void;
}

interface State {
  shareLink?: string;
  message: string;
  loading: boolean;
  confirm: boolean;
  numberPage: number;
}

class EmployeeInvite extends React.PureComponent<Props, State> {
  state: State = {
    message: '',
    loading: false,
    confirm: false,
    numberPage: 1,
  };

  async componentDidMount() {
    const { dispatch } = this.props;
    const json = await dispatch(fetchThunk(`${API_PATHS.getInviteLink}`, 'get', true));
    if (json.code === 200) {
      this.setState({ shareLink: json.data });
    }
  }
  onDelete = async (id: number) => {
    const { dispatch, onDeactivate, router, enqueueSnackbar, closeSnackbar } = this.props;
    const json = await dispatch(fetchThunk(`${API_PATHS.deleteMyInvite}`, 'post', true, JSON.stringify({ id })));
    if (json.code === 200) {
      onDeactivate(id);
      this.setState({ confirm: false });
      const searchState = router.location.search;
      const params = new URLSearchParams(searchState);
      params.delete(ACTION_PARAM_NAME);
      dispatch(
        replace({
          search: params.toString(),
          state: { ...router.location.state, employeeData: undefined },
        }),
      );
    }
    enqueueSnackbar(
      json.message,
      snackbarSetting(json.message, key => closeSnackbar(key)),
    );
  };

  public render() {
    const { router } = this.props;
    const { shareLink, confirm } = this.state;
    const stateRouter = router.location.state;
    const employeeData = stateRouter && stateRouter.employeeData;
    if (!employeeData) {
      const searchState = router.location.search;
      const params = new URLSearchParams(searchState);
      params.delete(ACTION_PARAM_NAME);
      return (
        <Redirect
          to={{
            pathname: ROUTES.management,
            search: `?${params.toString()}`,
            state: { backableToList: true, employeeData: shareLink },
          }}
        ></Redirect>
      );
    }
    if (!shareLink) {
      return (
        <div style={{ padding: '40px' }}>
          <LoadingIcon />
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>{employeeData && employeeData.subUserInfo ? employeeData.subUserInfo.name : ''}</title>
        </Helmet>
        <EmployeeDetailBreadcrumb />
        <Typography variant="h5" style={{ padding: '16px 0px' }}>
          <FormattedMessage id="m.employeeInfo" />
        </Typography>
        {employeeData.subUserInfo && (
          <>
            <Typography variant="subtitle2">
              <FormattedMessage id="m.contactInfo" />
            </Typography>
            <Line style={{ marginTop: '20px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.status" />
                :&nbsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {employeeData.status === 'waiting' && <FormattedMessage id="m.employeeInfo.waiting" />}
              </Typography>
            </Line>
            <Line style={{ marginTop: '12px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.fullName" />
                :&nbsp;{employeeData.subUserInfo.name}
              </Typography>
            </Line>
            <Line style={{ marginTop: '12px' }}>
              <Typography variant="body1">
                <FormattedMessage id="telephone" />
                :&nbsp; {employeeData.subUserInfo.phoneInfo}
              </Typography>
            </Line>
            <Line style={{ marginTop: '12px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.email" />
                :&nbsp;
              </Typography>
              <Typography variant="body1" style={{ color: BLUE }}>
                {employeeData.subUserInfo.emailInfo}
              </Typography>
            </Line>
            <Line style={{ marginTop: '20px', marginBottom: '20px' }}>
              <CopyTextField
                inputStyle={{ width: '272px' }}
                header={<FormattedMessage id="shareLink" />}
                text={shareLink}
                message={<FormattedMessage id="copiedLink" />}
              />
            </Line>
          </>
        )}
        <Button
          variant="text"
          style={{ marginTop: '4px', padding: '4px' }}
          onClick={() => this.setState({ confirm: true })}
        >
          <img src={ic_delete} alt="" style={{ width: '24px', height: '24px' }} />
          &nbsp;&nbsp;
          <Typography variant="body1">
            <FormattedMessage id="m.employee.deleteInvite" />
          </Typography>
        </Button>
        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" style={{ height: '117px' }} />
              <Typography variant="h6">
                <FormattedMessage id="m.employee.deleteInviteConfirm" />
              </Typography>
            </div>
          }
          show={confirm}
          cancelMessageId="ignore"
          confirmMessageId="ok"
          onAccept={() => {
            this.onDelete(employeeData.id);
          }}
          onCancel={() => {
            this.setState({ confirm: false });
          }}
        />
      </div>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(injectIntl(withSnackbar(EmployeeInvite)));
