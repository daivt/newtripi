import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { goBack, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { ACTION_PARAM_NAME } from '../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const EmployeeDetailBreadcrumb: React.FunctionComponent<Props> = props => {
  const backToEmployeeList = () => {
    const { state, dispatch, router } = props;
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(goBack());
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(ACTION_PARAM_NAME);
    params.delete('memberId');
    dispatch(replace({ search: params.toString() }));
  };
  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToEmployeeList()}
      >
        <FormattedMessage id="m.employee" />
      </Typography>
      <Typography variant="body2" color="secondary">
        <FormattedMessage id="m.employeeInfo" />
      </Typography>
    </Breadcrumbs>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(EmployeeDetailBreadcrumb);
