import { Button, IconButton, InputAdornment, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import icSearch from '../../../../svg/ic_search.svg';
import { FreeTextField } from '../../../booking/components/Form';
import { Line, snackbarSetting } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import MessageDialog from '../../../common/components/MessageDialog';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import { ACTION_PARAM_NAME, ListOfStaffActionType } from '../../constants';
import FlightBookingDetail from '../Orders/Flight/FlightBookingDetail';
import HotelDetailTab from '../Orders/Hotel/HotelDetailTab';
import AddNewEmployeeDialog from './AddNewEmployeeDialog';
import BookingDetailBreadcrumbs from './BookingDetailBreadcrumbs';
import EmployeCard from './EmployeeCard';
import EmployeeDetail from './EmployeeDetail';
import EmployeeInvite from './EmployeeInvite';
interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

const Staff: React.FunctionComponent<Props> = props => {
  const { dispatch, intl } = props;
  const [search, setSearch] = React.useState<string>('');
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [openCreateDialog, setOpenCreateDialog] = React.useState(false);
  const [deactivateId, setDeactivateId] = React.useState(0);
  const [cancelHoldingId, setCancelHoldingId] = React.useState(0);

  const searchState = props.router.location.search;
  const params = new URLSearchParams(searchState);
  const action = params.get(ACTION_PARAM_NAME) as ListOfStaffActionType | null;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  React.useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      setData([]);
      const json = await dispatch(fetchThunk(`${API_PATHS.getMyInviteList}`, 'get', true));
      if (json.code === 200) {
        setData(json.data);
      }
      const json2 = await dispatch(fetchThunk(`${API_PATHS.getMemberList}`, 'get', true));
      if (json2.code === 200) {
        setData(data => data && data.concat(json2.data));
      }
      setLoading(false);
    };
    fetch();
  }, [dispatch, deactivateId, cancelHoldingId]);

  const addNew = React.useCallback(
    async (userName: String) => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.inviteMember}`,
          'post',
          true,
          JSON.stringify({
            userName,
          }),
        ),
      );
      if (json.code === 200) {
        setDeactivateId(json.code);
        enqueueSnackbar(
          json.message,
          snackbarSetting(json.message, key => closeSnackbar(key)),
        );
      } else {
        setErrorMessage(json.message);
      }
      setOpenCreateDialog(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );
  const dataFilter = data
    ? data.filter(
        v =>
          (v.name && v.name.toLowerCase().includes(search.toLowerCase())) ||
          (v.phoneInfo && v.phoneInfo.toLowerCase().includes(search.toLowerCase())) ||
          (v.emailInfo && v.emailInfo.toLowerCase().includes(search.toLowerCase())) ||
          (v.subUserInfo && v.subUserInfo.name.toLowerCase().includes(search.toLowerCase())) ||
          (v.subUserInfo && v.subUserInfo.phoneInfo.toLowerCase().includes(search.toLowerCase())) ||
          (v.subUserInfo && v.subUserInfo.emailInfo.toLowerCase().includes(search.toLowerCase())),
      )
    : undefined;
  return (
    <div style={{ padding: '30px 20px 42px 30px' }}>
      <div style={{ display: action ? 'none' : undefined }}>
        <Line style={{ justifyContent: 'space-between' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.employee" />
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            size="large"
            style={{ width: '160px' }}
            onClick={() => {
              setOpenCreateDialog(true);
            }}
          >
            <FormattedMessage id="m.addNew" />
          </Button>
        </Line>
        <Line>
          <FreeTextField
            inputStyle={{ width: '270px', background: 'white' }}
            text={search}
            update={text => setSearch(text)}
            placeholder={intl.formatMessage({ id: 'm.employeeInit' })}
            header={<></>}
            optional
            endAdornment={
              <InputAdornment position="end">
                <IconButton style={{ padding: '8px' }}>
                  <img
                    src={icSearch}
                    alt=""
                    style={{
                      filter: 'brightness(1) invert(1)',
                      width: '24px',
                      height: '24px',
                      color: 'black',
                    }}
                  />
                </IconButton>
              </InputAdornment>
            }
          />
        </Line>
        <div>
          {!!data ? (
            <>
              {dataFilter && dataFilter.length > 0 && (
                <table style={{ width: '100%', borderCollapse: 'collapse', marginTop: '12px' }}>
                  <tbody>
                    {dataFilter.map((one, index) => (
                      <EmployeCard key={one.id} data={one} index={index + 1} search={searchState} />
                    ))}
                  </tbody>
                </table>
              )}
              {dataFilter && !dataFilter.length && data.length > 0 && search && (
                <NoDataResult id="m.employee.noDataFilter" style={{ marginTop: '48px' }} />
              )}
              {loading && <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />}
              {data && !data.length && !loading && (
                <NoDataResult
                  id="m.employee.noData"
                  style={{ marginTop: '48px' }}
                  content={
                    <Typography
                      variant="body1"
                      style={{ color: BLUE, cursor: 'pointer' }}
                      onClick={() => setOpenCreateDialog(true)}
                    >
                      &nbsp;
                      <FormattedMessage id="m.addNewNow" />
                    </Typography>
                  }
                />
              )}
            </>
          ) : (
            <LoadingIcon style={{ textAlign: 'center', height: '140px' }} />
          )}
        </div>
      </div>

      <MessageDialog
        show={!!errorMessage}
        onClose={() => setErrorMessage('')}
        message={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              textAlign: 'center',
              padding: '0px 16px',
            }}
          >
            <Typography variant="body1">{errorMessage}</Typography>
          </div>
        }
      />

      <AddNewEmployeeDialog
        open={openCreateDialog}
        onClose={() => {
          setOpenCreateDialog(false);
        }}
        onClick={addNew}
      />

      {action && (
        <div style={{ display: action !== 'detail' ? 'none' : undefined }}>
          <EmployeeDetail onDeactivate={id => setDeactivateId(id)} />
        </div>
      )}

      {action === 'invite' && <EmployeeInvite onDeactivate={id => setDeactivateId(id)} />}
      {action === 'flight' && (
        <FlightBookingDetail
          breadcrumb={<BookingDetailBreadcrumbs type="flight" />}
          onDeactivate={id => setDeactivateId(id)}
          onCancelHolding={id => setCancelHoldingId(id)}
          noAction
        />
      )}
      {action === 'hotel' && (
        <HotelDetailTab
          onDeactivate={id => setDeactivateId(id)}
          noAction
          breadcrumb={<BookingDetailBreadcrumbs type="hotel" />}
        />
      )}
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(Staff));
