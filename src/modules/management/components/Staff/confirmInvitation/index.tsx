import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps, FormattedMessage } from 'react-intl';
import Header from '../../../../common/components/Header';
import { AppState } from '../../../../../redux/reducers';
import { connect } from 'react-redux';
import NotFoundBox from '../../../../common/components/NotFoundBox';
import { Typography, Button, Container } from '@material-ui/core';
import { Line } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { fetchThunk } from '../../../../common/redux/thunks';
import { API_PATHS } from '../../../../../API';
import MessageDialog from '../../../../common/components/MessageDialog';
import { some } from '../../../../../constants';
import { BLUE } from '../../../../../colors';

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const ConfirmInvitation: React.FunctionComponent<Props> = props => {
  const { intl, router, dispatch } = props;
  const [errorMessage, setMessage] = React.useState('');
  const [data, setData] = React.useState<some | undefined>();

  const searchState = new URLSearchParams(router.location.search);
  const inviteToken = searchState.get('inviteToken');
  const agencyName = searchState.get('agencyName');
  const aceptInvite = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(`${API_PATHS.acceptInvite}?token=${inviteToken}`, 'get', true),
    );
    if (json.code === 200) {
      setData(json.data);
    } else {
      setMessage(json.message);
    }
  }, [dispatch, inviteToken]);
  return (
    <div>
      <Helmet>
        <title>{intl.formatMessage({ id: 'm.employee.comfirmInvitation' })}</title>
      </Helmet>
      <Header light={true} />
      {!inviteToken || !agencyName ? (
        <NotFoundBox />
      ) : (
        <Container style={{ flex: 1 }}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              marginTop: '128px',
            }}
          >
            {!!data ? (
              <>
                <Typography variant="body1">
                  <FormattedMessage id="m.employee.aceptSuccess" />
                  &nbsp;
                  {data.inviteInfo.rootUserInfo.name}
                </Typography>
                <Link to={'/'}>
                  <Button variant="text" style={{ minWidth: '144px', minHeight: '40px' }}>
                    <Typography variant="body1" style={{ color: BLUE }}>
                      <FormattedMessage id="returnHome" />
                    </Typography>
                  </Button>
                </Link>
              </>
            ) : (
              <>
                <Typography variant="body1">
                  <FormattedMessage id="m.employee.invite" />
                  &nbsp;
                  {agencyName}
                </Typography>
                <Line style={{ marginTop: '24px' }}>
                  <Button
                    variant="contained"
                    color="secondary"
                    style={{ marginRight: '16px', minWidth: '144px', minHeight: '40px' }}
                    onClick={aceptInvite}
                  >
                    <Typography variant="body1">
                      <FormattedMessage id="accept" />
                    </Typography>
                  </Button>
                  <Link to={'/'}>
                    <Button variant="contained" style={{ minWidth: '144px', minHeight: '40px' }}>
                      <Typography variant="body1">
                        <FormattedMessage id="cancel" />
                      </Typography>
                    </Button>
                  </Link>
                </Line>
              </>
            )}{' '}
          </div>
        </Container>
      )}
      <MessageDialog
        show={!!errorMessage}
        message={
          <Typography variant="body1" style={{ textAlign: 'center', padding: '16px' }}>
            {errorMessage}
          </Typography>
        }
        onClose={() => setMessage('')}
      />
    </div>
  );
};

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(injectIntl(ConfirmInvitation));
