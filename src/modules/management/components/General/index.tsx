import * as React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';
import Recharge from './Topup/Recharge';
import { RECHARGE_PARAM_NAME } from '../../constants';
import DashBoard from './DashBoard';

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

interface IGeneralProps extends ReturnType<typeof mapState2Props> {}

const General: React.FunctionComponent<IGeneralProps> = props => {
  const { search } = props;
  const params = new URLSearchParams(search);
  const isRecharge = params.get(RECHARGE_PARAM_NAME) === 'recharge';
  return <div style={{ padding: '30px 30px' }}>{isRecharge ? <Recharge /> : <DashBoard />}</div>;
};

export default connect(mapState2Props)(General);
