import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { goBack, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import { RECHARGE_PARAM_NAME } from '../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  onChange: (val: string) => void;
}

const TopupHistoryBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { router, dispatch, onChange } = props;
  const backToGeneral = () => {
    const backableToGeneral = router.location.state && router.location.state.backableToList;
    if (backableToGeneral) {
      dispatch(goBack());
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(RECHARGE_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  };
  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToGeneral()}
      >
        <FormattedMessage id="m.general" />
      </Typography>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => onChange('recharge')}
      >
        <FormattedMessage id="m.topup.recharge" />
      </Typography>
      <Typography variant="body2" color="secondary">
        <FormattedMessage id="m.topup.rechargeHistory" />
      </Typography>
    </Breadcrumbs>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router };
}

export default connect(mapStateToProps)(TopupHistoryBreadcrumbs);
