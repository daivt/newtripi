import { Button, Typography, FormHelperText } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { GREY, HOVER_GREY, PRIMARY, RED, SECONDARY } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import icTopupWarning from '../../../../../svg/ic_topup_warning.svg';
import icWarning from '../../../../../svg/ic_warning.svg';
import { CopyTextField, NumberTexField } from '../../../../booking/components/Form';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../../common/redux/thunks';
import { validTelephoneRegex } from '../../../../common/utils';
import { CURRENT_PARAM_NAME, MAX_PRICE } from '../../../constants';
import TopupBreadcrumbs from './TopupBreadcrumbs';
import IconCheck from '@material-ui/icons/CheckCircle';
import voca from 'voca';
import { MUI_THEME } from '../../../../../setupTheme';

const writtenNumber = require('written-number');

const BankBox = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 106px;
  height: 60px;
  border-radius: 2px;
  margin: 4px 8px 4px 0px;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14);
  background: white;
  cursor: pointer;
  :hover {
    background-color: ${HOVER_GREY};
  }
`;

const BankBoxSkeleton: React.FunctionComponent = props => {
  return (
    <div style={{ margin: '4px 8px 4px 0px' }}>
      <Skeleton width={106} height={59} variant="rect"></Skeleton>
    </div>
  );
};
function mapState2Props(state: AppState) {
  return {
    routerState: state.router.location.state,
    locale: state.intl.locale,
    router: state.router,
  };
}

interface IBalanceRechargeProps extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onChange: (val: string) => void;
}

const BalanceRecharge: React.FunctionComponent<IBalanceRechargeProps> = props => {
  const { dispatch, intl, locale } = props;
  const [bankData, setBankData] = React.useState<some[] | undefined>(undefined);
  const [chosenBank, setChosenBank] = React.useState<some | undefined>(undefined);
  const [amount, setAmount] = React.useState<number | undefined>(undefined);
  const [step, setStep] = React.useState<number>(1);
  const [requestTopupInfo, setRequestTopupInfo] = React.useState<some | undefined>(undefined);
  const [moneyValidation, setMoneyValidation] = React.useState<boolean>(true);

  const fetchData = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(`${API_PATHS.getActiveBankList}`, 'get', true));
    if (json.code === 200) {
      setBankData(json.data);
    }
  }, [dispatch]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  const sendRequestTopup = React.useCallback(async () => {
    if (amount && amount >= 50000) {
      setStep(2);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.requestTopup}`,
          'post',
          true,
          JSON.stringify({ amount, bankId: chosenBank && chosenBank.id }),
        ),
      );
      if (json.code === 200) {
        setRequestTopupInfo(json.data);
      }
    } else {
      setMoneyValidation(false);
    }
  }, [amount, chosenBank, dispatch]);
  const transferDescription =
    requestTopupInfo &&
    requestTopupInfo.transferDescription
      .toUpperCase()
      .split(' ')
      .join(intl.formatMessage({ id: 'm.topup.whiteSpace' }));

  return (
    <div>
      <Helmet>
        <title>{intl.formatMessage({ id: 'm.topup.recharge' })}</title>
      </Helmet>
      <TopupBreadcrumbs />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '16px 0px',
        }}
      >
        <Typography variant="h5">
          <FormattedMessage id="m.topup.recharge" />
        </Typography>
        <Button
          style={{ marginRight: '18px' }}
          color="secondary"
          variant="contained"
          size="large"
          onClick={() => props.onChange('rechargeHistory')}
        >
          <FormattedMessage id="m.topup.rechargeHistory" />
        </Button>
      </div>

      {step === 1 && (
        <>
          <Typography variant="body2" style={{ marginTop: '8px' }}>
            <FormattedMessage id="m.topup.rechargeTittle" />
          </Typography>
          <NumberTexField
            text={amount ? amount : undefined}
            regex={validTelephoneRegex}
            header={intl.formatMessage({ id: 'm.topup.enterNumber' })}
            placeholder={intl.formatMessage({ id: 'm.topup.amountExample' })}
            valid={moneyValidation}
            update={value => {
              if (value) {
                setAmount(value < MAX_PRICE ? value : MAX_PRICE);
              } else setAmount(undefined);
            }}
            optional
          />
          <FormHelperText error={true}>
            {!moneyValidation && (
              <FormattedMessage id="m.topup.moneyAmountError" values={{ amount: <FormattedNumber value={50000} /> }} />
            )}
          </FormHelperText>
          <div style={{ display: 'flex', marginTop: '20px' }}>
            <div
              onClick={() => setAmount(5000000)}
              style={{
                boxSizing: 'border-box',
                border: `1px solid ${GREY}`,
                cursor: 'pointer',
                borderRadius: '99px',
                width: '140px',
                height: '36px',
                background: amount === 5000000 ? PRIMARY : 'white',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography variant="body2" style={{ color: amount === 5000000 ? 'white' : 'black' }}>
                <FormattedNumber value={5000000} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
            <div
              onClick={() => setAmount(10000000)}
              style={{
                boxSizing: 'border-box',
                border: `1px solid ${GREY}`,
                cursor: 'pointer',
                borderRadius: '99px',
                width: '140px',
                height: '36px',
                marginLeft: '20px',
                background: amount === 10000000 ? PRIMARY : 'white',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography variant="body2" style={{ color: amount === 10000000 ? 'white' : 'black' }}>
                <FormattedNumber value={10000000} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
            <div
              onClick={() => setAmount(20000000)}
              style={{
                boxSizing: 'border-box',
                border: `1px solid ${GREY}`,
                cursor: 'pointer',
                borderRadius: '99px',
                width: '140px',
                height: '36px',
                marginLeft: '20px',
                background: amount === 20000000 ? PRIMARY : 'white',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography variant="body2" style={{ color: amount === 20000000 ? 'white' : 'black' }}>
                <FormattedNumber value={20000000} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
          </div>
          <div style={{ marginTop: '10px', height: '30px' }}>
            {amount && !isNaN(amount) && (
              <Typography variant="subtitle2">
                {`${voca.capitalize(writtenNumber(amount, { lang: locale }))}`}
                &nbsp; <FormattedMessage id="currencyFullText" />
                &nbsp; (<FormattedNumber value={amount} />
                &nbsp;
                <FormattedMessage id="currency" />)
              </Typography>
            )}
          </div>
          <Typography variant="subtitle2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="m.topup.selectBank" />
          </Typography>
          <Typography variant="body2" style={{ marginTop: '10px' }}>
            <FormattedMessage id="m.topup.selectBankTittle" />
          </Typography>

          {bankData ? (
            <div
              style={{
                display: 'flex',
                flexWrap: 'wrap',
                width: '482px',
                marginTop: '10px',
              }}
            >
              {bankData.map((bank: some) => (
                <BankBox
                  key={bank.id}
                  style={{
                    boxShadow:
                      chosenBank && chosenBank.id === bank.id
                        ? `${SECONDARY} 0px 1px 5px, ${SECONDARY} 0px 3px 4px, ${SECONDARY} 0px 2px 4px`
                        : undefined,
                  }}
                  onClick={() => setChosenBank(bank)}
                >
                  <img style={{ maxWidth: '59px' }} src={bank.logo} alt={bank.name} />
                  {chosenBank && chosenBank.id === bank.id && (
                    <IconCheck
                      style={{
                        position: 'absolute',
                        transition: 'opacity 300ms',
                        right: '4px',
                        top: '4px',
                        color: SECONDARY,
                        width: '14px',
                        height: '14px',
                      }}
                    />
                  )}
                </BankBox>
              ))}
            </div>
          ) : (
            <div style={{ display: 'flex' }}>
              <BankBoxSkeleton />
              <BankBoxSkeleton />
              <BankBoxSkeleton />
              <BankBoxSkeleton />
            </div>
          )}
          <div style={{ height: '17px', marginTop: '8px' }}>
            {chosenBank && !chosenBank.activeTopup && (
              <Typography variant="body2" color="secondary">
                <FormattedMessage id="m.topup.bankMaintenance" values={{ bankName: chosenBank.name }} />
              </Typography>
            )}
          </div>
          <div
            style={{
              display: 'flex',
              textAlign: 'center',
              fontWeight: 500,
              marginTop: '30px',
            }}
          >
            <Button
              size="large"
              color={'secondary'}
              disabled={!amount || !chosenBank || !chosenBank.activeTopup}
              variant="contained"
              style={{ minWidth: '160px', boxShadow: 'none' }}
              onClick={sendRequestTopup}
            >
              <FormattedMessage id="m.topup.continue" />
            </Button>
            <Link to={{ pathname: ROUTES.management, search: `${CURRENT_PARAM_NAME}=m.general` }}>
              <Button
                variant="outlined"
                style={{
                  minWidth: '160px',
                  marginLeft: '20px',
                }}
                size="large"
              >
                <Typography style={{ color: '#757575' }}>
                  <FormattedMessage id="m.topup.cancel" />
                </Typography>
              </Button>
            </Link>
          </div>
        </>
      )}
      {step === 2 && (
        <div style={{ marginTop: '20px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <span
              style={{
                background: PRIMARY,
                fontSize: MUI_THEME.typography.body2.fontSize,
                color: 'white',
                width: '24px',
                height: '24px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: '50%',
              }}
            >
              1
            </span>
            <Typography variant="caption" style={{ marginLeft: '10px' }}>
              <FormattedMessage id="m.topup.loginBankTittle" />
            </Typography>
          </div>
          <div style={{ display: 'flex', alignItems: 'center', marginTop: '20px' }}>
            <span
              style={{
                background: PRIMARY,
                fontSize: MUI_THEME.typography.body2.fontSize,
                color: 'white',
                width: '24px',
                height: '24px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: '50%',
              }}
            >
              2
            </span>
            <Typography variant="caption" style={{ marginLeft: '10px' }}>
              <FormattedMessage id="m.topup.transferGuidance" />
            </Typography>
          </div>
          <div style={{ display: 'flex', marginTop: '20px', alignItems: 'center' }}>
            <span
              style={{
                background: RED,
                fontSize: MUI_THEME.typography.body2.fontSize,
                width: '24px',
                height: '24px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: '50%',
              }}
            >
              <img src={icTopupWarning} alt=""></img>
            </span>
            <Typography variant="caption" style={{ marginLeft: '10px' }}>
              <FormattedMessage
                id="m.topup.transferWarning"
                values={{
                  copy: (
                    <span style={{ fontWeight: 'bold' }}>
                      <FormattedMessage id="copy" />
                    </span>
                  ),
                }}
              />
            </Typography>
          </div>
          <Typography variant="body1" style={{ marginTop: '20px' }}>
            <FormattedMessage id="m.topup.transferInformation" />
          </Typography>
          {requestTopupInfo ? (
            <div>
              <div style={{ display: 'flex', marginTop: '10px' }}>
                <div style={{ justifyContent: 'center' }}>
                  <img
                    src={requestTopupInfo.bankInfo.logo}
                    alt=""
                    style={{
                      width: '120px',
                      objectFit: 'contain',
                    }}
                  />
                </div>
                <div style={{ marginLeft: '20px' }}>
                  <Typography variant="body2" style={{ marginTop: '10px' }}>
                    {requestTopupInfo.bankInfo.description}
                  </Typography>
                  <Typography variant="body2" style={{ marginTop: '10px' }}>
                    <FormattedMessage id="m.topup.accountName" />
                    :&nbsp;
                    {requestTopupInfo.bankInfo.accountVnd}
                  </Typography>
                  <CopyTextField
                    style={{ marginTop: '10px' }}
                    text={requestTopupInfo.bankInfo.accountVnd}
                    header={intl.formatMessage({ id: 'm.topup.accountNumber' })}
                    inputStyle={{ color: SECONDARY }}
                  />
                  <CopyTextField
                    style={{ marginTop: '10px' }}
                    text={requestTopupInfo.transferDescription.toUpperCase()}
                    header={
                      <>
                        <FormattedMessage id="m.topup.transferDescription" />
                        &nbsp;{`(${transferDescription})`}
                      </>
                    }
                    inputStyle={{ color: SECONDARY }}
                  />
                  <CopyTextField
                    style={{ marginTop: '10px' }}
                    text={amount !== undefined ? amount.toString() : ''}
                    header={intl.formatMessage({ id: 'm.topup.moneyAmount' })}
                    inputStyle={{ color: SECONDARY }}
                  />
                </div>
              </div>
              <div style={{ display: 'flex' }}>
                <img src={icWarning} alt="" style={{ width: '20px', height: '20px' }} />
                <Typography variant="body2" style={{ marginLeft: '10px', color: RED }}>
                  <FormattedMessage id="m.topup.remind" />
                </Typography>
              </div>
              <Typography variant="body2" style={{ color: RED }}>
                <FormattedMessage id="m.topup.transferRemind" />
              </Typography>
              <div style={{ display: 'flex' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.topup.delayWarning" />
                </Typography>
                &nbsp;
                <Typography variant="body2" style={{ color: RED }}>
                  <FormattedMessage id="m.topup.transferDescription" />
                </Typography>
                &nbsp;
                <Typography variant="body2">
                  <FormattedMessage id="m.topup.and" />
                </Typography>
                &nbsp;
                <Typography variant="body2" style={{ color: RED }}>
                  <FormattedMessage id="m.topup.moneyAmount" />
                </Typography>
                &nbsp;
                <Typography variant="body2">
                  <FormattedMessage id="m.topup.invalid" />
                </Typography>
              </div>
              <Typography variant="body2">
                <FormattedMessage id="m.topup.supportWarning" />
              </Typography>
              <Typography variant="body2" style={{ marginBottom: '20px' }}>
                <FormattedMessage id="m.topup.codeWarning" />
              </Typography>
              <a
                href={requestTopupInfo.bankInfo.url.toString()}
                target="_blank"
                rel="noopener noreferrer"
                style={{
                  textDecoration: 'inherit',
                  color: 'inherit',
                }}
              >
                <Button
                  size="large"
                  color={'secondary'}
                  variant="contained"
                  style={{ minWidth: '160px', boxShadow: 'none' }}
                >
                  <FormattedMessage id="m.topup.continue" />
                </Button>
              </a>
            </div>
          ) : (
            <LoadingIcon
              style={{
                margin: '50px 0',
              }}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default connect(mapState2Props)(injectIntl(BalanceRecharge));
