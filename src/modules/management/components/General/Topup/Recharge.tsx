import * as React from 'react';
import BalanceRecharge from './BalanceRecharge';
import RechargeHistory from './RechargeHistory';

interface IGeneralProps {}

const Recharge: React.FunctionComponent<IGeneralProps> = props => {
  const [page, setPage] = React.useState('recharge');
  return (
    <div>
      {page === 'recharge' && <BalanceRecharge onChange={val => setPage(val)} />}
      {page === 'rechargeHistory' && <RechargeHistory onChange={val => setPage(val)} />}
    </div>
  );
};

export default Recharge;
