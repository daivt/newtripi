import { Button, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { BLUE, GREY, SECONDARY } from '../../../../../colors';
import { some, TOUR_PAGE_SIZE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunks';
import { getStatusColor } from '../../../constants';
import TopupHistoryBreadcrumbs from './TopupHistoryBreadcrumbs';
import Helmet from 'react-helmet';
import voca from 'voca';

const writtenNumber = require('written-number');

const TransactionWrapper = styled.div`
  border: 1px solid ${GREY};
  border-radius: 4px;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  margin-top: 10px;
  display: flex;
  padding: 8px;
`;
const TransactionSkeleton: React.FunctionComponent = props => {
  return (
    <TransactionWrapper>
      <Skeleton width="120px" height="170px" variant="rect" />
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: 1,
          padding: '12px',
          width: '100%',
        }}
      >
        <Skeleton width="80%" />
        <Skeleton width="80%" />
      </div>
    </TransactionWrapper>
  );
};

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
  };
}

interface IRechargeHistoryProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onChange: (val: string) => void;
}

const RechargeHistory: React.FunctionComponent<IRechargeHistoryProps> = props => {
  const { dispatch, onChange, intl, locale } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(0);
  const [total, setTotal] = React.useState(0);

  const loadMore = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getTopupHistories}`,
        'post',
        true,
        JSON.stringify({ paging: { page: page + 1, pageSize: TOUR_PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setPage(page + 1);
      setData(data => data && data.concat(json.data.transactionRequests));
    }
  }, [page, dispatch]);

  React.useEffect(() => {
    const fetch = async () => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getTopupHistories}`,
          'post',
          true,
          JSON.stringify({ paging: { page: 1, pageSize: TOUR_PAGE_SIZE } }),
        ),
      );
      if (json.code === 200) {
        setPage(1);
        setData(json.data.transactionRequests);
        setTotal(json.data.total);
      }
    };
    fetch();
  }, [dispatch]);
  return (
    <div>
      <Helmet>
        <title>{intl.formatMessage({ id: 'm.topup.rechargeHistory' })}</title>
      </Helmet>
      <TopupHistoryBreadcrumbs onChange={onChange} />
      <Typography
        variant="h5"
        style={{ margin: '16px 0px', height: '40px', alignItems: 'center', display: 'flex' }}
      >
        <FormattedMessage id="m.topup.rechargeHistory" />
      </Typography>
      {data ? (
        <>
          {data.map(item => (
            <TransactionWrapper key={item.id}>
              <div style={{ justifyContent: 'center' }}>
                <img
                  src={item.bankInfo.logo}
                  alt=""
                  style={{
                    width: '120px',
                    objectFit: 'contain',
                  }}
                />
              </div>
              <div style={{ width: '100%', margin: '0px 10px' }}>
                <div
                  style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
                >
                  <Typography variant="subtitle2" style={{ margin: '8px 0px' }}>
                    {item.bankInfo.description}
                  </Typography>
                  <Typography variant="body2" style={{ color: `${getStatusColor(item.status)}` }}>
                    <FormattedMessage id={item.status} />
                  </Typography>
                </div>

                <Typography variant="body2">
                  <FormattedMessage id="m.topup.accountName" />
                  :&nbsp;
                  {item.bankInfo.accountName}
                </Typography>
                <div style={{ display: 'flex' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.topup.accountNumber" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: SECONDARY }}>
                    {item.bankInfo.accountVnd}
                  </Typography>
                </div>

                <div style={{ display: 'flex' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.topup.moneyAmount" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: SECONDARY }}>
                    <FormattedNumber value={item.amount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                  <Typography variant="body2">
                    &nbsp; ({`${voca.capitalize(writtenNumber(item.amount, { lang: locale }))}`}
                    &nbsp; <FormattedMessage id="currencyFullText" />)
                  </Typography>
                </div>
                <div style={{ display: 'flex' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.topup.transferDescription" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: SECONDARY }}>
                    {item.transferDescription
                      .toUpperCase()
                      .split(' ')
                      .join(intl.formatMessage({ id: 'm.topup.whiteSpace' }))}
                  </Typography>
                  <Typography variant="body2">{`(${item.transferDescription.toUpperCase()})`}</Typography>
                </div>

                <Typography variant="body2">
                  <FormattedMessage id="m.topup.createdTime" />
                  :&nbsp;
                  {item.createdTime}
                </Typography>
                <Typography variant="body2">
                  <FormattedMessage id="m.topup.expiredTime" />
                  :&nbsp;
                  {item.expiredTime}
                </Typography>
              </div>
            </TransactionWrapper>
          ))}
          {total - TOUR_PAGE_SIZE * page > 0 && (
            <div style={{ textAlign: 'center', marginTop: '23px' }}>
              <Button onClick={loadMore}>
                <Typography style={{ color: BLUE }}>
                  <FormattedMessage
                    id="result.displayMore"
                    values={{ num: total - TOUR_PAGE_SIZE * page }}
                  />
                </Typography>
              </Button>
            </div>
          )}
        </>
      ) : (
        <>
          <TransactionSkeleton />
          <TransactionSkeleton />
          <TransactionSkeleton />
          <TransactionSkeleton />
          <TransactionSkeleton />
        </>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(RechargeHistory));
