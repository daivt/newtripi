import { Button, Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY } from '../../../../../colors';
import { ROUTES } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import profile_edit from '../../../../../svg/profile_edit.svg';
import { CustomTab, CustomTabs, Line } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { CURRENT_PARAM_NAME, RECHARGE_PARAM_NAME } from '../../../constants';
import FlightDashboard from './FlightDashboard';
import HotelDashboard from './HotelDashboard';

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
    userData: state.account.userData,
    booker: state.account.booker,
  };
}
const Box = styled.div`
  border: 1px solid ${GREY};
  border-radius: 4px;
  padding: 12px 10px;
  height: 80px;
`;
const TABS = [
  { id: 'flight', content: <FlightDashboard /> },
  { id: 'hotel', content: <HotelDashboard /> },
];
export interface FilterParams {
  fromAirport: string | null;
  fromDate: string;
  provinceId: number | null;
  toAirport: string | null;
  toDate: string;
}
interface IGeneralProps extends ReturnType<typeof mapState2Props> {}
interface State {
  currentTabIndex: number;
}
class General extends React.PureComponent<IGeneralProps> {
  state: State = { currentTabIndex: 0 };
  setRecharge(search: string) {
    const params = new URLSearchParams(search);
    params.set(RECHARGE_PARAM_NAME, 'recharge');
    return params.toString();
  }
  render() {
    const { search, userData, booker } = this.props;
    const { currentTabIndex } = this.state;
    return (
      <div>
        <Line style={{ justifyContent: 'space-between', alignItems: undefined }}>
          <div>
            <Line>
              <Typography variant="h5" style={{ lineHeight: '32px' }}>
                {userData && userData.name}
              </Typography>
              {userData && (
                <img
                  src={userData.userTitle.icon}
                  style={{ width: '32px', marginLeft: '12px' }}
                  alt=""
                />
              )}
            </Line>
            <Line style={{ marginTop: '12px' }}>
              <Typography variant="body1">
                <FormattedMessage id="account.pointCanUse" />
                :&nbsp;
              </Typography>
              <Typography variant="body1" color="primary">
                {userData ? <FormattedNumber value={userData.loyalty.availablePoint} /> : '---'}
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </Line>
            <Line style={{ marginTop: '12px' }}>
              <Typography variant="body1">
                <FormattedMessage id="account.main" />
                :&nbsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {userData && <FormattedNumber value={userData.credit} />}&nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </div>
          <Line>
            <Link
              to={{
                pathname: ROUTES.management,
                search: `?${this.setRecharge(search)}`,
                state: { backableToGeneral: true },
              }}
            >
              <Button
                style={{
                  minWidth: '130px',
                  marginRight: '16px',
                  display: userData && !userData.isAgencyManager ? 'none' : undefined,
                }}
                color="secondary"
                variant="contained"
                size="large"
              >
                <Typography variant="button">
                  <FormattedMessage id="m.topup.recharge" />
                </Typography>
              </Button>
            </Link>
            <Link
              to={{
                pathname: ROUTES.management,
                search: `?${CURRENT_PARAM_NAME}=m.profile`,
                state: { backableToGeneral: true },
              }}
            >
              <Button
                style={{ minWidth: '130px', marginRight: '16px', height: '38px' }}
                variant="outlined"
                size="large"
              >
                <img src={profile_edit} alt="" />
                &nbsp;
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="m.topup.updateProfile" />
                </Typography>
              </Button>
            </Link>
          </Line>
        </Line>
        <Grid
          container
          spacing={2}
          style={{ marginTop: '20px', marginBottom: '30px', width: '100%' }}
        >
          <Grid item style={{ minWidth: '215px' }}>
            <Box>
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="m.dashboard.revenue" />
              </Typography>

              <Typography color="primary" style={{ fontSize: '24px', lineHeight: '28px' }}>
                {booker.report ? (
                  <FormattedNumber value={booker.report.summaries.totalSales} />
                ) : (
                  '---'
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item style={{ minWidth: '215px' }}>
            <Box>
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="m.dashboard.profit" />
              </Typography>

              <Typography color="secondary" style={{ fontSize: '24px', lineHeight: '28px' }}>
                {booker.report ? (
                  <FormattedNumber value={booker.report.summaries.totalMarkupAmount} />
                ) : (
                  '---'
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item style={{ minWidth: '140px' }}>
            <Box>
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="holding" />
              </Typography>
              <Typography color="secondary" style={{ fontSize: '24px', lineHeight: '28px' }}>
                {booker.dashboad ? (
                  <FormattedNumber value={booker.dashboad.numHoldingBooking} />
                ) : (
                  '---'
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item style={{ minWidth: '140px' }}>
            <Box>
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="expire" />
              </Typography>
              <Typography color="textPrimary" style={{ fontSize: '24px', lineHeight: '28px' }}>
                {booker.dashboad ? (
                  <FormattedNumber value={booker.dashboad.nearlyExpired} />
                ) : (
                  '---'
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item style={{ flex: 1 }}>
            <Box>
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="account.waitingProcess" />
              </Typography>
              <Typography style={{ fontSize: '24px', lineHeight: '28px', color: BLUE }}>
                {booker.request ? <FormattedNumber value={booker.request.total} /> : '---'}
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <Typography variant="h6">
          <FormattedMessage id="report" />
        </Typography>
        <CustomTabs
          value={currentTabIndex === -1 ? false : currentTabIndex}
          onChange={(e, val) => this.setState({ currentTabIndex: val })}
        >
          {TABS.map(tab => (
            <CustomTab label={<FormattedMessage id={tab.id} />} key={tab.id} />
          ))}
        </CustomTabs>
        {TABS[currentTabIndex].content}
      </div>
    );
  }
}

export default connect(mapState2Props)(General);
