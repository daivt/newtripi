import { FormControlLabel, MenuItem, Radio, RadioGroup, Select, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { FilterParams } from '.';
import { API_PATHS } from '../../../../../API';
import { AppState } from '../../../../../redux/reducers';
import iconFlightDown from '../../../../../svg/ic_flight_down.svg';
import iconFlightUp from '../../../../../svg/ic_flight_up.svg';
import { BootstrapInput, Line } from '../../../../common/components/elements';
import Swap from '../../../../common/components/Swap';
import { Airport } from '../../../../common/models';
import { fetchThunk } from '../../../../common/redux/thunks';
import AsyncSelect from '../../../../search/components/AsyncSelect';
import { getLabel } from '../../../../search/components/FlightHuntSearch';
import { NotableAirports } from '../../../../search/constants';
import { filterByData } from '../../../constants';
import ChooseDateDialog from '../../common/ChooseDateDialog';

function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <div>
        <Typography variant="body2">
          {option.location} ({option.code})
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  params: FilterParams;
  onChange(params: FilterParams): void;
}

type Option = 'all' | 'custom';
const FlightFilterDashboard: React.FunctionComponent<Props> = props => {
  const { onChange, params, dispatch, intl } = props;
  const [sortBy, setSortBy] = React.useState(3);
  const [option, setOption] = React.useState<Option>('all');
  const [fromAirport, setFromAirport] = React.useState<Airport | null>(null);
  const [toAirport, setToAirport] = React.useState<Airport | null>(null);
  const [ticktock, setTick] = React.useState(false);
  const [openChooseDate, setOpen] = React.useState(false);

  return (
    <div style={{ marginTop: '20px' }}>
      <Line>
        <Line>
          <div style={{ minWidth: '170px' }}>
            <Typography variant="body2">
              <FormattedMessage id="m.dashboard.road" />
            </Typography>
          </div>
          <RadioGroup row style={{ justifyContent: 'space-between', width: '200px' }}>
            <FormControlLabel
              value="all"
              control={<Radio size="small" />}
              checked={option === 'all'}
              onChange={(e, checked) => {
                checked && setOption('all');
                onChange({ ...params, fromAirport: null, toAirport: null });
              }}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="all" />
                </Typography>
              }
            />
            <FormControlLabel
              value="custom"
              control={<Radio size="small" />}
              checked={option === 'custom'}
              onChange={(e, checked) => {
                checked && setOption('custom');
                onChange({
                  ...params,
                  fromAirport: fromAirport ? fromAirport.code : null,
                  toAirport: toAirport ? toAirport.code : null,
                });
              }}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="custom" />
                </Typography>
              }
            />
          </RadioGroup>
        </Line>
        <Line
          key={`${ticktock}`}
          style={{
            flex: 1,
            transition: 'opacity 0.5s',
            opacity: option === 'custom' ? 1 : 0,
            pointerEvents: option === 'custom' ? undefined : 'none',
            marginLeft: '10px',
          }}
        >
          <AsyncSelect<Airport>
            error={false}
            minimizedWidth="215px"
            fullWidth="290px"
            loadOptions={async (str: string) => {
              const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
              return json.data;
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightUp} />}
            optionHeader={<FormattedMessage id="search.airports" />}
            defaultValue={fromAirport ? getLabel(fromAirport) : ''}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseOrigin' })}
            focusOptions={NotableAirports}
            getLabel={getLabel}
            renderOption={renderOption}
            onSelect={airport => {
              onChange({ ...params, fromAirport: airport ? airport.code : null });
              setFromAirport(airport || null);
            }}
          />
          <div
            style={{
              padding: '8px 0',
              width: '0px',
              overflow: 'visible',
              position: 'relative',
              zIndex: 10,
              display: 'flex',
              justifyContent: 'center',
              height: '40px',
              boxSizing: 'content-box',
              alignItems: 'center',
            }}
          >
            <Swap
              onClick={async () => {
                onChange({
                  ...params,
                  fromAirport: params.toAirport,
                  toAirport: params.fromAirport,
                });
                setFromAirport(toAirport);
                setToAirport(fromAirport);
                setTick(!ticktock);
                // const origin = search.origin;
                // await dispatch(setOrigin(search.destination));
                // await dispatch(setDestination(origin)); // need to wait for store change before changing tick
                // this.setState({
                //   tick: !tick,
                //   error: { ...error, destination: error.origin, origin: error.destination },
                // });
              }}
            />
          </div>
          <AsyncSelect<Airport>
            error={false}
            minimizedWidth="215px"
            fullWidth="290px"
            loadOptions={async (str: string) => {
              const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
              return json.data;
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={iconFlightDown} />}
            optionHeader={<FormattedMessage id="search.airports" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseDestination' })}
            defaultValue={toAirport ? getLabel(toAirport) : ''}
            focusOptions={NotableAirports}
            getLabel={getLabel}
            renderOption={renderOption}
            onSelect={airport => {
              onChange({ ...params, toAirport: airport ? airport.code : null });
              setToAirport(airport || null);
            }}
          />
        </Line>
      </Line>
      <Line style={{ marginTop: '8px' }}>
        <Typography variant="body2" style={{ minWidth: '170px' }}>
          <FormattedMessage id="m.dashboard.optionTime" />
        </Typography>
        <div
          style={{
            background: 'white',
            width: '220px',
          }}
        >
          <Select
            value={sortBy}
            onChange={event => {
              setSortBy(event.target.value as number);
              const target = filterByData.find(v => v.id === (event.target.value as number));
              if (target && target.id !== 7) {
                onChange({ ...params, fromDate: target.fromDate, toDate: target.toDate });
              } else {
                setOpen(true);
              }
            }}
            input={<BootstrapInput />}
            fullWidth
          >
            {filterByData.map(item => (
              <MenuItem key={item.id} value={item.id}>
                <Typography variant="body2">
                  {sortBy !== 7 ? (
                    <FormattedMessage id={item.text} />
                  ) : item.id === 7 ? (
                    `${params.fromDate.split('-').join('/')} - ${params.toDate.split('-').join('/')}`
                  ) : (
                    <FormattedMessage id={item.text} />
                  )}
                </Typography>
              </MenuItem>
            ))}
          </Select>
        </div>
      </Line>
      {openChooseDate && (
        <ChooseDateDialog
          open={openChooseDate}
          onClose={() => setOpen(false)}
          params={{ fromDate: params.fromDate, toDate: params.toDate }}
          onChange={chooseDateParams => {
            onChange({
              ...params,
              fromDate: chooseDateParams.fromDate,
              toDate: chooseDateParams.toDate,
            });
            setOpen(false);
          }}
        />
      )}
    </div>
  );
};

export default connect()(injectIntl(FlightFilterDashboard));
