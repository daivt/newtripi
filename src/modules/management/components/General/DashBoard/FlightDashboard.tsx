import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { FilterParams } from '.';
import { API_PATHS } from '../../../../../API';
import { BLUE, LIGHT_BLUE, SECONDARY } from '../../../../../colors';
import { DATE_FORMAT, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NoDataResult from '../../../../common/components/NoDataResult';
import { fetchThunk } from '../../../../common/redux/thunks';
import FlightFilterDashboard from './FlightFilterDashboard';

am4core.useTheme(am4themes_animated);

interface Props {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
interface State {
  data?: some;
  params: FilterParams;
  dataChart: some[];
  loading: boolean;
}
const LABEL = [
  { id: 'numVNATicket', name: 'Vietnam Airlines', color: BLUE },
  { id: 'numVJTicket', name: 'VietJet', color: SECONDARY },
  { id: 'numBLTicket', name: 'Jestar', color: LIGHT_BLUE },
  // numBLTicket: 'Jetstar Airways',
];
class FlightDashboard extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      params: {
        fromDate: moment()
          .startOf('month')
          .format(DATE_FORMAT),
        toDate: moment().format(DATE_FORMAT),
        fromAirport: null,
        toAirport: null,
        provinceId: null,
      },
      loading: false,
      dataChart: [],
    };
  }
  chart: am4charts.PieChart | null = null;

  async componentDidMount() {
    const { dispatch } = this.props;
    const { params } = this.state;
    this.setState({ loading: true });
    const json = await dispatch(
      fetchThunk(`${API_PATHS.flightBookingsReport}`, 'post', true, JSON.stringify(params)),
    );
    if (json.code === 200) {
      this.setState({ data: json.data }, () => {
        const temp = LABEL.map(v => {
          if (this.state.data && this.state.data.summaries[v.id] > 0) {
            return { ...v, value: this.state.data.summaries[v.id] };
          }
          return undefined;
        }).filter(v => v) as some[];
        this.setState({ dataChart: temp });
        const chart = am4core.create('flightPieChartDashboard', am4charts.PieChart);
        chart.data = temp;
        const pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.dataFields.value = 'value';
        pieSeries.dataFields.category = 'name';
        pieSeries.slices.template.propertyFields.fill = 'color';
        pieSeries.legendSettings.labelText = '[bold {color}]{category}[/]';
        pieSeries.legendSettings.valueText = '[]';
        pieSeries.labels.template.text = "{name}: [bold]{value.percent.formatNumber('#.00')}%";
        chart.legend = new am4charts.Legend();
        chart.legend.useDefaultMarker = true;
        const marker: any = chart.legend.markers.template.children.getIndex(0);
        if (marker) {
          marker.cornerRadius(12, 12, 12, 12);
          marker.strokeWidth = 2;
          marker.strokeOpacity = 1;
          marker.stroke = am4core.color('#ccc');
        }
        chart.legend.position = 'bottom';

        this.chart = chart;
      });
    }
    this.setState({ loading: false });
  }

  async componentDidUpdate(prevProps: Props, prevState: State) {
    if (JSON.stringify(this.state.params) !== JSON.stringify(prevState.params)) {
      const { dispatch } = this.props;
      const { params } = this.state;
      this.setState({ loading: true });
      const json = await dispatch(
        fetchThunk(`${API_PATHS.flightBookingsReport}`, 'post', true, JSON.stringify(params)),
      );
      if (json.code === 200) {
        this.setState({ data: json.data }, () => {
          const temp = LABEL.map(v => {
            if (this.state.data && this.state.data.summaries[v.id] > 0) {
              return { ...v, value: this.state.data.summaries[v.id] };
            }
            return undefined;
          }).filter(v => v) as some[];
          this.setState({ dataChart: temp });
          if (this.chart) {
            this.chart.data = temp;
            this.chart.appear();
          }
        });
      }
      this.setState({ loading: false });
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  render() {
    const { params, data, loading, dataChart } = this.state;
    const show = dataChart.length > 0 && data;
    if (!data) {
      return (
        <LoadingIcon
          style={{
            height: '700px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
      );
    }
    return (
      <div>
        <FlightFilterDashboard
          params={params}
          onChange={(params: FilterParams) => {
            this.setState({ params });
          }}
        />
        <Line style={{ marginTop: '24px' }}>
          <div style={{ marginRight: '200px' }}>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalOrders" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="primary">
                {data.summaries.totalOrders}
              </Typography>
            </Line>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalBonusPoint" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="primary">
                {<FormattedNumber value={data.summaries.totalBonusPoint} />}&nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </Line>
          </div>
          <div>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalProfit" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {<FormattedNumber value={data.summaries.totalMarkupAmount} />}&nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalSales" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {<FormattedNumber value={data.summaries.totalSales} />}&nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </div>
        </Line>

        <div style={{ position: 'relative', minHeight: '500px' }}>
          <div
            id="flightPieChartDashboard"
            style={{
              height: '500px',
              display: show ? undefined : 'none',
              opacity: loading ? 0.5 : 1,
              transition: 'opacity 0.3s',
            }}
          />
          {loading && (
            <LoadingIcon
              style={{
                marginTop: '196px',
                position: 'absolute',
                display: 'flex',
                justifyContent: 'center',
                top: 0,
                width: '100%',
              }}
            />
          )}
          {!show && !loading && (
            <div
              style={{
                height: '500px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <NoDataResult id="m.noTransaction" />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(FlightDashboard);
