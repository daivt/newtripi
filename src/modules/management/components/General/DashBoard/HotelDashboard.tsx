import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { FilterParams } from '.';
import { API_PATHS } from '../../../../../API';
import { GREY, LIGHT_BLUE, PRIMARY, SECONDARY } from '../../../../../colors';
import { DATE_FORMAT, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NoDataResult from '../../../../common/components/NoDataResult';
import { fetchThunk } from '../../../../common/redux/thunks';
import HotelFilterDashboard from './HotelFilterDashboard';

am4core.useTheme(am4themes_animated);

interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
interface State {
  data?: some;
  dataChart: some[];
  params: FilterParams;
  loading: boolean;
}
const COLOR = [GREY, PRIMARY, SECONDARY, LIGHT_BLUE, '#795548', '#5E35B1'];
class HotelDashboard extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      params: {
        fromDate: moment()
          .startOf('month')
          .format(DATE_FORMAT),
        toDate: moment().format(DATE_FORMAT),
        fromAirport: null,
        toAirport: null,
        provinceId: null,
      },
      loading: false,
      dataChart: [],
    };
  }
  chart: am4charts.PieChart | null = null;

  async componentDidMount() {
    const { dispatch, intl } = this.props;
    const { params } = this.state;
    this.setState({ loading: true });
    const json = await dispatch(
      fetchThunk(`${API_PATHS.hotelBookingsReport}`, 'post', true, JSON.stringify(params)),
    );
    if (json.code === 200) {
      this.setState({ data: json.data }, () => {
        const temp =
          this.state.data &&
          this.state.data.summaries.numBookingByStar
            .map((v: number, index: number) => {
              if (v > 0) {
                return {
                  name: intl.formatMessage({ id: 'm.star' }, { num: index }),
                  value: v,
                  color: COLOR[index],
                };
              }
              return undefined;
            })
            .filter((v: some) => v);
        this.setState({ dataChart: temp });
        const chart = am4core.create('hotelPieChartDashboard', am4charts.PieChart);
        chart.data = temp;
        const pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.dataFields.value = 'value';
        pieSeries.dataFields.category = 'name';
        pieSeries.slices.template.propertyFields.fill = 'color';
        pieSeries.legendSettings.labelText = '[bold {color}]{category}[/]';
        pieSeries.legendSettings.valueText = '[]';
        pieSeries.labels.template.text = "{name}: [bold]{value.percent.formatNumber('#.00')}%";
        chart.legend = new am4charts.Legend();
        chart.legend.useDefaultMarker = true;
        const marker: any = chart.legend.markers.template.children.getIndex(0);
        if (marker) {
          marker.cornerRadius(12, 12, 12, 12);
          marker.strokeWidth = 2;
          marker.strokeOpacity = 1;
          marker.stroke = am4core.color('#ccc');
        }
        chart.legend.position = 'bottom';
        this.chart = chart;
      });
    }
    this.setState({ loading: false });
  }

  async componentDidUpdate(prevProps: Props, prevState: State) {
    if (JSON.stringify(this.state.params) !== JSON.stringify(prevState.params)) {
      const { dispatch, intl } = this.props;
      const { params } = this.state;
      this.setState({ loading: true });
      const json = await dispatch(
        fetchThunk(`${API_PATHS.hotelBookingsReport}`, 'post', true, JSON.stringify(params)),
      );
      if (json.code === 200) {
        this.setState({ data: json.data }, () => {
          const temp =
            this.state.data &&
            this.state.data.summaries.numBookingByStar
              .map((v: number, index: number) => {
                if (v > 0) {
                  return {
                    name: intl.formatMessage({ id: index ? 'm.star' : 'm.orther' }, { num: index }),
                    value: v,
                    color: COLOR[index],
                  };
                }
                return undefined;
              })
              .filter((v: some) => v);
          this.setState({ dataChart: temp });
          if (this.chart) {
            this.chart.data = temp;
            this.chart.appear();
          }
        });
      }
      this.setState({ loading: false });
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  render() {
    const { params, data, loading, dataChart } = this.state;
    const show = dataChart.length > 0 && data;
    if (!data) {
      return (
        <LoadingIcon
          style={{
            height: '700px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
      );
    }
    return (
      <div>
        <HotelFilterDashboard
          params={params}
          onChange={(params: FilterParams) => {
            this.setState({ params });
          }}
        />
        <Line style={{ marginTop: '24px' }}>
          <div style={{ marginRight: '200px' }}>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalOrders" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="primary">
                {data.summaries.totalOrders}
              </Typography>
            </Line>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalBonusPoint" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="primary">
                {<FormattedNumber value={data.summaries.totalBonusPoint} />}&nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </Line>
          </div>
          <div>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalProfit" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {<FormattedNumber value={data.summaries.totalMarkupAmount} />}&nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
            <Line style={{ justifyContent: 'space-between', height: '40px' }}>
              <Typography variant="body1">
                <FormattedMessage id="m.totalSales" />
                :&emsp;
              </Typography>
              <Typography variant="body1" color="secondary">
                {<FormattedNumber value={data.summaries.totalSales} />}&nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </div>
        </Line>

        <div style={{ position: 'relative', minHeight: '500px' }}>
          <div
            id="hotelPieChartDashboard"
            style={{
              height: '500px',
              display: show ? undefined : 'none',
              opacity: loading ? 0.5 : 1,
              transition: 'opacity 0.3s',
            }}
          />
          {(loading || !data) && (
            <LoadingIcon
              style={{
                marginTop: '196px',
                position: 'absolute',
                display: 'flex',
                justifyContent: 'center',
                top: 0,
                width: '100%',
              }}
            />
          )}
          {!show && !loading && (
            <div
              style={{
                height: '500px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <NoDataResult id="m.noTransaction" />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(injectIntl(HotelDashboard));
