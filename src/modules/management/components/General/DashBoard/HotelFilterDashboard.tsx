import { MenuItem, Select, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import voca from 'voca';
import { FilterParams } from '.';
import { API_PATHS } from '../../../../../API';
import { AppState } from '../../../../../redux/reducers';
import { BootstrapInput, Line } from '../../../../common/components/elements';
import { fetchThunk } from '../../../../common/redux/thunks';
import AsyncSelect from '../../../../search/components/AsyncSelect';
import { filterByData } from '../../../constants';
import ChooseDateDialog from '../../common/ChooseDateDialog';
interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  params: FilterParams;
  onChange(params: FilterParams): void;
}

type Option = 'all' | 'custom';
interface Provinces {
  id: number;
  name: string;
}
function renderOption(option: Provinces) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2">{option.name}</Typography>
    </div>
  );
}

const HotelFilterDashboard: React.FunctionComponent<Props> = props => {
  const { onChange, params, dispatch, intl } = props;
  const [sortBy, setSortBy] = React.useState(3);
  const [listProvinces, setList] = React.useState<Provinces[]>([]);
  const [openChooseDate, setOpen] = React.useState(false);
  React.useEffect(() => {
    const fetch = async () => {
      const json = await dispatch(fetchThunk(API_PATHS.getProvinces));
      if (json.code === 200) {
        setList(json.data);
      }
    };
    fetch();
  }, [dispatch]);
  return (
    <div>
      <div style={{ marginTop: '20px', display: 'flex' }}>
        <Line style={{ marginRight: '50px' }}>
          <Typography variant="body2" style={{ marginRight: '20px' }}>
            <FormattedMessage id="m.dashboard.optionTime" />
          </Typography>
          <div
            style={{
              flex: 1,
              background: 'white',
              width: '264px',
            }}
          >
            <Select
              value={sortBy}
              onChange={event => {
                setSortBy(event.target.value as number);
                const target = filterByData.find(v => v.id === (event.target.value as number));
                if (target && target.id !== 7) {
                  onChange({ ...params, fromDate: target.fromDate, toDate: target.toDate });
                } else {
                  setOpen(true);
                }
              }}
              input={<BootstrapInput />}
              fullWidth
            >
              {filterByData.map(item => (
                <MenuItem key={item.id} value={item.id}>
                  <Typography variant="body2">
                    {sortBy !== 7 ? (
                      <FormattedMessage id={item.text} />
                    ) : item.id === 7 ? (
                      `${params.fromDate.split('-').join('/')} - ${params.toDate
                        .split('-')
                        .join('/')}`
                    ) : (
                      <FormattedMessage id={item.text} />
                    )}
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </div>
        </Line>
        <Line>
          <Typography variant="body2" style={{ marginRight: '20px' }}>
            <FormattedMessage id="province" />
          </Typography>
          <div
            style={{
              width: '288px',
            }}
          >
            <AsyncSelect<Provinces>
              error={false}
              minimizedWidth="215px"
              fullWidth="330px"
              loadOptions={async (str: string) => {
                const text = voca.latinise(str);
                if (text === str) {
                  return listProvinces.filter(v =>
                    voca
                      .latinise(v.name)
                      .toLowerCase()
                      .includes(str.toLowerCase()),
                  );
                }
                return listProvinces.filter(v => v.name.toLowerCase().includes(str.toLowerCase()));
              }}
              icon={<></>}
              optionHeader={''}
              focusOptionHeader={''}
              placeholder={intl.formatMessage({ id: 'search.chooseLocation' })}
              focusOptions={listProvinces}
              getLabel={province => province.name}
              renderOption={renderOption}
              onSelect={province => {
                onChange({ ...params, provinceId: province ? province.id : null });
              }}
            />
          </div>
        </Line>
      </div>
      {openChooseDate && (
        <ChooseDateDialog
          open={openChooseDate}
          onClose={() => setOpen(false)}
          params={{ fromDate: params.fromDate, toDate: params.toDate }}
          onChange={chooseDateParams => {
            onChange({
              ...params,
              fromDate: chooseDateParams.fromDate,
              toDate: chooseDateParams.toDate,
            });
            setOpen(false);
          }}
        />
      )}
    </div>
  );
};

export default connect()(injectIntl(HotelFilterDashboard));
