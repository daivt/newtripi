import { Button, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { BLUE } from '../../../../../colors';
import { PAGE_SIZE, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import MessageDialog from '../../../../common/components/MessageDialog';
import NoDataResult from '../../../../common/components/NoDataResult';
import { fetchThunk } from '../../../../common/redux/thunks';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, TourManagementBookingItemActionType } from '../../../constants';
import OrderFilters, { OrderFiltersParams } from '../OrderFilters';
import TourBookingDetail from './TourBookingDetail';
import TourBookingItem, { TourBookingItemSkeleton } from './TourBookingItem';
import TourPayLater from './TourPayLater';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const Tour: React.FunctionComponent<Props> = props => {
  const { dispatch, router } = props;
  const [bookings, setBookings] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [message, setMessage] = React.useState<string>('');
  const [filters, setFilters] = React.useState<OrderFiltersParams>({});
  const [deletedId, setDeletedId] = React.useState(0);

  const search = router.location.search;
  const params = new URLSearchParams(search);
  const action = params.get(ACTION_PARAM_NAME) as TourManagementBookingItemActionType | null;

  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getTourBookings}`,
        'post',
        true,
        JSON.stringify({ filters, paging: { page: page + 1, size: PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setBookings(b => b && b.concat(json.data.bookings));
      setPage(page + 1);
    }
    setLoading(false);
  }, [page, dispatch, filters]);

  const filtersStr = JSON.stringify(filters);
  React.useEffect(() => {
    const fetch = async () => {
      setBookings([]);
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getTourBookings}`,
          'post',
          true,
          JSON.stringify({
            filters: JSON.parse(filtersStr),
            paging: { page: 1, size: PAGE_SIZE },
          }),
        ),
      );
      if (json.code === 200) {
        setPage(1);
        setBookings(json.data.bookings);
        setTotal(json.data.total);
      } else {
        setTotal(0);
      }
      setLoading(false);
    };
    fetch();
  }, [filtersStr, dispatch, deletedId]);

  const deleteFn = React.useCallback(
    async (bookingId: number) => {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.deactivate}`, 'post', true, JSON.stringify({ bookingId, module: 'tour' })),
      );
      if (json.code === 200) {
        setMessage(json.message);
        const params = new URLSearchParams(search);
        params.delete(BOOKING_ID_PARAM_NAME);
        params.delete(ACTION_PARAM_NAME);
        dispatch(replace({ search: `${params.toString()}` }));
        setDeletedId(bookingId);
      } else {
        setMessage(json.message);
      }
    },
    [dispatch, search],
  );

  return (
    <>
      <div style={{ display: action ? 'none' : undefined }}>
        <OrderFilters
          loading={loading}
          onChange={(params: OrderFiltersParams) => {
            setFilters(params);
          }}
        />
        <div>
          {bookings ? (
            <>
              {bookings.map(one => (
                <TourBookingItem key={one.id} data={one} />
              ))}
              {!loading && total - PAGE_SIZE * page > 0 && (
                <div style={{ textAlign: 'center', marginTop: '23px' }}>
                  <Button onClick={loadMore}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage id="result.displayMore" values={{ num: total - PAGE_SIZE * page }} />
                    </Typography>
                  </Button>
                </div>
              )}
              {loading && (
                <>
                  <TourBookingItemSkeleton />
                  <TourBookingItemSkeleton />
                  <TourBookingItemSkeleton />
                </>
              )}
              {!bookings.length && !loading && <NoDataResult id="m.noData" style={{ marginTop: '48px' }} />}
            </>
          ) : (
            <>
              <TourBookingItemSkeleton />
              <TourBookingItemSkeleton />
              <TourBookingItemSkeleton />
              <TourBookingItemSkeleton />
              <TourBookingItemSkeleton />
            </>
          )}
        </div>
        <MessageDialog
          style={{ width: '180px' }}
          show={!!message}
          disabledBtn
          onClose={() => setMessage('')}
          message={
            <div style={{ textAlign: 'center' }}>
              <Typography>
                <FormattedMessage id={message === 'Success!' ? 'deleteSuccess' : 'deleteFail'} />
              </Typography>
            </div>
          }
        />
      </div>
      {action === 'detail' && (
        <TourBookingDetail
          onDelete={bookingId => {
            deleteFn(bookingId);
          }}
        />
      )}
      {action === 'payLater' && <TourPayLater />}
    </>
  );
};

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}
export default connect(mapStateToProps)(Tour);
