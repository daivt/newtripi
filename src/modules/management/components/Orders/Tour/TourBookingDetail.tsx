import { Button, Typography } from '@material-ui/core';
import moment, { now } from 'moment';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { GREY, BLUE } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import ic_delete from '../../../../../svg/delete.svg';
import iconDelete from '../../../../../svg/ic_delete_ticket_alert.svg';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import PassengerCount from '../../../../common/components/PassengerCount';
import { fetchThunk } from '../../../../common/redux/thunks';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  getStatusColor,
  TourManagementBookingItemActionType,
} from '../../../constants';
import TourDetailBreadCrumbs from './TourDetailBreadCrumbs';
const Line = styled.div`
  height: 36px;
  display: flex;
  align-items: center;
  width: 100%;
`;
interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDelete: (bookingId: number) => void;
}

interface State {
  data?: some;
  confirm: boolean;
}
class TourBookingDetail extends React.PureComponent<Props, State> {
  state: State = { confirm: false };

  async componentDidMount() {
    const { router, dispatch } = this.props;
    const params = new URLSearchParams(router.location.search);
    const bookingId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getTourBookingDetail}?bookingId=${bookingId}`, 'get', true),
    );
    if (json.code === 200) {
      this.setState({ data: json.data });
    }
  }
  getRePayLink() {
    const { router } = this.props;
    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'payLater' as TourManagementBookingItemActionType);
    return params.toString();
  }

  public render() {
    const { data, confirm } = this.state;
    const { onDelete, router } = this.props;
    if (!data) {
      return (
        <div style={{ margin: '42px' }}>
          <LoadingIcon />
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>{data.tourName}</title>
        </Helmet>
        <TourDetailBreadCrumbs title={data.tourName} />
        <Typography variant="h5" style={{ paddingTop: '16px' }}>
          <FormattedMessage id="m.bookingDetail" />
        </Typography>
        <Typography variant="subtitle2" style={{ margin: '16px 0px' }}>
          {data.tourName}
        </Typography>
        <div
          style={{
            padding: '16px',
            background: 'white',
            border: `0.5px solid ${GREY}`,
            borderRadius: '4px',
            display: 'flex',
          }}
        >
          <div style={{ width: '50%' }}>
            <Typography variant="h6">
              <FormattedMessage id="m.order.orderInfo" />
            </Typography>
            <Line style={{ marginTop: '16px' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.order.status" />
                :&nbsp;
              </Typography>
              <Typography
                variant="body2"
                style={{
                  color: `${getStatusColor(data.status)}`,
                }}
              >
                <FormattedMessage id={data.status} />
                {/* <FormattedMessage id={data.bookStatus} /> */}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.order.bookingDate" />
                :&nbsp;
                {data.bookingTime}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.order.bookingCode" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" style={{ color: BLUE }}>
                {data.orderCode}
              </Typography>
            </Line>
            <Typography variant="h6" style={{ margin: '16px 0px' }}>
              <FormattedMessage id="m.order.tour.contactInfo" />
            </Typography>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="tour.booking.fullName" />
                :&nbsp;
                {data.mainContact.name}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="telephone" />
                :&nbsp;
                {data.mainContact.phone}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="email" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" style={{ color: BLUE }}>
                {data.mainContact.email}
              </Typography>
            </Line>
            {data.note && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="note" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2">{data.note}</Typography>
              </Line>
            )}
            <Typography variant="h6" style={{ margin: '16px 0px' }}>
              <FormattedMessage id="m.tourDetail" />
            </Typography>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.package" />
                :&nbsp;
                {data.activityPackageDetail.activityPackageTitle}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.packagePrice" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" color="secondary">
                <FormattedNumber value={data.activityPackageDetail.adultPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.departureDay" />
                :&nbsp;
                {data.departureDate}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="m.order.passengerCount" />
                :&nbsp;
                <PassengerCount
                  adults={data.numAdults}
                  children={data.numChildren}
                  babies={data.numInfants}
                  infants={data.numBabies}
                />
              </Typography>
            </Line>
          </div>
          <div style={{ width: '45%' }}>
            <Typography variant="h6">
              <FormattedMessage id="m.paymentDetail" />
            </Typography>
            {data.numAdults > 0 && (
              <Line style={{ marginTop: '16px', justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="adult" />
                </Typography>
                <Typography variant="body2">
                  {data.numAdults} x{' '}
                  <FormattedNumber value={data.activityPackageDetail.adultPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.numChildren > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="children" />
                </Typography>
                <Typography variant="body2">
                  {data.numChildren} x{' '}
                  <FormattedNumber value={data.activityPackageDetail.childrenPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.numBabies > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="infant" />
                </Typography>
                <Typography variant="body2">
                  {data.numBabies} x{' '}
                  <FormattedNumber value={data.activityPackageDetail.babyPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.numInfants > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="baby" />
                </Typography>
                <Typography variant="body2">
                  {data.numInfants} x{' '}
                  <FormattedNumber value={data.activityPackageDetail.infantPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.paymentMethodFee !== 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage
                    id={
                      data.paymentMethodFee > 0
                        ? 'booking.paymentFixedFeeSide'
                        : 'booking.paymentFixedDiscountSide'
                    }
                  />
                </Typography>
                <Typography
                  variant="body2"
                  color={data.paymentMethodFee > 0 ? 'secondary' : 'primary'}
                >
                  <FormattedNumber value={data.paymentMethodFee} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.discount > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.discountCode" />
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={-data.discount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.pointAmount > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="booking.pointPayment" />
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={-data.pointAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}

            {data.pointAmount > 0 && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="booking.pointPayment" />
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={data.paidPoint} />
                  &nbsp;
                  <FormattedMessage id="point" />
                  &nbsp;/&nbsp;
                  <FormattedNumber value={-data.pointAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {!!data.refundAmount && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="refunded" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={data.refundAmount} />
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            <Line style={{ justifyContent: 'space-between' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.finalPrice" />
              </Typography>
              <Typography variant="body2" color="secondary">
                <FormattedNumber value={data.finalPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
            {data.paymentMethod && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.paymentMethod" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  {data.paymentMethod.name}
                </Typography>
              </Line>
            )}
            {data.transferMoneyMethod && (
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.transferMoneyMethod" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  {data.transferMoneyMethod.name}
                </Typography>
              </Line>
            )}
            <Line style={{ justifyContent: 'space-between' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.order.status" />
              </Typography>
              <Typography
                variant="body2"
                style={{
                  color: `${getStatusColor(data.status)}`,
                }}
              >
                <FormattedMessage id={data.status} />
              </Typography>
            </Line>
            {data.status === 'holding' && moment(data.expiredTime).isAfter(now()) && (
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: this.getRePayLink(),
                  state: { ...router.location.state, backableToDetail: true },
                }}
              >
                <Button variant="contained" color="secondary">
                  <Typography variant="body1">
                    <FormattedMessage id="pay" />
                  </Typography>
                </Button>
              </Link>
            )}
          </div>
        </div>
        <Button
          variant="text"
          style={{ marginTop: '16px' }}
          onClick={() => this.setState({ confirm: true })}
        >
          <img src={ic_delete} alt="" style={{ width: '24px', height: '24px' }} />
          &nbsp;&nbsp;
          <Typography variant="body2">
            <FormattedMessage id="m.orders.tour.delete" />
          </Typography>
        </Button>
        <ConfirmDialog
          message={
            <div style={{ textAlign: 'center' }}>
              <img src={iconDelete} alt="" style={{ height: '117px' }} />
              <Typography variant="h6">
                <FormattedMessage id="m.orders.tour.confirm" />
              </Typography>
            </div>
          }
          show={confirm}
          cancelMessageId="ignore"
          confirmMessageId="ok"
          onAccept={() => {
            onDelete(data.id);
            this.setState({ confirm: false });
          }}
          onCancel={() => {
            this.setState({ confirm: false });
          }}
        />
      </div>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(TourBookingDetail);
