import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME } from '../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  title: string;
}

const TourDetailBreadCrumbs: React.FunctionComponent<Props> = props => {
  const { router } = props;
  const goBackToList = React.useCallback(() => {
    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    return params.toString();
  }, [router.location.search]);

  const backableToList = router.location.state && router.location.state.backableToList;

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() =>
            backableToList
              ? props.dispatch(go(-1))
              : props.dispatch(replace({ search: `${goBackToList()}` }))
          }
        >
          <FormattedMessage id="tour" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.bookingDetail" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router };
}

export default connect(mapStateToProps)(TourDetailBreadCrumbs);
