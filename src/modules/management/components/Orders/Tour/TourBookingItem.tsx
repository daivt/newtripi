import { Button, Divider, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment, { now } from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { GREEN, GREY, RED, BLUE } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import Link from '../../../../common/components/Link';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, getStatusColor } from '../../../constants';
const Wrapper = styled.div`
  background: #ffffff;
  border: 1px solid ${GREY};
  box-sizing: border-box;
  border-radius: 4px;
  min-height: 164px;
  overflow: hidden;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14);
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
}

const TourBookingItem: React.FunctionComponent<Props> = props => {
  const { data, router } = props;
  const isExpried = data.status === 'holding' && data.expiredTime !== null ? true : false;
  const seeDetail = (id: number) => {
    const params = new URLSearchParams(router.location.search);
    params.set(BOOKING_ID_PARAM_NAME, `${id}`);
    params.set(ACTION_PARAM_NAME, 'detail');
    return params.toString();
  };
  return (
    <Wrapper style={{ marginBottom: '10px', overflow: 'hidden' }}>
      {isExpried && (
        <>
          {moment(data.expiredTime).isAfter(now()) ? (
            <div style={{ background: GREEN }}>
              <Typography variant="body2" style={{ color: 'white', padding: '4px 12px' }}>
                <FormattedMessage id="m.orders.tour.countTime" />
                &nbsp;{moment(data.expiredTime).format('LT L')}
              </Typography>
            </div>
          ) : (
            <div style={{ background: RED }}>
              <Typography variant="body2" style={{ color: 'white', padding: '4px 12px' }}>
                <FormattedMessage id="m.order.paymentExpired" />
              </Typography>
            </div>
          )}
        </>
      )}
      <div style={{ display: 'flex' }}>
        <div style={{ position: 'relative', width: '290px' }}>
          <img
            src={data.tourThumbnail}
            alt=""
            style={{
              height: '100%',
              width: '100%',
              objectFit: 'cover',
              position: 'absolute',
            }}
          />
        </div>
        <div style={{ padding: '16px 20px 12px 12px', flex: 1 }}>
          <Typography variant="subtitle2">{data.tourName}</Typography>
          <Divider style={{ marginTop: '16px', marginBottom: '8px' }} />
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div>
              <div style={{ display: 'flex' }}>
                <Typography variant="body2" style={{ lineHeight: '24px' }}>
                  <FormattedMessage id="m.orders.tour.status" />
                  :&nbsp;
                </Typography>
                <Typography
                  variant="body2"
                  style={{
                    color: `${getStatusColor(data.status)}`,
                  }}
                >
                  <FormattedMessage id={data.status} />
                </Typography>
              </div>
              <div style={{ display: 'flex' }}>
                <Typography variant="body2" style={{ lineHeight: '24px' }}>
                  <FormattedMessage id="m.orderStatus.code" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ lineHeight: '24px', color: BLUE }}>
                  {data.orderCode}
                </Typography>
              </div>
              <Typography variant="body2" style={{ lineHeight: '24px' }}>
                <FormattedMessage id="m.orders.tour.startDay" />
                :&nbsp;{data.departureDate}
              </Typography>
              <Typography variant="body2" style={{ lineHeight: '24px' }}>
                <FormattedMessage id="m.orders.tour.amount" />
                :&nbsp;
                <FormattedNumber value={data.numSlots} />
              </Typography>
            </div>
            <div style={{ textAlign: 'end' }}>
              <Typography variant="body2" style={{ lineHeight: '24px' }}>
                <FormattedMessage id="m.finalPrice" />
              </Typography>
              <Typography variant="body1" color="secondary" style={{ lineHeight: '24px' }}>
                <FormattedNumber value={data.finalPrice} /> <FormattedMessage id="currency" />
              </Typography>
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${seeDetail(data.id)}`,
                  state: { backableToActivitiesManage: true },
                }}
              >
                <Button variant="contained" color="secondary" style={{ marginTop: '12px', width: '185px' }}>
                  <Typography variant="button">
                    <FormattedMessage id="m.orders.tour.seeDetail" />
                  </Typography>
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export const TourBookingItemSkeleton: React.FunctionComponent = props => {
  return (
    <Wrapper style={{ marginBottom: '10px', display: 'flex' }}>
      <Skeleton variant="rect" style={{ height: '177px', width: '290px', margin: '0px' }} />
      <div style={{ padding: '16px 20px 12px 12px', flex: 1, display: 'flex', flexDirection: 'column' }}>
        <Skeleton style={{ height: 26 }} variant="text" width={250} />
        <Divider style={{ marginTop: '16px', marginBottom: '8px' }} />
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <Skeleton variant="text" width={150} />
            <Skeleton variant="text" width={200} />
            <Skeleton variant="text" width={220} />
            <Skeleton variant="text" width={130} />
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
            <div style={{ flex: 1, display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
              <Skeleton variant="text" width={150} />
              <Skeleton variant="text" width={100} />
            </div>
            <Skeleton style={{ borderRadius: 8 }} variant="rect" width={185} height={40} />
          </div>
        </div>
      </div>
    </Wrapper>
  );
};
function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}
export default connect(mapStateToProps)(TourBookingItem);
