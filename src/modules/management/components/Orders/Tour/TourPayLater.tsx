import { Typography } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../../colors';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NotFoundBox from '../../../../common/components/NotFoundBox';
import DiscountCodeBox from '../../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../../common/components/Payment/PaymentMethodsBox';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../../../common/constants';
import { BOOKING_ID_PARAM_NAME, TAB_PARAM_NAME } from '../../../constants';
import {
  fetchDataDetail,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setTourPayLaterMessage,
} from '../../../redux/tourOrderReducer';
import { computeTourHoldingBookingPayable } from '../../../utils';
import TourRePayBreadCrumbs from './TourRePayBreadCrumbs';

const mapStateToProps = (state: AppState) => {
  return {
    tourOrder: state.management.tourOrder,
    router: state.router,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  paying: boolean;
  loading: boolean;
  message: string;
}

const TourPayLater: FunctionComponent<Props> = props => {
  const { tourOrder, dispatch, intl, router } = props;
  const data = tourOrder.data;

  const paramsUrl = new URLSearchParams(router.location.search);
  const bookingId = parseInt(paramsUrl.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;
  const module = paramsUrl.get(TAB_PARAM_NAME) || '';

  React.useEffect(() => {
    dispatch(setPaymentMethods());
    dispatch(setSelectedPaymentMethod());
    dispatch(fetchDataDetail(bookingId, module));
  }, [bookingId, dispatch, module]);

  React.useEffect(() => {
    dispatch(setTourPayLaterMessage());
  }, [dispatch]);

  const payableNumbers = computeTourHoldingBookingPayable(tourOrder);

  const price = payableNumbers.finalPrice + payableNumbers.paymentFee;
  const finalPrice = price > 0 ? price : 0;
  const priceAfter = payableNumbers.finalPrice;

  if (!bookingId || !module) {
    return <NotFoundBox />;
  }
  return (
    <div>
      <Helmet>
        <title>
          {data ? data.tourName : ''}-
          {`${intl.formatMessage({
            id: 'pay',
          })}`}
        </title>
      </Helmet>
      {tourOrder.paymentMethods && tourOrder.selectedPaymentMethod && data ? (
        <div>
          <TourRePayBreadCrumbs title={data.tourName} />
          <Typography variant="h5" style={{ padding: '16px 0' }}>
            <FormattedMessage id="pay" />
          </Typography>
          <div>
            {finalPrice !== payableNumbers.originPrice && (
              <Line
                style={{
                  justifyContent: 'space-between',
                  padding: '5px',
                }}
              >
                <Typography variant="body2">
                  <FormattedMessage id="m.order.totalFee" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={payableNumbers.originPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {tourOrder.usePointPayment &&
              tourOrder.selectedPaymentMethod &&
              tourOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
              !!tourOrder.pointUsing && (
                <Line
                  style={{
                    justifyContent: 'space-between',
                    padding: '5px',
                  }}
                >
                  <Typography variant="body2">
                    <FormattedMessage id="booking.pointPayment" />
                  </Typography>

                  <Typography variant="body2" color="primary">
                    <FormattedNumber value={-payableNumbers.pointToAmount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
              )}
            {payableNumbers.discountAmount > 0 && (
              <Line
                style={{
                  justifyContent: 'space-between',
                  padding: '5px',
                }}
              >
                {tourOrder.promotion && (
                  <Typography variant="body2" color="primary">
                    <FormattedMessage id="booking.addPromotionCode" />
                    &nbsp;
                    {tourOrder.promotionCode}
                  </Typography>
                )}
                {data.promotionCode && (
                  <Typography variant="body2" color="primary">
                    <FormattedMessage id="booking.addPromotionCode" />
                    &nbsp;
                    {data.promotionCode}
                  </Typography>
                )}
                <Typography variant="body2" color="primary">
                  - <FormattedNumber value={payableNumbers.discountAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {payableNumbers.paymentFee !== 0 && (
              <Line
                style={{
                  justifyContent: 'space-between',
                  padding: '5px',
                }}
              >
                {payableNumbers.paymentFee > 0 ? (
                  <Typography variant="body2">
                    <FormattedMessage id="booking.paymentFixedFeeSide" />
                  </Typography>
                ) : (
                  <Typography variant="body2">
                    <FormattedMessage
                      id="booking.discountPaymentMethod"
                      values={{
                        methodName: tourOrder.selectedPaymentMethod
                          ? tourOrder.selectedPaymentMethod.name
                          : '',
                      }}
                    />
                  </Typography>
                )}
                <Typography
                  variant="body2"
                  color={payableNumbers.paymentFee > 0 ? 'secondary' : 'primary'}
                >
                  <FormattedNumber value={payableNumbers.paymentFee} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            <Line
              style={{
                justifyContent: 'space-between',
                padding: '5px',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="m.order.totalPayable" />
              </Typography>
              <Typography variant="subtitle2" color="secondary">
                <FormattedNumber value={finalPrice} /> <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </div>
          {!data.discount && (
            <div
              style={{
                padding: '16px 0',
              }}
            >
              <Typography variant="h6" style={{ paddingBottom: '12px' }}>
                <FormattedMessage id="booking.discountCode" />
              </Typography>
              <DiscountCodeBox moduleType="tourPayLater" />
            </div>
          )}
          <Typography variant="h6" style={{ padding: '16px 0' }}>
            <FormattedMessage id="booking.paymentMethod" />
          </Typography>
          <PaymentMethodsBox
            paymentMethods={tourOrder.paymentMethods}
            total={payableNumbers.finalPrice}
            setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={tourOrder.selectedPaymentMethod}
            priceAfter={priceAfter}
            promotionCode={tourOrder.promotionCode}
            moduleType="tourPayLater"
          />
          <div
            style={{
              padding: '16px 16px 32px 16px',
              color: BLUE,
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="booking.seePaymentDetail" />
            </Typography>
          </div>
        </div>
      ) : (
        <div style={{ margin: '42px' }}>
          <LoadingIcon />
        </div>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(TourPayLater));
