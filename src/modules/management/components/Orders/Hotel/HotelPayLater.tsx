import { Typography } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../../colors';
import { AppState } from '../../../../../redux/reducers';
import { Line } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NotFoundBox from '../../../../common/components/NotFoundBox';
import DiscountCodeBox from '../../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../../common/components/Payment/PaymentMethodsBox';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../../../common/constants';
import { BOOKING_ID_PARAM_NAME, TAB_PARAM_NAME } from '../../../constants';
import {
  fetchDataDetail,
  setPaymentMethods,
  setPayResponseMessage,
  setSelectedPaymentMethod,
} from '../../../redux/hotelOrderReducer';
import { computeHotelHoldingBookingPayable } from '../../../utils';
import HotelRePayBreadCrumbs from './HotelRePayBreadCrumbs';

const mapStateToProps = (state: AppState) => {
  return {
    hotelOrder: state.management.hotelOrder,
    router: state.router,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const HotelPaylater: FunctionComponent<Props> = props => {
  const { hotelOrder, dispatch, intl, router } = props;
  const data = hotelOrder.data;

  const paramsUrl = new URLSearchParams(router.location.search);
  const bookingId = parseInt(paramsUrl.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;
  const module = paramsUrl.get(TAB_PARAM_NAME) || '';

  React.useEffect(() => {
    dispatch(setPaymentMethods());
    dispatch(setSelectedPaymentMethod());
    dispatch(fetchDataDetail(bookingId, module));
    dispatch(setPayResponseMessage());
  }, [bookingId, dispatch, module]);

  const payableNumbers = computeHotelHoldingBookingPayable(hotelOrder);

  const price = payableNumbers.finalPrice + payableNumbers.paymentFee;
  const finalPrice = price > 0 ? price : 0;
  const priceAfter = payableNumbers.finalPrice;

  if (!bookingId || !module) {
    return <NotFoundBox />;
  }
  return (
    <div>
      <Helmet>
        <title>
          {data ? data.hotelName : ''}-
          {`${intl.formatMessage({
            id: 'pay',
          })}`}
        </title>
      </Helmet>
      {hotelOrder.paymentMethods && hotelOrder.selectedPaymentMethod && data ? (
        <div>
          <>
            <HotelRePayBreadCrumbs title={data.hotelName} />
            <Typography variant="h5" style={{ padding: '16px 0' }}>
              <FormattedMessage id="pay" />
            </Typography>
            <div>
              {finalPrice !== payableNumbers.originPrice && (
                <Line
                  style={{
                    justifyContent: 'space-between',
                    padding: '5px',
                  }}
                >
                  <Typography variant="body2">
                    <FormattedMessage id="m.order.totalFee" />
                  </Typography>
                  <div style={{ display: 'flex' }}>
                    <Typography variant="body2" color="secondary">
                      <FormattedNumber value={payableNumbers.originPrice} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </Line>
              )}
              {hotelOrder.usePointPayment &&
                hotelOrder.selectedPaymentMethod &&
                hotelOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
                !!hotelOrder.pointUsing && (
                  <Line
                    style={{
                      justifyContent: 'space-between',
                      padding: '5px',
                    }}
                  >
                    <Typography variant="body2">
                      <FormattedMessage id="booking.pointPayment" />
                    </Typography>

                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={-payableNumbers.pointToAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
              {payableNumbers.discountAmount > 0 && (
                <Line
                  style={{
                    justifyContent: 'space-between',
                    padding: '5px',
                  }}
                >
                  {hotelOrder.promotion && (
                    <Typography variant="body2" color="primary">
                      <FormattedMessage id="booking.addPromotionCode" />
                      &nbsp;
                      {hotelOrder.promotionCode}
                    </Typography>
                  )}
                  {data.promotionCode && (
                    <Typography variant="body2" color="primary">
                      <FormattedMessage id="booking.addPromotionCode" />
                      &nbsp;
                      {data.promotionCode}
                    </Typography>
                  )}
                  <div style={{ display: 'flex' }}>
                    <Typography variant="body2" color="primary">
                      - <FormattedNumber value={payableNumbers.discountAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </Line>
              )}
              {payableNumbers.paymentFee !== 0 && (
                <Line
                  style={{
                    justifyContent: 'space-between',
                    padding: '5px',
                  }}
                >
                  {payableNumbers.paymentFee > 0 ? (
                    <Typography variant="body2">
                      <FormattedMessage id="booking.paymentFixedFeeSide" />
                    </Typography>
                  ) : (
                    <Typography variant="body2">
                      <FormattedMessage
                        id="booking.discountPaymentMethod"
                        values={{
                          methodName: hotelOrder.selectedPaymentMethod
                            ? hotelOrder.selectedPaymentMethod.name
                            : '',
                        }}
                      />
                    </Typography>
                  )}
                  <div style={{ display: 'flex' }}>
                    <Typography
                      variant="body2"
                      color={payableNumbers.paymentFee > 0 ? 'secondary' : 'primary'}
                    >
                      <FormattedNumber value={payableNumbers.paymentFee} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </Line>
              )}
              <Line
                style={{
                  justifyContent: 'space-between',
                  padding: '5px',
                }}
              >
                <Typography variant="body2">
                  <FormattedMessage id="m.order.totalPayable" />
                </Typography>
                <Typography variant="subtitle2" color="secondary">
                  <FormattedNumber value={finalPrice} /> <FormattedMessage id="currency" />
                </Typography>
              </Line>
            </div>
          </>
          {!data.discount && (
            <div
              style={{
                padding: '16px 0',
              }}
            >
              <Typography variant="h6" style={{ paddingBottom: '12px' }}>
                <FormattedMessage id="booking.discountCode" />
              </Typography>
              <DiscountCodeBox moduleType="hotelPayLater" />
            </div>
          )}
          <Typography variant="h6" style={{ padding: '16px 0' }}>
            <FormattedMessage id="booking.paymentMethod" />
          </Typography>
          <PaymentMethodsBox
            paymentMethods={hotelOrder.paymentMethods}
            total={payableNumbers.finalPrice}
            setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={hotelOrder.selectedPaymentMethod}
            priceAfter={priceAfter}
            promotionCode={hotelOrder.promotionCode}
            moduleType="hotelPayLater"
          />
          <div
            style={{
              padding: '16px 16px 32px 16px',
              color: BLUE,
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="booking.seePaymentDetail" />
            </Typography>
          </div>
        </div>
      ) : (
        <div style={{ margin: '42px' }}>
          <LoadingIcon />
        </div>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(HotelPaylater));
