import { Typography, Button } from '@material-ui/core';
import { goBack, push, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import icDelete from '../../../../../svg/delete.svg';
import exportInvoice from '../../../../../svg/exportInvoice.svg';
import iconDelete from '../../../../../svg/ic_delete_ticket_alert.svg';
import resendEmail from '../../../../../svg/resendEmail.svg';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import MessageDialog from '../../../../common/components/MessageDialog';
import { fetchThunk } from '../../../../common/redux/thunks';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, HotelManagementBookingItemActionType } from '../../../constants';
import ResendEmailPopup from '../common/ResendEmailPopup';

interface HotelBookingActionProps extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  data: some;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDeactivate(id: number): void;
}
type DialogActionType = 'deleteOrder' | 'resendEmail' | undefined;

const HotelBookingAction: React.FunctionComponent<HotelBookingActionProps> = props => {
  const { data, dispatch, intl, router } = props;
  const [showResendEmailPopup, setShowResendEmailPopup] = React.useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = React.useState(false);
  const [message, setMessage] = React.useState<string | undefined>(undefined);
  const [dialogAction, setDialogAction] = React.useState<DialogActionType>(undefined);

  const deleteHotelBookingOrder = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(
        API_PATHS.deactivate,
        'post',
        true,
        JSON.stringify({
          module: 'hotel',
          bookingId: data.id,
        }),
      ),
    );
    if (json.code === 200) {
      setMessage(intl.formatMessage({ id: 'm.order.deleteOrderSuccess' }));
      setDialogAction('deleteOrder');
      setShowConfirmDialog(false);
    } else {
      setMessage(json.message);
    }
  }, [dispatch, data.id, intl]);

  const onCloseMessageDialog = React.useCallback(async () => {
    setMessage('');
    if (dialogAction === 'deleteOrder') {
      const { router, dispatch } = props;
      const backableToList = router.location.state && router.location.state.backableToList;

      if (backableToList) {
        dispatch(goBack());
        return;
      }

      props.onDeactivate(data ? data.id : 0);
      const params = new URLSearchParams(router.location.search);
      params.delete(BOOKING_ID_PARAM_NAME);
      params.delete(ACTION_PARAM_NAME);
      dispatch(replace({ search: params.toString() }));
    }
  }, [dialogAction, data, props]);
  const changeExportInvoice = React.useCallback(
    (id: number) => {
      const params = new URLSearchParams(router.location.search);
      params.set(BOOKING_ID_PARAM_NAME, `${id}`);
      params.set(ACTION_PARAM_NAME, 'exportInvoice' as HotelManagementBookingItemActionType);
      return params.toString();
    },
    [router],
  );

  return (
    <div style={{ display: 'flex', marginTop: '20px' }}>
      {data.paymentStatus === 'success' && (
        <div style={{ display: 'flex' }}>
          <Button
            variant="text"
            style={{
              borderRadius: '4px',
              marginTop: '20px',
              marginRight: '24px',
            }}
            onClick={() => {
              data.vatInvoiceInfo
                ? setMessage(intl.formatMessage({ id: 'm.order.exportInvoiceInProgress' }))
                : dispatch(
                    push({
                      pathname: ROUTES.management,
                      search: `?${changeExportInvoice(data.id)}`,
                      state: { ...router.location.state, backableToDetail: true },
                    }),
                  );
            }}
            disabled={!!data.invoiceDescription}
          >
            <div style={{ display: 'flex', padding: '3px 12px' }}>
              <img src={exportInvoice} alt="" style={{ marginRight: '10px' }} />
              <Typography variant="body2">
                <FormattedMessage id="m.exportInvoice" />
              </Typography>
            </div>
          </Button>

          {data.bookerContact && (
            <Button
              variant="text"
              style={{ borderRadius: '4px', marginTop: '20px', marginRight: '24px' }}
              onClick={() => setShowResendEmailPopup(true)}
            >
              {' '}
              <div style={{ display: 'flex', padding: '3px 12px' }}>
                <img src={resendEmail} alt="" style={{ marginRight: '10px' }} />
                <Typography variant="body2">
                  <FormattedMessage id="m.resendEmail" />
                </Typography>
              </div>
            </Button>
          )}
        </div>
      )}
      <Button
        variant="text"
        style={{ borderRadius: '4px', marginTop: '20px', marginRight: '24px' }}
        onClick={() => setShowConfirmDialog(true)}
      >
        <div style={{ display: 'flex', padding: '3px 12px' }}>
          <img src={icDelete} alt="" style={{ marginRight: '10px' }} />
          <Typography variant="body2">
            <FormattedMessage id="m.deleteOrder" />
          </Typography>
        </div>
      </Button>
      {data.bookerContact && (
        <ResendEmailPopup
          show={showResendEmailPopup}
          data={data}
          dispatch={dispatch}
          onClose={() => setShowResendEmailPopup(false)}
          onSuccess={() => {
            setShowResendEmailPopup(false);
            setDialogAction('resendEmail');
            setMessage(intl.formatMessage({ id: 'm.order.resendEmailSuccess' }));
          }}
          onError={messageError => {
            setShowResendEmailPopup(false);
            setDialogAction('resendEmail');
            setMessage(messageError);
          }}
          module="hotel"
        />
      )}
      <ConfirmDialog
        message={
          <div style={{ textAlign: 'center' }}>
            <img src={iconDelete} alt="" />
            <Typography variant="body1">
              <FormattedMessage id="m.order.areYouSureWantDeleteThisOrder" />
            </Typography>
          </div>
        }
        show={showConfirmDialog}
        cancelMessageId="ignore"
        confirmMessageId="delete"
        onCancel={() => setShowConfirmDialog(false)}
        onAccept={deleteHotelBookingOrder}
      />
      <MessageDialog
        show={!!message}
        onClose={onCloseMessageDialog}
        message={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              padding: '18px',
              textAlign: 'center',
            }}
          >
            <Typography variant="body1">{message}</Typography>
          </div>
        }
      />
    </div>
  );
};
const mapState2Props = (state: AppState) => {
  return {
    router: state.router,
  };
};

export default connect(mapState2Props)(injectIntl(HotelBookingAction));
