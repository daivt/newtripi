import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME } from '../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const HotelDetailBreadcrumbs: React.FunctionComponent<Props> = props => {
  const backToList = () => {
    const { state, dispatch, router } = props;
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(go(-1));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  };

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
        <Typography
          color="textPrimary"
          variant="body2"
          style={{ cursor: 'pointer' }}
          onClick={() => backToList()}
        >
          <FormattedMessage id="hotel" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.orderDetail" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(HotelDetailBreadcrumbs);
