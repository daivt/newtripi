import { Button, Typography } from '@material-ui/core';
import IconLocation from '@material-ui/icons/LocationOnOutlined';
import Rating from '@material-ui/lab/Rating';
import moment from 'moment';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { BLUE, DARK_GREY, GREY, RED } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NotFoundBox from '../../../../common/components/NotFoundBox';
import { fetchThunk } from '../../../../common/redux/thunks';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  getStatusColor,
  HotelManagementBookingItemActionType,
} from '../../../constants';
import HotelBookingAction from './HotelBookingAction';
import OrderRequestList from '../../common/OrderRequestList';

const DetailTab = styled.div`
  display: flex;
  flex-direction: column;
`;
const Line = styled.div`
  min-height: 36px;
  display: flex;
  align-items: center;
  width: 100%;
`;
function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
    state: state.router.location.state,
  };
}

interface IOrdersProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onDeactivate(id: number): void;
  noAction?: boolean;
  breadcrumb: React.ReactNode;
}

const HotelDetailTab: React.FunctionComponent<IOrdersProps> = props => {
  const { search, state, dispatch, noAction, breadcrumb } = props;
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [expired, setExpired] = React.useState(false);
  const [isNotFound, setIsNotFound] = React.useState(false);

  React.useEffect(() => {
    const params = new URLSearchParams(search);
    const hotelId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;
    const fetch = async () => {
      setData(undefined);
      const json = await dispatch(fetchThunk(`${API_PATHS.getHotelBookingDetail}?id=${hotelId}`, 'get', true));
      if (json.code === 200) {
        setData(json.data);
        if (json.data.expiredTime) {
          setExpired(moment(json.data.expiredTime).diff(moment()) < 0);
        }
      } else if (json.code === 400) {
        setIsNotFound(true);
      }
    };
    fetch();
  }, [search, dispatch]);
  const getRePayLink = React.useMemo((): string => {
    const params = new URLSearchParams(search);
    params.set(ACTION_PARAM_NAME, 'payLater' as HotelManagementBookingItemActionType);
    return params.toString();
  }, [search]);

  return (
    <DetailTab>
      {data && !isNotFound ? (
        <div>
          <Helmet>
            <title>{data.hotelName}</title>
          </Helmet>
          {breadcrumb}

          <Typography variant="h5" style={{ paddingTop: '16px' }}>
            <FormattedMessage id="m.orderDetail" />
          </Typography>
          <div style={{ paddingTop: '16px', display: 'flex', alignItems: 'center' }}>
            <Typography variant="button">{data.hotelName}</Typography>
            <Rating style={{ paddingLeft: '8px' }} value={data.hotelStar} readOnly size="small" />
          </div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              margin: '16px 0px',
            }}
          >
            <IconLocation style={{ color: DARK_GREY }} />
            <Typography variant="body2" style={{ paddingLeft: '6px', color: DARK_GREY }}>
              {data.hotelAddress}
            </Typography>
          </div>
          <div
            style={{
              padding: '20px',
              background: 'white',
              border: `1px solid ${GREY}`,
              borderRadius: '4px',
            }}
          >
            <div style={{ display: 'flex' }}>
              <div style={{ flex: 1 }}>
                <Typography variant="subtitle2">
                  <FormattedMessage id="m.orderInfomations" />
                </Typography>
                <Line style={{ marginTop: '10px' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.status" />
                    :&nbsp;
                  </Typography>
                  <Typography
                    variant="body2"
                    style={{
                      color: getStatusColor(data.paymentStatus),
                    }}
                  >
                    <FormattedMessage id={data.paymentStatus} />
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.bookingDate" />
                    :&nbsp;
                    {data.bookingDate.replace(/-/g, '/')}
                  </Typography>
                </Line>{' '}
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.orderCode" />
                    :&nbsp;
                    {data.orderCode}
                  </Typography>
                </Line>
                <Line style={{ paddingRight: '24px' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.bookingRoomCode" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: BLUE, wordBreak: 'break-word' }}>
                    {data.partnerBookingCode && data.paymentStatus !== 'fail' && data.paymentStatus !== 'waiting' ? (
                      data.partnerBookingCode
                    ) : (
                      <FormattedMessage id="m.noBookingCode" />
                    )}
                  </Typography>
                </Line>
                <Typography variant="subtitle2" style={{ margin: '16px 0px 12px' }}>
                  <FormattedMessage id="m.customerInfomations" />
                </Typography>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.fullName" />
                    :&nbsp;
                    {data.bookerContact && data.bookerContact.lastName ? (
                      `${data.bookerContact.lastName} ${data.bookerContact.firstName}`
                    ) : data.bookerContact.firstName ? (
                      `${data.bookerContact.firstName}`
                    ) : (
                      <FormattedMessage id="m.order.noContact" />
                    )}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="telephone" />
                    :&nbsp;
                    {data.bookerContact && data.bookerContact.phone}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.email" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: BLUE }}>
                    {data.bookerContact && data.bookerContact.email}
                  </Typography>
                </Line>
                <Typography variant="subtitle2" style={{ margin: '16px 0px 12px 0px' }}>
                  <FormattedMessage id="m.hotelDetail" />
                </Typography>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.roomType" />
                    :&nbsp;
                    {data.roomTitle}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.customerCountInfo" />
                    :&nbsp;
                    {data.numAdults}
                    &nbsp;
                    <FormattedMessage id="m.adult" />, &nbsp;
                    {data.numChildren}
                    &nbsp;
                    <FormattedMessage id="m.children" />
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.roomCount" />
                    :&nbsp;
                    {data.numRooms}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.roomCheckinDate" />
                    :&nbsp;
                    {data.checkinDate.replace(/-/g, '/')}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="m.roomCheckoutDate" />
                    :&nbsp;
                    {data.checkoutDate.replace(/-/g, '/')}
                  </Typography>
                </Line>
              </div>
              <div style={{ flex: 1 }}>
                <Typography variant="subtitle2">
                  <FormattedMessage id="m.cancelation" />
                </Typography>
                <Line style={{ marginTop: '10px' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.cancelationPolicy" />
                    :&nbsp;
                    {data.freeCancellation ? (
                      <FormattedMessage id="m.freeCancelation" />
                    ) : data.cancellationPolicies.length > 0 ? (
                      <FormattedMessage id="m.followHotelPolicy" />
                    ) : (
                      <FormattedMessage id="m.noCancelationPolicy" />
                    )}
                  </Typography>
                </Line>

                {data.cancellationPolicies.length > 0 &&
                  data.cancellationPolicies.map((item: any) => (
                    <Line key={item}>
                      <Typography key={item} variant="body2" style={{ color: RED, lineHeight: '36px' }}>
                        {item}
                      </Typography>{' '}
                    </Line>
                  ))}
                <Line>
                  <Typography variant="subtitle2" style={{ margin: '16px 0px 12px' }}>
                    <FormattedMessage id="m.paymentDetail" />
                  </Typography>
                </Line>

                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.roomPrice" />
                  </Typography>
                  <Typography variant="body2">
                    <FormattedNumber value={data.basePrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
                {data.paymentMethodFee !== 0 && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage
                        id={
                          data.paymentMethodFee > 0 ? 'booking.paymentFixedFeeSide' : 'booking.paymentFixedDiscountSide'
                        }
                      />
                    </Typography>
                    <Typography variant="body2" color={data.paymentMethodFee > 0 ? 'secondary' : 'primary'}>
                      <FormattedNumber value={data.paymentMethodFee} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
                {data.discount > 0 && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage id="m.order.discountCode" />
                    </Typography>
                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={-data.discount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
                {data.pointAmount > 0 && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage id="booking.pointPayment" />
                    </Typography>
                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={data.paidPoint} />
                      &nbsp;
                      <FormattedMessage id="point" />
                      &nbsp;/&nbsp;
                      <FormattedNumber value={-data.pointAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
                {!!data.refundAmount && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage id="refunded" />
                    </Typography>
                    <Typography variant="body2" color="secondary">
                      <FormattedNumber value={data.refundAmount} />
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.totalPrice" />
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <FormattedNumber value={data.finalPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
                {data.bonusPoint > 0 && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage id="m.order.pointBonus" />
                    </Typography>
                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={data.bonusPoint} />
                      &nbsp;
                      <FormattedMessage id="point" />
                    </Typography>
                  </Line>
                )}
                {data.transferMoneyMethod && (
                  <Line style={{ justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                      <FormattedMessage id="m.order.transferMoneyMethod" />
                    </Typography>
                    <Typography variant="body2" color="secondary">
                      {data.transferMoneyMethod}
                    </Typography>
                  </Line>
                )}
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.paymentMethod" />
                  </Typography>
                  <Typography variant="body2">{data.paymentMethod}</Typography>
                </Line>

                {data.bookingStatus === 'holding' && (
                  <Link
                    to={{
                      pathname: ROUTES.management,
                      search: getRePayLink,
                      state: { ...state, backableToDetail: true },
                    }}
                  >
                    <Button color="secondary" variant="contained" disabled={expired || data.paymentStatus === 'fail'}>
                      <FormattedMessage id={!expired ? 'm.pay' : 'm.order.paymentExpired'} />
                    </Button>
                  </Link>
                )}
              </div>
            </div>
            <OrderRequestList bookingId={data.id} bookModule="hotel" />
            {data && data.invoiceDescription && data.invoiceInfo && (
              <div style={{ color: RED, marginTop: 12 }}>
                <p>
                  <strong>{data.invoiceInfo}</strong>
                </p>
                <FormattedHTMLMessage id="m.invoiceDescription" />
              </div>
            )}
          </div>
          {!noAction && <HotelBookingAction onDeactivate={props.onDeactivate} data={data} />}
        </div>
      ) : (
        <div>
          {isNotFound ? (
            <NotFoundBox />
          ) : (
            <div style={{ margin: '42px' }}>
              <LoadingIcon />
            </div>
          )}
        </div>
      )}
    </DetailTab>
  );
};

export default connect(mapState2Props)(HotelDetailTab);
