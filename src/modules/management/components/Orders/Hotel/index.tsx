import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { BLUE } from '../../../../../colors';
import { PAGE_SIZE, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunks';
import { ACTION_PARAM_NAME, HotelManagementBookingItemActionType } from '../../../constants';
import ExportInvoice from '../common/ExportInvoice';
import OrderFilters, { OrderFiltersParams } from '../OrderFilters';
import HotelBookingItem, { HotelBookingItemSkeleton } from './HotelBookingItem';
import HotelDetailBreadcrumbs from './HotelDetailBreadcrumbs';
import HotelDetailTab from './HotelDetailTab';
import NoDataResult from '../../../../common/components/NoDataResult';
import HotelPayLater from './HotelPayLater';
import ExportInvoiceRequestDetail from '../../../components/common/ExportInvoiceRequestDetail';
import HotelRequestDetailBreadscrumbs from './HotelRequestDetailBreadscrumbs';
interface IHotelProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

const Hotel: React.FunctionComponent<IHotelProps> = props => {
  const { dispatch } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [filters, setFilters] = React.useState<OrderFiltersParams>({});

  const [deactivateId, setDeactivateId] = React.useState(0);

  const params = new URLSearchParams(props.search);
  const action = params.get(ACTION_PARAM_NAME) as HotelManagementBookingItemActionType | null;

  const fetchMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getHotelBookings}`,
        'post',
        true,
        JSON.stringify({ filters, paging: { page: page + 1, size: PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setData(b => b && b.concat(json.data));
      setPage(page + 1);
    }
    setLoading(false);
  }, [page, dispatch, filters]);

  const filtersStr = JSON.stringify(filters);
  React.useEffect(() => {
    const fetch = async () => {
      setData([]);
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getHotelBookings}`,
          'post',
          true,
          JSON.stringify({
            filters: JSON.parse(filtersStr),
            paging: { page: 1, size: PAGE_SIZE },
          }),
        ),
      );
      if (json.code === 200) {
        setPage(1);
        setData(json.data);
        setTotal(json.total);
      }
      setLoading(false);
    };
    fetch();
  }, [filtersStr, dispatch, deactivateId]);

  return (
    <div>
      <div style={{ display: action ? 'none' : undefined }}>
        <OrderFilters
          loading={loading}
          onChange={(filters: OrderFiltersParams) => {
            setFilters(filters);
          }}
        />
        <div>
          {data ? (
            <>
              {data.map(one => (
                <HotelBookingItem key={one.id} data={one} />
              ))}
              {!loading && total - PAGE_SIZE * page > 0 && (
                <div style={{ textAlign: 'center', marginTop: '23px' }}>
                  <Button onClick={fetchMore}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage
                        id="result.displayMore"
                        values={{ num: total - PAGE_SIZE * page }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
              {loading && (
                <>
                  <HotelBookingItemSkeleton />
                  <HotelBookingItemSkeleton />
                  <HotelBookingItemSkeleton />
                </>
              )}
              {!data.length && !loading && (
                <NoDataResult id="m.noData" style={{ marginTop: '48px' }} />
              )}
            </>
          ) : (
            <>
              <HotelBookingItemSkeleton />
              <HotelBookingItemSkeleton />
              <HotelBookingItemSkeleton />
              <HotelBookingItemSkeleton />
              <HotelBookingItemSkeleton />
            </>
          )}
        </div>
      </div>
      {action === 'detail' && (
        <HotelDetailTab
          onDeactivate={id => setDeactivateId(id)}
          breadcrumb={<HotelDetailBreadcrumbs />}
        />
      )}
      {action === 'payLater' && <HotelPayLater />}
      {action === 'exportInvoice' && <ExportInvoice />}
      {action === 'orderRequestDetail' && (
        <ExportInvoiceRequestDetail breadcrumb={<HotelRequestDetailBreadscrumbs />} />
      )}
    </div>
  );
};

export default connect(mapState2Props)(Hotel);
