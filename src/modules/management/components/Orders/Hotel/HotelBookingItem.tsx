import { Button, Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import IconLocation from '@material-ui/icons/LocationOnOutlined';
import Rating from '@material-ui/lab/Rating';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREEN, GREY, RED } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import Link from '../../../../common/components/Link';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, getStatusColor } from '../../../constants';

const Wrapper = styled.div`
  background: #ffffff;
  border: 0.5px solid ${GREY};
  box-sizing: border-box;
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14);
`;

interface IHotelOrderItemProps extends ReturnType<typeof mapState2Props> {
  data: some;
}
function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

const seeDetail = (id: number, search: string) => {
  const params = new URLSearchParams(search);
  params.set(BOOKING_ID_PARAM_NAME, `${id}`);
  params.set(ACTION_PARAM_NAME, 'detail');
  return params.toString();
};

const HotelBookingItem: React.FunctionComponent<IHotelOrderItemProps> = props => {
  const { data, search } = props;
  return (
    <Wrapper style={{ marginBottom: '10px' }}>
      {data.paymentStatus === 'holding' && data.expiredTime && (
        <div>
          {moment(data.expiredTime).isSameOrAfter(moment()) ? (
            <div style={{ padding: '4px 12px', backgroundColor: GREEN }}>
              <Typography style={{ color: 'white' }} variant="body2">
                <FormattedMessage id="m.orders.tour.countTime" />
                &nbsp;
                {moment(data.expiredTime).format('LT L')}
              </Typography>
            </div>
          ) : (
            <div style={{ padding: '4px 12px', backgroundColor: GREEN }}>
              <Typography variant="body2" style={{ color: 'white' }} id="test123">
                <FormattedMessage id="m.expired" />
              </Typography>
            </div>
          )}
        </div>
      )}
      <div style={{ display: 'flex' }}>
        <div>
          <img
            src={data.hotelLogo}
            alt=""
            style={{
              height: '100%',
              width: '120px',
              objectFit: 'cover',
            }}
          />
        </div>
        <div style={{ display: 'flex' }}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              padding: '8px',
              flex: 1,
              width: '300px',
            }}
          >
            <Typography variant="subtitle1">{data.hotelName}</Typography>
            <Rating value={data.hotelStar} readOnly size="small" />
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                paddingTop: '4px',
              }}
            >
              <IconLocation style={{ color: DARK_GREY }} />
              <Typography variant="body2" style={{ paddingLeft: '6px', color: DARK_GREY }}>
                {data.hotelAddress}
              </Typography>
            </div>
          </div>
        </div>
        <div style={{ width: '100%', padding: '10px 20px 10px 0' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div>
              <Typography variant="body2">
                <FormattedMessage id="m.orderCode" />
                :&nbsp;
                {data.orderCode}
              </Typography>
              <div style={{ display: 'flex' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.bookingRoomCode" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: BLUE, width: '160px' }} noWrap>
                  {data.partnerBookingCode && data.paymentStatus !== 'fail' && data.paymentStatus !== 'waiting' ? (
                    data.partnerBookingCode
                  ) : (
                    <FormattedMessage id="m.noBookingCode" />
                  )}
                </Typography>
              </div>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography
                variant="body1"
                style={{
                  color: `${getStatusColor(data.paymentStatus)}`,
                }}
              >
                <FormattedMessage id={data.paymentStatus} />
              </Typography>
            </div>
          </div>
          <Divider />
          <div style={{ display: 'flex', justifyContent: 'space-between', paddingTop: '10px' }}>
            <div>
              <Typography variant="body2" style={{ paddingTop: '10px' }}>
                <FormattedMessage id="m.customer" />
                :&nbsp;
                {data.bookerContact && data.bookerContact.lastName ? (
                  `${data.bookerContact.lastName} ${data.bookerContact.firstName}`
                ) : data.bookerContact.firstName ? (
                  `${data.bookerContact.firstName}`
                ) : (
                  <FormattedMessage id="m.order.noContact" />
                )}
              </Typography>

              <Typography variant="body2">
                <FormattedMessage id="m.bookingDate" />
                :&nbsp;
                {data.bookingDate.replace(/-/g, '/')}
              </Typography>
              <Typography variant="body2">
                <FormattedMessage id="m.schedule" />
                {`: ${data.checkinDate.replace(/-/g, '/')} - ${data.checkoutDate.replace(/-/g, '/')}`}
              </Typography>
              <Typography variant="body2">
                <FormattedMessage id="m.customerCountInfo" />
                :&nbsp;
                {data.numAdults}
                &nbsp;
                <FormattedMessage id="m.adult" />, &nbsp;
                {data.numChildren}
                &nbsp;
                <FormattedMessage id="m.children" />
              </Typography>
              <Typography variant="body2">
                <FormattedMessage id="booking.totalRoom" />
                :&nbsp;
                {data.numRooms}
                &nbsp;
                <FormattedMessage id="booking.Room" />
              </Typography>
            </div>

            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.finalPrice" />
              </Typography>
              <Typography variant="body2" style={{ color: RED }}>
                {/* <FormattedNumber value={data.finalPrice} /> */}
                {data.finalPriceFormatted}
              </Typography>
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${seeDetail(data.id, search)}`,
                  state: { backableToList: true },
                }}
              >
                <Button
                  style={{ padding: '1px 0', width: '160px', height: '30px', boxShadow: 'none' }}
                  color="secondary"
                  variant="contained"
                  size="small"
                >
                  <FormattedMessage id="m.viewDetail" />
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export const HotelBookingItemSkeleton: React.FunctionComponent = props => {
  return (
    <Wrapper style={{ marginBottom: '10px' }}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Skeleton width="100px" height="170px" variant="rect" />
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            padding: '12px',
          }}
        >
          <Skeleton width="50%" />
          <Skeleton width="50%" />
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            padding: '12px',
          }}
        >
          <Skeleton width="50%" />
          <Skeleton width="50%" />
        </div>
      </div>
    </Wrapper>
  );
};

export default connect(mapState2Props)(HotelBookingItem);
