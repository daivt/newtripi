import { Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Dispatch } from 'redux';
import { ROUTES, ServiceType } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CustomTab, CustomTabs } from '../../../common/components/elements';
import { ACTION_PARAM_NAME, TAB_PARAM_NAME } from '../../constants';
import Flight from './Flight';
import Hotel from './Hotel';
import Train from './Train';

import Tour from './Tour';

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

const LIST: { id: ServiceType; content: React.ReactNode }[] = [
  { id: 'flight', content: <Flight /> },
  { id: 'hotel', content: <Hotel /> },
  { id: 'tour', content: <Tour /> },
  { id: 'train', content: <Train /> },
];

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: Dispatch;
}

const Orders: React.FunctionComponent<Props> = props => {
  const { search, intl } = props;

  const params = new URLSearchParams(search);
  const currentTab = params.get(TAB_PARAM_NAME);
  if (!currentTab) {
    params.set(TAB_PARAM_NAME, 'flight');
    return (
      <Redirect
        to={{
          pathname: ROUTES.management,
          search: params.toString(),
        }}
      />
    );
  }
  let tab: false | number = false;
  if (currentTab) {
    tab = LIST.findIndex(obj => obj.id === currentTab);
    if (tab === -1) {
      tab = false;
    }
  }
  const action = params.get(ACTION_PARAM_NAME);

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: currentTab })}</title>
      </Helmet>
      <div style={{ padding: '30px 30px' }}>
        <div>
          {!action && ( // only display tab for listing action which is an empty string action
            <>
              <div>
                <Typography variant="h5">
                  <FormattedMessage id="m.ordersManagement" />
                </Typography>
              </div>
              <CustomTabs
                value={tab}
                onChange={(e, val) => {
                  const params = new URLSearchParams(props.search);
                  params.set('tab', LIST[val].id);
                  props.dispatch(replace(`${ROUTES.management}?${params.toString()}`));
                }}
              >
                {LIST.map(v => (
                  <CustomTab
                    key={v.id}
                    label={
                      <Typography variant="body2">
                        <FormattedMessage id={v.id} />
                      </Typography>
                    }
                  />
                ))}
              </CustomTabs>
            </>
          )}
          {tab !== false && LIST[tab].content}
        </div>
      </div>
    </>
  );
};

export default connect(mapState2Props)(injectIntl(Orders));
