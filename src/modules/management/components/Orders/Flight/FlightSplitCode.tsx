import { Checkbox, FormControlLabel, Typography } from '@material-ui/core';
import { remove } from 'lodash';
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { DARK_GREY, GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingButton from '../../../../common/components/LoadingButton/index';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import MessageDialog from '../../../../common/components/MessageDialog';
import { NewTabLink } from '../../../../common/components/NewTabLink';
import { fetchThunk } from '../../../../common/redux/thunks';
import { BOOKING_ID_PARAM_NAME } from '../../../constants';
import FlightSplitCodeBreadCrumbs from './FlightSplitCodeBreadCrumbs';
import FlightSplitCodeSuccess from './FlightSplitCodeSuccess';

function mapState2Props(state: AppState) {
  return {
    router: state.router,
    splitCode: state.management.flightOrder.splitCodeData,
  };
}

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  reloadData: () => void;
}

interface State {
  passengerIds: number[];
  loading?: boolean;
  splitCodeSuccess?: boolean;
  message: string;
  data?: some;
}

class FlightSplitCode extends React.PureComponent<Props, State> {
  state: State = {
    passengerIds: [],
    message: '',
  };

  async onSplitCode() {
    const { dispatch, router } = this.props;
    const { passengerIds } = this.state;

    const params = new URLSearchParams(router.location.search);
    const bookingId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;
    this.setState({ loading: true });
    const json = await dispatch(
      fetchThunk(
        API_PATHS.divideBooking,
        'post',
        true,
        JSON.stringify({ passengerIds, bookingId }),
      ),
    );

    if (json.code === 200) {
      this.setState({ loading: false, splitCodeSuccess: true, data: json.data.booking });
    } else {
      this.setState({ loading: false, message: json.message });
    }
  }

  onSetPassenger(event: React.ChangeEvent<HTMLInputElement>, passenger: some) {
    const passengerIds = [...this.state.passengerIds];

    if (event.target.checked) {
      passengerIds.push(passenger.id);
    } else {
      remove(passengerIds, id => id === passenger.id);
    }

    this.setState({ passengerIds });
  }

  render() {
    const { intl, splitCode, reloadData } = this.props;
    const { passengerIds, loading, splitCodeSuccess, message, data } = this.state;

    if (!splitCode) {
      return (
        <div style={{ margin: '42px' }}>
          <LoadingIcon />
        </div>
      );
    }

    if (splitCodeSuccess && data) {
      return <FlightSplitCodeSuccess reloadData={reloadData} data={data} />;
    }

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'flight',
            })} - ${intl.formatMessage({ id: 'm.order.splitCode' })}`}
          </title>
        </Helmet>

        <FlightSplitCodeBreadCrumbs />

        <Typography variant="h5" style={{ padding: '16px 0' }}>
          <FormattedMessage id="m.order.splitCode" />
        </Typography>

        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="h6" style={{ padding: '16px 0' }}>
              <FormattedMessage id="m.order.choosePassenger" />
            </Typography>

            {splitCode.passengers.map((passenger: some) => (
              <div key={passenger.id}>
                <FormControlLabel
                  style={{
                    padding: '8px 0',
                    width: '100%',
                    outline: 'none',
                    margin: '0',
                    color: DARK_GREY,
                    borderBottom: passenger.infants.length ? undefined : `0.5px solid ${GREY}`,
                  }}
                  label={
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        minWidth: '375px',
                      }}
                    >
                      <div>
                        <Typography color="textPrimary">{passenger.fullName}</Typography>
                        <Typography variant="caption">{passenger.dob}</Typography>
                      </div>
                      <Typography color="textSecondary">
                        <FormattedMessage id={passenger.ageCategory} />
                      </Typography>
                    </div>
                  }
                  labelPlacement="end"
                  control={
                    <Checkbox
                      checked={passengerIds.indexOf(passenger.id) !== -1}
                      size="medium"
                      onChange={event => this.onSetPassenger(event, passenger)}
                      color="secondary"
                    />
                  }
                />

                {!!passenger.infants.length && (
                  <div style={{ borderBottom: `0.5px solid ${GREY}` }}>
                    {passenger.infants.map((infant: some) => (
                      <div key={infant.id}>
                        <div
                          style={{
                            marginLeft: '45px',
                            borderBottom: `0.5px solid ${GREY}`,
                            height: '1px',
                          }}
                        />
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            padding: '8px 0px 8px 45px',
                          }}
                        >
                          <div>
                            <Typography color="textPrimary">{infant.fullName}</Typography>
                            <Typography variant="caption">{infant.dob}</Typography>
                          </div>
                          <Typography color="textSecondary">
                            <FormattedMessage
                              id={infant.ageCategory === 'infant' ? 'baby' : infant.ageCategory}
                            />
                          </Typography>
                        </div>
                      </div>
                    ))}
                  </div>
                )}
              </div>
            ))}
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '95px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Typography variant="h6" style={{ padding: '16px 0' }}>
                <FormattedMessage id="m.order.splitCodePolicy" />
              </Typography>

              <NewTabLink href={splitCode.link}>
                <Typography variant="body2" style={{ padding: '16px 0' }}>
                  <FormattedMessage id="m.viewDetail" />
                </Typography>
              </NewTabLink>
            </div>

            <Typography variant="body2">
              <span dangerouslySetInnerHTML={{ __html: splitCode.policy }}></span>
            </Typography>

            <Typography variant="h6" style={{ padding: '16px 0' }}>
              <FormattedMessage id="booking.pay" />
            </Typography>

            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingBottom: '8px',
                borderBottom: `0.5px solid ${GREY}`,
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="m.order.splitCodeFee" />
              </Typography>

              <Typography color="secondary" variant="body2">
                <FormattedMessage id="m.order.free" />
              </Typography>
            </div>

            <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '20px' }}>
              <LoadingButton
                disabled={loading}
                variant="contained"
                size="large"
                style={{ width: '250px', position: 'relative' }}
                color="secondary"
                loading={loading}
                onClick={() => this.onSplitCode()}
              >
                <Typography variant="button">
                  <FormattedMessage id="m.order.splitPassenger" />
                </Typography>
              </LoadingButton>
            </div>
          </div>
        </div>

        <MessageDialog
          show={!!message}
          onClose={() => this.setState({ message: '' })}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '18px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{message}</Typography>
            </div>
          }
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(injectIntl(FlightSplitCode));
