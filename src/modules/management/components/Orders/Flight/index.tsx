import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { BLUE } from '../../../../../colors';
import { PAGE_SIZE, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import NoDataResult from '../../../../common/components/NoDataResult';
import { fetchThunk } from '../../../../common/redux/thunks';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../../common/utils';
import { ACTION_PARAM_NAME, FlightManagementBookingItemActionType } from '../../../constants';
import ExportInvoice from '../common/ExportInvoice';
import OrderFilters, { OrderFiltersParams } from '../OrderFilters';
import FlightBaggageBox from './Baggage/FlightBaggageBox';
import FlightBaggagePay from './Baggage/FlightBaggagePay';
import FlightExportInvoiceBreadcrumbs from './Baggage/FlightExportInvoiceBreadcrumbs';
import ChangeItineraryBox from './ChangeItinerary';
import ChangeItineraryPay from './ChangeItinerary/ChangeItineraryPay';
import FlightBookingDetail from './FlightBookingDetail';
import FlightBookingDetailBreadCrumbs from './FlightBookingDetailBreadCrumbs';
import FlightBookingHoldingPay from './FlightBookingHoldingPay';
import FlightBookingItem, { FlightBookingItemSkeleton } from './FlightBookingItem';
import FlightBookingRefund from './FlightBookingRefund';
import FlightSplitCode from './FlightSplitCode';
import ExportInvoiceRequestDetail from '../../common/ExportInvoiceRequestDetail';
import FlightRequestDetailBreadscrumbs from './FlightRequestDetailBreadscrumbs';

function mapStateToProps(state: AppState) {
  return {
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    lang: state.intl.locale.substring(0, 2),
    router: state.router,
  };
}

interface IFlightProps extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const Flight: React.FunctionComponent<IFlightProps> = props => {
  const { dispatch, getAirlineInfo, getTicketClass, lang } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [filters, setFilters] = React.useState<OrderFiltersParams>({});
  const [deactivateId, setDeactivateId] = React.useState(0);
  const [cancelHoldingId, setCancelHoldingId] = React.useState(0);
  const [reloadDataTickTock, setReloadDataTickTock] = React.useState(false);

  const { search } = props.router.location;
  const params = new URLSearchParams(search);

  const action = params.get(ACTION_PARAM_NAME) as FlightManagementBookingItemActionType | null;

  const fetchMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getFlightBookings}`,
        'post',
        true,
        JSON.stringify({ filters, paging: { page: page + 1, size: PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setPage(p => p + 1);
      setData(one => one && one.concat(json.data));
    }
    setLoading(false);
  }, [filters, page, dispatch]);

  const filtersStr = JSON.stringify(filters);
  React.useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      setData([]);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getFlightBookings}`,
          'post',
          true,
          JSON.stringify({ filters: JSON.parse(filtersStr), paging: { page: 1, size: PAGE_SIZE } }),
        ),
      );
      if (json.code === 200) {
        setPage(1);
        setData(json.data);
        setTotal(json.total);
      }
      setLoading(false);
    };
    fetch();
  }, [filtersStr, dispatch, deactivateId, cancelHoldingId, reloadDataTickTock]);

  return (
    <>
      <div style={{ display: action ? 'none' : undefined }}>
        <OrderFilters loading={loading} onChange={(value: OrderFiltersParams) => setFilters(value)} />
        <div>
          {data ? (
            <>
              {data.map(one => (
                <FlightBookingItem
                  key={one.id}
                  data={one}
                  getAirlineInfo={getAirlineInfo}
                  getTicketClass={getTicketClass}
                  lang={lang}
                  search={props.router.location.search}
                />
              ))}
              {!loading && total - PAGE_SIZE * page > 0 && (
                <div style={{ textAlign: 'center', marginTop: '23px' }}>
                  <Button onClick={fetchMore}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage id="result.displayMore" values={{ num: total - PAGE_SIZE * page }} />
                    </Typography>
                  </Button>
                </div>
              )}
              {loading && (
                <>
                  <FlightBookingItemSkeleton />
                  <FlightBookingItemSkeleton />
                  <FlightBookingItemSkeleton />
                </>
              )}
              {!data.length && !loading && <NoDataResult id="m.noData" style={{ marginTop: '48px' }} />}
            </>
          ) : (
            <>
              <FlightBookingItemSkeleton />
              <FlightBookingItemSkeleton />
              <FlightBookingItemSkeleton />
              <FlightBookingItemSkeleton />
              <FlightBookingItemSkeleton />
            </>
          )}
        </div>
      </div>

      {action === 'detail' && (
        <FlightBookingDetail
          breadcrumb={<FlightBookingDetailBreadCrumbs />}
          onDeactivate={id => setDeactivateId(id)}
          onCancelHolding={id => setCancelHoldingId(id)}
        />
      )}
      {action === 'changeBaggage' && <FlightBaggageBox />}
      {action === 'payBaggage' && <FlightBaggagePay />}
      {action === 'refund' && <FlightBookingRefund reloadData={() => setReloadDataTickTock(!reloadDataTickTock)} />}
      {action === 'splitCode' && <FlightSplitCode reloadData={() => setReloadDataTickTock(!reloadDataTickTock)} />}
      {action === 'changeItinerary' && <ChangeItineraryBox />}
      {action === 'payChangeItinerary' && <ChangeItineraryPay />}
      {action === 'payBookingHolding' && <FlightBookingHoldingPay />}
      {action === 'exportInvoice' && <ExportInvoice breadcrumb={<FlightExportInvoiceBreadcrumbs />} />}
      {action === 'orderRequestDetail' && (
        <ExportInvoiceRequestDetail breadcrumb={<FlightRequestDetailBreadscrumbs />} />
      )}
    </>
  );
};

export default connect(mapStateToProps)(Flight);
