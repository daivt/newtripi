import { Typography, Button } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../../redux/reducers';
import iconCheckSuccess from '../../../../../svg/ic_check_success.svg';
import {
  ACTION_PARAM_NAME,
  FlightManagementBookingItemActionType,
  BOOKING_ID_PARAM_NAME,
} from '../../../constants';
import Link from '../../../../common/components/Link';
import { ROUTES, some } from '../../../../../constants';
import { Helmet } from 'react-helmet';

const mapStateToProps = (state: AppState) => {
  return {
    router: state.router,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  data: some;
  reloadData: () => void;
}

interface State {}

class FlightSplitCodeSuccess extends React.PureComponent<Props, State> {
  state: State = {};

  backableToDetail() {
    const { router, data } = this.props;

    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
    params.set(BOOKING_ID_PARAM_NAME, data.id);
    return params.toString();
  }

  render() {
    const { intl, reloadData } = this.props;

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.paymentSuccess' })}</title>
        </Helmet>
        <div
          style={{
            paddingTop: '50px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <img src={iconCheckSuccess} alt="" />
          <Typography variant="body2" style={{ padding: '12px 0 18px' }}>
            <FormattedMessage id="m.order.splitCodeSuccess" />
          </Typography>

          <Link
            to={{
              pathname: ROUTES.management,
              search: `?${this.backableToDetail()}`,
            }}
            onClick={reloadData}
          >
            <Button
              size="large"
              variant="contained"
              color="secondary"
              style={{ width: '200px', margin: '10px 0' }}
              onClick={() => this.backableToDetail()}
            >
              <Typography variant="button">
                <FormattedMessage id="m.order.viewOrderDetailAgain" />
              </Typography>
            </Button>
          </Link>
        </div>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightSplitCodeSuccess));
