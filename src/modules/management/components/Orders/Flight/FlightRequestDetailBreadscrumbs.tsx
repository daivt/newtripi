import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  HotelManagementBookingItemActionType,
} from '../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightRequestDetailBreadscrumbs: React.FunctionComponent<Props> = props => {
  const { router } = props;
  const goBackList = React.useCallback(() => {
    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    return params.toString();
  }, [router.location.search]);
  const goBackDetail = React.useCallback(() => {
    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'detail' as HotelManagementBookingItemActionType);
    return params.toString();
  }, [router.location.search]);

  const backableToList = router.location.state && router.location.state.backableToList;

  const backableToOrderDetail =
    router.location.state && router.location.state.backableToOrderDetail;

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() =>
            backableToList
              ? props.dispatch(go(-2))
              : props.dispatch(replace({ search: `${goBackList()}` }))
          }
        >
          <FormattedMessage id="flight" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() =>
            backableToOrderDetail
              ? props.dispatch(go(-1))
              : props.dispatch(replace({ search: `${goBackDetail()}` }))
          }
        >
          <FormattedMessage id="m.orderDetail" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.invoice" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router };
}

export default connect(mapStateToProps)(FlightRequestDetailBreadscrumbs);
