import { Button, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREEN, GREY, RED, SECONDARY } from '../../../../../colors';
import { DATE_TIME_FORMAT, ROUTES, some } from '../../../../../constants';
import Link from '../../../../common/components/Link';
import PassengerCount from '../../../../common/components/PassengerCount';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  FlightManagementBookingItemActionType,
  getStatusColor,
} from '../../../constants';
import { getTicketStatus } from '../../../utils';
import { FlightAirlineInfoItem } from './FlightAirlineInfoItem';

const Wrapper = styled.div`
  background: #ffffff;
  border: 0.5px solid ${GREY};
  box-sizing: border-box;
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
`;

interface IFlightOrderItemProps {
  data: some;
  getAirlineInfo(id: number): some;
  getTicketClass(classCode: string): some | undefined;
  lang: string;
  search: string;
}

const FlightBookingItem: React.FunctionComponent<IFlightOrderItemProps> = props => {
  const { data, getAirlineInfo, getTicketClass, lang } = props;

  const seeDetail = (id: number) => {
    const params = new URLSearchParams(props.search);
    params.set(BOOKING_ID_PARAM_NAME, `${id}`);
    params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);

    return params.toString();
  };

  const ouboundTicketStatus = getTicketStatus(data, data.outbound);
  const inboundTicketStatus = data.isTwoWay ? getTicketStatus(data, data.inbound) : '';

  return (
    <Wrapper style={{ marginBottom: '10px' }}>
      {data.paymentStatus === 'holding' && (
        <>
          {moment(data.expiredTime).isAfter(moment()) ? (
            <div style={{ display: 'flex', padding: '4px 12px', backgroundColor: GREEN }}>
              <Typography variant="body2" style={{ color: 'white' }}>
                <FormattedMessage id="m.order.paymentExpiredTime" />
              </Typography>
              &nbsp;
              <Typography variant="body2" style={{ color: 'white' }}>
                {moment(data.expiredTime).format('LT L')}
              </Typography>
            </div>
          ) : (
            <div style={{ display: 'flex', padding: '4px 12px', backgroundColor: RED }}>
              <Typography variant="body2" style={{ color: 'white' }}>
                <FormattedMessage id="m.order.paymentExpired" />
              </Typography>
            </div>
          )}
        </>
      )}
      <div style={{ display: 'flex', padding: '10px 20px 20px' }}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FlightAirlineInfoItem
              airlineInfo={getAirlineInfo(data.outbound.airlineId)}
              ticketClass={getTicketClass(data.outbound.ticketClassCode)}
              lang={lang}
              ticket={data.outbound}
            />
          </div>

          {data.isTwoWay && (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                marginTop: '12px',
                paddingTop: '12px',
                borderTop: `1px solid ${GREY}`,
              }}
            >
              <FlightAirlineInfoItem
                airlineInfo={getAirlineInfo(data.inbound.airlineId)}
                ticketClass={getTicketClass(data.inbound.ticketClassCode)}
                lang={lang}
                ticket={data.inbound}
                isInbound={true}
              />
            </div>
          )}
        </div>
        <div style={{ paddingLeft: '30px' }}>
          <Typography variant="body2">
            <FormattedMessage id="m.order.status" />
            :&nbsp;
            <span
              style={{
                color: `${getStatusColor(data.paymentStatus)}`,
              }}
            >
              {!!data.paymentStatus && <FormattedMessage id={data.paymentStatus} />}
            </span>
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.order.code" />: {data.orderCode}
          </Typography>
          {data.parentBookingCode && (
            <Typography variant="body2">
              <FormattedMessage id="m.order.separateFromOrder" />: {data.parentBookingCode}
            </Typography>
          )}
          {data.isTwoWay ? (
            <>
              <Typography variant="body2">
                <FormattedMessage id="m.order.outboundCode" />
                :&nbsp;
                <span style={{ color: ouboundTicketStatus ? SECONDARY : BLUE }}>
                  {ouboundTicketStatus ? (
                    <FormattedMessage id={ouboundTicketStatus} />
                  ) : (
                    data.outboundPnrCode
                  )}
                </span>
              </Typography>
              <Typography variant="body2">
                <FormattedMessage id="m.order.inboundCode" />
                :&nbsp;
                <span style={{ color: inboundTicketStatus ? SECONDARY : BLUE }}>
                  {inboundTicketStatus ? (
                    <FormattedMessage id={inboundTicketStatus} />
                  ) : (
                    data.inboundPnrCode
                  )}
                </span>
              </Typography>
            </>
          ) : (
            <Typography variant="body2">
              <FormattedMessage id="m.order.bookingCode" />
              :&nbsp;
              <span style={{ color: ouboundTicketStatus ? SECONDARY : BLUE }}>
                {ouboundTicketStatus ? (
                  <FormattedMessage id={ouboundTicketStatus} />
                ) : (
                  data.outboundPnrCode
                )}
              </span>
            </Typography>
          )}
          <Typography variant="body2">
            <FormattedMessage id="m.order.customer" />
            :&nbsp;
            {data.bookerContact
              ? `${data.bookerContact.lastName} ${data.bookerContact.firstName}`
              : ''}
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.order.bookingDate" />
            :&nbsp;
            {moment(data.bookedDate, DATE_TIME_FORMAT).format('L HH:mm:ss')}
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.order.passengerCount" />
            :&nbsp;
            <PassengerCount
              adults={data.numAdults}
              children={data.numChildren}
              babies={data.numInfants}
            />
          </Typography>
        </div>
        <div style={{ display: 'flex', flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
          <Typography variant="body2">
            <FormattedMessage id="m.order.totalPayable" />
          </Typography>
          <Typography variant="subtitle2" color="secondary">
            {data.finalPriceFormatted}
          </Typography>

          <div style={{ paddingTop: '12px' }}>
            <Link
              to={{
                pathname: ROUTES.management,
                search: `?${seeDetail(data.id)}`,
                state: { backableToList: true },
              }}
            >
              <Button variant="contained" color="secondary" style={{ width: '190px' }}>
                <FormattedMessage id="m.viewDetail" />
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export const FlightBookingItemSkeleton: React.FunctionComponent = props => {
  return (
    <Wrapper style={{ marginBottom: '10px', height: '175px', padding: '10px 20px 20px' }}>
      <div style={{ display: 'flex' }}>
        <div
          style={{ width: '120px', display: 'flex', flexDirection: 'column', alignItems: 'center' }}
        >
          <Skeleton width="40px" height="40px" variant="circle" style={{ margin: '3px 0' }} />
          <Skeleton width="95px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="50px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="115px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '20px' }}>
          <Skeleton width="120px" variant="text" />
          <Skeleton width="210px" height="60px" variant="rect" style={{ marginTop: '6px' }} />
        </div>

        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '30px' }}>
          <Skeleton width="160px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="180px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="200px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        </div>

        <div style={{ display: 'flex', flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
          <Skeleton width="130px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="90px" variant="text" style={{ margin: '3px 0' }} />

          <div style={{ paddingTop: '12px' }}>
            <Skeleton width="190px" height="30px" variant="rect" style={{ margin: '3px 0' }} />
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default FlightBookingItem;
