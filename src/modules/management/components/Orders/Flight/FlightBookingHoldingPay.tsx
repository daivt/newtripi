import { Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { BLUE } from '../../../../../colors';
import { ROUTES } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import DiscountCodeBox from '../../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../../common/components/Payment/PaymentMethodsBox';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../../../common/constants';
import { ACTION_PARAM_NAME, FlightManagementBookingItemActionType } from '../../../constants';
import {
  fetchPaymentMethodsForHoldingBooking,
  setSelectedPaymentMethod,
  setPayResponseMessage,
} from '../../../redux/flightOrderReducer';
import { computeHoldingBookingPayable } from '../../../utils';
import FlightBookingHoldingPayBreadCrumbs from './FlightBookingHoldingPayBreadCrumbs';

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px;
`;

const mapStateToProps = (state: AppState) => {
  return {
    router: state.router,
    flightOrder: state.management.flightOrder,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class FlightBookingHoldingPay extends PureComponent<Props, State> {
  async componentDidMount() {
    const { dispatch, router, flightOrder } = this.props;

    dispatch(setPayResponseMessage());
    if (!flightOrder.flightOrderDetail) {
      const params = new URLSearchParams(router.location.search);
      params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
      dispatch(replace(`${ROUTES.management}?${params.toString()}`));
      return;
    }

    dispatch(fetchPaymentMethodsForHoldingBooking());
  }

  render() {
    const { intl, dispatch, flightOrder } = this.props;

    const payableNumber = computeHoldingBookingPayable(flightOrder);
    const priceAfter = payableNumber.total - payableNumber.pointToAmount;
    const totalPayable = priceAfter + payableNumber.paymentFee;

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'm.order.holdingBookingPayment',
            })}`}
          </title>
        </Helmet>

        <FlightBookingHoldingPayBreadCrumbs />

        {flightOrder.paymentMethods && flightOrder.selectedPaymentMethod ? (
          <div>
            <Typography variant="h5" style={{ padding: '16px 0' }}>
              <FormattedMessage id="booking.pay" />
            </Typography>

            <div>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.totalFee" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={payableNumber.originAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
              {!!payableNumber.discountAmount && (
                <Line>
                  <Typography variant="body2" color="primary">
                    <FormattedMessage id="booking.addPromotionCode" />
                    &nbsp;
                    {flightOrder.promotion && flightOrder.promotionCode}
                    {flightOrder.flightOrderDetail && flightOrder.flightOrderDetail.promotionCode}
                  </Typography>

                  <Typography variant="body2" color="primary">
                    <FormattedNumber value={0 - payableNumber.discountAmount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
              )}
              {flightOrder.usePointPayment &&
                flightOrder.selectedPaymentMethod &&
                flightOrder.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
                !!flightOrder.pointUsing && (
                  <Line>
                    <Typography variant="body2">
                      <FormattedMessage id="booking.pointPayment" />
                    </Typography>

                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={-payableNumber.pointToAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                )}
              {!!payableNumber.paymentFee && (
                <Line>
                  {payableNumber.paymentFee > 0 ? (
                    <Typography variant="body2">
                      <FormattedMessage id="booking.paymentFixedFeeSide" />
                    </Typography>
                  ) : (
                    <Typography variant="body2">
                      <FormattedMessage
                        id="booking.discountPaymentMethod"
                        values={{
                          methodName: flightOrder.selectedPaymentMethod
                            ? flightOrder.selectedPaymentMethod.name
                            : '',
                        }}
                      />
                    </Typography>
                  )}
                  <Typography
                    variant="body2"
                    color={payableNumber.paymentFee > 0 ? 'secondary' : 'primary'}
                  >
                    <FormattedNumber value={payableNumber.paymentFee} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
              )}
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.totalPayable" />
                </Typography>
                <Typography variant="subtitle2" color="secondary">
                  <FormattedNumber
                    value={totalPayable < 0 ? 0 : totalPayable}
                    maximumFractionDigits={0}
                  />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            </div>
            {flightOrder.flightOrderDetail && !flightOrder.flightOrderDetail.promotionCode && (
              <div
                style={{
                  padding: '16px 0',
                }}
              >
                <Typography variant="h6" style={{ paddingBottom: '12px' }}>
                  <FormattedMessage id="booking.discountCode" />
                </Typography>

                <DiscountCodeBox moduleType="flightPayLater" />
              </div>
            )}

            <Typography variant="h6" style={{ padding: '16px 0' }}>
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
            <PaymentMethodsBox
              paymentMethods={flightOrder.paymentMethods}
              total={payableNumber.total}
              setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
              selectedMethod={flightOrder.selectedPaymentMethod}
              priceAfter={priceAfter}
              promotionCode={flightOrder.promotionCode}
              moduleType="flightPayLater"
            />

            <div
              style={{
                padding: '16px 16px 32px 16px',
                color: BLUE,
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="booking.seePaymentDetail" />
              </Typography>
            </div>
          </div>
        ) : (
          <div style={{ margin: '42px' }}>
            <LoadingIcon />
          </div>
        )}
      </div>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightBookingHoldingPay));
