import { Button, ButtonBase, Typography } from '@material-ui/core';
import { goBack, push, replace } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { BLUE } from '../../../../../colors';
import { DATE_FORMAT, ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconDelete } from '../../../../../svg/delete.svg';
import { ReactComponent as IconExportInvoice } from '../../../../../svg/exportInvoice.svg';
import { ReactComponent as IconDeleteConfirm } from '../../../../../svg/ic_delete_ticket_alert.svg';
import { ReactComponent as IconBaggage } from '../../../../../svg/mng_flight_baggage.svg';
import { ReactComponent as IconClock } from '../../../../../svg/mng_flight_clock.svg';
import { ReactComponent as IconSplitCode } from '../../../../../svg/mng_flight_split_code.svg';
import { ReactComponent as IconTicketBack } from '../../../../../svg/mng_flight_ticket_back.svg';
import { ReactComponent as IconResendEmail } from '../../../../../svg/resendEmail.svg';
import { ReactComponent as IconWarning } from '../../../../../svg/ic_warning.svg';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import Link from '../../../../common/components/Link';
import LoadingButton from '../../../../common/components/LoadingButton/index';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import MessageDialog from '../../../../common/components/MessageDialog';
import { fetchThunk } from '../../../../common/redux/thunks';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../../common/utils';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, FlightManagementBookingItemActionType } from '../../../constants';
import {
  setChangeItineraryData,
  setFlightOrderDetail,
  setRefundData,
  setSplitCodeData,
} from '../../../redux/flightOrderReducer';
import ResendEmailPopup from '../common/ResendEmailPopup';
import { FlightAirlineInfoItem } from './FlightAirlineInfoItem';
import FlightBookingDetailInfoBox from './FlightBookingDetailInfoBox';

const ItemButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

type DialogActionType = 'deactivateOrder' | 'cancelHolding';

interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onDeactivate(id: number): void;
  onCancelHolding(id: number): void;
  breadcrumb: React.ReactNode;
  noAction?: boolean;
}

interface State {
  data?: some;
  message: string;
  loading: {
    ticketBack: boolean;
    splitCode: boolean;
  };
  dialogAction?: DialogActionType;
  showResendEmail: boolean;
  showConfirmDialog: boolean;
  showCancelHolding: boolean;
  showConfirmTicketBack: boolean;
}
class FlightBookingDetail extends React.PureComponent<Props, State> {
  state: State = {
    message: '',
    loading: {
      ticketBack: false,
      splitCode: false,
    },
    showResendEmail: false,
    showConfirmDialog: false,
    showCancelHolding: false,
    showConfirmTicketBack: false,
  };

  async componentDidMount() {
    const { router, dispatch } = this.props;
    const params = new URLSearchParams(router.location.search);
    const bookId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;

    const json = await dispatch(fetchThunk(`${API_PATHS.getFlightBookingDetail}?id=${bookId}`, 'get', true));

    if (json.code === 200) {
      this.setState({ data: json.data });
      dispatch(setFlightOrderDetail(json.data));
      dispatch(setChangeItineraryData({ isInboundSelected: false }));
    }
  }

  changeBaggage = (id: number) => {
    const { router } = this.props;

    const params = new URLSearchParams(router.location.search);
    params.set(BOOKING_ID_PARAM_NAME, `${id}`);
    params.set(ACTION_PARAM_NAME, 'changeBaggage' as FlightManagementBookingItemActionType);
    return params.toString();
  };

  changeItinerary = (id: number) => {
    const { router } = this.props;
    const params = new URLSearchParams(router.location.search);
    params.set(BOOKING_ID_PARAM_NAME, `${id}`);
    params.set(ACTION_PARAM_NAME, 'changeItinerary' as FlightManagementBookingItemActionType);
    return params.toString();
  };

  changeExportInvoice = (data: some) => {
    const { router, intl, dispatch } = this.props;

    if (data.vatInvoiceInfo) {
      this.setState({ message: intl.formatMessage({ id: 'm.order.exportInvoiceInProgress' }) });
      return;
    }

    const params = new URLSearchParams(router.location.search);
    params.set(BOOKING_ID_PARAM_NAME, `${data.id}`);
    params.set(ACTION_PARAM_NAME, 'exportInvoice' as FlightManagementBookingItemActionType);

    dispatch(
      push({
        pathname: ROUTES.management,
        search: params.toString(),
        state: { ...this.props.router.location.state, backableToDetail: true },
      }),
    );
  };

  async checkBookingVoidability() {
    const { router, dispatch } = this.props;
    const params = new URLSearchParams(router.location.search);
    const bookId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;

    this.setState({
      loading: { ...this.state.loading, ticketBack: true },
      showConfirmTicketBack: false,
    });

    const json = await dispatch(
      fetchThunk(`${API_PATHS.checkBookingVoidability}?bookingId=${bookId}&refund=true`, 'get', true),
    );

    this.setState({ loading: { ...this.state.loading, ticketBack: false } });

    if (json.code !== 200) {
      this.setState({ message: json.message });
      return;
    }

    dispatch(setRefundData(json.data));
    params.set(ACTION_PARAM_NAME, 'refund' as FlightManagementBookingItemActionType);
    dispatch(
      push(`${ROUTES.management}?${params.toString()}`, {
        ...router.location.state,
        backableToDetail: true,
      }),
    );
  }

  async checkDividable() {
    const { router, dispatch } = this.props;
    const params = new URLSearchParams(router.location.search);
    const bookId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;

    this.setState({ loading: { ...this.state.loading, splitCode: true } });
    const json = await dispatch(fetchThunk(`${API_PATHS.checkDividable}?id=${bookId}`, 'get', true));

    this.setState({ loading: { ...this.state.loading, splitCode: false } });
    if (json.code !== 200 || (json.code === 200 && !json.data.dividable)) {
      this.setState({ message: json.data.message });
      return;
    }

    dispatch(setSplitCodeData(json.data));
    params.set(ACTION_PARAM_NAME, 'splitCode' as FlightManagementBookingItemActionType);
    dispatch(
      push(`${ROUTES.management}?${params.toString()}`, {
        ...router.location.state,
        backableToDetail: true,
      }),
    );
  }

  async onDeactivate() {
    const { dispatch, intl } = this.props;
    const { data } = this.state;

    if (!data) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.deactivate,
        'post',
        true,
        JSON.stringify({
          module: 'flight',
          bookingId: data.id,
        }),
      ),
    );

    if (json.code === 200) {
      this.setState({
        showConfirmDialog: false,
        dialogAction: 'deactivateOrder',
        message: intl.formatMessage({ id: 'm.order.deleteOrderSuccess' }),
      });
    } else {
      this.setState({ message: json.message });
    }
  }

  async onCancelHolding() {
    const { dispatch, intl } = this.props;
    const { data } = this.state;

    if (!data) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.sendRequest,
        'post',
        true,
        JSON.stringify({
          module: 'flight',
          type: 'cancel_holding',
          bookingId: data.id,
        }),
      ),
    );

    if (json.code === 200) {
      this.setState({
        showCancelHolding: false,
        dialogAction: 'cancelHolding',
        message: intl.formatMessage({ id: 'm.order.cancelHoldingSuccess' }),
      });
    } else {
      this.setState({ message: json.message });
    }
  }

  onCloseMessageDialog() {
    const { data, dialogAction } = this.state;
    const { onDeactivate, onCancelHolding } = this.props;
    this.setState({ message: '' });
    if (dialogAction === 'deactivateOrder' || dialogAction === 'cancelHolding') {
      const { router, dispatch } = this.props;

      const backableToList = router.location.state && router.location.state.backableToList;

      dialogAction === 'deactivateOrder' ? onDeactivate(data ? data.id : 0) : onCancelHolding(data ? data.id : 0);

      if (backableToList) {
        dispatch(goBack());
        return;
      }

      const params = new URLSearchParams(router.location.search);
      params.delete(BOOKING_ID_PARAM_NAME);
      params.delete(ACTION_PARAM_NAME);
      dispatch(replace({ search: params.toString() }));
    }
  }

  public render() {
    const { dispatch, getAirlineInfo, getTicketClass, lang, intl, breadcrumb, noAction, router } = this.props;

    const { data, message, loading, showConfirmDialog, showCancelHolding, showConfirmTicketBack } = this.state;

    if (!data) {
      return (
        <div style={{ margin: '42px' }}>
          <LoadingIcon />
        </div>
      );
    }

    const ticketExpired = data.isTwoWay
      ? moment(data.inbound.departureDate, DATE_FORMAT).isBefore(moment())
      : moment(data.outbound.departureDate, DATE_FORMAT).isBefore(moment());

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({ id: 'flight' })} - ${data.outbound.fromAirportName} > ${
              data.outbound.toAirportName
            } - ${data.outbound.departureDate}`}
          </title>
        </Helmet>
        {breadcrumb}
        <Typography variant="h5" style={{ padding: '16px 0px' }}>
          <FormattedMessage id="m.bookingDetail" />
        </Typography>

        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FlightAirlineInfoItem
              airlineInfo={getAirlineInfo(data.outbound.airlineId)}
              ticketClass={getTicketClass(data.outbound.ticketClassCode)}
              lang={lang}
              ticket={data.outbound}
            />
          </div>
          {data.isTwoWay && (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <FlightAirlineInfoItem
                isInbound
                airlineInfo={getAirlineInfo(data.inbound.airlineId)}
                ticketClass={getTicketClass(data.inbound.ticketClassCode)}
                lang={lang}
                ticket={data.inbound}
              />
            </div>
          )}
          <Typography variant="body2" style={{ color: BLUE }}>
            <FormattedMessage id="m.order.flightDetail" />
          </Typography>
        </div>
        <div
          style={{
            display: 'flex',
            marginTop: '10px',
            flexWrap: 'wrap',
          }}
        >
          {data.paymentStatus === 'success' && !noAction && (
            <>
              {!ticketExpired && (
                <>
                  <Link
                    to={{
                      pathname: ROUTES.management,

                      search: `?${this.changeBaggage(data.id)}`,
                      state: { ...this.props.router.location.state, backableToDetail: true },
                    }}
                  >
                    <Button
                      variant="contained"
                      color="default"
                      style={{
                        margin: '8px 10px 8px 0',
                        minWidth: '155px',
                        backgroundColor: '#fff',
                      }}
                    >
                      <ItemButton>
                        <IconBaggage style={{ marginRight: '6px' }} />
                        <Typography variant="body2">
                          <FormattedMessage id="m.order.baggage" />
                        </Typography>
                      </ItemButton>
                    </Button>
                  </Link>
                  <Link
                    to={{
                      pathname: ROUTES.management,
                      search: `?${this.changeItinerary(data.id)}`,
                      state: { ...this.props.router.location.state, backableToDetail: true },
                    }}
                  >
                    <Button
                      variant="contained"
                      color="default"
                      style={{ margin: '8px 10px', minWidth: '155px', backgroundColor: '#fff' }}
                    >
                      <ItemButton>
                        <IconClock style={{ marginRight: '6px' }} />
                        <Typography variant="body2">
                          <FormattedMessage id="m.order.changeFlightTime" />
                        </Typography>
                      </ItemButton>
                    </Button>
                  </Link>
                </>
              )}
            </>
          )}

          {!noAction &&
            (data.paymentStatus === 'success' ||
              (data.paymentStatus === 'holding' && moment(data.expiredTime).isAfter(moment()))) && (
              <LoadingButton
                loading={loading.splitCode}
                variant="contained"
                color="default"
                style={{ margin: '8px 10px', minWidth: '155px', backgroundColor: '#fff' }}
                onClick={() => this.checkDividable()}
              >
                <ItemButton>
                  <IconSplitCode style={{ marginRight: '6px' }} />
                  <Typography variant="body2">
                    <FormattedMessage id="m.order.splitCode" />
                  </Typography>
                </ItemButton>
              </LoadingButton>
            )}

          {!noAction && data.paymentStatus === 'success' && (
            <LoadingButton
              loading={loading.ticketBack}
              variant="contained"
              color="default"
              style={{ margin: '8px 10px', minWidth: '155px', backgroundColor: '#fff' }}
              onClick={() =>
                data.paidPoint ? this.setState({ showConfirmTicketBack: true }) : this.checkBookingVoidability()
              }
            >
              <ItemButton>
                <IconTicketBack style={{ marginRight: '6px' }} />
                <Typography variant="body2">
                  <FormattedMessage id="m.order.ticketBack" />
                </Typography>
              </ItemButton>
            </LoadingButton>
          )}
        </div>

        <FlightBookingDetailInfoBox data={data} router={router} lang={lang} />

        <div style={{ display: 'flex' }}>
          {!noAction && (
            <>
              {data.paymentStatus === 'success' && (
                <>
                  <div style={{ marginTop: '20px', marginRight: '24px' }}>
                    <ButtonBase
                      style={{
                        borderRadius: '4px',
                      }}
                      onClick={() => this.changeExportInvoice(data)}
                    >
                      <div style={{ display: 'flex', padding: '3px 12px' }}>
                        <IconExportInvoice style={{ marginRight: '10px' }} />
                        <Typography variant="body2">
                          <FormattedMessage id="m.exportInvoice" />
                        </Typography>
                      </div>
                    </ButtonBase>
                  </div>

                  <div style={{ marginTop: '20px', marginRight: '24px' }}>
                    <ButtonBase
                      style={{
                        borderRadius: '4px',
                      }}
                      onClick={() => this.setState({ showResendEmail: true })}
                    >
                      <div style={{ display: 'flex', padding: '3px 12px' }}>
                        <IconResendEmail style={{ marginRight: '10px' }} />
                        <Typography variant="body2">
                          <FormattedMessage id="m.resendEmail" />
                        </Typography>
                      </div>
                    </ButtonBase>
                  </div>
                </>
              )}
              <div style={{ marginTop: '20px', marginRight: '24px' }}>
                <ButtonBase
                  style={{
                    borderRadius: '4px',
                  }}
                  onClick={() => this.setState({ showConfirmDialog: true })}
                >
                  <div style={{ display: 'flex', padding: '3px 12px' }}>
                    <IconDelete style={{ marginRight: '10px' }} />
                    <Typography variant="body2">
                      <FormattedMessage id="m.deleteOrder" />
                    </Typography>
                  </div>
                </ButtonBase>
              </div>
            </>
          )}
        </div>

        <ResendEmailPopup
          show={this.state.showResendEmail}
          data={data}
          dispatch={dispatch}
          onClose={() => this.setState({ showResendEmail: false })}
          onSuccess={() =>
            this.setState({
              showResendEmail: false,
              message: intl.formatMessage({ id: 'm.order.resendEmailSuccess' }),
            })
          }
          onError={messageError =>
            this.setState({
              showResendEmail: false,
              message: messageError,
            })
          }
          module="flight"
        />

        <ConfirmDialog
          message={
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <IconDeleteConfirm style={{ height: '120px' }} />
              <Typography variant="body1">
                <FormattedMessage id="m.order.areYouSureWantDeleteThisOrder" />
              </Typography>
            </div>
          }
          show={showConfirmDialog}
          cancelMessageId="ignore"
          confirmMessageId="delete"
          onCancel={() => this.setState({ showConfirmDialog: false })}
          onAccept={() => this.onDeactivate()}
        />

        <ConfirmDialog
          message={
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <IconDeleteConfirm style={{ height: '120px' }} />
              <Typography variant="body1">
                <FormattedMessage id="m.order.areYouSureWantCancelHolding" />
              </Typography>
            </div>
          }
          show={showCancelHolding}
          cancelMessageId="ignore"
          onCancel={() => this.setState({ showCancelHolding: false })}
          onAccept={() => this.onCancelHolding()}
        />

        <ConfirmDialog
          message={
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <IconWarning style={{ height: '120px' }} />
              <Typography variant="body1">
                <FormattedMessage id="m.order.refundPointDescription" />
              </Typography>
            </div>
          }
          show={showConfirmTicketBack}
          cancelMessageId="cancel"
          onCancel={() => this.setState({ showConfirmTicketBack: false })}
          onAccept={() => this.checkBookingVoidability()}
        />

        <MessageDialog
          show={!!message}
          onClose={() => this.onCloseMessageDialog()}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '18px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{message}</Typography>
            </div>
          }
        />
      </div>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return {
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    lang: state.intl.locale.substring(0, 2),
    router: state.router,
  };
};

export default connect(mapState2Props)(injectIntl(FlightBookingDetail));
