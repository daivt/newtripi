import { Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedDate } from 'react-intl';
import { GREY } from '../../../../../colors';
import { DATE_FORMAT, some } from '../../../../../constants';
import iconFlight from '../../../../../svg/ic_flight_itinerary_airplane.svg';
import { durationMillisecondToHour } from '../../../../common/utils';

interface Props {
  ticket: some;
  airlineInfo: some;
  ticketClass: some | undefined;
  lang: string;
  isInbound?: boolean;
}

export const FlightAirlineInfoItem: React.FC<Props> = props => {
  const { ticket, airlineInfo, ticketClass, lang } = props;

  const departureDate = moment(ticket.departureDate, DATE_FORMAT);
  const dateDiff = moment(ticket.arrivalDate, DATE_FORMAT).diff(departureDate, 'days');

  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          minWidth: '120px',
        }}
      >
        <img src={airlineInfo.logo} alt="" style={{ width: '40px' }} />
        <Typography variant="caption">{airlineInfo.name}</Typography>
        <Typography variant="caption">{ticket.flightCode}</Typography>
        <Typography variant="caption">
          {ticketClass ? (lang.startsWith('vi') ? ticketClass.v_name : ticketClass.i_name) : ''}
        </Typography>
      </div>

      <div style={{ minWidth: '250px', paddingLeft: '20px' }}>
        <Typography variant="body2">
          <FormattedDate value={departureDate.toDate()} weekday="short" />, &nbsp;
          {departureDate.format('L')}
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="subtitle1">{ticket.fromAirport}</Typography>
            <Typography variant="body2">{ticket.departureTime}</Typography>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
              alignItems: 'center',
            }}
          >
            <div style={{ display: 'flex', alignItems: 'center', width: '100%', height: '35px' }}>
              <div style={{ flex: 1 }}>
                <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
              </div>
              <img src={iconFlight} alt="" />
              <div style={{ flex: 1 }}>
                <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
              </div>
            </div>
            <Typography variant="body2" color="textSecondary">
              {durationMillisecondToHour(ticket.duration)}
            </Typography>
          </div>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="subtitle1">{ticket.toAirport}</Typography>
            <Typography variant="body2">
              {ticket.arrivalTime}
              <span style={{ opacity: dateDiff ? 1 : 0 }}>
                <span>
                  {' '}
                  (+
                  {dateDiff}
                  d)
                </span>
              </span>
            </Typography>
          </div>
        </div>
      </div>
    </>
  );
};
