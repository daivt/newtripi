import { Button, Typography } from '@material-ui/core';
import { RouterState } from 'connected-react-router';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY, SECONDARY } from '../../../../../colors';
import { DATE_FORMAT, DATE_TIME_FORMAT, ROUTES, some } from '../../../../../constants';
import Link from '../../../../common/components/Link';
import { ACTION_PARAM_NAME, FlightManagementBookingItemActionType, getStatusColor } from '../../../constants';
import { getTicketStatus } from '../../../utils';
import OrderRequestList from '../../common/OrderRequestList';
import FlightGuestsPopup from './FlightGuestsPopup';

const ItemRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 6px 0px;
`;

interface Props {
  data: some;
  router: RouterState;
  lang: string;
}

interface State {
  showGuestDetail: boolean;
}

export default class FlightBookingDetailInfoBox extends PureComponent<Props, State> {
  state: State = {
    showGuestDetail: false,
  };

  pay = () => {
    const { router } = this.props;

    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'payBookingHolding' as FlightManagementBookingItemActionType);
    return params.toString();
  };

  render() {
    const { data } = this.props;
    const ouboundTicketStatus = getTicketStatus(data, data.outbound);
    const inboundTicketStatus = data.isTwoWay ? getTicketStatus(data, data.inbound) : '';
    return (
      <div
        style={{
          padding: '20px',
          marginTop: '8px',
          background: '#ffffff',
          border: `0.5px solid ${GREY}`,
          borderRadius: '4px',
        }}
      >
        <div style={{ display: 'flex' }}>
          <div style={{ flex: 1 }}>
            <Typography variant="subtitle2" style={{ paddingBottom: '16px' }}>
              <FormattedMessage id="m.order.orderInfo" />
            </Typography>
            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="m.order.status" />
              :&nbsp;
              <span
                style={{
                  color: `${getStatusColor(data.status)}`,
                }}
              >
                {!!data.status && <FormattedMessage id={data.status} />}
              </span>
            </Typography>
            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="m.order.code" />: {data.orderCode}
            </Typography>

            {data.parentBookingCode && (
              <Typography variant="body2" style={{ padding: '6px 0' }}>
                <FormattedMessage id="m.order.separateFromOrder" />: {data.parentBookingCode}
              </Typography>
            )}
            {data.isTwoWay ? (
              <>
                <Typography variant="body2" style={{ padding: '6px 0' }}>
                  <FormattedMessage id="m.order.outboundCode" />
                  :&nbsp;
                  <span style={{ color: ouboundTicketStatus ? SECONDARY : BLUE }}>
                    {ouboundTicketStatus ? <FormattedMessage id={ouboundTicketStatus} /> : data.outboundPnrCode}
                  </span>
                </Typography>
                <Typography variant="body2" style={{ padding: '6px 0' }}>
                  <FormattedMessage id="m.order.inboundCode" />
                  :&nbsp;
                  <span style={{ color: inboundTicketStatus ? SECONDARY : BLUE }}>
                    {inboundTicketStatus ? <FormattedMessage id={inboundTicketStatus} /> : data.inboundPnrCode}
                  </span>
                </Typography>
              </>
            ) : (
              <Typography variant="body2" style={{ padding: '6px 0' }}>
                <FormattedMessage id="m.order.bookingCode" />
                :&nbsp;
                <span style={{ color: ouboundTicketStatus ? SECONDARY : BLUE }}>
                  {ouboundTicketStatus ? <FormattedMessage id={ouboundTicketStatus} /> : data.outboundPnrCode}
                </span>
              </Typography>
            )}

            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="m.order.bookingDate" />
              :&nbsp;
              {moment(data.bookedDate, DATE_TIME_FORMAT).format('L HH:mm:ss')}
            </Typography>

            {data.bookerContact && (
              <>
                <Typography variant="subtitle2" style={{ paddingTop: '20px', paddingBottom: '16px' }}>
                  <FormattedMessage id="m.order.customerInfo" />
                </Typography>

                <Typography variant="body2" style={{ padding: '6px 0' }}>
                  <FormattedMessage id="fullName" />
                  :&nbsp;
                  <span style={{ textTransform: 'capitalize' }}>
                    {data.bookerContact.lastName} {data.bookerContact.firstName}
                  </span>
                </Typography>
                <Typography variant="body2" style={{ padding: '6px 0' }}>
                  <FormattedMessage id="telephone" />
                  :&nbsp;
                  <span style={{ textTransform: 'capitalize' }}>{data.bookerContact.phone}</span>
                </Typography>
                <Typography variant="body2" style={{ padding: '6px 0' }}>
                  <FormattedMessage id="email" />
                  :&nbsp;
                  <a href={`mailto:${data.bookerContact.email}`} style={{ textDecoration: 'none', color: BLUE }}>
                    {data.bookerContact.email}
                  </a>
                </Typography>
              </>
            )}
            <Typography variant="subtitle2" style={{ paddingTop: '20px', paddingBottom: '16px' }}>
              <FormattedMessage id="m.order.contactInfo" />
            </Typography>

            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="fullName" />
              :&nbsp;
              <span style={{ textTransform: 'capitalize' }}>{data.mainContact.fullName}</span>
            </Typography>
            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="telephone" />
              :&nbsp;
              <span style={{ textTransform: 'capitalize' }}>{data.mainContact.phone1}</span>
            </Typography>
            <Typography variant="body2" style={{ padding: '6px 0' }}>
              <FormattedMessage id="address" />
              :&nbsp;
              {data.mainContact.addr1}
            </Typography>

            <div
              style={{
                paddingTop: '20px',
                paddingBottom: '8x',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}
            >
              <Typography variant="subtitle2" style={{ marginRight: '20px' }}>
                <FormattedMessage id="m.order.guestList" />
              </Typography>
              <Button
                variant="outlined"
                color="secondary"
                style={{ marginTop: '10px' }}
                size="large"
                onClick={() => this.setState({ showGuestDetail: true })}
              >
                <Typography variant="button">
                  <FormattedMessage id="m.viewDetail" />
                </Typography>
              </Button>
            </div>
          </div>
          <div style={{ flex: 1, paddingRight: '40px' }}>
            {data.insuranceContact && (
              <div style={{ marginBottom: '16px' }}>
                <Typography variant="subtitle2" style={{ paddingBottom: '16px' }}>
                  <FormattedMessage id="booking.travelInsurance" />
                </Typography>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="booking.insurancePackage" />
                  </Typography>
                  <Typography variant="body2">{data.guests?.[0].insuranceInfo.insurancePackageName}</Typography>
                </ItemRow>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="start" />
                  </Typography>
                  <Typography variant="body2">
                    {data.guests[0].insuranceInfo &&
                      moment(data.guests[0].insuranceInfo.fromDate, DATE_FORMAT).format('L')}
                  </Typography>
                </ItemRow>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="end" />
                  </Typography>
                  <Typography variant="body2">
                    {data.guests[0].insuranceInfo &&
                      moment(data.guests[0].insuranceInfo.toDate, DATE_FORMAT).format('L')}
                  </Typography>
                </ItemRow>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="m.order.unitPrice" />
                  </Typography>
                  <Typography variant="body2">
                    <FormattedMessage
                      id="m.order.unitPricePerPerson"
                      values={{
                        num: <FormattedNumber value={data.insuranceAmount / data.guests.length} />,
                      }}
                    />
                  </Typography>
                </ItemRow>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="m.order.numPassenger" />
                  </Typography>
                  <Typography variant="body2">{data.guests.length}</Typography>
                </ItemRow>
                <ItemRow>
                  <Typography variant="body2">
                    <FormattedMessage id="m.order.finalPrice" />
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <FormattedNumber value={data.insuranceAmount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </ItemRow>
              </div>
            )}

            <Typography variant="subtitle2" style={{ paddingBottom: '16px' }}>
              <FormattedMessage id="m.order.paymentDetail" />
            </Typography>

            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.totalPayable" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.grandTotal} /> <FormattedMessage id="currency" />
              </Typography>
            </ItemRow>
            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.discountCode" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={0 - data.discount} /> <FormattedMessage id="currency" />
              </Typography>
            </ItemRow>
            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.paymentMethod" />
              </Typography>
              <Typography variant="body2">{data.paymentMethod}</Typography>
            </ItemRow>
            {!!data.paymentMethodFee && (
              <ItemRow>
                <Typography variant="body2">
                  <FormattedMessage
                    id={data.paymentMethodFee < 0 ? 'booking.paymentFixedDiscountSide' : 'booking.paymentFixedFeeSide'}
                  />
                </Typography>
                <Typography variant="body2">
                  <FormattedNumber value={data.paymentMethodFee} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </ItemRow>
            )}
            {data.transferMoneyMethod && (
              <ItemRow>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.transferMoneyMethod" />
                </Typography>
                <Typography variant="body2">{data.transferMoneyMethod}</Typography>
              </ItemRow>
            )}
            {data.pointAmount > 0 && (
              <ItemRow>
                <Typography variant="body2">
                  <FormattedMessage id="booking.pointPayment" />
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={data.paidPoint} />
                  &nbsp;
                  <FormattedMessage id="point" />
                  &nbsp;/&nbsp;
                  <FormattedNumber value={-data.pointAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </ItemRow>
            )}
            {!!data.refundAmount && (
              <ItemRow>
                <Typography variant="body2">
                  <FormattedMessage id="refunded" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={data.refundAmount} /> <FormattedMessage id="currency" />
                </Typography>
              </ItemRow>
            )}
            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.paymentPrice" />
              </Typography>
              <Typography variant="body2" color="secondary">
                {data.finalPriceFormatted}
              </Typography>
            </ItemRow>
            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.pointBonus" />
              </Typography>
              <Typography variant="body2" color="primary">
                <FormattedNumber value={data.bonusPoint} /> <FormattedMessage id="point" />
              </Typography>
            </ItemRow>
            <ItemRow>
              <Typography variant="body2">
                <FormattedMessage id="m.order.paymentStatus" />
              </Typography>
              <Typography
                variant="body2"
                color="primary"
                style={{
                  color: `${getStatusColor(data.paymentStatus)}`,
                }}
              >
                {!!data.paymentStatus && <FormattedMessage id={data.paymentStatus} />}
              </Typography>
            </ItemRow>
            {data.paymentStatus === 'holding' && moment(data.expiredTime).isAfter(moment()) && (
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${this.pay()}`,
                  state: { ...this.props.router.location.state, backableToDetail: true },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ marginTop: '16px', width: '170px' }}
                  size="large"
                >
                  <Typography variant="button">
                    <FormattedMessage id="m.pay" />
                  </Typography>
                </Button>
              </Link>
            )}
          </div>
        </div>

        <OrderRequestList bookingId={data.id} bookModule="flight" />
        <FlightGuestsPopup
          guests={data.guests}
          show={this.state.showGuestDetail}
          onClose={() => this.setState({ showGuestDetail: false })}
        />
      </div>
    );
  }
}
