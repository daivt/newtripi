import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { some, DATE_FORMAT } from '../../../../../constants';
import moment from 'moment';

interface Props {
  show: boolean;
  guests: some[];
  onClose(): void;
}

interface State {}

class FlightGuestsPopup extends PureComponent<Props, State> {
  state: State = {};

  render() {
    const { show, guests, onClose } = this.props;

    return (
      <Dialog open={show} fullWidth maxWidth="sm">
        <div style={{ display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <div
              style={{
                display: 'flex',
                flex: 1,
                paddingLeft: '16px',
                paddingTop: '32px',
                justifyContent: 'center',
              }}
            >
              <Typography variant="h5">
                <FormattedMessage id="booking.travellersList" />
              </Typography>
            </div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <IconButton style={{ padding: '8px' }} size="small" onClick={onClose}>
                <IconClose />
              </IconButton>
            </div>
          </div>
          <div style={{ flex: 1, padding: '0 30px', overflow: 'auto' }}>
            {guests.map((guest, index: number) => (
              <div key={index} style={{ paddingTop: '10px' }}>
                <div style={{ display: 'flex', alignItems: 'center', paddingTop: '10px' }}>
                  <Typography variant="subtitle2">{guest.fullName}</Typography>&nbsp;-&nbsp;
                  <Typography variant="body1" color="textSecondary">
                    <FormattedMessage
                      id={guest.ageCategory === 'infant' ? 'baby' : guest.ageCategory}
                    />
                  </Typography>
                </div>
                {!!guest.dob && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="birthday" />: {guest.dob}
                  </Typography>
                )}
                {!!guest.outboundEticketNo && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="m.order.outboundTicketNumber" />:{' '}
                    {guest.outboundEticketNo}
                  </Typography>
                )}
                {guest.outboundBaggage && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.outBaggages" />: {guest.outboundBaggage.name}
                  </Typography>
                )}
                {!!guest.inboundEticketNo && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="m.order.inboundTicketNumber" />: {guest.inboundEticketNo}
                  </Typography>
                )}
                {!!guest.inboundBaggage && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.inBaggages" />: {guest.inboundBaggage.name}
                  </Typography>
                )}
                {!!guest.nationality && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.nationalityCountry" />: {guest.nationality}
                  </Typography>
                )}
                {!!guest.passport && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.passport" />: {guest.passport}
                  </Typography>
                )}
                {!!guest.passportCountry && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.passportCountry" />: {guest.passportCountry}
                  </Typography>
                )}
                {!!guest.passportExpiry && (
                  <Typography variant="body2" style={{ padding: '3px 0' }}>
                    <FormattedMessage id="booking.passportExpired" />:{' '}
                    {moment(guest.passportExpiry, DATE_FORMAT).format('L')}
                  </Typography>
                )}
              </div>
            ))}
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: '12px',
              minHeight: '70px',
              boxShadow:
                '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
            }}
          >
            <div style={{ padding: '0 16px' }}>
              <Button
                style={{ width: '170px' }}
                variant="contained"
                size="large"
                color="secondary"
                onClick={onClose}
              >
                <Typography variant="button">
                  <FormattedMessage id="close" />
                </Typography>
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default FlightGuestsPopup;
