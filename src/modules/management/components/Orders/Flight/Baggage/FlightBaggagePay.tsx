import { Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../../../colors';
import { ROUTES } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import { computePaymentFees } from '../../../../../booking/utils';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import PaymentMethodsBox from '../../../../../common/components/Payment/PaymentMethodsBox';
import { ACTION_PARAM_NAME, FlightManagementBookingItemActionType } from '../../../../constants';
import {
  fetchPaymentMethodsForAddingBaggages,
  fetchPointPaymentForBaggageAndChangeItinerary,
  setPayResponseMessage,
  setSelectedPaymentMethod,
} from '../../../../redux/flightOrderReducer';
import FlightBaggagePayBreadCrumbs from './FlightBaggagePayBreadCrumbs';

const mapStateToProps = (state: AppState) => {
  return {
    flightOrder: state.management.flightOrder,
    userData: state.account.userData,
    totalPrice: state.management.flightOrder.totalPrice,
    router: state.router,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class FlightBaggagePay extends PureComponent<Props, State> {
  async componentDidMount() {
    const { dispatch, router, flightOrder } = this.props;

    dispatch(setPayResponseMessage());
    if (!flightOrder.flightOrderDetail) {
      const params = new URLSearchParams(router.location.search);
      params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
      dispatch(replace(`${ROUTES.management}?${params.toString()}`));
      return;
    }
    dispatch(fetchPaymentMethodsForAddingBaggages());
    dispatch(fetchPointPaymentForBaggageAndChangeItinerary());
  }

  render() {
    const { intl, dispatch, flightOrder, totalPrice } = this.props;
    const paymentFee = flightOrder.selectedPaymentMethod
      ? computePaymentFees(flightOrder.selectedPaymentMethod, totalPrice)
      : 0;

    const totalPayable = totalPrice + paymentFee;

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'booking.buyMoreCheckin',
            })} - ${intl.formatMessage({ id: 'booking.pay' })}`}
          </title>
        </Helmet>

        <FlightBaggagePayBreadCrumbs />

        {flightOrder.paymentMethods && flightOrder.selectedPaymentMethod ? (
          <div>
            <Typography variant="h5" style={{ padding: '16px 0' }}>
              <FormattedMessage id="booking.pay" />
            </Typography>

            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '5px',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="m.order.totalFee" />
              </Typography>
              <Typography variant="body2" color="secondary">
                <FormattedNumber value={totalPrice} /> <FormattedMessage id="currency" />
              </Typography>
            </div>
            {!!paymentFee && (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: '5px',
                }}
              >
                <Typography variant="body2">
                  <FormattedMessage id="booking.paymentFixedFeeSide" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={paymentFee} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            )}
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '5px',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="m.order.totalPayable" />
              </Typography>
              <Typography variant="subtitle2" color="secondary">
                <FormattedNumber
                  value={totalPayable < 0 ? 0 : totalPayable}
                  maximumFractionDigits={0}
                />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>

            <Typography variant="h6" style={{ padding: '16px 0' }}>
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
            <PaymentMethodsBox
              paymentMethods={flightOrder.paymentMethods}
              total={totalPrice}
              setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
              selectedMethod={flightOrder.selectedPaymentMethod}
              priceAfter={totalPrice}
              moduleType="baggages"
            />
            <div
              style={{
                padding: '16px 16px 32px 16px',
                color: BLUE,
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="booking.seePaymentDetail" />
              </Typography>
            </div>
          </div>
        ) : (
          <div style={{ margin: '42px' }}>
            <LoadingIcon />
          </div>
        )}
      </div>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightBaggagePay));
