import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../../redux/reducers';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  FlightManagementBookingItemActionType,
} from '../../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightExportInvoiceBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { router, state, dispatch } = props;

  const backToOrderList = React.useCallback(() => {
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(go(-2));
      return;
    }

    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  }, [state, router, dispatch]);

  const backToDetail = React.useCallback(() => {
    const backableToDetail = state && state.backableToDetail;

    if (backableToDetail) {
      dispatch(go(-1));
      return;
    }
    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
    dispatch(replace({ search: params.toString() }));
  }, [state, router, dispatch]);

  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToOrderList()}
      >
        <FormattedMessage id="flight" />
      </Typography>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToDetail()}
      >
        <FormattedMessage id="m.bookingDetail" />
      </Typography>
      <Typography variant="body2" color="secondary">
        <FormattedMessage id="m.exportInvoice" />
      </Typography>
    </Breadcrumbs>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(FlightExportInvoiceBreadcrumbs);
