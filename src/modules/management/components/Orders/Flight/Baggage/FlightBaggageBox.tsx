import { Button, Typography } from '@material-ui/core';
import { goBack, push, replace } from 'connected-react-router';
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../../API';
import { GREY } from '../../../../../../colors';
import { ROUTES, some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import { CustomTab, CustomTabs } from '../../../../../common/components/elements';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import MessageDialog from '../../../../../common/components/MessageDialog';
import { fetchThunk } from '../../../../../common/redux/thunks';
import {
  ACTION_PARAM_NAME,
  BOOKING_ID_PARAM_NAME,
  FlightManagementBookingItemActionType,
} from '../../../../constants';
import { setGuests, setTotalPrice } from '../../../../redux/flightOrderReducer';
import { computeBaggagePayable } from '../../../../utils';
import FlightBaggageBreadCrumbs from './FlightBaggageBreadCrumbs';
import { FlightBaggageItem } from './FlightBaggageItem';

const Wrapper = styled.div`
  background: #ffffff;
  border: 0.5px solid ${GREY};
  box-sizing: border-box;
  border-radius: 4px;
  padding: 0 20px 10px;
`;

const ItemRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 0px;
  border-bottom: 0.5px solid ${GREY};
`;

function mapState2Props(state: AppState) {
  return {
    router: state.router,
    flightOrderDetail: state.management.flightOrder.flightOrderDetail,
  };
}

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  tab: number;
  data?: {
    inbound: some[];
    outbound: some[];
  };
  guests: some[];
  errorMessage: string;
}

class FlightBaggageBox extends React.PureComponent<Props, State> {
  state: State = {
    tab: 0,
    guests: this.props.flightOrderDetail ? this.props.flightOrderDetail.guests : [],
    errorMessage: '',
  };

  async componentDidMount() {
    const { router, dispatch, flightOrderDetail } = this.props;

    const params = new URLSearchParams(router.location.search);
    const bookId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;

    if (!flightOrderDetail) {
      params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
      dispatch(replace(`${ROUTES.management}?${params.toString()}`));
      return;
    }

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getBaggageForFlightBooking}?bookingId=${bookId}`, 'get', true),
    );

    if (json.code === 200) {
      this.setState({ data: json.data });
    } else {
      this.setState({ errorMessage: json.message });
    }
  }

  genPayParams() {
    const params = new URLSearchParams(this.props.router.location.search);
    params.set(ACTION_PARAM_NAME, 'payBaggage' as FlightManagementBookingItemActionType);
    return params.toString();
  }

  render() {
    const { tab, data, guests, errorMessage } = this.state;
    const { intl, dispatch, router, flightOrderDetail } = this.props;

    if (!data || !flightOrderDetail) {
      return (
        <div style={{ padding: '40px' }}>
          <LoadingIcon />
          <MessageDialog
            show={!!errorMessage}
            onClose={() => {
              this.setState({ errorMessage: '' });
              dispatch(goBack());
            }}
            message={
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="body1">{errorMessage}</Typography>
              </div>
            }
          />
        </div>
      );
    }

    const payableNums = computeBaggagePayable(flightOrderDetail.guests, guests);

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'flight',
            })} - ${intl.formatMessage({ id: 'booking.buyMoreCheckin' })}`}
          </title>
        </Helmet>

        <FlightBaggageBreadCrumbs />

        <Typography variant="h5" style={{ padding: '16px 0' }}>
          <FormattedMessage id="booking.buyMoreCheckin" />
        </Typography>

        <Wrapper>
          <CustomTabs value={tab} onChange={(e, tab) => this.setState({ tab })}>
            <CustomTab
              label={
                <Typography variant="body2">
                  <FormattedMessage id="outbound" />
                </Typography>
              }
            />
            {flightOrderDetail.isTwoWay && (
              <CustomTab
                label={
                  <Typography variant="body2">
                    <FormattedMessage id="inbound" />
                  </Typography>
                }
              />
            )}
          </CustomTabs>

          {tab === 0 ? (
            <FlightBaggageItem
              bookingDetail={flightOrderDetail}
              baggages={data.outbound}
              guests={guests}
              isOutbound={true}
              onSelect={(outboundBaggage: some, index: number) =>
                this.setState({
                  guests: [
                    ...guests.slice(0, index),
                    { ...guests[index], outboundBaggage },
                    ...guests.slice(index + 1),
                  ],
                })
              }
            />
          ) : (
            <FlightBaggageItem
              bookingDetail={flightOrderDetail}
              baggages={data.inbound}
              guests={guests}
              isOutbound={false}
              onSelect={(inboundBaggage: some, index: number) =>
                this.setState({
                  guests: [
                    ...guests.slice(0, index),
                    { ...guests[index], inboundBaggage },
                    ...guests.slice(index + 1),
                  ],
                })
              }
            />
          )}
        </Wrapper>

        <div style={{ display: 'flex', flexDirection: 'column', width: '560px', margin: '20px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="pay" />
          </Typography>

          <ItemRow>
            <Typography variant="body2">
              <FormattedMessage id="m.order.totalCost" />
            </Typography>

            <Typography variant="body2" color="secondary">
              <FormattedNumber value={payableNums.totalPriceNoTax} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </ItemRow>
          <ItemRow>
            <Typography variant="body2">
              <FormattedMessage id="m.order.vat" />
            </Typography>

            <Typography variant="body2" color="secondary">
              <FormattedNumber value={payableNums.totalTax} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </ItemRow>
          <ItemRow>
            <Typography variant="body2">
              <FormattedMessage id="m.order.totalPayable" />
            </Typography>

            <Typography variant="body2" color="secondary">
              <FormattedNumber value={payableNums.totalPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </ItemRow>

          <div style={{ display: 'flex', justifyContent: 'flex-end', margin: '20px 0' }}>
            <Button
              variant="contained"
              size="large"
              color="secondary"
              disabled={!payableNums.totalPrice}
              onClick={() => {
                dispatch(setGuests(guests));
                dispatch(setTotalPrice(payableNums.totalPrice));
                dispatch(
                  push(`${ROUTES.management}?${this.genPayParams()}`, {
                    ...router.location.state,
                    backableToBuyMore: true,
                  }),
                );
              }}
            >
              <Typography variant="button">
                <FormattedMessage id="pay" />
              </Typography>
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapState2Props)(injectIntl(FlightBaggageBox));
