import React from 'react';
import { some, DATE_TIME_FORMAT_NO_S } from '../../../../../../constants';
import { Typography, ButtonBase } from '@material-ui/core';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { GREY, SECONDARY, DARK_GREY } from '../../../../../../colors';
import moment from 'moment';
import LoadingIcon from '../../../../../common/components/LoadingIcon';

interface Props {
  bookingDetail?: some;
  baggages: some[];
  isOutbound: boolean;
  guests: some[];
  onSelect(data: some, index: number): void;
}

export const FlightBaggageItem: React.FC<Props> = props => {
  const { bookingDetail, baggages, guests, isOutbound, onSelect } = props;

  if (!bookingDetail) {
    return (
      <div style={{ margin: '42px' }}>
        <LoadingIcon />
      </div>
    );
  }

  const isExpired = moment(
    isOutbound
      ? `${bookingDetail.outbound.departureDate} ${bookingDetail.outbound.departureTime}`
      : `${bookingDetail.inbound.departureDate} ${bookingDetail.inbound.departureTime}`,
    DATE_TIME_FORMAT_NO_S,
  ).isSameOrBefore(moment());

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {bookingDetail && baggages.length && !isExpired ? (
        bookingDetail.guests.map(
          (guest: some, index: number) =>
            guest.ageCategory !== 'infant' && (
              <div key={guest.id} style={{ display: 'flex', flexDirection: 'column' }}>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    paddingTop: '20px',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Typography variant="subtitle2">{guest.fullName}</Typography>&nbsp;
                    {isOutbound
                      ? guest.outboundBaggage && (
                          <>
                            - &nbsp;
                            <Typography color="textSecondary" variant="body1">
                              <FormattedMessage
                                id="m.order.bought"
                                values={{ text: guest.outboundBaggage.name }}
                              />
                            </Typography>
                          </>
                        )
                      : guest.inboundBaggage && (
                          <>
                            - &nbsp;
                            <Typography color="textSecondary" variant="body1">
                              <FormattedMessage
                                id="m.order.bought"
                                values={{ text: guest.inboundBaggage.name }}
                              />
                            </Typography>
                          </>
                        )}
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="m.order.buyMore" />
                      :&nbsp;
                    </Typography>
                    {isOutbound
                      ? guests[index].outboundBaggage && (
                          <Typography color="secondary" variant="subtitle2">
                            <FormattedNumber
                              value={
                                guests[index].outboundBaggage.price -
                                (guest.outboundBaggage ? guest.outboundBaggage.price : 0)
                              }
                            />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </Typography>
                        )
                      : guests[index].inboundBaggage && (
                          <Typography color="secondary" variant="subtitle2">
                            <FormattedNumber
                              value={
                                guests[index].inboundBaggage.price -
                                (guest.inboundBaggage ? guest.inboundBaggage.price : 0)
                              }
                            />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </Typography>
                        )}
                  </div>
                </div>
                {isOutbound ? (
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {baggages.map((baggage: some) => {
                      const isCurrent =
                        guests[index].outboundBaggage &&
                        baggage.weight === guests[index].outboundBaggage.weight;
                      return (
                        (!guest.outboundBaggage ||
                          baggage.weight >= guest.outboundBaggage.weight) && (
                          <ButtonBase
                            key={baggage.id}
                            style={{
                              width: '105px',
                              height: '72px',
                              border: `0.5px solid ${isCurrent ? SECONDARY : GREY}`,
                              borderRadius: '4px',
                              marginRight: '18px',
                              marginTop: '10px',
                              color: `${isCurrent ? SECONDARY : DARK_GREY}`,
                              background: `${
                                isCurrent
                                  ? 'linear-gradient(0deg, rgba(255, 112, 67, 0.12), rgba(255, 112, 67, 0.12)), #FFFFFF'
                                  : '#fff'
                              }`,
                              boxShadow:
                                '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
                            }}
                            onClick={() => onSelect(baggage, index)}
                          >
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                              <Typography variant="body1">{baggage.weight}kg</Typography>
                              <Typography variant="body1">
                                <FormattedNumber value={baggage.price} />
                                &nbsp;
                                <FormattedMessage id="currency" />
                              </Typography>
                            </div>
                          </ButtonBase>
                        )
                      );
                    })}
                  </div>
                ) : (
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {baggages.map((baggage: some) => {
                      const isCurrent =
                        guests[index].inboundBaggage &&
                        baggage.weight === guests[index].inboundBaggage.weight;
                      return (
                        (!guest.inboundBaggage ||
                          baggage.weight >= guest.inboundBaggage.weight) && (
                          <ButtonBase
                            key={baggage.id}
                            style={{
                              width: '105px',
                              height: '72px',
                              border: `0.5px solid ${isCurrent ? SECONDARY : GREY}`,
                              borderRadius: '4px',
                              marginRight: '18px',
                              marginTop: '10px',
                              color: `${isCurrent ? SECONDARY : DARK_GREY}`,
                              background: `${
                                isCurrent
                                  ? 'linear-gradient(0deg, rgba(255, 112, 67, 0.12), rgba(255, 112, 67, 0.12)), #FFFFFF'
                                  : '#fff'
                              }`,
                              boxShadow:
                                '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
                            }}
                            onClick={() => onSelect(baggage, index)}
                          >
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                              <Typography variant="body1">{baggage.weight}kg</Typography>
                              <Typography variant="body1">
                                <FormattedNumber value={baggage.price} />
                                &nbsp;
                                <FormattedMessage id="currency" />
                              </Typography>
                            </div>
                          </ButtonBase>
                        )
                      );
                    })}
                  </div>
                )}
              </div>
            ),
        )
      ) : (
        <div style={{ minHeight: '120px' }}>
          <Typography variant="body1" style={{ marginTop: '32px' }}>
            <FormattedMessage
              id="m.order.notSupportBuyBaggage"
              values={{
                text: (
                  <span style={{ textTransform: 'lowercase' }}>
                    <FormattedMessage id={isOutbound ? 'outbound' : 'inbound'} />
                  </span>
                ),
              }}
            />
          </Typography>
        </div>
      )}
    </div>
  );
};
