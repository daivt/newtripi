import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { replace, goBack } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../redux/reducers';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME } from '../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightBookingDetailBreadCrumbs: React.FunctionComponent<Props> = props => {
  const backToOrderList = () => {
    const { router, state, dispatch } = props;
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(goBack());
      return;
    }

    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  };

  return (
    <div>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: 'pointer' }}
          onClick={() => backToOrderList()}
        >
          <FormattedMessage id="flight" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="m.bookingDetail" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(FlightBookingDetailBreadCrumbs);
