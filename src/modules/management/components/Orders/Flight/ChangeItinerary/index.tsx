import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps, FormattedMessage, FormattedNumber } from 'react-intl';
import { Typography, Button, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import { AppState } from '../../../../../../redux/reducers';
import { makeGetAirlineInfo } from '../../../../../common/utils';
import { connect } from 'react-redux';
import { ChangeItineraryInfoBox } from './ChangeItineraryInfoBox';
import SelectFlightDialog from './SelectFlightDialog';
import { some, ROUTES } from '../../../../../../constants';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { fetchThunk } from '../../../../../common/redux/thunks';
import { API_PATHS } from '../../../../../../API';
import { setChangeItineraryData } from '../../../../redux/flightOrderReducer';
import MessageDialog from '../../../../../common/components/MessageDialog';
import { computeChangeItineraryPayable } from '../../../../utils';
import Link from '../../../../../common/components/Link';
import { FlightManagementBookingItemActionType, ACTION_PARAM_NAME } from '../../../../constants';
import iconArrow from '../../../../../../svg/ic_arrow.svg';
import ChangeItineraryBreadCrumbs from './ChangeItineraryBreadCrumbs';

function mapState2Props(state: AppState) {
  return {
    flightOrderDetail: state.management.flightOrder.flightOrderDetail,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    router: state.router,
    flightSearchResult: state.result.flight.data,
    changeItineraryData: state.management.flightOrder.changeItineraryData,
  };
}
interface Props extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  showDialog: boolean;
  message: string;
}

class ChangeItineraryBox extends PureComponent<Props, State> {
  state: State = {
    showDialog: false,
    message: '',
  };

  async calculateItinerary(data: some) {
    const { dispatch, flightOrderDetail, flightSearchResult, changeItineraryData } = this.props;

    if (!flightOrderDetail || !flightSearchResult) {
      return;
    }

    const tickets = changeItineraryData.isInboundSelected
      ? {
          inbound: {
            agencyId: data.outbound.aid,
            requestId: flightSearchResult.requestId,
            ticketId: parseInt(data.tid, 10),
          },
        }
      : {
          outbound: {
            agencyId: data.outbound.aid,
            requestId: flightSearchResult.requestId,
            ticketId: parseInt(data.tid, 10),
          },
        };

    const json = await dispatch(
      fetchThunk(
        API_PATHS.calculateItineraryChangingPrice,
        'post',
        true,
        JSON.stringify({
          tickets,
          bookingId: flightOrderDetail.id,
        }),
      ),
    );

    if (json.code === 200) {
      changeItineraryData.isInboundSelected
        ? dispatch(
            setChangeItineraryData({
              ...changeItineraryData,
              inbound: {
                ...data.outbound,
                supportChangeItinerary: true,
                requestId: flightSearchResult.requestId,
                ticketId: parseInt(data.tid, 10),
              },
              inboundChangingPrice: json.data,
            }),
          )
        : dispatch(
            setChangeItineraryData({
              ...changeItineraryData,
              outbound: {
                ...data.outbound,
                supportChangeItinerary: true,
                requestId: flightSearchResult.requestId,
                ticketId: parseInt(data.tid, 10),
              },
              outboundChangingPrice: json.data,
            }),
          );

      this.setState({ showDialog: false });
      return;
    }

    this.setState({ message: json.message });
  }

  genPayParams() {
    const params = new URLSearchParams(this.props.router.location.search);
    params.set(ACTION_PARAM_NAME, 'payChangeItinerary' as FlightManagementBookingItemActionType);
    return params.toString();
  }

  render() {
    const { intl, getAirlineInfo, flightOrderDetail, changeItineraryData, router, dispatch } = this.props;
    const { showDialog, message } = this.state;

    if (!flightOrderDetail) {
      return <div />;
    }

    const pricePayable = computeChangeItineraryPayable(changeItineraryData);
    const noFlightItineraryData =
      !changeItineraryData || (!changeItineraryData.outbound && !changeItineraryData.outbound);

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'flight',
            })} - ${intl.formatMessage({ id: 'm.order.changeFlightTime' })}`}
          </title>
        </Helmet>

        <ChangeItineraryBreadCrumbs />

        <Typography variant="h5" style={{ padding: '16px 0' }}>
          <FormattedMessage id="m.order.changeFlightTime" />
        </Typography>

        {flightOrderDetail.isTwoWay && (
          <>
            <Typography variant="subtitle2" style={{ padding: '16px 0' }}>
              <FormattedMessage id="m.order.doYouWantChangeItinerary" />
            </Typography>

            <RadioGroup row>
              <FormControlLabel
                style={{ paddingRight: '64px' }}
                value="outbound"
                control={<Radio size="small" />}
                checked={!changeItineraryData.isInboundSelected}
                onChange={() =>
                  dispatch(
                    setChangeItineraryData({
                      ...changeItineraryData,
                      isInboundSelected: false,
                    }),
                  )
                }
                label={
                  <Typography
                    variant="body2"
                    color={!changeItineraryData.isInboundSelected ? 'secondary' : 'textSecondary'}
                  >
                    <FormattedMessage id="outbound" />
                  </Typography>
                }
              />
              <FormControlLabel
                style={{ paddingRight: '64px' }}
                value="inbound"
                control={<Radio />}
                checked={changeItineraryData.isInboundSelected}
                onChange={() =>
                  dispatch(
                    setChangeItineraryData({
                      ...changeItineraryData,
                      isInboundSelected: true,
                    }),
                  )
                }
                label={
                  <Typography
                    variant="body2"
                    color={changeItineraryData.isInboundSelected ? 'secondary' : 'textSecondary'}
                  >
                    <FormattedMessage id="inbound" />
                  </Typography>
                }
              />
            </RadioGroup>
          </>
        )}

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          {!changeItineraryData.isInboundSelected ? (
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ display: 'flex', flexDirection: 'column', margin: '10px 0' }}>
                <ChangeItineraryInfoBox
                  oldItinerary={!!changeItineraryData && !!changeItineraryData.outbound}
                  airlineInfo={getAirlineInfo(flightOrderDetail.outbound.airlineId)}
                  ticket={flightOrderDetail.outbound}
                  onChange={() => this.setState({ showDialog: true })}
                />
              </div>

              {changeItineraryData && changeItineraryData.outbound && (
                <>
                  <img src={iconArrow} alt="" style={{ marginTop: '32px' }} />
                  <div style={{ display: 'flex', flexDirection: 'column', margin: '10px 0' }}>
                    <ChangeItineraryInfoBox
                      isNewTicket={true}
                      hideHeader={true}
                      airlineInfo={getAirlineInfo(changeItineraryData.outbound.aid)}
                      ticket={changeItineraryData.outbound}
                      onChange={() => this.setState({ showDialog: true })}
                    />
                  </div>
                </>
              )}
            </div>
          ) : (
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ display: 'flex', flexDirection: 'column', margin: '10px 0' }}>
                <ChangeItineraryInfoBox
                  oldItinerary={!!changeItineraryData && !!changeItineraryData.inbound}
                  airlineInfo={getAirlineInfo(flightOrderDetail.inbound.airlineId)}
                  ticket={flightOrderDetail.inbound}
                  isInbound={true}
                  onChange={() => this.setState({ showDialog: true })}
                />
              </div>

              {changeItineraryData && changeItineraryData.inbound && (
                <>
                  <img src={iconArrow} alt="" style={{ marginTop: '32px' }} />
                  <div style={{ display: 'flex', flexDirection: 'column', margin: '10px 0' }}>
                    <ChangeItineraryInfoBox
                      isNewTicket={true}
                      hideHeader={true}
                      airlineInfo={getAirlineInfo(changeItineraryData.inbound.aid)}
                      ticket={changeItineraryData.inbound}
                      isInbound={true}
                      onChange={() => this.setState({ showDialog: true })}
                    />
                  </div>
                </>
              )}
            </div>
          )}

          <div style={{ display: 'flex', flexDirection: 'column', maxWidth: '350px' }}>
            <Typography variant="h6" style={{ padding: '16px 0' }}>
              <FormattedMessage id="m.pay" />
            </Typography>

            <div style={{ display: 'flex', justifyContent: 'space-between', padding: '3px 0' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.order.priceDifference" />
              </Typography>

              <Typography variant="body2" color="secondary">
                <FormattedNumber value={pricePayable.ticketPriceChangeAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-between', padding: '3px 0' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.order.changeFee" />
              </Typography>

              <Typography variant="body2" color="secondary">
                <FormattedNumber value={pricePayable.changingFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-between', padding: '3px 0' }}>
              <Typography variant="body2">
                <FormattedMessage id="m.order.totalPayable" />
              </Typography>

              <Typography variant="body2" color="secondary">
                <FormattedNumber value={pricePayable.totalAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>

            <div>
              <Link
                style={{
                  pointerEvents: noFlightItineraryData ? 'none' : undefined,
                }}
                to={{
                  pathname: ROUTES.management,
                  search: `?${this.genPayParams()}`,
                  state: { ...router.location.state, backableToChangeItinerary: true },
                }}
              >
                <Button
                  style={{ width: '170px', margin: '8px 0' }}
                  size="large"
                  color="secondary"
                  variant="contained"
                  disabled={noFlightItineraryData}
                >
                  <Typography variant="button">
                    <FormattedMessage id="pay" />
                  </Typography>
                </Button>
              </Link>
            </div>
          </div>
        </div>

        <MessageDialog
          show={!!message}
          onClose={() => this.setState({ message: '' })}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '18px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{message}</Typography>
            </div>
          }
        />

        <SelectFlightDialog
          open={showDialog}
          flightOrderDetail={flightOrderDetail}
          isInbound={changeItineraryData.isInboundSelected}
          onClose={() => this.setState({ showDialog: false })}
          onSelect={data => this.calculateItinerary(data)}
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(injectIntl(ChangeItineraryBox));
