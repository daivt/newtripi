import { Typography, Button } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedDate } from 'react-intl';
import { GREY } from '../../../../../../colors';
import { some, SLASH_DATE_FORMAT, DATE_FORMAT } from '../../../../../../constants';
import iconFlight from '../../../../../../svg/ic_flight_itinerary_airplane.svg';
import { durationMillisecondToHour } from '../../../../../common/utils';

interface Props {
  ticket: some;
  airlineInfo: some;
  isInbound?: boolean;
  hideHeader?: boolean;
  isNewTicket?: boolean;
  oldItinerary?: boolean;
  onChange(): void;
}

export const ChangeItineraryInfoBox: React.FC<Props> = props => {
  const { ticket, airlineInfo, isNewTicket, onChange, oldItinerary } = props;

  const departureDate = !isNewTicket
    ? moment(ticket.departureDate, DATE_FORMAT)
    : moment(ticket.departureDayStr, SLASH_DATE_FORMAT);

  const dateDiff = !isNewTicket
    ? moment(ticket.arrivalDate, DATE_FORMAT).diff(departureDate, 'days')
    : moment(ticket.arrivalDayStr, SLASH_DATE_FORMAT).diff(departureDate, 'days');

  return (
    <>
      <Typography
        variant="body2"
        style={{
          flex: 1,
          opacity: oldItinerary ? 0.5 : 1,
        }}
      >
        <FormattedMessage id="m.departureDay" />
        :&nbsp;
        <FormattedDate value={departureDate.toDate()} weekday="short" />, &nbsp;
        {departureDate.format('L')}
      </Typography>
      <div
        style={{
          display: 'flex',
          border: `1px solid ${GREY}`,
          borderRadius: '4px',
          background: '#fff',
          padding: '8px 0',
          flexDirection: 'column',
          minHeight: '125px',
          opacity: oldItinerary ? 0.5 : 1,
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              minWidth: '120px',
            }}
          >
            <img src={airlineInfo.logo} alt="" style={{ width: '48px', marginBottom: '6px' }} />
            <Typography variant="caption">{airlineInfo.name}</Typography>
          </div>

          <div style={{ minWidth: '280px', paddingLeft: '20px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Typography variant="subtitle1">
                  {!isNewTicket ? ticket.fromAirport : ticket.departureAirport}
                </Typography>
                <Typography variant="body2">{!isNewTicket ? ticket.departureTime : ticket.departureTimeStr}</Typography>
              </div>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  flex: 1,
                  alignItems: 'center',
                }}
              >
                <div style={{ display: 'flex', alignItems: 'center', width: '100%', height: '35px' }}>
                  <div style={{ flex: 1 }}>
                    <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
                  </div>
                  <img src={iconFlight} alt="" />
                  <div style={{ flex: 1 }}>
                    <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
                  </div>
                </div>
                <Typography variant="body2" color="textSecondary">
                  {durationMillisecondToHour(ticket.duration)}
                </Typography>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Typography variant="subtitle1">{!isNewTicket ? ticket.toAirport : ticket.arrivalAirport}</Typography>
                <Typography variant="body2">
                  {!isNewTicket ? ticket.arrivalTime : ticket.arrivalTimeStr}
                  <span style={{ opacity: dateDiff ? 1 : 0 }}>
                    <span>
                      {' '}
                      (+
                      {dateDiff}
                      d)
                    </span>
                  </span>
                </Typography>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            flex: 1,
            marginRight: '10px',
            alignItems: 'flex-end',
          }}
        >
          {oldItinerary ? (
            <Typography style={{ paddingBottom: '6px' }}>
              <FormattedMessage id="m.order.oldItinerary" />
            </Typography>
          ) : (
            <Button
              variant="contained"
              color="secondary"
              style={{ width: '150px' }}
              onClick={() => ticket.supportChangeItinerary && onChange()}
            >
              <Typography variant="button">
                <FormattedMessage id={ticket.supportChangeItinerary ? 'booking.hotel.change' : 'm.order.notSupport'} />
              </Typography>
            </Button>
          )}
        </div>
      </div>
    </>
  );
};
