import { Dialog, DialogContent, DialogTitle, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import Close from '@material-ui/icons/Close';
import FilterList from '@material-ui/icons/FilterList';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../../../colors';
import { DATE_FORMAT, DESKTOP_WIDTH, some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import { Airport } from '../../../../../common/models';
import FlightFilter from '../../../../../result/components/flight/FlightFilter';
import { search as searchFlight } from '../../../../../result/redux/flightResultReducer';
import { FlightSearchParamsState, setParams } from '../../../../../search/redux/flightSearchReducer';
import FlightResultBoxOneWay from './FlightResultBoxOneWay';
import SelectItineraryDate from './SelectItineraryDate';
import { StickyDiv } from '../../../../../common/components/elements';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  filterButton?: React.ReactNode;
  open: boolean;
  flightOrderDetail: some;
  isInbound?: boolean;
  onClose(): void;
  onSelect(data: some): void;
}

const SelectFlightDialog: React.FC<Props> = props => {
  const { open, onClose, onSelect, isInbound, flightOrderDetail } = props;
  const ticketData = isInbound ? flightOrderDetail.inbound : flightOrderDetail.outbound;
  const [newDate, setNewDate] = React.useState<Moment | null>(null);
  const [showFilters, setShowFilters] = React.useState(false);

  const search = React.useCallback(
    (date: Moment | null = null) => {
      const { dispatch, flightOrderDetail } = props;

      const params: FlightSearchParamsState = {
        origin: {
          code: ticketData.fromAirport,
          location: ticketData.fromAirportName,
          name: ticketData.fromAirportName,
        } as Airport,
        destination: {
          code: ticketData.toAirport,
          location: ticketData.toAirportName,
          name: ticketData.toAirportName,
        } as Airport,
        departureDate: date || moment(ticketData.departureDate, DATE_FORMAT),
        returnDate: undefined,
        travellerCountInfo: {
          adultCount: flightOrderDetail.numAdults,
          childCount: flightOrderDetail.numChildren,
          babyCount: flightOrderDetail.numInfants,
        },
        seatClass: [],
      };

      dispatch(setParams(params));
      dispatch(searchFlight(ticketData.airlineId, !isInbound, flightOrderDetail.id));
    },
    [
      isInbound,
      props,
      ticketData.airlineId,
      ticketData.departureDate,
      ticketData.fromAirport,
      ticketData.fromAirportName,
      ticketData.toAirport,
      ticketData.toAirportName,
    ],
  );

  const onEntered = React.useCallback(() => {
    setNewDate(moment(ticketData.departureDate, DATE_FORMAT));
    search();
  }, [search, ticketData.departureDate]);

  const onPickDate = React.useCallback(
    (date: Moment) => {
      setNewDate(date);
      search(date);
    },
    [search],
  );

  return (
    <Dialog maxWidth="lg" fullWidth open={open} onEntered={onEntered} scroll="paper">
      <DialogTitle style={{ padding: 0, flex: 1 }}>
        <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '6px' }}>
          <IconButton style={{ padding: '8px' }} size="small" onClick={onClose}>
            <IconClose />
          </IconButton>
        </div>
      </DialogTitle>

      <MediaQuery minWidth={DESKTOP_WIDTH}>
        {match => {
          if (match) {
            return (
              <DialogContent dividers>
                <div style={{ display: 'flex' }}>
                  <div style={{ width: '260px', marginRight: '10px', flexShrink: 0 }}>
                    <StickyDiv style={{ top: 0 }}>
                      <Typography variant="subtitle2" style={{ padding: '12px 0' }}>
                        <FormattedMessage id="m.order.changeDate" />
                      </Typography>
                      <SelectItineraryDate date={newDate || null} onPickDate={date => onPickDate(date)} />
                      <FlightFilter isChangeItinerary />
                    </StickyDiv>
                  </div>
                  <div style={{ flex: 1, minHeight: '400px' }}>
                    <FlightResultBoxOneWay open={open} onClose={() => onClose()} onSelect={data => onSelect(data)} />
                  </div>
                </div>
              </DialogContent>
            );
          }
          return (
            <DialogContent dividers style={{ paddingTop: 0 }}>
              <div style={{ display: 'flex' }}>
                <div
                  style={{
                    width: '40px',
                    overflow: 'visible',
                    position: 'relative',
                    zIndex: 100,
                    minHeight: '100%',
                    display: 'flex',
                  }}
                >
                  <div
                    style={{
                      flexShrink: 0,
                      width: '300px',
                      transform: showFilters ? 'translateX(0)' : 'translateX(calc(-100% + 40px))',
                      backgroundColor: BACKGROUND,
                      padding: '0 10px 20px 0',
                      borderRight: `1px solid ${GREY}`,
                      transition: 'all 300ms',
                      minHeight: '100%',
                    }}
                  >
                    <StickyDiv style={{ top: 0 }}>
                      <div style={{ textAlign: 'end' }}>
                        {showFilters ? (
                          <IconButton style={{ margin: '10px 0' }} onClick={() => setShowFilters(false)}>
                            <Close style={{ color: DARK_GREY }} />
                          </IconButton>
                        ) : (
                          <IconButton onClick={() => setShowFilters(true)} style={{ margin: '10px 0' }}>
                            <FilterList style={{ color: DARK_GREY }} />
                          </IconButton>
                        )}
                      </div>
                      <div
                        style={{
                          opacity: showFilters ? 1 : 0.4,
                          position: 'relative',
                        }}
                      >
                        <Typography variant="subtitle2" style={{ padding: '12px 0' }}>
                          <FormattedMessage id="m.order.changeDate" />
                        </Typography>

                        <SelectItineraryDate date={newDate || null} onPickDate={date => onPickDate(date)} />
                        <FlightFilter isChangeItinerary />
                        {!showFilters && (
                          <div
                            style={{
                              position: 'absolute',
                              top: 0,
                              bottom: 0,
                              left: 0,
                              right: 0,
                            }}
                          />
                        )}
                      </div>
                    </StickyDiv>
                  </div>
                </div>

                <div style={{ flex: 1, minHeight: '400px', paddingTop: '16px' }}>
                  <FlightResultBoxOneWay open={open} onClose={() => onClose()} onSelect={data => onSelect(data)} />
                </div>
              </div>
            </DialogContent>
          );
        }}
      </MediaQuery>
    </Dialog>
  );
};

function mapStateToProps(state: AppState) {
  return {};
}

export default connect(mapStateToProps)(SelectFlightDialog);
