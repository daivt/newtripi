import { fade } from '@material-ui/core/styles';
import moment, { Moment } from 'moment';
import React from 'react';
import { DayPickerSingleDateController, isInclusivelyAfterDay } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREY, PRIMARY } from '../../../../../../colors';
import calendarSvg from '../../../../../../svg/calendar.svg';
import { renderMonthText } from '../../../../../common/utils';
import { VIBRATE_KEYFRAME } from '../../../../../search/constants';

const InputContainer = styled.div<{ isFocused: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  width: 100%;
  background-color: white;
  box-shadow: ${props => (props.isFocused ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem` : '')};
  border-color: ${props => (props.isFocused ? props.theme.primary : '')};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  cursor: pointer;
`;

interface Props {
  date: Moment | null;
  onPickDate(date: Moment): void;
}

interface State {
  isFocused: boolean;
}

function isDayHighlighted(day: Moment) {
  return moment().format('L') === day.format('L') ? (
    <span style={{ fontWeight: 'bold', color: PRIMARY }}>{day.format('D')}</span>
  ) : (
    day.format('D')
  );
}

class SelectItineraryDate extends React.PureComponent<Props, State> {
  state: State = {
    isFocused: false,
  };

  parent = React.createRef<HTMLDivElement>();
  firefoxWorkaroundHelper = React.createRef<HTMLButtonElement>();
  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }

    this.setState({ isFocused: false });
  };

  onDateChange(date: Moment | null) {
    if (date) {
      this.setState({ isFocused: false });
      this.props.onPickDate(date);
    }
  }

  render() {
    const { isFocused } = this.state;
    const { date } = this.props;

    return (
      <div
        style={{
          borderRadius: '4px',
          background: isFocused ? 'white' : 'transparent',
          position: 'relative',
          outline: 'none',
        }}
        tabIndex={-1}
        ref={this.parent}
        onBlur={this.onBlur}
        onFocus={e => {
          e.target !== this.parent.current &&
            this.firefoxWorkaroundHelper.current &&
            this.firefoxWorkaroundHelper.current.focus();
          e.target === e.currentTarget && this.setState({ isFocused: true });
        }}
      >
        <div>
          <InputContainer isFocused={isFocused}>
            <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
              <img alt="" src={calendarSvg} />
            </div>
            <div
              style={{
                height: '24px',
                margin: '7px 1px',
                flex: 1,
              }}
            >
              {!date && <FormattedMessage id="nothingYet" />}
              {date && `${date.format('L')}`}
            </div>
          </InputContainer>
        </div>
        <button
          onFocus={e => e.stopPropagation()}
          ref={this.firefoxWorkaroundHelper}
          style={{ position: 'absolute', opacity: 0, zIndex: -1 }}
        ></button>
        <div
          style={{
            opacity: isFocused ? 1 : 0,
            position: 'absolute',
            transition: 'opacity 300ms',
            visibility: isFocused ? 'visible' : 'hidden',
            zIndex: 100,
            right: 0,
            left: 0,
            marginTop: '6px',
            background: '#fff',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingTop: '12px',
            }}
          >
            <DayPickerSingleDateController
              renderMonthText={renderMonthText}
              hideKeyboardShortcutsPanel
              date={date}
              onDateChange={date => this.onDateChange(date)}
              focused={isFocused}
              onFocusChange={() => {}}
              renderDayContents={day => isDayHighlighted(day)}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              daySize={36}
              numberOfMonths={1}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default SelectItineraryDate;
