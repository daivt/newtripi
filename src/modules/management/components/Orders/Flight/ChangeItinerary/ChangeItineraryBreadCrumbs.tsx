import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go, goBack, replace } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../../../redux/reducers';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME } from '../../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const ChangeItineraryBreadCrumbs: React.FunctionComponent<Props> = props => {
  const { router, state, dispatch } = props;

  const backToOrderList = () => {
    const backableToList = state && state.backableToList;

    if (backableToList) {
      dispatch(go(-2));
      return;
    }

    const params = new URLSearchParams(router.location.search);
    params.delete(BOOKING_ID_PARAM_NAME);
    params.delete(ACTION_PARAM_NAME);
    dispatch(replace({ search: params.toString() }));
  };

  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: 'pointer' }}
        onClick={() => backToOrderList()}
      >
        <FormattedMessage id="flight" />
      </Typography>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ cursor: state.backableToDetail ? 'pointer' : undefined }}
        onClick={() => (state.backableToDetail ? dispatch(goBack()) : {})}
      >
        <FormattedMessage id="m.bookingDetail" />
      </Typography>
      <Typography variant="body2" color="secondary">
        <FormattedMessage id="m.order.changeFlightTime" />
      </Typography>
    </Breadcrumbs>
  );
};

function mapStateToProps(state: AppState) {
  return { router: state.router, state: state.router.location.state };
}

export default connect(mapStateToProps)(ChangeItineraryBreadCrumbs);
