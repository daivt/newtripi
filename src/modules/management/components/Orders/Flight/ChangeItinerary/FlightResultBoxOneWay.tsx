import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../../../colors';
import { some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../../../common/utils';
import FlightResultHeader from '../../../../../result/components/flight/FlightResultHeader';
import FlightSortBox from '../../../../../result/components/flight/FlightSortBox';
import FlightTicketItem from '../../../../../result/components/flight/FlightTicketItem';
import { FLIGHT_SORT_BY } from '../../../../../result/constants';
import { filterAndSort, flightCheapestCmp, flightFastestCmp, flightTimeTakeOffCmp } from '../../../../../result/utils';

const STEP = 10;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  filterButton?: React.ReactNode;
  open: boolean;
  onClose(): void;
  onSelect(data: some): void;
}

interface State {
  maxDisplay: number;
}

class FlightResultBoxOneWay extends React.Component<Props, State> {
  state = {
    maxDisplay: 10,
  };

  public render(): JSX.Element {
    const {
      data,
      filterParams,
      dispatch,
      getAirlineInfo,
      searchCompleted,
      getTicketClass,
      sortBy,
      filterButton,
      onSelect,
    } = this.props;

    const { maxDisplay } = this.state;

    if (!data || !data.tickets) {
      return (
        <div
          style={{
            height: '200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      );
    }

    const filteredTickets = filterAndSort(
      data.tickets as some[],
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );

    const tickets = filteredTickets.filter((ticket: some, i: number) => i < maxDisplay);

    return (
      <>
        <div style={{ display: 'flex' }}>
          {filterButton}
          <FlightResultHeader count={filteredTickets.length} />
          <FlightSortBox sortBy={sortBy} dispatch={dispatch} />
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
          }}
        >
          {tickets.map((ticket: some, index: number) => (
            <FlightTicketItem
              buttonMsgId="select"
              getAirlineInfo={getAirlineInfo}
              getTicketClass={getTicketClass}
              key={ticket.tid}
              ticket={ticket}
              onClick={() => onSelect(ticket)}
              getPrice={value => <FormattedNumber value={value.outbound.ticketdetail.priceAdultUnit} />}
              priceRemark={
                <Typography variant="caption">
                  <FormattedMessage id="result.oneWayPrice" />
                </Typography>
              }
            />
          ))}
          {searchCompleted < 99.9 && (
            <div
              style={{
                height: '200px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <LoadingIcon />
            </div>
          )}
          {maxDisplay < filteredTickets.length && (
            <div style={{ textAlign: 'center', color: BLUE, lineHeight: '24px', margin: '20px 0' }}>
              <span
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  if (maxDisplay < filteredTickets.length) {
                    this.setState({ maxDisplay: maxDisplay + STEP });
                  }
                }}
              >
                <FormattedMessage id="result.displayMore" values={{ num: filteredTickets.length - maxDisplay }} />
              </span>
            </div>
          )}
        </div>
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    data: state.result.flight.data,
    filterParams: state.result.flight.filterParams,
    sortBy: state.result.flight.sortBy,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    searchCompleted: state.result.flight.searchCompleted,
  };
}

export default connect(mapStateToProps)(FlightResultBoxOneWay);
