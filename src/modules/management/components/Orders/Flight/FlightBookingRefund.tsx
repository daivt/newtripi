import { FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingButton from '../../../../common/components/LoadingButton/index';
import MessageDialog from '../../../../common/components/MessageDialog';
import { fetchThunk } from '../../../../common/redux/thunks';
import { makeGetAirlineInfo, makeGetTicketClass } from '../../../../common/utils';
import { ACTION_PARAM_NAME, FlightManagementBookingItemActionType, RefundType } from '../../../constants';
import { FlightAirlineInfoItem } from './FlightAirlineInfoItem';
import FlightBookingRefundBreadCrumbs from './FlightBookingRefundBreadCrumbs';
import { NewTabLink } from '../../../../common/components/NewTabLink';

function getRefundObject(refundData: some, refundType: RefundType) {
  return refundType === 'outbound'
    ? refundData.voidOutboundInfo
    : refundType === 'inbound'
    ? refundData.voidInboundInfo
    : refundData.voidAllInfo;
}

function mapState2Props(state: AppState) {
  return {
    flightOrderDetail: state.management.flightOrder.flightOrderDetail,
    getAirlineInfo: makeGetAirlineInfo(state.common),
    getTicketClass: makeGetTicketClass(state.common),
    lang: state.intl.locale.substring(0, 2),
    refundData: state.management.flightOrder.refundData,
    router: state.router,
  };
}

interface Props extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  reloadData: () => void;
}

interface State {
  refundType: RefundType;
  loading: boolean;
  message: string;
  refundSuccess?: boolean;
}

class FlightBookingRefund extends React.PureComponent<Props, State> {
  state: State = {
    refundType: 'outbound',
    loading: false,
    message: '',
  };

  componentDidMount() {
    const { refundData } = this.props;

    if (!refundData) {
      this.backableToDetail();
      return;
    }

    this.setState({
      refundType:
        refundData.voidAllInfo && refundData.voidAllInfo.voidable
          ? 'all'
          : refundData.voidOutboundInfo && refundData.voidOutboundInfo.voidable
          ? 'outbound'
          : 'inbound',
    });
  }

  async onRefund() {
    const { dispatch, refundData, reloadData } = this.props;
    const { refundType } = this.state;

    if (!refundData) {
      return;
    }

    this.setState({ loading: true });

    const refundObject = getRefundObject(refundData, refundType);

    if (!refundObject) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.refundBooking,
        'post',
        true,
        JSON.stringify({
          refundInbound: refundType !== 'outbound',
          refundOutbound: refundType !== 'inbound',
          type: refundObject.type,
          bookingId: refundData.bookingId,
          voidPnrCodes: refundObject.pnrCodes,
        }),
      ),
    );

    if (json.code === 200) {
      reloadData();
      this.setState({ message: json.message, refundSuccess: true, loading: false });
    } else {
      this.setState({ message: json.message, loading: false });
    }
  }

  onChange(refundType: RefundType) {
    const { refundData } = this.props;

    if (!refundData) {
      return;
    }

    const refundObject = getRefundObject(refundData, refundType);

    if (refundObject && !refundObject.voidable) {
      this.setState({ message: refundObject.failReason });
      return;
    }

    this.setState({ refundType });
  }

  backableToDetail() {
    const { router, dispatch } = this.props;

    const params = new URLSearchParams(router.location.search);
    params.set(ACTION_PARAM_NAME, 'detail' as FlightManagementBookingItemActionType);
    dispatch(replace(`${ROUTES.management}?${params.toString()}`));
  }

  render() {
    const { intl, refundData, flightOrderDetail, getAirlineInfo, getTicketClass, lang } = this.props;
    const { message, loading, refundType, refundSuccess } = this.state;

    if (!flightOrderDetail || !refundData) {
      return <div />;
    }

    const refundObject = getRefundObject(refundData, refundType);

    return (
      <div>
        <Helmet>
          <title>
            {`${intl.formatMessage({
              id: 'flight',
            })} - ${intl.formatMessage({ id: 'm.order.ticketBack' })}`}
          </title>
        </Helmet>

        <FlightBookingRefundBreadCrumbs />

        <Typography variant="h5" style={{ padding: '16px 0' }}>
          <FormattedMessage id="m.order.ticketBack" />
        </Typography>

        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FlightAirlineInfoItem
              airlineInfo={getAirlineInfo(flightOrderDetail.outbound.airlineId)}
              ticketClass={getTicketClass(flightOrderDetail.outbound.ticketClassCode)}
              lang={lang}
              ticket={flightOrderDetail.outbound}
            />
          </div>
          {flightOrderDetail.isTwoWay && (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <FlightAirlineInfoItem
                isInbound
                airlineInfo={getAirlineInfo(flightOrderDetail.inbound.airlineId)}
                ticketClass={getTicketClass(flightOrderDetail.inbound.ticketClassCode)}
                lang={lang}
                ticket={flightOrderDetail.inbound}
              />
            </div>
          )}
        </div>

        <div>
          <Typography variant="h6" style={{ padding: '16px 0' }}>
            <FormattedMessage id="m.order.refundPolicy" />
          </Typography>

          <Typography variant="body2">
            <span dangerouslySetInnerHTML={{ __html: refundData.refundPolicy }} />
          </Typography>

          <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
            <NewTabLink href={refundData.moreInfoLink}>
              <Typography variant="body2">
                <FormattedMessage id="tour.booking.seeMore" />
              </Typography>
            </NewTabLink>
          </div>
        </div>

        <div style={{ marginTop: '20px' }}>
          <RadioGroup row>
            {!refundData.voidAllInfo && (
              <>
                <FormControlLabel
                  style={{ paddingRight: '64px' }}
                  value="outbound"
                  control={<Radio size="small" />}
                  checked={refundType === 'outbound'}
                  onChange={(e, checked) => checked && this.onChange('outbound')}
                  label={
                    <Typography variant="body2" color={refundType === 'outbound' ? 'secondary' : 'textSecondary'}>
                      <FormattedMessage id="m.order.refundOutbound" />
                    </Typography>
                  }
                />
                <FormControlLabel
                  style={{ paddingRight: '64px' }}
                  value="inbound"
                  control={<Radio />}
                  checked={refundType === 'inbound'}
                  onChange={(e, checked) => checked && this.onChange('inbound')}
                  label={
                    <Typography variant="body2" color={refundType === 'inbound' ? 'secondary' : 'textSecondary'}>
                      <FormattedMessage id="m.order.refundInbound" />
                    </Typography>
                  }
                />
              </>
            )}
            {refundData.voidAllInfo && refundData.voidAllInfo.voidable && (
              <FormControlLabel
                value="all"
                control={<Radio />}
                checked={refundType === 'all'}
                onChange={(e, checked) => checked && this.onChange('all')}
                label={
                  <Typography variant="body2" color={refundType === 'all' ? 'secondary' : 'textSecondary'}>
                    <FormattedMessage id="all" />
                  </Typography>
                }
              />
            )}
          </RadioGroup>
        </div>

        {refundObject && (
          <div style={{ maxWidth: '300px', marginTop: '8px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Typography variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="m.order.refundFee" />
              </Typography>

              <Typography variant="body2">
                <FormattedNumber value={refundObject.voidFee} /> <FormattedMessage id="currency" />
              </Typography>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Typography variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="m.order.moneyRefund" />
              </Typography>

              <Typography variant="body2">
                <FormattedNumber value={refundObject.ticketPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Typography variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="m.order.priceAfterRefund" />
              </Typography>

              <Typography variant="body2" color="secondary">
                <FormattedNumber value={refundObject.refundAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>
            <LoadingButton
              size="large"
              style={{ width: '170px', marginTop: '20px' }}
              variant="contained"
              color="secondary"
              disabled={!refundObject.refundAmount || loading}
              loading={loading}
              onClick={() => this.onRefund()}
            >
              <Typography variant="button">
                <FormattedMessage id="m.order.ticketBack" />
              </Typography>
            </LoadingButton>
          </div>
        )}

        <MessageDialog
          show={!!message}
          onClose={() => (refundSuccess ? this.backableToDetail() : this.setState({ message: '' }))}
          message={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                padding: '18px',
                textAlign: 'center',
              }}
            >
              <Typography variant="body1">{message}</Typography>
            </div>
          }
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(injectIntl(FlightBookingRefund));
