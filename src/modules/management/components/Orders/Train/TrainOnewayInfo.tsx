import React from 'react';
import { Typography } from '@material-ui/core';
import { FormattedDate, FormattedMessage } from 'react-intl';
import moment from 'moment';
import { some } from '../../../../../constants';
import { GREY } from '../../../../../colors';
import icTrain from '../../../../../svg/Train.svg';
import icTrainHead from '../../../../../svg/ic_train_head.svg';

interface Props {
  date: some;
  train: some;
}

export const TrainOnewayInfo: React.FC<Props> = props => {
  const { date, train } = props;
  const departureDate = moment(date);
  const dateDiff = moment(train.arriveDate).diff(train.pickupDate, 'days');
  return (
    <div style={{ flex: 1, display: 'flex', marginTop: '10px' }}>
      <div
        style={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <img src={icTrain} alt="" style={{ width: '32px' }} />
        <Typography variant="caption" style={{ marginTop: '8px' }}>
          <FormattedMessage id="train" />
          &nbsp;
          {train.tripCode}
        </Typography>
      </div>

      <div style={{ padding: '0px 20px', flex: 1 }}>
        <Typography variant="body2">
          <FormattedDate value={departureDate.toDate()} weekday="short" />, &nbsp;
          {departureDate.format('DD/MM/YYYY')}
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="subtitle1">{train.departureStationName}</Typography>
            <Typography variant="body2">{train.departureHour}</Typography>
          </div>
          <div style={{ flex: 1 }}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                alignItems: 'center',
              }}
            >
              <div
                style={{ display: 'flex', alignItems: 'center', width: '150px', height: '35px' }}
              >
                <div style={{ flex: 1 }}>
                  <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
                </div>
                <img src={icTrainHead} alt="" />
                <div style={{ flex: 1 }}>
                  <div style={{ borderBottom: `2px solid ${GREY}`, margin: '0 8px' }} />
                </div>
              </div>
              <Typography variant="body2" color="textSecondary">
                {train.duration}
              </Typography>
            </div>
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'end' }}>
            <Typography variant="subtitle1">{train.arrivedStationName}</Typography>
            <Typography variant="body2">
              {train.arriveHour}
              {dateDiff && (
                <span>
                  <span> (+{dateDiff}d)</span>
                </span>
              )}
            </Typography>
          </div>
        </div>
      </div>
    </div>
  );
};
