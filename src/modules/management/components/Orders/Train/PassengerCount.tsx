import { countBy } from 'lodash';
import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { some } from '../../../../../constants';
import { AllTrainCustomersGroup } from '../../../constants';

interface IPassengerCount extends WrappedComponentProps {
  data: some;
}

const PassengerCount: React.FC<IPassengerCount> = props => {
  const { data, intl } = props;
  const passengers: string[] = [];

  const countPax = countBy(data, o => {
    return o.customerGroupId;
  });

  AllTrainCustomersGroup.forEach(item => {
    if (countPax[item.customerGroupId]) {
      passengers.push(
        `${countPax[item.customerGroupId]} ${intl
          .formatMessage({ id: item.ageGroupName })
          .toLowerCase()}`,
      );
    }
  });

  return <span>{passengers.join(', ')}</span>;
};

export default injectIntl(PassengerCount);
