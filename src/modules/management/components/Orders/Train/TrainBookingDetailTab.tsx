import { Button, ButtonBase, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../API';
import { BLUE, GREY } from '../../../../../colors';
import { SLASH_DATE_FORMAT, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import TrainTicketDetailDialog from '../../../../booking/components/train/TrainTicketDetailDialog';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../../common/redux/thunks';
import { BOOKING_ID_PARAM_NAME, getStatusColor } from '../../../constants';
import { TrainOnewayInfo } from './TrainOnewayInfo';

const DetailTab = styled.div`
  display: flex;
  flex-direction: column;
`;

const Line = styled.div`
  min-height: 36px;
  display: flex;
  align-items: center;
  width: 100%;
`;

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

interface IOrdersProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onDeactivate(id: number): void;
  breadcrumb: React.ReactNode;
}

const TrainBookingDetailTab: React.FunctionComponent<IOrdersProps> = props => {
  const { breadcrumb, dispatch, search } = props;
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [departurePrice, setDeparturePrice] = React.useState(0);
  const [returnPrice, setReturnPrice] = React.useState(0);
  const [isTwoWay, setIsTwoWay] = React.useState(false);
  const [openDetailDialog, setOpenDetailDialog] = React.useState(false);
  const [ticketDetailTab, setTicketDetailTab] = React.useState(0);
  const intl = useIntl();

  React.useEffect(() => {
    const params = new URLSearchParams(search);
    const trainBookingId = parseInt(params.get(BOOKING_ID_PARAM_NAME) || '0', 10) || 0;
    const fetch = async () => {
      const json = await dispatch(fetchThunk(`${API_PATHS.getTrainBookingDetail}?id=${trainBookingId}`, 'get', true));
      if (json.code === 200) {
        setData(json.data);
        setDeparturePrice(
          parseInt(
            json.data.paxListInDepartureTrain.reduce(
              (accumulator: number, currentValue: any) => accumulator + currentValue.totalFinalPrice,
              0,
            ) || '0',
            10,
          ),
        );
        setReturnPrice(
          json.data.returnDate &&
            json.data.paxListInReturnTrain.reduce(
              (accumulator: number, currentValue: any) => accumulator + currentValue.totalFinalPrice,
              0,
            ),
        );
        setIsTwoWay(json.data.returnDate && json.data.trains.length > 1);
      }
    };
    fetch();
  }, [dispatch, setData, search]);

  return (
    <DetailTab>
      {data ? (
        <div>
          <Helmet>
            <title>{intl.formatMessage({ id: 'trainTickets' })}</title>
          </Helmet>
          {breadcrumb}
          <Typography variant="h5" style={{ paddingTop: '16px' }}>
            <FormattedMessage id="m.orderDetail" />
          </Typography>
          <Line style={{ marginTop: '20px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.train.ticketInfo" />
              &nbsp;-&nbsp;
            </Typography>
            <ButtonBase
              onClick={() => {
                setTicketDetailTab(0);
                setOpenDetailDialog(true);
              }}
            >
              <Typography variant="body1" style={{ color: BLUE }}>
                <FormattedMessage id="ticketAlert.seeDetail" />
              </Typography>
            </ButtonBase>
          </Line>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              position: 'relative',
            }}
          >
            <TrainOnewayInfo date={data.departureDate} train={data.trains[0]} />
            <div style={{ flex: 1 }}>
              {isTwoWay && <TrainOnewayInfo date={data.returnDate} train={data.trains[1]} />}
            </div>
          </div>

          <div
            style={{
              display: 'flex',
              marginTop: '10px',
              padding: '20px 60px 20px 20px',
              background: 'white',
              border: `1px solid ${GREY}`,
            }}
          >
            <div style={{ flex: 1 }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="m.orderInfomations" />
              </Typography>
              <Line style={{ marginTop: '10px' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.status" />
                  :&nbsp;
                </Typography>
                <Typography
                  variant="body2"
                  style={{
                    color: getStatusColor(data.paymentStatusCode),
                  }}
                >
                  <FormattedMessage id={data.paymentStatusCode} />
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.bookingDate" />
                  :&nbsp;
                  {moment(data.createdDate).format(SLASH_DATE_FORMAT)}
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.orderCode" />
                  :&nbsp;
                  {data.bookId}
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.order.bookingCode" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: BLUE }}>
                  {data.bookingStatus === 'success' ? data.bookCode : <FormattedMessage id="m.noBookingCode" />}
                </Typography>
              </Line>
              <Typography variant="subtitle2" style={{ paddingTop: '24px' }}>
                <FormattedMessage id="booking.bookerInfo" />
              </Typography>
              <Line style={{ marginTop: '10px' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.fullName" />
                  :&nbsp;
                  {data.bookerCustomer.fullName}
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="telephone" />
                  :&nbsp;
                  {data.bookerCustomer.mobile}
                </Typography>
              </Line>

              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="m.email" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ color: BLUE }}>
                  {data.bookerCustomer.email}
                </Typography>
              </Line>
              <Typography variant="subtitle2" style={{ paddingTop: '24px' }}>
                <FormattedMessage id="contactInfo" />
              </Typography>
              <Line style={{ marginTop: '10px' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.fullName" />
                  :&nbsp;
                  {data.fullName}
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="telephone" />
                  :&nbsp;
                  {data.mobile}
                </Typography>
              </Line>
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="address" />
                  :&nbsp;
                  {data.address}
                </Typography>
              </Line>
              <Typography variant="subtitle2" style={{ paddingTop: '24px' }}>
                <FormattedMessage id="booking.travellersList" />
              </Typography>
              <Button
                variant="outlined"
                size="large"
                color="secondary"
                style={{ marginTop: '12px' }}
                onClick={() => {
                  setTicketDetailTab(1);
                  setOpenDetailDialog(true);
                }}
              >
                <Typography>
                  <FormattedMessage id="booking.seeDetails" />
                </Typography>
              </Button>
            </div>
            <div style={{ flex: 1 }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="m.paymentDetail" />
              </Typography>
              <Line style={{ marginTop: '10px', justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.orders.train.departurePrice" />
                </Typography>
                <Typography variant="body2">
                  <FormattedNumber value={departurePrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
              {isTwoWay && (
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.orders.train.returnPrice" />
                  </Typography>
                  <Typography variant="body2">
                    <FormattedNumber value={returnPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
              )}
              {data.paymentFee && (
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.paymentFee" />
                  </Typography>
                  <Typography variant="body2">
                    <FormattedNumber value={data.amountPay * (data.paymentFee / 100)} />
                  </Typography>
                </Line>
              )}
              {data.paymentMethod && (
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.paymentMethod" />
                  </Typography>
                  <Typography variant="body2">{data.paymentMethod.name}</Typography>
                </Line>
              )}
              {data.paymentStatus && (
                <Line style={{ justifyContent: 'space-between' }}>
                  <Typography variant="body2">
                    <FormattedMessage id="m.orders.train.paymentStatus" />
                  </Typography>
                  <Typography variant="body2">{data.paymentStatus}</Typography>
                </Line>
              )}
              <Line style={{ justifyContent: 'space-between' }}>
                <Typography variant="body2">
                  <FormattedMessage id="m.orders.train.totalPrice" />
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={data.totalAmountPay} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
              {data.bookingStatus === 'holding' && (
                <Button
                  style={{
                    padding: '1px 0',
                    marginTop: '20px',
                    width: '160px',
                    height: '30px',
                    boxShadow: 'none',
                  }}
                  color="secondary"
                  variant="contained"
                  size="small"
                >
                  <FormattedMessage id="m.pay" />
                </Button>
              )}
            </div>
          </div>
          {/* {!noAction && <HotelBookingAction onDeactivate={props.onDeactivate} data={data} />} */}
        </div>
      ) : (
        <LoadingIcon
          style={{
            height: '250px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      )}
      {data && (
        <TrainTicketDetailDialog
          open={openDetailDialog}
          tabIndex={ticketDetailTab}
          onClose={() => setOpenDetailDialog(false)}
          ticketInfo={data.trains}
          departureData={data.paxListInDepartureTrain}
          returnData={data.paxListInReturnTrain}
        />
      )}
    </DetailTab>
  );
};

export default connect(mapState2Props)(TrainBookingDetailTab);
