import { Button, Divider, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, GREY } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import Link from '../../../../common/components/Link';
import { ACTION_PARAM_NAME, BOOKING_ID_PARAM_NAME, getStatusColor } from '../../../constants';
import PassengerCount from './PassengerCount';
import { TrainOnewayInfo } from './TrainOnewayInfo';

const Wrapper = styled.div`
  background: #ffffff;
  border: 1px solid ${GREY};
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14);
`;

interface IHotelOrderItemProps extends ReturnType<typeof mapState2Props> {
  data: some;
}
function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

const seeDetail = (id: number, search: string) => {
  const params = new URLSearchParams(search);
  params.set(BOOKING_ID_PARAM_NAME, `${id}`);
  params.set(ACTION_PARAM_NAME, 'detail');
  return params.toString();
};

const TrainBookingItem: React.FunctionComponent<IHotelOrderItemProps> = props => {
  const { data, search } = props;

  return (
    <Wrapper style={{ marginBottom: '10px', display: 'flex' }}>
      <div
        style={{
          width: '50%',
          display: 'flex',
          flexDirection: 'column',
          paddingBottom: '16px',
          paddingLeft: '30px',
        }}
      >
        <TrainOnewayInfo date={data.departureDate} train={data.trains[0]} />
        {data.returnDate && (
          <div>
            <Divider style={{ marginTop: '16px' }} />

            <TrainOnewayInfo date={data.return} train={data.trains[1]} />
          </div>
        )}
      </div>
      <div style={{ width: '50%', padding: '10px 20px 10px 30px', display: 'flex' }}>
        <div>
          <Typography variant="body2">
            <FormattedMessage id="m.order.status" />
            :&nbsp;
            <span
              style={{
                color: `${getStatusColor(data.paymentStatusCode)}`,
              }}
            >
              {!!data.paymentStatusCode && <FormattedMessage id={data.paymentStatusCode} />}
            </span>
          </Typography>
          <div style={{ display: 'flex', alignItems: 'center', paddingTop: '8px' }}>
            <Typography variant="body2">
              <FormattedMessage id="m.order.bookingCode" />
              :&nbsp;
            </Typography>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.paymentStatusCode === 'success' ? data.bookCode : <FormattedMessage id="m.noBookingCode" />}
            </Typography>
          </div>

          <Typography variant="body2">
            <FormattedMessage id="m.order.customer" />: {data.bookerCustomer ? data.bookerCustomer.fullName : ''}
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.order.bookingDate" />: {data.createdDateFormat}
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="m.order.passengerCount" />
            :&nbsp;
            <PassengerCount data={data.paxListInDepartureTrain} />
          </Typography>
        </div>
        <div style={{ display: 'flex', flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
          <Typography variant="body2">
            <FormattedMessage id="m.order.totalPayable" />
          </Typography>
          <Typography variant="subtitle2" color="secondary">
            <FormattedNumber value={data.totalAmountPay} />
            <FormattedMessage id="currency" />
          </Typography>

          {data.bookId && (
            <div style={{ paddingTop: '12px' }}>
              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${seeDetail(data.dataBookTicketId, search)}`,
                  state: { backableToList: true },
                }}
              >
                <Button variant="contained" color="secondary" style={{ width: '190px' }}>
                  <FormattedMessage id="m.viewDetail" />
                </Button>
              </Link>
            </div>
          )}
        </div>
      </div>
    </Wrapper>
  );
};

export const TrainBookingItemSkeleton: React.FunctionComponent = props => {
  return (
    <Wrapper style={{ marginBottom: '10px', height: '175px', padding: '10px 20px 20px' }}>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '120px', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <Skeleton width="40px" height="40px" variant="circle" style={{ margin: '3px 0' }} />
          <Skeleton width="95px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="50px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="115px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '20px' }}>
          <Skeleton width="120px" variant="text" />
          <Skeleton width="210px" height="60px" variant="rect" style={{ marginTop: '6px' }} />
        </div>

        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '30px' }}>
          <Skeleton width="160px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="180px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="200px" height="12px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        </div>

        <div style={{ display: 'flex', flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
          <Skeleton width="130px" variant="text" style={{ margin: '3px 0' }} />
          <Skeleton width="90px" variant="text" style={{ margin: '3px 0' }} />

          <div style={{ paddingTop: '12px' }}>
            <Skeleton width="190px" height="30px" variant="rect" style={{ margin: '3px 0' }} />
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default connect(mapState2Props)(TrainBookingItem);
