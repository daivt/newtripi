import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../API';
import { BLUE } from '../../../../../colors';
import { some, PAGE_SIZE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunks';
import { HotelManagementBookingItemActionType, ACTION_PARAM_NAME } from '../../../constants';
import OrderFilters, { OrderFiltersParams } from '../OrderFilters';
import ic_noResult from '../../../../../svg/ic_contact_no_result.svg';
import TrainBookingItem, { TrainBookingItemSkeleton } from './TrainBookingItem';
import TrainBookingDetailTab from './TrainBookingDetailTab';
import { find } from 'lodash';
import TrainDetailBreadscrumbs from './TrainDetailBreadscrumbs';

interface IHotelProps extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

function mapState2Props(state: AppState) {
  return {
    search: state.router.location.search,
  };
}

const Hotel: React.FunctionComponent<IHotelProps> = props => {
  const { dispatch } = props;
  const [data, setData] = React.useState<some[] | undefined>(undefined);
  const [page, setPage] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [total, setTotal] = React.useState(0);
  const [filters, setFilters] = React.useState<OrderFiltersParams>({});

  const [deactivateId, setDeactivateId] = React.useState(0);

  const params = new URLSearchParams(props.search);
  const action = params.get(ACTION_PARAM_NAME) as HotelManagementBookingItemActionType | null;
  const bookingId = parseInt(params.get('bookingId') || '0', 10) || 0;
  const findData = find(data, o => {
    return o.dataBookTicketId === bookingId;
  });

  const fetchMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getTrainBookings}`,
        'post',
        true,
        JSON.stringify({ filters, paging: { page: page + 1, size: PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setData(b => b && b.concat(json.data.history));
      setPage(page + 1);
    }
    setLoading(false);
  }, [page, dispatch, filters]);

  React.useEffect(() => {
    const fetch = async () => {
      setData([]);
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getTrainBookings}`,
          'post',
          true,
          JSON.stringify({
            filters,
            paging: { page: 1, size: PAGE_SIZE },
          }),
        ),
      );
      if (json.code === 200) {
        setPage(1);
        setData(json.data.history);
        setTotal(json.data.total);
      }
      setLoading(false);
    };
    fetch();
  }, [filters, dispatch, deactivateId]);

  return (
    <div>
      <div style={{ display: action ? 'none' : undefined }}>
        <OrderFilters
          loading={loading}
          onChange={(filters: OrderFiltersParams) => {
            setFilters(filters);
          }}
        />
        <div>
          {data ? (
            <>
              {data.map(one => (
                <TrainBookingItem key={one.dataBookTicketId} data={one} />
              ))}
              {!loading && total - PAGE_SIZE * page > 0 && (
                <div style={{ textAlign: 'center', marginTop: '23px' }}>
                  <Button onClick={fetchMore}>
                    <Typography style={{ color: BLUE }}>
                      <FormattedMessage
                        id="result.displayMore"
                        values={{ num: total - PAGE_SIZE * page }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
              {loading && (
                <>
                  <TrainBookingItemSkeleton />
                  <TrainBookingItemSkeleton />
                  <TrainBookingItemSkeleton />
                </>
              )}
              {!data.length && !loading && (
                <div style={{ textAlign: 'center', marginTop: '48px' }}>
                  <img src={ic_noResult} alt="" />
                  <Typography variant="body1" style={{ marginTop: '16px' }}>
                    <FormattedMessage id="m.noData" />
                  </Typography>
                </div>
              )}
            </>
          ) : (
            <>
              <TrainBookingItemSkeleton />
              <TrainBookingItemSkeleton />
              <TrainBookingItemSkeleton />
              <TrainBookingItemSkeleton />
              <TrainBookingItemSkeleton />
            </>
          )}
        </div>
      </div>
      {action === 'detail' && findData && (
        <TrainBookingDetailTab
          breadcrumb={<TrainDetailBreadscrumbs />}
          onDeactivate={id => setDeactivateId(id)}
        />
      )}
    </div>
  );
};

export default connect(mapState2Props)(Hotel);
