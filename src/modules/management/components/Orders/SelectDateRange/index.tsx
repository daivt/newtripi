import { Button, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../../colors';
import { DATE_FORMAT } from '../../../../../constants';
import calendarSvg from '../../../../../svg/calendar.svg';
import { renderMonthText } from '../../../../common/utils';
import styles from './styles.module.scss';
const FULL_WIDTH = '100%';
const MIN_WIDTH = '150px';

const InputContainer = styled.div`
  position: relative;
  display: flex;
  height: 42px;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  background-color: white;
  color: ${BLACK_TEXT};
`;
interface Props {
  minimizedWidth?: string;
  onChange(checkInDate?: string, checkOutDate?: string): void;
  onDelete?: () => void;
  checkInDate?: string;
  checkOutDate?: string;
  error?: boolean;
  header?: string;
  placeholder?: string;
  isOutsideRange?: (date: any) => boolean;
}

interface State {
  isFocused: boolean;
  focusedInput: FocusedInputShape;
  checkInDate?: string;
  checkOutDate?: string;
}

class SelectDateRange extends PureComponent<Props, State> {
  state: State = {
    isFocused: false,
    focusedInput: 'startDate' as FocusedInputShape,
    checkInDate: this.props.checkInDate,
    checkOutDate: this.props.checkOutDate,
  };

  parent = React.createRef<HTMLDivElement>();
  firefoxWorkaroundHelper = React.createRef<HTMLButtonElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }
    const checkInDate = this.state.checkInDate;
    const checkOutDate = this.state.checkOutDate;
    if (checkInDate !== this.props.checkInDate || checkOutDate !== this.props.checkOutDate) {
      this.props.onChange(checkInDate, checkOutDate);
    }
    this.setState({ checkInDate, checkOutDate, isFocused: false });
  };

  render() {
    const { isFocused } = this.state;
    const { minimizedWidth, header, placeholder, isOutsideRange } = this.props;
    const { checkInDate, checkOutDate } = this.state;
    return (
      <div
        style={{
          maxWidth: minimizedWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          position: 'relative',
          color: isFocused ? BLACK_TEXT : undefined,
          flex: 1,
          outline: 'none',
        }}
      >
        {header && (
          <div style={{ marginLeft: '12px', marginBottom: '4px' }}>
            <Typography variant="body2">
              <FormattedMessage id={header} />
            </Typography>
          </div>
        )}
        <div
          onBlur={this.onBlur}
          tabIndex={-1}
          style={{ outline: 'none' }}
          onFocus={e => {
            e.target !== this.parent.current &&
              this.firefoxWorkaroundHelper.current &&
              this.firefoxWorkaroundHelper.current.focus();
            this.setState({ isFocused: true });
          }}
        >
          <button
            onFocus={e => e.stopPropagation()}
            ref={this.firefoxWorkaroundHelper}
            style={{ position: 'absolute', opacity: 0, zIndex: -1 }}
          ></button>
          <InputContainer>
            <div
              style={{ display: 'flex', outline: 'none', cursor: 'pointer', flex: 1 }}
              tabIndex={-1}
              onFocus={() => this.setState({ isFocused: true })}
              ref={this.parent}
            >
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
                <img alt="" src={calendarSvg} />
              </div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  wordBreak: 'break-word',
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  color: placeholder && !checkInDate && !checkOutDate ? GREY : undefined,
                }}
              >
                <Typography variant="body2">
                  {checkInDate && checkInDate.replace(/-/gm, '/')}
                  {checkOutDate && (
                    <>
                      &nbsp;&nbsp;{'-'}&nbsp;&nbsp;
                      {checkOutDate.replace(/-/gm, '/')}
                    </>
                  )}
                  {placeholder && !checkInDate && !checkOutDate && <FormattedMessage id={placeholder} />}
                </Typography>
              </div>
            </div>
            {(checkInDate || checkOutDate) && (
              <div>
                <IconButton
                  color="default"
                  size="small"
                  style={{
                    display: !checkInDate && !checkOutDate ? 'none' : undefined,
                  }}
                  onFocus={e => {
                    e.stopPropagation();
                  }}
                  onClick={() => {
                    this.setState({
                      checkInDate: undefined,
                      checkOutDate: undefined,
                      focusedInput: 'startDate',
                    });
                    this.props.onChange(undefined, undefined);
                  }}
                >
                  <CloseIcon style={{ width: '20px', height: '20px' }} />
                </IconButton>
              </div>
            )}
          </InputContainer>
          <CSSTransition
            timeout={200}
            in={isFocused}
            unmountOnExit
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
          >
            <div
              style={{
                transition: 'all 300ms ease',
                backgroundColor: 'white',
                position: 'absolute',
                boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
                zIndex: 10,
              }}
            >
              <DayPickerRangeController
                renderMonthText={renderMonthText}
                hideKeyboardShortcutsPanel
                daySize={36}
                numberOfMonths={2}
                startDate={checkInDate ? moment(checkInDate, DATE_FORMAT) : null}
                endDate={checkOutDate ? moment(checkOutDate, DATE_FORMAT) : null}
                focusedInput={this.state.focusedInput}
                onDatesChange={({ startDate, endDate }) => {
                  this.setState({
                    checkInDate: startDate ? startDate.format(DATE_FORMAT) : undefined,
                    checkOutDate: endDate ? endDate.format(DATE_FORMAT) : undefined,
                  });
                }}
                onFocusChange={focusedInput =>
                  this.setState({
                    focusedInput: focusedInput || 'startDate',
                  })
                }
                isOutsideRange={isOutsideRange}
                minimumNights={1}
              />
              <div
                style={{
                  textAlign: 'end',
                }}
              >
                <Button
                  style={{
                    textTransform: 'none',
                    marginTop: '8px',
                    marginRight: '12px',
                    marginBottom: '8px',
                  }}
                  size="large"
                  color="secondary"
                  variant="contained"
                  onClick={() => this.parent.current && this.parent.current.blur()}
                >
                  <FormattedMessage id="done" />
                </Button>
              </div>
            </div>
          </CSSTransition>
        </div>
      </div>
    );
  }
}

export default SelectDateRange;
