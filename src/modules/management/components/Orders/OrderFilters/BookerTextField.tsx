import { IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../../colors';
const MIN_WIDTH = '150px';

const InputContainer = styled.div`
  position: relative;
  display: flex;
  height: 42px;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  background-color: white;
  color: ${BLACK_TEXT};
`;
interface Props {
  header?: string;
  text?: string;
  placeholder?: string;
  onClick: () => void;
  onDelete: () => void;
}

class BookerTextField extends PureComponent<Props> {
  render() {
    const { header, text, onClick, onDelete, placeholder } = this.props;
    return (
      <div
        style={{
          minWidth: MIN_WIDTH,
          position: 'relative',
          flex: 1,
          outline: 'none',
        }}
      >
        {header && (
          <div style={{ marginLeft: '12px', marginBottom: '4px' }}>
            <Typography variant="body2">
              <FormattedMessage id={header} />
            </Typography>
          </div>
        )}
        <InputContainer>
          <div
            style={{
              display: 'flex',
              height: '24px',
              margin: '0px 10px',
              paddingRight: '5px',
              wordBreak: 'break-word',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
              color: !text ? GREY : undefined,
              flex: 1,
            }}
            onClick={onClick}
          >
            <Typography variant="body2">{!!text ? text : placeholder}</Typography>
          </div>
          {text && (
            <div style={{ textAlign: 'end' }}>
              <IconButton color="default" size="small" onClick={onDelete}>
                <CloseIcon style={{ width: '20px', height: '20px' }} />
              </IconButton>
            </div>
          )}
        </InputContainer>
      </div>
    );
  }
}

export default BookerTextField;
