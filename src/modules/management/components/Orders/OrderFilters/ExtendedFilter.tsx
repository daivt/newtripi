import { Grid } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { OrderFiltersParams } from '.';
import { ServiceType } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { FreeTextField } from '../../../../booking/components/Form';
import LoadingButton from '../../../../common/components/LoadingButton';
import SelectContactDialog from '../../../../common/components/SelectContactDialog';
import { validTelephoneRegex } from '../../../../common/utils';
import { TAB_PARAM_NAME } from '../../../constants';
import SelectDateRange from '../SelectDateRange';
import BookerTextField from './BookerTextField';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  filters: OrderFiltersParams;
  onChange: (params: OrderFiltersParams) => void;
  loading: boolean;
}

const ExtendedFilter: React.FunctionComponent<Props> = props => {
  const { filters, onChange, intl, loading, router } = props;
  const [params, setParams] = React.useState<OrderFiltersParams>(filters);
  const [bookerName, setBookerName] = React.useState<string>('');
  const [isSelectContact, setIsSelectContact] = React.useState<boolean>(false);
  const { search } = router.location;
  const paramsUrl = new URLSearchParams(search);
  const module = paramsUrl.get(TAB_PARAM_NAME) as ServiceType | null;
  return (
    <div>
      <div style={{ marginBottom: '20px', display: 'flex' }}>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <FreeTextField
              style={{ marginRight: '0px' }}
              optional
              header={<FormattedMessage id="m.orders.tour.bookingCode" />}
              placeholder={intl.formatMessage({ id: 'm.orders.tour.bookingCodePH' })}
              text={params.bookingCode || ''}
              update={bookingCode => {
                setParams({ ...params, bookingCode });
              }}
              inputStyle={{ background: 'white' }}
            />
          </Grid>
          <Grid item xs={4}>
            <FreeTextField
              style={{ marginRight: '0px' }}
              optional
              header={<FormattedMessage id="m.orders.tour.bookingId" />}
              placeholder={intl.formatMessage({ id: 'm.orders.tour.bookingIdPH' })}
              text={params.bookingId || ''}
              regex={validTelephoneRegex}
              update={bookingId => {
                setParams({ ...params, bookingId });
              }}
              inputStyle={{ background: 'white' }}
            />
          </Grid>
          <Grid item xs={4}>
            <BookerTextField
              header="m.orders.tour.bookerContactId"
              placeholder={intl.formatMessage({ id: 'm.orders.tour.bookerContactIdPH' })}
              onClick={() => {
                setIsSelectContact(true);
              }}
              onDelete={() => {
                setParams({ ...params, bookerContactId: undefined });
                setBookerName('');
              }}
              text={bookerName}
            />
          </Grid>
          <Grid item xs={4}>
            <SelectDateRange
              checkInDate={params.departureFromDate}
              checkOutDate={params.departureToDate}
              onChange={(checkInDate: string, checkOutDate: string) => {
                setParams({
                  ...params,
                  departureFromDate: checkInDate,
                  departureToDate: checkOutDate,
                });
              }}
              header={module !== 'hotel' ? 'm.orders.departureDate' : 'm.orders.checkIn'}
              placeholder="m.orders.aboutDay"
            />
          </Grid>
          <Grid item xs={4}>
            <SelectDateRange
              checkInDate={params.createdFromDate}
              checkOutDate={params.createdToDate}
              onChange={(checkInDate: string, checkOutDate: string) => {
                setParams({ ...params, createdFromDate: checkInDate, createdToDate: checkOutDate });
              }}
              header={module !== 'hotel' ? 'm.orders.createDate' : 'm.orders.createDate'}
              placeholder="m.orders.aboutDay"
              isOutsideRange={date =>
                moment()
                  .add(1, 'days')
                  .startOf('day')
                  .isSameOrBefore(date)
              }
            />
          </Grid>
          <Grid item xs={4}>
            <LoadingButton
              loading={loading}
              variant="contained"
              color="primary"
              style={{ minWidth: '100px', marginTop: '28px' }}
              onClick={() => onChange(params)}
            >
              <FormattedMessage id="apply" />
            </LoadingButton>
          </Grid>
        </Grid>
      </div>
      <SelectContactDialog
        show={isSelectContact}
        onClose={() => setIsSelectContact(false)}
        onSelect={customer => {
          setParams({
            ...params,
            bookerContactId: customer.id,
          });
          setBookerName(`${customer.firstName} ${customer.lastName}`);
          setIsSelectContact(false);
        }}
      />
    </div>
  );
};
function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}
export default connect(mapStateToProps)(injectIntl(ExtendedFilter));
