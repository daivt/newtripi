import { Button, Collapse, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../colors';
import { some } from '../../../../../constants';
import { CheckButton } from '../../../../common/components/elements';
import ExtendedFilter from './ExtendedFilter';

const FILTER = [
  { id: 'default', value: undefined },
  { id: 'all', value: ['all'] },
  { id: 'm.orderStatus.holding', value: ['holding'] },
  { id: 'm.orderStatus.success', value: ['completed', 'success'] },
];
export interface OrderFiltersParams {
  paymentStatuses?: ReadonlyArray<string>;
  createdFromDate?: string;
  createdToDate?: string;
  departureFromDate?: string;
  departureToDate?: string;
  bookingId?: string;
  bookingCode?: string;
  bookerContactId?: number;
}
interface Props {
  onChange: (params: OrderFiltersParams) => void;
  loading: boolean;
}

const OrderFilters: React.FunctionComponent<Props> = props => {
  const { onChange, loading } = props;
  const [open, setOpen] = React.useState<boolean>(false);
  const [params, setParams] = React.useState<OrderFiltersParams>({});

  const setPaymentStatuses = React.useCallback<(obj: some, value: OrderFiltersParams) => void>(
    (obj: some, value: OrderFiltersParams) => {
      let paymentStatuses: string[] | undefined;
      if (obj.id === 'all' || obj.id === 'default') {
        paymentStatuses = obj.value;
      } else if (value.paymentStatuses) {
        paymentStatuses = value.paymentStatuses.filter(x => !obj.value.includes(x) && x !== 'all' && x !== 'default');
        paymentStatuses =
          paymentStatuses.join(',') === value.paymentStatuses.join(',')
            ? paymentStatuses.concat(obj.value)
            : paymentStatuses.length > 0
            ? paymentStatuses
            : obj.value;
      } else {
        paymentStatuses = obj.value;
      }
      const newParams = { ...value, paymentStatuses };
      setParams(newParams);
      onChange(newParams);
    },
    [onChange],
  );
  return (
    <div>
      <div style={{ display: 'flex', alignItems: 'center', padding: '20px 0' }}>
        <Typography variant="body2" style={{ marginRight: '9px' }}>
          <FormattedMessage id="m.orderStatus" />
        </Typography>
        {FILTER.map(obj => (
          <CheckButton
            loadingColor="primary"
            loading={loading}
            key={obj.id}
            active={
              params.paymentStatuses && obj.value
                ? params.paymentStatuses.join(',').includes(obj.value.join(','))
                : !!(!params.paymentStatuses && !obj.value)
            }
            style={{ marginRight: '12px' }}
            onClick={() => setPaymentStatuses(obj, params)}
          >
            <Typography variant="body2">
              <FormattedMessage id={obj.id} />
            </Typography>
          </CheckButton>
        ))}
        <div style={{ textAlign: 'end', flex: 1 }}>
          <Button onClick={() => setOpen(!open)}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="body2" style={{ color: BLUE }}>
                <FormattedMessage id="m.orderFilters" />
                <span
                  style={{
                    display: 'inline-block',
                    marginLeft: '6px',
                    fontSize: '10px',
                    transition: 'all 300ms',
                    transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
                    cursor: 'pointer',
                  }}
                >
                  &#9650;
                </span>
              </Typography>
            </div>
          </Button>
        </div>
      </div>
      <Collapse in={open} unmountOnExit>
        <ExtendedFilter
          loading={loading}
          filters={params}
          onChange={obj => {
            const newParams = { ...obj, paymentStatuses: params.paymentStatuses };
            setParams(newParams);
            onChange(newParams);
          }}
        />
      </Collapse>
    </div>
  );
};
export default OrderFilters;
