import { Button, IconButton, InputAdornment, Typography } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import IconClose from '@material-ui/icons/Clear';
import { debounce } from 'lodash';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../../API';
import { GREY } from '../../../../../../colors';
import { PAGE_SIZE, some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import icSearch from '../../../../../../svg/ic_search.svg';
import { FreeTextField } from '../../../../../booking/components/Form';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import NoDataResult from '../../../../../common/components/NoDataResult';
import { fetchThunk } from '../../../../../common/redux/thunks';

const ScrollBar = styled.div`
  overflow: auto;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border: 8px solid white;
  }
  &::-webkit-scrollbar {
    background: white;
    width: 24px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;
interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  open: boolean;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  onClose(): void;
  onSelect(data: some): void;
}

const ExportInvoiceHistoryDialog: React.FunctionComponent<Props> = props => {
  const { open, dispatch, intl, onClose, onSelect } = props;
  const [data, setData] = React.useState<some[] | undefined>();
  const [searchTerm, setSearch] = React.useState('');
  const [total, setTotal] = React.useState(0);
  const onChangeInput = React.useCallback(
    debounce(
      (searchTerm: string) => {
        addData(1, searchTerm);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );
  const inputChange = React.useCallback(
    (text: string) => {
      setSearch(text);
      onChangeInput(text);
    },
    [onChangeInput],
  );

  const addData = React.useCallback(
    async (page: number = 1, searchTerm: string) => {
      if (page === 1) {
        setData(undefined);
      }
      const json = await dispatch(
        fetchThunk(
          API_PATHS.getInvoiceInfoHistories,
          'post',
          true,
          JSON.stringify({ page, searchTerm, size: PAGE_SIZE }),
        ),
      );
      if (json.code === 200) {
        setData(data => (data ? data.concat(json.data.infoList) : json.data.infoList));
        setTotal(json.data.total);
      }
    },
    [dispatch],
  );
  React.useEffect(() => {
    addData(1, '');
  }, [addData]);
  return (
    <Dialog
      open={open}
      keepMounted={false}
      maxWidth="lg"
      PaperProps={{ style: { width: '650px', height: '600px' } }}
    >
      <IconButton
        style={{ padding: '8px', position: 'absolute', top: 0, right: 0 }}
        size="small"
        onClick={onClose}
      >
        <IconClose />
      </IconButton>
      <div style={{ padding: '20px 30px' }}>
        <div style={{ textAlign: 'center' }}>
          <Typography variant="h5">
            <FormattedMessage id="m.invoice.companyList" />
          </Typography>
        </div>
        <div>
          <FreeTextField
            inputStyle={{ background: 'white' }}
            text={searchTerm}
            update={inputChange}
            placeholder={intl.formatMessage({ id: 'm.invoice.initPL' })}
            header={<></>}
            optional
            style={{ margin: '0px' }}
            endAdornment={
              <InputAdornment position="end">
                <img
                  src={icSearch}
                  alt=""
                  style={{
                    filter: 'brightness(1) invert(1)',
                    width: '24px',
                    height: '24px',
                    color: 'black',
                    margin: '8px',
                  }}
                />
              </InputAdornment>
            }
          />{' '}
        </div>
      </div>
      <ScrollBar
        style={{
          flex: 1,
          padding: '10px 30px 20px 30px',
        }}
      >
        <InfiniteScroll
          pageStart={1}
          initialLoad={false}
          loadMore={page => addData(page, searchTerm)}
          hasMore={data ? data.length < total : true}
          loader={
            <LoadingIcon
              key="loader"
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            />
          }
          useWindow={false}
        >
          {data && (
            <>
              {data.length > 0 ? (
                data.map((v: some) => (
                  <div
                    key={v.bookingId}
                    style={{
                      borderTop: `1px solid ${GREY}`,
                      borderBottom: `1px solid ${GREY}`,
                      padding: '12px 0px',
                      display: 'flex',
                      justifyContent: 'space-between',
                    }}
                  >
                    <div style={{ flex: 1 }}>
                      <Typography variant="subtitle2">{v.companyName}</Typography>
                      <Typography variant="body2" style={{ marginTop: '12px' }}>
                        <FormattedMessage id="m.invoice.taxNumber" />: &nbsp;{v.taxIdNumber}
                      </Typography>
                      <Typography variant="body2">
                        <FormattedMessage id="m.invoice.address" />: &nbsp;{v.companyAddress}
                      </Typography>
                      <Typography variant="body2">
                        <FormattedMessage id="m.invoice.recipientName" />: &nbsp;{v.recipientName}
                      </Typography>
                      <Typography variant="body2">
                        <FormattedMessage id="telephone" />: &nbsp;{v.recipientPhone}
                      </Typography>
                    </div>
                    <div
                      style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}
                    >
                      <Button
                        variant="contained"
                        color="secondary"
                        style={{ minWidth: '114px', minHeight: '32px', marginLeft: '32px' }}
                        onClick={() => onSelect(v)}
                      >
                        <FormattedMessage id="select" />
                      </Button>
                    </div>
                  </div>
                ))
              ) : (
                <NoDataResult id="m.noData" />
              )}
            </>
          )}
          <div />
        </InfiniteScroll>
      </ScrollBar>
    </Dialog>
  );
};
function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}
export default connect(mapStateToProps)(injectIntl(ExportInvoiceHistoryDialog));
