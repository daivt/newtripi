import {
  Button,
  Checkbox,
  Collapse,
  FormControlLabel,
  Radio,
  Typography,
  ButtonBase,
} from '@material-ui/core';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import RadioButtonCheckedOutlined from '@material-ui/icons/RadioButtonCheckedOutlined';
import RadioButtonUncheckedOutlined from '@material-ui/icons/RadioButtonUncheckedOutlined';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { GREY, HOVER_GREY, RED, BLUE } from '../../../../../../colors';
import { some } from '../../../../../../constants';
import { ReactComponent as IcSearch } from '../../../../../../svg/ic_search.svg';
import { ReactComponent as IconContactList } from '../../../../../../svg/ic_contact_list.svg';
import { FreeTextField } from '../../../../../booking/components/Form';
import { Line } from '../../../../../common/components/elements';
import Link from '../../../../../common/components/Link';
import { validTelephoneRegex } from '../../../../../common/utils';
import ExportInvoiceHistoryDialog from './ExportInvoiceHistoryDialog';
import { AddContact } from '../../../../../booking/components/styles';
import { ExportInvoiceParams, ExportInvoiceValidation } from '../../../../utils';

interface Props extends WrappedComponentProps {
  params: ExportInvoiceParams;
  validation: ExportInvoiceValidation;
  packageData?: some[];
  onChange: (params: ExportInvoiceParams, valid: ExportInvoiceValidation) => void;
  pay(): void;
  search(): void;
  ticktock: boolean;
}

const FormInfo: React.FunctionComponent<Props> = props => {
  const { params, validation, onChange, intl, packageData, pay, ticktock, search } = props;
  const [showPackage, setShow] = React.useState(false);
  const [agree, setAgree] = React.useState(false);
  const [showHistory, setShowHistory] = React.useState(false);

  return (
    <div style={{ width: '680px' }}>
      <Line>
        <FreeTextField
          key={`taxIdNumber${ticktock}`}
          valid={validation.taxIdNumber}
          style={{ maxWidth: '320px' }}
          text={params.taxIdNumber || ''}
          update={text => {
            onChange({ ...params, taxIdNumber: text }, { ...validation, taxIdNumber: true });
          }}
          header={intl.formatMessage({ id: 'm.invoice.taxNumber' })}
          placeholder={intl.formatMessage({ id: 'm.invoice.taxNumber' })}
        />
        <Button
          size="large"
          color="secondary"
          variant="contained"
          style={{ alignSelf: 'flex-end', height: '42px', width: '72px', marginBottom: '2px' }}
          onClick={search}
        >
          <IcSearch />
        </Button>
        <ButtonBase
          onClick={() => setShowHistory(true)}
          style={{
            marginBottom: '8px',
            borderRadius: '4px',
            alignSelf: 'flex-end',
            marginLeft: '32px',
          }}
        >
          <AddContact>
            <IconContactList style={{ width: '24px', height: '24px' }} />
            <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
              <FormattedMessage id="booking.addFromContact" />
            </Typography>
          </AddContact>
        </ButtonBase>
      </Line>
      <Line style={{ marginTop: '16px' }}>
        <FreeTextField
          key={`companyName${ticktock}`}
          valid={validation.companyName}
          text={params.companyName || ''}
          update={text => {
            onChange({ ...params, companyName: text }, { ...validation, companyName: true });
          }}
          header={intl.formatMessage({ id: 'm.invoice.companyName' })}
          placeholder={intl.formatMessage({ id: 'm.invoice.companyName' })}
        />{' '}
        <FreeTextField
          key={`companyAddress${ticktock}`}
          valid={validation.companyAddress}
          text={params.companyAddress || ''}
          update={text => {
            onChange({ ...params, companyAddress: text }, { ...validation, companyAddress: true });
          }}
          header={intl.formatMessage({ id: 'm.invoice.address' })}
          placeholder={intl.formatMessage({ id: 'm.invoice.address' })}
        />
      </Line>
      <Typography variant="subtitle2" style={{ marginTop: '32px' }}>
        <FormattedMessage id="m.invoice.recipientInfo" />
      </Typography>
      <Line style={{ marginTop: '16px' }}>
        <FreeTextField
          key={`recipientName${ticktock}`}
          valid={validation.recipientName}
          text={params.recipientName || ''}
          update={text => {
            onChange({ ...params, recipientName: text }, { ...validation, recipientName: true });
          }}
          header={intl.formatMessage({ id: 'm.fullName' })}
          placeholder={intl.formatMessage({ id: 'm.fullName' })}
        />{' '}
        <FreeTextField
          key={`recipientAddress${ticktock}`}
          valid={validation.recipientAddress}
          text={params.recipientAddress || ''}
          update={text => {
            onChange(
              { ...params, recipientAddress: text },
              { ...validation, recipientAddress: true },
            );
          }}
          header={intl.formatMessage({ id: 'm.invoice.address' })}
          placeholder={intl.formatMessage({ id: 'm.invoice.address' })}
        />
      </Line>
      <Line style={{ marginTop: '16px' }}>
        <FreeTextField
          key={`recipientEmail${ticktock}`}
          valid={validation.recipientEmail}
          text={params.recipientEmail || ''}
          update={text => {
            onChange({ ...params, recipientEmail: text }, { ...validation, recipientEmail: true });
          }}
          header={intl.formatMessage({ id: 'm.email' })}
          placeholder={intl.formatMessage({ id: 'm.email' })}
        />{' '}
        <FreeTextField
          key={`recipientPhone${ticktock}`}
          valid={validation.recipientPhone}
          text={params.recipientPhone || ''}
          update={text => {
            onChange({ ...params, recipientPhone: text }, { ...validation, recipientPhone: true });
          }}
          header={intl.formatMessage({ id: 'telephone' })}
          placeholder={intl.formatMessage({ id: 'telephone' })}
          regex={validTelephoneRegex}
        />
      </Line>
      <Line style={{ marginTop: '16px' }}>
        <FreeTextField
          text={params.note || ''}
          update={text => {
            onChange({ ...params, note: text }, validation);
          }}
          optional
          header={intl.formatMessage({ id: 'note' })}
          placeholder={intl.formatMessage({ id: 'note' })}
        />
      </Line>

      <div
        style={{
          marginTop: '20px',
          marginRight: '20px',
        }}
      >
        {packageData && (
          <>
            <Line
              style={{
                paddingLeft: '12px',
                background: HOVER_GREY,
              }}
            >
              <FormControlLabel
                style={{ outline: 'none' }}
                tabIndex={-1}
                control={
                  <Checkbox
                    color="secondary"
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={
                      <CheckBoxIcon
                        fontSize="small"
                        style={{ background: 'white', borderRadius: '3px' }}
                      />
                    }
                    checked={showPackage}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setShow(!showPackage);
                      onChange({ ...params, benefitPackageSetting: undefined }, validation);
                    }}
                  />
                }
                label={
                  <Typography variant="subtitle2" color="inherit">
                    <FormattedMessage id="m.invoice.buyMorePackage" />
                  </Typography>
                }
              />
            </Line>
            <Collapse in={showPackage}>
              {packageData.map(v => (
                <div
                  key={v.id}
                  style={{
                    borderBottom: `1px solid ${GREY}`,
                    height: '44px',
                    display: 'flex',
                    alignItems: 'center',
                    paddingLeft: '12px',
                  }}
                >
                  <FormControlLabel
                    style={{ outline: 'none' }}
                    tabIndex={-1}
                    control={
                      <Radio
                        style={{ padding: '0px', margin: '9px' }}
                        color="secondary"
                        icon={<RadioButtonUncheckedOutlined fontSize="small" />}
                        checkedIcon={
                          <RadioButtonCheckedOutlined
                            fontSize="small"
                            style={{ background: 'white', borderRadius: '3px' }}
                          />
                        }
                        checked={
                          params.benefitPackageSetting
                            ? v.id === params.benefitPackageSetting.id
                            : false
                        }
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                          onChange({ ...params, benefitPackageSetting: v }, validation);
                        }}
                      />
                    }
                    label={
                      <Line style={{ marginTop: '4px' }}>
                        <Typography variant="body2" color="textPrimary">
                          {v.title}
                        </Typography>
                        &nbsp;-&nbsp;
                        <Typography variant="body2" color="secondary">
                          {v.feesDescription}
                        </Typography>
                      </Line>
                    }
                  />
                </div>
              ))}
              <Typography variant="body1" style={{ color: RED, marginTop: '10px' }}>
                <FormattedMessage id="m.invoice.warning" />
              </Typography>
            </Collapse>
          </>
        )}
        <Line style={{ margin: '20px 12px' }}>
          <FormControlLabel
            style={{ outline: 'none' }}
            tabIndex={-1}
            control={
              <Checkbox
                color="secondary"
                icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                checkedIcon={
                  <CheckBoxIcon
                    fontSize="small"
                    style={{ background: 'white', borderRadius: '3px' }}
                  />
                }
                checked={agree}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  setAgree(!agree);
                }}
              />
            }
            label={
              <Typography variant="body2" style={{ marginTop: '4px' }}>
                <FormattedMessage
                  id="booking.agreeWithTerms"
                  values={{
                    link: (
                      <Link to="/terms">
                        <FormattedMessage id="booking.conditionsOfTripi" />
                      </Link>
                    ),
                  }}
                />
              </Typography>
            }
          />
        </Line>
        <Button
          variant="contained"
          color="secondary"
          style={{ width: '170px', height: '40px' }}
          disabled={!agree}
          onClick={pay}
        >
          <Typography variant="subtitle2">
            <FormattedMessage id="pay" />
          </Typography>
        </Button>
        <ExportInvoiceHistoryDialog
          open={showHistory}
          onClose={() => setShowHistory(false)}
          onSelect={(data: some) => {
            setShowHistory(false);
            onChange(
              {
                ...params,
                taxIdNumber: data.taxIdNumber,
                companyName: data.companyName,
                companyAddress: data.companyAddress,
                recipientName: data.recipientName,
                recipientAddress: data.recipientAddress,
                recipientEmail: data.recipientEmail,
                recipientPhone: data.recipientPhone,
              },
              validation,
            );
          }}
        />
      </div>
    </div>
  );
};

export default injectIntl(FormInfo);
