import { Typography } from '@material-ui/core';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../../API';
import { some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import MessageDialog from '../../../../../common/components/MessageDialog';
import { fetchThunk } from '../../../../../common/redux/thunks';
import { BOOKING_ID_PARAM_NAME, TAB_PARAM_NAME, ACTION_PARAM_NAME } from '../../../../constants';
import {
  isValidExportInvoice,
  validateExportInvoiceParams,
  ExportInvoiceValidation,
  ExportInvoiceParams,
} from '../../../../utils';
import FormInfo from './FormInfo';
import { goBack, replace } from 'connected-react-router';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  breadcrumb?: React.ReactNode;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}
const DEFAULT_VALIDATION: ExportInvoiceValidation = {
  companyName: true,
  companyAddress: true,
  recipientAddress: true,
  recipientEmail: true,
  recipientName: true,
  recipientPhone: true,
  taxIdNumber: true,
};
const ExportInvoice: React.FunctionComponent<Props> = props => {
  const { breadcrumb, intl, dispatch, router } = props;
  const [params, setParams] = React.useState<ExportInvoiceParams>({});
  const [paramsValidation, setParamsValidation] = React.useState<ExportInvoiceValidation>(
    DEFAULT_VALIDATION,
  );
  const [packageData, setPackageData] = React.useState<some[] | undefined>(undefined);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [ticktock, setTick] = React.useState(false);
  const search = router.location.search;
  const paramsUrl = new URLSearchParams(search);
  const module = paramsUrl.get(TAB_PARAM_NAME);
  const bookingId = paramsUrl.get(BOOKING_ID_PARAM_NAME);
  const requestVATInvoice = React.useCallback(() => {
    const fetch = async () => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.requestVATInvoice}`,
          'post',
          true,
          JSON.stringify({ ...params, bookingId, module }),
        ),
      );
      if (json.code === 200) {
        setErrorMessage('m.invoice.success');
      } else {
        setErrorMessage(json.message);
      }
    };
    fetch();
  }, [dispatch, params, bookingId, module]);
  React.useEffect(() => {
    const fetch = async () => {
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getServicePackOptions}?bookingId=${bookingId}&module=${module}`,
          'get',
          true,
        ),
      );
      if (json.code === 200) {
        setPackageData(json.data);
      }
    };
    fetch();
  }, [dispatch, bookingId, module]);
  const payCheck = React.useCallback(() => {
    const validation = validateExportInvoiceParams(params);
    setParamsValidation(validation);
    setTick(!ticktock);
    if (isValidExportInvoice(validation)) {
      requestVATInvoice();
    } else {
      setErrorMessage('m.invoice.errorForm');
    }
  }, [params, ticktock, requestVATInvoice]);
  const searchByTaxNumber = React.useCallback(() => {
    if (params.taxIdNumber) {
      const fetch = async () => {
        const json = await dispatch(
          fetchThunk(
            `${API_PATHS.getEnterpriseInfo}`,
            'post',
            true,
            JSON.stringify({ taxCode: params.taxIdNumber }),
          ),
        );
        if (json.code === 200 && json.data) {
          setParams({ ...params, ...json.data });
          setParamsValidation(DEFAULT_VALIDATION);
        }
      };
      fetch();
    }
  }, [dispatch, params]);
  return (
    <div>
      <Helmet>
        <title>{`${intl.formatMessage({ id: 'm.exportInvoice' })}`}</title>
      </Helmet>
      {breadcrumb}
      <Typography variant="h5" style={{ padding: '16px 0' }}>
        <FormattedMessage id="m.invoice.title" />
      </Typography>
      <Typography variant="subtitle1" style={{ padding: '16px 0' }}>
        <FormattedMessage id="m.invoice.titleNote" />
      </Typography>
      <FormInfo
        ticktock={ticktock}
        params={params}
        validation={paramsValidation}
        onChange={(params: ExportInvoiceParams, validation: ExportInvoiceValidation) => {
          setParams(params);
          setParamsValidation(validation);
        }}
        packageData={packageData}
        search={searchByTaxNumber}
        pay={payCheck}
      />
      <MessageDialog
        show={!!errorMessage}
        onClose={() => {
          setErrorMessage('');
          if (errorMessage === 'm.invoice.success') {
            const backableToDetail =
              router.location.state && router.location.state.backableToDetail;
            if (backableToDetail) {
              dispatch(goBack());
              return;
            }
            const params = new URLSearchParams(router.location.search);
            params.set(ACTION_PARAM_NAME, 'detail');
            dispatch(replace({ search: params.toString() }));
          }
        }}
        message={
          <Typography variant="body1" style={{ padding: '0px 16px', textAlign: 'center' }}>
            {errorMessage && <FormattedMessage id={errorMessage} />}
          </Typography>
        }
      />
    </div>
  );
};
function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}
export default connect(mapStateToProps)(injectIntl(ExportInvoice));
