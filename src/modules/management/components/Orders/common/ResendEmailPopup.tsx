import React, { PureComponent } from 'react';
import { Dialog, Typography, IconButton, Button } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { FreeTextField } from '../../../../booking/components/Form';
import LoadingButton from '../../../../common/components/LoadingButton/index';
import { AppState } from '../../../../../redux/reducers';
import { some } from '../../../../../constants';
import { fetchThunk } from '../../../../common/redux/thunks';
import { API_PATHS } from '../../../../../API';
import { validEmailRegex } from '../../../../common/utils';

interface Props extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  show: boolean;
  onClose(): void;
  onSuccess(): void;
  onError(messageError: string): void;
  data: some;
  module?: string;
}

interface State {
  email: string;
  emailValid: boolean;
  loading: boolean;
  ticktock: boolean;
}

class ResendEmailPopup extends PureComponent<Props, State> {
  state: State = {
    email: this.props.data.customerEmail
      ? this.props.data.customerEmail
      : this.props.data.mainContact.email
      ? this.props.data.mainContact.email
      : '',
    emailValid: true,
    loading: false,
    ticktock: false,
  };

  onExited() {
    const { data } = this.props;
    this.setState({
      email: data.customerEmail ? data.customerEmail : data.mainContact.email ? data.mainContact.email : '',
      emailValid: true,
      loading: false,
      ticktock: false,
    });
  }

  async resendEmail() {
    const { email, ticktock } = this.state;
    const { dispatch, data, onSuccess, module, onError } = this.props;
    const emailValid = validEmailRegex.test(email);
    this.setState({ emailValid, ticktock: !ticktock, loading: true });
    if (!emailValid) {
      this.setState({ loading: false });
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.resendBookingSuccessEmail,
        'post',
        true,
        JSON.stringify({
          email,
          module,
          bookingId: data.id,
        }),
      ),
    );

    this.setState({ loading: false });
    if (json.code === 200) {
      onSuccess();
    } else {
      onError(json.message);
    }
  }

  render() {
    const { show, onClose, intl } = this.props;
    const { email, emailValid, loading, ticktock } = this.state;
    return (
      <Dialog open={show} fullWidth maxWidth="sm" onExited={() => this.onExited()}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <div
              style={{
                display: 'flex',
                flex: 1,
                paddingLeft: '16px',
                paddingTop: '32px',
                justifyContent: 'center',
              }}
            >
              <Typography variant="h5">
                <FormattedMessage id="m.order.resendEmailConfirm" />
              </Typography>
            </div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <IconButton style={{ padding: '8px' }} size="small" onClick={onClose}>
                <IconClose />
              </IconButton>
            </div>
          </div>
          <div style={{ margin: '0 120px' }}>
            <Typography variant="body2" style={{ padding: '20px', display: 'flex', justifyContent: 'center' }}>
              <FormattedMessage id="m.order.weResendEmail" />
            </Typography>

            <FreeTextField
              key={`email${ticktock}`}
              style={{ margin: 0 }}
              text={email}
              valid={emailValid}
              header={intl.formatMessage({ id: 'm.order.enterEmail' })}
              placeholder={intl.formatMessage({ id: 'signUp.emailExample' })}
              update={value => this.setState({ email: value, emailValid: true })}
            />
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingTop: '24px',
              paddingBottom: '32px',
            }}
          >
            <div style={{ padding: '0 16px' }}>
              <LoadingButton
                loading={loading}
                style={{ width: '170px' }}
                variant="contained"
                color="secondary"
                size="large"
                onClick={() => this.resendEmail()}
              >
                <Typography variant="button">
                  <FormattedMessage id="accept" />
                </Typography>
              </LoadingButton>
            </div>
            <div style={{ padding: '0 16px' }}>
              <Button style={{ width: '170px' }} variant="outlined" size="large" onClick={onClose}>
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="cancel" />
                </Typography>
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default injectIntl(ResendEmailPopup);
