import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import {
  endPaying,
  fetchPaymentMethodsDirect,
  fetchPointPaymentDirect,
  setCardInfo,
  setPaymentMethods,
  setPromotion,
  setPromotionCode,
  setSelectedPaymentMethod,
} from '../redux/mobileReducer';
import MobileTopupPayTablet from '../components/mobile/MobileTopupPayTablet';
import MobileTopupPayDesktop from '../components/mobile/MobileTopupPayDesktop';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.mobile,
    userData: state.account.userData,
    routerState: state.router.location.state,
  };
};
export interface IHotelPayProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class MobileTopupPay extends React.Component<IHotelPayProps, State> {
  state: State = {};

  componentDidMount() {
    this.props.dispatch(setPromotionCode(''));
    this.props.dispatch(setPromotion());
    this.props.dispatch(setCardInfo());
    this.props.dispatch(setPaymentMethods());
    this.props.dispatch(setSelectedPaymentMethod());
    this.props.dispatch(fetchPaymentMethodsDirect());
    this.props.dispatch(fetchPointPaymentDirect());
    this.props.dispatch(endPaying());
  }

  public render() {
    const { booking, intl, routerState } = this.props;

    const buyer = booking.buyerMobileTopupInfo;
    if (!buyer.cardValue || !buyer.operatorCode || !buyer.phone) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
            state: routerState,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>
            {intl.formatMessage({ id: 'mobile.titleDirect' })}
            :&nbsp;
            {intl.formatMessage({ id: 'mobile.payTitle' })}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <MobileTopupPayDesktop />;
            }
            return <MobileTopupPayTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(MobileTopupPay));
