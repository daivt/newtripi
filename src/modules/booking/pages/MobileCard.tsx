import { Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import React from 'react';
import { Helmet } from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { DESKTOP_WIDTH, ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { MobileCardBuyer, BuyerInfoValidation } from '../../common/models';
import MobileCardDesktop from '../components/mobile/MobileCardDesktop';
import { fetchMobileData, setBuyerInfo, setBuyerInfoValidation } from '../redux/mobileReducer';
import { validateBuyerInfo, validBuyerInfo } from '../utils';
import MobileCardTablet from '../components/mobile/MobileCardTablet';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  errorMessage: string;
}

class MobileCard extends React.PureComponent<Props, State> {
  state: State = { errorMessage: '' };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchMobileData());
  }

  continue = async () => {
    const { dispatch, intl, buyerMobileCardInfo, mobileData, routerState } = this.props;
    if (mobileData && buyerMobileCardInfo) {
      const buyerInfoValidation = validateBuyerInfo(buyerMobileCardInfo);
      dispatch(setBuyerInfoValidation(buyerInfoValidation));
      const operator = mobileData.find(v => v.code === buyerMobileCardInfo.operatorCode);
      if (!buyerInfoValidation.email) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidEmail' }) });
        return;
      }

      if (!validBuyerInfo(buyerInfoValidation)) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidBuyerInfo' }) });
        return;
      }
      if (!operator) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidOperator' }) });
        return;
      }
      if (operator && !operator.cardProducts.find((v: some) => v.value === buyerMobileCardInfo.cardValue)) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidCard' }) });
      } else {
        dispatch(
          push({
            pathname: ROUTES.mobile.mobileCardPay,
            state: {
              ...routerState,
              backableToMobileCode: true,
              backableToMobileDirect: false,
            },
          }),
        );
      }
    }
  };

  render() {
    const { buyerMobileCardInfo, intl, mobileData, buyerInfoValidation, dispatch } = this.props;
    const { errorMessage } = this.state;
    const props = {
      buyerInfoValidation,
      buyerMobileCardInfo,
      mobileData: mobileData || [
        { id: 1, skeleton: true },
        { id: 2, skeleton: true },
        { id: 3, skeleton: true },
        { id: 4, skeleton: true },
        { id: 5, skeleton: true },
        { id: 6, skeleton: true },
        { id: 7, skeleton: true },
        { id: 8, skeleton: true },
      ],
      updateBuyerInfo: async (info: MobileCardBuyer) => {
        await dispatch(setBuyerInfo(info));
      },
      updateBuyerInfoValidation: (validation: BuyerInfoValidation) => dispatch(setBuyerInfoValidation(validation)),
      continue_: this.continue,
    };
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'mobile.titleCode' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <MobileCardDesktop {...props} />;
            }
            return <MobileCardTablet {...props} />;
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}
const mapStateToProps = (state: AppState) => {
  return {
    buyerMobileCardInfo: state.booking.mobile.buyerMobileCardInfo,
    buyerInfoValidation: state.booking.mobile.buyerInfoValidation,
    mobileData: state.booking.mobile.mobileData,
    routerState: state.router.location.state,
  };
};
export default connect(mapStateToProps)(injectIntl(MobileCard));
