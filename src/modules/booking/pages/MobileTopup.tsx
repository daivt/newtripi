import { Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import React from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { DESKTOP_WIDTH, ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { BuyerInfoValidation, MobileTopupBuyer } from '../../common/models';
import { validVietnamTelephoneRegex } from '../../common/utils';
import MobileTopupDesktop from '../components/mobile/MobileTopupDesktop';
import MobileTopupDirectTablet from '../components/mobile/MobileTopupTablet';
import { fetchMobileData, setbuyerMobileTopupInfo, setBuyerInfoValidation } from '../redux/mobileReducer';

interface State {
  errorMessage: string;
}
interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

class MobileTopup extends React.PureComponent<Props, State> {
  state: State = { errorMessage: '' };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchMobileData());
  }

  continue = async () => {
    const { dispatch, intl, buyerMobileTopupInfo, mobileData, routerState, buyerInfoValidation } = this.props;
    if (mobileData && buyerMobileTopupInfo) {
      const phone = validVietnamTelephoneRegex.test(buyerMobileTopupInfo.phone || '');
      dispatch(setBuyerInfoValidation({ ...buyerInfoValidation, phone }));
      const operator = mobileData.find(v => v.code === buyerMobileTopupInfo.operatorCode);
      if (!phone) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidPhone' }) });
        return;
      }
      if (!operator) {
        return;
      }
      if (operator && !operator.mobileTopupProducts.find((v: some) => v.value === buyerMobileTopupInfo.cardValue)) {
        this.setState({ errorMessage: intl.formatMessage({ id: 'mobile.invalidCard' }) });
      } else {
        dispatch(
          push({
            pathname: ROUTES.mobile.mobileTopupPay,
            state: {
              ...routerState,
              backableToMobileDirect: true,
              backableToMobileCode: false,
            },
          }),
        );
      }
    }
  };

  render() {
    const { buyerMobileTopupInfo, intl, mobileData, buyerInfoValidation, dispatch } = this.props;
    const { errorMessage } = this.state;
    const props = {
      buyerInfoValidation,
      buyerMobileTopupInfo,
      updateBuyerInfoValidation: (validation: BuyerInfoValidation) => dispatch(setBuyerInfoValidation(validation)),
      mobileData: mobileData
        ? mobileData.filter(v => v.isTelco)
        : [
            { id: 1, skeleton: true },
            { id: 2, skeleton: true },
            { id: 3, skeleton: true },
            { id: 4, skeleton: true },
            { id: 5, skeleton: true },
            { id: 6, skeleton: true },
            { id: 7, skeleton: true },
            { id: 8, skeleton: true },
          ],
      updateBuyerInfo: async (info: MobileTopupBuyer) => {
        await dispatch(setbuyerMobileTopupInfo(info));
      },
      continue_: this.continue,
    };
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'mobile.titleDirect' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <MobileTopupDesktop {...props} />;
            }
            return <MobileTopupDirectTablet {...props} />;
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}
const mapStateToProps = (state: AppState) => {
  return {
    buyerMobileTopupInfo: state.booking.mobile.buyerMobileTopupInfo,
    mobileData: state.booking.mobile.mobileData,
    routerState: state.router.location.state,
    buyerInfoValidation: state.booking.mobile.buyerInfoValidation,
  };
};
export default connect(mapStateToProps)(injectIntl(MobileTopup));
