import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import FlightPayDesktop from '../components/flight/FlightPayDesktop';
import FlightPayTablet from '../components/flight/FlightPayTablet';
import {
  endPaying,
  fetchPaymentMethods,
  fetchPointPayment,
  setPayResponseMessage,
} from '../redux/flightBookingReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.flight,
  };
};
export interface IFlightPayProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class FlightPay extends React.Component<IFlightPayProps, State> {
  async componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchPaymentMethods());
    dispatch(fetchPointPayment());
    dispatch(endPaying());
    dispatch(setPayResponseMessage());
  }

  public render() {
    const { booking, intl } = this.props;

    if (!booking.tid || !booking.travellersInfo) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.flightPay' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <FlightPayDesktop />;
            }
            return <FlightPayTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightPay));
