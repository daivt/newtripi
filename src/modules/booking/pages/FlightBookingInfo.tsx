import Typography from '@material-ui/core/Typography';
import { push, replace } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES, MY_TOUR } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import {
  ContactInfo,
  ContactInfoValidation,
  defaultPassportInfo,
  TravellerInfo,
  TravellersInfo,
  TravellersInfoValidation,
  validContactInfoValidation,
} from '../../common/models';
import FlightBookingInfoDesktop from '../components/flight/FlightBookingInfoDesktop';
import FlightBookingInfoTablet from '../components/flight/FlightBookingInfoTablet';
import {
  fetchTicketDataAndInsurancePackage,
  setContactInfo,
  setSelectedPaymentMethod,
  setTravellersInfo,
  validateInsuranceInfo,
} from '../redux/flightBookingReducer';
import { valid, validateContactInfo, validateTravellersInfo, validContact } from '../utils';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.flight,
    flightSearch: state.search.flight,
    router: state.router,
  };
};
export interface IFlightBookingProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  travellersInfo: TravellersInfo;
  contactInfo: ContactInfo;
  travellersInfoValidation: TravellersInfoValidation;
  contactInfoValidation: ContactInfoValidation;
  init: boolean;
  errorMessage: string;
}

class FlightBookingInfo extends React.PureComponent<IFlightBookingProps, State> {
  state: State = {
    travellersInfo: {
      adults: [],
      children: [],
      babies: [],
    },
    contactInfo: {
      address: '',
      email: '',
      familyName: '',
      gender: 'm',
      givenName: '',
      telephone: '',
    },
    travellersInfoValidation: {
      adults: [],
      children: [],
      babies: [],
    },
    contactInfoValidation: validContactInfoValidation,
    init: false,
    errorMessage: '',
  };

  componentDidMount() {
    const { dispatch, router } = this.props;
    const { state } = router.location;

    if (state && state.noFetch) {
      dispatch(replace({ ...router.location, state: undefined }));
      return;
    }
    dispatch(fetchTicketDataAndInsurancePackage());
    dispatch(setSelectedPaymentMethod());
  }

  static getDerivedStateFromProps(props: IFlightBookingProps, state: State): State | null {
    if (props.booking.outbound.ticket && !state.init) {
      if (props.booking.travellersInfo && props.booking.contactInfo) {
        return {
          ...state,
          travellersInfo: props.booking.travellersInfo,
          contactInfo: props.booking.contactInfo,
          init: true,
        };
      }
      if (props.router.location.state && props.router.location.state.bookingInfoState) {
        return props.router.location.state.bookingInfoState;
      }

      const defaultTravellerInfo: TravellerInfo = {
        familyName: '',
        givenName: '',
        birthday: '',
        gender: 'm',
        passportInfo: defaultPassportInfo,
      };

      const adults = Array<TravellerInfo>(props.booking.outbound.searchRequest.numAdults).fill(defaultTravellerInfo);

      const children = Array<TravellerInfo>(props.booking.outbound.searchRequest.numChildren).fill(
        defaultTravellerInfo,
      );

      const babies = Array<TravellerInfo>(props.booking.outbound.searchRequest.numInfants).fill(defaultTravellerInfo);

      return {
        ...state,
        travellersInfo: { adults, children, babies },
        init: true,
      };
    }
    return null;
  }

  saveState = () => {
    const { dispatch, router } = this.props;
    dispatch(
      replace({
        ...router.location,
        state: { ...router.location.state, bookingInfoState: this.state },
      }),
    );
  };

  continue = async () => {
    const { contactInfo, travellersInfo } = this.state;
    const { dispatch, booking, intl, router } = this.props;

    const outboundTicket = booking.outbound.ticket;
    const inboundTicket = booking.inbound.ticket ? booking.inbound.ticket : undefined;

    const needPassport = outboundTicket ? outboundTicket.outbound.ticketdetail.needPassport : false;

    let arrivalTime = outboundTicket ? outboundTicket.outbound.arrivalTime : 0;
    if (inboundTicket) {
      arrivalTime = inboundTicket.outbound.arrivalTime;
    }

    const travellersInfoValidation = validateTravellersInfo(travellersInfo, needPassport, arrivalTime);

    const contactInfoValidation = validateContactInfo(contactInfo);
    this.setState({
      travellersInfoValidation,
      contactInfoValidation,
    });
    if (!booking.booker && !MY_TOUR) {
      this.setState({ errorMessage: intl.formatMessage({ id: 'booking.noBooker' }) });
      return;
    }

    if (valid(travellersInfoValidation)) {
      if (validContact(contactInfoValidation)) {
        if (await this.validateInsuranceInfo()) {
          await dispatch(replace({ ...router.location, state: { noFetch: true } }));
          dispatch(setTravellersInfo(travellersInfo));
          dispatch(setContactInfo(contactInfo));
          dispatch(
            push({
              pathname: ROUTES.flight.flightReview,
              state: { ...router.location.state, backableToInfo: true },
            }),
          );
        }
      } else {
        this.setState({ errorMessage: intl.formatMessage({ id: 'booking.invalidContactInfo' }) });
      }
    } else {
      this.setState({ errorMessage: intl.formatMessage({ id: 'booking.invalidTravellerInfo' }) });
    }
  };

  async validateInsuranceInfo() {
    const { dispatch, booking } = this.props;
    const { contactInfo, travellersInfo } = this.state;

    if (booking.insurancePackage && booking.buyInsurance) {
      const json = await dispatch(validateInsuranceInfo(contactInfo, travellersInfo));

      if (json.code === 200) {
        return true;
      }

      this.setState({ errorMessage: json.message });
      return false;
    }
    return true;
  }

  public render() {
    const { contactInfo, travellersInfo, contactInfoValidation, travellersInfoValidation, errorMessage } = this.state;
    const { intl } = this.props;

    const props = {
      contactInfo,
      travellersInfo,
      travellersInfoValidation,
      contactInfoValidation,
      updateContactInfo: (info: ContactInfo) => this.setState({ contactInfo: info }, this.saveState),
      updateContactInfoValidation: (validation: ContactInfoValidation) =>
        this.setState({ contactInfoValidation: validation }, this.saveState),
      updateTravellersInfo: (info: TravellersInfo) => this.setState({ travellersInfo: info }, this.saveState),
      updateTravellersInfoValidation: (validation: TravellersInfoValidation) =>
        this.setState({ travellersInfoValidation: validation }, this.saveState),
      continue_: this.continue,
    };

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.flightTravellersInfo' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <FlightBookingInfoDesktop {...props} />;
            }
            return <FlightBookingInfoTablet {...props} />;
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightBookingInfo));
