import Typography from '@material-ui/core/Typography';
import { push } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { scrollTo } from '../../../utils';
import MessageDialog from '../../common/components/MessageDialog';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import {
  AdultInfoValidation,
  AdultTravellerInfo,
  ChildInfoValidation,
  ChildTravellerInfo,
  ContactInfo,
  ContactInfoValidation,
  CustomerInfo,
  CustomerInfoValidation,
  defaultAdultTravellerInfo,
  defaultChildTravellerInfo,
  defaultContactInfo,
  defaultCustomerInfo,
  defaultSeniorTravellerInfo,
  defaultStudentTravellerInfo,
  defaultTravellersInfo,
  ExportBillInfo,
  ExportBillInfoValidation,
  SeniorInfoValidation,
  SeniorTravellerInfo,
  StudentInfoValidation,
  StudentTravellerInfo,
  TravellersInfo,
  TravellersInfoValidation,
  validAdultInfoValidation,
  validChildInfoValidation,
  validContactInfoValidation,
  validCustomerInfoValidation,
  validExportBillInfoValidation,
  validSeniorInfoValidation,
  validStudentInfoValidation,
  validTravellersInfoValidation,
} from '../../common/models/train';
import TrainBookingInfoDesktop from '../components/train/TrainBookingInfoDesktop';
import { BookingInfoBoxTrainType } from '../constants';
import { setContactInfo, setCustomerInfo, setExportBillInfo, setTravellersInfo } from '../redux/trainBookingReducer';
import {
  validateContactInfoTrain,
  validateCustomerInfoTrain,
  validateExportBillInfoTrain,
  validateTrvellersInfoTrain,
  validContactInfoTrain,
  validCustomerInfoTrain,
  validExportBillInfoTrain,
  validTravellersInfoTrain,
} from '../utils/train';
import TrainBookingInfoTablet from '../components/train/TrainBookingInfoTablet';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
    router: state.router,
  };
};
export interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  travellersInfo: TravellersInfo;
  contactInfo: ContactInfo;
  customerInfo: CustomerInfo;
  exportBillInfo?: ExportBillInfo;
  travellersInfoValidation: TravellersInfoValidation;
  contactInfoValidation: ContactInfoValidation;
  customerInfoValidation: CustomerInfoValidation;
  exportBillInfoValidation: ExportBillInfoValidation;
  init: boolean;
  ticktock: boolean;
  errorMessage: string;
}

class TrainBookingInfo extends React.PureComponent<Props, State> {
  state: State = {
    travellersInfo: defaultTravellersInfo,
    customerInfo: defaultCustomerInfo,
    contactInfo: defaultContactInfo,
    travellersInfoValidation: validTravellersInfoValidation,
    contactInfoValidation: validContactInfoValidation,
    customerInfoValidation: validCustomerInfoValidation,
    exportBillInfoValidation: validExportBillInfoValidation,
    init: false,
    errorMessage: '',
    ticktock: false,
  };

  static getDerivedStateFromProps(props: Props, state: State): State | null {
    if (props.booking.holdSeatData && !state.init) {
      const adultsValidation = Array<AdultInfoValidation>(props.booking.holdSeatData.reqSearchTrain.adt).fill(
        validAdultInfoValidation,
      );

      const studentsValidation = Array<StudentInfoValidation>(props.booking.holdSeatData.reqSearchTrain.std).fill(
        validStudentInfoValidation,
      );

      const childrensValidation = Array<ChildInfoValidation>(props.booking.holdSeatData.reqSearchTrain.chd).fill(
        validChildInfoValidation,
      );

      const seniorsValidation = Array<SeniorInfoValidation>(props.booking.holdSeatData.reqSearchTrain.old).fill(
        validSeniorInfoValidation,
      );
      if (
        props.booking.guestInfo.travellersInfo &&
        props.booking.guestInfo.contactInfo &&
        props.booking.guestInfo.customerInfo
      ) {
        return {
          ...state,
          travellersInfo: props.booking.guestInfo.travellersInfo,
          contactInfo: props.booking.guestInfo.contactInfo,
          customerInfo: props.booking.guestInfo.customerInfo,
          exportBillInfo: props.booking.guestInfo.exportBillInfo
            ? props.booking.guestInfo.exportBillInfo
            : state.exportBillInfo,
          travellersInfoValidation: {
            adults: adultsValidation,
            childrens: childrensValidation,
            students: studentsValidation,
            seniors: seniorsValidation,
          },
          init: true,
        };
      }
      if (props.router.location.state && props.router.location.state.bookingInfoState) {
        return props.router.location.state.bookingInfoState;
      }
      const adults = Array<AdultTravellerInfo>(props.booking.holdSeatData.reqSearchTrain.adt).fill(
        defaultAdultTravellerInfo,
      );

      const students = Array<StudentTravellerInfo>(props.booking.holdSeatData.reqSearchTrain.std).fill(
        defaultStudentTravellerInfo,
      );

      const childrens = Array<ChildTravellerInfo>(props.booking.holdSeatData.reqSearchTrain.chd).fill(
        defaultChildTravellerInfo,
      );

      const seniors = Array<SeniorTravellerInfo>(props.booking.holdSeatData.reqSearchTrain.old).fill(
        defaultSeniorTravellerInfo,
      );
      /** -------------validation------------ */

      return {
        ...state,
        travellersInfo: { adults, students, childrens, seniors },
        travellersInfoValidation: {
          adults: adultsValidation,
          childrens: childrensValidation,
          students: studentsValidation,
          seniors: seniorsValidation,
        },
        init: true,
      };
    }
    return null;
  }

  continue = async () => {
    const { dispatch, router } = this.props;
    const { contactInfo, customerInfo, travellersInfo, exportBillInfo, ticktock } = this.state;
    const travellersInfoValidation = validateTrvellersInfoTrain(travellersInfo);
    const contactInfoValidation = validateContactInfoTrain(contactInfo);
    const customerInfoValidation = validateCustomerInfoTrain(customerInfo);
    const exportBillInfoValidation = exportBillInfo
      ? validateExportBillInfoTrain(exportBillInfo)
      : validExportBillInfoValidation;
    this.setState({
      travellersInfoValidation,
      contactInfoValidation,
      customerInfoValidation,
      exportBillInfoValidation,
      ticktock: !ticktock,
    });
    const travellerValid = validTravellersInfoTrain(travellersInfoValidation);
    const customerValid = validCustomerInfoTrain(customerInfoValidation);
    const contactValid = validContactInfoTrain(contactInfoValidation);
    const exportBillValid = validExportBillInfoTrain(exportBillInfoValidation);
    if (customerValid) {
      scrollTo('customerinfo' as BookingInfoBoxTrainType, 250);
    } else if (!travellerValid) {
      scrollTo('travellerInfo' as BookingInfoBoxTrainType, 250);
    } else if (!contactValid) {
      scrollTo('contactInfo' as BookingInfoBoxTrainType, 250);
    } else if (!exportBillValid) {
      scrollTo('exportBillInfo' as BookingInfoBoxTrainType, 250);
    } else {
      dispatch(setTravellersInfo(travellersInfo));
      dispatch(setCustomerInfo(customerInfo));
      dispatch(setContactInfo(contactInfo));
      dispatch(setExportBillInfo(exportBillInfo));
      dispatch(
        push({
          pathname: ROUTES.train.review,
          state: { ...router.location.state, backableToBookingInfo: true },
        }),
      );
      /** --------Do someThing--------- */
    }
  };

  public render() {
    const {
      contactInfo,
      customerInfo,
      travellersInfo,
      exportBillInfo,
      customerInfoValidation,
      contactInfoValidation,
      travellersInfoValidation,
      exportBillInfoValidation,
      errorMessage,
      ticktock,
    } = this.state;

    const { intl, booking } = this.props;

    const props = {
      ticktock,
      customerInfo,
      contactInfo,
      exportBillInfo,
      travellersInfo,
      customerInfoValidation,
      contactInfoValidation,
      travellersInfoValidation,
      exportBillInfoValidation,
      updateContactInfo: (info: ContactInfo) => this.setState({ contactInfo: info }),
      updateCustomerInfo: (info: CustomerInfo) => this.setState({ customerInfo: info }),
      updateExportBillInfo: (info: ExportBillInfo | undefined) => this.setState({ exportBillInfo: info }),
      updateTravellersInfo: (info: TravellersInfo) => this.setState({ travellersInfo: info }),
      updateContactInfoValidation: (validation: ContactInfoValidation) =>
        this.setState({ contactInfoValidation: validation }),
      updateTravellersInfoValidation: (validation: TravellersInfoValidation) =>
        this.setState({ travellersInfoValidation: validation }),
      updateCustomerInfoValidation: (validation: CustomerInfoValidation) =>
        this.setState({ customerInfoValidation: validation }),
      updateExportBillInfoValidation: (validation: ExportBillInfoValidation) =>
        this.setState({ exportBillInfoValidation: validation }),
      continue_: this.continue,
    };
    if (!booking.holdSeatData) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.trainTravellersInfo' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <TrainBookingInfoDesktop {...props} />;
            }
            return <TrainBookingInfoTablet {...props} />;
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(TrainBookingInfo));
