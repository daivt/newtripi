import * as React from 'react';
import Helmet from 'react-helmet';
import { useIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import TrainReviewDesktop from '../components/train/TrainReviewDesktop';
import TrainReviewTablet from '../components/train/TrainReviewTablet';
import { setLoading } from '../redux/trainBookingReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
    router: state.router,
  };
};
export interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

const TrainReview: React.FunctionComponent<Props> = props => {
  const { booking, dispatch } = props;
  const { contactInfo, customerInfo, travellersInfo } = booking.guestInfo;
  const intl = useIntl();
  React.useEffect(() => {
    dispatch(setLoading(false));
  }, [dispatch]);
  if (!contactInfo || !customerInfo || !travellersInfo || !booking.holdSeatData) {
    return (
      <Redirect
        to={{
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }}
      />
    );
  }
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'booking.trainTravellersInfo' })}</title>
      </Helmet>
      <MediaQuery minWidth={DESKTOP_WIDTH}>
        {match => {
          if (match) {
            return <TrainReviewDesktop />;
          }
          return <TrainReviewTablet />;
        }}
      </MediaQuery>
    </>
  );
};

export default connect(mapStateToProps)(TrainReview);
