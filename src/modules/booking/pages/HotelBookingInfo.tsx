import Typography from '@material-ui/core/Typography';
import { push } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { RoomBookingInfo, RoomBookingInfoValidation, validRoomBookingInfo } from '../../common/models';
import { fetchHotelDetails, fetchroomsData } from '../../result/redux/hotelResultReducer';
import HotelBookingInfoDesktop from '../components/hotel/HotelBookingInfoDesktop';
import HotelBookingInfoTablet from '../components/hotel/HotelBookingInfoTablet';
import {
  setHotelData,
  setRoomsBookingInfo,
  setRoomsData,
  setSelectedPaymentMethod,
  setFlightTicketCode,
} from '../redux/hotelBookingReducer';
import { validateRoomsBookingInfo, validBookingInfo } from '../utils';
import { getRoomData } from '../../common/utils';

const mapState2Props = (state: AppState) => {
  return {
    params: state.booking.hotel.params,
    roomsBookingInfoProps: state.booking.hotel.roomsBookingInfo,
    roomsData: state.booking.hotel.roomsData,
    hotelData: state.booking.hotel.hotelData,
    routerState: state.router.location.state,
    flightTicketCode: state.booking.hotel.flightTicketCode,
  };
};

export interface IHotelBookingInfoProps extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  roomsBookingInfo: RoomBookingInfo[];
  roomsBookingInfoValidation: RoomBookingInfoValidation[];
  phoneNumberValidation: boolean;
  errorMessage: string;
  init: boolean;
  flightTicketCode: string;
  flightTicketCodeValidation: boolean;
}

class HotelBookingInfo extends React.Component<IHotelBookingInfoProps, State> {
  state: State = {
    roomsBookingInfo: [],
    roomsBookingInfoValidation: [],
    phoneNumberValidation: true,
    errorMessage: '',
    init: false,
    flightTicketCode: '',
    flightTicketCodeValidation: true,
  };

  static getDerivedStateFromProps(props: IHotelBookingInfoProps, state: State): State | null {
    if (props.params && !state.init) {
      const defaultRoomBookingInfo: RoomBookingInfo = {
        name: '',
      };

      const roomsBookingInfo = Array<RoomBookingInfo>(props.params?.guestInfo.roomCount).fill(defaultRoomBookingInfo);
      const roomsBookingInfoValidation = Array<RoomBookingInfoValidation>(props.params?.guestInfo.roomCount).fill(
        validRoomBookingInfo,
      );
      roomsBookingInfo[0].phoneNumber = '';

      return {
        ...state,
        roomsBookingInfo,
        roomsBookingInfoValidation,
        init: true,
        flightTicketCode: '',
      };
    }
    return null;
  }

  async componentDidMount() {
    const { dispatch, params } = this.props;
    if (params) {
      dispatch(setRoomsData());
      dispatch(setHotelData());

      dispatch(fetchroomsData(params)).then(json => {
        if (json.code === 200) {
          dispatch(setRoomsData(json.data));
        }
      });
      dispatch(fetchHotelDetails(params)).then(json => {
        if (json.code === 200) {
          dispatch(setHotelData(json.data));
        }
      });
    }
    dispatch(setSelectedPaymentMethod());
  }

  continue = () => {
    const { roomsBookingInfo, flightTicketCode } = this.state;
    const { dispatch, intl, routerState, roomsData, params, roomsBookingInfoProps } = this.props;

    const roomsBookingInfoState = roomsBookingInfoProps || roomsBookingInfo;
    const roomsBookingInfoValidation = validateRoomsBookingInfo(roomsBookingInfoState);
    this.setState({ roomsBookingInfoValidation });
    const phoneNumberValidation =
      (roomsBookingInfoState[0].phoneNumber && roomsBookingInfoState[0].phoneNumber.length > 4) || false;

    const room = roomsData && params ? getRoomData(roomsData, params) : undefined;

    const flightTicketCodeValidation = room ? (room.isPackageRate ? !!flightTicketCode.trim() : true) : false;

    this.setState({ phoneNumberValidation, flightTicketCodeValidation });

    if (validBookingInfo(roomsBookingInfoValidation) && phoneNumberValidation && flightTicketCodeValidation) {
      dispatch(setRoomsBookingInfo(roomsBookingInfoState));
      if (flightTicketCode) {
        dispatch(setFlightTicketCode(flightTicketCode));
      }
      dispatch(
        push({
          pathname: ROUTES.hotel.hotelPay,
          state: { ...routerState, backableToBookingInfo: true },
        }),
      );
    } else {
      this.setState({
        errorMessage: intl.formatMessage({
          id: !flightTicketCodeValidation ? 'booking.invalidFlightTicket' : 'booking.invalidGuestInfo',
        }),
      });
    }
  };

  public render() {
    const { intl, params, hotelData, roomsData } = this.props;
    const {
      roomsBookingInfo,
      errorMessage,
      roomsBookingInfoValidation,
      phoneNumberValidation,
      flightTicketCode,
      flightTicketCodeValidation,
    } = this.state;

    if (!params) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidLink)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.hotelEnterInformation' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return (
                <HotelBookingInfoDesktop
                  roomsData={roomsData}
                  hotelData={hotelData}
                  params={params}
                  roomsBookingInfo={roomsBookingInfo}
                  roomsBookingInfoValidation={roomsBookingInfoValidation}
                  phoneNumberValidation={phoneNumberValidation}
                  flightTicketCodeValidation={flightTicketCodeValidation}
                  updateRoomsBookingInfo={(info: RoomBookingInfo[]) => {
                    this.setState({ roomsBookingInfo: info });
                  }}
                  continue_={this.continue}
                  flightTicketCode={flightTicketCode}
                  updateFlightTicketCode={(ticketCode: string) => {
                    this.setState({ flightTicketCode: ticketCode });
                  }}
                />
              );
            }
            return (
              <HotelBookingInfoTablet
                roomsData={roomsData}
                hotelData={hotelData}
                params={params}
                roomsBookingInfo={roomsBookingInfo}
                roomsBookingInfoValidation={roomsBookingInfoValidation}
                phoneNumberValidation={phoneNumberValidation}
                flightTicketCodeValidation={flightTicketCodeValidation}
                updateRoomsBookingInfo={(info: RoomBookingInfo[]) => {
                  this.setState({ roomsBookingInfo: info });
                }}
                continue_={this.continue}
                flightTicketCode={flightTicketCode}
                updateFlightTicketCode={(ticketCode: string) => {
                  this.setState({ flightTicketCode: ticketCode });
                }}
              />
            );
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">{errorMessage}</Typography>
            </div>
          }
        />
      </>
    );
  }
}

export default connect(mapState2Props)(injectIntl(HotelBookingInfo));
