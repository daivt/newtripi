import { Button, Typography } from '@material-ui/core';
import { goBack, push } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import FlightReviewDesktop from '../components/flight/FlightReviewDesktop';
import FlightReviewTablet from '../components/flight/FlightReviewTablet';
import { setSelectedPaymentMethod } from '../redux/flightBookingReducer';

const mapStateToProps = (state: AppState) => {
  return { booking: state.booking.flight, router: state.router };
};
export interface IFlightReviewProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

class FlightReview extends React.Component<IFlightReviewProps, some> {
  componentDidMount() {
    this.props.dispatch(setSelectedPaymentMethod());
  }

  public render() {
    const { booking, intl, dispatch, router } = this.props;

    if (!booking.travellersInfo || !booking.contactInfo) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    const buttonGroup = (
      <div style={{ margin: '30px 0', textAlign: 'center' }}>
        <Button
          style={{ width: '190px' }}
          variant="outlined"
          color="default"
          onClick={() => dispatch(goBack())}
          size="large"
        >
          <Typography variant="button" color="textSecondary">
            <FormattedMessage id="back" />
          </Typography>
        </Button>
        <Button
          style={{ width: '190px', marginLeft: '40px' }}
          color="secondary"
          variant="contained"
          onClick={() =>
            dispatch(
              push({
                pathname: ROUTES.flight.flightPay,
                state: { ...router.location.state, backableToReview: true },
              }),
            )
          }
          size="large"
        >
          <Typography variant="button">
            <FormattedMessage id="booking.proceedToPayment" />
          </Typography>
        </Button>
      </div>
    );

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.flightReviewInfo' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <FlightReviewDesktop buttonGroup={buttonGroup} booking={booking} />;
            }
            return <FlightReviewTablet buttonGroup={buttonGroup} booking={booking} />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(FlightReview));
