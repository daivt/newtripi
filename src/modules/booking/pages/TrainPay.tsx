import * as React from 'react';
import Helmet from 'react-helmet';
import { useIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import TrainPayDesktop from '../components/train/TrainPayDesktop';
import TrainPayTablet from '../components/train/TrainPayTablet';
import { setTrainPayMessage } from '../redux/trainBookingReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};
export interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TrainPay: React.FunctionComponent<Props> = props => {
  const { booking, dispatch } = props;
  const { bookingData } = booking;
  const intl = useIntl();

  React.useEffect(() => {
    dispatch(setTrainPayMessage());
  }, [dispatch]);

  if (!bookingData) {
    return (
      <Redirect
        to={{
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }}
      />
    );
  }

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'booking.hotel.pay' })}</title>
      </Helmet>
      <MediaQuery minWidth={DESKTOP_WIDTH}>
        {match => {
          if (match) {
            return <TrainPayDesktop />;
          }
          return <TrainPayTablet />;
        }}
      </MediaQuery>
    </>
  );
};

export default connect(mapStateToProps)(TrainPay);
