import * as React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import MediaQuery from 'react-responsive';
import { DESKTOP_WIDTH } from '../../../constants';
import MobileHistoryDeskTop from '../components/mobile/MobileHistoryTabletDeskTop';
import { connect } from 'react-redux';

export interface Props extends WrappedComponentProps {}

class MobileHistory extends React.PureComponent<Props> {
  public render() {
    const { intl } = this.props;
    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'mobile.historyBooking' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <MobileHistoryDeskTop />;
            }
            return <MobileHistoryDeskTop />;
          }}
        </MediaQuery>
      </>
    );
  }
}
export default connect()(injectIntl(MobileHistory));
