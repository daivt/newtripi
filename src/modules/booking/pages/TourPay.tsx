import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import TourPayDesktop from '../components/tour/TourPayDesktop';
import TourPayTablet from '../components/tour/TourPayTablet';
import { endPaying, fetchPaymentMethods, fetchPointPayment, setPayTourMessage } from '../redux/tourBookingReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.tour,
    userData: state.account.userData,
  };
};

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class TourPay extends PureComponent<Props, State> {
  async componentDidMount() {
    const { dispatch } = this.props;
    await dispatch(fetchPaymentMethods());
    await dispatch(fetchPointPayment());
    dispatch(endPaying());
    dispatch(setPayTourMessage());
  }

  render() {
    const { booking, intl } = this.props;

    if (!booking.params || !booking.activityPackage || !booking.tourCustomerInfo || !booking.date) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.tour.pay' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <TourPayDesktop />;
            }
            return <TourPayTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(TourPay));
