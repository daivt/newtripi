import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import HotelPayDesktop from '../components/hotel/HotelPayDesktop';
import HotelPayTablet from '../components/hotel/HotelPayTablet';
import {
  endPaying,
  fetchPaymentMethods,
  fetchPointPayment as fetchHotelPointPayment,
  setPayResponseMessage,
} from '../redux/hotelBookingReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.hotel,
    userData: state.account.userData,
  };
};
export interface IHotelPayProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class HotelPay extends React.Component<IHotelPayProps, State> {
  async componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchPaymentMethods());
    dispatch(fetchHotelPointPayment());
    dispatch(endPaying());
    dispatch(setPayResponseMessage());
  }

  public render() {
    const { booking, intl } = this.props;

    if (!booking.params || !booking.roomsBookingInfo) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.hotel.pay' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <HotelPayDesktop />;
            }
            return <HotelPayTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(HotelPay));
