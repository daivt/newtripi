import { Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import MessageDialog from '../../common/components/MessageDialog';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import {
  TourCustomerInfo,
  TourCustomerInfoValidation,
  validTourCustomerInfoValidation,
} from '../../common/models/index';
import TourBookingInfoDesktop from '../components/tour/TourBookingInfoDesktop';
import TourBookingInfoTablet from '../components/tour/TourBookingInfoTablet';
import { setTourCustomerInfo, setSelectedPaymentMethod, copyTourDataFromResult } from '../redux/tourBookingReducer';
import { validateTourCustomerInfo, validTourBookingInfo, validTourCustomer } from '../utils';
import { fetchTourDetail } from '../../result/redux/tourResultReducer';

export interface ITourBookingInfoProps extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

export interface ITourBookingInfoState {
  tourCustomerInfo: TourCustomerInfo;
  tourCustomerInfoValidation: TourCustomerInfoValidation;
  errorMessage: string;
}

class TourBookingInfo extends React.Component<ITourBookingInfoProps, ITourBookingInfoState> {
  state: ITourBookingInfoState = {
    tourCustomerInfo: this.props.tourCustomerInfo
      ? this.props.tourCustomerInfo
      : { id: 0, fullName: '', phone: '', email: '' },
    tourCustomerInfoValidation: validTourCustomerInfoValidation,
    errorMessage: '',
  };

  async componentDidMount() {
    const { dispatch, result, booking } = this.props;
    dispatch(setSelectedPaymentMethod());
    if (booking.params) {
      if (!result.tourData || result.tourData.id !== booking.params.tourId) {
        await dispatch(fetchTourDetail());
      }
      dispatch(copyTourDataFromResult());
    }
  }

  continue = () => {
    const { tourCustomerInfo } = this.state;
    const { dispatch, booking, routerState } = this.props;

    const tourCustomerInfoValidation: TourCustomerInfoValidation = validateTourCustomerInfo(tourCustomerInfo);

    const errorMessage = validTourBookingInfo(booking, tourCustomerInfoValidation);

    this.setState({
      tourCustomerInfoValidation,
      errorMessage,
    });

    if (!errorMessage && validTourCustomer(tourCustomerInfoValidation)) {
      dispatch(setTourCustomerInfo(tourCustomerInfo));
      dispatch(
        push({
          pathname: ROUTES.tour.tourPay,
          state: { ...routerState, backableToBookingInfo: true },
        }),
      );
    }
  };

  public render() {
    const { booking, result, intl } = this.props;
    const { tourCustomerInfo, tourCustomerInfoValidation, errorMessage } = this.state;
    const { tourData } = result;

    if (!booking.params) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'tour.bookingInfo' })}</title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return (
                <TourBookingInfoDesktop
                  tourData={tourData}
                  tourCustomerInfo={tourCustomerInfo}
                  tourCustomerInfoValidation={tourCustomerInfoValidation}
                  updateTourCustomerInfo={newTourCustomerInfo =>
                    this.setState({ tourCustomerInfo: newTourCustomerInfo })
                  }
                  continue_={this.continue}
                />
              );
            }
            return (
              <TourBookingInfoTablet
                tourData={tourData}
                tourCustomerInfo={tourCustomerInfo}
                tourCustomerInfoValidation={tourCustomerInfoValidation}
                updateTourCustomerInfo={newTourCustomerInfo => this.setState({ tourCustomerInfo: newTourCustomerInfo })}
                continue_={this.continue}
              />
            );
          }}
        </MediaQuery>
        <MessageDialog
          show={!!errorMessage}
          onClose={() => this.setState({ errorMessage: '' })}
          message={
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="body1">
                {intl.formatMessage({
                  id: errorMessage || 'booking.invalidGuestInfo',
                })}
              </Typography>
            </div>
          }
        />
      </>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.tour,
    tourCustomerInfo: state.booking.tour.tourCustomerInfo,
    result: state.result.tour,
    routerState: state.router.location.state,
  };
}

export default connect(mapStateToProps)(injectIntl(TourBookingInfo));
