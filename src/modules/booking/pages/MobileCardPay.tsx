import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Redirect } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { DESKTOP_WIDTH } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import MobileCardPayDesktop from '../components/mobile/MobileCardPayDesktop';
import MobileCardPayTablet from '../components/mobile/MobileCardPayTablet';
import {
  endPaying,
  fetchPaymentMethodsCode,
  fetchPointPaymentCode,
  setCardInfo,
  setPaymentMethods,
  setPayMobileMessage,
  setPromotion,
  setPromotionCode,
  setSelectedPaymentMethod,
} from '../redux/mobileReducer';

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.mobile,
  };
};
export interface IHotelPayProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

class MobileCardPay extends React.Component<IHotelPayProps, State> {
  state: State = {};

  async componentDidMount() {
    this.props.dispatch(setPromotionCode(''));
    this.props.dispatch(setPromotion());
    this.props.dispatch(setCardInfo());
    this.props.dispatch(setPaymentMethods());
    this.props.dispatch(setSelectedPaymentMethod());
    this.props.dispatch(setPayMobileMessage());
    this.props.dispatch(fetchPaymentMethodsCode());
    this.props.dispatch(fetchPointPaymentCode());
    this.props.dispatch(endPaying());
  }

  public render() {
    const { booking, intl } = this.props;
    const buyer = booking.buyerMobileCardInfo;
    if (!buyer.cardValue || !buyer.operatorCode || !buyer.email || !buyer.amount) {
      return (
        <Redirect
          to={{
            pathname: '/',
            search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
          }}
        />
      );
    }

    return (
      <>
        <Helmet>
          <title>
            {intl.formatMessage({ id: 'mobile.titleCode' })}
            :&nbsp;
            {intl.formatMessage({ id: 'mobile.payTitle' })}
          </title>
        </Helmet>
        <MediaQuery minWidth={DESKTOP_WIDTH}>
          {match => {
            if (match) {
              return <MobileCardPayDesktop />;
            }
            return <MobileCardPayTablet />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default connect(mapStateToProps)(injectIntl(MobileCardPay));
