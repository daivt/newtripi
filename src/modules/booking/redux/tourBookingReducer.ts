import { replace } from 'connected-react-router';
import moment, { Moment } from 'moment';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { PAYMENT_HOLDING_CODE, SEND_HOME_REASONS } from '../../common/constants';
import { TourCustomerCountInfo, TourCustomerInfo } from '../../common/models/index';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_TOUR_STATE, HOLD_PARAM_NAME } from '../constants';
import { computePaymentFees, computeTourPaybleNumbers, getBonusPoint, getPointPaymentData } from '../utils';

export interface TourBookingParams {
  tourId: number;
  direct?: boolean;
}

export interface TourBookingState {
  params?: TourBookingParams;
  tourCustomerInfo?: TourCustomerInfo;
  tourCustomerCountInfo: TourCustomerCountInfo;
  additionalRequest: string;
  date?: Moment;
  activityPackage?: some;
  tourData?: some;
  paymentMethods?: ReadonlyArray<some>;
  selectedPaymentMethod?: Readonly<some>;
  promotionCode: string;
  promotion?: Readonly<some>;
  usePointPayment: boolean;
  pointUsing: number;
  pointPaymentData?: Readonly<some>;
  paying: boolean;
  payMessage?: string;
}
export const setPayTourMessage = createAction('mobile/setPayTourMessage', resolve => (data?: string) =>
  resolve({ data }),
);
export const setBookingParams = createAction('booking/tour/setBookingParams', resolve => (params: TourBookingParams) =>
  resolve({ params }),
);

export const setDate = createAction('booking/tour/setDate', resolve => (date: Moment) => resolve({ date }));

export const setActivityPackage = createAction('booking/tour/setActivityPackage', resolve => (data?: some) =>
  resolve({ data }),
);

export const setTourData = createAction('booking/tour/setTourData', resolve => (data: some) => resolve({ data }));

export const setTourCustomerInfo = createAction(
  'booking/tour/setTourCustomerInfo',
  resolve => (data: TourCustomerInfo) => resolve({ data }),
);

export const setTourCustomerCountInfo = createAction(
  'booking/tour/setTourCustomerCountInfo',
  resolve => (data: TourCustomerCountInfo) => resolve({ data }),
);

export const setAdditionalRequest = createAction('booking/tour/setAdditionalRequest', resolve => (value: string) =>
  resolve({ value }),
);

export const setPaymentMethods = createAction('booking/tour/setPaymentMethods', resolve => (data: some[]) =>
  resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'booking/tour/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction('booking/tour/setPointPaymentData', resolve => (data?: some) =>
  resolve({ data }),
);

export const setUsePointPayment = createAction('booking/tour/setUsePointPayment', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPointUsing = createAction('booking/tour/setPointUsing', resolve => (val: number) => resolve({ val }));

export const setPromotionCode = createAction('booking/tour/setPromotionCode', resolve => (data: string) =>
  resolve({ data }),
);

export const setPromotion = createAction('booking/tour/setPromotion', resolve => (data?: some) => resolve({ data }));

export const startPaying = createAction('booking/tour/startPaying');

export const endPaying = createAction('booking/tour/endPaying');

export const resetBooking = createAction('booking/tour/resetBooking');

export function copyTourDataFromResult(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    if (state.result.tour.tourData) {
      dispatch(setTourData(state.result.tour.tourData));
    }
  };
}

export function fetchPaymentMethods(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const tourBooking = state.booking.tour;
    if (!tourBooking.params || !tourBooking.date) {
      return;
    }

    const params = {
      numberBabies: tourBooking.tourCustomerCountInfo.babyCount,
      tourId: tourBooking.params.tourId,
      departureDate: moment(tourBooking.date).format(DATE_FORMAT),
      numberSlots: tourBooking.tourCustomerCountInfo.adultCount,
      numberInfants: tourBooking.tourCustomerCountInfo.infantCount,
      isDeposit: false,
      numberChilds: tourBooking.tourCustomerCountInfo.childrenCount,
    };

    const json = await dispatch(fetchThunk(API_PATHS.getTourPaymentMethods, 'post', true, JSON.stringify(params)));

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
    }
  };
}

export function fetchPointPayment(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.tour;

    if (!booking.params) {
      return undefined;
    }
    const payableNumbers = computeTourPaybleNumbers(booking);
    const originAmount = payableNumbers.originPrice;
    const { finalPrice } = payableNumbers;
    const { paymentMethods } = getState().booking.tour;
    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: paymentMethods
            ? paymentMethods.map(v => {
                return {
                  originAmount: originAmount + computePaymentFees(v, originAmount),
                  amount: finalPrice + computePaymentFees(v, originAmount),
                  code: v.code,
                };
              })
            : [],
          module: 'tour',
          usePromoCode: payableNumbers.discountAmount > 0,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }
    return json;
  };
}

export function getTourAvailabilities(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.tour;

    if (!booking.params) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getTourAvailabilities}`,
        'post',
        true,
        JSON.stringify({
          tourId: booking.params.tourId,
        }),
      ),
    );

    return json;
  };
}

export function fetchRewardsHistory(
  search: string = '',
  page: number = 1,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.tour;

    if (!booking.paymentMethods) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search || '',
          info: {
            productType: 'tour',
          },
          module: 'tour',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}

export function checkPromotion(code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.tour;

    if (!booking.activityPackage) {
      return undefined;
    }

    const payableNumbers = computeTourPaybleNumbers(booking);

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          productType: 'tour',
          module: 'tour',
          numAdults: booking.tourCustomerCountInfo.adultCount,
          numInfants: booking.tourCustomerCountInfo.infantCount,
          numBabies: booking.tourCustomerCountInfo.babyCount,
          numChildren: booking.tourCustomerCountInfo.childrenCount,
          originPrice: payableNumbers.originPrice,
          departureDate: moment(booking.date).format(DATE_FORMAT),
          originPoint: getBonusPoint(booking),
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}
export function tourPay(smartOTPPass?: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const booking = state.booking.tour;
    const {
      tourCustomerCountInfo,
      tourCustomerInfo,
      promotionCode,
      selectedPaymentMethod,
      activityPackage,
      date,
      params,
    } = booking;
    const { userData } = state.account;
    if (!params || !tourCustomerInfo || !selectedPaymentMethod || !activityPackage || !date || !userData) {
      return undefined;
    }
    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.bookTour,
        'post',
        true,
        JSON.stringify({
          creditPassword: smartOTPPass,
          numSlots: tourCustomerCountInfo.adultCount,
          numChildren: tourCustomerCountInfo.childrenCount,
          numInfants: tourCustomerCountInfo.infantCount,
          numBabies: tourCustomerCountInfo.babyCount,
          contactPhone: tourCustomerInfo.phone,
          contactName: tourCustomerInfo.fullName,
          contactEmail: userData.emailInfo,
          fromMobile: false,
          note: booking.additionalRequest,
          promoCode: promotionCode,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          activityPackageId: activityPackage.activityPackageId,
          departureDate: date.format(DATE_FORMAT),
          point: booking.usePointPayment ? booking.pointUsing : 0,
          tourId: params.tourId,
          isDeposit: false,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      const { paymentLink } = json.data;
      if (selectedPaymentMethod.code === PAYMENT_HOLDING_CODE) {
        window.location.replace(`${paymentLink}?${HOLD_PARAM_NAME}=true`);
      } else {
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayTourMessage(json.message));
    }
    return json;
  };
}
const actions = {
  setBookingParams,
  setDate,
  setTourData,
  setActivityPackage,
  setTourCustomerInfo,
  setTourCustomerCountInfo,
  setAdditionalRequest,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  resetBooking,
  setPayTourMessage,
};

type ActionT = ActionType<typeof actions>;

function reducer(state: TourBookingState = DEFAULT_TOUR_STATE, action: ActionT): TourBookingState {
  switch (action.type) {
    case getType(resetBooking):
      return DEFAULT_TOUR_STATE;
    case getType(setPayTourMessage):
      return { ...state, payMessage: action.payload.data };
    case getType(setBookingParams):
      return { ...state, params: action.payload.params };
    case getType(setDate):
      return { ...state, date: action.payload.date };
    case getType(setTourData):
      return { ...state, tourData: action.payload.data };
    case getType(setActivityPackage):
      return { ...state, activityPackage: action.payload.data };
    case getType(setTourCustomerInfo):
      return { ...state, tourCustomerInfo: action.payload.data };
    case getType(setTourCustomerCountInfo):
      return { ...state, tourCustomerCountInfo: action.payload.data };
    case getType(setAdditionalRequest):
      return { ...state, additionalRequest: action.payload.value };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    default:
      return state;
  }
}
export default reducer;
