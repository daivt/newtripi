import { replace } from 'connected-react-router';
import moment, { Moment } from 'moment';
import querystring from 'querystring';
import { ThunkAction } from 'redux-thunk';
import { Action, ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS, PAYMENT_HOLDING_CODE } from '../../common/constants';
import { ContactInfo, GuestCountInfo, RoomBookingInfo } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { computeHotelPayableNumbers, computePaymentFees, getPointPaymentData } from '../utils';
import { getRoomData } from '../../common/utils';
import { DEFAULT_HOTEL_STATE, HOLD_PARAM_NAME, TRIP_CREDITS_CODE } from '../constants';

export interface HotelBookingParams {
  hotelId: number;
  shrui?: string;
  guestInfo: GuestCountInfo;
  checkIn: Moment;
  checkOut: Moment;
  direct?: boolean;
}
export interface HotelBookingState {
  readonly contactInfo?: ContactInfo;
  readonly additionalRequest: string;
  readonly params?: HotelBookingParams;
  readonly roomsData?: some[];
  readonly hotelData?: some;
  readonly paymentMethods?: ReadonlyArray<some>;
  readonly selectedPaymentMethod?: Readonly<some>;
  readonly promotionCode: string;
  readonly promotion?: Readonly<some>;
  readonly usePointPayment: boolean;
  readonly pointUsing: number;
  readonly pointPaymentData?: Readonly<some>;
  readonly paying: boolean;
  readonly payResponseMessage?: string;
  readonly roomsBookingInfo?: RoomBookingInfo[];
  readonly flightTicketCode?: string;
}

export const setRoomsBookingInfo = createAction(
  'booking/hotel/setRoomsBookingInfo',
  resolve => (info: RoomBookingInfo[]) => resolve({ info }),
);

export const setFlightTicketCode = createAction(
  'booking/hotel/setFlightTicketCode',
  resolve => (flightTicketCode: string) => resolve({ flightTicketCode }),
);

export const setContactInfo = createAction('booking/hotel/setContactInfo', resolve => (info: ContactInfo) =>
  resolve({ info }),
);

export const setAdditionalRequest = createAction('booking/hotel/setAdditionalRequest', resolve => (value: string) =>
  resolve({ value }),
);

export const setParams = createAction(
  'booking/hotel/setParams',
  resolve => (params: HotelBookingParams, skipSaga: boolean = true) => resolve({ params, skipSaga }),
);

export const setPaymentMethods = createAction('booking/hotel/setPaymentMethods', resolve => (data: some[]) =>
  resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'booking/hotel/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction('booking/hotel/setPointPaymentData', resolve => (data?: some) =>
  resolve({ data }),
);

export const setRoomsData = createAction('booking/hotel/setRoomsData', resolve => (data?: some[]) => resolve({ data }));

export const setHotelData = createAction('booking/hotel/setHotelData', resolve => (data?: some) => resolve({ data }));

export const setUsePointPayment = createAction('booking/hotel/setUsePointPayment', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPointUsing = createAction('booking/hotel/setPointUsing', resolve => (val: number) => resolve({ val }));

export const setPromotionCode = createAction('booking/hotel/setPromotionCode', resolve => (data: string) =>
  resolve({ data }),
);

export const setPromotion = createAction('booking/hotel/setPromotion', resolve => (data?: some) => resolve({ data }));

export const startPaying = createAction('booking/hotel/startPaying');

export const endPaying = createAction('booking/hotel/endPaying');

export const resetBooking = createAction('booking/hotel/resetBooking');

export const setPayResponseMessage = createAction('booking/hotel/setPayResponseMessage', resolve => (val?: string) =>
  resolve({ val }),
);

export function fetchPaymentMethods(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const hotelBooking = state.booking.hotel;

    if (!hotelBooking.params || !hotelBooking.roomsData) {
      return;
    }
    const room = getRoomData(hotelBooking.roomsData, hotelBooking.params);
    const numNights = moment(hotelBooking.params.checkOut).diff(hotelBooking.params.checkIn, 'days');
    const numRooms = hotelBooking.params.guestInfo.roomCount;
    const params = querystring.stringify({
      numNights,
      numRooms,
      daysToCheckin: 1,
      module: 'hotel',
      holdingTime: room ? room.holdingTime : 0,
      supportCOD: true,
    });

    const json = await dispatch(fetchThunk(`${API_PATHS.getHotelPaymentMethods}?${params}`, 'get', true));

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidInput)}`));
    }
  };
}

export function fetchPointPayment(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.hotel;

    if (!booking.paymentMethods || !booking.roomsData) {
      return undefined;
    }

    const payableNumbers = computeHotelPayableNumbers(booking, true);

    const originAmount = payableNumbers.finalPriceIncludeTax;
    const amount = booking.promotion && booking.promotion.price !== undefined ? booking.promotion.price : originAmount;

    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: booking.paymentMethods
            ? booking.paymentMethods.map(obj => ({
                originAmount: originAmount + computePaymentFees(obj, originAmount),
                amount: amount + computePaymentFees(obj, originAmount),
                code: obj.code,
              }))
            : [],
          module: 'hotel',
          usePromoCode: originAmount !== amount,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}

export function fetchRewardsHistory(
  search: string = '',
  page: number = 1,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.hotel;

    if (!booking.paymentMethods) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search || '',
          info: {
            productType: 'hotel',
          },
          module: 'hotel',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}

export function checkPromotion(code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.hotel;

    if (!booking.paymentMethods || !booking.roomsData || !booking.params || !booking.hotelData) {
      return undefined;
    }

    const roomDetail = getRoomData(booking.roomsData, booking.params);

    if (!roomDetail) {
      return undefined;
    }

    const payableNumbers = computeHotelPayableNumbers(booking, true);
    const originPrice = payableNumbers.finalPriceIncludeTax;
    const { provinceId } = booking.hotelData;
    const numRooms = payableNumbers.roomCount;
    const departureDate = booking.params.checkIn.format(DATE_FORMAT);
    const numNights = payableNumbers.nightCount;
    const originPoint = roomDetail.bonusPoint;

    const json = await dispatch(
      fetchThunk(
        API_PATHS.checkPromotion,
        'post',
        true,
        JSON.stringify({
          originPrice,
          numRooms,
          code,
          departureDate,
          provinceId,
          numNights,
          originPoint,
          productType: 'hotel',
          module: 'hotel',
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}

export function hotelPay(smartOTPPass?: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { userData } = state.account;
    const booking = state.booking.hotel;
    const { params, selectedPaymentMethod, roomsData } = booking;

    if (!roomsData || !params || !selectedPaymentMethod || !userData) {
      return;
    }
    const room = getRoomData(roomsData, params);
    if (!room) {
      return;
    }

    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.bookHotel,
        'post',
        true,
        JSON.stringify({
          creditPassword: smartOTPPass,
          promoCode: booking.promotionCode,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          numRooms: params.guestInfo.roomCount,
          point: booking.usePointPayment && selectedPaymentMethod.code === TRIP_CREDITS_CODE ? booking.pointUsing : 0,
          providerId: room.agencyId,
          SHRUI: params.shrui,
          numAdults: params.guestInfo.adultCount,
          childrenAges: params.guestInfo.childrenAges,
          numChildren: params.guestInfo.childCount,
          price: room.finalPrice,
          finalPriceWithoutBookerMarkup: room.finalPriceWithoutBookerMarkup,
          hotelId: params.hotelId,
          checkIn: params.checkIn.format(DATE_FORMAT),
          checkOut: params.checkOut.format(DATE_FORMAT),
          fromMobile: false,
          roomTypeId: room.roomTypeId,
          holdingTime: room.holdingTime,
          rateTypeId: room.rateTypeId,
          isTripiPartner: !room.isTripiProvider,
          contactInfo: {
            email: userData.emailInfo,
            address: userData.address,
            name: userData.name,
            phoneNumber: userData.phoneInfo,
          },
          roomsBookingInfo: booking.roomsBookingInfo,
          callbackDomain: `${window.location.origin}`,
          pnr: booking.flightTicketCode,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      const { paymentLink } = json.data;
      if (selectedPaymentMethod.code === PAYMENT_HOLDING_CODE) {
        window.location.replace(`${paymentLink}?${HOLD_PARAM_NAME}=true`);
      } else {
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

const actions = {
  setContactInfo,
  setRoomsBookingInfo,
  setAdditionalRequest,
  setParams,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setRoomsData,
  setHotelData,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  resetBooking,
  setPayResponseMessage,
  setFlightTicketCode,
};

type ActionT = ActionType<typeof actions>;

function reducer(state: HotelBookingState = DEFAULT_HOTEL_STATE, action: ActionT): HotelBookingState {
  switch (action.type) {
    case getType(resetBooking):
      return DEFAULT_HOTEL_STATE;
    case getType(setContactInfo):
      return { ...state, contactInfo: action.payload.info };
    case getType(setRoomsBookingInfo):
      return { ...state, roomsBookingInfo: action.payload.info };
    case getType(setAdditionalRequest):
      return { ...state, additionalRequest: action.payload.value };
    case getType(setParams):
      return { ...state, params: { ...state.params, ...action.payload.params } };
    case getType(setRoomsData):
      return { ...state, roomsData: action.payload.data };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setHotelData):
      return { ...state, hotelData: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    case getType(setPayResponseMessage):
      return {
        ...state,
        payResponseMessage: action.payload.val,
      };
    case getType(setFlightTicketCode):
      return {
        ...state,
        flightTicketCode: action.payload.flightTicketCode,
      };
    default:
      return state;
  }
}

export default reducer;
