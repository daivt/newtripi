import flightReducer, { FlightBookingState } from './flightBookingReducer';
import { combineReducers } from 'redux';
import hotelReducer, { HotelBookingState } from './hotelBookingReducer';
import tourReducer, { TourBookingState } from './tourBookingReducer';
import mobileReducer, { MobileState } from './mobileReducer';
import trainReducer, { TrainBookingState } from './trainBookingReducer';
import {
  DEFAULT_FLIGHT_STATE,
  DEFAULT_HOTEL_STATE,
  DEFAULT_TOUR_STATE,
  DEFAULT_MOBILE_STATE,
  DEFAULT_TRAIN_STATE,
} from '../constants';

export interface BookingState {
  flight: FlightBookingState;
  hotel: HotelBookingState;
  tour: TourBookingState;
  mobile: MobileState;
  train: TrainBookingState;
}

export const DEFAULT_BOOKING_STATE: BookingState = {
  flight: DEFAULT_FLIGHT_STATE,
  hotel: DEFAULT_HOTEL_STATE,
  tour: DEFAULT_TOUR_STATE,
  mobile: DEFAULT_MOBILE_STATE,
  train: DEFAULT_TRAIN_STATE,
};

export default combineReducers({
  flight: flightReducer,
  hotel: hotelReducer,
  tour: tourReducer,
  mobile: mobileReducer,
  train: trainReducer,
});
