import { replace } from 'connected-react-router';
import { merge } from 'lodash';
import moment from 'moment';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import {
  DATE_FORMAT,
  DATE_TIME_FORMAT,
  PAGE_SIZE,
  SLASH_DATE_FORMAT,
  SLASH_DATE_TIME_FORMAT,
  some,
  ROUTES,
  MY_TOUR,
} from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { PAYMENT_HOLDING_CODE, SEND_HOME_REASONS } from '../../common/constants';
import { ContactInfo, TravellersInfo } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_FLIGHT_STATE, HOLD_PARAM_NAME } from '../constants';
import {
  computePayableNumbers,
  computePaymentFees,
  computePoints,
  genFlightPayParams,
  getGuestParams,
  getPointPaymentData,
} from '../utils';

export interface FlightBookingParams {
  readonly inbound?: TicketId;
  readonly outbound: TicketId;
  readonly requestId: number;
}

export interface TicketId {
  readonly id: string;
  readonly aid: number;
}

export type OneDirection =
  | {
      readonly ticket: Readonly<some>;
      readonly extraBaggages: number[];
      readonly searchRequest: Readonly<some>;
    }
  | { readonly ticket: null; readonly extraBaggages: null };

export interface FlightBookingState {
  readonly inbound: OneDirection;
  readonly outbound: OneDirection;
  readonly tid?: FlightBookingParams;
  readonly travellersInfo?: TravellersInfo;
  readonly contactInfo?: ContactInfo;
  readonly booker?: Readonly<some>;
  readonly paymentMethods?: ReadonlyArray<some>;
  readonly selectedPaymentMethod?: Readonly<some>;
  readonly promotionCode: string;
  readonly promotion?: Readonly<some>;
  readonly usePointPayment: boolean;
  readonly pointUsing: number;
  readonly insurancePackage?: Readonly<some>;
  readonly buyInsurance: boolean;
  readonly pointPaymentData?: Readonly<some>;
  readonly paying: boolean;
  readonly validatingInsurance: boolean;
  readonly fetchingTicketData: boolean;
  readonly payResponseMessage?: string;
}

export const setInbound = createAction(
  'booking/flight/setInbound',
  resolve => (val: Readonly<some>, requestId: number, req: Readonly<some>) => resolve({ val, requestId, req }),
);

export const setOutbound = createAction(
  'booking/flight/setOutbound',
  resolve => (val: Readonly<some>, requestId: number, req: Readonly<some>) => resolve({ val, requestId, req }),
);

export const clearInbound = createAction('booking/flight/clearInbound');
export const clearOutbound = createAction('booking/flight/clearOutbound');

export const setBookingStateFromParams = createAction(
  'booking/flight/setBookingStateFromParams',
  resolve => (val: FlightBookingParams) => resolve({ val }),
);

export const setTravellersInfo = createAction('booking/flight/setTravellersInfo', resolve => (info: TravellersInfo) =>
  resolve({ info }),
);

export const setOutboundExtraBaggages = createAction(
  'booking/flight/setOutboundExtraBaggages',
  resolve => (data: number[]) => resolve({ data }),
);

export const setInboundExtraBaggages = createAction(
  'booking/flight/setInboundExtraBaggages',
  resolve => (data: number[]) => resolve({ data }),
);

export const setContactInfo = createAction('booking/flight/setContactInfo', resolve => (info: ContactInfo) =>
  resolve({ info }),
);

export const setPaymentMethods = createAction('booking/flight/setPaymentMethods', resolve => (data: some[]) =>
  resolve({ data }),
);

export const setUsePointPayment = createAction('booking/flight/setUsePointPayment', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPointUsing = createAction('booking/flight/setPointUsing', resolve => (val: number) => resolve({ val }));

export const setPromotionCode = createAction('booking/flight/setPromotionCode', resolve => (data: string) =>
  resolve({ data }),
);

export const setPromotion = createAction('booking/flight/setPromotion', resolve => (data?: some) => resolve({ data }));

export const setBooker = createAction('booking/flight/setBooker', resolve => (booker: some) => resolve({ booker }));

export const setSelectedPaymentMethod = createAction(
  'booking/flight/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction('booking/flight/setPointPaymentData', resolve => (data?: some) =>
  resolve({ data }),
);

export const resetBooking = createAction('booking/flight/resetBooking');

export const setFetchingTicketData = createAction('booking/flight/setFetchingTicketData', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setInsurancePackage = createAction('booking/flight/setInsurancePackage', resolve => (data?: some) =>
  resolve({ data }),
);
export const startPaying = createAction('booking/flight/startPaying');
export const endPaying = createAction('booking/flight/endPaying');
export const setBuyInsurance = createAction('booking/flight/setBuyInsurance', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setValidatingInsurance = createAction('booking/flight/setValidatingInsurance', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPayResponseMessage = createAction('booking/flight/setPayResponseMessage', resolve => (val?: string) =>
  resolve({ val }),
);

export function fetchTicketDataAndInsurancePackage(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { tid } = state.booking.flight;

    if (!tid) {
      return;
    }

    if (tid.outbound) {
      dispatch(setFetchingTicketData(true));
      const [outJson, inJson] = await Promise.all([
        dispatch(
          fetchThunk(
            API_PATHS.getTicketDetail,
            'post',
            true,
            JSON.stringify({
              agencyId: `${tid.outbound.aid}`,
              requestId: `${tid.requestId}`,
              ticketId: `${tid.outbound.id}`,
            }),
          ),
        ),
        tid.inbound
          ? dispatch(
              fetchThunk(
                API_PATHS.getTicketDetail,
                'post',
                true,
                JSON.stringify({
                  agencyId: `${tid.inbound.aid}`,
                  requestId: `${tid.requestId}`,
                  ticketId: `${tid.inbound.id}`,
                }),
              ),
            )
          : Promise.resolve(undefined),
      ]);
      dispatch(setFetchingTicketData(false));
      if (outJson?.code === 200) {
        const adultCount = outJson.data.searchRequest.numAdults;
        const childCount = outJson.data.searchRequest.numChildren;

        dispatch(
          setOutbound(
            merge(state.booking.flight.outbound.ticket || {}, outJson.data.ticket),
            tid.requestId,
            outJson.data.searchRequest,
          ),
        );
        dispatch(setOutboundExtraBaggages(Array(adultCount + childCount).fill(0)));
        if (inJson && inJson.code === 200) {
          dispatch(
            setInbound(
              merge(state.booking.flight.inbound.ticket || {}, inJson.data.ticket),
              tid.requestId,
              outJson.data.searchRequest,
            ),
          );
          dispatch(setInboundExtraBaggages(Array(adultCount + childCount).fill(0)));
        }

        await dispatch(fetchInsurancePackage());
      } else if (outJson?.code === 400 || (inJson && inJson.code === 400)) {
        dispatch(replace(`/404?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.expiredTicket)}`));
      }
    }
  };
}

export function fetchPaymentMethods(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { tid } = state.booking.flight;
    if (!tid) {
      return;
    }

    const json = await dispatch(
      fetchThunk(
        API_PATHS.getFlightPaymentMethods,
        'post',
        true,
        JSON.stringify({
          supportCOD: true,
          tickets: {
            inbound: tid.inbound
              ? {
                  agencyId: `${tid.inbound.aid}`,
                  requestId: `${tid.requestId}`,
                  ticketId: `${tid.inbound.id}`,
                }
              : undefined,
            outbound: {
              agencyId: `${tid.outbound.aid}`,
              requestId: `${tid.requestId}`,
              ticketId: `${tid.outbound.id}`,
            },
          },
        }),
      ),
    );
    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(
        replace(
          `${MY_TOUR ? ROUTES.flight.flight : '/404'}?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.expiredTicket)}`,
        ),
      );
    }
  };
}

export function fetchPointPayment(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.flight;

    if (!booking.paymentMethods) {
      return undefined;
    }

    const payableNumbers = computePayableNumbers(booking, true);

    const originAmount = payableNumbers.originPrice;

    const amount = booking.promotion && booking.promotion.price !== undefined ? booking.promotion.price : originAmount;

    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: booking.paymentMethods
            ? booking.paymentMethods.map(obj => ({
                originAmount: originAmount + computePaymentFees(obj, originAmount),
                amount: amount + computePaymentFees(obj, originAmount),
                code: obj.code,
              }))
            : [],
          module: 'flight',
          usePromoCode: originAmount !== amount,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}

export function fetchRewardsHistory(
  search: string = '',
  page: number = 1,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search || '',
          info: {
            productType: 'flight',
          },
          module: 'flight',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}

// function checkPromotionOld(code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
//   return async (dispatch, getState) => {
//     const booking = getState().booking.flight;

//     if (!booking.paymentMethods || !booking.outbound.ticket) {
//       return undefined;
//     }
//     const payableNumbers = computePayableNumbers(booking, true);
//     const originPrice = payableNumbers.ticketTotal + payableNumbers.insuranceCost + payableNumbers.extraBaggagesCosts;
//     const { numAdults } = booking.outbound.searchRequest;
//     const { numChildren } = booking.outbound.searchRequest;
//     const { numInfants } = booking.outbound.searchRequest;

//     const json = await dispatch(
//       fetchThunk(
//         `${API_PATHS.checkPromotion}`,
//         'post',
//         true,
//         JSON.stringify({
//           originPrice,
//           code,
//           numChildren,
//           numAdults,
//           numInfants,
//           productType: 'flight',
//           isOneWay: !booking.inbound.ticket,
//           outboundFarePrice: booking.outbound.ticket.outbound.ticketdetail.farePrice,
//           inboundFarePrice: booking.inbound.ticket ? booking.inbound.ticket.outbound.ticketdetail.farePrice : undefined,
//           originPoint: computePoints(booking),
//         }),
//       ),
//     );

//     if (json.code === 200) {
//       dispatch(setPromotion({ ...json.data }));
//     } else {
//       dispatch(setPromotion({ promotionDescription: json.message }));
//     }
//     dispatch(setPromotionCode(code));

//     return json;
//   };
// }

export function checkPromotion(code: string): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.flight;
    const { tid, contactInfo, travellersInfo, selectedPaymentMethod } = booking;

    if (!tid || !contactInfo || !travellersInfo || !selectedPaymentMethod) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          module: 'flight',
          originPoint: computePoints(booking),
          data: { ...genFlightPayParams(booking) },
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}

function fetchInsurancePackage(): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.flight;

    const departureOutboundDate = booking.outbound.ticket
      ? moment(
          `${booking.outbound.ticket.outbound.departureDayStr} ${booking.outbound.ticket.outbound.departureTimeStr}`,
          SLASH_DATE_TIME_FORMAT,
        ).format(DATE_TIME_FORMAT)
      : '';

    let arrivalOutboundDate = booking.outbound.ticket
      ? moment(
          `${booking.outbound.ticket.outbound.arrivalDayStr} ${booking.outbound.ticket.outbound.arrivalTimeStr}`,
          SLASH_DATE_TIME_FORMAT,
        ).format(DATE_TIME_FORMAT)
      : '';

    if (booking.inbound.ticket) {
      arrivalOutboundDate = moment(
        `${booking.inbound.ticket.outbound.arrivalDayStr} ${booking.inbound.ticket.outbound.arrivalTimeStr}`,
        SLASH_DATE_TIME_FORMAT,
      ).format(DATE_TIME_FORMAT);
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getInsurancePackage}`,
        'post',
        true,
        JSON.stringify({
          fromAirportCode: booking.outbound.ticket ? booking.outbound.ticket.outbound.departureAirport : '',
          toAirportCode: booking.outbound.ticket ? booking.outbound.ticket.outbound.arrivalAirport : '',
          hasTransit: booking.outbound.ticket ? booking.outbound.ticket.outbound.transitTickets : false,
          isOneWay: !booking.inbound.ticket,
          fromDate: departureOutboundDate,
          toDate: arrivalOutboundDate,
          isMultipleInsurance: true,
        }),
        { code: 500 },
      ),
    );

    if (json.code === 200 && json.data.length) {
      dispatch(setInsurancePackage(json.data[0]));
    } else {
      dispatch(setInsurancePackage());
    }

    return json;
  };
}

export function validateInsuranceInfo(
  contactInfo: ContactInfo,
  info: TravellersInfo,
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.flight;

    const params = {
      fromAirportCode: booking.outbound.ticket ? booking.outbound.ticket.outbound.departureAirport : '',
      toAirportCode: booking.outbound.ticket ? booking.outbound.ticket.outbound.arrivalAirport : '',
      insuranceContact: {
        addr1: contactInfo.address,
        firstName: contactInfo.givenName,
        lastName: contactInfo.familyName,
        passport: '',
        email: contactInfo.email,
        phone1: contactInfo.telephone,
        dob: moment(info.adults[0].birthday, SLASH_DATE_FORMAT).format(DATE_FORMAT),
      },
      guests: getGuestParams(info, booking),
    };

    try {
      dispatch(setValidatingInsurance(true));
      const json = await dispatch(
        fetchThunk(`${API_PATHS.validateInsuranceInfo}`, 'post', true, JSON.stringify(params)),
      );

      return json;
    } finally {
      dispatch(setValidatingInsurance(false));
    }
  };
}

export function flightPay(smartOTPPass?: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.flight;
    const { tid, contactInfo, travellersInfo, selectedPaymentMethod } = booking;

    if (!tid || !contactInfo || !travellersInfo || !selectedPaymentMethod) {
      return;
    }

    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.bookTicket,
        'post',
        true,
        JSON.stringify({
          ...genFlightPayParams(booking),
          creditPassword: smartOTPPass,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      const { paymentLink } = json.data;
      if (selectedPaymentMethod.code === PAYMENT_HOLDING_CODE) {
        window.location.replace(`${paymentLink}?${HOLD_PARAM_NAME}=true`);
      } else {
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayResponseMessage(json.message));
    }
  };
}

const actions = {
  resetBooking,
  setFetchingTicketData,
  setInbound,
  setOutbound,
  clearInbound,
  clearOutbound,
  setBookingStateFromParams,
  setTravellersInfo,
  setContactInfo,
  setPaymentMethods,
  setInboundExtraBaggages,
  setOutboundExtraBaggages,
  setPromotionCode,
  setPromotion,
  setUsePointPayment,
  setPointUsing,
  setBooker,
  setSelectedPaymentMethod,
  setInsurancePackage,
  setValidatingInsurance,
  setBuyInsurance,
  setPointPaymentData,
  startPaying,
  endPaying,
  setPayResponseMessage,
};

type ActionT = ActionType<typeof actions>;

export default function reducer(state: FlightBookingState = DEFAULT_FLIGHT_STATE, action: ActionT): FlightBookingState {
  switch (action.type) {
    case getType(resetBooking):
      return DEFAULT_FLIGHT_STATE;
    case getType(setInbound):
      if (state.tid && state.outbound.ticket) {
        const searchRequest = action.payload.req;
        const adultCount = searchRequest.numAdults || 0;
        const childCount = searchRequest.numChildren || 0;

        return {
          ...state,
          inbound: {
            ...state.inbound,
            ticket: action.payload.val,
            extraBaggages: Array(adultCount + childCount).fill(0),
            searchRequest: action.payload.req,
          },
          tid: {
            ...state.tid,
            inbound: { id: action.payload.val.tid, aid: action.payload.val.outbound.aid },
            requestId: action.payload.requestId,
          },
        };
      }
      return state;
    case getType(setOutbound): {
      const searchRequest = action.payload.req;
      const adultCount = searchRequest.numAdults || 0;
      const childCount = searchRequest.numChildren || 0;

      return {
        ...state,
        outbound: {
          ...state.outbound,
          ticket: action.payload.val,
          extraBaggages: Array(adultCount + childCount).fill(0),
          searchRequest: action.payload.req,
        },
        tid: {
          outbound: { id: action.payload.val.tid, aid: action.payload.val.outbound.aid },
          requestId: action.payload.requestId,
        },
      };
    }
    case getType(clearInbound):
      if (state.tid) {
        return {
          ...state,
          inbound: { ticket: null, extraBaggages: null },
          tid: { ...state.tid, inbound: undefined },
        };
      }
      return state;
    case getType(clearOutbound):
      return {
        ...state,
        outbound: { ticket: null, extraBaggages: null },
        tid: undefined,
      };
    case getType(setBookingStateFromParams):
      return {
        ...state,
        tid: action.payload.val,
      };
    case getType(setTravellersInfo):
      return { ...state, travellersInfo: action.payload.info };
    case getType(setInboundExtraBaggages):
      if (state.inbound.ticket) {
        return { ...state, inbound: { ...state.inbound, extraBaggages: action.payload.data } };
      }
      return state;
    case getType(setOutboundExtraBaggages):
      if (state.outbound.ticket) {
        return { ...state, outbound: { ...state.outbound, extraBaggages: action.payload.data } };
      }
      return state;
    case getType(setContactInfo):
      return { ...state, contactInfo: action.payload.info };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(setBooker):
      return { ...state, booker: action.payload.booker };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setInsurancePackage):
      return {
        ...state,
        insurancePackage: action.payload.data,
        buyInsurance: MY_TOUR ? state.buyInsurance : !!action.payload.data,
      };
    case getType(setValidatingInsurance):
      return { ...state, validatingInsurance: action.payload.val };
    case getType(setBuyInsurance):
      return { ...state, buyInsurance: action.payload.val };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    case getType(setFetchingTicketData):
      return {
        ...state,
        fetchingTicketData: action.payload.val,
      };
    case getType(setPayResponseMessage):
      return {
        ...state,
        payResponseMessage: action.payload.val,
      };
    default:
      return state;
  }
}
