import { replace, push } from 'connected-react-router';
import { ThunkAction } from 'redux-thunk';
import { Action, ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { PAGE_SIZE, some, TOUR_PAGE_SIZE } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../common/constants';
import { BuyerInfoValidation, MobileCardBuyer, MobileTopupBuyer } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_MOBILE_STATE } from '../constants';
import { getOperatorCode, getPointPaymentData, isValidBuyerInfoCode, isValidBuyerInfoDirect } from '../utils';

export interface MobileState {
  readonly buyerMobileCardInfo: MobileCardBuyer;
  readonly buyerMobileTopupInfo: MobileTopupBuyer;
  readonly cardInfo?: some;
  readonly buyerInfoValidation: BuyerInfoValidation;
  readonly mobileData?: some[];
  readonly paymentMethods?: ReadonlyArray<some>;
  readonly selectedPaymentMethod?: Readonly<some>;
  readonly promotionCode: string;
  readonly promotion?: Readonly<some>;
  readonly usePointPayment: boolean;
  readonly pointUsing: number;
  readonly pointPaymentData?: Readonly<some>;
  readonly paying: boolean;
  readonly errorMessage?: string;
  readonly payMessage?: string;
}

export function fetchMobileData(direct?: string): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const json = await dispatch(fetchThunk(`${API_PATHS.getMobileData}`, 'get'));
    const { buyerMobileTopupInfo } = state.booking.mobile;
    const { buyerMobileCardInfo } = state.booking.mobile;
    const { userData } = state.account;
    if (json.code === 200) {
      dispatch(setMobileData(json.data));
      if (!buyerMobileTopupInfo.phone) {
        dispatch(
          setbuyerMobileTopupInfo({
            phone: userData ? userData.phoneInfo : '',
            operatorCode: userData ? getOperatorCode(userData.phoneInfo, json.data) : '',
          }),
        );
      }
      if (!buyerMobileCardInfo.name && !buyerMobileCardInfo.email && !buyerMobileCardInfo.phone) {
        dispatch(
          setBuyerInfo({
            phone: userData ? userData.phoneInfo : '',
            email: userData ? userData.emailInfo : '',
            name: userData ? userData.name : '',
            amount: 1,
          }),
        );
      }
    }
  };
}
export function fetchPaymentMethodsCode(
  promoCode?: string,
): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const buyer = state.booking.mobile.buyerMobileCardInfo;
    if (!isValidBuyerInfoCode(buyer)) {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
      return;
    }
    const params = JSON.stringify({
      promoCode,
      cardType: buyer.operatorCode,
      identity: buyer.email,
      quantity: buyer.amount,
      value: buyer.cardValue,
    });

    const json = await dispatch(fetchThunk(`${API_PATHS.getMobilePaymentMethods}`, 'post', true, params));

    if (json.code === 200 || json.code === '200') {
      dispatch(setCardInfo(json.data.bookingInfo));
      dispatch(setPaymentMethods(json.data.paymentMethods));
      if (
        !state.booking.mobile.paymentMethods ||
        !state.booking.mobile.selectedPaymentMethod ||
        (state.booking.mobile.paymentMethods &&
          json.data.paymentMethods.findIndex(
            (v: some) =>
              state.booking.mobile.selectedPaymentMethod && v.id === state.booking.mobile.selectedPaymentMethod.id,
          ) < 0)
      ) {
        dispatch(setSelectedPaymentMethod(json.data.paymentMethods[0]));
      }
    } else {
      dispatch(setErrorMessage(json.message));
    }
  };
}
export function fetchPointPaymentCode(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileCardInfo.cardValue || !booking.cardInfo) {
      return undefined;
    }
    const totalPrice = booking.buyerMobileCardInfo.amount * booking.buyerMobileCardInfo.cardValue;
    const amount = booking.cardInfo.finalPrice;

    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: [{ amount, totalPrice, code: 'CD' }],
          module: 'mobile_topup',
          usePromoCode: totalPrice !== amount,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      setPointUsing(pointPaymentData.min);
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}
export function fetchRewardsHistoryCode(
  search: string = '',
  page: number = 1,
): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileCardInfo) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search || null,
          info: {
            productType: 'card',
            cardType: booking.buyerMobileCardInfo.operatorCode,
            quantity: booking.buyerMobileCardInfo.amount,
            value: booking.buyerMobileCardInfo.cardValue,
          },
          module: 'card',
          size: TOUR_PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}
export function checkPromotionCode(
  code: string,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileCardInfo) {
      return undefined;
    }
    const cardType = booking.buyerMobileCardInfo.operatorCode;
    const value = booking.buyerMobileCardInfo.cardValue;
    const quantity = booking.buyerMobileCardInfo.amount;
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          cardType,
          value,
          quantity,
          productType: 'card',
          module: 'card',
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
      dispatch(fetchPointPaymentCode());
      dispatch(fetchPaymentMethodsCode(code));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
      dispatch(fetchPaymentMethodsCode());
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}
export function fetchPaymentMethodsDirect(promoCode?: string): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const buyer = state.booking.mobile.buyerMobileTopupInfo;
    const quantity = 1;
    if (!isValidBuyerInfoDirect(buyer)) {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
      return;
    }
    const params = JSON.stringify({
      promoCode,
      quantity,
      // cardType: buyer.operatorCode,
      value: buyer.cardValue,
      mobileNo: state.booking.mobile.buyerMobileTopupInfo.phone,
    });

    const json = await dispatch(fetchThunk(`${API_PATHS.getMobilePaymentMethods}`, 'post', true, params));
    if (json.code === 200 || json.code === '200') {
      dispatch(setCardInfo(json.data.bookingInfo));
      dispatch(setPaymentMethods(json.data.paymentMethods));
      if (
        !state.booking.mobile.paymentMethods ||
        !state.booking.mobile.selectedPaymentMethod ||
        (state.booking.mobile.paymentMethods &&
          json.data.paymentMethods.findIndex(
            (v: some) =>
              state.booking.mobile.selectedPaymentMethod && v.id === state.booking.mobile.selectedPaymentMethod.id,
          ) < 0)
      ) {
        dispatch(setSelectedPaymentMethod(json.data.paymentMethods[0]));
      }
    } else {
      dispatch(setErrorMessage(json.message));
    }
  };
}
export function fetchPointPaymentDirect(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileTopupInfo || !booking.cardInfo) {
      return undefined;
    }
    const totalPrice = booking.buyerMobileTopupInfo.cardValue;
    const amount = booking.cardInfo.finalPrice;

    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: [{ amount, totalPrice, code: 'CD' }],
          module: 'mobile_topup',
          usePromoCode: totalPrice !== amount,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      setPointUsing(pointPaymentData.min);
      dispatch(setPointPaymentData(pointPaymentData));
    }

    return json;
  };
}
export function fetchRewardsHistoryDirect(
  search: string = '',
  page: number = 1,
): ThunkAction<some, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileTopupInfo) {
      return undefined;
    }

    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getRewardsHistory}`,
        'post',
        true,
        JSON.stringify({
          page,
          isUsed: false,
          term: search || null,
          info: {
            productType: 'mobile_topup',
            cardTypemobileNo: booking.buyerMobileTopupInfo.phone,
            value: booking.buyerMobileTopupInfo.cardValue,
          },
          module: 'mobile_topup',
          size: PAGE_SIZE,
        }),
      ),
    );

    return json;
  };
}
export function checkPromotionDirect(
  code: string,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.mobile;

    if (!booking.paymentMethods || !booking.buyerMobileTopupInfo) {
      return undefined;
    }
    const mobileNo = booking.buyerMobileTopupInfo.phone;
    const value = booking.buyerMobileTopupInfo.cardValue;
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.checkPromotion}`,
        'post',
        true,
        JSON.stringify({
          code,
          mobileNo,
          value,
          productType: 'mobile_topup',
          module: 'mobile_topup',
        }),
      ),
    );

    if (json.code === 200) {
      dispatch(setPromotion({ ...json.data }));
      dispatch(fetchPaymentMethodsDirect(code));
    } else {
      dispatch(setPromotion({ promotionDescription: json.message }));
      dispatch(fetchPaymentMethodsDirect());
    }
    dispatch(setPromotionCode(code));

    return json;
  };
}

export function mobileCardPay(
  smartOTPPass?: string,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const booking = state.booking.mobile;
    const routerState = state.router.location.state;
    const {
      selectedPaymentMethod,
      buyerMobileCardInfo,
      cardInfo,
      pointUsing,
      promotionCode,
      usePointPayment,
    } = booking;
    if (!cardInfo || !isValidBuyerInfoCode(buyerMobileCardInfo) || !selectedPaymentMethod) {
      return undefined;
    }
    dispatch(startPaying());
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getCardsCode,
        'post',
        true,
        JSON.stringify({
          promotionCode,
          creditPassword: smartOTPPass,
          cardType: buyerMobileCardInfo.operatorCode,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          fromMobile: false,
          quantity: buyerMobileCardInfo.amount,
          value: buyerMobileCardInfo.cardValue,
          point: usePointPayment ? pointUsing : 0,
          contactInfo: {
            email: buyerMobileCardInfo.email,
            name: buyerMobileCardInfo.name,
            phoneNumber: buyerMobileCardInfo.phone,
          },
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(endPaying());

    if (json.code === 200 || json.code === '200') {
      if (selectedPaymentMethod.code === 'CD') {
        const url = new URL(json.data.bookingInfo.paymentLink);
        dispatch(
          push({
            pathname: url.pathname,
            state: { ...routerState, mobilePayResutlData: json.data.bookingInfo },
          }),
        );
      } else {
        const { paymentLink } = json.data;
        window.location.replace(paymentLink);
      }
    } else {
      dispatch(setPayMobileMessage(json.message));
    }
    return json;
  };
}
export function mobileTopupPay(
  smartOTPPass?: string,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const booking = state.booking.mobile;
    const routerState = state.router.location.state;
    const { selectedPaymentMethod, buyerMobileTopupInfo, cardInfo, pointUsing, promotionCode } = booking;
    if (!cardInfo || !buyerMobileTopupInfo || !selectedPaymentMethod) {
      return undefined;
    }
    dispatch(startPaying());
    if (isValidBuyerInfoDirect(buyerMobileTopupInfo)) {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.getCardsDirect,
          'post',
          true,
          JSON.stringify({
            promotionCode,
            creditPassword: smartOTPPass,
            mobileNo: buyerMobileTopupInfo.phone,
            paymentMethodId: selectedPaymentMethod.id,
            paymentMethodBankId: selectedPaymentMethod.bankId,
            fromMobile: false,
            value: buyerMobileTopupInfo.cardValue,
            point: booking.usePointPayment ? pointUsing : 0,
            callbackDomain: `${window.location.origin}`,
          }),
        ),
      );
      dispatch(endPaying());
      if (json.code === 200 || json.code === '200') {
        if (selectedPaymentMethod.code === 'CD') {
          const url = new URL(json.data.bookingInfo.paymentLink);
          dispatch(
            push({
              pathname: url.pathname,
              state: { ...routerState, mobilePayResutlData: json.data.bookingInfo },
            }),
          );
        } else {
          const { paymentLink } = json.data;
          window.location.replace(paymentLink);
        }
      } else {
        dispatch(setPayMobileMessage(json.message));
      }
      return json;
    }
    return undefined;
  };
}
export const startPaying = createAction('mobile/startPaying');
export const endPaying = createAction('mobile/endPaying');
export const setPayMobileMessage = createAction('mobile/setPayMobileMessage', resolve => (data?: string) =>
  resolve({ data }),
);
export const setErrorMessage = createAction('mobile/setErrorMessage', resolve => (data?: string) => resolve({ data }));
export const setCardInfo = createAction('mobile/setCardInfo', resolve => (data?: some) => resolve({ data }));
export const setPaymentMethods = createAction('mobile/setPaymentMethods', resolve => (data?: some[]) =>
  resolve({ data }),
);

export const setSelectedPaymentMethod = createAction('mobile/setSelectedPaymentMethod', resolve => (data?: some) =>
  resolve({ data }),
);

export const setPointPaymentData = createAction('mobile/setPointPaymentData', resolve => (data?: some) =>
  resolve({ data }),
);

export const setBuyerInfo = createAction('mobile/setBuyerInfo', resolve => (params: MobileCardBuyer) =>
  resolve({ params }),
);
export const setBuyerInfoValidation = createAction(
  'mobile/setBuyerInfoValidation',
  resolve => (params: BuyerInfoValidation) => resolve({ params }),
);

export const setMobileData = createAction('mobile/setMobileData', resolve => (params: some[]) => resolve({ params }));

export const setUsePointPayment = createAction('mobile/setUsePointPayment', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPointUsing = createAction('mobile/setPointUsing', resolve => (val: number) => resolve({ val }));

export const setPromotionCode = createAction('mobile/setPromotionCode', resolve => (data: string) => resolve({ data }));

export const setPromotion = createAction('mobile/setPromotion', resolve => (data?: some) => resolve({ data }));
export const setbuyerMobileTopupInfo = createAction(
  'mobile/setbuyerMobileTopupInfo',
  resolve => (params: MobileTopupBuyer) => resolve({ params }),
);

const actions = {
  setBuyerInfo,
  setMobileData,
  setBuyerInfoValidation,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setUsePointPayment,
  setPointUsing,
  startPaying,
  endPaying,
  setPromotionCode,
  setPromotion,
  setCardInfo,
  setbuyerMobileTopupInfo,
  setErrorMessage,
  setPayMobileMessage,
};

type ActionT = ActionType<typeof actions>;

function reducer(state: MobileState = DEFAULT_MOBILE_STATE, action: ActionT): MobileState {
  switch (action.type) {
    case getType(setbuyerMobileTopupInfo):
      return { ...state, buyerMobileTopupInfo: action.payload.params };
    case getType(setBuyerInfo):
      return { ...state, buyerMobileCardInfo: action.payload.params };
    case getType(setBuyerInfoValidation):
      return { ...state, buyerInfoValidation: action.payload.params };
    case getType(setMobileData):
      return { ...state, mobileData: action.payload.params };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(startPaying):
      return { ...state, paying: true };
    case getType(endPaying):
      return { ...state, paying: false };
    case getType(setPromotionCode):
      return { ...state, promotionCode: action.payload.data };
    case getType(setPromotion):
      return { ...state, promotion: action.payload.data };
    case getType(setCardInfo):
      return { ...state, cardInfo: action.payload.data };
    case getType(setErrorMessage):
      return { ...state, errorMessage: action.payload.data };
    case getType(setPayMobileMessage):
      return { ...state, payMessage: action.payload.data };
    default:
      return state;
  }
}

export default reducer;
