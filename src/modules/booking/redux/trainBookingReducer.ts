import { push, replace } from 'connected-react-router';
import moment from 'moment';
import { ThunkAction } from 'redux-thunk';
import { Action, ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { C_DATE_FORMAT, ROUTES, SLASH_DATE_FORMAT, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { REASON } from '../../common/components/SendHomeReasonDialog';
import { PAYMENT_HOLDING_CODE, SEND_HOME_REASONS } from '../../common/constants';
import {
  AdultTravellerInfo,
  ChildTravellerInfo,
  ContactInfo,
  CustomerInfo,
  ExportBillInfo,
  SeniorTravellerInfo,
  StudentTravellerInfo,
  TravellersInfo,
} from '../../common/models/train';
import { fetchThunk } from '../../common/redux/thunks';
import {
  ADULT_CUSTOMER_GROUP_ID,
  CHILDREN_CUSTOMER_GROUP_ID,
  SENIOR_CUSTOMER_GROUP_ID,
  STUDENT_CUSTOMER_GROUP_ID,
  DEFAULT_TRAIN_STATE,
  HOLD_PARAM_NAME,
} from '../constants';
import { computePaymentFees, getPointPaymentData } from '../utils';

export interface SeatingInfo {
  adults: some[];
  students: some[];
  seniors: some[];
  children: some[];
}

export interface OneDirection {
  train?: some;
  carriage?: some;
}

export interface TrainTicketParams {
  inbound: OneDirection;
  outbound: OneDirection;
  inboundSeating: SeatingInfo;
  outboundSeating: SeatingInfo;
}
export interface TrainGuestInfo {
  readonly contactInfo?: ContactInfo;
  readonly travellersInfo?: TravellersInfo;
  readonly customerInfo?: CustomerInfo;
  readonly exportBillInfo?: ExportBillInfo;
}
export interface TrainBookingState {
  readonly holdSeatData?: some;
  readonly bookingData?: some;
  readonly guestInfo: TrainGuestInfo;
  readonly timeCountHolding?: number;
  readonly timeCountBooking?: number;
  readonly loading: boolean;
  readonly paying: boolean;
  readonly paymentMethods?: ReadonlyArray<some>;
  readonly selectedPaymentMethod?: Readonly<some>;
  readonly pointUsing: number;
  readonly pointPaymentData?: Readonly<some>;
  readonly usePointPayment: boolean;
  readonly errorMessage?: string;
  readonly payMessage?: string;
  readonly ticketParams: TrainTicketParams;
}

type ActionT = ActionType<typeof actions>;

export function goToBookingInfoPage(
  trainBookingState: TrainTicketParams,
): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    dispatch(setBookingData(trainBookingState));
    dispatch(setLoading(true));
    dispatch(setHoldSeatData());
    const state = getState();
    const search = state.result.train.trainData;
    const { ticketParams } = state.booking.train;
    if (!search || !ticketParams) {
      return undefined;
    }
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.holdSeat}`,
        'post',
        true,
        JSON.stringify({
          doQueryId: search.doQueryId,
          doQueryDataIdDeparture: ticketParams.outbound.train ? ticketParams.outbound.train.doQueryDataId : null,
          doQueryDataIdReturn: ticketParams.inbound.train ? ticketParams.inbound.train.doQueryDataId : null,
          listSeatsDeparture: [
            ...ticketParams.outboundSeating.adults.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: ADULT_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.outboundSeating.students.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: STUDENT_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.outboundSeating.children.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: CHILDREN_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.outboundSeating.seniors.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: SENIOR_CUSTOMER_GROUP_ID,
              };
            }),
          ],
          listSeatsReturn: [
            ...ticketParams.inboundSeating.adults.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: ADULT_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.inboundSeating.students.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: STUDENT_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.inboundSeating.children.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: CHILDREN_CUSTOMER_GROUP_ID,
              };
            }),
            ...ticketParams.inboundSeating.seniors.map(v => {
              return {
                carriageTempId: v.carriageTempId,
                seatTempID: v.seatTempID,
                customerGroupId: SENIOR_CUSTOMER_GROUP_ID,
              };
            }),
          ],
        }),
      ),
    );
    if (json.code === 200 || json.code === '200') {
      dispatch(setHoldSeatData(json.data));
      dispatch(setGuestInfo({}));
      dispatch(
        setTimeCountHolding(
          moment()
            .add(8, 'minutes')
            .valueOf(),
        ),
      );
      dispatch(push(ROUTES.train.trainBookingInfo, { backableToResult: true }));
    } else {
      dispatch(setErrorMessage(json.message));
    }
    dispatch(setLoading(false));
    return json;
  };
}

export function bookTicketTrain(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const booking = state.booking.train;
    const { guestInfo } = booking;
    if (!booking.holdSeatData || !guestInfo.contactInfo || !guestInfo.travellersInfo || !guestInfo.customerInfo) {
      return;
    }
    dispatch(setLoading(true));
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.bookTicketTrain}`,
        'post',
        true,
        JSON.stringify({
          doQueryId: booking.holdSeatData.doQueryId,
          doQueryDataIdDeparture: booking.holdSeatData.doQueryDataIdDeparture,
          doQueryDataIdReturn: booking.holdSeatData.doQueryDataIdReturn,
          bookerCustomer: {
            bookerContactId: guestInfo.customerInfo.bookerContactId,
            idCardNumber: guestInfo.customerInfo.idCardNumber,
            fullName: guestInfo.customerInfo.name,
            email: guestInfo.customerInfo.email,
            mobile: guestInfo.customerInfo.phone,
          },
          listPassenger: [
            ...guestInfo.travellersInfo.adults.map((v: AdultTravellerInfo) => {
              return {
                iDcardNumber: v.option === 'identity' ? v.identity : undefined,
                fullName: `${v.familyName} ${v.givenName}`,
                birthDay:
                  v.option === 'birthday' ? moment(v.birthday, SLASH_DATE_FORMAT).format(C_DATE_FORMAT) : undefined,
                customerGroupId: ADULT_CUSTOMER_GROUP_ID,
              };
            }),
            ...guestInfo.travellersInfo.students.map((v: StudentTravellerInfo) => {
              return {
                iDcardNumber: v.identity,
                fullName: `${v.familyName} ${v.givenName}`,
                customerGroupId: STUDENT_CUSTOMER_GROUP_ID,
              };
            }),
            ...guestInfo.travellersInfo.childrens.map((v: ChildTravellerInfo) => {
              return {
                fullName: `${v.familyName} ${v.givenName}`,
                birthDay: moment(v.birthday, SLASH_DATE_FORMAT).format(C_DATE_FORMAT),
                customerGroupId: CHILDREN_CUSTOMER_GROUP_ID,
              };
            }),
            ...guestInfo.travellersInfo.seniors.map((v: SeniorTravellerInfo) => {
              return {
                fullName: `${v.familyName} ${v.givenName}`,
                birthDay: moment(v.birthday, SLASH_DATE_FORMAT).format(C_DATE_FORMAT),
                customerGroupId: SENIOR_CUSTOMER_GROUP_ID,
              };
            }),
          ],
          contactPerson: {
            fullName: `${guestInfo.contactInfo.familyName} ${guestInfo.contactInfo.givenName}`,
            idCardNumber: guestInfo.contactInfo.identity,
            email: guestInfo.contactInfo.email,
            mobile: guestInfo.contactInfo.phone,
          },
          langId: state.intl.locale,
        }),
      ),
    );
    if (json.code === 200) {
      dispatch(
        setTimeCountBooking(
          moment()
            .add(30, 'minutes')
            .valueOf(),
        ),
      );
      dispatch(setBookingTicketData(json.data));
      dispatch(fetchPaymentMethods());
      dispatch(
        push({
          pathname: ROUTES.train.trainPay,
          state: { ...state.router.location.state, backableToReview: true },
        }),
      );
    } else {
      dispatch(setErrorMessage(json.message));
    }
    dispatch(setLoading(false));
  };
}
export function fetchPaymentMethods(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const trainBooking = state.booking.train;

    if (!trainBooking.bookingData) {
      return;
    }
    dispatch(setLoading(true));
    const json = await dispatch(
      fetchThunk(`${API_PATHS.paymentMethodTrain}?bookId=${trainBooking.bookingData.bookData.bookId}`, 'get', true),
    );

    if (json.code === 200 || json.code === '200') {
      dispatch(setPaymentMethods(json.data));
      dispatch(setSelectedPaymentMethod(json.data[0]));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(replace(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`));
    }
    dispatch(setLoading(false));
  };
}

export function fetchPointPayment(): ThunkAction<Promise<some | undefined>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const booking = getState().booking.train;

    if (!booking.bookingData) {
      return undefined;
    }
    const originAmount = booking.bookingData.bookData.amount;
    const finalPrice = booking.bookingData.bookData.amountPay;
    const { paymentMethods } = getState().booking.train;
    dispatch(setPointPaymentData());
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPointPayment}`,
        'post',
        true,
        JSON.stringify({
          amounts: paymentMethods
            ? paymentMethods.map(v => {
                return {
                  originAmount: originAmount + computePaymentFees(v, originAmount),
                  amount: finalPrice + computePaymentFees(v, originAmount),
                  code: v.code,
                };
              })
            : [],
          module: 'train',
          usePromoCode: false,
        }),
      ),
    );

    if (json.code === 200) {
      const pointPaymentData = getPointPaymentData(json.data);
      dispatch(setPointUsing(pointPaymentData.min));
      dispatch(setPointPaymentData(pointPaymentData));
    }
    return json;
  };
}
export function trainPay(smartOTPPass?: string): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const trainBooking = state.booking.train;
    const { selectedPaymentMethod } = trainBooking;
    if (!trainBooking.bookingData || !selectedPaymentMethod) {
      return;
    }
    dispatch(setPaying(true));
    const json = await dispatch(
      fetchThunk(
        API_PATHS.payTrain,
        'post',
        true,
        JSON.stringify({
          creditPassword: smartOTPPass,
          bookingId: trainBooking.bookingData.bookData.bookId,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
          callbackDomain: `${window.location.origin}`,
        }),
      ),
    );
    dispatch(setPaying(false));
    if (json.code === 200 || json.code === '200') {
      if (json.data.redirect) {
        const { paymentLink } = json.data;
        if (selectedPaymentMethod.code === PAYMENT_HOLDING_CODE) {
          window.location.replace(`${paymentLink}?${HOLD_PARAM_NAME}=true`);
        } else {
          window.location.replace(paymentLink);
        }
      } else {
        dispatch(replace(`${ROUTES.paySuccess.gen('train', json.data.bookingId)}`));
      }
    } else {
      dispatch(setErrorMessage(json.message));
    }
  };
}

export const setTimeCountHolding = createAction('booking/train/setTimeCountHolding', resolve => (val?: number) =>
  resolve({ val }),
);
export const setTimeCountBooking = createAction('booking/train/setTimeCountBooking', resolve => (val?: number) =>
  resolve({ val }),
);
export const setHoldSeatData = createAction('booking/train/setHoldSeatData', resolve => (data?: some) =>
  resolve({ data }),
);
export const setTravellersInfo = createAction('booking/train/setTravellersInfo', resolve => (info: TravellersInfo) =>
  resolve({ info }),
);
export const setCustomerInfo = createAction('booking/train/setCustomerInfo', resolve => (info: CustomerInfo) =>
  resolve({ info }),
);
export const setContactInfo = createAction('booking/train/setContactInfo', resolve => (info: ContactInfo) =>
  resolve({ info }),
);
export const setExportBillInfo = createAction(
  'booking/train/setExportBillInfo',
  resolve => (info: ExportBillInfo | undefined) => resolve({ info }),
);
export const setGuestInfo = createAction('booking/train/setGuestInfo', resolve => (info: TrainGuestInfo) =>
  resolve({ info }),
);
export const setLoading = createAction('booking/train/setLoading', resolve => (val: boolean) => resolve({ val }));
export const setPaying = createAction('booking/train/setPaying', resolve => (val: boolean) => resolve({ val }));
export const setPaymentMethods = createAction('booking/train/setPaymentMethods', resolve => (data?: some[]) =>
  resolve({ data }),
);

export const setSelectedPaymentMethod = createAction(
  'booking/train/setSelectedPaymentMethod',
  resolve => (data?: some) => resolve({ data }),
);

export const setPointPaymentData = createAction('booking/train/setPointPaymentData', resolve => (data?: some) =>
  resolve({ data }),
);

export const setUsePointPayment = createAction('booking/train/setUsePointPayment', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setPointUsing = createAction('booking/train/setPointUsing', resolve => (val: number) => resolve({ val }));
export const setErrorMessage = createAction('booking/train/setErrorMessage', resolve => (val?: string) =>
  resolve({ val }),
);
export const setTrainPayMessage = createAction('booking/train/setTrainPayMessage', resolve => (val?: string) =>
  resolve({ val }),
);
export const setBookingData = createAction('booking/train/setBookingData', resolve => (data: TrainTicketParams) =>
  resolve({ data }),
);
export const setBookingTicketData = createAction('booking/train/setBookingTicketData', resolve => (data?: some) =>
  resolve({ data }),
);
export const setTrainBookingState = createAction(
  'booking/train/setTrainBookingState',
  resolve => (data: TrainBookingState) => resolve({ data }),
);

const actions = {
  setBookingData,
  setTimeCountHolding,
  setTimeCountBooking,
  setTravellersInfo,
  setCustomerInfo,
  setContactInfo,
  setExportBillInfo,
  setLoading,
  setPaying,
  setPaymentMethods,
  setSelectedPaymentMethod,
  setPointPaymentData,
  setUsePointPayment,
  setPointUsing,
  setErrorMessage,
  setHoldSeatData,
  setGuestInfo,
  setBookingTicketData,
  setTrainBookingState,
  setTrainPayMessage,
};

export default function reducer(state: TrainBookingState = DEFAULT_TRAIN_STATE, action: ActionT): TrainBookingState {
  switch (action.type) {
    case getType(setHoldSeatData):
      return { ...state, holdSeatData: action.payload.data };
    case getType(setTimeCountHolding):
      return { ...state, timeCountHolding: action.payload.val };
    case getType(setTimeCountBooking):
      return { ...state, timeCountBooking: action.payload.val };
    case getType(setTravellersInfo):
      return { ...state, guestInfo: { ...state.guestInfo, travellersInfo: action.payload.info } };
    case getType(setCustomerInfo):
      return { ...state, guestInfo: { ...state.guestInfo, customerInfo: action.payload.info } };
    case getType(setContactInfo):
      return { ...state, guestInfo: { ...state.guestInfo, contactInfo: action.payload.info } };
    case getType(setExportBillInfo):
      return { ...state, guestInfo: { ...state.guestInfo, exportBillInfo: action.payload.info } };
    case getType(setGuestInfo):
      return { ...state, guestInfo: action.payload.info };
    case getType(setPaymentMethods):
      return { ...state, paymentMethods: action.payload.data };
    case getType(setSelectedPaymentMethod):
      return { ...state, selectedPaymentMethod: action.payload.data };
    case getType(setUsePointPayment):
      return { ...state, usePointPayment: action.payload.val };
    case getType(setPointUsing):
      return { ...state, pointUsing: action.payload.val };
    case getType(setPointPaymentData):
      return { ...state, pointPaymentData: action.payload.data };
    case getType(setErrorMessage):
      return { ...state, errorMessage: action.payload.val };
    case getType(setTrainPayMessage):
      return { ...state, payMessage: action.payload.val };
    case getType(setLoading):
      return { ...state, loading: action.payload.val };
    case getType(setPaying):
      return { ...state, paying: action.payload.val };
    case getType(setBookingData):
      return { ...state, ticketParams: action.payload.data };
    case getType(setBookingTicketData):
      return { ...state, bookingData: action.payload.data };
    case getType(setTrainBookingState):
      return action.payload.data;
    default:
      return state;
  }
}
