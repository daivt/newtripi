import moment from 'moment';
import voca from 'voca';
import { DATE_FORMAT, DATE_TIME_FORMAT, SLASH_DATE_FORMAT, SLASH_DATE_TIME_FORMAT, some } from '../../constants';
import atm from '../../svg/atm_payments.svg';
import hold from '../../svg/hold_payment.svg';
import tc from '../../svg/tripi_credits.svg';
import visa from '../../svg/visa_logo.svg';
import vnpay from '../../svg/vnpay_qr.svg';
import { calculateTax } from '../../utils';
import {
  FLIGHT_BOOKING_PARAM_NAME,
  HOTEL_BOOK_PARAMS_NAMES,
  PAYMENT_ATM_CODE,
  PAYMENT_HOLDING_CODE,
  PAYMENT_TRIPI_CREDIT_CODE,
  PAYMENT_VISA_CODE,
  PAYMENT_VNPAY_CODE,
} from '../common/constants';
import {
  BuyerInfoValidation,
  ContactInfo,
  ContactInfoValidation,
  Gender,
  MobileCardBuyer,
  MobileTopupBuyer,
  PassportInfoValidation,
  PointPayment,
  RoomBookingInfo,
  RoomBookingInfoValidation,
  TourCustomerCountInfo,
  TourCustomerInfo,
  TourCustomerInfoValidation,
  TravellerInfo,
  TravellerInfoValidation,
  TravellersInfo,
  TravellersInfoValidation,
} from '../common/models';
import { getRoomData, validEmailRegex, validNameRegex, validVietnamTelephoneRegex } from '../common/utils';
import { parseHotelDetailParams } from '../result/utils';
import { FlightBookingParams, FlightBookingState, OneDirection } from './redux/flightBookingReducer';
import { HotelBookingState } from './redux/hotelBookingReducer';
import { MobileState } from './redux/mobileReducer';
import { TourBookingState } from './redux/tourBookingReducer';

export function computePayableNumbers(booking: FlightBookingState, noMarkup = false) {
  const adult = { number: 0, unitPrice: 0 };
  const children = { number: 0, unitPrice: 0 };
  const baby = { number: 0, unitPrice: 0 };
  if (booking.outbound.ticket) {
    adult.number = booking.outbound.searchRequest.numAdults;
    children.number = booking.outbound.searchRequest.numChildren;
    baby.number = booking.outbound.searchRequest.numInfants;
    const detail = booking.outbound.ticket.outbound.ticketdetail;
    adult.unitPrice += !noMarkup ? detail.priceAdultUnit : detail.priceAdultUnitWithoutBookerMarkup;
    children.unitPrice += !noMarkup ? detail.priceChildUnit : detail.priceChildUnitWithoutBookerMarkup;
    baby.unitPrice += !noMarkup ? detail.priceInfantUnit : detail.priceInfantUnitWithoutBookerMarkup;
  }
  if (booking.inbound.ticket) {
    const detail = booking.inbound.ticket.outbound.ticketdetail;
    adult.unitPrice += !noMarkup ? detail.priceAdultUnit : detail.priceAdultUnitWithoutBookerMarkup;
    children.unitPrice += !noMarkup ? detail.priceChildUnit : detail.priceChildUnitWithoutBookerMarkup;
    baby.unitPrice += !noMarkup ? detail.priceInfantUnit : detail.priceInfantUnitWithoutBookerMarkup;
  }

  let ticketTotal = 0;
  if (booking.inbound.ticket) {
    ticketTotal += noMarkup
      ? booking.inbound.ticket.outbound.ticketdetail.grandTotalWithoutBookerMarkup
      : booking.inbound.ticket.outbound.ticketdetail.grandTotal;
  }
  if (booking.outbound.ticket) {
    ticketTotal += noMarkup
      ? booking.outbound.ticket.outbound.ticketdetail.grandTotalWithoutBookerMarkup
      : booking.outbound.ticket.outbound.ticketdetail.grandTotal;
  }

  const pointToAmount = booking.usePointPayment
    ? booking.pointPaymentData &&
      booking.selectedPaymentMethod &&
      booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? booking.pointUsing * booking.pointPaymentData.pointFactor
      : 0
    : 0;

  const extraBaggagesCostsObj = computeExtraBaggagesCost(booking);
  const extraBaggagesCosts = extraBaggagesCostsObj.inbound + extraBaggagesCostsObj.outbound;

  let insuranceCost = 0;
  if (booking.insurancePackage && booking.buyInsurance) {
    insuranceCost = (adult.number + children.number + baby.number) * booking.insurancePackage.price;
  }

  const prediscountTotal = ticketTotal + extraBaggagesCosts + insuranceCost;

  const discountAmount = booking.promotion && booking.promotion.price ? prediscountTotal - booking.promotion.price : 0;

  const originPrice = ticketTotal + insuranceCost + extraBaggagesCosts;

  const finalPrice =
    booking.selectedPaymentMethod && booking.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE
      ? originPrice
      : originPrice - discountAmount;

  const paymentFee = booking.selectedPaymentMethod ? computePaymentFees(booking.selectedPaymentMethod, finalPrice) : 0;

  return {
    ticketTotal,
    insuranceCost,
    extraBaggagesCosts,
    pointToAmount,
    discountAmount,
    adult,
    children,
    baby,
    paymentFee,
    originPrice,
    finalPrice,
  };
}

export function computePoints(booking: FlightBookingState) {
  let points = 0;
  if (booking.inbound.ticket) {
    points += booking.inbound.ticket.outbound.ticketdetail.bonusPoint || 0;
  }
  if (booking.outbound.ticket) {
    points += booking.outbound.ticket.outbound.ticketdetail.bonusPoint || 0;
  }
  return points;
}

export function computePaymentFees(method: some, amount: number) {
  let retval = 0;
  retval += method.fixedFee;
  retval += (method.percentFee / 100) * amount;
  retval = Math.round(retval);
  return retval;
}

export function computeExtraBaggagesCost(booking: FlightBookingState) {
  const retval = { inbound: 0, outbound: 0 };

  if (
    booking.outbound.extraBaggages &&
    booking.outbound.ticket.outbound.baggages &&
    booking.outbound.ticket.outbound.baggages.length
  ) {
    const eb = booking.outbound.extraBaggages;
    for (let i = 0; i < eb.length; i += 1) {
      retval.outbound += booking.outbound.ticket.outbound.baggages[eb[i]].price;
    }
  }

  if (
    booking.inbound.extraBaggages &&
    booking.inbound.ticket.outbound.baggages &&
    booking.inbound.ticket.outbound.baggages.length
  ) {
    const eb = booking.inbound.extraBaggages;
    for (let i = 0; i < eb.length; i += 1) {
      retval.inbound += booking.inbound.ticket.outbound.baggages[eb[i]].price;
    }
  }
  return retval;
}

export function parseFlightBookingParams(params: URLSearchParams): FlightBookingParams {
  const str = params.get(FLIGHT_BOOKING_PARAM_NAME);
  if (!str) {
    throw new Error('No booking param');
  }
  const state: FlightBookingParams = JSON.parse(str);

  if (!state.outbound) {
    throw new Error('tid not specified');
  }

  if (!state.requestId) {
    throw new Error('No request id');
  }

  return state;
}

export function stringifyParams(state: FlightBookingParams) {
  return `${FLIGHT_BOOKING_PARAM_NAME}=${encodeURIComponent(JSON.stringify(state))}`;
}

export function validateTravellersInfo(
  travellersInfo: TravellersInfo,
  needPassport: boolean = false,
  arrivalTime: number = 0,
): TravellersInfoValidation {
  return {
    adults: travellersInfo.adults.map(one => ({
      ...validateTravellerInfo(one, needPassport, arrivalTime),
    })),
    children: travellersInfo.children.map(one => ({
      ...validateChildInfo(one, needPassport, arrivalTime),
    })),
    babies: travellersInfo.babies.map(one => ({
      ...validateBabyInfo(one, needPassport, arrivalTime),
    })),
  };
}

export function validateTravellerInfo(
  info: TravellerInfo,
  needPassport: boolean = false,
  arrivalTime: number = 0,
): TravellerInfoValidation {
  const birthday = moment(info.birthday, 'L', true);
  return {
    birthday: needPassport ? birthday.isValid() && birthday.isBefore(moment(), 'days') : true,
    familyName: info.familyName ? !!info.familyName.trim() : false,
    givenName: info.givenName ? !!info.givenName.trim() : false,

    passportInfo: {
      passport: needPassport ? (info.passportInfo.passport ? info.passportInfo.passport.length > 4 : false) : true,
      passportExpiredDate: needPassport
        ? info.passportInfo.passportExpiredDate
          ? moment(info.passportInfo.passportExpiredDate, 'L', true).isValid() && arrivalTime
            ? moment(info.passportInfo.passportExpiredDate, 'L', true).isSameOrAfter(
                moment(arrivalTime).add(3, 'days'), // officially 2 but we use 3 to have a safety buffer against timezone difference
                'days',
              )
            : false
          : false
        : true,
      passportCountry: needPassport ? !!info.passportInfo.passportCountry.id : true,
      nationalityCountry: needPassport ? !!info.passportInfo.nationalityCountry.id : true,
    },
  };
}

export function genFlightPayParams(booking: FlightBookingState) {
  const { booker, tid, contactInfo, travellersInfo, selectedPaymentMethod } = booking;

  if (!tid || !contactInfo || !travellersInfo || !selectedPaymentMethod) {
    return {};
  }

  return {
    bookerContactId: booker?.id,
    contact: {
      addr1: contactInfo.address,
      email: contactInfo.email,
      firstName: contactInfo.givenName,
      lastName: contactInfo.familyName,
      phone1: contactInfo.telephone,
      gender: contactInfo.gender.toUpperCase(),
      title: contactInfo.gender === 'f' ? 'Mrs' : 'Mr',
    },
    guests: convertTravellersFull(
      travellersInfo,
      booking.outbound,
      booking.inbound,
      booking.buyInsurance ? booking.insurancePackage : undefined,
    ),
    insuranceContact: {
      addr1: contactInfo.address,
      email: contactInfo.email,
      firstName: contactInfo.givenName,
      lastName: contactInfo.familyName,
      phone1: contactInfo.telephone,
      gender: contactInfo.gender.toUpperCase(),
      title: contactInfo.gender === 'f' ? 'Mrs' : 'Mr',
    },
    paymentMethodId: selectedPaymentMethod.id,
    paymentMethodBankId: selectedPaymentMethod.bankId,
    point: booking.usePointPayment ? booking.pointUsing : 0,
    promoCode: booking.promotionCode,
    tickets: {
      inbound: tid.inbound ? { agencyId: tid.inbound.aid, requestId: tid.requestId, ticketId: tid.inbound.id } : null,
      outbound: {
        agencyId: tid.outbound.aid,
        requestId: tid.requestId,
        ticketId: tid.outbound.id,
      },
    },
  };
}

export function validateChildInfo(
  info: TravellerInfo,
  needPassport: boolean = false,
  arrivalTime: number = 0,
): TravellerInfoValidation {
  const now = moment();
  const birthday = moment(info.birthday, 'L', true);
  return {
    ...validateTravellerInfo(info, needPassport, arrivalTime),
    birthday: birthday.isValid() && now.diff(birthday, 'months') >= 24 && now.diff(birthday, 'years') < 12,
  };
}

export function validateBabyInfo(
  info: TravellerInfo,
  needPassport: boolean = false,
  arrivalTime: number = 0,
): TravellerInfoValidation {
  const now = moment();
  const birthday = moment(info.birthday, 'L', true);
  return {
    ...validateTravellerInfo(info, needPassport, arrivalTime),
    birthday: birthday.isValid() && now.diff(birthday, 'months') < 24,
  };
}

export function validateBuyerInfo(info: MobileCardBuyer): BuyerInfoValidation {
  return {
    name: info.name ? !!info.name.trim() : false,
    email: info.email ? validEmailRegex.test(info.email) : false,
    phone: info.phone ? validVietnamTelephoneRegex.test(info.phone) : false,
  };
}
export function isValidBuyerInfoCode(buyerMobileCardInfo: MobileCardBuyer): boolean {
  if (
    buyerMobileCardInfo.amount && buyerMobileCardInfo.cardValue && buyerMobileCardInfo.email && buyerMobileCardInfo.name
      ? !!buyerMobileCardInfo.name.trim()
      : false && buyerMobileCardInfo.operatorCode && buyerMobileCardInfo.phone
  ) {
    return true;
  }
  return false;
}
export function isValidBuyerInfoDirect(buyerMobileCardInfo: MobileTopupBuyer): boolean {
  if (buyerMobileCardInfo.cardValue && buyerMobileCardInfo.operatorCode && buyerMobileCardInfo.phone) {
    return true;
  }
  return false;
}
export function validateContactInfo(info: ContactInfo): ContactInfoValidation {
  return {
    familyName: info.familyName ? !!info.familyName.trim() : false,
    givenName: info.givenName ? !!info.givenName.trim() : false,
    birthday: true,
    email: validEmailRegex.test(info.email),
    // id: validIdRegex.test(info.id) || !info.id.trim(),
    // idIssuedDate: moment(info.idIssuedDate, DATE_FORMAT).isValid() || !info.idIssuedDate.trim(),
    telephone: validVietnamTelephoneRegex.test(info.telephone),
  };
}

function validPassport(passportInfo: PassportInfoValidation) {
  return (
    passportInfo.nationalityCountry &&
    passportInfo.passport &&
    passportInfo.passportCountry &&
    passportInfo.passportExpiredDate
  );
}

export function valid(validation: TravellersInfoValidation) {
  let check = true;
  for (let key = 0; key < validation.adults.length; key += 1) {
    const adult = validation.adults[key];
    if (!(adult.birthday && adult.familyName && adult.givenName && validPassport(adult.passportInfo))) {
      check = false;
    }
  }
  for (let key = 0; key < validation.children.length; key += 1) {
    const child = validation.children[key];
    if (!(child.birthday && child.familyName && child.givenName && validPassport(child.passportInfo))) {
      check = false;
    }
  }
  for (let key = 0; key < validation.babies.length; key += 1) {
    const baby = validation.babies[key];
    if (!(baby.birthday && baby.familyName && baby.givenName && validPassport(baby.passportInfo))) {
      check = false;
    }
  }
  return check;
}

export function validContact(validation: ContactInfoValidation) {
  return (
    validation.birthday && validation.email && validation.familyName && validation.givenName && validation.telephone
  );
}

export function validateRoomsBookingInfo(info: RoomBookingInfo[]): RoomBookingInfoValidation[] {
  return info.map(item => ({
    name: item.name ? !!item.name.trim() : false,
  }));
}

export function validBookingInfo(infoValidaion: RoomBookingInfoValidation[]): boolean {
  let result = true;
  infoValidaion.forEach(item => {
    if (!item.name) {
      result = false;
    }
  });
  return result;
}

export function validBuyerInfo(validation: BuyerInfoValidation) {
  return validation.email && validation.name && validation.phone;
}

function validTourCustomerCountInfo(tourCustomerCountInfo: TourCustomerCountInfo) {
  return (
    tourCustomerCountInfo.adultCount > 0 ||
    tourCustomerCountInfo.childrenCount > 0 ||
    tourCustomerCountInfo.babyCount > 0 ||
    tourCustomerCountInfo.infantCount > 0
  );
}

export function validateTourCustomerInfo(info: TourCustomerInfo): TourCustomerInfoValidation {
  return {
    fullName: info.fullName ? !!info.fullName.trim() : false,
    phone: info.phone.length > 4,
  };
}

export function validTourCustomer(validation: TourCustomerInfoValidation) {
  return validation.fullName && validation.phone;
}

export function validTourBookingInfo(
  tourBookingState: TourBookingState,
  tourCustomerInfoValidation: TourCustomerInfoValidation,
) {
  return !tourBookingState.date
    ? 'tour.booking.noChooseDate'
    : '' || (tourBookingState.date && tourBookingState.date.isBefore(moment()))
    ? 'tour.booking.chooseDateValid'
    : '' || !tourBookingState.activityPackage
    ? 'tour.booking.noChoosePackage'
    : '' || !validTourCustomerCountInfo(tourBookingState.tourCustomerCountInfo)
    ? 'tour.booking.noChoosePackageNote'
    : '' || !validTourCustomer(tourCustomerInfoValidation)
    ? 'booking.invalidGuestInfo'
    : '';
}

export function getPaymentMethodIcon(code: string) {
  if (code === PAYMENT_TRIPI_CREDIT_CODE) {
    return tc;
  }
  if (code === PAYMENT_VNPAY_CODE) {
    return vnpay;
  }
  if (code === PAYMENT_ATM_CODE) {
    return atm;
  }
  if (code === PAYMENT_VISA_CODE) {
    return visa;
  }
  if (code === PAYMENT_HOLDING_CODE) {
    return hold;
  }
  return undefined;
}

export function convertTravellersFull(
  info: TravellersInfo,
  outbound: OneDirection,
  inbound: OneDirection,
  insurancePackage?: some,
) {
  let retval: some[] = [];
  const needPassport = outbound.ticket ? outbound.ticket.outbound.ticketdetail.needPassport : false;

  retval = retval.concat(
    info.adults.map((one, i) => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: needPassport ? moment(one.birthday, 'L').format(DATE_FORMAT) : undefined,
      outboundBaggageId:
        outbound.ticket && outbound.ticket.outbound.baggages[outbound.extraBaggages[i]]
          ? outbound.ticket.outbound.baggages[outbound.extraBaggages[i]].id
          : null,
      inboundBaggageId:
        inbound.ticket && inbound.ticket.outbound.baggages[inbound.extraBaggages[i]]
          ? inbound.ticket.outbound.baggages[inbound.extraBaggages[i]].id
          : null,
      insuranceInfo: insurancePackage ? genInsuranceInfo(outbound, inbound, insurancePackage) : undefined,
      passport: one.passportInfo.passport,
      passportExpiredDate: one.passportInfo.passportExpiredDate
        ? moment(one.passportInfo.passportExpiredDate, 'L').format(DATE_FORMAT)
        : '',
      passportCountryId: one.passportInfo.passportCountry.id || null,
      nationalityCountryId: one.passportInfo.nationalityCountry.id || null,
    })),
  );
  retval = retval.concat(
    info.children.map((one, i) => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: moment(one.birthday, 'L').format(DATE_FORMAT),
      outboundBaggageId:
        outbound.ticket && outbound.ticket.outbound.baggages[outbound.extraBaggages[info.adults.length + i]]
          ? outbound.ticket.outbound.baggages[outbound.extraBaggages[info.adults.length + i]].id
          : null,
      inboundBaggageId:
        inbound.ticket && inbound.ticket.outbound.baggages[inbound.extraBaggages[info.adults.length + i]]
          ? inbound.ticket.outbound.baggages[inbound.extraBaggages[info.adults.length + i]].id
          : null,
      insuranceInfo: insurancePackage ? genInsuranceInfo(outbound, inbound, insurancePackage) : undefined,
      passport: one.passportInfo.passport,
      passportExpiredDate: one.passportInfo.passportExpiredDate
        ? moment(one.passportInfo.passportExpiredDate, 'L').format(DATE_FORMAT)
        : '',
      passportCountryId: one.passportInfo.passportCountry.id || null,
      nationalityCountryId: one.passportInfo.nationalityCountry.id || null,
    })),
  );
  retval = retval.concat(
    info.babies.map(one => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: moment(one.birthday, 'L').format(DATE_FORMAT),
      insuranceInfo: insurancePackage ? genInsuranceInfo(outbound, inbound, insurancePackage) : undefined,
      passport: one.passportInfo.passport,
      passportExpiredDate: one.passportInfo.passportExpiredDate
        ? moment(one.passportInfo.passportExpiredDate, 'L').format(DATE_FORMAT)
        : '',
      passportCountryId: one.passportInfo.passportCountry.id || null,
      nationalityCountryId: one.passportInfo.nationalityCountry.id || null,
    })),
  );
  return retval;
}

function genInsuranceInfo(outbound: OneDirection, inbound: OneDirection, insurancePackage?: some) {
  const departureOutboundDate = outbound.ticket
    ? moment(
        `${outbound.ticket.outbound.departureDayStr} ${outbound.ticket.outbound.departureTimeStr}`,
        SLASH_DATE_TIME_FORMAT,
      ).format(DATE_TIME_FORMAT)
    : '';

  let arrivalOutboundDate = outbound.ticket
    ? moment(
        `${outbound.ticket.outbound.arrivalDayStr} ${outbound.ticket.outbound.arrivalTimeStr}`,
        SLASH_DATE_TIME_FORMAT,
      ).format(DATE_TIME_FORMAT)
    : '';

  if (inbound.ticket) {
    arrivalOutboundDate = moment(
      `${inbound.ticket.outbound.arrivalDayStr} ${inbound.ticket.outbound.arrivalTimeStr}`,
      SLASH_DATE_TIME_FORMAT,
    ).format(DATE_TIME_FORMAT);
  }
  return {
    insurancePackageCode: insurancePackage ? insurancePackage.code : '',
    fromDate: arrivalOutboundDate,
    toDate: departureOutboundDate,
  };
}

export function convertTravellers(info: TravellersInfo) {
  let retval: some[] = [];
  retval = retval.concat(
    info.adults.map(one => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: moment(one.birthday, 'L').format(DATE_FORMAT),
    })),
  );
  retval = retval.concat(
    info.children.map(one => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: moment(one.birthday, 'L').format(DATE_FORMAT),
    })),
  );
  retval = retval.concat(
    info.babies.map(one => ({
      firstName: one.givenName,
      lastName: one.familyName,
      gender: one.gender.toUpperCase(),
      dob: moment(one.birthday, 'L').format(DATE_FORMAT),
    })),
  );
  return retval;
}

export function getGuestParams(info: TravellersInfo, booking: FlightBookingState) {
  const departureOutboundDate = booking.outbound.ticket
    ? moment(booking.outbound.ticket.outbound.departureDayStr, SLASH_DATE_FORMAT).format(DATE_FORMAT)
    : '';

  let arrivalOutboundDate = booking.outbound.ticket
    ? moment(booking.outbound.ticket.outbound.arrivalDayStr, SLASH_DATE_FORMAT).format(DATE_FORMAT)
    : '';

  if (booking.inbound.ticket) {
    arrivalOutboundDate = moment(booking.inbound.ticket.outbound.arrivalDayStr, SLASH_DATE_FORMAT).format(DATE_FORMAT);
  }

  const retval = convertTravellers(info);
  const guests = retval.map(item => ({
    ...item,
    dob: moment(item.dob, SLASH_DATE_FORMAT).format(DATE_FORMAT),
    insuranceInfo: {
      insurancePackageCode: booking.insurancePackage ? booking.insurancePackage.code : '',
      fromDate: arrivalOutboundDate,
      toDate: departureOutboundDate,
    },
    passport: '',
  }));

  return guests;
}

export function parseBookRoomParams(params: URLSearchParams) {
  const obj = parseHotelDetailParams(params);
  const shrui = params.get(HOTEL_BOOK_PARAMS_NAMES.shrui);
  if (!shrui) {
    throw new Error('No SHRUI');
  }
  return { shrui, ...obj };
}

export const initPointPayment: PointPayment = {
  availablePoint: 0,
  min: 0,
  max: 0,
  paymentMethodCode: PAYMENT_TRIPI_CREDIT_CODE,
  pointFactor: 0,
  credit: 0,
  maxPointOnModuleTag: '',
  usedPointOnModuleTag: '',
};

export function getPointPaymentData(data: some): PointPayment {
  return data.find((one: some) => one.paymentMethodCode === PAYMENT_TRIPI_CREDIT_CODE) || initPointPayment;
}

export function computeHotelPayableNumbers(booking: HotelBookingState, noMarkUp = false) {
  const pointToAmount = booking.usePointPayment
    ? booking.pointPaymentData &&
      booking.selectedPaymentMethod &&
      booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? booking.pointUsing * booking.pointPaymentData.pointFactor
      : 0
    : 0;

  let roomPrice = 0;

  if (booking.roomsData) {
    const roomDetail = booking.params ? getRoomData(booking.roomsData, booking.params) : undefined;
    roomPrice = roomDetail ? (noMarkUp ? roomDetail.finalPriceWithoutBookerMarkup : roomDetail.finalPrice) : 0;
  }

  const nightCount = booking.params ? booking.params.checkOut.diff(booking.params.checkIn, 'days') : 1;
  const roomCount = booking.params ? booking.params.guestInfo.roomCount : 1;

  const finalPriceIncludeTax = roomPrice * roomCount * nightCount;
  const totalTax = calculateTax(finalPriceIncludeTax);
  const finalPriceNoTax = finalPriceIncludeTax - totalTax;

  const discountAmount =
    booking.promotion && booking.promotion.price ? booking.promotion.originPrice - booking.promotion.price : 0;

  const finalPrice =
    booking.selectedPaymentMethod && booking.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE
      ? finalPriceIncludeTax
      : finalPriceIncludeTax - discountAmount;

  const paymentFee = booking.selectedPaymentMethod ? computePaymentFees(booking.selectedPaymentMethod, finalPrice) : 0;

  return {
    nightCount,
    roomCount,
    pointToAmount,
    discountAmount,
    totalTax,
    paymentFee,
    finalPriceNoTax,
    finalPriceIncludeTax,
    finalPrice,
  };
}

export function computeMobilePaybleNumbers(booking: MobileState) {
  const pointToAmount = booking.usePointPayment
    ? booking?.selectedPaymentMethod?.code === 'CD'
      ? booking.pointUsing * booking?.pointPaymentData?.pointFactor
      : 0
    : 0;
  const discounts = booking.cardInfo ? booking.cardInfo.productValue - booking.cardInfo.totalPrice : 0;
  const promotion = booking.cardInfo ? booking.cardInfo.discount : 0;
  const finalPrice = booking.cardInfo ? booking.cardInfo.finalPrice : 0;
  const fee = booking.selectedPaymentMethod ? computePaymentFees(booking.selectedPaymentMethod, finalPrice) : 0;
  const totalPrice = booking.cardInfo ? booking.cardInfo.totalPrice : 0;
  const { cardInfo } = booking;
  return {
    pointToAmount,
    fee,
    discounts,
    promotion,
    totalPrice,
    cardInfo,
    finalPrice,
  };
}

export function computeTourPaybleNumbers(booking: TourBookingState) {
  const { tourCustomerCountInfo, activityPackage } = booking;
  let originPrice = 0;

  if (activityPackage) {
    originPrice =
      booking.tourCustomerCountInfo.adultCount * activityPackage.adultPrice +
      tourCustomerCountInfo.childrenCount * activityPackage.childrenPrice +
      tourCustomerCountInfo.infantCount * activityPackage.infantPrice +
      tourCustomerCountInfo.babyCount * activityPackage.babyPrice;
  }

  const pointToAmount = booking.usePointPayment
    ? booking.pointPaymentData &&
      booking.selectedPaymentMethod &&
      booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? booking.pointUsing * booking.pointPaymentData.pointFactor
      : 0
    : 0;

  const discountAmount =
    booking.promotion &&
    booking.promotion.price &&
    booking.selectedPaymentMethod &&
    booking.selectedPaymentMethod.code !== PAYMENT_HOLDING_CODE
      ? booking.promotion.originPrice - booking.promotion.price
      : 0;

  const finalPrice = originPrice - discountAmount - pointToAmount;
  const paymentFee = booking.selectedPaymentMethod ? computePaymentFees(booking.selectedPaymentMethod, finalPrice) : 0;
  return {
    originPrice,
    pointToAmount,
    discountAmount,
    paymentFee,
    finalPrice: finalPrice > 0 ? finalPrice : 0,
  };
}

export function transformTravellerInfo(contact: some) {
  const familyName = voca.latinise(contact.lastName);
  const givenName = voca.latinise(contact.firstName);

  return {
    familyName: validNameRegex.test(familyName) ? familyName : '',
    givenName: validNameRegex.test(givenName) ? givenName : '',
    gender: (contact.gender as string).toLowerCase() as Gender,
    birthday: moment(contact.formattedDateOfBirth, DATE_FORMAT).format('L'),
    passportInfo: {
      passport: '',
      passportExpiredDate: '',
      passportCountry: {},
      nationalityCountry: {},
    },
  };
}

export function getOperatorCode(phone: string, mobileData: some[]) {
  let code = '';
  const header = phone.substring(0, 3);
  const header2 = phone.substring(0, 4);
  mobileData.forEach(element => {
    element.prefixes.forEach((element2: string) => {
      if (element2 === header || element2 === header2) {
        code = element.code;
      }
    });
  });
  return code;
}
export function getBonusPoint(booking: TourBookingState) {
  let num = 0;
  if (booking.activityPackage) {
    if (booking.tourCustomerCountInfo.adultCount) {
      num += booking.tourCustomerCountInfo.adultCount * booking.activityPackage.bonusPoint;
    }

    if (booking.tourCustomerCountInfo.childrenCount) {
      num += booking.tourCustomerCountInfo.childrenCount * booking.activityPackage.childrenBonusPoint;
    }

    if (booking.tourCustomerCountInfo.infantCount) {
      num += booking.tourCustomerCountInfo.infantCount * booking.activityPackage.infantBonusPoint;
    }

    if (booking.tourCustomerCountInfo.babyCount) {
      num += booking.tourCustomerCountInfo.babyCount * booking.activityPackage.babyBonusPoint;
    }
  }
  return num;
}
