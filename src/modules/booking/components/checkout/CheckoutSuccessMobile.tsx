import { Container } from '@material-ui/core';
import * as React from 'react';
import { ModuleType, some, MY_TOUR } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import FooterMobile from '../../../common/components/FooterMobile';
import HeaderMobile from '../../../common/components/HeaderMobile';
import BookingSuccessBox from './BookingSuccessBox';
import ManagementSuccessBox from './ManagementSuccessBox';
import MobileTopupSuccessBox from './MobileTopupSuccessBox';
import Header from '../../../common/components/Header';

interface Props {
  moduleType?: ModuleType;
  bookingId: string;
  msg?: string;
  mobileData?: some;
  hold: boolean;
}

const CheckoutSuccessMobile: React.FunctionComponent<Props> = props => {
  const { moduleType, msg, bookingId, mobileData, hold } = props;

  if (MY_TOUR) {
    return (
      <PageWrapper>
        <Header light />
        <Container
          style={{
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
          }}
        >
          {moduleType === 'flight' && <BookingSuccessBox moduleType={moduleType} msg={msg} hold={hold} isMobile />}
        </Container>
        <FooterMobile />
      </PageWrapper>
    );
  }
  return (
    <PageWrapper>
      <HeaderMobile />
      <Container
        style={{
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        {(moduleType === 'baggages' || moduleType === 'itinerary_changing') && (
          <ManagementSuccessBox moduleType={moduleType} bookingId={bookingId} msg={msg} />
        )}

        {(moduleType === 'flight' || moduleType === 'hotel' || moduleType === 'tour' || moduleType === 'train') && (
          <BookingSuccessBox moduleType={moduleType} msg={msg} hold={hold} isMobile />
        )}

        {(moduleType === 'mobile_card' || moduleType === 'mobile_topup') && mobileData && (
          <MobileTopupSuccessBox data={mobileData} msg={msg} isMobile />
        )}
      </Container>
      <FooterMobile />
    </PageWrapper>
  );
};

export default CheckoutSuccessMobile;
