import { Card, Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ModuleType } from '../../../../constants';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import { PageWrapper } from '../../../common/components/elements';
import FooterMobile from '../../../common/components/FooterMobile';
import HeaderMobile from '../../../common/components/HeaderMobile';

interface Props {
  message?: string;
  moduleType?: ModuleType | 'all';
}

const CheckoutFailMobile: React.FC<Props> = props => {
  const { message } = props;
  return (
    <PageWrapper>
      <HeaderMobile />
      <Container style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
        <Card
          elevation={2}
          style={{
            padding: '32px',
            margin: '24px 0',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <IconWarning />
          </div>
          <div style={{ margin: '32px 0 12px' }}>
            <Typography variant="h6">
              <FormattedMessage id="booking.checkout.failed" />
            </Typography>
          </div>
          <div style={{ margin: 'auto' }}>
            <Typography variant="body2">
              {message || <FormattedMessage id="booking.checkout.failDescription" />}
            </Typography>
          </div>
        </Card>
      </Container>
      <FooterMobile />
    </PageWrapper>
  );
};

export default CheckoutFailMobile;
