import { Button, Card, Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ModuleType, ROUTES, TABLET_WIDTH, MY_TOUR } from '../../../../constants';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import Link from '../../../common/components/Link';
import { CURRENT_PARAM_NAME } from '../../../management/constants';

interface Props {
  message?: string;
  moduleType?: ModuleType | 'all';
}

const CheckoutFailTabletDesktop: React.FC<Props> = props => {
  const { message, moduleType } = props;
  return (
    <PageWrapper
      style={{
        minWidth: TABLET_WIDTH,
      }}
    >
      <Header light />
      <Container style={{ flex: 1 }}>
        <div style={{ width: '700px', margin: 'auto' }}>
          <Card
            elevation={2}
            style={{
              padding: '32px',
              margin: '24px 0',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <IconWarning />
            </div>
            <div style={{ margin: '32px 0 12px' }}>
              <Typography variant="h6">
                <FormattedMessage id="booking.checkout.failed" />
              </Typography>
            </div>
            <div style={{ width: '400px', margin: 'auto' }}>
              <Typography variant="body2">
                {message || <FormattedMessage id="booking.checkout.failDescription" />}
              </Typography>
            </div>
            {MY_TOUR ? (
              <a href="https://mytour.vn/" style={{ textDecoration: 'none', marginTop: '80px' }}>
                <Button style={{ minWidth: '260px' }} size="large" variant="contained" color="secondary">
                  <Typography variant="button">
                    <FormattedMessage id="booking.checkout.backMainPage" />
                  </Typography>
                </Button>
              </a>
            ) : (
              <div style={{ marginTop: '100px' }}>
                <Link
                  to={
                    moduleType !== 'mobile_topup' && moduleType !== 'mobile_card'
                      ? `${ROUTES.management}?${CURRENT_PARAM_NAME}=m.orders&tab=${
                          moduleType === 'baggages' || moduleType === 'itinerary_changing' ? 'flight' : moduleType
                        }`
                      : ROUTES.mobile.mobileHistory
                  }
                >
                  <Button
                    style={{ marginRight: '32px', width: '260px' }}
                    size="large"
                    variant="contained"
                    color="secondary"
                  >
                    <FormattedMessage id="booking.checkout.orderManagement" />
                  </Button>
                </Link>
                <Link to="/">
                  <Button style={{ width: '260px' }} size="large" variant="outlined">
                    <FormattedMessage id="booking.checkout.backMainPage" />
                  </Button>
                </Link>
              </div>
            )}
          </Card>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default CheckoutFailTabletDesktop;
