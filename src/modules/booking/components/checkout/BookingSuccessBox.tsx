import { Button, Card, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ModuleType, MY_TOUR, ROUTES } from '../../../../constants';
import { ReactComponent as IconCheckoutSvg } from '../../../../svg/checkoutSuccess.svg';
import Link from '../../../common/components/Link';

interface Props {
  moduleType?: ModuleType;
  msg?: string;
  hold: boolean;
  isMobile?: boolean;
}

const BookingSuccessBox = (props: Props) => {
  const { moduleType, msg, hold, isMobile } = props;

  return (
    <>
      <Typography variant="h5">
        {msg || <FormattedMessage id={hold ? 'booking.checkout.holdSuccess' : 'booking.checkout.success'} />}
      </Typography>
      <Card
        elevation={2}
        style={{
          padding: '32px',
          margin: '24px 0',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <IconCheckoutSvg />
        <div style={{ margin: '8px 0' }}>
          <Typography variant="h6">
            {msg || <FormattedMessage id={hold ? 'booking.checkout.holdSuccess' : 'booking.checkout.success'} />}
          </Typography>
        </div>
        <div style={{ margin: 'auto', textAlign: 'center' }}>
          <Typography variant="body2">
            <FormattedMessage id={hold ? 'booking.checkout.holdNote' : 'booking.checkout.bookingNote'} />
          </Typography>
        </div>
        {MY_TOUR ? (
          <a href="https://mytour.vn/" style={{ textDecoration: 'none', marginTop: '80px' }}>
            <Button style={{ minWidth: '260px' }} size="large" variant="contained" color="secondary">
              <Typography variant="button">
                <FormattedMessage id="booking.checkout.backMainPage" />
              </Typography>
            </Button>
          </a>
        ) : (
          <>
            {!isMobile && (
              <div style={{ marginTop: '80px' }}>
                <Link to={`${ROUTES.management}?current=m.orders&tab=${moduleType}`}>
                  <Button
                    style={{ marginRight: '32px', minWidth: '260px' }}
                    size="large"
                    variant="contained"
                    color="secondary"
                  >
                    <Typography variant="button">
                      <FormattedMessage id="booking.checkout.orderManagement" />
                    </Typography>
                  </Button>
                </Link>
                <Link to="/">
                  <Button style={{ minWidth: '260px' }} size="large" variant="outlined">
                    <Typography variant="button" color="textSecondary">
                      <FormattedMessage id="booking.checkout.backMainPage" />
                    </Typography>
                  </Button>
                </Link>
              </div>
            )}
          </>
        )}
      </Card>
    </>
  );
};

export default BookingSuccessBox;
