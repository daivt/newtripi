import { Button, Card, Divider, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { BLUE, SECONDARY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { ReactComponent as IconPaymentSuccess } from '../../../../svg/payment_success.svg';
import Link from '../../../common/components/Link';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import { CopyTextField } from '../Form';

const CardInfoBox = (props: { data: some; index: number }) => {
  const { data, index } = props;
  return (
    <div style={{ marginBottom: '20px' }}>
      <Typography variant="subtitle2" style={{ margin: '0px 0px 16px 10px' }}>
        <FormattedMessage id="mobile.card" values={{ num: index }} />
      </Typography>
      <CopyTextField
        header={<FormattedMessage id="mobile.syntax" />}
        text={`*100*${data.pinCode}#`}
        inputStyle={{ color: SECONDARY }}
      />
      <CopyTextField
        style={{ marginTop: '12px' }}
        header={<FormattedMessage id="mobile.pinCode" />}
        text={`${data.pinCode}`}
        inputStyle={{ color: SECONDARY }}
      />
      <CopyTextField
        style={{ marginTop: '12px' }}
        header={<FormattedMessage id="mobile.serial" />}
        text={`${data.serial}`}
        inputStyle={{ color: SECONDARY }}
      />
    </div>
  );
};

interface Props extends WrappedComponentProps {
  data: some;
  msg?: string;
  isMobile?: boolean;
}

const MobileTopupSuccessBox: React.FunctionComponent<Props> = props => {
  const { intl, data, msg, isMobile } = props;
  return (
    <>
      <Typography variant="h5">{msg || <FormattedMessage id="mobile.success" />}</Typography>
      <Card
        elevation={2}
        style={{
          padding: '32px',
          margin: '24px 0',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <IconPaymentSuccess style={{ marginBottom: '30px', width: '104px', height: '104px' }} />
        <Typography
          variant="body2"
          style={{
            textAlign: 'center',
          }}
        >
          {data.cardsInfo ? (
            <FormattedMessage
              id="mobile.tripiCardMessage"
              values={{
                num: data.cardsInfo.length,
                name: data.providerName,
                value: intl.formatNumber(data.value),
              }}
            />
          ) : (
            <FormattedMessage
              id="mobile.tripiCardTopupMessage"
              values={{
                name: data.providerName,
                value: intl.formatNumber(data.value),
              }}
            />
          )}
        </Typography>
        <div style={{ width: '100%', margin: '12px 0px 20px 0px' }}>
          <Divider />
        </div>
        <div>
          <Line style={{ marginBottom: '20px' }}>
            <Typography variant="body2">
              <FormattedMessage id="mobile.bookingId" />
              :&nbsp;
            </Typography>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.orderCode}
            </Typography>
          </Line>
          {data.cardsInfo &&
            data.cardsInfo.map((v: some, index: number) => <CardInfoBox data={v} index={index + 1} key={index} />)}
        </div>
        {!isMobile && (
          <Line style={{ marginTop: '50px' }}>
            <Link to="/">
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ width: '260px', marginRight: '30px' }}
                disableElevation
              >
                <FormattedMessage id="mobile.backToHomepage" />
              </Button>
            </Link>
            <Link to={`${ROUTES.mobile.mobileHistory}`}>
              <Button variant="outlined" size="large" style={{ width: '260px' }}>
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="mobile.toHistorypage" />
                </Typography>
              </Button>
            </Link>
          </Line>
        )}
      </Card>
    </>
  );
};

export default connect()(injectIntl(MobileTopupSuccessBox));
