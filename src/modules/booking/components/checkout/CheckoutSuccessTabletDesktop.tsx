import { Container } from '@material-ui/core';
import * as React from 'react';
import { ModuleType, MY_TOUR, some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import BookingSuccessBox from './BookingSuccessBox';
import ManagementSuccessBox from './ManagementSuccessBox';
import MobileTopupSuccessBox from './MobileTopupSuccessBox';

interface Props {
  moduleType?: ModuleType;
  bookingId: string;
  msg?: string;
  mobileData?: some;
  hold: boolean;
}

const CheckoutSuccessTabletDesktop: React.FunctionComponent<Props> = props => {
  const { moduleType, msg, bookingId, mobileData, hold } = props;
  if (MY_TOUR) {
    return (
      <PageWrapper>
        <Header light />
        <Container
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            maxWidth: '768px',
          }}
        >
          {moduleType === 'flight' && <BookingSuccessBox moduleType={moduleType} msg={msg} hold={hold} isMobile />}
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container
        style={{
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          flexDirection: 'column',
          maxWidth: '768px',
        }}
      >
        {(moduleType === 'baggages' || moduleType === 'itinerary_changing') && (
          <ManagementSuccessBox moduleType={moduleType} bookingId={bookingId} msg={msg} />
        )}

        {(moduleType === 'flight' || moduleType === 'hotel' || moduleType === 'tour' || moduleType === 'train') && (
          <BookingSuccessBox moduleType={moduleType} msg={msg} hold={hold} />
        )}

        {(moduleType === 'mobile_card' || moduleType === 'mobile_topup') && mobileData && (
          <MobileTopupSuccessBox data={mobileData} msg={msg} />
        )}
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default CheckoutSuccessTabletDesktop;
