import { Typography } from '@material-ui/core';
import { replace } from 'connected-react-router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { ModuleType, ROUTES, ServiceType } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import iconCheckSuccess from '../../../../svg/ic_check_success.svg';
import LoadingButton from '../../../common/components/LoadingButton/index';
import { fetchThunk } from '../../../common/redux/thunks';
import {
  FlightManagementBookingItemActionType,
  BOOKING_ID_PARAM_NAME,
  ACTION_PARAM_NAME,
} from '../../../management/constants';

interface Props {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  moduleType?: ModuleType;
  message?: React.ReactNode;
  msg?: string;
  bookingId: string;
}

interface State {
  loading?: boolean;
}

class ManagementSuccessBox extends React.PureComponent<Props, State> {
  state: State = {};

  async backableToDetail() {
    const { dispatch, bookingId } = this.props;
    this.setState({ loading: true });
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getFlightTask,
        'post',
        true,
        JSON.stringify({
          bookingId,
        }),
      ),
    );

    this.setState({ loading: false });
    if (json?.code === 200) {
      const action: FlightManagementBookingItemActionType = 'detail';
      const tab: ServiceType = 'flight';

      dispatch(
        replace(
          `${ROUTES.management}?current=m.orders&tab=${tab}&${BOOKING_ID_PARAM_NAME}=${json.data.bookingInfo.rawBookingId}&${ACTION_PARAM_NAME}=${action}`,
        ),
      );
    }
  }

  render() {
    const { moduleType, msg } = this.props;
    const { loading } = this.state;

    if (!moduleType) {
      return <div />;
    }

    return (
      <div
        style={{
          paddingTop: '50px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <img src={iconCheckSuccess} alt="" />
        <Typography variant="body2" style={{ padding: '12px 0 18px' }}>
          {msg || (
            <>
              {moduleType === 'baggages' && <FormattedMessage id="m.order.payMoreBaggageSuccess" />}
              {moduleType === 'itinerary_changing' && <FormattedMessage id="m.order.changeItinerarySuccess" />}
            </>
          )}
        </Typography>

        <LoadingButton
          loading={loading}
          size="large"
          variant="contained"
          color="secondary"
          style={{ width: '200px', margin: '10px 0' }}
          onClick={() => this.backableToDetail()}
        >
          <Typography variant="button">
            <FormattedMessage id="m.order.viewOrderDetailAgain" />
          </Typography>
        </LoadingButton>
      </div>
    );
  }
}

export default connect()(ManagementSuccessBox);
