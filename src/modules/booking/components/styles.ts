import styled from 'styled-components';

export const Box = styled.div`
  background: white;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 4px;
  padding: 8px 12px 16px 12px;
  margin-bottom: 16px;
`;

export const SectionHeader = styled.div`
  margin-bottom: 8px;
  min-height: 40px;
  align-items: center;
  display: flex;
`;

export const Wrapper = styled.div`
  min-height: calc(100vh - 400px);
  flex: 1;
`;

export const AddContact = styled.div`
  display: flex;
  align-items: center;
  padding: 3px 6px 3px 3px;
`;
