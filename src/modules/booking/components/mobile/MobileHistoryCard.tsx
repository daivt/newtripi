import * as React from 'react';
import { Typography, Button } from '@material-ui/core';
import styled from 'styled-components';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import moment from 'moment';
import { GREY, PRIMARY, SECONDARY, RED } from '../../../../colors';
import { some } from '../../../../constants';

const Line = styled.div`
  display: flex;
  min-height: 28px;
  align-items: center;
`;
interface Props {
  data: some;
  onClick(): void;
  operators: some[];
}

const MobileHistoryCard: React.FunctionComponent<Props> = props => {
  const { data, onClick, operators } = props;
  const operator = operators.find(v => v.code === data.provider.code);
  const getStatus = () => {
    let temp;
    if (data.paymentStatus === 'failed') {
      temp = { color: RED, id: 'fail' };
    } else if (data.module === 'card') {
      if (data.bookStatus === 'success') {
        temp = { color: PRIMARY, id: 'success' };
      } else {
        temp = { color: SECONDARY, id: 'processing' };
      }
    } else {
      temp = { color: PRIMARY, id: 'success' };
    }
    return temp;
  };
  return (
    <div
      style={{
        boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        border: `1px solid ${GREY}`,
        background: 'white',
        borderRadius: '4px',
        overflow: 'hidden',
        display: 'flex',
        padding: '0px 20px 11px 0px',
        width: '100%',
        margin: '10px 0px',
      }}
    >
      <img
        src={data.provider.logoUrl}
        alt=""
        style={{ width: '142px', height: '72px', objectFit: 'contain', marginRight: '20px' }}
      />
      <div style={{ marginTop: '16px', flex: 1 }}>
        <Typography variant="subtitle2">
          {data.module === 'card' ? (
            <FormattedMessage id="mobile.titleCode" />
          ) : (
            <FormattedMessage id="mobile.titleDirect" />
          )}
          &nbsp;
          {operator && operator.name}
        </Typography>
        <div style={{ display: 'flex' }}>
          <div style={{ display: 'flex', flex: 1 }}>
            <div style={{ width: '50%' }}>
              {data.createdTime && (
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.createdTime" />
                    :&nbsp;
                    {moment(data.createdTime).format('HH:mm:ss[, ] DD/MM/YYYY')}
                  </Typography>
                </Line>
              )}
              {data.orderCode && (
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.bookingId" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" color="primary">
                    {data.orderCode}
                  </Typography>
                </Line>
              )}
              {data.contactInfo && (
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.buyerName" />
                    :&nbsp;
                    {data.contactInfo.name}
                  </Typography>
                </Line>
              )}
            </div>
            <div style={{ width: '50%' }}>
              {data.value && (
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.priceCard" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <FormattedNumber value={data.value} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Line>
              )}
              {data.quantity && (
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.amount" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2">{data.quantity}</Typography>
                </Line>
              )}
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.finalPrice" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={data.finalPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            </div>
          </div>
          <div style={{ alignSelf: 'flex-end', paddingBottom: '16px' }}>
            <Typography
              variant="body2"
              style={{
                color: getStatus().color,
                textAlign: 'end',
                marginBottom: '10px',
              }}
            >
              <FormattedMessage id={getStatus().id} />
            </Typography>
            <Button variant="contained" color="secondary" style={{ width: '170px' }} onClick={onClick}>
              <FormattedMessage id="booking.seeDetails" />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MobileHistoryCard;
