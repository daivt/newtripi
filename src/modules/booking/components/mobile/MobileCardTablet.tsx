import { Button, Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { ROUTES, some, TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import Link from '../../../common/components/Link';
import { BuyerInfoValidation, MobileCardBuyer } from '../../../common/models';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import MobileCardBreadcrumbs from './MobileCardBreadcrumbs';
import MobileCardMain from './MobileCardMain';
import MobileCardSideBox from './MobileCardSideBox';

interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileCardInfo: MobileCardBuyer;
  mobileData: some[];
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfo(info: MobileCardBuyer): void;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
  continue_(): void;
}
const MobileCardTablet: React.FunctionComponent<Props> = props => {
  const { buyerMobileCardInfo, mobileData, routerState } = props;
  const [showInfoBox, setShowInfoBox] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex', overflow: 'hidden' }}>
          <div style={{ flex: 1 }}>
            <MobileCardBreadcrumbs />
            <Line style={{ justifyContent: 'space-between', marginBottom: '12px' }}>
              <Typography variant="h5" style={{ marginBottom: '16px' }}>
                <FormattedMessage id="mobile.titleCode" />
              </Typography>
              <Link
                to={{
                  pathname: ROUTES.mobile.mobileHistory,
                  state: { ...routerState, backableToMobileCode: true },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ width: '170px', height: '40px' }}
                >
                  <Typography variant="subtitle2">
                    <FormattedMessage id="mobile.history" />
                  </Typography>
                </Button>
              </Link>
            </Line>
            <MobileCardMain {...props} style={{ flex: 1 }} />
          </div>
          <div
            style={{
              position: 'relative',
              direction: 'rtl',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {!showInfoBox && (
              <IconButton
                style={{ margin: '10px', position: 'absolute' }}
                onClick={() => setShowInfoBox(true)}
              >
                <Open style={{ color: DARK_GREY }} />
              </IconButton>
            )}
            <div style={{ width: 1, flex: 1, display: 'flex' }}>
              <Slide in={showInfoBox} direction="left">
                <div
                  style={{
                    backgroundColor: BACKGROUND,
                    padding: '22px 0 22px 10px',
                    marginTop: '-22px',
                    borderLeft: `1px solid ${GREY}`,
                    width: '420px',
                  }}
                >
                  <div style={{ textAlign: 'end' }}>
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShowInfoBox(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  </div>
                  <div style={{ direction: 'ltr' }}>
                    <MobileCardSideBox
                      buyerMobileCardInfo={buyerMobileCardInfo}
                      mobileData={mobileData}
                    />
                  </div>
                </div>
              </Slide>
            </div>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};
function mapStateToProps(state: AppState) {
  return { routerState: state.router.location.state };
}
export default connect(mapStateToProps)(MobileCardTablet);
