import { Container, Typography, Button } from '@material-ui/core';
import * as React from 'react';
import { DESKTOP_WIDTH, some, ROUTES } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { BuyerInfoValidation, MobileCardBuyer } from '../../../common/models';
import MobileCardBreadcrumbs from './MobileCardBreadcrumbs';
import MobileCardMain from './MobileCardMain';
import MobileCardSideBox from './MobileCardSideBox';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import { FormattedMessage } from 'react-intl';
import Link from '../../../common/components/Link';

interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileCardInfo: MobileCardBuyer;
  mobileData: some[];
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfo(info: MobileCardBuyer): void;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
  continue_(): void;
}

class MobileCardDesktop extends React.Component<Props> {
  public render() {
    const { buyerMobileCardInfo, mobileData, routerState } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <div>
          <Header light={true} />
        </div>
        <Container>
          <MobileCardBreadcrumbs />
          <Line style={{ justifyContent: 'space-between', marginBottom: '12px', width: '816px' }}>
            <Typography variant="h5" style={{ marginBottom: '16px' }}>
              <FormattedMessage id="mobile.titleCode" />
            </Typography>
            <Link
              to={{
                pathname: ROUTES.mobile.mobileHistory,
                state: { ...routerState, backableToMobileCode: true },
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                style={{ width: '170px', height: '40px' }}
              >
                <Typography variant="subtitle2">
                  <FormattedMessage id="mobile.history" />
                </Typography>
              </Button>
            </Link>
          </Line>
          <div style={{ display: 'flex' }}>
            <MobileCardMain {...this.props} style={{ flex: 1 }} />
            <MobileCardSideBox buyerMobileCardInfo={buyerMobileCardInfo} mobileData={mobileData} />
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}
function mapStateToProps(state: AppState) {
  return { routerState: state.router.location.state };
}
export default connect(mapStateToProps)(MobileCardDesktop);
