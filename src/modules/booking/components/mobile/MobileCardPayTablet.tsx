import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { Wrapper } from '../styles';
import MobileCardPayBox from './MobileCardPayBox';
import MobileCardSideBox from './MobileCardSideBox';
import MessageDialog from '../../../common/components/MessageDialog';
import { setErrorMessage } from '../../redux/mobileReducer';
import { goBack } from 'connected-react-router';
import MobileCardPayBreadcrumbs from './MobileCardPayBreadcrumbs';
const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.mobile,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {}

const MobileCardPayTablet: React.FunctionComponent<Props> = props => {
  const [showInfoBox, setShowInfoBox] = React.useState(false);
  const { booking, dispatch } = props;
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex', overflow: 'hidden' }}>
          <div style={{ flex: 1 }}>
            <MobileCardPayBreadcrumbs />
            <Typography variant="h5" style={{ marginBottom: '16px' }}>
              <FormattedMessage id="mobile.titleDirect" />
            </Typography>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <MobileCardPayBox />
            )}
            <MessageDialog
              show={!!booking.errorMessage}
              onClose={() => {
                dispatch(setErrorMessage());
                dispatch(goBack());
              }}
              message={
                <div
                  style={{
                    padding: '18px',
                    textAlign: 'center',
                  }}
                >
                  <Typography variant="body1">{booking.errorMessage}</Typography>
                </div>
              }
            />
          </div>
          <div
            style={{
              position: 'relative',
              direction: 'rtl',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {!showInfoBox && (
              <IconButton
                style={{ margin: '10px', position: 'absolute' }}
                onClick={() => setShowInfoBox(true)}
              >
                <Open style={{ color: DARK_GREY }} />
              </IconButton>
            )}
            <div style={{ width: 1, flex: 1, display: 'flex' }}>
              <Slide in={showInfoBox} direction="left">
                <div
                  style={{
                    backgroundColor: BACKGROUND,
                    padding: '22px 0 22px 10px',
                    marginTop: '-22px',
                    borderLeft: `1px solid ${GREY}`,
                    width: '420px',
                  }}
                >
                  <div style={{ textAlign: 'end' }}>
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShowInfoBox(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  </div>
                  <div style={{ direction: 'ltr' }}>
                    {booking.buyerMobileCardInfo && (
                      <MobileCardSideBox
                        buyerMobileCardInfo={booking.buyerMobileCardInfo}
                        mobileData={booking.mobileData ? booking.mobileData : []}
                      />
                    )}
                  </div>
                </div>
              </Slide>
            </div>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(MobileCardPayTablet);
