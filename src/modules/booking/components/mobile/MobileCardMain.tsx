import { Button, Grid, Typography } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { GREEN, SECONDARY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconChecked } from '../../../../svg/ic_checked.svg';
import { BuyerInfoValidation, MobileCardBuyer } from '../../../common/models';
import { Box, SectionHeader } from '../styles';
import BuyerMobileCardInfoForm from './BuyerMobileCardInfoForm';
const CustomButton = (props: ButtonProps & { active: boolean }) => {
  const { active, ...rest } = props;
  return (
    <Button
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active ? GREEN : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        padding: '3px 8px',
        width: '128px',
        height: '32px',
        textTransform: 'none',
        color: active ? 'white' : '#757575',
        borderColor: active ? GREEN : undefined,
      }}
      {...rest}
    >
      {props.children}
    </Button>
  );
};
interface Props {
  dispatch: Dispatch;
  style?: React.CSSProperties;
  buyerMobileCardInfo: MobileCardBuyer;
  mobileData: some[];
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfo(info: MobileCardBuyer): void;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
  continue_(): void;
}

interface State {
  ticktock: boolean;
}

class MobileCardMain extends React.PureComponent<Props, State> {
  state: State = {
    ticktock: false,
  };

  render() {
    const { ticktock } = this.state;
    const {
      style,
      mobileData,
      buyerMobileCardInfo,
      buyerInfoValidation,
      updateBuyerInfo,
      updateBuyerInfoValidation,
      continue_,
    } = this.props;
    const ListCard = mobileData.find((v: some) => v.code === buyerMobileCardInfo.operatorCode);
    return (
      <div style={style}>
        <Box>
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="mobile.networkOperator" />
            </Typography>
          </SectionHeader>
          <Grid style={{ padding: '24px 0px', width: '610px' }} container spacing={3}>
            {mobileData.map((v: some) => (
              <Grid
                key={v.id}
                item
                style={{
                  padding: '8px 12px',
                  width: '20%',
                }}
              >
                {!v.skeleton ? (
                  <Button
                    variant="outlined"
                    onClick={() => {
                      updateBuyerInfo({
                        ...buyerMobileCardInfo,
                        operatorCode: v.code,
                      });
                    }}
                    style={{
                      boxShadow:
                        v.code === buyerMobileCardInfo.operatorCode
                          ? `0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px ${SECONDARY}, 0px 2px 4px rgba(0, 0, 0, 0.14)`
                          : '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
                      borderRadius: '2px',
                      height: '59px',
                      width: '106px',
                      padding: '0px',
                      border:
                        v.code === buyerMobileCardInfo.operatorCode
                          ? `1px solid ${SECONDARY}`
                          : undefined,
                    }}
                  >
                    <img
                      style={{
                        width: '100%',
                        height: '100%',
                        objectFit: 'contain',
                        padding: '0px',
                      }}
                      src={v.logoUrl}
                      alt=""
                    />
                    {v.code === buyerMobileCardInfo.operatorCode && (
                      <IconChecked style={{ position: 'absolute', top: '0px', right: '0px' }} />
                    )}
                  </Button>
                ) : (
                  <Skeleton width={106} height={59} variant="rect"></Skeleton>
                )}
              </Grid>
            ))}
          </Grid>
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="mobile.valueCard" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', width: '488px' }}>
            {ListCard && ListCard.cardProducts
              ? ListCard.cardProducts.map((v: some, index: number) => (
                  <div style={{ padding: '6px' }} key={index}>
                    <CustomButton
                      active={buyerMobileCardInfo.cardValue === v.value}
                      onClick={() =>
                        updateBuyerInfo({
                          ...buyerMobileCardInfo,
                          cardValue: v.value,
                        })
                      }
                    >
                      <Typography variant="body2">{v.formatedValue}</Typography>
                    </CustomButton>
                  </div>
                ))
              : Array(6)
                  .fill(0)
                  .map((v: some, index: number) => (
                    <div style={{ padding: '6px' }} key={index}>
                      <CustomButton active={false}>
                        <Typography variant="body2">--------</Typography>
                      </CustomButton>
                    </div>
                  ))}
          </div>
          <BuyerMobileCardInfoForm
            ticktock={ticktock}
            validation={buyerInfoValidation}
            info={buyerMobileCardInfo}
            update={(info, validation) => {
              updateBuyerInfo(info);
              if (!!validation) {
                updateBuyerInfoValidation(validation);
              }
            }}
          />
          <div style={{ margin: '32px 0px' }}>
            <Button
              size="large"
              style={{ width: '170px' }}
              variant="contained"
              color="secondary"
              onClick={() => {
                continue_();
                this.setState({ ticktock: !ticktock });
              }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="pay" />
              </Typography>
            </Button>
          </div>
        </Box>
      </div>
    );
  }
}

export default connect()(MobileCardMain);
