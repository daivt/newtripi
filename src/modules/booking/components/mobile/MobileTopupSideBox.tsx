import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { DARK_GREY, RED } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { MobileTopupBuyer } from '../../../common/models';
import { computeMobilePaybleNumbers } from '../../utils';
import { Box } from '../styles';
const Line = styled.div`
  min-height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileTopupInfo: MobileTopupBuyer;
  mobileData: some[];
}

const MobileTopupSideBox: React.FunctionComponent<Props> = props => {
  const { buyerMobileTopupInfo, mobileData, mobile } = props;
  const operator =
    buyerMobileTopupInfo &&
    mobileData.find((v: some) => v.code === buyerMobileTopupInfo.operatorCode);
  const card = operator?.mobileTopupProducts?.find(
    (v: some) => v.value === buyerMobileTopupInfo.cardValue,
  );
  const checkUrl = window.location.pathname === ROUTES.mobile.mobileTopupPay ? true : false;
  const payableNumbers = computeMobilePaybleNumbers(mobile);
  return (
    <Box
      style={{
        width: '368px',
        marginLeft: '32px',
        padding: '14px 16px',
        minHeight: '149px',
        height: 'max-content',
      }}
    >
      <Typography variant="h6">
        <FormattedMessage id="mobile.orderDetail" />
      </Typography>
      {operator ? (
        <Line style={{ marginTop: '16px' }}>
          <Typography variant="body2">
            <FormattedMessage id="mobile.operator" />
          </Typography>
          <Line>
            <img
              style={{
                maxWidth: '80px',
                maxHeight: '40px',
                objectFit: 'contain',
                marginRight: '8px',
              }}
              src={operator && operator.logoUrl}
              alt=""
            />
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              {operator.name}
            </Typography>
          </Line>
        </Line>
      ) : (
        <Line>
          <Typography variant="caption" color="textSecondary">
            <FormattedMessage id="mobile.noInfo" />
          </Typography>
        </Line>
      )}
      {buyerMobileTopupInfo.phone && (
        <Line>
          <Typography variant="body2">
            <FormattedMessage id="telephone" />
          </Typography>
          <Typography variant="body2" color="secondary">
            {buyerMobileTopupInfo.phone}
          </Typography>
        </Line>
      )}
      {card && (
        <Line>
          <Typography variant="body2">
            <FormattedMessage id="mobile.priceCard" />
          </Typography>
          <Typography variant="body2" color="secondary">
            {card.formatedValue}
          </Typography>
        </Line>
      )}
      {card && checkUrl && (
        <>
          {payableNumbers.fee !== 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage
                  id={
                    payableNumbers.fee > 0
                      ? 'booking.paymentFixedFeeSide'
                      : 'payableNumbers.paymentRateDiscount'
                  }
                />
              </Typography>
              <Typography variant="body2" color={payableNumbers.fee > 0 ? 'secondary' : 'primary'}>
                <FormattedNumber value={payableNumbers.fee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.discounts" />
            </Typography>
            <Typography variant="body2" color="primary">
              <FormattedNumber value={-payableNumbers.discounts} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>

          {payableNumbers.promotion > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.promotion" />
              </Typography>
              <Typography variant="body2" color="primary">
                <FormattedNumber value={-payableNumbers.promotion} /> &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="mobile.finalPrice" />
            </Typography>
            <Typography variant="h6" style={{ color: RED }}>
              <FormattedNumber
                value={
                  payableNumbers.finalPrice - payableNumbers.pointToAmount + payableNumbers.fee
                }
              />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </>
      )}
    </Box>
  );
};

const mapStateToProps = (state: AppState) => ({
  mobile: state.booking.mobile,
});
export default connect(mapStateToProps)(MobileTopupSideBox);
