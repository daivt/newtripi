import { Button, Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DESKTOP_WIDTH, ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import Link from '../../../common/components/Link';
import { BuyerInfoValidation, MobileTopupBuyer } from '../../../common/models';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import MobileTopupBreadcrumbs from './MobileTopupBreadcrumbs';
import MobileTopupDirectMain from './MobileTopupMain';
import MobileTopupSideBox from './MobileTopupSideBox';
// import MobileTopupInfoBox from '../mobileCode/MobileTopupInfoBox';

interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileTopupInfo: MobileTopupBuyer;
  mobileData: some[];
  updateBuyerInfo(info: MobileTopupBuyer): void;
  continue_(): void;
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
}

class MobileTopupDesktop extends React.Component<Props> {
  public render() {
    const { buyerMobileTopupInfo, mobileData, routerState } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <div>
          <Header light={true} />
        </div>
        <Container style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <MobileTopupBreadcrumbs />
          <Line style={{ justifyContent: 'space-between', marginBottom: '12px', width: '816px' }}>
            <Typography variant="h5">
              <FormattedMessage id="mobile.titleDirect" />
            </Typography>
            <Link
              to={{
                pathname: ROUTES.mobile.mobileHistory,
                state: { ...routerState, backableToMobileDirect: true },
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                style={{ width: '170px', height: '40px' }}
              >
                <Typography variant="subtitle2">
                  <FormattedMessage id="mobile.history" />
                </Typography>
              </Button>
            </Link>
          </Line>
          <div style={{ display: 'flex', flex: 1 }}>
            <MobileTopupDirectMain {...this.props} style={{ flex: 1 }} />
            <MobileTopupSideBox
              buyerMobileTopupInfo={buyerMobileTopupInfo}
              mobileData={mobileData}
            />
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}
function mapStateToProps(state: AppState) {
  return { routerState: state.router.location.state };
}
export default connect(mapStateToProps)(MobileTopupDesktop);
