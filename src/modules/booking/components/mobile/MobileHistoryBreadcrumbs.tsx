import { Breadcrumbs, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import { go } from 'connected-react-router';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const MobileCardBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToHomePage = state && state.backableToHomePage;
  const backableToMobileCode = state && state.backableToMobileCode;
  const backableToMobileDirect = state && state.backableToMobileDirect;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToHomePage ? 'pointer' : undefined }}
          onClick={() => (backableToHomePage ? props.dispatch(go(-2)) : undefined)}
        >
          <FormattedMessage id="homePage" />
        </Typography>
        {(backableToMobileDirect || backableToMobileCode) && (
          <Typography
            variant="body2"
            color="textPrimary"
            style={{
              cursor: backableToMobileCode || backableToMobileDirect ? 'pointer' : undefined,
            }}
            onClick={() =>
              backableToMobileCode || backableToMobileDirect ? props.dispatch(go(-1)) : undefined
            }
          >
            <FormattedMessage
              id={backableToMobileDirect ? 'mobile.titleDirect' : 'mobile.titleCode'}
            />
          </Typography>
        )}
        <Typography color="secondary" variant="body2">
          <FormattedMessage id="mobile.historyBooking" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(MobileCardBreadcrumbs);
