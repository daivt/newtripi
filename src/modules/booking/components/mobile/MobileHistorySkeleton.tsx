import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { GREY } from '../../../../colors';

interface Props {}

const MobileHistorySkeleton: React.FunctionComponent<Props> = props => {
  return (
    <div
      style={{
        boxShadow:
          '0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)',
        border: `1px solid ${GREY}`,
        background: 'white',
        borderRadius: '4px',
        overflow: 'hidden',
        display: 'flex',
        paddingRight: '20px',
        width: '100%',
        margin: '10px 0px',
      }}
    >
      <Skeleton
        style={{ width: '122px', height: '72px', marginRight: '20px', marginLeft: '20px' }}
      />
      <div style={{ flex: 1 }}>
        <Skeleton width={150} />
        <div style={{ display: 'flex' }}>
          <div style={{ display: 'flex', flex: 1 }}>
            <div style={{ width: '50%' }}>
              <Skeleton width={250} />
              <Skeleton width={200} />
              <Skeleton width={225} />
            </div>
            <div style={{ width: '50%' }}>
              <Skeleton width={250} />
              <Skeleton width={200} />
              <Skeleton width={225} />
            </div>
          </div>
          <div
            style={{
              alignSelf: 'flex-end',
              paddingBottom: '16px',
            }}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Skeleton style={{ marginBottom: '10px' }} width={75} />
            </div>
            <Skeleton style={{ width: '170px', height: '30px' }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MobileHistorySkeleton;
