import { Breadcrumbs, Typography } from '@material-ui/core';
import { go } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const MobileTopupPayBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToMobileDirect = state && state.backableToMobileDirect;
  const backableToMobileCode = state && state.backableToMobileCode;
  const backableToMobileHistory = state && state.backableToMobileHistory;
  const backableToHomePage = state && state.backableToHomePage;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToHomePage ? 'pointer' : undefined }}
          onClick={() =>
            backableToHomePage ? props.dispatch(go(backableToMobileHistory ? -3 : -2)) : undefined
          }
        >
          1.&nbsp;
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{
            cursor: backableToMobileCode || backableToMobileDirect ? 'pointer' : undefined,
          }}
          onClick={() =>
            backableToMobileCode || backableToMobileDirect
              ? props.dispatch(go(backableToMobileHistory ? -2 : -1))
              : undefined
          }
        >
          2.&nbsp;
          <FormattedMessage
            id={backableToMobileDirect ? 'mobile.titleDirect' : 'mobile.titleCode'}
          />
        </Typography>
        {backableToMobileHistory && (
          <Typography
            variant="body2"
            color="textPrimary"
            style={{ cursor: backableToMobileHistory ? 'pointer' : undefined }}
            onClick={() => (backableToMobileHistory ? props.dispatch(go(-1)) : undefined)}
          >
            3.&nbsp;
            <FormattedMessage id="mobile.historyBooking" />
          </Typography>
        )}
        <Typography color="secondary" variant="body2">
          {backableToMobileHistory ? '4' : '3'}. <FormattedMessage id="mobile.payTitle" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(MobileTopupPayBreadcrumbs);
