import { Breadcrumbs, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const MobileCardBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToHomePage = state && state.backableToHomePage;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToHomePage ? 'pointer' : undefined }}
          onClick={() => (backableToHomePage ? props.dispatch(go(-1)) : undefined)}
        >
          1.&nbsp;
          <FormattedMessage id="homePage" />
        </Typography>
        <Typography color="secondary" variant="body2">
          2.&nbsp;
          <FormattedMessage id="mobile.titleDirect" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(MobileCardBreadcrumbs);
