import { Container } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import MobileHistoryBreadcrumbs from './MobileHistoryBreadcrumbs';
import MobileHistoryMain from './MobileHistoryMain';

export interface Props {}
const MobileHistoryTabletDeskTop: React.FunctionComponent<Props> = props => {
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <div>
        <Header light={true} />
      </div>
      <Container style={{ flex: 1 }}>
        <MobileHistoryBreadcrumbs />
        <MobileHistoryMain />
      </Container>
      <Footer />
    </PageWrapper>
  );
};
export default connect()(MobileHistoryTabletDeskTop);
