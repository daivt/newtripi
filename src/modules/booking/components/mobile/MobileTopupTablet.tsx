import { Container, IconButton, Slide, Typography, Button } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { some, TABLET_WIDTH, ROUTES } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { MobileTopupBuyer, BuyerInfoValidation } from '../../../common/models';
import MobileTopupBreadcrumbs from './MobileTopupBreadcrumbs';
import MobileTopupMain from './MobileTopupMain';
import MobileTopupSideBox from './MobileTopupSideBox';
import { Line } from '../../../result/components/hotel/HotelDetailTabs/styles';
import Link from '../../../common/components/Link';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';

interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileTopupInfo: MobileTopupBuyer;
  mobileData: some[];
  updateBuyerInfo(info: MobileTopupBuyer): void;
  continue_(): void;
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
}
const MobileTopupTablet: React.FunctionComponent<Props> = props => {
  const { buyerMobileTopupInfo, mobileData, routerState } = props;
  const [showInfoBox, setShowInfoBox] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex', overflow: 'hidden' }}>
          <div style={{ flex: 1 }}>
            <MobileTopupBreadcrumbs />
            <Line style={{ justifyContent: 'space-between', marginBottom: '12px' }}>
              <Typography variant="h5">
                <FormattedMessage id="mobile.titleDirect" />
              </Typography>
              <Link
                to={{
                  pathname: ROUTES.mobile.mobileHistory,
                  state: { ...routerState, backableToMobileDirect: true },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ width: '170px', height: '40px' }}
                >
                  <Typography variant="subtitle2">
                    <FormattedMessage id="mobile.history" />
                  </Typography>
                </Button>
              </Link>
            </Line>
            <MobileTopupMain {...props} style={{ flex: 1 }} />
          </div>
          <div
            style={{
              position: 'relative',
              direction: 'rtl',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {!showInfoBox && (
              <IconButton
                style={{ margin: '10px', position: 'absolute' }}
                onClick={() => setShowInfoBox(true)}
              >
                <Open style={{ color: DARK_GREY }} />
              </IconButton>
            )}
            <div style={{ width: 1, flex: 1, display: 'flex' }}>
              <Slide in={showInfoBox} direction="left">
                <div
                  style={{
                    backgroundColor: BACKGROUND,
                    padding: '22px 0 22px 10px',
                    marginTop: '-22px',
                    borderLeft: `1px solid ${GREY}`,
                    width: '420px',
                  }}
                >
                  <div style={{ textAlign: 'end' }}>
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShowInfoBox(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  </div>
                  <div style={{ direction: 'ltr' }}>
                    <MobileTopupSideBox
                      buyerMobileTopupInfo={buyerMobileTopupInfo}
                      mobileData={mobileData}
                    />
                  </div>
                </div>
              </Slide>
            </div>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};
function mapStateToProps(state: AppState) {
  return { routerState: state.router.location.state };
}
export default connect(mapStateToProps)(MobileTopupTablet);
