import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { Wrapper, SectionHeader } from '../styles';
import { FormattedMessage } from 'react-intl';
import MobilePayBreadcrumbs from './MobileCardPayBreadcrumbs';
import MobileCardPayBox from './MobileCardPayBox';
import MobileCardSideBox from './MobileCardSideBox';
import MessageDialog from '../../../common/components/MessageDialog';
import { setErrorMessage } from '../../redux/mobileReducer';
import { goBack } from 'connected-react-router';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.mobile,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {}

class MobileCardPayDesktop extends React.Component<Props, State> {
  public render() {
    const { booking, dispatch } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light={true} />
        <Container style={{ flex: 1 }}>
          <MobilePayBreadcrumbs />
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="booking.pay" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  <MobileCardPayBox />
                </div>
                {booking.buyerMobileCardInfo && (
                  <MobileCardSideBox
                    buyerMobileCardInfo={booking.buyerMobileCardInfo}
                    mobileData={booking.mobileData ? booking.mobileData : []}
                  />
                )}
              </>
            )}
          </div>
          <MessageDialog
            show={!!booking.errorMessage}
            onClose={() => {
              dispatch(setErrorMessage());
              dispatch(goBack());
            }}
            message={
              <div
                style={{
                  padding: '18px',
                  textAlign: 'center',
                }}
              >
                <Typography variant="body1">{booking.errorMessage}</Typography>
              </div>
            }
          />
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(MobileCardPayDesktop);
