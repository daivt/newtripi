import { Button, Grid, Typography } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import Skeleton from '@material-ui/lab/Skeleton';
import { push } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE, GREEN, SECONDARY } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { ReactComponent as IconChecked } from '../../../../svg/ic_checked.svg';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import { BuyerInfoValidation, MobileTopupBuyer } from '../../../common/models';
import { validTelephoneRegex } from '../../../common/utils';
import { getOperatorCode } from '../../utils';
import { FreeTextField } from '../Form';
import { Box, SectionHeader } from '../styles';
const CustomButton = (props: ButtonProps & { active: boolean }) => {
  const { active, ...rest } = props;
  return (
    <Button
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active ? GREEN : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        padding: '3px 8px',
        width: '128px',
        height: '32px',
        textTransform: 'none',
        color: active ? 'white' : '#757575',
        borderColor: active ? GREEN : undefined,
      }}
      {...rest}
    >
      {props.children}
    </Button>
  );
};

interface Props extends WrappedComponentProps {
  dispatch: Dispatch;
  style?: React.CSSProperties;
  buyerMobileTopupInfo: MobileTopupBuyer;
  mobileData: some[];
  updateBuyerInfo(buyerMobileTopupInfo: MobileTopupBuyer): void;
  continue_(): void;
  buyerInfoValidation: BuyerInfoValidation;
  updateBuyerInfoValidation(validation: BuyerInfoValidation): void;
}

interface State {
  isSelectContact: boolean;
  key: boolean;
}

class MobileTopupMain extends React.PureComponent<Props, State> {
  state: State = {
    isSelectContact: false,
    key: false,
  };

  onChangPhoneNumber(phone: string) {
    const {
      mobileData,
      buyerMobileTopupInfo,
      updateBuyerInfo,
      updateBuyerInfoValidation,
      buyerInfoValidation,
    } = this.props;
    const info = { ...buyerMobileTopupInfo };
    if (phone.length > 9) {
      info.operatorCode = getOperatorCode(phone, mobileData);
    }
    updateBuyerInfo({ ...info, phone });
    updateBuyerInfoValidation({ ...buyerInfoValidation, phone: true });
    this.setState({ isSelectContact: false });
  }
  render() {
    const {
      mobileData,
      buyerMobileTopupInfo,
      updateBuyerInfo,
      continue_,
      intl,
      dispatch,
      buyerInfoValidation,
    } = this.props;
    const { isSelectContact, key } = this.state;
    const ListCard = mobileData.find((v: some) => v.code === buyerMobileTopupInfo.operatorCode);
    const operator = mobileData
      .filter((v: some) => v.mobileTopupProducts && v.mobileTopupProducts.length > 0)
      .map((v: some) => v.name)
      .join(', ');
    return (
      <div style={{ marginBottom: '30px', ...this.props.style }}>
        <Box style={{ height: '100%' }}>
          <Typography variant="body1" style={{ margin: '12px 0px' }}>
            <FormattedMessage id="mobile.suportedNetworkOperator" />
          </Typography>
          <Grid style={{ marginBottom: '24px', width: '610px' }} container spacing={3}>
            {mobileData.map((v: some) => (
              <Grid
                key={v.id}
                item
                style={{
                  padding: '8px 12px',
                  width: '20%',
                }}
              >
                {!v.skeleton ? (
                  <Button
                    variant="outlined"
                    onClick={() => {
                      updateBuyerInfo({
                        ...buyerMobileTopupInfo,
                        operatorCode: v.code,
                      });
                    }}
                    style={{
                      boxShadow:
                        v.code === buyerMobileTopupInfo.operatorCode
                          ? `0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px ${SECONDARY}, 0px 2px 4px rgba(0, 0, 0, 0.14)`
                          : '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 3px 4px rgba(0, 0, 0, 0.12), 0px 2px 4px rgba(0, 0, 0, 0.14)',
                      borderRadius: '2px',
                      height: '59px',
                      width: '106px',
                      padding: '0px',
                      border:
                        v.code === buyerMobileTopupInfo.operatorCode
                          ? `1px solid ${SECONDARY}`
                          : undefined,
                    }}
                  >
                    <img
                      style={{
                        width: '100%',
                        height: '100%',
                        objectFit: 'contain',
                        padding: '0px',
                      }}
                      src={v.logoUrl}
                      alt=""
                    />{' '}
                    {v.code === buyerMobileTopupInfo.operatorCode && (
                      <IconChecked style={{ position: 'absolute', top: '0px', right: '0px' }} />
                    )}
                  </Button>
                ) : (
                  <Skeleton width={106} height={59} variant="rect"></Skeleton>
                )}
              </Grid>
            ))}
          </Grid>
          <div style={{ marginBottom: '12px', width: '450px' }}>
            <Typography variant="h6">
              <FormattedMessage id="mobile.initPhone" />
            </Typography>
            <div
              style={{
                display: 'flex',
                marginBottom: '12px',
                marginTop: '12px',
                alignItems: 'center',
              }}
            >
              <FreeTextField
                key={`${key}`}
                valid={buyerInfoValidation.phone}
                text={buyerMobileTopupInfo.phone || ''}
                placeholder={intl.formatMessage({ id: 'mobile.phoneEx' })}
                update={phone => {
                  this.onChangPhoneNumber(phone);
                }}
                regex={validTelephoneRegex}
              />

              <Button
                variant="text"
                onClick={() =>
                  this.setState({
                    isSelectContact: true,
                  })
                }
              >
                <IconContactList />
                <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
                  <FormattedMessage id="booking.addFromContact" />
                </Typography>
              </Button>
            </div>
          </div>
          {ListCard && ListCard.mobileTopupProducts && ListCard.mobileTopupProducts.length > 0 ? (
            <>
              <SectionHeader>
                <Typography variant="h6">
                  <FormattedMessage id="mobile.valueCard" />
                </Typography>
              </SectionHeader>
              <div
                style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', width: '488px' }}
              >
                {ListCard.mobileTopupProducts.map((v: some, index: number) => (
                  <div style={{ padding: '6px' }} key={v.id}>
                    <CustomButton
                      active={buyerMobileTopupInfo.cardValue === v.value}
                      onClick={() =>
                        updateBuyerInfo({
                          ...buyerMobileTopupInfo,
                          cardValue: v.value,
                        })
                      }
                    >
                      <Typography variant="body2">{v.formatedValue}</Typography>
                    </CustomButton>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <Typography variant="body2">
              <FormattedMessage
                id="mobile.suportedNetworkOperatorNote"
                values={{ text: operator }}
              />
            </Typography>
          )}
          <div style={{ margin: '32px 0px' }}>
            <Button
              size="large"
              style={{ minWidth: '170px' }}
              variant="contained"
              color="secondary"
              onClick={() => {
                if (ListCard?.mobileTopupProducts?.length > 0) {
                  continue_();
                  this.setState({ key: !key });
                } else {
                  dispatch(push(`${ROUTES.mobile.mobileCard}`));
                }
              }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage
                  id={ListCard?.mobileTopupProducts?.length > 0 ? 'pay' : 'mobile.titleCode'}
                />
              </Typography>
            </Button>
          </div>
        </Box>{' '}
        <SelectContactDialog
          show={isSelectContact}
          onClose={() => this.setState({ isSelectContact: false })}
          onSelect={guest => this.onChangPhoneNumber(guest.phone)}
        />
      </div>
    );
  }
}

export default connect()(injectIntl(MobileTopupMain));
