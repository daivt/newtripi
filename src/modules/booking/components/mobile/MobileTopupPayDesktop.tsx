import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { SectionHeader, Wrapper } from '../styles';
import MobileTopupSideBox from './MobileTopupSideBox';
import MobileTopupDirectPayBox from './MobileTopupPayBox';
import MessageDialog from '../../../common/components/MessageDialog';
import { setErrorMessage } from '../../redux/mobileReducer';
import { goBack } from 'connected-react-router';
import MobileTopupPayBreadcrumbs from './MobileTopupPayBreadcrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.mobile,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {}

class MobileTopupPayDesktop extends React.Component<Props, State> {
  public render() {
    const { booking, dispatch } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light={true} />
        <Container style={{ flex: 1 }}>
          <MobileTopupPayBreadcrumbs />
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="booking.pay" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  <MobileTopupDirectPayBox />
                </div>
                <MobileTopupSideBox
                  buyerMobileTopupInfo={booking.buyerMobileTopupInfo}
                  mobileData={booking.mobileData ? booking.mobileData : []}
                />
              </>
            )}
            <MessageDialog
              show={!!booking.errorMessage}
              onClose={() => {
                dispatch(setErrorMessage());
                dispatch(goBack());
              }}
              message={
                <div
                  style={{
                    padding: '18px',
                    textAlign: 'center',
                  }}
                >
                  <Typography variant="body1">{booking.errorMessage}</Typography>
                </div>
              }
            />
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(MobileTopupPayDesktop);
