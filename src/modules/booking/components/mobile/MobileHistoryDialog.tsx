import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { push } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, PRIMARY, SECONDARY, RED } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { CustomTab, CustomTabs } from '../../../common/components/elements';
import { setbuyerMobileTopupInfo, setBuyerInfo } from '../../redux/mobileReducer';
import { CopyTextField } from '../Form';
const Line = styled.div`
  display: flex;
  min-height: 28px;
  align-items: center;
`;
const ScrollBar = styled.div`
  overflow: auto;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border-right: 8px solid white;
    /* border-left: 8px solid white; */
  }
  &::-webkit-scrollbar {
    background: transparent;
    width: 16px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;
interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  open: boolean;
  onClose(): void;
  data: some;
  operators: some[];
  dispatch: Dispatch;
}
const TABS = ['mobile.orderDetail', 'mobile.listCard'];
const Card = (props: { data: some; index: number }) => {
  const { data, index } = props;
  return (
    <div style={{ marginBottom: '20px' }}>
      <Typography variant="subtitle2" style={{ margin: '0px 0px 16px 10px' }}>
        <FormattedMessage id="mobile.card" values={{ num: index }} />
      </Typography>
      <CopyTextField
        header={<FormattedMessage id="mobile.syntax" />}
        text={`*100*${data.pinCode}#`}
        inputStyle={{ color: SECONDARY }}
      />
      <CopyTextField
        style={{ marginTop: '16px' }}
        header={<FormattedMessage id="mobile.pinCode" />}
        text={`${data.pinCode}`}
        inputStyle={{ color: SECONDARY }}
      />
      <CopyTextField
        style={{ marginTop: '16px' }}
        header={<FormattedMessage id="mobile.serial" />}
        text={`${data.serial}`}
        inputStyle={{ color: SECONDARY }}
      />
    </div>
  );
};
const MODULE_CARD = 'card';
const MODULE_MOIBILE_TOPUP = 'mobile_topup';
const MobileHistoryDialog: React.FunctionComponent<Props> = props => {
  const { open, onClose, data, intl, operators, dispatch, routerState } = props;
  const [currentTabIndex, setCurrentTabIndex] = React.useState<number>(0);
  const operator = operators.find(v => v.code === data.provider.code);

  const getStatus = () => {
    let temp;
    if (data.paymentStatus === 'failed') {
      temp = { color: RED, id: 'fail' };
    } else {
      if (data.module === MODULE_CARD) {
        if (data.bookStatus === 'success') {
          temp = { color: PRIMARY, id: 'success' };
        } else {
          temp = { color: SECONDARY, id: 'processing' };
        }
      } else {
        temp = { color: PRIMARY, id: 'success' };
      }
    }
    return temp;
  };
  const feePrice = data.finalPrice - data.totalPrice + data.discount;
  return (
    <Dialog
      open={open}
      fullWidth
      maxWidth="xs"
      onClose={onClose}
      PaperProps={{ style: { width: '440px', maxHeight: '680px' } }}
    >
      <>
        <IconButton
          style={{
            position: 'absolute',
            right: '10px',
            top: '10px',
          }}
          color="default"
          size="small"
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
        <div style={{ padding: '16px 20px' }}>
          <Typography variant="h5" style={{ textAlign: 'center', marginBottom: '12px' }}>
            <FormattedMessage id="mobile.historyDetail" />
          </Typography>
          {data.module === MODULE_CARD &&
            data.paymentStatus === 'success' &&
            data.bookStatus === 'success' && (
              <CustomTabs
                variant="fullWidth"
                value={currentTabIndex === -1 ? false : currentTabIndex}
                onChange={(e, val) => setCurrentTabIndex(val)}
              >
                {TABS.map(tab => (
                  <CustomTab label={intl.formatMessage({ id: tab })} key={tab} />
                ))}
              </CustomTabs>
            )}
        </div>
        {currentTabIndex === 0 && (
          <div style={{ padding: '0px 20px 16px 20px' }}>
            <Line>
              <img
                src={data.provider.logoUrl}
                alt=""
                style={{ height: '72px', objectFit: 'contain', marginRight: '30px' }}
              />
              <Typography variant="subtitle2">
                {data.module === MODULE_CARD && <FormattedMessage id="mobile.titleCode" />}
                {data.module === MODULE_MOIBILE_TOPUP && (
                  <FormattedMessage id="mobile.titleDirect" />
                )}
                &nbsp;
                {operator && operator.name}
              </Typography>
            </Line>
            {data.contactInfo && (
              <>
                {getStatus().id === 'processing' && (
                  <Typography variant="body2" style={{ marginBottom: '16px' }}>
                    <FormattedMessage id="mobile.history.note" />
                  </Typography>
                )}
                <Line style={{ marginBottom: '8px' }}>
                  <Typography variant="subtitle2">
                    <FormattedMessage id="mobile.buyerInfo" />
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.name" />
                    :&nbsp;
                    {data.contactInfo.name}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.email" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: BLUE }}>
                    {data.contactInfo.email}
                  </Typography>
                </Line>
                <Line>
                  <Typography variant="body2">
                    <FormattedMessage id="mobile.phone" />
                    :&nbsp;
                    {data.contactInfo.phoneNumber}
                  </Typography>
                </Line>
              </>
            )}
            <Line style={{ marginTop: '20px', marginBottom: '8px' }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="mobile.orderInfo" />
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.bookingId" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" style={{ color: PRIMARY }}>
                {data.orderCode}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.createdTime" />
                :&nbsp; {moment(data.createdTime).format('HH:mm:ss[, ] DD/MM/YYYY')}
              </Typography>
            </Line>
            {data.value && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.priceCard" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={data.value} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}{' '}
            {!data.contactInfo && data.phoneNumber && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.phone" />
                  :&nbsp;
                  {data.phoneNumber}
                </Typography>
              </Line>
            )}
            {data.quantity && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.amount" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2">{data.quantity}</Typography>
              </Line>
            )}{' '}
            {data.value && data.totalPrice && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.discounts" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={data.totalPrice - data.value * (data.quantity || 1)} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {data.discount > 0 && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.promotion" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color="primary">
                  <FormattedNumber value={-data.discount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            {feePrice > 0 && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="booking.paymentFixedFeeSide" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={feePrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.finalPrice" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" color="secondary">
                <FormattedNumber value={data.finalPrice} /> &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
            {data.paymentMethod && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="mobile.paymentMethod" />
                  :&nbsp;{data.paymentMethod}
                </Typography>
              </Line>
            )}
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.status" />
                :&nbsp;
              </Typography>
              <Typography
                variant="body2"
                style={{
                  color: getStatus().color,
                }}
              >
                <FormattedMessage id={getStatus().id} />
              </Typography>
            </Line>
            {data.paymentStatus !== 'success' && (
              <Line style={{ textAlign: 'center', marginTop: '16px', justifyContent: 'center' }}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={async () => {
                    if (data.module === MODULE_CARD) {
                      await dispatch(
                        setBuyerInfo({
                          amount: data.quantity,
                          cardValue: data.value,
                          email: data.contactInfo.email,
                          name: data.contactInfo.name,
                          phone: data.contactInfo.phoneNumber,
                          operatorCode: data.provider.code,
                        }),
                      );
                      dispatch(
                        push({
                          pathname: ROUTES.mobile.mobileCardPay,
                          state: {
                            ...routerState,
                            backableToMobileHistory: true,
                          },
                        }),
                      );
                    } else if (data.module === MODULE_MOIBILE_TOPUP) {
                      await dispatch(
                        setbuyerMobileTopupInfo({
                          cardValue: data.value,
                          phone: data.phoneNumber,
                          operatorCode: data.provider.code,
                        }),
                      );
                      dispatch(
                        push({
                          pathname: ROUTES.mobile.mobileTopupPay,
                          state: {
                            ...routerState,
                            backableToMobileHistory: true,
                          },
                        }),
                      );
                    }
                  }}
                >
                  <Typography variant="subtitle2">
                    <FormattedMessage id="mobile.rePay" />
                  </Typography>
                </Button>
              </Line>
            )}
          </div>
        )}
        {currentTabIndex === 1 && data.cardsInfo && (
          <ScrollBar style={{ marginLeft: '20px', marginBottom: '16px' }}>
            {data.cardsInfo.map((v: some, index: number) => (
              <Card data={v} index={index + 1} key={index} />
            ))}
          </ScrollBar>
        )}
      </>
    </Dialog>
  );
};
const mapStateToProps = (state: AppState) => {
  return { routerState: state.router.location.state };
};
export default connect(mapStateToProps)(injectIntl(MobileHistoryDialog));
