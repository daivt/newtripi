import { IconButton, Typography, Button } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircleOutline';
import RemoveCircle from '@material-ui/icons/RemoveCircleOutline';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { BLUE, GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import { BuyerInfoValidation, MobileCardBuyer } from '../../../common/models';
import { validTelephoneRegex } from '../../../common/utils';
import { FreeTextField } from '../Form';
interface Props extends WrappedComponentProps {
  update(info: MobileCardBuyer, validation?: BuyerInfoValidation): void;
  info: MobileCardBuyer;
  validation: BuyerInfoValidation;
  ticktock: boolean;
}

const BuyerMobileCardInfoForm: React.FunctionComponent<Props> = props => {
  const { info, update, intl, validation, ticktock } = props;
  const [open, setOpen] = React.useState<boolean>(false);
  const onSelectBuyer = (buyer: some) => {
    setOpen(false);
    update(
      {
        ...info,
        name: `${buyer.lastName} ${buyer.firstName}`,
        email: buyer.email,
        phone: buyer.phone,
      },
      { email: true, name: true, phone: true },
    );
  };
  return (
    <div>
      <div style={{ marginBottom: '12px' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            width: '244px',
            marginTop: '16px',
            marginBottom: '24px',
            alignItems: 'center',
          }}
        >
          <Typography variant="body1">
            <FormattedMessage id="mobile.amount" />
          </Typography>
          <div style={{ display: 'flex', marginRight: '32px', alignItems: 'center' }}>
            <IconButton
              size="small"
              disabled={info.amount === 1}
              onClick={() => update({ ...info, amount: info.amount - 1 })}
            >
              <RemoveCircle
                style={{
                  fontSize: '24px',
                  color: info.amount === 1 ? GREY : undefined,
                }}
                color="primary"
              />
            </IconButton>
            <Typography style={{ padding: '0px 8px' }}>
              <FormattedNumber value={info.amount} />
            </Typography>
            <IconButton size="small" onClick={() => update({ ...info, amount: info.amount + 1 })}>
              <AddCircle
                style={{
                  fontSize: '24px',
                }}
                color="primary"
              />
            </IconButton>
          </div>
        </div>
        <Typography variant="h5">
          <FormattedMessage id="mobile.buyerInfo" />
        </Typography>
        <Typography variant="body2" style={{ marginTop: '8px' }}>
          <FormattedMessage id="mobile.note" />
        </Typography>

        <div style={{ display: 'flex', alignItems: 'flex-start', marginTop: '24px' }}>
          <div style={{ width: '344px' }}>
            <FreeTextField
              key={`name${ticktock}`}
              style={{ marginBottom: '14px' }}
              text={info.name || ''}
              valid={validation.name}
              header={intl.formatMessage({ id: 'mobile.name' })}
              placeholder={intl.formatMessage({ id: 'mobile.nameEx' })}
              update={name => update({ ...info, name }, { ...validation, name: true })}
            />
            <FreeTextField
              key={`email${ticktock}`}
              style={{ marginBottom: '14px' }}
              text={info.email || ''}
              valid={validation.email}
              header={intl.formatMessage({ id: 'mobile.email' })}
              placeholder={intl.formatMessage({ id: 'mobile.emailEx' })}
              update={email => update({ ...info, email }, { ...validation, email: true })}
            />
            <FreeTextField
              key={`phone${ticktock}`}
              text={info.phone || ''}
              valid={validation.phone}
              header={intl.formatMessage({ id: 'mobile.phone' })}
              placeholder={intl.formatMessage({ id: 'mobile.phoneEx' })}
              update={phone => update({ ...info, phone }, { ...validation, phone: true })}
              regex={validTelephoneRegex}
            />
          </div>
          <Button
            variant="text"
            style={{
              marginTop: '32px',
            }}
            onClick={() => setOpen(true)}
          >
            <IconContactList />
            <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
              <FormattedMessage id="booking.addFromContact" />
            </Typography>
          </Button>
        </div>
        <SelectContactDialog
          show={open}
          onClose={() => setOpen(false)}
          onSelect={guest => onSelectBuyer(guest)}
        />
      </div>
    </div>
  );
};

export default injectIntl(BuyerMobileCardInfoForm);
