import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import DiscountCodeBox from '../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../common/components/Payment/PaymentMethodsBox';
import { setSelectedPaymentMethod } from '../../redux/mobileReducer';
import { computeMobilePaybleNumbers } from '../../utils';
import { Box, SectionHeader } from '../styles';
const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.mobile,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const MobileCardPayBox: React.FunctionComponent<Props> = props => {
  const { booking, dispatch } = props;

  const payableNumbers = computeMobilePaybleNumbers(booking);
  const priceAfter = payableNumbers.finalPrice;

  return (
    <>
      <Box>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.discountCode" />
          </Typography>
        </SectionHeader>
        <DiscountCodeBox moduleType="mobile_card" />
      </Box>
      {booking.paymentMethods && booking.selectedPaymentMethod && (
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
          </SectionHeader>
          <PaymentMethodsBox
            paymentMethods={booking.paymentMethods}
            total={payableNumbers.finalPrice}
            setSelectedMethod={(method: some) => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={booking.selectedPaymentMethod}
            priceAfter={priceAfter}
            promotionCode={booking.promotionCode}
            moduleType="mobile_card"
          />
        </Box>
      )}
      <div
        style={{
          padding: '16px 16px 32px 16px',
          color: BLUE,
        }}
      >
        <Typography variant="body2">
          <FormattedMessage id="booking.seePaymentDetail" />
        </Typography>
      </div>
    </>
  );
};

export default connect(mapState2Props)(MobileCardPayBox);
