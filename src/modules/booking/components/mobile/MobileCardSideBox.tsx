import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { DARK_GREY, RED } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { MobileCardBuyer } from '../../../common/models';
import { Box } from '../styles';
import { computeMobilePaybleNumbers } from '../../utils';

const Line = styled.div`
  min-height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
interface Props extends ReturnType<typeof mapStateToProps> {
  buyerMobileCardInfo: MobileCardBuyer;
  mobileData: some[];
}

const MobileCardSideBox: React.FunctionComponent<Props> = props => {
  const { buyerMobileCardInfo, mobileData, userData, mobile } = props;
  const operator = buyerMobileCardInfo && mobileData.find((v: some) => v.code === buyerMobileCardInfo.operatorCode);
  const card =
    operator &&
    operator.cardProducts &&
    operator.cardProducts.find((v: some) => v.value === buyerMobileCardInfo.cardValue);
  const finalPrice = buyerMobileCardInfo ? buyerMobileCardInfo.amount * (card ? card.value : 0) : 0;
  const checkUrl = window.location.pathname === ROUTES.mobile.mobileCardPay;
  const payableNumbers = computeMobilePaybleNumbers(mobile);
  return (
    <Box style={{ width: '368px', marginLeft: '32px', padding: '14px 16px', height: '100%' }}>
      <Typography variant="h5">
        <FormattedMessage id="mobile.orderDetail" />
      </Typography>
      {checkUrl && (
        <>
          <Line style={{ height: '48px' }}>
            <Typography variant="h6">
              <FormattedMessage id="mobile.buyerInfo" />
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.name" />
            </Typography>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              {buyerMobileCardInfo && buyerMobileCardInfo.name}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.email" />
            </Typography>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              {buyerMobileCardInfo && buyerMobileCardInfo.email}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.phone" />
            </Typography>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              {buyerMobileCardInfo && buyerMobileCardInfo.phone}
            </Typography>
          </Line>
        </>
      )}
      {userData && (
        <>
          <Line style={{ marginTop: '28px' }}>
            <Typography variant="body2">
              <FormattedMessage id="account" />
            </Typography>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              {userData.name}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.balance" />
            </Typography>
            <Typography variant="body2" style={{ color: DARK_GREY }}>
              <FormattedNumber value={userData.credit} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </>
      )}
      <Line style={{ height: '48px', marginTop: '24px' }}>
        <Typography variant="h6">
          <FormattedMessage id="mobile.priceInfo" />
        </Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="mobile.operator" />
        </Typography>
        <Line>
          <img
            style={{
              maxWidth: '80px',
              maxHeight: '40px',
              objectFit: 'contain',
              marginRight: '8px',
            }}
            src={operator && operator.logoUrl}
            alt=""
          />
          <Typography variant="body2" style={{ color: DARK_GREY }}>
            {operator && operator.name}
          </Typography>
        </Line>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="mobile.priceCard" />
        </Typography>
        <Typography variant="body2" color="secondary">
          {card && card.formatedValue}
        </Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="totalPrice" />
        </Typography>
        <Line style={{ justifyContent: 'flex-start' }}>
          {card && (
            <>
              <FormattedNumber value={buyerMobileCardInfo ? buyerMobileCardInfo.amount : 0} />
              &nbsp;x&nbsp;
              <Typography variant="body2" color="secondary">
                {card && card.formatedValue}
              </Typography>
            </>
          )}
        </Line>
      </Line>
      {card && !checkUrl && (
        <Line>
          <Typography variant="subtitle2">
            <FormattedMessage id="mobile.finalPrice" />
          </Typography>
          <Typography color="error" variant="h6">
            <FormattedNumber value={Math.round(finalPrice)} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>
      )}{' '}
      {card && checkUrl && payableNumbers.cardInfo && (
        <>
          {payableNumbers.fee !== 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage
                  id={payableNumbers.fee > 0 ? 'booking.paymentFixedFeeSide' : 'booking.paymentFixedDiscountSide'}
                />
              </Typography>
              <Typography variant="body2" color={payableNumbers.fee < 0 ? 'primary' : 'secondary'}>
                <FormattedNumber value={payableNumbers.fee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="mobile.discounts" />
            </Typography>
            <Typography variant="body2" color="primary">
              <FormattedNumber
                value={
                  payableNumbers.cardInfo.totalPrice - buyerMobileCardInfo.amount * payableNumbers.cardInfo.productValue
                }
              />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          {payableNumbers.promotion > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="mobile.promotion" />
              </Typography>
              <Typography variant="body2" color="primary">
                <FormattedNumber value={-payableNumbers.promotion} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="mobile.finalPrice" />
            </Typography>
            <Typography variant="h6" style={{ color: RED }}>
              <FormattedNumber value={payableNumbers.finalPrice - payableNumbers.pointToAmount + payableNumbers.fee} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </>
      )}
    </Box>
  );
};

const mapStateToProps = (state: AppState) => ({
  userData: state.account.userData,
  mobile: state.booking.mobile,
});
export default connect(mapStateToProps)(MobileCardSideBox);
