import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { BLUE } from '../../../../colors';
import { PAGE_SIZE, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import MessageDialog from '../../../common/components/MessageDialog';
import NoDataResult from '../../../common/components/NoDataResult';
import { fetchThunk } from '../../../common/redux/thunks';
import MobileHistoryCard from './MobileHistoryCard';
import MobileHistoryDialog from './MobileHistoryDialog';
import MobileHistorySkeleton from './MobileHistorySkeleton';
export interface Props {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const MobileHistoryMain: React.FunctionComponent<Props> = props => {
  const { dispatch } = props;
  const [bookings, setBookings] = React.useState<some[]>([]);
  const [operators, setOperators] = React.useState<some[]>([]);
  const [page, setPage] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [dataDetail, setDataDetail] = React.useState<some | undefined>();
  const [total, setTotal] = React.useState(0);
  const [message, setMessage] = React.useState<string>('');
  const loadMore = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getMobileHistoryData}`,
        'post',
        true,
        JSON.stringify({ paging: { page: page + 1, pageSize: PAGE_SIZE } }),
      ),
    );
    if (json.code === 200) {
      setBookings(b => b && b.concat(json.data.bookings));
      setPage(page + 1);
    } else {
      setMessage(json.message);
    }
    setLoading(false);
  }, [dispatch, page]);
  const fetchData = React.useCallback(async () => {
    setBookings([]);
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getMobileHistoryData}`,
        'post',
        true,
        JSON.stringify({
          paging: { page: 1, pageSize: PAGE_SIZE },
        }),
      ),
    );
    if (json.code === 200) {
      setPage(1);
      setBookings(json.data.bookings);
      setTotal(json.data.total);
      setOperators(json.data.filters.providers);
    } else {
      setTotal(0);
      setMessage(json.message);
    }
    setLoading(false);
  }, [dispatch]);
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);
  return (
    <>
      <Typography variant="h5">
        <FormattedMessage id="mobile.historyBooking" />
      </Typography>
      <div>
        {bookings.map(one => (
          <MobileHistoryCard
            key={one.bookingId}
            data={one}
            onClick={() => {
              setDataDetail(one);
            }}
            operators={operators}
          />
        ))}

        {!loading && total - PAGE_SIZE * page > 0 && (
          <div style={{ textAlign: 'center', margin: '24px 0px' }}>
            <Button onClick={loadMore}>
              <Typography style={{ color: BLUE }}>
                <FormattedMessage
                  id="result.displayMore"
                  values={{ num: total - PAGE_SIZE * page }}
                />
              </Typography>
            </Button>
          </div>
        )}
        {!bookings.length && !loading && (
          <NoDataResult id="m.noTransaction" style={{ marginTop: '48px' }} />
        )}
        {loading && (
          <>
            <MobileHistorySkeleton />
            <MobileHistorySkeleton />
            <MobileHistorySkeleton />
            <MobileHistorySkeleton />
          </>
        )}
      </div>
      {dataDetail && (
        <MobileHistoryDialog
          open={!!dataDetail}
          operators={operators}
          onClose={() => {
            setDataDetail(undefined);
          }}
          data={dataDetail}
        />
      )}
      <MessageDialog
        show={!!message}
        onClose={() => setMessage('')}
        message={
          <div>
            <Typography style={{ textAlign: 'center' }}>{message}</Typography>
          </div>
        }
      />
    </>
  );
};
export default connect()(MobileHistoryMain);
