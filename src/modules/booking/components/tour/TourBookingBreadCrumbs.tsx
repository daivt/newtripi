import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const TourBreadcrums: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToSearch = state && state.backableToSearch;
  const backableToDetail = state && state.backableToDetail;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToSearch ? 'pointer' : undefined }}
          onClick={() => (backableToSearch ? props.dispatch(go(-1 - state.backableToSearch)) : undefined)}
        >
          1.&nbsp;
          <FormattedMessage id="tour.search" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToDetail ? 'pointer' : undefined }}
          onClick={() => (backableToDetail ? props.dispatch(go(-1)) : undefined)}
        >
          2.&nbsp;
          <FormattedMessage id="tour.infoTour" />
        </Typography>
        <Typography variant="body2" color="secondary">
          3.&nbsp;
          <FormattedMessage id="tour.bookingInfo" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          4.&nbsp;
          <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(TourBreadcrums);
