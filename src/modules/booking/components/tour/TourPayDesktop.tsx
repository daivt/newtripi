import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { SectionHeader, Wrapper } from '../styles';
import TourBookingSideBox from './TourBookingSideBox';
import TourPayBox from './TourPayBox';
import TourPayBreadCrumbs from './TourPayBreadCrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.tour,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

class TourPayDesktop extends PureComponent<Props> {
  render() {
    const { booking } = this.props;

    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <TourPayBreadCrumbs />
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="booking.pay" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  <TourPayBox />
                </div>
                <TourBookingSideBox promotion={booking.promotion} />
              </>
            )}
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(TourPayDesktop);
