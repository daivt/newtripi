import { Typography } from '@material-ui/core';
import IconWarning from '@material-ui/icons/ReportProblemOutlined';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { DARK_GREY } from '../../../../colors';
import { TourCustomerInfo, TourCustomerInfoValidation } from '../../../common/models/index';
import { validTelephoneRegex } from '../../../common/utils';
import { FreeTextField } from '../Form';

interface Props {
  info: TourCustomerInfo;
  updateTourCustomerInfo(tourCustomerInfo: TourCustomerInfo): void;
  valid: TourCustomerInfoValidation;
  ticktock: boolean;
}

const TourCustomerInfoForm: React.FC<Props> = props => {
  const { info, updateTourCustomerInfo, valid, ticktock } = props;

  const intl = useIntl();

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ display: 'flex' }}>
        <FreeTextField
          key={`fullName${ticktock}`}
          text={info.fullName}
          header={intl.formatMessage({ id: 'tour.booking.fullName' })}
          placeholder={intl.formatMessage({ id: 'tour.booking.fullNameEx' })}
          update={fullName => updateTourCustomerInfo({ ...info, fullName })}
          valid={valid.fullName}
        />
        <FreeTextField
          key={`phone${ticktock}`}
          text={info.phone}
          regex={validTelephoneRegex}
          header={intl.formatMessage({ id: 'telephone' })}
          placeholder={intl.formatMessage({ id: 'telephoneEx' })}
          update={phone => updateTourCustomerInfo({ ...info, phone })}
          valid={valid.phone}
        />
      </div>
      <div style={{ display: 'flex', alignItems: 'center', padding: '16px 0' }}>
        <IconWarning style={{ marginRight: '8px', color: DARK_GREY }} />
        <Typography variant="caption" color="textSecondary">
          <FormattedMessage
            id="tour.booking.notify"
            values={{
              text: (
                <strong>
                  <FormattedMessage id="tour.booking.notifyInfo" />
                </strong>
              ),
            }}
          />
        </Typography>
      </div>
    </div>
  );
};

export default TourCustomerInfoForm;
