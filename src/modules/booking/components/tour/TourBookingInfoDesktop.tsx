import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { DESKTOP_WIDTH, some } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { TourCustomerInfo, TourCustomerInfoValidation } from '../../../common/models';
import TourBreadcrums from './TourBookingBreadCrumbs';
import TourBookingInfoBox from './TourBookingInfoBox';
import TourBookingSideBox from './TourBookingSideBox';
import LoadingIcon from '../../../common/components/LoadingIcon';

interface ITourBookingInfoDesktopProps {
  tourData?: some;
  tourCustomerInfo: TourCustomerInfo;
  tourCustomerInfoValidation: TourCustomerInfoValidation;
  updateTourCustomerInfo(tourCustomerInfo: TourCustomerInfo): void;
  continue_(): void;
}

const TourBookingInfoDesktop: React.FunctionComponent<ITourBookingInfoDesktopProps> = props => {
  const { tourData, tourCustomerInfo, updateTourCustomerInfo, tourCustomerInfoValidation, continue_ } = props;

  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <TourBreadcrums />
        <Typography variant="h5" style={{ marginBottom: '16px' }}>
          <FormattedMessage id="tour.bookingInfo" />
        </Typography>
        {tourData ? (
          <div style={{ display: 'flex' }}>
            <TourBookingInfoBox
              tourData={tourData}
              tourCustomerInfo={tourCustomerInfo}
              tourCustomerInfoValidation={tourCustomerInfoValidation}
              updateTourCustomerInfo={updateTourCustomerInfo}
              continue_={continue_}
            />
            <TourBookingSideBox />
          </div>
        ) : (
          <LoadingIcon style={{ height: '150px' }} />
        )}
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default TourBookingInfoDesktop;
