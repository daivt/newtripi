import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { some, TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { TourCustomerInfo, TourCustomerInfoValidation } from '../../../common/models';
import TourBreadcrums from './TourBookingBreadCrumbs';
import TourBookingInfoBox from './TourBookingInfoBox';
import TourBookingSideBox from './TourBookingSideBox';

interface Props {
  tourData?: some;
  tourCustomerInfo: TourCustomerInfo;
  tourCustomerInfoValidation: TourCustomerInfoValidation;
  updateTourCustomerInfo(tourCustomerInfo: TourCustomerInfo): void;
  continue_(): void;
}

const TourBookingInfoTablet: React.FC<Props> = props => {
  const { tourData, tourCustomerInfo, updateTourCustomerInfo, tourCustomerInfoValidation, continue_ } = props;
  const [showInfoBox, setShowInfoBox] = React.useState(false);

  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        {tourData ? (
          <div style={{ display: 'flex', overflow: 'hidden' }}>
            <div style={{ flex: 1 }}>
              <TourBreadcrums />
              <Typography variant="h5" style={{ marginBottom: '16px' }}>
                <FormattedMessage id="tour.bookingInfo" />
              </Typography>
              {tourData ? (
                <TourBookingInfoBox
                  tourData={tourData}
                  tourCustomerInfo={tourCustomerInfo}
                  tourCustomerInfoValidation={tourCustomerInfoValidation}
                  updateTourCustomerInfo={updateTourCustomerInfo}
                  continue_={continue_}
                />
              ) : (
                <LoadingIcon
                  style={{
                    margin: '50px 0',
                  }}
                />
              )}
            </div>
            <div
              style={{
                position: 'relative',
                direction: 'rtl',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {!showInfoBox && (
                <IconButton style={{ margin: '10px', position: 'absolute' }} onClick={() => setShowInfoBox(true)}>
                  <Open style={{ color: DARK_GREY }} />
                </IconButton>
              )}
              <div style={{ width: 1, flex: 1, display: 'flex' }}>
                <Slide in={showInfoBox} direction="left">
                  <div
                    style={{
                      backgroundColor: BACKGROUND,
                      padding: '22px 0 22px 10px',
                      marginTop: '-22px',
                      borderLeft: `1px solid ${GREY}`,
                      width: '420px',
                    }}
                  >
                    <div style={{ textAlign: 'end' }}>
                      <IconButton style={{ margin: '10px 0' }} onClick={() => setShowInfoBox(false)}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    </div>
                    <div style={{ direction: 'ltr' }}>
                      <TourBookingSideBox />
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        ) : (
          <LoadingIcon style={{ height: '150px' }} />
        )}
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default TourBookingInfoTablet;
