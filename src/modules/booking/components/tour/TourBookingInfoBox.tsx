import { Button, ButtonBase, Radio, RadioGroup, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { BLUE, GREY, HOVER_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import { TourCustomerInfo, TourCustomerInfoValidation } from '../../../common/models/index';
import { fetchPackages } from '../../../result/redux/tourResultReducer';
import {
  getTourAvailabilities,
  setActivityPackage,
  setAdditionalRequest,
  setDate,
  setTourCustomerCountInfo,
} from '../../redux/tourBookingReducer';
import { FreeTextField } from '../Form';
import { AddContact, Box, SectionHeader } from '../styles';
import TourCustomerInfoForm from './TourCustomerInfoForm';
import TourDatePicker from './TourDatePicker';
import { TouristCountItem } from './TouristCountItem';
import { DEFAULT_TOUR_CUSTOMER_COUNT } from '../../constants';

const ItemDiv = styled.div<{ first: boolean }>`
  display: flex;
  padding: 14px 5px;
  border-top: ${props => (props.first ? '' : `0.5px solid ${GREY}`)};
  cursor: pointer;
  :hover {
    background: ${HOVER_GREY};
  }
`;

export interface ITourBookingInfoBoxProps extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  tourData: some;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  tourCustomerInfo: TourCustomerInfo;
  tourCustomerInfoValidation: TourCustomerInfoValidation;
  updateTourCustomerInfo(tourCustomerInfo: TourCustomerInfo): void;
  continue_(): void;
}

interface State {
  isSelectContact: boolean;
  packagesData?: { activityPackagePriceList: some[] };
  packageCollapse: boolean[];
  tourAvailabilities: some;
  ticktock: boolean;
}

class TourBookingInfoBox extends React.Component<ITourBookingInfoBoxProps, State> {
  state: State = {
    isSelectContact: false,
    tourAvailabilities: {
      isDaily: false,
      availabilities: [],
    },
    ticktock: false,
    packageCollapse: [],
  };

  async componentDidMount() {
    const { dispatch } = this.props;
    this.fetchPackages();
    const json = await dispatch(getTourAvailabilities());

    if (json) {
      this.setState({ tourAvailabilities: json.data });
    }
  }

  onSelectContact(contact: some) {
    const { updateTourCustomerInfo } = this.props;

    const info: TourCustomerInfo = {
      id: contact.id,
      fullName: `${contact.lastName} ${contact.firstName}`,
      phone: contact.phone || '',
      email: contact.email,
    };

    this.setState({ isSelectContact: false });
    updateTourCustomerInfo(info);
  }

  fetchPackages = async (isDateChange: boolean = false) => {
    const { dispatch } = this.props;

    const json = await dispatch(fetchPackages());

    if (isDateChange) {
      dispatch(setActivityPackage());
      dispatch(setTourCustomerCountInfo(DEFAULT_TOUR_CUSTOMER_COUNT));
    }

    if (json && json.code === 200) {
      this.setState({
        packagesData: json.data,
        packageCollapse: Array(json.data.activityPackagePriceList.length).fill(true),
      });
    }
  };

  public render() {
    const {
      dispatch,
      booking,
      intl,
      tourCustomerInfo,
      updateTourCustomerInfo,
      tourCustomerInfoValidation,
    } = this.props;
    const { packagesData, isSelectContact, tourAvailabilities, ticktock, packageCollapse } = this.state;
    const guestCategoryDescription = booking.tourData ? booking.tourData.guestCategoryDescription : undefined;

    return (
      <div style={{ flex: 1 }}>
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="tour.booking.pickDate" />
            </Typography>
          </SectionHeader>
          <TourDatePicker
            tourAvailabilities={tourAvailabilities}
            date={booking.date || null}
            pickDate={date => {
              dispatch(setDate(date));
              this.fetchPackages(true);
            }}
          />
        </Box>
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="tour.booking.pickPackage" />
            </Typography>
          </SectionHeader>
          <RadioGroup radioGroup="activityPackage">
            {packagesData &&
              packagesData.activityPackagePriceList.map((one, i) => {
                const current = booking.activityPackage
                  ? booking.activityPackage.activityPackageId === one.activityPackageId
                  : false;

                return (
                  <ItemDiv
                    first={i === 0}
                    key={one.activityPackageId}
                    onClick={() =>
                      !current &&
                      dispatch(setActivityPackage(one)) &&
                      dispatch(setTourCustomerCountInfo(DEFAULT_TOUR_CUSTOMER_COUNT))
                    }
                  >
                    <div>
                      <Radio
                        value={one.activityPackageId}
                        checked={current}
                        color="primary"
                        style={{ padding: '4px' }}
                      />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
                      <Typography variant="body1">{one.activityPackageTitle}</Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        style={{
                          maxHeight: packageCollapse[i] ? '72px' : undefined,
                          display: '-webkit-box',
                          WebkitLineClamp: packageCollapse[i] ? 3 : undefined,
                          WebkitBoxOrient: 'vertical',
                          overflow: 'hidden',
                        }}
                      >
                        <span
                          style={{ whiteSpace: 'pre-wrap' }}
                          dangerouslySetInnerHTML={{ __html: one.activityPackageDescription }}
                        />
                      </Typography>
                      <Typography
                        variant="body2"
                        style={{ color: BLUE, display: 'flex', justifyContent: 'flex-start' }}
                        onClick={() => {
                          const collapseList = [...packageCollapse];
                          collapseList[i] = !collapseList[i];
                          this.setState({ packageCollapse: collapseList });
                        }}
                      >
                        <FormattedMessage id={packageCollapse[i] ? 'tour.booking.seeMore' : 'tour.booking.hide'} />
                      </Typography>
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'flex-end',
                        flexDirection: 'column',
                        minWidth: '120px',
                      }}
                    >
                      <Typography variant="body1" color="secondary">
                        <FormattedNumber value={one.adultPrice} />
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </Typography>
                      {one.adultOriginPrice && (
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          style={{ textDecorationLine: 'line-through' }}
                        >
                          <FormattedNumber value={one.adultOriginPrice} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      )}
                    </div>
                  </ItemDiv>
                );
              })}
          </RadioGroup>
        </Box>
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="tour.booking.packageCount" />
            </Typography>
          </SectionHeader>
          {booking.activityPackage && (
            <>
              {booking.activityPackage.adultPrice !== -1 && (
                <TouristCountItem
                  price={booking.activityPackage.adultPrice}
                  touristName={
                    guestCategoryDescription
                      ? guestCategoryDescription.adultTitle
                      : intl.formatMessage({ id: 'tour.booking.adult' })
                  }
                  touristDescription={
                    guestCategoryDescription
                      ? guestCategoryDescription.adultDescription
                      : intl.formatMessage({ id: 'tour.booking.adultDescription' })
                  }
                  touristCount={booking.tourCustomerCountInfo.adultCount}
                  setCount={adultCount =>
                    dispatch(setTourCustomerCountInfo({ ...booking.tourCustomerCountInfo, adultCount }))
                  }
                />
              )}
              {booking.activityPackage.childrenPrice !== -1 && (
                <TouristCountItem
                  price={booking.activityPackage.childrenPrice}
                  touristName={
                    guestCategoryDescription
                      ? guestCategoryDescription.childTitle
                      : intl.formatMessage({ id: 'tour.booking.children' })
                  }
                  touristDescription={
                    guestCategoryDescription
                      ? guestCategoryDescription.childDescription
                      : intl.formatMessage({ id: 'tour.booking.childrenDescription' })
                  }
                  touristCount={booking.tourCustomerCountInfo.childrenCount}
                  setCount={childrenCount =>
                    dispatch(setTourCustomerCountInfo({ ...booking.tourCustomerCountInfo, childrenCount }))
                  }
                />
              )}
              {booking.activityPackage.babyPrice !== -1 && (
                <TouristCountItem
                  price={booking.activityPackage.babyPrice}
                  touristName={
                    guestCategoryDescription
                      ? guestCategoryDescription.babyTitle
                      : intl.formatMessage({ id: 'tour.booking.baby' })
                  }
                  touristDescription={
                    guestCategoryDescription
                      ? guestCategoryDescription.babyDescription
                      : intl.formatMessage({ id: 'tour.booking.babyDescription' })
                  }
                  touristCount={booking.tourCustomerCountInfo.babyCount}
                  setCount={babyCount =>
                    dispatch(setTourCustomerCountInfo({ ...booking.tourCustomerCountInfo, babyCount }))
                  }
                />
              )}
              {booking.activityPackage.infantPrice !== -1 && (
                <TouristCountItem
                  price={booking.activityPackage.infantPrice}
                  touristName={
                    guestCategoryDescription
                      ? guestCategoryDescription.infantTitle
                      : intl.formatMessage({ id: 'tour.booking.infant' })
                  }
                  touristDescription={
                    guestCategoryDescription
                      ? guestCategoryDescription.infantDescription
                      : intl.formatMessage({ id: 'tour.booking.infantDescription' })
                  }
                  touristCount={booking.tourCustomerCountInfo.infantCount}
                  setCount={infantCount =>
                    dispatch(setTourCustomerCountInfo({ ...booking.tourCustomerCountInfo, infantCount }))
                  }
                />
              )}
            </>
          )}
        </Box>
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="tour.booking.customerInfo" />
            </Typography>
          </SectionHeader>
          <ButtonBase
            onClick={() => this.setState({ isSelectContact: true })}
            style={{ marginBottom: '8px', borderRadius: '4px' }}
          >
            <AddContact>
              <IconContactList style={{ width: '24px', height: '24px' }} />
              <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
                <FormattedMessage id="booking.addFromContact" />
              </Typography>
            </AddContact>
          </ButtonBase>
          <TourCustomerInfoForm
            ticktock={ticktock}
            info={tourCustomerInfo}
            updateTourCustomerInfo={updateTourCustomerInfo}
            valid={tourCustomerInfoValidation}
          />

          <div>
            <FreeTextField
              text={booking.additionalRequest}
              optional
              header={intl.formatMessage({ id: 'tour.booking.note' })}
              placeholder={intl.formatMessage({ id: 'tour.booking.note' })}
              update={value => dispatch(setAdditionalRequest(value))}
            />
          </div>
        </Box>

        <div style={{ textAlign: 'center', margin: '32px' }}>
          <Button
            size="large"
            style={{ width: '260px' }}
            variant="contained"
            color="secondary"
            onClick={() => {
              // eslint-disable-next-line no-underscore-dangle
              this.props.continue_();
              this.setState({ ticktock: !ticktock });
            }}
          >
            <FormattedMessage id="continue" />
          </Button>
        </div>

        <SelectContactDialog
          show={isSelectContact}
          onClose={() => this.setState({ isSelectContact: false })}
          onSelect={customer => this.onSelectContact(customer)}
        />
      </div>
    );
  }
}

function mapState2Props(state: AppState) {
  return {
    booking: state.booking.tour,
  };
}

export default connect(mapState2Props)(injectIntl(TourBookingInfoBox));
