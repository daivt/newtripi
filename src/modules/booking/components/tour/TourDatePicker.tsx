import { Button, Fade, IconButton, Typography, Backdrop } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { DayPickerSingleDateController, isInclusivelyAfterDay } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { DARK_GREY, GREY, PRIMARY } from '../../../../colors';
import { DATE_FORMAT, some } from '../../../../constants';
import { ReactComponent as IconCalendarSvg } from '../../../../svg/calendar.svg';
import { renderMonthText } from '../../../common/utils';

const InputContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${GREY};
  background-color: white;
  width: 250px;
  cursor: pointer;
  z-index: 2100;
`;

interface ITourDatePickerProps {
  pickDate(date: Moment): void;
  date: Moment | null;
  tourAvailabilities: some;
}

const TourDatePicker: React.FunctionComponent<ITourDatePickerProps> = props => {
  const [show, setShow] = React.useState(false);
  const [date, setDate] = React.useState<Moment | null>(props.date);

  const isDayHighlighted = (day: Moment) => {
    return moment().format('L') === day.format('L') ? (
      <span style={{ fontWeight: 'bold', color: PRIMARY }}>{day.format('D')}</span>
    ) : (
      day.format('D')
    );
  };

  const availabilitiesDate = props.tourAvailabilities.availabilities.map((day: some) => day.departureDate);

  return (
    <div>
      <Backdrop
        open={show}
        style={{
          zIndex: 1001,
        }}
        onClick={() => setShow(false)}
      />
      <div
        style={{
          position: 'absolute',
          zIndex: 1002,
          background: 'white',
          padding: '8px',
          margin: '-8px',
          borderRadius: '4px',
        }}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <InputContainer onClick={() => setShow(true)}>
            <IconCalendarSvg style={{ margin: '0 10px' }} />
            <div
              style={{
                height: '24px',
                margin: '7px 1px',
                flex: 1,
              }}
            >
              {date ? date.format('L') : <FormattedMessage id="nothingYet" />}
            </div>
            <NavigateNextIcon fontSize="small" style={{ color: DARK_GREY }} />
          </InputContainer>
          {show && (
            <IconButton
              style={{ padding: '0 6px' }}
              onClick={() => {
                setDate(props.date);
                setShow(false);
              }}
            >
              <IconClose />
            </IconButton>
          )}
        </div>
        <Fade in={show} unmountOnExit>
          <div
            style={{
              position: show ? 'static' : 'absolute',
              marginTop: '12px',
            }}
          >
            <DayPickerSingleDateController
              hideKeyboardShortcutsPanel
              date={date}
              onDateChange={newDate => setDate(newDate)}
              focused={show}
              onFocusChange={() => {}}
              numberOfMonths={2}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              isDayBlocked={day =>
                !props.tourAvailabilities.isDaily && availabilitiesDate.indexOf(moment(day).format(DATE_FORMAT)) === -1
              }
              renderDayContents={day => isDayHighlighted(day)}
              renderMonthText={renderMonthText}
            />
            <div style={{ padding: '10px 12px 0 12px', display: 'flex', justifyContent: 'flex-end' }}>
              <Button
                size="large"
                variant="contained"
                color="secondary"
                onClick={() => {
                  setShow(false);
                  if (date) {
                    props.pickDate(date);
                  }
                }}
                disabled={!date}
              >
                <Typography variant="button">
                  <FormattedMessage id="done" />
                </Typography>
              </Button>
            </div>
          </div>
        </Fade>
      </div>
      <div style={{ height: '40px' }} />
    </div>
  );
};

export default TourDatePicker;
