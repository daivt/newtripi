import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { SectionHeader, Wrapper } from '../styles';
import TourBookingSideBox from './TourBookingSideBox';
import TourPayBox from './TourPayBox';
import TourPayBreadCrumbs from './TourPayBreadCrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.tour,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {
  showInfoBox: boolean;
}

class TourPayTablet extends PureComponent<Props> {
  state: State = { showInfoBox: false };

  render() {
    const { booking } = this.props;
    const { showInfoBox } = this.state;

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <div style={{ display: 'flex', overflow: 'hidden' }}>
            <div style={{ flex: 1 }}>
              <TourPayBreadCrumbs />
              <SectionHeader>
                <Typography variant="h5">
                  <FormattedMessage id="booking.pay" />
                </Typography>
              </SectionHeader>

              {!booking.tourData ? (
                <Wrapper>
                  <LoadingIcon
                    style={{
                      height: '300px',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  />
                </Wrapper>
              ) : (
                <TourPayBox />
              )}
            </div>
            <div
              style={{
                position: 'relative',
                direction: 'rtl',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {!showInfoBox && (
                <IconButton
                  style={{ margin: '10px', position: 'absolute' }}
                  onClick={() => this.setState({ showInfoBox: true })}
                >
                  <Open style={{ color: DARK_GREY }} />
                </IconButton>
              )}
              <div style={{ width: 1, flex: 1, display: 'flex' }}>
                <Slide in={showInfoBox} direction="left">
                  <div
                    style={{
                      backgroundColor: BACKGROUND,
                      padding: '22px 0 22px 10px',
                      marginTop: '-22px',
                      borderLeft: `1px solid ${GREY}`,
                      width: '420px',
                    }}
                  >
                    <div style={{ textAlign: 'end' }}>
                      <IconButton style={{ margin: '10px 0' }} onClick={() => this.setState({ showInfoBox: false })}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    </div>
                    <div style={{ direction: 'ltr' }}>
                      {booking.tourData && <TourBookingSideBox promotion={booking.promotion} />}
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(TourPayTablet);
