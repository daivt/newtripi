import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLACK_TEXT, GREEN, GREY, RED } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconCoinSvg } from '../../../../svg/coin.svg';
import TourBasicInfo from '../../../common/components/Tour/TourBasicInfo';
import { PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../../common/constants';
import { computeTourPaybleNumbers, getBonusPoint } from '../../utils';

const Wrapper = styled.div`
  flex-shrink: 0;
  width: 370px;
  margin-left: 32px;
`;

const ItemRow = styled.div`
  display: flex;
  justify-content: space-between;
  min-height: 40px;
  align-items: center;
`;

function mapState2Props(state: AppState) {
  return {
    booking: state.booking.tour,
  };
}

interface ITourBookingSideBoxProps extends ReturnType<typeof mapState2Props>, WrappedComponentProps {
  promotion?: some;
}

interface State {}

const TourBookingSideBox: React.FunctionComponent<ITourBookingSideBoxProps> = props => {
  const { booking, promotion, intl } = props;
  const { tourData } = booking;
  const guestCategoryDescription = tourData && tourData.guestCategoryDescription;
  const renderTourCustomerCountInfo = React.useCallback(() => {
    const tourCustomerCountInfo = [];

    if (booking.tourCustomerCountInfo.adultCount) {
      if (guestCategoryDescription) {
        tourCustomerCountInfo.push(
          `${booking.tourCustomerCountInfo.adultCount} ${guestCategoryDescription.adultTitle.toLowerCase()}`,
        );
      } else {
        tourCustomerCountInfo.push(
          intl.formatMessage(
            { id: 'tour.booking.adultCount' },
            { adultCount: booking.tourCustomerCountInfo.adultCount },
          ),
        );
      }
    }

    if (booking.tourCustomerCountInfo.childrenCount) {
      if (guestCategoryDescription) {
        tourCustomerCountInfo.push(
          `${booking.tourCustomerCountInfo.childrenCount} ${guestCategoryDescription.childTitle.toLowerCase()}`,
        );
      } else {
        tourCustomerCountInfo.push(
          intl.formatMessage(
            { id: 'tour.booking.childCount' },
            { childCount: booking.tourCustomerCountInfo.childrenCount },
          ),
        );
      }
    }

    if (booking.tourCustomerCountInfo.infantCount) {
      if (guestCategoryDescription) {
        tourCustomerCountInfo.push(
          `${booking.tourCustomerCountInfo.infantCount} ${guestCategoryDescription.infantTitle.toLowerCase()}`,
        );
      } else {
        tourCustomerCountInfo.push(
          intl.formatMessage(
            { id: 'tour.booking.infantCount' },
            { infantCount: booking.tourCustomerCountInfo.infantCount },
          ),
        );
      }
    }

    if (booking.tourCustomerCountInfo.babyCount) {
      if (guestCategoryDescription) {
        tourCustomerCountInfo.push(
          `${booking.tourCustomerCountInfo.babyCount} ${guestCategoryDescription.babyTitle.toLowerCase()}`,
        );
      } else {
        tourCustomerCountInfo.push(
          intl.formatMessage({ id: 'tour.booking.babyCount' }, { babyCount: booking.tourCustomerCountInfo.babyCount }),
        );
      }
    }
    return tourCustomerCountInfo.length ? tourCustomerCountInfo.join(', ') : '---';
  }, [booking, intl, guestCategoryDescription]);

  const payableNumbers = computeTourPaybleNumbers(booking);
  const finalPrice = payableNumbers.finalPrice + payableNumbers.paymentFee;
  if (!tourData) {
    return <div />;
  }
  return (
    <Wrapper>
      <div
        style={{
          background: '#fff',
          borderRadius: '4px',
          display: 'flex',
          flexDirection: 'column',
          padding: '16px',
          boxShadow: '0px 1px 3px rgba(0,0,0,0.2), 0px 2px 2px rgba(0,0,0,0.12), 0px 0px 2px rgba(0,0,0,0.14)',
        }}
      >
        <Typography variant="h5" style={{ paddingBottom: '12px' }}>
          {tourData.name}
        </Typography>
        <TourBasicInfo tour={tourData} />
        {!!tourData.tourServices.length && (
          <>
            <Typography variant="h6" style={{ padding: '12px 0' }}>
              <FormattedMessage id="tour.booking.serviceInclude" />
            </Typography>
            <div>
              {tourData.tourServices.map((one: some, index: number) => (
                <div key={index} style={{ display: 'flex', alignItems: 'center' }}>
                  <img style={{ width: '24px' }} src={one.iconUrl} alt="" />
                  <Typography variant="caption" style={{ paddingLeft: '8px' }}>
                    {one.description}
                  </Typography>
                </div>
              ))}
            </div>
          </>
        )}
        <div style={{ padding: '12px 0', display: 'flex', flexDirection: 'column' }}>
          <Typography variant="h6">
            <FormattedMessage id="tour.booking.bookingDetail" />
          </Typography>

          <ItemRow>
            <Typography variant="body2" style={{ flexShrink: 0 }}>
              <FormattedMessage id="tour.booking.yourSelect" />
            </Typography>
            <Typography variant="body2" color="textSecondary" style={{ paddingLeft: '10px' }}>
              {booking.activityPackage ? booking.activityPackage.activityPackageTitle : '---'}
            </Typography>
          </ItemRow>
          <div style={{ display: 'flex', minHeight: '40px', marginTop: '8px' }}>
            <Typography variant="body2" style={{ alignSelf: 'flex-start' }}>
              <FormattedMessage id="tour.booking.packageCount" />
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              style={{ flex: 1, paddingLeft: '16px', textAlign: 'end' }}
            >
              {renderTourCustomerCountInfo()}
            </Typography>
          </div>
        </div>

        <div style={{ padding: '12px 0', display: 'flex', flexDirection: 'column' }}>
          <Typography variant="h6">
            <FormattedMessage id="tour.booking.priceInfo" />
          </Typography>
          {booking.activityPackage && (
            <>
              <ItemRow>
                <Typography variant="body2">{booking.activityPackage.activityPackageTitle}</Typography>
                <Typography variant="body2" color="secondary">
                  <FormattedNumber value={booking.activityPackage.adultPrice} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </ItemRow>
              {!!booking.tourCustomerCountInfo.adultCount && (
                <ItemRow>
                  <Typography variant="body2">
                    {guestCategoryDescription ? (
                      guestCategoryDescription.adultTitle
                    ) : (
                      <FormattedMessage id="booking.adultPrice" />
                    )}
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <span style={{ color: BLACK_TEXT }}>
                      {booking.tourCustomerCountInfo.adultCount}
                      &nbsp;x&nbsp;
                    </span>
                    <FormattedNumber
                      value={booking.activityPackage.adultPrice !== -1 ? booking.activityPackage.adultPrice : 0}
                    />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </ItemRow>
              )}
              {!!booking.tourCustomerCountInfo.childrenCount && (
                <ItemRow>
                  <Typography variant="body2">
                    {guestCategoryDescription ? (
                      guestCategoryDescription.childTitle
                    ) : (
                      <FormattedMessage id="booking.childPrice" />
                    )}
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <span style={{ color: BLACK_TEXT }}>
                      {booking.tourCustomerCountInfo.childrenCount}
                      &nbsp;x&nbsp;
                    </span>
                    <FormattedNumber
                      value={booking.activityPackage.childrenPrice !== -1 ? booking.activityPackage.childrenPrice : 0}
                    />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </ItemRow>
              )}
              {!!booking.tourCustomerCountInfo.infantCount && (
                <ItemRow>
                  <Typography variant="body2">
                    {guestCategoryDescription ? (
                      guestCategoryDescription.infantTitle
                    ) : (
                      <FormattedMessage id="booking.infantPrice" />
                    )}
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <span style={{ color: BLACK_TEXT }}>
                      {booking.tourCustomerCountInfo.infantCount}
                      &nbsp;x&nbsp;
                    </span>
                    <FormattedNumber
                      value={booking.activityPackage.infantPrice !== -1 ? booking.activityPackage.infantPrice : 0}
                    />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </ItemRow>
              )}
              {!!booking.tourCustomerCountInfo.babyCount && (
                <ItemRow>
                  <Typography variant="body2">
                    {guestCategoryDescription ? (
                      guestCategoryDescription.babyTitle
                    ) : (
                      <FormattedMessage id="booking.babyPrice" />
                    )}
                  </Typography>
                  <Typography variant="body2" color="secondary">
                    <span style={{ color: BLACK_TEXT }}>
                      {booking.tourCustomerCountInfo.babyCount}
                      &nbsp;x&nbsp;
                    </span>
                    <FormattedNumber
                      value={booking.activityPackage.babyPrice !== -1 ? booking.activityPackage.babyPrice : 0}
                    />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </ItemRow>
              )}

              {promotion && promotion.price && (
                <ItemRow>
                  {booking.selectedPaymentMethod && booking.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE ? (
                    <FormattedMessage id="booking.discountCodeNotAcceptPaymentMethodHolding" />
                  ) : (
                    <>
                      <Typography variant="body2">{promotion.promotionDescription}</Typography>
                      <div style={{ display: 'flex' }}>
                        <Typography variant="body2" style={{ color: GREEN }}>
                          <FormattedNumber value={0 - payableNumbers.discountAmount} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      </div>
                    </>
                  )}
                </ItemRow>
              )}

              {booking.usePointPayment &&
                booking.selectedPaymentMethod &&
                booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
                !!booking.pointUsing && (
                  <ItemRow>
                    <Typography variant="body2">
                      <FormattedMessage id="booking.pointPayment" />
                    </Typography>

                    <Typography variant="body2" color="primary">
                      <FormattedNumber value={-payableNumbers.pointToAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </ItemRow>
                )}
              {!!payableNumbers.paymentFee && (
                <ItemRow>
                  {payableNumbers.paymentFee > 0 ? (
                    <FormattedMessage id="booking.paymentFixedFeeSide" />
                  ) : (
                    <FormattedMessage
                      id="booking.discountPaymentMethod"
                      values={{
                        methodName: booking.selectedPaymentMethod ? booking.selectedPaymentMethod.name : '',
                      }}
                    />
                  )}
                  <div style={{ display: 'flex' }}>
                    <Typography variant="body2" color={payableNumbers.paymentFee > 0 ? 'secondary' : 'primary'}>
                      <FormattedNumber value={payableNumbers.paymentFee} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </ItemRow>
              )}
              <ItemRow>
                <Typography variant="subtitle2">
                  <FormattedMessage id="booking.totalPayable" />
                </Typography>
                <Typography variant="h6" style={{ color: RED }}>
                  <FormattedNumber value={finalPrice > 0 ? finalPrice : 0} maximumFractionDigits={0} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </ItemRow>
            </>
          )}
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              color: `${GREY}`,
            }}
          >
            <ItemRow>
              <Typography color="textSecondary" variant="caption">
                <FormattedMessage id="booking.includeTaxesAndFees" />
              </Typography>
            </ItemRow>
          </div>
          <div>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
              <IconCoinSvg style={{ marginRight: '10px' }} />
              <Typography variant="body2" style={{ color: GREEN }}>
                <FormattedNumber
                  value={promotion && payableNumbers.discountAmount ? promotion.bonusPoint : getBonusPoint(booking)}
                />
                &nbsp;
                <FormattedMessage id="booking.point" />
              </Typography>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default connect(mapState2Props)(injectIntl(TourBookingSideBox));
