import { Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { NewTabLink } from '../../../common/components/NewTabLink';
import DiscountCodeBox from '../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../common/components/Payment/PaymentMethodsBox';
import { setSelectedPaymentMethod } from '../../redux/tourBookingReducer';
import { computeTourPaybleNumbers } from '../../utils';
import { Box, SectionHeader } from '../styles';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.tour,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

class TourPayBox extends PureComponent<Props> {
  render() {
    const { booking, dispatch } = this.props;
    const payableNumbers = computeTourPaybleNumbers(booking);
    const priceAfter = payableNumbers.finalPrice;

    return (
      <>
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.discountCode" />
            </Typography>
          </SectionHeader>
          <DiscountCodeBox moduleType="tour" />
        </Box>
        {booking.paymentMethods && booking.selectedPaymentMethod && (
          <Box>
            <SectionHeader>
              <Typography variant="h6">
                <FormattedMessage id="booking.paymentMethod" />
              </Typography>
            </SectionHeader>
            <PaymentMethodsBox
              paymentMethods={booking.paymentMethods}
              total={payableNumbers.finalPrice}
              setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
              selectedMethod={booking.selectedPaymentMethod}
              priceAfter={priceAfter}
              promotionCode={booking.promotionCode}
              moduleType="tour"
            />
          </Box>
        )}
        <div
          style={{
            padding: '16px 16px 32px 16px',
            color: BLUE,
          }}
        >
          <NewTabLink href={ROUTES.terms}>
            <Typography variant="body2">
              <FormattedMessage id="booking.seePaymentDetail" />
            </Typography>
          </NewTabLink>
        </div>
      </>
    );
  }
}

export default connect(mapState2Props)(TourPayBox);
