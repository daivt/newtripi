import { IconButton, Typography } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircle from '@material-ui/icons/RemoveCircleOutline';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { SECONDARY } from '../../../../colors';

interface Props {
  price: number;
  touristCount: number;
  touristName: string;
  touristDescription: string;
  setCount(val: number): void;
}

export const TouristCountItem: React.FC<Props> = props => {
  const { price, touristCount, touristName, touristDescription, setCount } = props;

  return (
    <div style={{ display: 'flex', justifyContent: 'space-between', padding: '8px 0' }}>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography variant="body2">
          {touristName}
          &nbsp;-&nbsp;
          <span style={{ color: SECONDARY }}>
            <FormattedNumber value={price} />
            &nbsp;
            <FormattedMessage id="currency" />
          </span>
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {touristDescription}
        </Typography>
      </div>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          minWidth: '100px',
        }}
      >
        <IconButton size="small" disabled={!touristCount} onClick={() => setCount(touristCount ? touristCount - 1 : 0)}>
          <RemoveCircle
            style={{
              fontSize: '18px',
            }}
            color={touristCount ? 'primary' : 'disabled'}
          />
        </IconButton>
        <Typography variant="body2">{touristCount}</Typography>
        <IconButton size="small" onClick={() => setCount(touristCount + 1)}>
          <AddCircle
            style={{
              fontSize: '18px',
            }}
            color="primary"
          />
        </IconButton>
      </div>
    </div>
  );
};
