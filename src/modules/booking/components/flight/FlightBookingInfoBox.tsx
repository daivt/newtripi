import { ButtonBase, Checkbox, FormControlLabel, MenuItem, Select, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE, PURPLE, RED, ORANGE } from '../../../../colors';
import { some, MY_TOUR } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import { ReactComponent as IconIdeaSvg } from '../../../../svg/idea.svg';
import { ReactComponent as IconWarningSvg } from '../../../../svg/warning.svg';
import { BootstrapInput, Row } from '../../../common/components/elements';
import LoadingButton from '../../../common/components/LoadingButton';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import SelectCountryDialog from '../../../common/components/SelectCountryDialog';
import {
  ContactInfo,
  ContactInfoValidation,
  Gender,
  TravellersInfo,
  TravellersInfoValidation,
  validTravellerInfoValidation,
} from '../../../common/models';
import { PassportInfoType } from '../../constants';
import {
  setBooker,
  setBuyInsurance,
  setInboundExtraBaggages,
  setOutboundExtraBaggages,
} from '../../redux/flightBookingReducer';
import {
  transformTravellerInfo,
  validateBabyInfo,
  validateChildInfo,
  validateContactInfo,
  validateTravellerInfo,
} from '../../utils';
import AdultInfoForm from '../AdultInfoForm';
import BabyInfoForm from '../BabyInfoForm';
import ChildInfoForm from '../ChildInfoForm';
import ContactInfoForm from '../ContactInfoForm';
import { AddContact, Box, SectionHeader } from '../styles';
import { DirectionColumn, FieldHeader, TravellerColumn } from './FlightBookingInfo.styles';

const CONTACT_TYPE = {
  CUSTOMER_INFO: 'CUSTOMER_INFO',
  CONTACT_INFO: 'CONTACT_INFO',
  ADULT_INFO: 'ADULT_INFO',
  BABY_INFO: 'BABY_INFO',
  CHILD_INFO: 'CHILD_INFO',
};

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.flight,
    userData: state.account.userData,
  };
};

interface IFlightBookingInfoBoxProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  contactInfo: ContactInfo;
  travellersInfo: TravellersInfo;
  contactInfoValidation: ContactInfoValidation;
  travellersInfoValidation: TravellersInfoValidation;
  updateContactInfo(info: ContactInfo): void;
  updateTravellersInfo(info: TravellersInfo): void;
  updateContactInfoValidation(validation: ContactInfoValidation): void;
  updateTravellersInfoValidation(validation: TravellersInfoValidation): void;
  continue_(): void;
}

interface State {
  isSelectContact: boolean;
  selectContactIndex: number;
  selectContactType?: string;
  isSelectCountry: boolean;
  selectCountryIndex: number;
  passportInfoType: PassportInfoType;
  ticktock: boolean;
}

class FlightBookingInfoBox extends React.PureComponent<IFlightBookingInfoBoxProps, State> {
  state: State = {
    isSelectContact: false,
    selectContactIndex: 0,
    isSelectCountry: false,
    selectCountryIndex: 0,
    passportInfoType: 'passportCountry',
    ticktock: false,
  };

  componentDidMount() {
    const { booking, updateContactInfo, updateContactInfoValidation, userData } = this.props;
    if (!userData) {
      return;
    }

    if (!booking.contactInfo) {
      const familyName = userData.name.split(' ')[0];
      const givenName = userData.name.split(familyName)[1];
      const info = {
        familyName,
        givenName,
        gender: (userData.gender || 'm').toLowerCase() as Gender,
        address: userData.address || '',
        email: userData.emailInfo || '',
        telephone: userData.phoneInfo,
      };

      updateContactInfo(info);
      updateContactInfoValidation(validateContactInfo(info));
    }
  }

  onSelectContact(contact: some) {
    const { selectContactType, selectContactIndex } = this.state;
    const {
      dispatch,
      booking,
      travellersInfo,
      travellersInfoValidation,
      updateTravellersInfo,
      updateTravellersInfoValidation,
      updateContactInfo,
      updateContactInfoValidation,
    } = this.props;

    if (selectContactType === CONTACT_TYPE.CUSTOMER_INFO) {
      this.setState({
        isSelectContact: false,
      });
      dispatch(setBooker({ ...contact, email: contact.email || '' }));
      return;
    }

    const outboundTicket = booking.outbound.ticket;
    const inboundTicket = booking.inbound.ticket ? booking.inbound.ticket : undefined;

    const needPassport = outboundTicket ? outboundTicket.outbound.ticketdetail.needPassport : false;

    let arrivalTime = outboundTicket ? outboundTicket.outbound.arrivalTime : 0;
    if (inboundTicket) {
      arrivalTime = inboundTicket.outbound.arrivalTime;
    }

    if (selectContactType === CONTACT_TYPE.ADULT_INFO) {
      const adultInfo = transformTravellerInfo(contact);

      const travellers = {
        ...travellersInfo,
        adults: [
          ...travellersInfo.adults.slice(0, selectContactIndex),
          adultInfo,
          ...travellersInfo.adults.slice(selectContactIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        adults: [
          ...travellersInfoValidation.adults.slice(0, selectContactIndex),
          validateTravellerInfo(adultInfo, needPassport, arrivalTime),
          ...travellersInfoValidation.adults.slice(selectContactIndex + 1),
        ],
      });
      this.setState({ isSelectContact: false });
      return;
    }

    if (selectContactType === CONTACT_TYPE.CHILD_INFO) {
      const childInfo = transformTravellerInfo(contact);

      const travellers = {
        ...travellersInfo,
        children: [
          ...travellersInfo.children.slice(0, selectContactIndex),
          childInfo,
          ...travellersInfo.children.slice(selectContactIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        children: [
          ...travellersInfoValidation.children.slice(0, selectContactIndex),
          validateChildInfo(childInfo, needPassport, arrivalTime),
          ...travellersInfoValidation.children.slice(selectContactIndex + 1),
        ],
      });
      this.setState({ isSelectContact: false });
      return;
    }

    if (selectContactType === CONTACT_TYPE.BABY_INFO) {
      const babyInfo = transformTravellerInfo(contact);

      const travellers = {
        ...travellersInfo,
        babies: [
          ...travellersInfo.babies.slice(0, selectContactIndex),
          babyInfo,
          ...travellersInfo.babies.slice(selectContactIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        babies: [
          ...travellersInfoValidation.babies.slice(0, selectContactIndex),
          validateBabyInfo(babyInfo, needPassport, arrivalTime),
          ...travellersInfoValidation.babies.slice(selectContactIndex + 1),
        ],
      });
      this.setState({ isSelectContact: false });
      return;
    }

    if (selectContactType === CONTACT_TYPE.CONTACT_INFO) {
      const info = {
        familyName: contact.lastName,
        givenName: contact.firstName,
        gender: (contact.gender as string).toLowerCase() as Gender,
        address: contact.address || '',
        email: contact.email || '',
        telephone: contact.phone,
      };

      updateContactInfo(info);

      updateContactInfoValidation(validateContactInfo(info));
      this.setState({ isSelectContact: false });
    }
  }

  onSelectCountry(country: some) {
    const { selectContactType, selectCountryIndex, passportInfoType } = this.state;
    const {
      booking,
      travellersInfo,
      updateTravellersInfo,
      travellersInfoValidation,
      updateTravellersInfoValidation,
    } = this.props;

    const outboundTicket = booking.outbound.ticket;
    const inboundTicket = booking.inbound.ticket ? booking.inbound.ticket : undefined;

    let arrivalTime = outboundTicket ? outboundTicket.outbound.arrivalTime : 0;
    if (inboundTicket) {
      arrivalTime = inboundTicket.outbound.arrivalTime;
    }

    if (selectContactType === CONTACT_TYPE.ADULT_INFO) {
      const info = travellersInfo.adults[selectCountryIndex];
      const adultInfo = {
        ...info,
        passportInfo: {
          ...info.passportInfo,
          passportCountry: passportInfoType === 'passportCountry' ? country : info.passportInfo.passportCountry,
          nationalityCountry:
            passportInfoType === 'nationalityCountry' ? country : info.passportInfo.nationalityCountry,
        },
      };

      const travellers = {
        ...travellersInfo,
        adults: [
          ...travellersInfo.adults.slice(0, selectCountryIndex),
          adultInfo,
          ...travellersInfo.adults.slice(selectCountryIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        adults: [
          ...travellersInfoValidation.adults.slice(0, selectCountryIndex),
          validateTravellerInfo(adultInfo, true, arrivalTime),
          ...travellersInfoValidation.adults.slice(selectCountryIndex + 1),
        ],
      });
      this.setState({ isSelectCountry: false });
      return;
    }

    if (selectContactType === CONTACT_TYPE.CHILD_INFO) {
      const info = travellersInfo.children[selectCountryIndex];
      const childInfo = {
        ...info,
        passportInfo: {
          ...info.passportInfo,
          passportCountry: passportInfoType === 'passportCountry' ? country : info.passportInfo.passportCountry,
          nationalityCountry:
            passportInfoType === 'nationalityCountry' ? country : info.passportInfo.nationalityCountry,
        },
      };

      const travellers = {
        ...travellersInfo,
        children: [
          ...travellersInfo.children.slice(0, selectCountryIndex),
          childInfo,
          ...travellersInfo.children.slice(selectCountryIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        children: [
          ...travellersInfoValidation.children.slice(0, selectCountryIndex),
          validateChildInfo(childInfo, true, arrivalTime),
          ...travellersInfoValidation.children.slice(selectCountryIndex + 1),
        ],
      });
      this.setState({ isSelectCountry: false });
      return;
    }

    if (selectContactType === CONTACT_TYPE.BABY_INFO) {
      const info = travellersInfo.babies[selectCountryIndex];
      const babyInfo = {
        ...info,
        passportInfo: {
          ...info.passportInfo,
          passportCountry: passportInfoType === 'passportCountry' ? country : info.passportInfo.passportCountry,
          nationalityCountry:
            passportInfoType === 'nationalityCountry' ? country : info.passportInfo.nationalityCountry,
        },
      };

      const travellers = {
        ...travellersInfo,
        babies: [
          ...travellersInfo.babies.slice(0, selectCountryIndex),
          babyInfo,
          ...travellersInfo.babies.slice(selectCountryIndex + 1),
        ],
      };

      updateTravellersInfo(travellers);
      updateTravellersInfoValidation({
        ...travellersInfoValidation,
        babies: [
          ...travellersInfoValidation.babies.slice(0, selectCountryIndex),
          validateBabyInfo(babyInfo, true, arrivalTime),
          ...travellersInfoValidation.babies.slice(selectCountryIndex + 1),
        ],
      });
      this.setState({ isSelectCountry: false });
    }
  }

  renderSelectContact(contactType: string, index: number = 0) {
    if (MY_TOUR) {
      return null;
    }
    return (
      <ButtonBase
        style={{ borderRadius: '4px' }}
        onClick={() =>
          this.setState({
            isSelectContact: true,
            selectContactType: contactType,
            selectContactIndex: index,
          })
        }
      >
        <AddContact>
          <IconContactList style={{ width: '24px', height: '24px' }} />
          <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
            <FormattedMessage id="booking.addFromContact" />
          </Typography>
        </AddContact>
      </ButtonBase>
    );
  }

  render() {
    const {
      booking,
      travellersInfo,
      contactInfo,
      dispatch,
      travellersInfoValidation,
      contactInfoValidation,
      updateContactInfo,
      updateContactInfoValidation,
      updateTravellersInfo,
      updateTravellersInfoValidation,
    } = this.props;
    const { adults } = travellersInfo;
    const { children } = travellersInfo;
    const { babies } = travellersInfo;

    const { isSelectContact, isSelectCountry, ticktock } = this.state;
    const { booker } = booking;

    const needPassport = booking.outbound.ticket ? booking.outbound.ticket.outbound.ticketdetail.needPassport : false;

    return (
      <div style={{ flex: 1 }}>
        {!MY_TOUR && (
          <Box>
            <SectionHeader>
              <Typography variant="h6">
                <FormattedMessage id="booking.bookerInfo" />
              </Typography>
            </SectionHeader>
            {this.renderSelectContact(CONTACT_TYPE.CUSTOMER_INFO)}
            <div style={{ paddingTop: '16px' }}>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2" style={{ minWidth: '200px' }}>
                  <FormattedMessage id="booking.name" />
                </Typography>
                <Typography variant="body2">{booker ? `${booker.lastName} ${booker.firstName}` : ''}</Typography>
              </div>

              <div style={{ display: 'flex', alignItems: 'center', padding: '8px 0' }}>
                <Typography variant="body2" style={{ minWidth: '200px' }}>
                  <FormattedMessage id="telephone" />
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  {booker ? booker.phone : ''}
                </Typography>
              </div>

              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2" style={{ minWidth: '200px' }}>
                  <FormattedMessage id="email" />
                </Typography>
                {booker && (
                  <Typography variant="body2">
                    <a style={{ color: BLUE, textDecoration: 'none' }} href={`mailto:${booker.email}`}>
                      {booker.email}
                    </a>
                  </Typography>
                )}
              </div>
            </div>
          </Box>
        )}
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.travellersInfo" />
            </Typography>
          </SectionHeader>
          <div
            style={{
              display: 'flex',
              color: RED,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <IconWarningSvg />
            <Typography variant="body2">
              <FormattedMessage id="booking.inputInfoInstructions" />
            </Typography>
          </div>
          {adults.map((one, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={i} style={{ marginTop: '12px' }}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${i + 1}. `}
                  <FormattedMessage id="booking.adult" values={{ num: adults.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.ADULT_INFO, i)}
              </FieldHeader>
              <AdultInfoForm
                ticktock={ticktock}
                needPassport={needPassport}
                validation={travellersInfoValidation.adults[i] || validTravellerInfoValidation}
                info={one}
                update={(info, validation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    adults: [...travellersInfo.adults.slice(0, i), info, ...travellersInfo.adults.slice(i + 1)],
                  });
                  if (validation) {
                    updateTravellersInfoValidation({
                      ...travellersInfoValidation,
                      adults: [
                        ...travellersInfoValidation.adults.slice(0, i),
                        validation,
                        ...travellersInfoValidation.adults.slice(i + 1),
                      ],
                    });
                  }
                }}
                showCountryDialog={passportInfoType =>
                  this.setState({
                    passportInfoType,
                    isSelectCountry: true,
                    selectCountryIndex: i,
                    selectContactType: CONTACT_TYPE.ADULT_INFO,
                  })
                }
              />
            </div>
          ))}
          {children.map((one, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={i} style={{ marginTop: '12px' }}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${adults.length + i + 1}. `}
                  <FormattedMessage id="booking.child" values={{ num: children.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.CHILD_INFO, i)}
              </FieldHeader>
              <ChildInfoForm
                ticktock={ticktock}
                needPassport={needPassport}
                validation={travellersInfoValidation.children[i] || validTravellerInfoValidation}
                info={one}
                update={(info, validation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    children: [...travellersInfo.children.slice(0, i), info, ...travellersInfo.children.slice(i + 1)],
                  });
                  if (validation) {
                    updateTravellersInfoValidation({
                      ...travellersInfoValidation,
                      children: [
                        ...travellersInfoValidation.children.slice(0, i),
                        validation,
                        ...travellersInfoValidation.children.slice(i + 1),
                      ],
                    });
                  }
                }}
                showCountryDialog={passportInfoType =>
                  this.setState({
                    passportInfoType,
                    isSelectCountry: true,
                    selectCountryIndex: i,
                    selectContactType: CONTACT_TYPE.CHILD_INFO,
                  })
                }
              />
            </div>
          ))}
          {babies.map((one, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={i} style={{ marginTop: '12px' }}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${adults.length + children.length + i + 1}. `}
                  <FormattedMessage id="booking.baby" values={{ num: babies.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.BABY_INFO, i)}
              </FieldHeader>
              <BabyInfoForm
                ticktock={ticktock}
                needPassport={needPassport}
                validation={travellersInfoValidation.babies[i] || validTravellerInfoValidation}
                info={one}
                update={(info, validation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    babies: [...travellersInfo.babies.slice(0, i), info, ...travellersInfo.babies.slice(i + 1)],
                  });
                  if (validation) {
                    updateTravellersInfoValidation({
                      ...travellersInfoValidation,
                      babies: [
                        ...travellersInfoValidation.babies.slice(0, i),
                        validation,
                        ...travellersInfoValidation.babies.slice(i + 1),
                      ],
                    });
                  }
                }}
                showCountryDialog={passportInfoType =>
                  this.setState({
                    passportInfoType,
                    isSelectCountry: true,
                    selectCountryIndex: i,
                    selectContactType: CONTACT_TYPE.BABY_INFO,
                  })
                }
              />
            </div>
          ))}
        </Box>
        <Box>
          <SectionHeader>
            <Typography variant="h6" style={{ paddingRight: '24px' }}>
              <FormattedMessage id="contactInfo" />
            </Typography>
            {this.renderSelectContact(CONTACT_TYPE.CONTACT_INFO)}
          </SectionHeader>
          <ContactInfoForm
            ticktock={ticktock}
            validation={contactInfoValidation}
            info={contactInfo}
            update={(info, newContactInfoValidation) => {
              updateContactInfo(info);
              if (newContactInfoValidation) {
                updateContactInfoValidation(newContactInfoValidation);
              }
            }}
          />
        </Box>

        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.buyMoreCheckin" />
            </Typography>
          </SectionHeader>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              color: PURPLE,
              marginBottom: '12px',
            }}
          >
            <IconIdeaSvg />
            &nbsp;
            <FormattedMessage id="booking.buyMoreCheckinAd" />
          </div>
          {adults.map(
            (one, i) =>
              booking.outbound.ticket && (
                // eslint-disable-next-line react/no-array-index-key
                <div key={i} style={{ marginTop: i !== 0 ? '16px' : 0 }}>
                  <Row>
                    <TravellerColumn />
                    <DirectionColumn>
                      <FormattedMessage id="outbound" />
                    </DirectionColumn>
                    {booking.inbound.ticket && (
                      <DirectionColumn>
                        <FormattedMessage id="inbound" />
                      </DirectionColumn>
                    )}
                  </Row>
                  <Row>
                    <TravellerColumn>
                      <Typography variant="subtitle2">
                        {`${i + 1}. `}
                        <FormattedMessage id="booking.adult" values={{ num: adults.length === 1 ? '' : `${i + 1}` }} />
                      </Typography>
                    </TravellerColumn>
                    <DirectionColumn>
                      {booking.outbound.ticket.outbound.baggages &&
                      booking.outbound.ticket.outbound.baggages[0] &&
                      booking.outbound.extraBaggages ? (
                        <Select
                          value={booking.outbound.extraBaggages[i]}
                          onChange={e => {
                            if (booking.outbound.extraBaggages) {
                              const extraBaggage = booking.outbound.extraBaggages.slice();
                              extraBaggage[i] = e.target.value as number;
                              dispatch(setOutboundExtraBaggages(extraBaggage));
                            }
                          }}
                          fullWidth
                          input={<BootstrapInput />}
                        >
                          {booking.outbound.ticket.outbound.baggages.map((oneBaggage: any, bi: number) => (
                            <MenuItem key={oneBaggage.id} value={bi}>
                              <Typography variant="body2">
                                {`${oneBaggage.name} - `}
                                <FormattedNumber value={oneBaggage.price} />
                                &nbsp;
                                <FormattedMessage id="currency" />
                              </Typography>
                            </MenuItem>
                          ))}
                        </Select>
                      ) : (
                        <Typography variant="body2" color="textSecondary">
                          <FormattedMessage id="booking.noSupportBuyBaggage" />
                        </Typography>
                      )}
                    </DirectionColumn>
                    {booking.inbound.ticket && (
                      <DirectionColumn>
                        {booking.inbound.ticket.outbound.baggages &&
                        booking.inbound.ticket.outbound.baggages[0] &&
                        booking.inbound.extraBaggages ? (
                          <Select
                            value={booking.inbound.extraBaggages[i]}
                            onChange={e => {
                              if (booking.inbound.extraBaggages) {
                                const extraBaggage = booking.inbound.extraBaggages.slice();
                                extraBaggage[i] = e.target.value as number;
                                dispatch(setInboundExtraBaggages(extraBaggage));
                              }
                            }}
                            fullWidth
                            input={<BootstrapInput />}
                          >
                            {booking.inbound.ticket.outbound.baggages.map((oneBaggage: any, bi: number) => (
                              <MenuItem key={oneBaggage.id} value={bi}>
                                <Typography variant="body2">
                                  {`${oneBaggage.name} - `}
                                  <FormattedNumber value={oneBaggage.price} />
                                  &nbsp;
                                  <FormattedMessage id="currency" />
                                </Typography>
                              </MenuItem>
                            ))}
                          </Select>
                        ) : (
                          <Typography variant="body2" color="textSecondary">
                            <FormattedMessage id="booking.noSupportBuyBaggage" />
                          </Typography>
                        )}
                      </DirectionColumn>
                    )}
                  </Row>
                </div>
              ),
          )}
          {children.map(
            (one, i) =>
              booking.outbound.ticket && (
                // eslint-disable-next-line react/no-array-index-key
                <div key={i} style={{ marginTop: '16px' }}>
                  <Row>
                    <TravellerColumn />
                    <DirectionColumn>
                      <FormattedMessage id="outbound" />
                    </DirectionColumn>
                    {booking.inbound.ticket && (
                      <DirectionColumn>
                        <FormattedMessage id="inbound" />
                      </DirectionColumn>
                    )}
                  </Row>
                  <Row>
                    <TravellerColumn>
                      <Typography variant="subtitle2">
                        {`${adults.length + i + 1}. `}
                        <FormattedMessage
                          id="booking.child"
                          values={{ num: children.length === 1 ? '' : `${i + 1}` }}
                        />
                      </Typography>
                    </TravellerColumn>
                    <DirectionColumn>
                      {booking.outbound.ticket.outbound.baggages &&
                      booking.outbound.ticket.outbound.baggages[0] &&
                      booking.outbound.extraBaggages ? (
                        <Select
                          value={booking.outbound.extraBaggages[adults.length + i]}
                          onChange={e => {
                            if (booking.outbound.extraBaggages) {
                              const extraBaggage = booking.outbound.extraBaggages.slice();
                              extraBaggage[adults.length + i] = e.target.value as number;
                              dispatch(setOutboundExtraBaggages(extraBaggage));
                            }
                          }}
                          fullWidth
                          input={<BootstrapInput />}
                        >
                          {booking.outbound.ticket.outbound.baggages.map((oneBaggage: any, bi: number) => (
                            <MenuItem key={oneBaggage.id} value={bi}>
                              <Typography variant="body2">
                                {`${oneBaggage.name} - `}
                                <FormattedNumber value={oneBaggage.price} />
                                &nbsp;
                                <FormattedMessage id="currency" />
                              </Typography>
                            </MenuItem>
                          ))}
                        </Select>
                      ) : (
                        <Typography variant="body2" color="textSecondary">
                          <FormattedMessage id="booking.noSupportBuyBaggage" />
                        </Typography>
                      )}
                    </DirectionColumn>
                    {booking.inbound.ticket && (
                      <DirectionColumn>
                        {booking.inbound.ticket.outbound.baggages &&
                        booking.inbound.ticket.outbound.baggages[0] &&
                        booking.inbound.extraBaggages ? (
                          <Select
                            value={booking.inbound.extraBaggages[adults.length + i]}
                            onChange={e => {
                              if (booking.inbound.extraBaggages) {
                                const extraBaggage = booking.inbound.extraBaggages.slice();
                                extraBaggage[adults.length + i] = e.target.value as number;
                                dispatch(setInboundExtraBaggages(extraBaggage));
                              }
                            }}
                            fullWidth
                            input={<BootstrapInput />}
                          >
                            {booking.inbound.ticket.outbound.baggages.map((oneBaggage: any, bi: number) => (
                              <MenuItem key={oneBaggage.id} value={bi}>
                                <Typography variant="body2">
                                  {`${oneBaggage.name} - `}
                                  <FormattedNumber value={oneBaggage.price} />
                                  &nbsp;
                                  <FormattedMessage id="currency" />
                                </Typography>
                              </MenuItem>
                            ))}
                          </Select>
                        ) : (
                          <Typography variant="body2" color="textSecondary">
                            <FormattedMessage id="booking.noSupportBuyBaggage" />
                          </Typography>
                        )}
                      </DirectionColumn>
                    )}
                  </Row>
                </div>
              ),
          )}
        </Box>

        <Box>
          <SectionHeader style={{ justifyContent: 'space-between' }}>
            <Typography variant="h6">
              <FormattedMessage id="booking.travelInsurance" />
            </Typography>

            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-end',
                flex: 1,
                paddingRight: 20,
              }}
            >
              {booking.insurancePackage && (
                <img
                  alt=""
                  src={booking.insurancePackage.image}
                  style={{ height: 36, width: 80, objectFit: 'cover' }}
                />
              )}
              <Typography variant="caption">{booking?.insurancePackage?.title}</Typography>
            </div>
          </SectionHeader>

          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <FormControlLabel
              label={
                <Typography variant="body2" style={{ fontWeight: 'bold' }}>
                  <FormattedMessage id="booking.buyInsurance" />
                </Typography>
              }
              disabled={!booking.insurancePackage}
              style={{ paddingBottom: '8px' }}
              labelPlacement="end"
              value={booking.buyInsurance}
              onChange={(e, value) => dispatch(setBuyInsurance(value))}
              control={<Checkbox color="primary" checked={booking.buyInsurance} />}
            />
            {booking.insurancePackage && (
              <Typography variant="body1" style={{ marginRight: '20px', color: ORANGE }}>
                <FormattedMessage
                  id="booking.chubbInsurance.price"
                  values={{
                    price: <FormattedNumber value={booking.insurancePackage.price} />,
                  }}
                />
              </Typography>
            )}
          </div>

          {booking.insurancePackage ? (
            <Typography variant="caption">
              <span dangerouslySetInnerHTML={{ __html: booking.insurancePackage.introduction }} />
            </Typography>
          ) : (
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="booking.noSupportBuyInsurance" />
            </Typography>
          )}
        </Box>
        <div style={{ width: '260px', margin: '30px auto' }}>
          <LoadingButton
            color="secondary"
            variant="contained"
            size="large"
            fullWidth
            loading={booking.validatingInsurance}
            onClick={() => {
              this.setState({ ticktock: !ticktock });
              // eslint-disable-next-line no-underscore-dangle
              this.props.continue_();
            }}
          >
            <Typography variant="button">
              <FormattedMessage id="continue" />
            </Typography>
          </LoadingButton>
        </div>
        <SelectContactDialog
          show={isSelectContact}
          onClose={() => this.setState({ isSelectContact: false })}
          onSelect={customer => this.onSelectContact(customer)}
        />
        <SelectCountryDialog
          show={isSelectCountry}
          onClose={() => this.setState({ isSelectCountry: false })}
          onSelect={country => this.onSelectCountry(country)}
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(FlightBookingInfoBox);
