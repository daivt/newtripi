import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { ContactInfo, ContactInfoValidation, TravellersInfo, TravellersInfoValidation } from '../../../common/models';
import { SectionHeader, Wrapper } from '../styles';
import FlightBookingInfoBox from './FlightBookingInfoBox';
import FlightBookingInfoBreadcrumbs from './FlightBookingInfoBreadcrumbs';
import FlightInfoBox from './FlightInfoBox';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.flight,
  };
};

export interface IFlightBookingInfoDesktopProps extends ReturnType<typeof mapState2Props> {
  contactInfo: ContactInfo;
  travellersInfo: TravellersInfo;
  contactInfoValidation: ContactInfoValidation;
  travellersInfoValidation: TravellersInfoValidation;
  updateContactInfo(info: ContactInfo): void;
  updateTravellersInfo(info: TravellersInfo): void;
  updateContactInfoValidation(validation: ContactInfoValidation): void;
  updateTravellersInfoValidation(validation: TravellersInfoValidation): void;
  continue_(): void;
}

interface State {
  seeDetails: boolean;
}

class FlightBookingInfoDesktop extends React.PureComponent<IFlightBookingInfoDesktopProps, State> {
  state: State = {
    seeDetails: false,
  };

  public render() {
    const {
      booking,
      travellersInfo,
      contactInfo,
      travellersInfoValidation,
      contactInfoValidation,
      updateContactInfo,
      updateContactInfoValidation,
      updateTravellersInfo,
      updateTravellersInfoValidation,
      continue_,
    } = this.props;

    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <FlightBookingInfoBreadcrumbs />
          <SectionHeader>
            <Typography variant="h5" style={{ marginBottom: '16px' }}>
              <FormattedMessage id="booking.clientInfo" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {booking.outbound.ticket && !booking.fetchingTicketData ? (
              <>
                <div style={{ flex: 1 }}>
                  <FlightBookingInfoBox
                    contactInfo={contactInfo}
                    contactInfoValidation={contactInfoValidation}
                    continue_={continue_}
                    travellersInfo={travellersInfo}
                    travellersInfoValidation={travellersInfoValidation}
                    updateContactInfo={updateContactInfo}
                    updateContactInfoValidation={updateContactInfoValidation}
                    updateTravellersInfo={updateTravellersInfo}
                    updateTravellersInfoValidation={updateTravellersInfoValidation}
                  />
                </div>
                <div style={{ width: '370px', marginLeft: '30px', marginBottom: '94px' }}>
                  <StickyDiv style={{ top: 80 }}>
                    <FlightInfoBox hideTravellersBox seeDetail={() => this.setState({ seeDetails: true })} />
                  </StickyDiv>
                </div>
              </>
            ) : (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            )}
          </div>
        </Container>
        <FlightTicketDialog open={this.state.seeDetails} close={() => this.setState({ seeDetails: false })} />
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(FlightBookingInfoDesktop);
