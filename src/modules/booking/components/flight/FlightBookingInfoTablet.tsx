import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { ContactInfo, ContactInfoValidation, TravellersInfo, TravellersInfoValidation } from '../../../common/models';
import FlightBookingInfoBox from './FlightBookingInfoBox';
import FlightBookingInfoBreadcrumbs from './FlightBookingInfoBreadcrumbs';
import FlightInfoBox from './FlightInfoBox';
import { Wrapper } from '../styles';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.flight,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  contactInfo: ContactInfo;
  travellersInfo: TravellersInfo;
  contactInfoValidation: ContactInfoValidation;
  travellersInfoValidation: TravellersInfoValidation;
  updateContactInfo(info: ContactInfo): void;
  updateTravellersInfo(info: TravellersInfo): void;
  updateContactInfoValidation(validation: ContactInfoValidation): void;
  updateTravellersInfoValidation(validation: TravellersInfoValidation): void;
  continue_(): void;
}

interface State {
  seeDetails: boolean;
  showInfoBox: boolean;
}

class FlightBookingInfoTablet extends React.Component<Props, State> {
  state: State = {
    seeDetails: false,
    showInfoBox: false,
  };

  public render() {
    const {
      booking,
      travellersInfo,
      contactInfo,
      travellersInfoValidation,
      contactInfoValidation,
      updateContactInfo,
      updateContactInfoValidation,
      updateTravellersInfo,
      updateTravellersInfoValidation,
      continue_,
    } = this.props;
    const { showInfoBox } = this.state;

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1, overflow: 'hidden' }}>
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>
              <FlightBookingInfoBreadcrumbs />
              <Typography variant="h5" style={{ marginBottom: '16px' }}>
                <FormattedMessage id="booking.clientInfo" />
              </Typography>
              {!booking.outbound.ticket || booking.fetchingTicketData ? (
                <Wrapper>
                  <LoadingIcon
                    style={{
                      height: '300px',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  />
                </Wrapper>
              ) : (
                <FlightBookingInfoBox
                  contactInfo={contactInfo}
                  contactInfoValidation={contactInfoValidation}
                  continue_={continue_}
                  travellersInfo={travellersInfo}
                  travellersInfoValidation={travellersInfoValidation}
                  updateContactInfo={updateContactInfo}
                  updateContactInfoValidation={updateContactInfoValidation}
                  updateTravellersInfo={updateTravellersInfo}
                  updateTravellersInfoValidation={updateTravellersInfoValidation}
                />
              )}
            </div>
            <div style={{ position: 'relative', direction: 'rtl' }}>
              {!showInfoBox && (
                <IconButton
                  style={{ margin: '10px', position: 'absolute' }}
                  onClick={() => this.setState({ showInfoBox: true })}
                >
                  <Open style={{ color: DARK_GREY }} />
                </IconButton>
              )}
              <div style={{ overflow: 'hidden', width: 0 }}>
                <Slide in={showInfoBox} direction="left">
                  <div
                    style={{
                      position: 'absolute',
                      zIndex: 10,
                      top: 0,
                      right: -32,
                      bottom: 0,
                      backgroundColor: BACKGROUND,
                      padding: '22px 10px 22px 10px',
                      marginTop: '-22px',
                      borderLeft: `1px solid ${GREY}`,
                    }}
                  >
                    <div style={{ textAlign: 'end' }}>
                      <IconButton style={{ margin: '10px 0' }} onClick={() => this.setState({ showInfoBox: false })}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    </div>
                    <div style={{ direction: 'ltr' }}>
                      {booking.outbound.ticket && (
                        <FlightInfoBox hideTravellersBox seeDetail={() => this.setState({ seeDetails: true })} />
                      )}
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        </Container>
        <FlightTicketDialog open={this.state.seeDetails} close={() => this.setState({ seeDetails: false })} />
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(FlightBookingInfoTablet);
