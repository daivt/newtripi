import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { setSelectedPaymentMethod } from '../../redux/flightBookingReducer';
import { computePayableNumbers } from '../../utils';
import DiscountCodeBox from '../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../common/components/Payment/PaymentMethodsBox';
import { Box, SectionHeader } from '../styles';
import { MY_TOUR } from '../../../../constants';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.flight,
    userData: state.account.userData,
  };
};

interface IFlightPayBoxProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const FlightPayBox: React.FunctionComponent<IFlightPayBoxProps> = props => {
  const { booking, dispatch } = props;
  const payableNumbers = computePayableNumbers(booking, true);
  const priceAfter = payableNumbers.finalPrice - payableNumbers.pointToAmount;
  return (
    <>
      <Box>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.discountCode" />
          </Typography>
        </SectionHeader>
        <DiscountCodeBox moduleType="flight" />
      </Box>
      {booking.paymentMethods && booking.selectedPaymentMethod && (
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
          </SectionHeader>
          <PaymentMethodsBox
            paymentMethods={booking.paymentMethods}
            total={payableNumbers.finalPrice}
            setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={booking.selectedPaymentMethod}
            priceAfter={priceAfter}
            promotionCode={booking.promotionCode}
            moduleType="flight"
          />
        </Box>
      )}
      {!MY_TOUR && (
        <div
          style={{
            padding: '16px 16px 32px 16px',
            color: BLUE,
          }}
        >
          <Typography variant="body2">
            <FormattedMessage id="booking.seePaymentDetail" />
          </Typography>
        </div>
      )}
    </>
  );
};

export default connect(mapState2Props)(FlightPayBox);
