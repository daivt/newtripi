import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { SectionHeader, Wrapper } from '../styles';
import FlightInfoBox from './FlightInfoBox';
import FlightPayBox from './FlightPayBox';
import FlightPayBreadcrumbs from './FlightPayBreadcrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.flight,
  };
};

export interface IFlightPayDesktopProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {
  seeDetails: boolean;
}

class FlightPayDesktop extends React.Component<IFlightPayDesktopProps, State> {
  state: State = { seeDetails: false };

  public render() {
    const { booking } = this.props;
    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <FlightPayBreadcrumbs />
          <SectionHeader>
            <Typography variant="h5" style={{ marginBottom: '16px' }}>
              <FormattedMessage id="booking.pay" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  <FlightPayBox />
                </div>
                <div style={{ width: '370px', marginLeft: '30px', marginBottom: '94px' }}>
                  <StickyDiv
                    style={{
                      top: 80,
                    }}
                  >
                    <FlightInfoBox
                      promotion={booking.promotion}
                      seeDetail={() => this.setState({ seeDetails: true })}
                    />
                  </StickyDiv>
                </div>
              </>
            )}
          </div>
        </Container>
        <FlightTicketDialog open={this.state.seeDetails} close={() => this.setState({ seeDetails: false })} />
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(FlightPayDesktop);
