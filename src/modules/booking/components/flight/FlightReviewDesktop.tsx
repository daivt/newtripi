import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { DESKTOP_WIDTH } from '../../../../constants';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { FlightBookingState } from '../../redux/flightBookingReducer';
import { SectionHeader } from '../styles';
import FlightInfoBox from './FlightInfoBox';
import FlightReviewBox from './FlightReviewBox';
import FlightReviewBreadcrumbs from './FlightReviewBreadcrumbs';

interface IFlightReviewDesktopProps {
  buttonGroup: React.ReactNode;
  booking: FlightBookingState;
}

interface State {
  seeDetails: boolean;
}
class FlightReviewDesktop extends React.Component<IFlightReviewDesktopProps, State> {
  state: State = {
    seeDetails: false,
  };

  componentDidMount() {}

  public render() {
    const { booking, buttonGroup } = this.props;

    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <FlightReviewBreadcrumbs />
          <SectionHeader>
            <Typography
              variant="h5"
              style={{
                marginBottom: '16px',
              }}
            >
              <FormattedMessage id="booking.reviewInfo" />
            </Typography>
          </SectionHeader>

          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>
              <FlightReviewBox booking={booking} buttonGroup={buttonGroup} />
            </div>
            <div style={{ width: '370px', marginLeft: '30px', marginBottom: '94px' }}>
              <StickyDiv style={{ top: 80 }}>
                <FlightInfoBox seeDetail={() => this.setState({ seeDetails: true })} />
              </StickyDiv>
            </div>
          </div>
        </Container>
        <FlightTicketDialog open={this.state.seeDetails} close={() => this.setState({ seeDetails: false })} />
        <Footer />
      </PageWrapper>
    );
  }
}

export default FlightReviewDesktop;
