import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { BLUE } from '../../../../colors';
import { ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { REASON } from '../../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../../common/constants';
import { Box } from '../styles';
import FlightItineraryBox from './FlightItineraryBox';
import FlightPriceBox from './FlightPriceBox';
import FlightTravellersBox from './FlightTravellersBox';

interface IFlightInfoBoxProps extends ReturnType<typeof mapStateToProps> {
  seeDetail(): void;
  hideTravellersBox?: boolean;
  promotion?: some;
  styles?: React.CSSProperties;
}

const FlightInfoBox: React.FunctionComponent<IFlightInfoBoxProps> = props => {
  const { booking, hideTravellersBox, location, promotion, styles } = props;
  const noMarkUp = location.pathname === ROUTES.flight.flightPay;

  if (!booking.travellersInfo && !hideTravellersBox) {
    return (
      <Redirect
        to={{
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }}
      />
    );
  }

  return (
    <Box
      style={{
        ...styles,
        background: 'white',
        boxShadow: '0px 1px 3px rgba(0,0,0,0.2), 0px 2px 2px rgba(0,0,0,0.12), 0px 0px 2px rgba(0,0,0,0.14)',
        borderRadius: '4px',
        padding: '14px 16px',
        marginBottom: '20px',
        alignSelf: 'flex-start',
        minWidth: 370,
      }}
    >
      <div style={{ display: 'flex', alignItems: 'baseline' }}>
        <Typography variant="h5" style={{ flex: 1 }}>
          <FormattedMessage id="result.flightInfo" />
        </Typography>
        <Typography variant="caption" style={{ color: BLUE, cursor: 'pointer' }} onClick={props.seeDetail}>
          <FormattedMessage id="booking.seeDetails" />
        </Typography>
      </div>
      <FlightItineraryBox />
      {!hideTravellersBox && booking.travellersInfo && <FlightTravellersBox travellers={booking.travellersInfo} />}
      <FlightPriceBox booking={booking} noMarkUp={noMarkUp} promotion={promotion} />
    </Box>
  );
};

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking.flight,
    location: state.router.location,
  };
}

export default connect(mapStateToProps)(FlightInfoBox);
