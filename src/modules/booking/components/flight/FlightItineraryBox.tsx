import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import moment from 'moment';
import { GREY } from '../../../../colors';
import { some, SLASH_DATE_FORMAT } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconAirplane } from '../../../../svg/ic_flight_itinerary_airplane.svg';
import { millisecondsToHour } from '../../../../utils';
import { subtitle3Styles } from '../../../common/components/elements';
import { REASON } from '../../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../../common/constants';
import { makeGetAirlineInfo } from '../../../common/utils';

interface IFlightItineraryBoxProps extends ReturnType<typeof mapStateToProps> {}

function renderFlightItineraryItem(ticketDetail: some, airlineInfo: some, index?: number) {
  return (
    <div
      key={index}
      style={{
        borderRadius: '4px',
        border: `1px solid ${GREY}`,
        marginBottom: '10px',
        marginLeft: '8px',
      }}
    >
      <div
        style={{
          display: 'flex',
          padding: '2px 10px',
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant="body2">{ticketDetail.departureTimeStr}</Typography>
          <Typography variant="body2">{ticketDetail.arrivalTimeStr}</Typography>
        </div>

        <div
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            paddingLeft: '16px',
          }}
        >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="body2">
              {ticketDetail.departureCity} ({ticketDetail.departureAirport})
            </Typography>
            <Typography variant="body2">
              {ticketDetail.arrivalCity} ({ticketDetail.arrivalAirport})
            </Typography>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {airlineInfo && <img style={{ maxWidth: '48px' }} src={airlineInfo.logo} alt="" />}
        </div>
      </div>
    </div>
  );
}

const RenderFlightItineraryBox: React.FunctionComponent<some> = props => {
  const { outbound } = props.ticket;
  const transitTickets = outbound.transitTickets ? outbound.transitTickets : undefined;

  return (
    <div style={{ paddingTop: '6px' }}>
      {transitTickets
        ? transitTickets.map((item: some, index: number) =>
            renderFlightItineraryItem(item, props.getAirlineInfo(item.aid), index),
          )
        : renderFlightItineraryItem(outbound, props.getAirlineInfo(outbound.aid))}
    </div>
  );
};

const FlightItineraryBox: React.FunctionComponent<IFlightItineraryBoxProps> = props => {
  const outboundTicket = props.booking.outbound.ticket;
  const inboundTicket = props.booking.inbound.ticket ? props.booking.inbound.ticket : undefined;

  if (!outboundTicket) {
    return (
      <Redirect
        to={{
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }}
      />
    );
  }

  const holdingTime = inboundTicket
    ? Math.min(outboundTicket.outbound.ticketdetail.holdingTime, inboundTicket.outbound.ticketdetail.holdingTime)
    : outboundTicket.outbound.ticketdetail.holdingTime;

  const holdingTimeHours = millisecondsToHour(holdingTime);

  return (
    <div>
      <div
        style={{
          paddingTop: '12px',
        }}
      >
        {!!holdingTimeHours && <FormattedMessage id="result.holdingTime" values={{ num: holdingTimeHours }} />}
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '12px' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <IconAirplane />
          <span style={{ ...subtitle3Styles, paddingLeft: '10px' }}>
            <FormattedMessage id="outbound" /> -{' '}
            {moment(outboundTicket.outbound.departureDayStr, SLASH_DATE_FORMAT).format('L')}
          </span>
        </div>

        <RenderFlightItineraryBox ticket={outboundTicket} getAirlineInfo={props.getAirlineInfo} />
      </div>

      {inboundTicket && (
        <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '12px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconAirplane />
            <span style={{ ...subtitle3Styles, paddingLeft: '10px' }}>
              <FormattedMessage id="inbound" /> -{' '}
              {moment(inboundTicket.outbound.departureDayStr, SLASH_DATE_FORMAT).format('L')}
            </span>
          </div>

          <RenderFlightItineraryBox ticket={inboundTicket} getAirlineInfo={props.getAirlineInfo} />
        </div>
      )}
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    getAirlineInfo: makeGetAirlineInfo(state.common),
    booking: state.booking.flight,
  };
}

export default connect(mapStateToProps)(FlightItineraryBox);
