import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREEN, GREY, ORANGE, PRIMARY, RED } from '../../../../colors';
import { some } from '../../../../constants';
import { ReactComponent as IconCoinSvg } from '../../../../svg/coin.svg';
import { subtitle3Styles } from '../../../common/components/elements';
import { PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../../common/constants';
import { FlightBookingState } from '../../redux/flightBookingReducer';
import { computePayableNumbers, computePaymentFees, computePoints } from '../../utils';

interface IFlightPriceBoxProps {
  booking: FlightBookingState;
  noMarkUp?: boolean;
  promotion?: some;
}
const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const MoneySpan = styled.span`
  color: ${ORANGE};
`;

const CountSpan = styled.span`
  color: ${BLACK_TEXT};
`;
const FlightPriceBox: React.FunctionComponent<IFlightPriceBoxProps> = props => {
  const { booking, noMarkUp, promotion } = props;
  const payableNumbers = computePayableNumbers(booking, noMarkUp);
  const paymentFee = booking.selectedPaymentMethod
    ? computePaymentFees(booking.selectedPaymentMethod, payableNumbers.finalPrice)
    : 0;

  const { adult, children, baby, extraBaggagesCosts, discountAmount } = payableNumbers;

  const totalPayable = payableNumbers.finalPrice - payableNumbers.pointToAmount + paymentFee;
  const bonusPoint = promotion && promotion.bonusPoint ? promotion.bonusPoint : computePoints(booking);
  return (
    <div style={{ flex: 1, marginTop: '16px' }}>
      <Typography variant="h6">
        <FormattedMessage id="booking.priceDetails" />
      </Typography>
      <div style={{ flex: 1, marginTop: '8px' }}>
        {!!adult.number && (
          <Line>
            <FormattedMessage id="booking.adultPrice" />
            <MoneySpan>
              <CountSpan>
                {adult.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={adult.unitPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        {!!children.number && (
          <Line>
            <FormattedMessage id="booking.childPrice" />
            <MoneySpan>
              <CountSpan>
                {children.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={children.unitPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        {!!baby.number && (
          <Line>
            <FormattedMessage id="booking.babyPrice" />
            <MoneySpan>
              <CountSpan>
                {baby.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={baby.unitPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        {booking.insurancePackage && booking.buyInsurance && (
          <Line>
            <FormattedMessage id="booking.travelInsurance" />
            <MoneySpan>
              <CountSpan>
                {adult.number + children.number + baby.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={booking.insurancePackage.price} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        {!!extraBaggagesCosts && (
          <Line>
            <FormattedMessage id="booking.buyMoreCheckin" />
            <MoneySpan>
              <FormattedNumber value={extraBaggagesCosts} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}

        {booking.promotion && !!discountAmount && (
          <Line>
            {booking.selectedPaymentMethod && booking.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE ? (
              <FormattedMessage id="booking.discountCodeNotAcceptPaymentMethodHolding" />
            ) : (
              <>
                {booking.promotion.promotionDescription}
                <span style={{ color: PRIMARY }}>
                  <FormattedNumber value={0 - discountAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </span>
              </>
            )}
          </Line>
        )}

        {booking.usePointPayment &&
          booking.selectedPaymentMethod &&
          booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
          !!booking.pointUsing && (
            <Line>
              <FormattedMessage id="booking.pointPayment" />
              <MoneySpan style={{ color: PRIMARY }}>
                <FormattedNumber value={0 - payableNumbers.pointToAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </MoneySpan>
            </Line>
          )}

        {!!paymentFee && (
          <Line>
            {paymentFee > 0 ? (
              <FormattedMessage id="booking.paymentFixedFeeSide" />
            ) : (
              <FormattedMessage
                id="booking.discountPaymentMethod"
                values={{
                  methodName: booking.selectedPaymentMethod ? booking.selectedPaymentMethod.name : '',
                }}
              />
            )}
            <MoneySpan style={{ color: paymentFee > 0 ? ORANGE : GREEN }}>
              <FormattedNumber value={paymentFee} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: '10px',
        }}
      >
        <div style={subtitle3Styles}>
          <FormattedMessage id="booking.totalPayable" />
        </div>
        <Typography variant="subtitle1" style={{ fontStyle: 'bold', color: RED }}>
          <FormattedNumber value={totalPayable > 0 ? totalPayable : 0} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </div>

      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          color: `${GREY}`,
        }}
      >
        <Line>
          <Typography color="textSecondary" variant="caption">
            <FormattedMessage id="booking.includeTaxesAndFees" />
          </Typography>
        </Line>
      </div>
      {bonusPoint > 0 && (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          <IconCoinSvg style={{ marginRight: '10px' }} />
          <Typography variant="body2" style={{ color: GREEN }}>
            <FormattedNumber value={bonusPoint} />
            &nbsp;
            <FormattedMessage id="point" />
          </Typography>
        </div>
      )}
    </div>
  );
};

export default FlightPriceBox;
