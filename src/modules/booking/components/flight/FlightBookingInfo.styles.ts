import styled from 'styled-components';

export const FieldHeader = styled.div`
  display: flex;
  align-items: center;
  height: 40px;
`;

export const TravellerColumn = styled.div`
  flex: 1;
`;

export const DirectionColumn = styled.div`
  flex: 2;
  margin-right: 20px;
`;

export const InsuranceBox = styled.div`
  display: flex;
  align-items: center;
  padding-top: 4px;
`;
