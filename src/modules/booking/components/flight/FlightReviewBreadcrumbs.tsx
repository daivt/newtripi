import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { go } from 'connected-react-router';
import { AppState } from '../../../../redux/reducers';
import { MY_TOUR } from '../../../../constants';

interface IFlightReviewBreadcrumbsProps extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightReviewBreadcrumbs: React.FunctionComponent<IFlightReviewBreadcrumbsProps> = props => {
  const { state } = props;
  const backableToResult = state && state.backableToResult;
  const backableToResultTwoWay = state && state.backableToResultTwoWay;
  const backableToInfo = state && state.backableToInfo;

  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToResult || backableToResultTwoWay ? 'pointer' : undefined }}
          onClick={() =>
            backableToResult || backableToResultTwoWay
              ? props.dispatch(go(backableToResultTwoWay ? -3 : -2))
              : undefined
          }
        >
          1. <FormattedMessage id="booking.search" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToInfo ? 'pointer' : undefined }}
          onClick={() => (backableToInfo ? props.dispatch(go(-1)) : undefined)}
        >
          2. <FormattedMessage id="booking.travellersInfo" />
        </Typography>
        <Typography variant="body2" color={MY_TOUR ? 'primary' : 'secondary'}>
          3. <FormattedMessage id="booking.reviewInfo" />
        </Typography>
        <Typography variant="body2">
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(FlightReviewBreadcrumbs);
