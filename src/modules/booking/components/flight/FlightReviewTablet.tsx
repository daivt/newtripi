import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { PageWrapper } from '../../../common/components/elements';
import FlightTicketDialog from '../../../common/components/FlightTicketDialog';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { FlightBookingState } from '../../redux/flightBookingReducer';
import FlightInfoBox from './FlightInfoBox';
import FlightReviewBox from './FlightReviewBox';
import FlightReviewBreadcrumbs from './FlightReviewBreadcrumbs';

interface IFlightReviewTabletProps {
  buttonGroup: React.ReactNode;
  booking: FlightBookingState;
}

interface State {
  seeDetails: boolean;
  showInfoBox: boolean;
}
class FlightReviewTablet extends React.Component<IFlightReviewTabletProps, State> {
  state: State = {
    seeDetails: false,
    showInfoBox: false,
  };

  componentDidMount() {}

  public render() {
    const { booking, buttonGroup } = this.props;
    const { showInfoBox } = this.state;

    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1, overflow: 'hidden' }}>
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>
              <FlightReviewBreadcrumbs />
              <Typography
                variant="h5"
                style={{
                  marginBottom: '16px',
                }}
              >
                <FormattedMessage id="booking.reviewInfo" />
              </Typography>
              <FlightReviewBox booking={booking} buttonGroup={buttonGroup} />
            </div>
            <div style={{ position: 'relative', direction: 'rtl' }}>
              {!showInfoBox && (
                <IconButton
                  style={{ margin: '10px', position: 'absolute' }}
                  onClick={() => this.setState({ showInfoBox: true })}
                >
                  <Open style={{ color: DARK_GREY }} />
                </IconButton>
              )}
              <div style={{ overflow: 'hidden', width: 0 }}>
                <Slide in={showInfoBox} direction="left">
                  <div
                    style={{
                      position: 'absolute',
                      zIndex: 10,
                      top: 0,
                      right: -32,
                      bottom: 0,
                      backgroundColor: BACKGROUND,
                      padding: '22px 10px 22px 10px',
                      marginTop: '-22px',
                      borderLeft: `1px solid ${GREY}`,
                    }}
                  >
                    <div style={{ textAlign: 'end' }}>
                      <IconButton style={{ margin: '10px 0' }} onClick={() => this.setState({ showInfoBox: false })}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    </div>
                    <div style={{ direction: 'ltr' }}>
                      <FlightInfoBox hideTravellersBox seeDetail={() => this.setState({ seeDetails: true })} />
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        </Container>
        <FlightTicketDialog open={this.state.seeDetails} close={() => this.setState({ seeDetails: false })} />
        <Footer />
      </PageWrapper>
    );
  }
}

export default FlightReviewTablet;
