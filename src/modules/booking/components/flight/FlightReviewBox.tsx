/* eslint-disable react/jsx-one-expression-per-line */
import { Divider, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, HOVER_GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { ContactInfo, TravellerInfo } from '../../../common/models';
import { FlightBookingState } from '../../redux/flightBookingReducer';
import { Box, SectionHeader } from '../styles';

const Typo = (props: some) => (
  <Typography variant="body2" style={{ ...props.style, flexShrink: 0 }}>
    {props.children}
  </Typography>
);
const Line = styled.div`
  min-height: 40px;
  display: flex;
  align-items: center;
`;
function renderInfo(name: string, infos: TravellerInfo[], offset: number, data: FlightBookingState) {
  if (!data.outbound.ticket) {
    return false;
  }

  const outboundBag = data.outbound.extraBaggages;
  const inboundBag = data.inbound.extraBaggages;
  const ticketOutboundBag = data.outbound.ticket.outbound.baggages;
  let ticketInboundBag: some;
  if (data.inbound.ticket) {
    ticketInboundBag = data.inbound.ticket.outbound.baggages;
  }

  const extraBaggagesMsg = outboundBag.map((v, index) => ({
    out:
      ticketOutboundBag && outboundBag[offset + index] ? (
        <>
          {`${ticketOutboundBag[outboundBag[offset + index]].name}` +
            ' - ' +
            `${ticketOutboundBag[outboundBag[offset + index]].price} `}
          <FormattedMessage id="currency" />
        </>
      ) : (
        <FormattedMessage id="none" />
      ),
    in: ticketInboundBag ? (
      inboundBag && inboundBag[offset + index] ? (
        <>
          {`${ticketInboundBag[inboundBag[offset + index]].name}` +
            ' - ' +
            `${ticketInboundBag[inboundBag[offset + index]].price}`}
          <FormattedMessage id="currency" />
        </>
      ) : (
        <FormattedMessage id="none" />
      )
    ) : null,
  }));

  return infos.map((info, index) => {
    return (
      // eslint-disable-next-line react/no-array-index-key
      <div key={index}>
        <div style={{ display: 'flex', padding: '8px 0px 16px 0px' }}>
          <Typography
            style={{
              fontWeight: 500,
              height: '40px',
              flexBasis: '160px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {index + 1 + offset}
            .&nbsp;
            <FormattedMessage id={name} values={{ num: infos.length !== 1 ? index + 1 : '' }} />
          </Typography>
          <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
            <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '50px' }}>
              <Line>
                <Typo style={{ width: '130px' }}>
                  <FormattedMessage id="fullName" />
                  :&nbsp;
                </Typo>
                <Typo style={{ width: '220px' }}>
                  <span style={{ fontWeight: 500 }}>
                    {info.familyName}
                    &nbsp;
                    {info.givenName}
                  </span>
                </Typo>
                <Typo style={{ width: '130px' }}>
                  <FormattedMessage id="gender" />:
                </Typo>
                <Typography variant="body2" style={{ fontWeight: 500, marginLeft: '20px' }}>
                  <FormattedMessage id={info.gender === 'm' ? 'male' : 'female'} />
                </Typography>
              </Line>
              {((name === 'booking.adult' && !!info.passportInfo.passport) || name !== 'booking.adult') && (
                <Line>
                  <Typo style={{ width: '130px' }}>
                    <FormattedMessage id="birthday" />
                    :&nbsp;
                  </Typo>
                  <Typo>
                    <span style={{ fontWeight: 500 }}>{info.birthday}</span>
                  </Typo>
                </Line>
              )}

              {!!info.passportInfo.passport && (
                <>
                  <Line>
                    <Typo style={{ width: '130px' }}>
                      <FormattedMessage id="booking.passport" />
                      :&nbsp;
                    </Typo>
                    <Typo style={{ width: '220px' }}>
                      <span style={{ fontWeight: 500 }}>{info.passportInfo.passport}</span>
                    </Typo>
                    <Typo style={{ width: '130px' }}>
                      <FormattedMessage id="booking.passportExpired" />:
                    </Typo>
                    <Typography variant="body2" style={{ fontWeight: 500, marginLeft: '20px' }}>
                      {info.passportInfo.passportExpiredDate}
                    </Typography>
                  </Line>
                  <Line>
                    <Typo style={{ width: '130px' }}>
                      <FormattedMessage id="booking.passportCountry" />
                      :&nbsp;
                    </Typo>
                    <Typo>
                      <span style={{ fontWeight: 500 }}>{info.passportInfo.passportCountry.name}</span>
                    </Typo>
                  </Line>
                  <Line>
                    <Typo style={{ width: '130px' }}>
                      <FormattedMessage id="booking.nationalityCountry" />
                      :&nbsp;
                    </Typo>
                    <Typography variant="body2" style={{ fontWeight: 500 }}>
                      {info.passportInfo.nationalityCountry.name}
                    </Typography>
                  </Line>
                </>
              )}
            </div>
            {name !== 'booking.baby' && (
              <div style={{ display: 'flex', flexDirection: 'column', background: HOVER_GREY }}>
                <Line
                  style={{
                    background: HOVER_GREY,
                    marginLeft: '50px',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Typography variant="body2">
                    <FormattedMessage id="booking.outBaggages" />: &nbsp;
                    {extraBaggagesMsg[index].out}
                  </Typography>
                </Line>
                {extraBaggagesMsg[index].in && (
                  <Line
                    style={{
                      background: HOVER_GREY,
                      marginLeft: '50px',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Typography variant="body2">
                      <FormattedMessage id="booking.inBaggages" />: &nbsp;
                      {extraBaggagesMsg[index].in}
                    </Typography>
                  </Line>
                )}
              </div>
            )}
          </div>
        </div>
        {index !== infos.length - 1 && <Divider />}
      </div>
    );
  });
}

function renderContactInfo(infos: ContactInfo) {
  return (
    <div style={{ padding: '0px 8px' }}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <Line>
          <Typo style={{ width: '120px' }}>
            <FormattedMessage id="fullName" />:
          </Typo>
          <Typo style={{ width: '192px', fontWeight: 500 }}>
            {infos.familyName}
            &nbsp;
            {infos.givenName}
          </Typo>
          <Typo style={{ width: '80px' }}>
            <FormattedMessage id="gender" />:
          </Typo>
          <Typo style={{ fontWeight: 500 }}>
            <FormattedMessage id={infos.gender === 'm' ? 'male' : 'female'} />
          </Typo>
        </Line>
        <Line>
          <Typo style={{ width: '120px' }}>
            <FormattedMessage id="telephone" />:
          </Typo>
          <Typo style={{ width: '192px', fontWeight: 500 }}>{infos.telephone}</Typo>
          <Typo style={{ width: '80px' }}>
            <FormattedMessage id="email" />:
          </Typo>
          <Typo style={{ color: `${BLUE}` }}>{infos.email}</Typo>
        </Line>
      </div>
      <Line>
        <Typo style={{ width: '120px' }}>
          <FormattedMessage id="address" />:
        </Typo>
        <Typo style={{ fontWeight: 500 }}>{infos.address}</Typo>
      </Line>
    </div>
  );
}

interface IFlightReviewBoxProps {
  buttonGroup: React.ReactNode;
  booking: FlightBookingState;
}

const FlightReviewBox: React.FunctionComponent<IFlightReviewBoxProps> = props => {
  const { booking, buttonGroup } = props;

  if (!booking.travellersInfo || !booking.contactInfo || !booking.outbound.ticket) {
    return <></>;
  }

  return (
    <>
      <Box style={{ paddingLeft: '22px' }}>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.travellersInfo" />
          </Typography>
        </SectionHeader>
        {renderInfo('booking.adult', booking.travellersInfo.adults, 0, booking)}
        {!!booking.travellersInfo.children.length && <Divider />}
        {renderInfo('booking.child', booking.travellersInfo.children, booking.travellersInfo.adults.length, booking)}
        {!!booking.travellersInfo.babies.length && <Divider />}
        {renderInfo(
          'booking.baby',
          booking.travellersInfo.babies,
          booking.travellersInfo.adults.length + booking.travellersInfo.children.length,
          booking,
        )}
      </Box>

      <Box>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="contactInfo" />
          </Typography>
        </SectionHeader>
        {renderContactInfo(booking.contactInfo)}
      </Box>
      {buttonGroup}
    </>
  );
};

export default FlightReviewBox;
