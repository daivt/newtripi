import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';
import { TravellersInfo } from '../../../common/models';

interface IFlightTravellersBoxProps {
  travellers: TravellersInfo;
}
const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const FlightTravellersBox: React.FunctionComponent<IFlightTravellersBoxProps> = props => {
  const { travellers } = props;
  const adults = travellers.adults;
  const children = travellers.children;
  const babies = travellers.babies;
  return (
    <div style={{ flex: 1, marginTop: '10px' }}>
      <Typography variant="h6">
        <FormattedMessage id="booking.travellersList" />
      </Typography>

      <div style={{ flex: 1, marginTop: '10px' }}>
        {adults.map((v, i) => (
          <Line key={i}>
            <Typography variant={'body2'}>
              {v.familyName}&nbsp;{v.givenName}
            </Typography>
            <Typography color={'textSecondary'} variant={'body2'}>
              <FormattedMessage id="booking.adult" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
        {children.map((v, i) => (
          <Line key={i}>
            <Typography variant={'body2'}>
              {v.familyName}&nbsp;{v.givenName}
            </Typography>
            <Typography color={'textSecondary'} variant={'body2'}>
              <FormattedMessage id="booking.child" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
        {babies.map((v, i) => (
          <Line key={i}>
            <Typography variant={'body2'}>
              {v.familyName}&nbsp;{v.givenName}
            </Typography>
            <Typography color={'textSecondary'} variant={'body2'}>
              <FormattedMessage id="booking.baby" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
      </div>
    </div>
  );
};

export default FlightTravellersBox;
