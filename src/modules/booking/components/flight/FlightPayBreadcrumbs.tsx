import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { go } from 'connected-react-router';
import { Dispatch } from 'redux';
import { MY_TOUR } from '../../../../constants';

interface IFlightPayBreadcrumbsProps extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const FlightPayBreadcrumbs: React.FunctionComponent<IFlightPayBreadcrumbsProps> = props => {
  const { state } = props;
  const backableToResult = state && state.backableToResult;
  const backableToResultTwoWay = state && state.backableToResultTwoWay;
  const backableToInfo = state && state.backableToInfo;
  const backableToReview = state && state.backableToReview;

  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToResult || backableToResultTwoWay ? 'pointer' : undefined }}
          onClick={() =>
            backableToResult || backableToResultTwoWay
              ? props.dispatch(go(backableToResultTwoWay ? -4 : -3))
              : undefined
          }
        >
          1. <FormattedMessage id="booking.search" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToInfo ? 'pointer' : undefined }}
          onClick={() => (backableToInfo ? props.dispatch(go(-2)) : undefined)}
        >
          2. <FormattedMessage id="booking.travellersInfo" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToReview ? 'pointer' : undefined }}
          onClick={() => (backableToReview ? props.dispatch(go(-1)) : undefined)}
        >
          3. <FormattedMessage id="booking.reviewInfo" />
        </Typography>
        <Typography variant="body2" color={MY_TOUR ? 'primary' : 'secondary'}>
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(FlightPayBreadcrumbs);
