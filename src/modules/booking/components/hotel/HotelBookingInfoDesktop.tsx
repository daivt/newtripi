import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { DESKTOP_WIDTH, some } from '../../../../constants';
import { ReactComponent as IconWarningSvg } from '../../../../svg/warning.svg';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { RoomBookingInfo, RoomBookingInfoValidation } from '../../../common/models/index';
import { HotelBookingParams } from '../../redux/hotelBookingReducer';
import HotelBookingInfoBox from './HotelBookingInfoBox';
import HotelBookingInfoBreadcrums from './HotelBookingInfoBreadcrumbs';
import HotelBookingInfoMainBox from './HotelBookingInfoMainBox';

interface IHotelBookingInfoDesktopProps {
  roomsData?: some[];
  hotelData?: some;
  params: HotelBookingParams;
  roomsBookingInfo: RoomBookingInfo[];
  roomsBookingInfoValidation: RoomBookingInfoValidation[];
  phoneNumberValidation: boolean;
  flightTicketCodeValidation: boolean;
  updateRoomsBookingInfo(info: RoomBookingInfo[]): void;
  continue_(): void;
  flightTicketCode: string;
  updateFlightTicketCode(flightTicketCode: string): void;
}

const HotelBookingInfoDesktop: React.FunctionComponent<IHotelBookingInfoDesktopProps> = props => {
  const {
    roomsData,
    hotelData,
    params,
    roomsBookingInfo,
    updateRoomsBookingInfo,
    continue_,
    roomsBookingInfoValidation,
    phoneNumberValidation,
    flightTicketCode,
    flightTicketCodeValidation,
    updateFlightTicketCode,
  } = props;
  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <HotelBookingInfoBreadcrums />
        <Typography variant="h5" style={{ marginBottom: '16px' }}>
          <FormattedMessage id="booking.bookingHotelInfo" />
        </Typography>
        <div style={{ display: 'flex', alignItems: 'center', margin: '18px 0' }}>
          <IconWarningSvg style={{ marginRight: '10px' }} />
          <Typography variant="caption" color="error">
            <FormattedMessage id="booking.hotel.bookerInfoWarning" />
          </Typography>
        </div>
        {roomsData && hotelData ? (
          <div style={{ display: 'flex' }}>
            <HotelBookingInfoMainBox
              roomsBookingInfoState={roomsBookingInfo}
              updateRoomsBookingInfo={updateRoomsBookingInfo}
              roomsBookingInfoValidation={roomsBookingInfoValidation}
              phoneNumberValidation={phoneNumberValidation}
              continue_={continue_}
              style={{ flex: 1 }}
              params={params}
              roomsData={roomsData}
              flightTicketCode={flightTicketCode}
              updateFlightTicketCode={updateFlightTicketCode}
              flightTicketCodeValidation={flightTicketCodeValidation}
            />
            <HotelBookingInfoBox
              roomsData={roomsData}
              hotelData={hotelData}
              params={params}
              style={{ marginLeft: '31px', marginBottom: '32px' }}
            />
          </div>
        ) : (
          <LoadingIcon
            style={{
              margin: '50px 0',
            }}
          />
        )}
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default HotelBookingInfoDesktop;
