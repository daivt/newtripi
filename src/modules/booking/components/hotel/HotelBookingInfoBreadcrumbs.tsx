import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../../redux/reducers';

interface IHotelBookingInfoBreadcrumsProps extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const HotelBookingInfoBreadcrums: React.FunctionComponent<
  IHotelBookingInfoBreadcrumsProps
> = props => {
  const { state } = props;
  const backableToResult = state && state.backableToResult;
  const backableToDetail = state && state.backableToDetail;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToResult ? 'pointer' : undefined }}
          onClick={() => (backableToResult ? props.dispatch(go(-2)) : undefined)}
        >
          1. <FormattedMessage id="booking.search" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToDetail ? 'pointer' : undefined }}
          onClick={() => (backableToDetail ? props.dispatch(go(-1)) : undefined)}
        >
          2. <FormattedMessage id="result.hotelInfo" />
        </Typography>
        <Typography variant="body2" color="secondary">
          3. <FormattedMessage id="booking.enterInformation" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(HotelBookingInfoBreadcrums);
