import styled from 'styled-components';
import { GREY } from '../../../../colors';

export const Box = styled.div`
  background: white;
  border: 1px solid ${GREY};
  border-radius: 4px;
  padding: 8px 12px 16px 12px;
  margin-bottom: 16px;
`;

export const SectionHeader = styled.div`
  margin-bottom: 8px;
  min-height: 40px;
  align-items: center;
  display: flex;
`;
