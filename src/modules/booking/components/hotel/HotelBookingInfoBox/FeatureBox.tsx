import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../colors';
import { some } from '../../../../../constants';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;

class FeatureBox extends React.PureComponent<{ data: some }, { extend: boolean }> {
  state = { extend: false };
  public render() {
    const { data } = this.props;
    const { extend } = this.state;

    return (
      <>
        <div>
          <ul
            style={{
              columnCount: 2,
              listStyleType: 'disc',
              margin: 0,
              padding: '15px',
              paddingBottom: '0px',
            }}
          >
            {data.slice(0, extend ? data.length : 10).map((v: string, index: number) => (
              <li key={index}>
                <Typography variant="caption">{v}</Typography>
              </li>
            ))}
          </ul>
        </div>
        {data.length > 10 ? (
          <Line
            style={{
              justifyContent: 'flex-end',
              position: 'relative',
            }}
          >
            <Extend onClick={() => this.setState({ extend: !extend })}>
              <FormattedMessage id={extend ? 'booking.collapse' : 'booking.seeAll'} />
            </Extend>
          </Line>
        ) : (
          <div
            style={{
              height: '15px',
            }}
          />
        )}
      </>
    );
  }
}

export default FeatureBox;
