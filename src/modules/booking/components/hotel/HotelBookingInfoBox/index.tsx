import { Button, Dialog, DialogContent, IconButton, Typography, ButtonBase } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconLocation from '@material-ui/icons/LocationOnOutlined';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import { go, replace } from 'connected-react-router';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREEN, GREY, PRIMARY, RED } from '../../../../../colors';
import { ROUTES, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconCoinSvg } from '../../../../../svg/coin.svg';
import { HOTEL_BOOK_PARAMS_NAMES, PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../../../common/constants';
import { getRoomData } from '../../../../common/utils';
import { HotelBookingParams } from '../../../redux/hotelBookingReducer';
import { computeHotelPayableNumbers } from '../../../utils';
import FeatureBox from './FeatureBox';
import { RoomPriceDetail } from './RoomPriceDetail';
import { ReactComponent as InfoIcon } from '../../../../../svg/ic_payment_info.svg';

function mapState2Props(state: AppState) {
  return {
    booking: state.booking.hotel,
    location: state.router.location,
    state: state.router.location.state,
    search: state.router.location.search,
  };
}

export interface IHotelBookingInfoBoxProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
  hotelData: some;
  params: HotelBookingParams;
  roomsData: some[];
  style?: React.CSSProperties;
  promotion?: some;
}

interface IHotelBookingInfoBoxState {
  showExpireDialog: boolean;
  showCancelPoliciesDialog: boolean;
  showCheckinGuidanceDialog: boolean;
}

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
`;

class HotelBookingInfoBox extends React.PureComponent<IHotelBookingInfoBoxProps, IHotelBookingInfoBoxState> {
  state: IHotelBookingInfoBoxState = {
    showExpireDialog: true,
    showCancelPoliciesDialog: false,
    showCheckinGuidanceDialog: false,
  };

  closeDialog = () => {
    const { dispatch, state, search } = this.props;

    this.setState({ showExpireDialog: false });
    const backableToDetail = state && state.backableToDetail;

    if (backableToDetail) {
      dispatch(go(-1));
    } else {
      const params = new URLSearchParams(search);
      params.delete(HOTEL_BOOK_PARAMS_NAMES.shrui);
      dispatch(replace({ pathname: ROUTES.hotel.hotelDetail, search: params.toString() }));
    }
  };

  public render() {
    const { hotelData, style, params, roomsData, promotion, booking, location } = this.props;
    const { showExpireDialog, showCancelPoliciesDialog, showCheckinGuidanceDialog } = this.state;
    const room = getRoomData(roomsData, params);

    if (!room) {
      return (
        <Dialog open={!!showExpireDialog} maxWidth="sm">
          <div style={{ position: 'relative' }}>
            <div
              style={{
                width: '100%',
                textAlign: 'end',
                paddingRight: '3px',
                paddingTop: '3px',
                position: 'absolute',
              }}
            >
              <IconButton size="small" onClick={this.closeDialog}>
                <CloseIcon style={{ fontSize: '24px' }} />
              </IconButton>
            </div>
          </div>
          <DialogContent
            style={{
              padding: '0 34px',
              display: 'flex',
              alignContent: 'center',
              justifyContent: 'space-around',
            }}
          >
            <div style={{ backgroundColor: 'white', padding: '15px', textAlign: 'center' }}>
              <div>
                <FormattedMessage id="booking.hotel.expiredHotel" />
              </div>
              <Button
                style={{
                  padding: '1px 0',
                  width: '90px',
                  height: '30px',
                  boxShadow: 'none',
                  marginTop: '20px',
                }}
                color="secondary"
                variant="contained"
                size="small"
                onClick={this.closeDialog}
              >
                <FormattedMessage id="ok" />
              </Button>
            </div>
          </DialogContent>
        </Dialog>
      );
    }
    const nights = moment(params.checkOut, 'start-days').diff(moment(params.checkIn, 'start-days'), 'days');

    const payableNumbers = computeHotelPayableNumbers(booking, location.pathname === ROUTES.hotel.hotelPay);

    const finalPrice = payableNumbers.finalPrice + payableNumbers.paymentFee - payableNumbers.pointToAmount;

    const { priceDetail } = room;

    return (
      <div style={{ ...style, display: 'flex', flexDirection: 'column' }}>
        <div
          style={{
            width: '368px',
            backgroundColor: 'white',
            borderRadius: '4px',
            boxShadow: '0px 1px 3px rgba(0,0,0,0.2), 0px 2px 2px rgba(0,0,0,0.12), 0px 0px 2px rgba(0,0,0,0.14)',
            padding: '16px',
          }}
        >
          <Typography variant="h5">{hotelData.name}</Typography>
          <Line>
            <Rating
              icon={<IconStar style={{ height: '20px', marginRight: '-8px' }} />}
              value={hotelData.starNumber}
              readOnly
            />
          </Line>
          <Line style={{ padding: '8px 0px', justifyContent: 'flex-start' }}>
            <IconLocation style={{ padding: '4px', color: DARK_GREY }} />
            <Typography color="textSecondary" style={{ marginLeft: '8px' }}>
              {hotelData.address}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage id="m.holdingTime" />
            </Typography>
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage
                id="m.time"
                values={{
                  day: Math.floor(moment.duration(room.holdingTime).asDays()),
                  time: moment.utc(moment.duration(room.holdingTime).asMilliseconds()).format('HH:mm:ss'),
                }}
              />
            </Typography>
          </Line>
          <Line style={{ marginTop: '16px' }}>
            <Typography variant="subtitle1">
              <FormattedMessage id="booking.bookingHotelInfo" />
            </Typography>
          </Line>
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.checkIn" />
            </Typography>
            <Line style={{ justifyContent: 'flex-end' }}>
              <Typography variant="body2" color="textSecondary">
                {moment(params.checkIn).format('L')}
                {params.checkIn && hotelData.checkInTime && (
                  <>
                    &nbsp;
                    <FormattedMessage id="from" />
                    &nbsp;
                  </>
                )}
                {hotelData.checkInTime}
              </Typography>
            </Line>
          </Line>
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.checkOut" />
            </Typography>
            <Line style={{ justifyContent: 'flex-end' }}>
              <Typography variant="body2" color="textSecondary">
                {moment(params.checkOut).format('L')}

                {params.checkIn && hotelData.checkInTime && (
                  <>
                    &nbsp;
                    <FormattedMessage id="to" />
                    &nbsp;
                  </>
                )}

                {hotelData.checkOutTime}
              </Typography>
            </Line>
          </Line>
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.totalDay" />
            </Typography>
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="booking.nights" values={{ num: nights }} />
            </Typography>
          </Line>
          <div style={{ marginTop: '12px', display: 'flex', justifyContent: 'space-between' }}>
            <Typography variant="subtitle2" style={{ marginRight: '16px' }}>
              <FormattedMessage id="booking.room" values={{ num: '' }} />
            </Typography>
            <div style={{ display: 'flex', justifyContent: 'flex-end', wordBreak: 'break-word' }}>
              <Typography style={{ textAlign: 'right' }} variant="body2" color="textSecondary">
                {room.roomTitle}
              </Typography>
            </div>
          </div>
          <ButtonBase
            style={{ display: 'flex', alignItems: 'center' }}
            onClick={() => {
              this.setState({ showCancelPoliciesDialog: true });
            }}
          >
            <InfoIcon />
            <Typography variant="caption" style={{ color: BLUE, marginLeft: '11px' }}>
              <FormattedMessage id="booking.hotel.cancelPolicies" />
            </Typography>
          </ButtonBase>
          <ButtonBase
            style={{ display: 'flex', alignItems: 'center', marginTop: '10px' }}
            onClick={() => {
              this.setState({ showCheckinGuidanceDialog: true });
            }}
          >
            <InfoIcon />
            <Typography variant="caption" style={{ color: BLUE, marginLeft: '11px' }}>
              <FormattedMessage id="booking.hotel.checkinGuidance" />
            </Typography>
          </ButtonBase>
          <Dialog open={showCancelPoliciesDialog}>
            <div style={{ position: 'relative' }}>
              <div
                style={{
                  width: '100%',
                  textAlign: 'end',
                  paddingRight: '3px',
                  paddingTop: '3px',
                  position: 'absolute',
                }}
              >
                <IconButton
                  size="small"
                  onClick={() => {
                    this.setState({ showCancelPoliciesDialog: false });
                  }}
                >
                  <CloseIcon style={{ fontSize: '24px' }} />
                </IconButton>
              </div>
            </div>
            <DialogContent
              style={{
                padding: '0 34px',
                display: 'flex',
                alignContent: 'center',
                justifyContent: 'space-around',
              }}
            >
              <div style={{ backgroundColor: 'white', padding: '15px' }}>
                <div style={{ textAlign: 'center', marginBottom: '10px' }}>
                  <Typography variant="h5">
                    <FormattedMessage id="booking.hotel.cancelPolicies" />
                  </Typography>
                </div>
                {room.cancellationPoliciesList && room.cancellationPoliciesList.length > 0 ? (
                  <>
                    {room.cancellationPoliciesList.map((v: string, index: number) => (
                      <Typography key={index} variant="body2" style={{ padding: '4px 0px ' }}>
                        -&nbsp;
                        {v}
                      </Typography>
                    ))}
                  </>
                ) : (
                  <Typography variant="body2" style={{ color: RED }}>
                    <FormattedMessage id="hotel.result.list.noRefund" />
                  </Typography>
                )}
                <div style={{ width: '100%', textAlign: 'center' }}>
                  <Button
                    style={{
                      padding: '1px 0',
                      width: '90px',
                      height: '30px',
                      boxShadow: 'none',
                      marginTop: '20px',
                    }}
                    color="secondary"
                    variant="contained"
                    size="small"
                    onClick={() => {
                      this.setState({ showCancelPoliciesDialog: false });
                    }}
                  >
                    <FormattedMessage id="ok" />
                  </Button>
                </div>
              </div>
            </DialogContent>
          </Dialog>
          <Dialog open={showCheckinGuidanceDialog}>
            <div style={{ position: 'relative' }}>
              <div
                style={{
                  width: '100%',
                  textAlign: 'end',
                  paddingRight: '3px',
                  paddingTop: '3px',
                  position: 'absolute',
                }}
              >
                <IconButton
                  size="small"
                  onClick={() => {
                    this.setState({ showCheckinGuidanceDialog: false });
                  }}
                >
                  <CloseIcon style={{ fontSize: '24px' }} />
                </IconButton>
              </div>
            </div>
            <DialogContent
              style={{
                padding: '0 34px',
                display: 'flex',
                alignContent: 'center',
                justifyContent: 'space-around',
              }}
            >
              <div style={{ backgroundColor: 'white', padding: '15px' }}>
                <div style={{ textAlign: 'center', marginBottom: '10px' }}>
                  <Typography variant="h5">
                    <FormattedMessage id="booking.hotel.checkinGuidance" />
                  </Typography>
                </div>
                {room.checkinInstructions || room.checkinSpecialInstructions ? (
                  <>
                    {room.checkinInstructions && (
                      <Typography variant="body2" style={{ padding: '4px 0px ' }}>
                        -&nbsp;
                        <span dangerouslySetInnerHTML={{ __html: room.checkinInstructions }} />
                      </Typography>
                    )}
                    {room.checkinSpecialInstructions && (
                      <Typography variant="body2" style={{ padding: '4px 0px ' }}>
                        -&nbsp;
                        <span dangerouslySetInnerHTML={{ __html: room.checkinSpecialInstructions }} />
                      </Typography>
                    )}
                  </>
                ) : (
                  <Typography variant="body2" style={{ color: RED }}>
                    <FormattedMessage id="hotel.result.list.noRefundMessages" />
                  </Typography>
                )}
                <div style={{ width: '100%', textAlign: 'center' }}>
                  <Button
                    style={{
                      padding: '1px 0',
                      width: '90px',
                      height: '30px',
                      boxShadow: 'none',
                      marginTop: '20px',
                    }}
                    color="secondary"
                    variant="contained"
                    size="small"
                    onClick={() => {
                      this.setState({ showCheckinGuidanceDialog: false });
                    }}
                  >
                    <FormattedMessage id="ok" />
                  </Button>
                </div>
              </div>
            </DialogContent>
          </Dialog>

          <Line style={{ marginTop: '16px' }}>
            <Typography variant="subtitle1">
              <FormattedMessage id="booking.includedServices" />
            </Typography>
          </Line>
          <FeatureBox data={room.roomFeatures} />

          {priceDetail && (
            <RoomPriceDetail priceDetail={priceDetail} checkIn={params.checkIn} isEurope={room.isEuropeHotel} />
          )}

          {promotion && !!payableNumbers.discountAmount && (
            <Line>
              {booking.selectedPaymentMethod && booking.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE ? (
                <FormattedMessage id="booking.discountCodeNotAcceptPaymentMethodHolding" />
              ) : (
                <>
                  <Typography variant="body2">{promotion.promotionDescription}</Typography>
                  <div style={{ display: 'flex' }}>
                    <Typography style={{ color: PRIMARY }}>
                      <FormattedNumber value={0 - payableNumbers.discountAmount} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </div>
                </>
              )}
            </Line>
          )}

          {booking.usePointPayment &&
            booking.selectedPaymentMethod &&
            booking.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
            !!booking.pointUsing && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="booking.pointPayment" />
                </Typography>

                <Typography style={{ color: PRIMARY }}>
                  <FormattedNumber value={0 - payableNumbers.pointToAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
          {!!payableNumbers.paymentFee && (
            <Line>
              {payableNumbers.paymentFee > 0 ? (
                <FormattedMessage id="booking.paymentFixedFeeSide" />
              ) : (
                <FormattedMessage
                  id="booking.discountPaymentMethod"
                  values={{
                    methodName: booking.selectedPaymentMethod ? booking.selectedPaymentMethod.name : '',
                  }}
                />
              )}
              <div style={{ display: 'flex' }}>
                <Typography style={{ color: PRIMARY }}>
                  <FormattedNumber value={payableNumbers.paymentFee} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            </Line>
          )}
          <Line>
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.totalPayable" />
            </Typography>
            <Typography variant="h6" style={{ color: RED }}>
              <FormattedNumber value={finalPrice > 0 ? finalPrice : 0} maximumFractionDigits={0} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              color: `${GREY}`,
            }}
          >
            <Line>
              <Typography color="textSecondary" variant="caption">
                <FormattedMessage id="booking.includeTaxesAndFees" />
              </Typography>
            </Line>
          </div>
          <div>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
              <IconCoinSvg style={{ marginRight: '10px' }} />
              <Typography variant="body2" style={{ color: GREEN }}>
                <FormattedNumber
                  value={promotion && payableNumbers.discountAmount ? promotion.bonusPoint : room.bonusPoint}
                />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapState2Props)(HotelBookingInfoBox);
