import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Dispatch } from 'redux';
import { go } from 'connected-react-router';
import { connect } from 'react-redux';
import { AppState } from '../../../../redux/reducers';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

const HotelPayBreadcrums: React.FunctionComponent<Props> = props => {
  const { state } = props;
  const backableToResult = state && state.backableToResult;
  const backableToDetail = state && state.backableToDetail;
  const backableToBookingInfo = state && state.backableToBookingInfo;
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToResult ? 'pointer' : undefined }}
          onClick={() => (backableToResult ? props.dispatch(go(-3)) : undefined)}
        >
          1. <FormattedMessage id="booking.search" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToDetail ? 'pointer' : undefined }}
          onClick={() => (backableToDetail ? props.dispatch(go(-2)) : undefined)}
        >
          2. <FormattedMessage id="result.hotelInfo" />
        </Typography>
        <Typography
          variant="body2"
          color="textPrimary"
          style={{ cursor: backableToBookingInfo ? 'pointer' : undefined }}
          onClick={() => (backableToBookingInfo ? props.dispatch(go(-1)) : undefined)}
        >
          3. <FormattedMessage id="booking.enterInformation" />
        </Typography>
        <Typography variant="body2" color="secondary">
          4. <FormattedMessage id="booking.pay" />
        </Typography>
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(HotelPayBreadcrums);
