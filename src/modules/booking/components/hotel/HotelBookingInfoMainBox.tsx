import {
  Button,
  ButtonBase,
  Checkbox,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { BLUE, DARK_GREY, RED } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import { RoomBookingInfo, RoomBookingInfoValidation } from '../../../common/models';
import { getRoomData, validSpaceRegex, validTelephoneRegex } from '../../../common/utils';
import { HotelBookingParams, setRoomsBookingInfo } from '../../redux/hotelBookingReducer';
import { FreeTextField } from '../Form';
import { Box, SectionHeader } from '../styles';

const StyledNotice = styled.div`
  & strong {
    color: ${RED};
  }

  & p,
  span {
    padding-bottom: 6px;
    white-space: nowrap;
  }
`;

interface IHotelBookingInfoMainBoxProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  style?: React.CSSProperties;
  dispatch: Dispatch;
  roomsBookingInfoState: RoomBookingInfo[];
  roomsBookingInfoValidation: RoomBookingInfoValidation[];
  phoneNumberValidation: boolean;
  updateRoomsBookingInfo(info: RoomBookingInfo[]): void;
  continue_(): void;
  params: HotelBookingParams;
  roomsData: some[];
  flightTicketCode: string;
  flightTicketCodeValidation: boolean;
  updateFlightTicketCode(flightTicketCode: string): void;
}

interface State {
  isSelectContact: boolean;
  ticktock: boolean;
  index: number;
  confirm: boolean;
}

class HotelBookingInfoMainBox extends React.PureComponent<IHotelBookingInfoMainBoxProps, State> {
  state: State = {
    isSelectContact: false,
    ticktock: false,
    index: -1,
    confirm: false,
  };

  onSelectContact(guest: some, index: number) {
    const { roomsBookingInfoState, dispatch, roomsBookingInfoProps } = this.props;
    const roomsBookingInfo = roomsBookingInfoProps || roomsBookingInfoState;
    this.setState({ isSelectContact: false });
    const newInfo = {
      ...roomsBookingInfo[index],
      name: `${guest.lastName} ${guest.firstName}`,
      phoneNumber: guest.phone,
    };
    roomsBookingInfo[index] = newInfo;
    // updateRoomsBookingInfo(roomsBookingInfo);
    dispatch(setRoomsBookingInfo(roomsBookingInfo));
  }

  onCheck(value: boolean) {
    const { roomsBookingInfoState, updateRoomsBookingInfo, roomsBookingInfoProps } = this.props;
    const roomsBookingInfo = roomsBookingInfoProps || roomsBookingInfoState;
    if (value) {
      for (let i = 1; i < roomsBookingInfo.length; i += 1) {
        roomsBookingInfo[i].name = roomsBookingInfo[0].name;
        roomsBookingInfo[i].phoneNumber = roomsBookingInfo[0].phoneNumber;
      }
      updateRoomsBookingInfo(roomsBookingInfo);
    } else {
      for (let i = 1; i < roomsBookingInfo.length; i += 1) {
        roomsBookingInfo[i].name = '';
        roomsBookingInfo[i].phoneNumber = '';
      }
      updateRoomsBookingInfo(roomsBookingInfo);
    }
  }

  // updateRequests(request: string, index: number) {
  //   const { roomsBookingInfo, updateRoomsBookingInfo } = this.props;
  //   const newInfo = { ...roomsBookingInfo[index], specialRequest: request };
  //   roomsBookingInfo[index] = newInfo;
  //   updateRoomsBookingInfo(roomsBookingInfo);
  // }

  render() {
    const { isSelectContact, ticktock, confirm } = this.state;
    const {
      roomsBookingInfoState,
      intl,
      updateRoomsBookingInfo,
      roomsBookingInfoValidation,
      phoneNumberValidation,
      params,
      roomsData,
      flightTicketCodeValidation,
      flightTicketCode,
      updateFlightTicketCode,
      roomsBookingInfoProps,
      continue_,
    } = this.props;
    const room = getRoomData(roomsData, params);
    const roomsBookingInfo = roomsBookingInfoProps || roomsBookingInfoState;
    return (
      <div style={this.props.style}>
        {room && room.isPackageRate && (
          <Box>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="h6" style={{ marginTop: '16px' }}>
                <FormattedMessage id="booking.hotel.flightTicketCode" />
              </Typography>
              <Typography variant="body2" style={{ marginTop: '8px', color: DARK_GREY }}>
                <FormattedMessage id="booking.hotel.flightTicketCodeWarning" />
              </Typography>
              <FreeTextField
                key={`flightTicketCode${ticktock}`}
                text={flightTicketCode}
                placeholder={intl.formatMessage({ id: 'booking.hotel.flightTicketCodePlaceholder' })}
                update={value => !validSpaceRegex.test(value) && updateFlightTicketCode(value)}
                style={{ marginTop: '16px' }}
                valid={flightTicketCodeValidation}
              />
            </div>
          </Box>
        )}
        {roomsBookingInfo.map((item, index) => (
          <Box key={index}>
            <div style={{ display: 'flex' }}>
              <SectionHeader>
                <Typography variant="h6">
                  <FormattedMessage id="booking.hotel.customerInfo" values={{ num: index + 1 }} />
                </Typography>
                <ButtonBase
                  style={{
                    borderRadius: '4px',
                    marginLeft: '10px',
                  }}
                  onClick={() =>
                    this.setState({
                      isSelectContact: true,
                      index,
                    })
                  }
                >
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '32px',
                    }}
                  >
                    <IconContactList style={{ width: '24px', height: '24px' }} />
                  </div>
                  <Typography variant="body2" style={{ color: BLUE, padding: '0 8px' }}>
                    <FormattedMessage id="booking.addFromContact" />
                  </Typography>
                </ButtonBase>
              </SectionHeader>
            </div>
            <div style={{ display: 'flex' }}>
              <FreeTextField
                key={`customerName${ticktock}`}
                text={item.name}
                valid={roomsBookingInfoValidation[index] ? roomsBookingInfoValidation[index].name : true}
                header={intl.formatMessage({ id: 'signUp.fullName' })}
                placeholder={intl.formatMessage({ id: 'signUp.fullNameExample' })}
                update={value => {
                  const newInfo = { ...roomsBookingInfo[index], name: value };
                  roomsBookingInfo[index] = newInfo;
                  updateRoomsBookingInfo(roomsBookingInfo);
                }}
              />
              {index === 0 ? (
                <FreeTextField
                  key={`telephone${ticktock}`}
                  text={item.phoneNumber || ''}
                  valid={phoneNumberValidation}
                  header={intl.formatMessage({ id: 'telephone' })}
                  placeholder={intl.formatMessage({ id: 'signUp.phoneExample' })}
                  update={value => {
                    const newInfo = { ...roomsBookingInfo[index], phoneNumber: value };
                    roomsBookingInfo[index] = newInfo;
                    updateRoomsBookingInfo(roomsBookingInfo);
                  }}
                  regex={validTelephoneRegex}
                  style={{ flex: 1 }}
                />
              ) : (
                <div style={{ flex: 1, marginRight: '20px' }} />
              )}
            </div>
            {index === 0 && roomsBookingInfo.length > 1 && (
              <div style={{ display: 'flex', marginTop: ' 24px', alignItems: 'center' }}>
                <Checkbox
                  style={{ padding: 0, width: '18px', height: '18px', marginLeft: '4px' }}
                  // checked={true}
                  onChange={event => this.onCheck(event.target.checked)}
                  disabled={!roomsBookingInfo[0].name}
                />
                <Typography variant="body2" style={{ marginLeft: '10px' }}>
                  <FormattedMessage id="booking.hotel.useForOtherRooms" />
                </Typography>
              </div>
            )}
            <ExpansionPanel style={{ boxShadow: 'none', border: 'none', marginTop: '20px' }}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                style={{ padding: '0px' }}
              >
                <Typography variant="subtitle2">
                  <FormattedMessage id="booking.hotel.addRequest" />
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails
                style={{ background: 'white', paddingTop: '16px', padding: 'unset', flexDirection: 'column' }}
              >
                <Typography variant="body2" color="textSecondary">
                  <FormattedMessage id="booking.hotel.enterYourRequest" />
                </Typography>
                <FreeTextField
                  key={`customerName${ticktock}`}
                  text={item.specialRequest || ''}
                  header={intl.formatMessage({ id: 'booking.hotel.request' })}
                  placeholder={intl.formatMessage({ id: 'booking.hotel.enterRequest' })}
                  update={value => {
                    const newInfo = { ...roomsBookingInfo[index], specialRequest: value };
                    roomsBookingInfo[index] = newInfo;
                    updateRoomsBookingInfo(roomsBookingInfo);
                  }}
                  style={{ marginTop: '8px' }}
                  optional
                />
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Box>
        ))}
        {room && room.isPackageRate && (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Checkbox
              color="secondary"
              checked={confirm}
              onChange={event => this.setState({ confirm: event.target.checked })}
            />
            <Typography variant="body2">
              <FormattedMessage id="booking.hotel.confirmFlightTicketCode" />
            </Typography>
          </div>
        )}

        {room && room.invoiceDescription && (
          <StyledNotice>
            <div dangerouslySetInnerHTML={{ __html: room.invoiceDescription }} />
          </StyledNotice>
        )}

        <div style={{ textAlign: 'center', margin: '32px' }}>
          <Button
            size="large"
            style={{ width: '260px' }}
            variant="contained"
            color="secondary"
            disabled={!confirm && room?.isPackageRate}
            onClick={continue_}
          >
            <FormattedMessage id="continue" />
          </Button>
        </div>

        <SelectContactDialog
          show={isSelectContact}
          onClose={() => this.setState({ isSelectContact: false })}
          onSelect={guest => this.onSelectContact(guest, this.state.index)}
        />
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return { booking: state.booking.hotel, roomsBookingInfoProps: state.booking.hotel.roomsBookingInfo };
}

export default connect(mapStateToProps)(injectIntl(HotelBookingInfoMainBox));
