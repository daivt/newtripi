import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { some, TABLET_WIDTH } from '../../../../constants';
import { ReactComponent as IconWarningSvg } from '../../../../svg/warning.svg';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { RoomBookingInfo, RoomBookingInfoValidation } from '../../../common/models/index';
import { HotelBookingParams } from '../../redux/hotelBookingReducer';
import HotelBookingInfoBox from './HotelBookingInfoBox';
import HotelBookingInfoBreadcrums from './HotelBookingInfoBreadcrumbs';
import HotelBookingInfoMainBox from './HotelBookingInfoMainBox';

interface Props {
  roomsData?: some[];
  hotelData?: some;
  params: HotelBookingParams;
  roomsBookingInfo: RoomBookingInfo[];
  roomsBookingInfoValidation: RoomBookingInfoValidation[];
  phoneNumberValidation: boolean;
  flightTicketCodeValidation: boolean;
  updateRoomsBookingInfo(info: RoomBookingInfo[]): void;
  continue_(): void;
  flightTicketCode: string;
  updateFlightTicketCode(flightTicketCode: string): void;
}

const HotelBookingInfoTablet: React.FunctionComponent<Props> = props => {
  const {
    roomsData,
    hotelData,
    params,
    roomsBookingInfo,
    updateRoomsBookingInfo,
    continue_,
    roomsBookingInfoValidation,
    phoneNumberValidation,
    flightTicketCode,
    flightTicketCodeValidation,
    updateFlightTicketCode,
  } = props;
  const [showInfoBox, setShowInfoBox] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1, display: 'flex', overflow: 'hidden' }}>
        <div style={{ flex: 1 }}>
          <HotelBookingInfoBreadcrums />
          <Typography variant="h5" style={{ marginBottom: '16px' }}>
            <FormattedMessage id="booking.bookingHotelInfo" />
          </Typography>
          <div style={{ display: 'flex', alignItems: 'center', margin: '18px 0' }}>
            <IconWarningSvg style={{ marginRight: '10px' }} />
            <Typography variant="caption" color="error">
              <FormattedMessage id="booking.hotel.bookerInfoWarning" />
            </Typography>
          </div>
          {roomsData && hotelData ? (
            <HotelBookingInfoMainBox
              roomsBookingInfoState={roomsBookingInfo}
              roomsBookingInfoValidation={roomsBookingInfoValidation}
              phoneNumberValidation={phoneNumberValidation}
              updateRoomsBookingInfo={updateRoomsBookingInfo}
              continue_={continue_}
              style={{ flex: 1 }}
              params={params}
              roomsData={roomsData}
              flightTicketCode={flightTicketCode}
              flightTicketCodeValidation={flightTicketCodeValidation}
              updateFlightTicketCode={updateFlightTicketCode}
            />
          ) : (
            <LoadingIcon
              style={{
                margin: '50px 0',
              }}
            />
          )}
        </div>
        <div
          style={{
            position: 'relative',
            direction: 'rtl',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          {!showInfoBox && (
            <IconButton style={{ margin: '10px', position: 'absolute' }} onClick={() => setShowInfoBox(true)}>
              <Open style={{ color: DARK_GREY }} />
            </IconButton>
          )}
          <div style={{ width: 1, flex: 1, display: 'flex' }}>
            <Slide in={showInfoBox} direction="left">
              <div
                style={{
                  backgroundColor: BACKGROUND,
                  padding: '22px 0 22px 10px',
                  marginTop: '-22px',
                  borderLeft: `1px solid ${GREY}`,
                  width: '420px',
                }}
              >
                <div style={{ textAlign: 'end' }}>
                  <IconButton style={{ margin: '10px 0' }} onClick={() => setShowInfoBox(false)}>
                    <Close style={{ color: DARK_GREY }} />
                  </IconButton>
                </div>
                <div style={{ direction: 'ltr' }}>
                  {roomsData && hotelData && (
                    <HotelBookingInfoBox
                      roomsData={roomsData}
                      hotelData={hotelData}
                      params={params}
                      style={{ marginLeft: '31px', marginBottom: '32px' }}
                    />
                  )}
                </div>
              </div>
            </Slide>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default HotelBookingInfoTablet;
