import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { HotelBookingParams } from '../../redux/hotelBookingReducer';
import { SectionHeader, Wrapper } from '../styles';
import HotelBookingInfoBox from './HotelBookingInfoBox';
import HotelPayBox from './HotelPayBox';
import HotelPayBreadcrums from './HotelPayBreadcrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.hotel,
  };
};

export interface IHotelPayTabletProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {
  showInfoBox: boolean;
}

class HotelPayTablet extends React.Component<IHotelPayTabletProps, State> {
  state: State = { showInfoBox: false };

  public render() {
    const { booking } = this.props;
    const roomsData = booking.roomsData || [];
    const hotelData = booking.hotelData || {};
    const params = booking.params || ({} as HotelBookingParams);
    const { showInfoBox } = this.state;
    return (
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <div style={{ display: 'flex', overflowX: 'hidden' }}>
            <div style={{ flex: 1 }}>
              <HotelPayBreadcrums />
              <SectionHeader>
                <Typography variant="h5">
                  <FormattedMessage id="booking.pay" />
                </Typography>
              </SectionHeader>
              {!booking.paymentMethods ? (
                <Wrapper>
                  <LoadingIcon
                    style={{
                      height: '300px',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  />
                </Wrapper>
              ) : (
                <HotelPayBox />
              )}
            </div>
            <div
              style={{
                position: 'relative',
                direction: 'rtl',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {!showInfoBox && (
                <IconButton
                  style={{ margin: '10px', position: 'absolute' }}
                  onClick={() => this.setState({ showInfoBox: true })}
                >
                  <Open style={{ color: DARK_GREY }} />
                </IconButton>
              )}
              <div style={{ width: 1, flex: 1, display: 'flex' }}>
                <Slide in={showInfoBox} direction="left">
                  <div
                    style={{
                      backgroundColor: BACKGROUND,
                      padding: '22px 0 22px 10px',
                      marginTop: '-22px',
                      borderLeft: `1px solid ${GREY}`,
                      width: '420px',
                    }}
                  >
                    <div style={{ textAlign: 'end' }}>
                      <IconButton style={{ margin: '10px 0' }} onClick={() => this.setState({ showInfoBox: false })}>
                        <Close style={{ color: DARK_GREY }} />
                      </IconButton>
                    </div>
                    <div style={{ direction: 'ltr' }}>
                      {roomsData && hotelData && (
                        <HotelBookingInfoBox
                          roomsData={roomsData}
                          hotelData={hotelData}
                          params={params}
                          promotion={booking.promotion}
                          style={{ marginLeft: '31px', marginBottom: '32px' }}
                        />
                      )}
                    </div>
                  </div>
                </Slide>
              </div>
            </div>
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(HotelPayTablet);
