import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ROUTES } from '../../../../constants';
import { NewTabLink } from '../../../common/components/NewTabLink';

interface Props {}

const TermsAndConditions: React.FC<Props> = () => {
  return (
    <div>
      <Typography variant="h6">
        <FormattedMessage id="booking.hotel.termsAndConditions" />
      </Typography>

      <Typography variant="body2" style={{ paddingTop: '16px' }}>
        <FormattedMessage id="booking.hotel.termsAndConditionsDescription" />
        <NewTabLink href={ROUTES.terms}>
          &nbsp;
          <FormattedMessage id="booking.seeDetails" />
        </NewTabLink>
      </Typography>
    </div>
  );
};

export default TermsAndConditions;
