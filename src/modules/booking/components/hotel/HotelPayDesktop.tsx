import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { HotelBookingParams } from '../../redux/hotelBookingReducer';
import { SectionHeader, Wrapper } from '../styles';
import HotelBookingInfoBox from './HotelBookingInfoBox';
import HotelPayBox from './HotelPayBox';
import HotelPayBreadcrums from './HotelPayBreadcrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.hotel,
  };
};

export interface IHotelPayDesktopProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

interface State {}

class HotelPayDesktop extends React.Component<IHotelPayDesktopProps, State> {
  state: State = {};

  public render() {
    const { booking } = this.props;
    const roomsData = booking.roomsData || [];
    const hotelData = booking.hotelData || {};
    const params = booking.params || ({} as HotelBookingParams);

    return (
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <HotelPayBreadcrums />
          <SectionHeader>
            <Typography variant="h5">
              <FormattedMessage id="booking.pay" />
            </Typography>
          </SectionHeader>
          <div style={{ display: 'flex' }}>
            {!booking.paymentMethods ? (
              <Wrapper>
                <LoadingIcon
                  style={{
                    height: '300px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              </Wrapper>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  <HotelPayBox />
                </div>
                <HotelBookingInfoBox
                  style={{ marginLeft: '31px', marginBottom: '32px' }}
                  roomsData={roomsData}
                  hotelData={hotelData}
                  params={params}
                  promotion={booking.promotion}
                />
              </>
            )}
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    );
  }
}

export default connect(mapState2Props)(HotelPayDesktop);
