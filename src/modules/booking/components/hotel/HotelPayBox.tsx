import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Box, SectionHeader } from './styles';
import { BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import DiscountCodeBox from '../../../common/components/Payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../common/components/Payment/PaymentMethodsBox';
import { setSelectedPaymentMethod } from '../../redux/hotelBookingReducer';
import { computeHotelPayableNumbers } from '../../utils';
import TermsAndConditions from './TermsAndConditions';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.hotel,
    userData: state.account.userData,
  };
};

interface IHotelPayBoxProps extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const HotelPayBox: React.FunctionComponent<IHotelPayBoxProps> = props => {
  const { booking, dispatch } = props;

  const payableNumbers = computeHotelPayableNumbers(booking, true);
  const priceAfter = payableNumbers.finalPrice - payableNumbers.pointToAmount;

  return (
    <>
      <Box>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.discountCode" />
          </Typography>
        </SectionHeader>
        <DiscountCodeBox moduleType="hotel" />
      </Box>
      {booking.paymentMethods && booking.selectedPaymentMethod && (
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
          </SectionHeader>
          <PaymentMethodsBox
            paymentMethods={booking.paymentMethods}
            total={payableNumbers.finalPrice}
            setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={booking.selectedPaymentMethod}
            priceAfter={priceAfter}
            promotionCode={booking.promotionCode}
            moduleType="hotel"
          />
        </Box>
      )}
      <div
        style={{
          padding: '16px 16px 32px 16px',
          color: BLUE,
        }}
      >
        <Typography variant="body2">
          <FormattedMessage id="booking.seePaymentDetail" />
        </Typography>
      </div>

      <TermsAndConditions />
    </>
  );
};

export default connect(mapState2Props)(HotelPayBox);
