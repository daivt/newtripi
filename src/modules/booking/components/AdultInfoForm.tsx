import { InputAdornment, Typography } from '@material-ui/core';
import * as React from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { DARK_GREY } from '../../../colors';
import { TravellerInfoValidation, TravellerInfo } from '../../common/models';
import { validIdRegex, validNameRegex } from '../../common/utils';
import { PassportInfoType } from '../constants';
import { DateField, FieldDiv, FreeTextField, GenderField } from './Form';

interface IAdultInfoFormProps extends WrappedComponentProps {
  update(info: TravellerInfo, validation?: TravellerInfoValidation): void;
  showCountryDialog(passportInfoType: PassportInfoType): void;
  info: TravellerInfo;
  validation: TravellerInfoValidation;
  needPassport?: boolean;
  ticktock: boolean;
}

const AdultInfoForm: React.FunctionComponent<IAdultInfoFormProps> = props => {
  const { info, update, intl, validation, needPassport, showCountryDialog, ticktock } = props;

  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'booking.familyNameEx' })}
          update={name =>
            update({ ...info, familyName: name }, { ...validation, familyName: true })
          }
          regex={validNameRegex}
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'givenName' })}
          placeholder={intl.formatMessage({ id: 'givenNameEx' })}
          update={name => update({ ...info, givenName: name }, { ...validation, givenName: true })}
          regex={validNameRegex}
        />
        <GenderField gender={info.gender} update={gender => update({ ...info, gender })} />
      </div>
      {needPassport && (
        <>
          <div style={{ display: 'flex' }}>
            <DateField
              disableFuture
              key={`birthday${ticktock}`}
              header={intl.formatMessage({ id: 'birthday' })}
              date={info.birthday}
              update={birthday => update({ ...info, birthday }, { ...validation, birthday: true })}
              valid={validation.birthday}
            />
            <FreeTextField
              key={`passport${ticktock}`}
              text={info.passportInfo.passport}
              valid={validation.passportInfo.passport}
              header={intl.formatMessage({ id: 'booking.passport' })}
              placeholder={intl.formatMessage({ id: 'booking.passportEx' })}
              update={passport =>
                update(
                  { ...info, passportInfo: { ...info.passportInfo, passport } },
                  { ...validation, passportInfo: { ...validation.passportInfo, passport: true } },
                )
              }
              regex={validIdRegex}
            />
            <DateField
              disablePast
              key={`passportExpiredDate${ticktock}`}
              header={intl.formatMessage({ id: 'booking.passportExpired' })}
              date={info.passportInfo.passportExpiredDate}
              update={passportExpiredDate =>
                update(
                  { ...info, passportInfo: { ...info.passportInfo, passportExpiredDate } },
                  {
                    ...validation,
                    passportInfo: { ...validation.passportInfo, passportExpiredDate: true },
                  },
                )
              }
              valid={validation.passportInfo.passportExpiredDate}
            />
          </div>
          <div style={{ display: 'flex', flexWrap: 'wrap', margin: '12px 0' }}>
            <FreeTextField
              key={`passportCountry${ticktock}`}
              text={info.passportInfo.passportCountry.name || ''}
              valid={validation.passportInfo.passportCountry}
              header={intl.formatMessage({ id: 'booking.passportCountry' })}
              placeholder={intl.formatMessage({ id: 'booking.selectPassportCountry' })}
              update={value => {}}
              regex={validIdRegex}
              inputProps={{ readOnly: true }}
              onClick={() => showCountryDialog('passportCountry')}
              endAdornment={
                <InputAdornment position="end" variant="filled">
                  <Typography variant="caption" style={{ color: DARK_GREY, marginRight: '6px' }}>
                    &#9660;
                  </Typography>
                </InputAdornment>
              }
            />
            <FreeTextField
              key={`nationalityCountry${ticktock}`}
              text={info.passportInfo.nationalityCountry.name || ''}
              valid={validation.passportInfo.nationalityCountry}
              header={intl.formatMessage({ id: 'booking.nationalityCountry' })}
              placeholder={intl.formatMessage({ id: 'booking.selectNationalityCountry' })}
              update={value => {}}
              regex={validIdRegex}
              inputProps={{ readOnly: true }}
              onClick={() => showCountryDialog('nationalityCountry')}
              endAdornment={
                <InputAdornment position="end" variant="filled">
                  <Typography variant="caption" style={{ color: DARK_GREY, marginRight: '6px' }}>
                    &#9660;
                  </Typography>
                </InputAdornment>
              }
            />
            <FieldDiv />
          </div>
        </>
      )}
    </div>
  );
};

export default injectIntl(AdultInfoForm);
