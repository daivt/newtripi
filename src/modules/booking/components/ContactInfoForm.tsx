import * as React from 'react';
import { WrappedComponentProps, injectIntl } from 'react-intl';
import { ContactInfo, ContactInfoValidation } from '../../common/models';
import { validTelephoneRegex } from '../../common/utils';
import { FieldDiv, FreeTextField, GenderField } from './Form';

interface IContactInfoFormProps extends WrappedComponentProps {
  update(info: ContactInfo, validation?: ContactInfoValidation): void;
  info: ContactInfo;
  validation: ContactInfoValidation;
  ticktock: boolean;
}

const ContactInfoForm: React.FunctionComponent<IContactInfoFormProps> = props => {
  const { info, update, intl, validation, ticktock } = props;
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'familyNameExD' })}
          update={name =>
            update({ ...info, familyName: name }, { ...validation, familyName: true })
          }
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'givenName' })}
          placeholder={intl.formatMessage({ id: 'givenNameExD' })}
          update={name => update({ ...info, givenName: name }, { ...validation, givenName: true })}
        />
        <GenderField gender={info.gender} update={gender => update({ ...info, gender })} />
      </div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`telephone${ticktock}`}
          header={intl.formatMessage({ id: 'telephone' })}
          placeholder={intl.formatMessage({ id: 'booking.telephoneEx' })}
          text={info.telephone}
          update={telephone => update({ ...info, telephone }, { ...validation, telephone: true })}
          valid={validation.telephone}
          regex={validTelephoneRegex}
        />
        <FreeTextField
          key={`email${ticktock}`}
          header={intl.formatMessage({ id: 'email' })}
          placeholder={intl.formatMessage({ id: 'emailEx' })}
          text={info.email}
          update={email => update({ ...info, email }, { ...validation, email: true })}
          valid={validation.email}
        />
        <FieldDiv />
      </div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          header={intl.formatMessage({ id: 'address' })}
          placeholder={intl.formatMessage({ id: 'booking.addressPlaceholder' })}
          text={info.address}
          update={address => update({ ...info, address })}
          optional
        />
      </div>
    </div>
  );
};

export default injectIntl(ContactInfoForm);
