import MomentUtils from '@date-io/moment';
import { Button, FormControlLabel, IconButton, Radio, RadioGroup, Typography } from '@material-ui/core';
import { InputBaseComponentProps } from '@material-ui/core/InputBase';
import { createMuiTheme } from '@material-ui/core/styles';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider } from '@material-ui/styles';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { MouseEventHandler, useEffect, useMemo, useRef, useState } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MaskedInput from 'react-text-mask';
import styled from 'styled-components';
import { GREY, RED, SECONDARY, PRIMARY } from '../../../colors';
import { some, MY_TOUR } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as CalendarIcon } from '../../../svg/calendar.svg';
import { BootstrapInput, snackbarSetting } from '../../common/components/elements';
import { Gender } from '../../common/models';

export const FreeTextField: React.SFC<{
  text: string;
  placeholder: string;
  header?: React.ReactNode;
  update?: (text: string) => void;
  regex?: RegExp;
  optional?: boolean;
  valid?: boolean;
  type?: string;
  style?: React.CSSProperties;
  inputStyle?: React.CSSProperties;
  endAdornment?: React.ReactNode;
  startAdornment?: React.ReactNode;
  onClick?: MouseEventHandler<some>;
  inputProps?: InputBaseComponentProps;
  disabled?: boolean;
  inputComponent?: any;
  autoComplete?: string;
  autoFocus?: boolean;
  multiline?: boolean;
  readOnly?: boolean;
  rows?: string | number;
}> = props => {
  const {
    text,
    placeholder,
    header,
    update,
    regex,
    optional,
    valid,
    style,
    endAdornment,
    startAdornment,
    inputStyle,
    type,
    onClick,
    inputProps,
    disabled,
    inputComponent,
    autoComplete,
    autoFocus,
    multiline,
    rows,
    readOnly,
  } = props;

  return (
    <FieldDiv style={style}>
      {header && (
        <div style={{ marginLeft: '12px', marginBottom: '4px', color: disabled ? GREY : undefined }}>
          <Typography variant="body2">
            {header}
            &nbsp;
            {!optional && redMark}
          </Typography>
        </div>
      )}
      <BootstrapInput
        disabled={disabled}
        style={inputStyle}
        fullWidth
        value={text}
        error={!valid}
        placeholder={placeholder}
        onChange={e => {
          if ((!regex || regex.test(e.target.value) || !e.target.value) && update) {
            update(e.target.value);
          }
        }}
        readOnly={readOnly}
        type={type || 'text'}
        endAdornment={endAdornment}
        startAdornment={startAdornment}
        inputProps={{
          style: {
            outline: 'none',
            border: 'none',
            position: 'relative',
          },
          ...inputProps,
        }}
        autoComplete={autoComplete}
        onClick={onClick}
        inputComponent={inputComponent}
        autoFocus={autoFocus}
        multiline={multiline}
        rows={rows}
      />
    </FieldDiv>
  );
};
FreeTextField.defaultProps = {
  valid: true,
};
export const NumberTexField = injectIntl(
  (
    props: {
      text?: number;
      placeholder?: string;
      header: React.ReactNode;
      update: (text: number) => void;
      regex?: RegExp;
      optional?: boolean;
      valid?: boolean;
      type?: string;
      style?: React.CSSProperties;
      inputStyle?: React.CSSProperties;
      endAdornment?: React.ReactNode;
      onClick?: MouseEventHandler<some>;
      inputProps?: InputBaseComponentProps;
      disabled?: boolean;
      inputComponent?: any;
    } & WrappedComponentProps,
  ) => {
    const {
      text,
      placeholder,
      header,
      update,
      regex,
      optional,
      valid,
      style,
      endAdornment,
      inputStyle,
      type,
      onClick,
      inputProps,
      disabled,
      inputComponent,
      intl,
    } = props;
    const thousandSep = intl.formatNumber(1111).replace(/1/g, '');
    return (
      <FieldDiv style={style}>
        <div style={{ marginLeft: '12px', marginBottom: '4px', color: disabled ? GREY : undefined }}>
          <Typography variant="body2">
            {header}
            &nbsp;
            {!optional && redMark}
          </Typography>
        </div>
        <BootstrapInput
          disabled={disabled}
          style={inputStyle}
          fullWidth
          value={text ? intl.formatNumber(text) : placeholder ? '' : '0'}
          placeholder={placeholder || ''}
          error={!valid}
          onChange={e => {
            const number = parseInt(e.target.value.split(thousandSep).join('') || '0', 10);
            if (!regex || regex.test(e.target.value) || !e.target.value || Number.isInteger(number)) {
              update(number);
            }
          }}
          type={type || 'text'}
          endAdornment={endAdornment}
          inputProps={{
            style: {
              outline: 'none',
              border: 'none',
              height: '24px',
            },
            ...inputProps,
          }}
          onClick={onClick}
          inputComponent={inputComponent}
        />
      </FieldDiv>
    );
  },
);
NumberTexField.defaultProps = {
  valid: true,
};
export const GenderField = injectIntl(
  (
    props: {
      gender: Gender;
      update: (gender: Gender) => void;
      oneLine?: boolean;
    } & WrappedComponentProps,
  ) => {
    const { gender, update, intl, oneLine } = props;
    return (
      <FieldDiv
        style={
          oneLine
            ? {
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'flex-start',
              }
            : {}
        }
      >
        <div style={{ marginLeft: '12px', marginBottom: '4px' }}>
          <Typography variant="body2">
            <FormattedMessage id="gender" />
            &nbsp;
            {redMark}
          </Typography>
        </div>
        <div style={oneLine ? { marginLeft: '40px' } : {}}>
          <RadioGroup row>
            <FormControlLabel
              value="m"
              control={<Radio size="small" />}
              checked={gender === 'm'}
              onChange={(e, checked) => checked && update('m')}
              label={<Typography variant="body2">{intl.formatMessage({ id: 'male' })}</Typography>}
            />
            <FormControlLabel
              value="f"
              control={<Radio size="small" />}
              checked={gender === 'f'}
              onChange={(e, checked) => checked && update('f')}
              label={<Typography variant="body2">{intl.formatMessage({ id: 'female' })}</Typography>}
            />
          </RadioGroup>
        </div>
      </FieldDiv>
    );
  },
);

export const DateField = connect((state: AppState) => ({ locale: state.intl.locale }))(
  (props: {
    valid: boolean;
    header?: string;
    date: string;
    update: (val: string) => void;
    optional?: boolean;
    inputStyle?: React.CSSProperties;
    disableFuture?: boolean;
    disablePast?: boolean;
    locale: string;
  }) => {
    const { header, valid, date, update, optional, inputStyle, disableFuture, disablePast, locale } = props;
    const prevLocale = useRef(locale);
    const [dateFormat, setDateFormat] = useState(moment.localeData().longDateFormat('L'));
    const { mDate, time } = useMemo(() => {
      const dateObj = moment(date, dateFormat);
      if (dateObj.isValid()) {
        return { mDate: dateObj, time: dateObj.valueOf() };
      }
      return { mDate: null, time: null };
    }, [date, dateFormat]);
    useEffect(() => {
      if (locale !== prevLocale.current) {
        const newDateFormat = moment.localeData().longDateFormat('L');
        setDateFormat(newDateFormat);
        if (time !== null) {
          const newDateStr = moment(time).format(newDateFormat);
          update(newDateStr);
        }
      }
    }, [locale, time, update, date]);
    const [open, setOpen] = useState(false);
    const theme = useMemo(() => createMuiTheme({ palette: { primary: { main: MY_TOUR ? PRIMARY : SECONDARY } } }), []);
    useEffect(() => {
      prevLocale.current = locale;
    }, [locale]);

    return (
      <FieldDiv>
        {header && (
          <div style={{ marginLeft: '12px', marginBottom: '4px' }}>
            <Typography variant="body2">
              {header}
              &nbsp;
              {!optional && redMark}
            </Typography>
          </div>
        )}
        <BootstrapInput
          fullWidth
          error={!valid}
          value={date}
          style={inputStyle}
          placeholder={dateFormat}
          onChange={e => update(e.target.value)}
          inputComponent={DateMaskCustom as any}
          onClick={() => setOpen(true)}
          endAdornment={
            <IconButton size="small" edge="start">
              <CalendarIcon />
            </IconButton>
          }
        />
        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
          <ThemeProvider theme={theme}>
            <DatePicker
              variant="dialog"
              autoOk
              open={open}
              disableFuture={disableFuture}
              disablePast={disablePast}
              maxDate={disableFuture ? moment() : undefined}
              minDate={disablePast ? moment() : undefined}
              openTo="year"
              views={['year', 'month', 'date']}
              value={mDate || undefined}
              onChange={newDate => {
                if (newDate) {
                  update(newDate.format(dateFormat));
                }
              }}
              TextFieldComponent={() => <></>}
              onAccept={() => setOpen(false)}
              onClose={() => setOpen(false)}
              okLabel={<FormattedMessage id="ok" />}
              cancelLabel={<FormattedMessage id="cancel" />}
            />
          </ThemeProvider>
        </MuiPickersUtilsProvider>
      </FieldDiv>
    );
  },
);

interface DateMaskCustomProps {
  inputRef: (ref: HTMLInputElement | null) => void;
  placeholder: string;
}
interface NumberMaskCustomProps {
  inputRef: (ref: HTMLInputElement | null) => void;
  placeholder: string;
  value: string;
}

export const DateMaskCustom = (props: DateMaskCustomProps) => {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
      placeholder={props.placeholder}
      guide={false}
    />
  );
};

export const FieldDiv = styled.div`
  margin-right: 20px;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const redMark = <span style={{ color: RED }}>*</span>;

export const CopyTextField = injectIntl(
  (
    props: {
      text: string;
      header?: React.ReactNode;
      style?: React.CSSProperties;
      inputStyle?: React.CSSProperties;
      message?: React.ReactNode;
    } & WrappedComponentProps,
  ) => {
    const { text, header, style, inputStyle, message } = props;
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    return (
      <FieldDiv style={style}>
        <div>
          {header && (
            <div style={{ marginLeft: '12px', marginBottom: '4px' }}>
              <Typography variant="body2">
                {header}
                &nbsp;
              </Typography>
            </div>
          )}
          <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <BootstrapInput
              disabled
              value={text}
              type="text"
              style={{ width: '240px', marginRight: '20px', backgroundColor: 'white', ...inputStyle }}
            />
            <CopyToClipboard
              text={text}
              onCopy={(textValue: string, v: boolean) => {
                if (textValue && v) {
                  enqueueSnackbar(
                    textValue,
                    snackbarSetting(message || <FormattedMessage id="copied" />, key => closeSnackbar(key)),
                  );
                }
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                style={{ width: '140px', margin: '8px 0' }}
                disableElevation
              >
                <FormattedMessage id="copy" />
              </Button>
            </CopyToClipboard>
          </div>
        </div>
      </FieldDiv>
    );
  },
);
