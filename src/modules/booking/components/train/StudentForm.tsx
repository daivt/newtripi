import * as React from 'react';
import { useIntl } from 'react-intl';
import { StudentInfoValidation, StudentTravellerInfo } from '../../../common/models/train';
import { FieldDiv, FreeTextField } from '../Form';

interface Props {
  update(info: StudentTravellerInfo, validation?: StudentInfoValidation): void;
  info: StudentTravellerInfo;
  validation: StudentInfoValidation;
  ticktock: boolean;
}
const StudentForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName || ''}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'booking.familyNameEx' })}
          update={familyName =>
            update({ ...info, familyName }, { ...validation, familyName: true })
          }
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName || ''}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'booking.givenName' })}
          placeholder={intl.formatMessage({ id: 'booking.givenNameEx' })}
          update={givenName => update({ ...info, givenName }, { ...validation, givenName: true })}
        />{' '}
        <FieldDiv />
      </div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`identity${ticktock}`}
          text={info.identity || ''}
          valid={validation.identity}
          header={intl.formatMessage({ id: 'booking.identity' })}
          placeholder={intl.formatMessage({ id: 'booking.identityEx' })}
          update={identity => update({ ...info, identity }, { ...validation, identity: true })}
        />
        <FieldDiv />
        <FieldDiv />
      </div>
    </div>
  );
};

export default StudentForm;
