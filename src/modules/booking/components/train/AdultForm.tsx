import { FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import * as React from 'react';
import { useIntl } from 'react-intl';
import { AdultInfoValidation, AdultTravellerInfo } from '../../../common/models/train';
import { DateField, FieldDiv, FreeTextField } from '../Form';
import { validateAdultInfoTrain } from '../../utils/train';

interface Props {
  update(info: AdultTravellerInfo, validation?: AdultInfoValidation): void;
  info: AdultTravellerInfo;
  validation: AdultInfoValidation;
  ticktock: boolean;
}
const AdultForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName || ''}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'booking.familyNameEx' })}
          update={familyName =>
            update({ ...info, familyName }, { ...validation, familyName: true })
          }
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName || ''}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'booking.givenName' })}
          placeholder={intl.formatMessage({ id: 'booking.givenNameEx' })}
          update={givenName => update({ ...info, givenName }, { ...validation, givenName: true })}
        />
        <FieldDiv />
      </div>
      <div>
        <RadioGroup row style={{ margin: '16px 0px' }}>
          <FormControlLabel
            value="identity"
            control={<Radio size="small" />}
            checked={info.option === 'identity'}
            onChange={(e, checked) => {
              checked &&
                update(
                  { ...info, option: 'identity' },
                  validateAdultInfoTrain({ ...info, option: 'identity' }),
                );
            }}
            label={
              <Typography variant="body2">
                {intl.formatMessage({ id: 'booking.train.identity' })}
              </Typography>
            }
          />
          <FormControlLabel
            value="birthday"
            control={<Radio />}
            checked={info.option === 'birthday'}
            onChange={(e, checked) => {
              checked &&
                update(
                  { ...info, option: 'birthday' },
                  validateAdultInfoTrain({ ...info, option: 'birthday' }),
                );
            }}
            label={
              <Typography variant="body2">
                {intl.formatMessage({ id: 'booking.birthday' })}
              </Typography>
            }
          />
        </RadioGroup>
        <div style={{ display: 'flex', marginBottom: '12px' }}>
          {info.option === 'identity' && (
            <FreeTextField
              key={`identity${ticktock}`}
              text={info.identity || ' '}
              valid={validation.identity}
              header={intl.formatMessage({ id: 'booking.identity' })}
              placeholder={intl.formatMessage({ id: 'booking.identityEx' })}
              update={identity =>
                update(
                  { ...info, identity, birthday: '' },
                  { ...validation, identity: true, birthday: true },
                )
              }
            />
          )}
          {info.option === 'birthday' && (
            <DateField
              disableFuture={true}
              key={`birthday${ticktock}`}
              header={intl.formatMessage({ id: 'booking.adultTrainBirthday' })}
              date={info.birthday || ''}
              valid={validation.birthday}
              update={birthday =>
                update(
                  { ...info, birthday, identity: '' },
                  { ...validation, birthday: true, identity: true },
                )
              }
            />
          )}
          <FieldDiv />
          <FieldDiv />
        </div>
      </div>
    </div>
  );
};

export default AdultForm;
