import * as React from 'react';
import { useIntl } from 'react-intl';
import { CustomerInfo, CustomerInfoValidation } from '../../../common/models/train';
import { validTelephoneRegex } from '../../../common/utils';
import { FreeTextField } from '../Form';

interface Props {
  update(info: CustomerInfo, validation?: CustomerInfoValidation): void;
  info: CustomerInfo;
  validation: CustomerInfoValidation;
  ticktock: boolean;
}

const CustomerForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`name${ticktock}`}
          text={info.name || ''}
          valid={validation.name}
          header={intl.formatMessage({ id: 'booking.name' })}
          placeholder={intl.formatMessage({ id: 'booking.nameEx' })}
          update={name => update({ ...info, name }, { ...validation, name: true })}
        />
        <FreeTextField
          key={`phone${ticktock}`}
          text={info.phone || ''}
          valid={validation.phone}
          regex={validTelephoneRegex}
          header={intl.formatMessage({ id: 'booking.phone' })}
          placeholder={intl.formatMessage({ id: 'booking.phoneEx' })}
          update={phone => update({ ...info, phone }, { ...validation, phone: true })}
        />
        <FreeTextField
          key={`email${ticktock}`}
          text={info.email || ''}
          valid={validation.email}
          header={intl.formatMessage({ id: 'booking.email' })}
          placeholder={intl.formatMessage({ id: 'booking.emailEx' })}
          update={email => update({ ...info, email }, { ...validation, email: true })}
        />
      </div>
    </div>
  );
};

export default CustomerForm;
