import { Typography } from '@material-ui/core';
import { sumBy } from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, GREY } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconClock } from '../../../../svg/ic_clock.svg';
import { ReactComponent as IconTrain } from '../../../../svg/Train.svg';
import { ReactComponent as IconTrainHead } from '../../../../svg/train_head.svg';
import { Line } from '../../../common/components/elements';
import { Box, SectionHeader } from '../styles';
import { TrainResultItemSideBox } from '../../../result/components/train/TrainResultItemSideBox';
import { TRAIN_ROUND_TRIP_CODE } from '../../constants';

const Point = styled.div`
  width: 6px;
  height: 6px;
  border-radius: 6px;
  margin-left: 6px;
  margin-right: 6px;
  background: black;
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  isReview?: boolean;
}

const TrainBookingInfoSideBox: React.FunctionComponent<Props> = props => {
  const { booking, isReview } = props;
  const reqSearchTrain = booking.holdSeatData && booking.holdSeatData.reqSearchTrain;

  const departureTrip = booking.holdSeatData ? booking.holdSeatData.departureTrip : undefined;
  const seatDepartureTrip = booking.holdSeatData ? booking.holdSeatData.seatDepartureTrip : undefined;
  const departurePrice = seatDepartureTrip
    ? sumBy(seatDepartureTrip, (val: some) => {
        return sumBy(val.seats, 'ticketPrice');
      })
    : 0;
  const returnTrip = booking.holdSeatData ? booking.holdSeatData.returnTrip : undefined;
  const seatReturnTrip = booking.holdSeatData ? booking.holdSeatData.seatReturnTrip : undefined;
  const returnPrice = seatReturnTrip
    ? sumBy(seatReturnTrip, (val: some) => {
        return sumBy(val.seats, 'ticketPrice');
      })
    : 0;

  const getCustomerNumber = React.useCallback(() => {
    let num = 0;
    if (reqSearchTrain) {
      num = reqSearchTrain.adt + reqSearchTrain.chd + reqSearchTrain.old + reqSearchTrain.std;
    }
    return num;
  }, [reqSearchTrain]);
  const getDayCount = React.useCallback((start: number, end: number) => {
    const day = Math.floor(moment.duration(Math.abs(end - start)).asDays());
    return day > 0 ? `(+${day}d)` : '';
  }, []);
  const trainBookingState = booking.ticketParams;
  return (
    <Box>
      <SectionHeader>
        <Typography variant="h5">
          <FormattedMessage id="booking.train.info" />
        </Typography>
      </SectionHeader>
      <Line>
        <IconTrainHead style={{ marginRight: '8px' }} />
        <Typography variant="subtitle2">
          <FormattedMessage id="outbound" /> &nbsp;-&nbsp; {moment(departureTrip.departureDate).format('L')}&nbsp;
        </Typography>
        <Point />
        <Typography variant="subtitle2">
          {getCustomerNumber()}&nbsp;
          <FormattedMessage id="guest" />
        </Typography>
      </Line>
      <div
        style={{
          display: 'flex',
          minHeight: '96px',
          marginTop: '12px',
          padding: '8px 12px 8px 24px',
          border: `1px solid ${GREY}`,
          borderRadius: '4px',
          marginBottom: '8px',
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant="body2">{departureTrip.departureHour}</Typography>
          <div style={{ flex: 1, border: `1px dashed ${GREY}`, width: '1px', margin: 'auto' }}></div>
          <Typography variant="body2">
            {departureTrip.arriveHour}
            {getDayCount(departureTrip.departureDate, departureTrip.arriveDate)}
          </Typography>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '24px' }}>
          <Typography variant="body2">{departureTrip.departureStationName}</Typography>
          <Line style={{ flex: 1 }}>
            <IconClock style={{ marginRight: '4px' }} stroke={BLUE} />
            <Typography variant="body2" style={{ color: BLUE }}>
              {departureTrip.duration}
            </Typography>
          </Line>
          <Typography variant="body2">{departureTrip.arrivedStationName}</Typography>
        </div>

        <Line
          style={{
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          <Line style={{ flexDirection: 'column', alignItems: 'center' }}>
            <IconTrain style={{ height: '36px', width: 'auto', marginBottom: '4px' }} />
            <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
              {departureTrip.tripCode}
            </Typography>
          </Line>
        </Line>
      </div>
      {trainBookingState.outbound && trainBookingState.outbound.train && (
        <TrainResultItemSideBox
          train={trainBookingState.outbound.train}
          seatingInfo={trainBookingState.outboundSeating}
          travellersInfo={booking.guestInfo.travellersInfo}
          isReview={isReview}
        />
      )}

      {returnTrip && reqSearchTrain.tripType === TRAIN_ROUND_TRIP_CODE && (
        <>
          <Line style={{ marginTop: '24px' }}>
            <IconTrainHead style={{ marginRight: '8px' }} />
            <Typography variant="subtitle2">
              <FormattedMessage id="inbound" /> &nbsp;-&nbsp; {moment(returnTrip.departureDate).format('L')}&nbsp;
            </Typography>
            <Point />
            <Typography variant="subtitle2">
              {getCustomerNumber()}&nbsp;
              <FormattedMessage id="guest" />
            </Typography>
          </Line>
          <div
            style={{
              display: 'flex',
              minHeight: '96px',
              marginTop: '12px',
              padding: '8px 12px 8px 24px',
              border: `1px solid ${GREY}`,
              borderRadius: '4px',
              marginBottom: '8px',
            }}
          >
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="body2">{returnTrip.departureHour}</Typography>
              <div style={{ flex: 1, border: `1px dashed ${GREY}`, width: '1px', margin: 'auto' }}></div>
              <Typography variant="body2">
                {returnTrip.arriveHour}
                {getDayCount(returnTrip.departureDate, returnTrip.arriveDate)}
              </Typography>
            </div>
            <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '24px' }}>
              <Typography variant="body2">{returnTrip.departureStationName}</Typography>
              <Line style={{ flex: 1 }}>
                <IconClock style={{ marginRight: '4px' }} stroke={BLUE} />
                <Typography variant="body2" style={{ color: BLUE }}>
                  {returnTrip.duration}
                </Typography>
              </Line>
              <Typography variant="body2">{returnTrip.arrivedStationName}</Typography>
            </div>

            <Line
              style={{
                flexDirection: 'column',
                alignItems: 'flex-end',
                justifyContent: 'center',
                flex: 1,
              }}
            >
              <Line style={{ flexDirection: 'column', alignItems: 'center' }}>
                <IconTrain style={{ height: '36px', width: 'auto', marginBottom: '4px' }} />
                <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                  {returnTrip.tripCode}
                </Typography>
              </Line>
            </Line>
          </div>
        </>
      )}
      {trainBookingState.inbound.train && (
        <TrainResultItemSideBox
          isInbound
          train={trainBookingState.inbound.train}
          seatingInfo={trainBookingState.inboundSeating}
          travellersInfo={booking.guestInfo.travellersInfo}
          isReview={isReview}
        />
      )}
      {returnPrice > 0 && (
        <>
          <Line style={{ justifyContent: 'space-between', marginTop: '42px', minHeight: '36px' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.outboundPrice" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={departurePrice} />
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          <Line style={{ justifyContent: 'space-between', minHeight: '24px' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.inboundPrice" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={returnPrice} />
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </>
      )}
      <Line style={{ justifyContent: 'space-between', minHeight: '36px', marginTop: '16px' }}>
        <Typography variant="body2">
          <FormattedMessage id="ticketAlert.totalPrice" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedNumber value={returnPrice + departurePrice} />
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
    </Box>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};

export default connect(mapStateToProps)(TrainBookingInfoSideBox);
