import { Typography } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { BLUE } from '../../../../colors';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { NewTabLink } from '../../../common/components/NewTabLink';
import PaymentMethodsBox from '../../../common/components/Payment/PaymentMethodsBox';
import { setSelectedPaymentMethod } from '../../redux/trainBookingReducer';
import { Box, SectionHeader } from '../styles';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const TrainPayBox: FunctionComponent<Props> = props => {
  const { booking, dispatch } = props;
  const { bookingData, selectedPaymentMethod, paymentMethods } = booking;
  return (
    <>
      {!paymentMethods && <LoadingIcon style={{ marginTop: '240px' }} />}
      {paymentMethods && selectedPaymentMethod && bookingData && (
        <Box>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.paymentMethod" />
            </Typography>
          </SectionHeader>
          <PaymentMethodsBox
            paymentMethods={paymentMethods}
            total={bookingData.bookData.amount}
            setSelectedMethod={method => dispatch(setSelectedPaymentMethod(method))}
            selectedMethod={selectedPaymentMethod}
            priceAfter={bookingData.bookData.amountPay}
            moduleType="train"
          />
        </Box>
      )}
      <div
        style={{
          padding: '16px 16px 32px 16px',
          color: BLUE,
        }}
      >
        <NewTabLink href={ROUTES.terms}>
          <Typography variant="body2">
            <FormattedMessage id="booking.seePaymentDetail" />
          </Typography>
        </NewTabLink>
      </div>
    </>
  );
};

export default connect(mapState2Props)(TrainPayBox);
