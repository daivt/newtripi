import { Button, Dialog, Typography } from '@material-ui/core';
import moment, { now } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { LIGHT_BLUE } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconChangeTime } from '../../../../svg/change_time.svg';
import { ReactComponent as IconClock } from '../../../../svg/ic_clock.svg';
import { SecondsDown } from '../../../auth/components/SecondsDown';
import { Dispatch } from 'redux';
import { go } from 'connected-react-router';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  timeExpired?: number;
}

const TimeExpiredCountHolding: React.FunctionComponent<Props> = props => {
  const { timeExpired, dispatch, state } = props;
  const currentTime = React.useMemo(() => now(), []);
  const countTime = React.useMemo(
    () =>
      timeExpired
        ? Math.round((timeExpired - currentTime > 0 ? timeExpired - currentTime : 0) / 1000)
        : 0,
    [currentTime, timeExpired],
  );
  const [showExpired, setShow] = React.useState(false);
  const backableToResult = state && state.backableToResult;
  const backableToBookingInfo = state && state.backableToBookingInfo;
  const backableToReview = state && state.backableToReview;
  return (
    <>
      {timeExpired && (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              marginBottom: '16px',
              borderRadius: '4px',
              background: LIGHT_BLUE,
              display: 'flex',
              padding: '8px 20px',
              alignItems: 'center',
              minWidth: '270px',
            }}
          >
            <IconClock style={{ filter: 'brightness(0) invert(1)', marginRight: '12px' }} />
            <Typography variant="body2" style={{ color: 'white' }}>
              <FormattedMessage id="booking.train.hold" />
              &nbsp;
              <SecondsDown
                num={countTime}
                format={seconds => <>{moment.utc(seconds * 1000).format('mm:ss')}</>}
                onCountingCompleted={() => {
                  setShow(true);
                }}
              />
            </Typography>
          </div>
          <div style={{ flex: 1 }} />
        </div>
      )}
      <Dialog
        open={showExpired}
        fullWidth
        maxWidth={'sm'}
        PaperProps={{ style: { padding: '24px 24px 16px 24px', width: '450px' } }}
      >
        <Typography variant="h6">
          <FormattedMessage id="booking.train.expiredTitle" />
        </Typography>
        <IconChangeTime style={{ margin: '24px auto' }} />
        <Typography variant="body2" color="textSecondary">
          <FormattedMessage id="booking.train.expiredNote" />
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: '21px' }}>
          <Button
            variant="contained"
            color="secondary"
            style={{ height: '40px' }}
            onClick={() => {
              if (backableToReview) {
                dispatch(go(-3));
              } else if (backableToBookingInfo) {
                dispatch(go(-2));
              } else if (backableToResult) {
                dispatch(go(-1));
              }
            }}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="booking.train.backToSearch" />
            </Typography>
          </Button>
        </div>
      </Dialog>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return { state: state.router.location.state };
};
export default connect(mapStateToProps)(TimeExpiredCountHolding);
