import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { go } from 'connected-react-router';
import { some } from '../../../../constants';

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  step: number;
}

const TrainBreadcrumbs: React.FunctionComponent<Props> = props => {
  const { state, step } = props;
  const backableToResult = state && state.backableToResult;
  const backableToBookingInfo = state && state.backableToBookingInfo;
  const backableToReview = state && state.backableToReview;
  const LIST_STEP = [
    {
      id: 'booking.search',
      state: false,
    },
    {
      id: 'booking.selectSeat',
      state: backableToResult,
    },
    {
      id: 'booking.travellersInfo',
      state: backableToBookingInfo,
    },
    {
      id: 'booking.reviewInfo',
      state: backableToReview,
    },
    {
      id: 'booking.pay',
      state: false,
    },
  ];
  return (
    <div style={{ padding: '20px 0' }}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        {LIST_STEP.map((v: some, index: number) => (
          <Typography
            key={index}
            variant="body2"
            color={index + 1 === step ? 'secondary' : 'textPrimary'}
            style={{ cursor: v.state ? 'pointer' : undefined }}
            onClick={() => {
              if (v.state && -step + index + 1 < 0) {
                props.dispatch(go(-step + index + 1));
              }
            }}
          >
            {index + 1}. <FormattedMessage id={v.id} />
          </Typography>
        ))}
      </Breadcrumbs>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return { state: state.router.location.state };
}

export default connect(mapStateToProps)(TrainBreadcrumbs);
