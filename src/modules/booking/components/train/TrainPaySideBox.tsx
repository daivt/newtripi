import { Button, Typography } from '@material-ui/core';
import { sumBy } from 'lodash';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { BLUE, GREY } from '../../../../colors';
import { C_DATE_FORMAT, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconClock } from '../../../../svg/ic_clock.svg';
import { ReactComponent as IconTrain } from '../../../../svg/Train.svg';
import { ReactComponent as IconTrainHead } from '../../../../svg/train_head.svg';
import { Line } from '../../../common/components/elements';
import { Box, SectionHeader } from '../styles';
import TrainTicketDetailDialog from './TrainTicketDetailDialog';
import { TRAIN_ROUND_TRIP_CODE } from '../../constants';

const LineRow = styled.div`
  min-height: 36px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Point = styled.div`
  width: 6px;
  height: 6px;
  border-radius: 6px;
  margin-left: 6px;
  margin-right: 6px;
  background: black;
`;

interface Props extends ReturnType<typeof mapStateToProps> {}

const TrainPaySideBox: React.FunctionComponent<Props> = props => {
  const { booking } = props;
  const [openDetail, setOpen] = React.useState(false);
  const reqSearchTrain = booking.holdSeatData && booking.holdSeatData.reqSearchTrain;

  const departureTrip = booking.holdSeatData ? booking.holdSeatData.departureTrip : undefined;

  const departurePrice = booking.bookingData
    ? sumBy(booking.bookingData.passengersInDepartureTrip, 'totalFinalPrice')
    : 0;
  const returnTrip = booking.holdSeatData ? booking.holdSeatData.returnTrip : undefined;
  const returnPrice = booking.bookingData ? sumBy(booking.bookingData.passengersInReturnTrip, 'totalFinalPrice') : 0;
  const getDayCount = React.useCallback((start: number, end: number) => {
    const day = Math.floor(moment.duration(Math.abs(end - start)).asDays());
    return day > 0 ? `(+${day}d)` : '';
  }, []);

  return (
    <Box>
      <SectionHeader style={{ justifyContent: 'space-between' }}>
        <Typography variant="h5">
          <FormattedMessage id="booking.train.info" />
        </Typography>
        <Button variant="text" onClick={() => setOpen(true)} style={{ padding: '0px' }}>
          <Typography variant="body2" style={{ color: BLUE }}>
            <FormattedMessage id="booking.seeDetails" />
          </Typography>
        </Button>
      </SectionHeader>
      <Line>
        <IconTrainHead style={{ marginRight: '8px' }} />
        <Typography variant="subtitle2">
          <FormattedMessage id="outbound" /> &nbsp;-&nbsp; {moment(reqSearchTrain.departureDate).format('L')}&nbsp;
        </Typography>
        <Point />
        <Typography variant="subtitle2">
          {booking.bookingData ? booking.bookingData.passengersInDepartureTrip.length : 0}&nbsp;
          <FormattedMessage id="guest" />
        </Typography>
      </Line>
      <div
        style={{
          display: 'flex',
          minHeight: '96px',
          marginTop: '12px',
          padding: '8px 12px 8px 24px',
          border: `1px solid ${GREY}`,
          borderRadius: '4px',
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant="body2">{departureTrip.departureHour}</Typography>
          <div style={{ flex: 1, border: `1px dashed ${GREY}`, width: '1px', margin: 'auto' }}></div>
          <Typography variant="body2">
            {departureTrip.arriveHour}
            {getDayCount(departureTrip.arriveDate, departureTrip.departureDate)}
          </Typography>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '24px' }}>
          <Typography variant="body2">{departureTrip.departureStationName}</Typography>
          <Line style={{ flex: 1 }}>
            <IconClock style={{ marginRight: '4px' }} stroke={BLUE} />
            <Typography variant="body2" style={{ color: BLUE }}>
              {departureTrip.duration}
            </Typography>
          </Line>
          <Typography variant="body2">{departureTrip.arrivedStationName}</Typography>
        </div>

        <Line
          style={{
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          <Line style={{ flexDirection: 'column', alignItems: 'center' }}>
            <IconTrain style={{ height: '36px', width: 'auto', marginBottom: '8px' }} />
            <Typography variant="subtitle2">{departureTrip.tripCode}</Typography>
          </Line>
        </Line>
      </div>

      {returnTrip && reqSearchTrain.tripType === TRAIN_ROUND_TRIP_CODE && (
        <>
          <Line style={{ marginTop: 12 }}>
            <IconTrainHead style={{ marginRight: '8px' }} />
            <Typography variant="subtitle2">
              <FormattedMessage id="inbound" /> &nbsp;-&nbsp;
              {moment(reqSearchTrain.returnDate).format('L')}&nbsp;
            </Typography>
            <Point />
            <Typography variant="subtitle2">
              {booking.bookingData ? booking.bookingData.passengersInReturnTrip.length : 0}&nbsp;
              <FormattedMessage id="guest" />
            </Typography>
          </Line>
          <div
            style={{
              display: 'flex',
              minHeight: '96px',
              marginTop: '12px',
              padding: '8px 12px 8px 24px',
              border: `1px solid ${GREY}`,
              borderRadius: '4px',
            }}
          >
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="body2">{returnTrip.departureHour}</Typography>
              <div style={{ flex: 1, border: `1px dashed ${GREY}`, width: '1px', margin: 'auto' }}></div>
              <Typography variant="body2">
                {returnTrip.arriveHour}
                {getDayCount(returnTrip.arriveDate, returnTrip.departureDate)}
              </Typography>
            </div>
            <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '24px' }}>
              <Typography variant="body2">{returnTrip.departureStationName}</Typography>
              <Line style={{ flex: 1 }}>
                <IconClock style={{ marginRight: '4px' }} stroke={BLUE} />
                <Typography variant="body2" style={{ color: BLUE }}>
                  {returnTrip.duration}
                </Typography>
              </Line>
              <Typography variant="body2">{returnTrip.arrivedStationName}</Typography>
            </div>

            <Line
              style={{
                flexDirection: 'column',
                alignItems: 'flex-end',
                justifyContent: 'center',
                flex: 1,
              }}
            >
              <Line style={{ flexDirection: 'column', alignItems: 'center' }}>
                <IconTrain style={{ height: '42px', width: 'auto', marginBottom: '8px' }} />
                <Typography variant="subtitle2">{returnTrip.tripCode}</Typography>
              </Line>
            </Line>
          </div>
        </>
      )}
      <Line style={{ marginTop: '24px', marginBottom: '4px' }}>
        <Typography variant="h6">
          <FormattedMessage id="booking.clientInfo" />
        </Typography>
      </Line>
      {booking.bookingData &&
        booking.bookingData.passengersInDepartureTrip.map((v: some, index: number) => (
          <LineRow key={index}>
            <Typography variant="body2">{v.customerName}</Typography>
            <Typography variant="body2">
              {v.birthDay ? moment(v.birthDay, C_DATE_FORMAT, true).format('L') : v.idCardNumber}
            </Typography>
          </LineRow>
        ))}
      <LineRow style={{ marginTop: '24px', marginBottom: '4px' }}>
        <Typography variant="h6">
          <FormattedMessage id="totalPrice" />
        </Typography>
      </LineRow>
      {returnPrice > 0 && (
        <>
          <LineRow style={{ justifyContent: 'space-between' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.outboundPrice" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={departurePrice} />
              <FormattedMessage id="currency" />
            </Typography>
          </LineRow>
          <LineRow style={{ justifyContent: 'space-between', marginTop: '8px' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.inboundPrice" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={returnPrice} />
              <FormattedMessage id="currency" />
            </Typography>
          </LineRow>
        </>
      )}
      <LineRow style={{ justifyContent: 'space-between', marginTop: '8px' }}>
        <Typography variant="body2">
          <FormattedMessage id="ticketAlert.totalPrice" />
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedNumber value={returnPrice + departurePrice} />
          <FormattedMessage id="currency" />
        </Typography>
      </LineRow>

      {booking.bookingData && (
        <TrainTicketDetailDialog
          open={openDetail}
          onClose={() => setOpen(false)}
          ticketInfo={booking.bookingData.trains}
          departureData={booking.bookingData.passengersInDepartureTrip}
          returnData={booking.bookingData.passengersInReturnTrip}
        />
      )}
    </Box>
  );
};

const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};

export default connect(mapStateToProps)(TrainPaySideBox);
