import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import LoadingIcon from '../../../common/components/LoadingIcon';
import {
  ContactInfo,
  ContactInfoValidation,
  CustomerInfo,
  CustomerInfoValidation,
  ExportBillInfo,
  ExportBillInfoValidation,
  TravellersInfo,
  TravellersInfoValidation,
} from '../../../common/models/train';
import { Wrapper } from '../styles';
import TimeCountHolding from './TimeCountHolding';
import TrainBookingInfoBox from './TrainBookingInfoBox';
import TrainBookingInfoSideBox from './TrainBookingInfoSideBox';
import TrainBreadcrumbs from './TrainBreadcrumbs';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};

export interface Props extends ReturnType<typeof mapState2Props> {
  ticktock: boolean;
  contactInfo: ContactInfo;
  customerInfo: CustomerInfo;
  travellersInfo: TravellersInfo;
  exportBillInfo?: ExportBillInfo;
  customerInfoValidation: CustomerInfoValidation;
  contactInfoValidation: ContactInfoValidation;
  travellersInfoValidation: TravellersInfoValidation;
  exportBillInfoValidation: ExportBillInfoValidation;
  updateCustomerInfo(info: CustomerInfo): void;
  updateContactInfo(info: ContactInfo): void;
  updateExportBillInfo(info: ExportBillInfo | undefined): void;
  updateTravellersInfo(info: TravellersInfo): void;
  updateCustomerInfoValidation(validation: CustomerInfoValidation): void;
  updateContactInfoValidation(validation: ContactInfoValidation): void;
  updateExportBillInfoValidation(validation: ExportBillInfoValidation): void;
  updateTravellersInfoValidation(validation: TravellersInfoValidation): void;
  continue_(): void;
}

const TrainBookingInfoDesktop: React.FunctionComponent<Props> = props => {
  const { booking } = props;
  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <TrainBreadcrumbs step={3} />
        <Typography variant="h5" style={{ marginBottom: '16px' }}>
          <FormattedMessage id="booking.clientInfo" />
        </Typography>
        <TimeCountHolding timeExpired={booking.timeCountHolding} />
        <div style={{ display: 'flex' }}>
          {booking.holdSeatData ? (
            <>
              <div style={{ flex: 1 }}>
                <TrainBookingInfoBox {...props} />
              </div>
              <div style={{ width: '370px', marginLeft: '30px', marginBottom: '102px' }}>
                <div style={{ position: 'sticky', top: 80 }}>
                  <TrainBookingInfoSideBox />
                </div>
              </div>
            </>
          ) : (
            <Wrapper>
              <LoadingIcon
                style={{
                  height: '300px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              />
            </Wrapper>
          )}
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(TrainBookingInfoDesktop);
