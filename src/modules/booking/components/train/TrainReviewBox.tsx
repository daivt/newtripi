import { Button, Typography } from '@material-ui/core';
import { goBack } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { Action } from 'typesafe-actions';
import { BLUE, GREY } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import LoadingButton from '../../../common/components/LoadingButton';
import MessageDialog from '../../../common/components/MessageDialog';
import { REASON } from '../../../common/components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../../../common/constants';
import {
  AdultTravellerInfo,
  ChildTravellerInfo,
  ContactInfo,
  CustomerInfo,
  ExportBillInfo,
  SeniorTravellerInfo,
  StudentTravellerInfo,
  TravellersInfo,
} from '../../../common/models/train';
import { bookTicketTrain, setErrorMessage } from '../../redux/trainBookingReducer';
import { Box, SectionHeader } from '../styles';
const Line = styled.tr`
  height: 56px;
`;
const Line2 = styled.tr`
  height: 32px;
`;
const Line3 = styled.tr`
  height: 40px;
`;

function renderTravellersInfo(data: TravellersInfo) {
  return (
    <>
      <table cellSpacing={0} style={{ width: '100%', borderCollapse: 'collapse' }}>
        <tbody>
          {data.adults.map((val: AdultTravellerInfo, index: number) => (
            <Line
              key={index}
              style={{
                borderTop: index > 0 ? `1px solid ${GREY}` : undefined,
              }}
            >
              <td style={{ width: '184px' }}>
                <Typography variant="subtitle2">
                  {index + 1}.&nbsp;
                  <FormattedMessage id="booking.adult" values={{ num: index + 1 }} />
                </Typography>
              </td>
              <td>
                <Typography variant="body2">
                  <FormattedMessage id="fullName" /> :
                </Typography>
              </td>
              <td>
                <Typography variant="body2" style={{ fontWeight: 500 }}>
                  {val.familyName}&nbsp;{val.givenName}
                </Typography>
              </td>
              <td>
                <Typography variant="body2">
                  {val.option === 'identity' ? (
                    <FormattedMessage id="booking.identity" />
                  ) : (
                    <FormattedMessage id="booking.birthday" />
                  )}
                </Typography>
              </td>
              <td>
                <Typography variant="body2" style={{ fontWeight: 500 }}>
                  {val.option === 'identity' ? val.identity : val.birthday}
                </Typography>
              </td>
            </Line>
          ))}
          {data.students.map((val: StudentTravellerInfo, i: number) => {
            const index = i + data.adults.length;
            return (
              <Line
                key={index}
                style={{
                  borderTop: `1px solid ${GREY}`,
                }}
              >
                <td>
                  <Typography variant="subtitle2">
                    {index + 1}.&nbsp;
                    <FormattedMessage id="booking.student" values={{ num: i + 1 }} />
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="fullName" /> :
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.familyName}&nbsp;{val.givenName}
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="booking.identity" />:
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.identity}
                  </Typography>
                </td>
              </Line>
            );
          })}
          {data.childrens.map((val: ChildTravellerInfo, i: number) => {
            const index = i + data.adults.length + data.students.length;
            return (
              <Line
                key={index}
                style={{
                  borderTop: `1px solid ${GREY}`,
                }}
              >
                <td>
                  <Typography variant="subtitle2">
                    {index + 1}.&nbsp;
                    <FormattedMessage id="booking.child" values={{ num: i + 1 }} />
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="fullName" /> :
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.familyName}&nbsp;{val.givenName}
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="birthday" />:
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.birthday}
                  </Typography>
                </td>
              </Line>
            );
          })}
          {data.seniors.map((val: SeniorTravellerInfo, i: number) => {
            const index = i + data.adults.length + data.students.length + data.childrens.length;
            return (
              <Line
                key={index}
                style={{
                  borderTop: `1px solid ${GREY}`,
                }}
              >
                <td>
                  <Typography variant="subtitle2">
                    {index + 1}.&nbsp;
                    <FormattedMessage id="booking.senior" values={{ num: i + 1 }} />
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="fullName" /> :
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.familyName}&nbsp;{val.givenName}
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2">
                    <FormattedMessage id="birthday" />:
                  </Typography>
                </td>
                <td>
                  <Typography variant="body2" style={{ fontWeight: 500 }}>
                    {val.birthday}
                  </Typography>
                </td>
              </Line>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
export function renderCustomerInfo(data: CustomerInfo) {
  return (
    <table cellSpacing={0} style={{ minWidth: '300px', borderCollapse: 'collapse' }}>
      <tbody>
        <Line2>
          <td style={{ width: '184px' }}>
            <Typography variant="body2">
              <FormattedMessage id="fullName" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.name}
            </Typography>
          </td>
        </Line2>
        <Line2>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="telephone" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" color="textSecondary">
              {data.phone}
            </Typography>
          </td>
        </Line2>
        <Line2>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="email" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.email}
            </Typography>
          </td>
        </Line2>
      </tbody>
    </table>
  );
}
function renderContactInfo(data: ContactInfo) {
  return (
    <table cellSpacing={0} style={{ minWidth: '100%', borderCollapse: 'collapse' }}>
      <tbody>
        <Line3>
          <td style={{ width: '184px' }}>
            <Typography variant="body2">
              <FormattedMessage id="fullName" />:
            </Typography>
          </td>
          <td style={{ minWidth: '180px' }}>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.familyName}&nbsp;{data.givenName}
            </Typography>
          </td>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="booking.identity" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.identity}
            </Typography>
          </td>
        </Line3>
        <Line3>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="telephone" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.phone}
            </Typography>
          </td>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="email" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ color: BLUE }}>
              {data.email}
            </Typography>
          </td>
        </Line3>
      </tbody>
    </table>
  );
}
function renderExportBillInfo(data: ExportBillInfo) {
  return (
    <table cellSpacing={0} style={{ minWidth: '250px', borderCollapse: 'collapse' }}>
      <tbody>
        <Line3>
          <td style={{ width: '184px' }}>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.companyName" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.company}
            </Typography>
          </td>
        </Line3>
        <Line3>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="booking.train.taxNumber" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.taxNumber}
            </Typography>
          </td>
        </Line3>
        <Line3>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="address" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.address}
            </Typography>
          </td>
        </Line3>
        <Line3>
          <td>
            <Typography variant="body2">
              <FormattedMessage id="telephone" />:
            </Typography>
          </td>
          <td>
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {data.phone}
            </Typography>
          </td>
        </Line3>
      </tbody>
    </table>
  );
}
const mapStateToProps = (state: AppState) => {
  return {
    booking: state.booking.train,
    router: state.router,
  };
};
export interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {}

const TrainReviewBox: React.FunctionComponent<Props> = props => {
  const { booking, dispatch } = props;
  const { contactInfo, customerInfo, exportBillInfo, travellersInfo } = booking.guestInfo;
  if (!contactInfo || !customerInfo || !travellersInfo || !booking.holdSeatData) {
    return (
      <Redirect
        to={{
          pathname: '/',
          search: `?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.noDataInSession)}`,
        }}
      ></Redirect>
    );
  }
  return (
    <>
      <Box style={{ paddingLeft: '16px' }}>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.bookerInfo" />
          </Typography>
        </SectionHeader>
        {renderCustomerInfo(customerInfo)}
      </Box>
      <Box style={{ paddingLeft: '16px', paddingBottom: '0px' }}>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="booking.travellersInfo" />
          </Typography>
        </SectionHeader>
        {renderTravellersInfo(travellersInfo)}
      </Box>
      <Box style={{ paddingLeft: '16px' }}>
        <SectionHeader>
          <Typography variant="h6">
            <FormattedMessage id="contactInfo" />
          </Typography>
        </SectionHeader>
        {renderContactInfo(contactInfo)}
      </Box>
      {exportBillInfo && (
        <Box style={{ paddingLeft: '16px' }}>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="contactInfo" />
            </Typography>
          </SectionHeader>
          {renderExportBillInfo(exportBillInfo)}
        </Box>
      )}
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: '32px',
        }}
      >
        <Button
          variant="outlined"
          style={{ marginRight: '16px', minWidth: '144px', height: '40px' }}
          onClick={() => {
            dispatch(goBack());
          }}
        >
          <FormattedMessage id="back" />
        </Button>

        <LoadingButton
          style={{ marginRight: '16px', minWidth: '144px', height: '40px' }}
          variant="contained"
          color="secondary"
          size="large"
          loading={booking.loading}
          onClick={() => {
            dispatch(bookTicketTrain());
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="pay" />
          </Typography>
        </LoadingButton>
      </div>
      <MessageDialog
        show={!!booking.errorMessage}
        message={
          <Typography variant="body1" style={{ padding: '0px 16px', textAlign: 'center' }}>
            {booking.errorMessage}
          </Typography>
        }
        onClose={() => {
          dispatch(setErrorMessage());
          dispatch(goBack());
        }}
      />
    </>
  );
};

export default connect(mapStateToProps)(TrainReviewBox);
