import * as React from 'react';
import { useIntl } from 'react-intl';
import { SeniorInfoValidation, SeniorTravellerInfo } from '../../../common/models/train';
import { DateField, FieldDiv, FreeTextField } from '../Form';

interface Props {
  update(info: SeniorTravellerInfo, validation?: SeniorInfoValidation): void;
  info: SeniorTravellerInfo;
  validation: SeniorInfoValidation;
  ticktock: boolean;
}
const SeniorForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName || ''}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'booking.familyNameEx' })}
          update={familyName =>
            update({ ...info, familyName }, { ...validation, familyName: true })
          }
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName || ''}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'booking.givenName' })}
          placeholder={intl.formatMessage({ id: 'booking.givenNameEx' })}
          update={givenName => update({ ...info, givenName }, { ...validation, givenName: true })}
        />{' '}
        <FieldDiv />
      </div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <DateField
          disableFuture={true}
          key={`birthday${ticktock}`}
          header={intl.formatMessage({ id: 'booking.seniorBirthday' })}
          date={info.birthday}
          valid={validation.birthday}
          update={birthday => update({ ...info, birthday }, { ...validation, birthday: true })}
        />
        <FieldDiv />
        <FieldDiv />
      </div>
    </div>
  );
};

export default SeniorForm;
