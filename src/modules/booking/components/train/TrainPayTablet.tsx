import { Container, IconButton, Slide, Typography } from '@material-ui/core';
import Open from '@material-ui/icons/ChevronLeft';
import Close from '@material-ui/icons/ChevronRight';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BACKGROUND, DARK_GREY, GREY } from '../../../../colors';
import { TABLET_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import TimeCountHolding from './TimeCountHolding';
import TrainBreadcrumbs from './TrainBreadcrumbs';
import TrainPayBox from './TrainPayBox';
import TrainPaySideBox from './TrainPaySideBox';

interface Props extends ReturnType<typeof mapStateToProps> {}
const TrainPayTablet: React.FunctionComponent<Props> = props => {
  const { time } = props;
  const [showInfoBox, setShow] = React.useState(false);
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <div style={{ display: 'flex', overflow: 'hidden' }}>
          <div style={{ flex: 1 }}>
            <TrainBreadcrumbs step={5} />
            <Typography variant="h5" style={{ marginBottom: '16px' }}>
              <FormattedMessage id="booking.clientInfo" />
            </Typography>
            <TimeCountHolding timeExpired={time} />
            <TrainPayBox />
          </div>
          <div
            style={{
              position: 'relative',
              direction: 'rtl',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {!showInfoBox && (
              <IconButton
                style={{ margin: '10px', position: 'absolute' }}
                onClick={() => setShow(true)}
              >
                <Open style={{ color: DARK_GREY }} />
              </IconButton>
            )}
            <div style={{ width: 1, flex: 1, display: 'flex' }}>
              <Slide in={showInfoBox} direction="left">
                <div
                  style={{
                    backgroundColor: BACKGROUND,
                    padding: '22px 0 22px 10px',
                    marginTop: '-22px',
                    borderLeft: `1px solid ${GREY}`,
                    width: '420px',
                  }}
                >
                  <div style={{ textAlign: 'end' }}>
                    <IconButton style={{ margin: '10px 0' }} onClick={() => setShow(false)}>
                      <Close style={{ color: DARK_GREY }} />
                    </IconButton>
                  </div>
                  <div style={{ direction: 'ltr' }}>
                    <div style={{ width: '370px', marginRight: '4px' }}>
                      <TrainPaySideBox />
                    </div>
                  </div>
                </div>
              </Slide>
            </div>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    time: state.booking.train.timeCountBooking,
  };
};
export default connect(mapStateToProps)(TrainPayTablet);
