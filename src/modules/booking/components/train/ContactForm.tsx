import * as React from 'react';
import { useIntl } from 'react-intl';
import { ContactInfo, ContactInfoValidation } from '../../../common/models/train';
import { validTelephoneRegex } from '../../../common/utils';
import { FieldDiv, FreeTextField } from '../Form';

interface Props {
  update(info: ContactInfo, validation?: ContactInfoValidation): void;
  info: ContactInfo;
  validation: ContactInfoValidation;
  ticktock: boolean;
}

const ContactForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`familyName${ticktock}`}
          text={info.familyName || ''}
          valid={validation.familyName}
          header={intl.formatMessage({ id: 'booking.familyName' })}
          placeholder={intl.formatMessage({ id: 'booking.familyNameEx' })}
          update={familyName => update({ ...info, familyName }, { ...validation, familyName: true })}
        />
        <FreeTextField
          key={`givenName${ticktock}`}
          text={info.givenName || ''}
          valid={validation.givenName}
          header={intl.formatMessage({ id: 'booking.givenName' })}
          placeholder={intl.formatMessage({ id: 'booking.givenNameEx' })}
          update={givenName => update({ ...info, givenName }, { ...validation, givenName: true })}
        />{' '}
        <FieldDiv />
      </div>
      <div style={{ display: 'flex', marginBottom: '12px' }}>
        <FreeTextField
          key={`identity${ticktock}`}
          text={info.identity || ''}
          valid={validation.identity}
          header={intl.formatMessage({ id: 'booking.identity' })}
          placeholder={intl.formatMessage({ id: 'booking.identityEx' })}
          update={identity => update({ ...info, identity }, { ...validation, identity: true })}
        />
        <FreeTextField
          key={`phone${ticktock}`}
          text={info.phone || ''}
          valid={validation.phone}
          regex={validTelephoneRegex}
          header={intl.formatMessage({ id: 'booking.phone' })}
          placeholder={intl.formatMessage({ id: 'booking.phoneEx' })}
          update={phone => update({ ...info, phone }, { ...validation, phone: true })}
          inputProps={{
            maxLength: 13,
          }}
        />
        <FreeTextField
          key={`email${ticktock}`}
          text={info.email || ''}
          valid={validation.email}
          header={intl.formatMessage({ id: 'booking.email' })}
          placeholder={intl.formatMessage({ id: 'booking.emailEx' })}
          update={email => update({ ...info, email }, { ...validation, email: true })}
        />
      </div>
    </div>
  );
};

export default ContactForm;
