import { Container, Typography } from '@material-ui/core';
import React, { FunctionComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import { SectionHeader } from '../styles';
import TimeCountHolding from './TimeCountHolding';
import TrainBreadcrumbs from './TrainBreadcrumbs';
import TrainPayBox from './TrainPayBox';
import TrainPaySideBox from './TrainPaySideBox';

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.train,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  dispatch: Dispatch;
}

const TourPayDesktop: FunctionComponent<Props> = props => {
  const { booking } = props;

  return (
    <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
      <Header light />
      <Container style={{ flex: 1 }}>
        <TrainBreadcrumbs step={5} />
        <SectionHeader>
          <Typography variant="h5">
            <FormattedMessage id="booking.pay" />
          </Typography>
        </SectionHeader>
        <TimeCountHolding timeExpired={booking.timeCountBooking} />
        <div style={{ display: 'flex' }}>
          <div style={{ flex: 1 }}>
            <TrainPayBox />
          </div>
          <div style={{ width: '370px', marginLeft: '30px', marginBottom: '102px' }}>
            <StickyDiv style={{ top: 80 }}>
              <TrainPaySideBox />
            </StickyDiv>
          </div>
        </div>
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default connect(mapState2Props)(TourPayDesktop);
