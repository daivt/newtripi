import { Checkbox, FormControlLabel, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { ExportBillInfo, ExportBillInfoValidation, validExportBillInfoValidation } from '../../../common/models/train';
import { validTelephoneRegex } from '../../../common/utils';
import { exportBillDefault } from '../../utils/train';
import { FieldDiv, FreeTextField } from '../Form';

interface Props {
  update(info: ExportBillInfo | undefined, validation?: ExportBillInfoValidation): void;
  info?: ExportBillInfo;
  validation: ExportBillInfoValidation;
  ticktock: boolean;
}

const ExportBillForm: React.FunctionComponent<Props> = props => {
  const { info, update, validation, ticktock } = props;
  const intl = useIntl();
  return (
    <div>
      <FormControlLabel
        label={
          <Typography variant="body2" style={{ fontWeight: 'bold' }}>
            <FormattedMessage id="booking.train.exportBill" />
          </Typography>
        }
        // disabled={!booking.insurancePackage}
        style={{ padding: '12px' }}
        labelPlacement="end"
        // value={booking.buyInsurance}
        onChange={(e, value) =>
          info
            ? update(undefined, validExportBillInfoValidation)
            : update(exportBillDefault, validExportBillInfoValidation)
        }
        control={<Checkbox style={{ padding: '0px', marginRight: '8px' }} color="secondary" checked={!!info} />}
      />
      {info && (
        <>
          <div style={{ display: 'flex', marginBottom: '12px' }}>
            <FreeTextField
              key={`company${ticktock}`}
              text={info.company || ''}
              valid={validation.company}
              header={intl.formatMessage({ id: 'booking.train.companyName' })}
              placeholder={intl.formatMessage({ id: 'booking.train.companyNameEx' })}
              update={company => update({ ...info, company }, { ...validation, company: true })}
            />
            <FreeTextField
              key={`taxNumber${ticktock}`}
              text={info.taxNumber || ''}
              valid={validation.taxNumber}
              regex={validTelephoneRegex}
              header={intl.formatMessage({ id: 'booking.train.taxNumber' })}
              placeholder={intl.formatMessage({ id: 'booking.train.taxNumberEx' })}
              update={taxNumber => update({ ...info, taxNumber }, { ...validation, taxNumber: true })}
              inputProps={{
                maxLength: 20,
              }}
            />
            <FreeTextField
              key={`address${ticktock}`}
              text={info.address || ''}
              valid={validation.address}
              header={intl.formatMessage({ id: 'booking.train.companyAddress' })}
              placeholder={intl.formatMessage({ id: 'booking.train.companyAddressEx' })}
              update={address => update({ ...info, address }, { ...validation, address: true })}
            />
          </div>
          <div style={{ display: 'flex', marginBottom: '12px' }}>
            <FreeTextField
              key={`phone${ticktock}`}
              text={info.phone || ''}
              valid={validation.phone}
              regex={validTelephoneRegex}
              header={intl.formatMessage({ id: 'booking.phone' })}
              placeholder={intl.formatMessage({ id: 'booking.phoneEx' })}
              update={phone => update({ ...info, phone }, { ...validation, phone: true })}
            />
            <FieldDiv /> <FieldDiv />
          </div>
        </>
      )}
    </div>
  );
};

export default ExportBillForm;
