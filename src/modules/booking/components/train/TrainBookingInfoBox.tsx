import { Button, ButtonBase, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BLUE } from '../../../../colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconContactList } from '../../../../svg/ic_contact_list.svg';
import { ReactComponent as IconWarning } from '../../../../svg/warning.svg';
import { Line } from '../../../common/components/elements';
import SelectContactDialog from '../../../common/components/SelectContactDialog';
import {
  AdultInfoValidation,
  AdultTravellerInfo,
  ChildInfoValidation,
  ChildTravellerInfo,
  ContactInfo,
  ContactInfoValidation,
  CustomerInfo,
  CustomerInfoValidation,
  ExportBillInfo,
  ExportBillInfoValidation,
  SeniorInfoValidation,
  SeniorTravellerInfo,
  StudentInfoValidation,
  StudentTravellerInfo,
  TravellersInfo,
  TravellersInfoValidation,
} from '../../../common/models/train';
import { BookingInfoBoxTrainType } from '../../constants';
import {
  transformAdultInfoTrain,
  transformChildInfoTrain,
  transformContactInfoTrain,
  transformCustomerInfoTrain,
  transformSeniorInfoTrain,
  transformStudentInfoTrain,
  validateAdultInfoTrain,
  validateChildInfoTrain,
  validateContactInfoTrain,
  validateCustomerInfoTrain,
  validateSeniorInfoTrain,
  validateStudentInfoTrain,
  validCustomerInfoTrain,
} from '../../utils/train';
import { AddContact, Box, SectionHeader } from '../styles';
import AdultForm from './AdultForm';
import ChildrenForm from './ChildrenForm';
import ContactForm from './ContactForm';
import ExportBillForm from './ExportBillForm';
import OlderForm from './SeniorForm';
import StudentForm from './StudentForm';
import { FieldHeader } from './TrainBookingInfo.styles';
import { renderCustomerInfo } from './TrainReviewBox';

const CONTACT_TYPE = {
  CUSTOMER_INFO: 'CUSTOMER_INFO',
  CONTACT_INFO: 'CONTACT_INFO',
  ADULT_INFO: 'ADULT_INFO',
  STUDENT_INFO: 'STUDENT_INFO',
  CHILD_INFO: 'CHILD_INFO',
  OLDER_INFO: 'OLDER_INFO',
};

const mapState2Props = (state: AppState) => {
  return {
    booking: state.booking.train,
    userData: state.account.userData,
  };
};

interface Props extends ReturnType<typeof mapState2Props> {
  ticktock: boolean;
  dispatch: Dispatch;
  contactInfo: ContactInfo;
  customerInfo: CustomerInfo;
  travellersInfo: TravellersInfo;
  exportBillInfo?: ExportBillInfo;
  customerInfoValidation: CustomerInfoValidation;
  contactInfoValidation: ContactInfoValidation;
  travellersInfoValidation: TravellersInfoValidation;
  exportBillInfoValidation: ExportBillInfoValidation;
  updateCustomerInfo(info: CustomerInfo): void;
  updateContactInfo(info: ContactInfo): void;
  updateExportBillInfo(info: ExportBillInfo | undefined): void;
  updateTravellersInfo(info: TravellersInfo): void;
  updateCustomerInfoValidation(validation: CustomerInfoValidation): void;
  updateContactInfoValidation(validation: ContactInfoValidation): void;
  updateExportBillInfoValidation(validation: ExportBillInfoValidation): void;
  updateTravellersInfoValidation(validation: TravellersInfoValidation): void;
  continue_(): void;
}

interface State {
  isSelectContact: boolean;
  selectContactIndex: number;
  selectContactType?: string;
}

class TrainBookingInfoBox extends React.PureComponent<Props, State> {
  state: State = {
    isSelectContact: false,
    selectContactIndex: 0,
  };

  onSelectContact(contact: some) {
    const { selectContactType, selectContactIndex } = this.state;
    const {
      travellersInfo,
      travellersInfoValidation,
      updateCustomerInfo,
      updateContactInfo,
      updateTravellersInfo,
      updateCustomerInfoValidation,
      updateContactInfoValidation,
      updateTravellersInfoValidation,
    } = this.props;

    if (selectContactType === CONTACT_TYPE.CUSTOMER_INFO) {
      const customer: CustomerInfo = transformCustomerInfoTrain(contact);
      updateCustomerInfo(customer);
      updateCustomerInfoValidation(validateCustomerInfoTrain(customer));
      this.setState({ isSelectContact: false });
      return;
    }
    if (selectContactType === CONTACT_TYPE.CONTACT_INFO) {
      const contactData: ContactInfo = transformContactInfoTrain(contact);
      updateContactInfo(contactData);
      updateContactInfoValidation(validateContactInfoTrain(contactData));
      this.setState({ isSelectContact: false });
      return;
    }
    if (selectContactType === CONTACT_TYPE.ADULT_INFO) {
      const info = travellersInfo.adults[selectContactIndex];
      const adult: AdultTravellerInfo = { ...info, ...transformAdultInfoTrain(contact) };
      const travellers = {
        ...travellersInfo,
        adults: [
          ...travellersInfo.adults.slice(0, selectContactIndex),
          adult,
          ...travellersInfo.adults.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfo(travellers);
      const validateTravellers = {
        ...travellersInfoValidation,
        adults: [
          ...travellersInfoValidation.adults.slice(0, selectContactIndex),
          validateAdultInfoTrain(adult),
          ...travellersInfoValidation.adults.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfoValidation(validateTravellers);
      this.setState({ isSelectContact: false });
      return;
    }
    if (selectContactType === CONTACT_TYPE.STUDENT_INFO) {
      const student: StudentTravellerInfo = transformStudentInfoTrain(contact);
      const travellers = {
        ...travellersInfo,
        students: [
          ...travellersInfo.students.slice(0, selectContactIndex),
          student,
          ...travellersInfo.students.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfo(travellers);
      const validateTravellers = {
        ...travellersInfoValidation,
        students: [
          ...travellersInfoValidation.students.slice(0, selectContactIndex),
          validateStudentInfoTrain(student),
          ...travellersInfoValidation.students.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfoValidation(validateTravellers);
      this.setState({ isSelectContact: false });
      return;
    }
    if (selectContactType === CONTACT_TYPE.CHILD_INFO) {
      const child: ChildTravellerInfo = transformChildInfoTrain(contact);
      const travellers = {
        ...travellersInfo,
        childrens: [
          ...travellersInfo.childrens.slice(0, selectContactIndex),
          child,
          ...travellersInfo.childrens.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfo(travellers);
      const validateTravellers = {
        ...travellersInfoValidation,
        childrens: [
          ...travellersInfoValidation.childrens.slice(0, selectContactIndex),
          validateChildInfoTrain(child),
          ...travellersInfoValidation.childrens.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfoValidation(validateTravellers);
      this.setState({ isSelectContact: false });
      return;
    }
    if (selectContactType === CONTACT_TYPE.OLDER_INFO) {
      const senior: ChildTravellerInfo = transformSeniorInfoTrain(contact);
      const travellers = {
        ...travellersInfo,
        seniors: [
          ...travellersInfo.seniors.slice(0, selectContactIndex),
          senior,
          ...travellersInfo.seniors.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfo(travellers);
      const validateTravellers = {
        ...travellersInfoValidation,
        seniors: [
          ...travellersInfoValidation.seniors.slice(0, selectContactIndex),
          validateSeniorInfoTrain(senior),
          ...travellersInfoValidation.seniors.slice(selectContactIndex + 1),
        ],
      };
      updateTravellersInfoValidation(validateTravellers);
      this.setState({ isSelectContact: false });
      return;
    }
  }

  renderSelectContact(contactType: string, index: number = 0) {
    return (
      <ButtonBase
        style={{ borderRadius: '4px' }}
        onClick={() =>
          this.setState({
            isSelectContact: true,
            selectContactType: contactType,
            selectContactIndex: index,
          })
        }
      >
        <AddContact>
          <IconContactList style={{ width: '24px', height: '24px' }} />
          <Typography variant="body2" style={{ color: BLUE, paddingLeft: '8px' }}>
            <FormattedMessage id="booking.addFromContact" />
          </Typography>
        </AddContact>
      </ButtonBase>
    );
  }

  render() {
    const {
      ticktock,
      continue_,
      customerInfo,
      contactInfo,
      travellersInfo,
      exportBillInfo,
      customerInfoValidation,
      contactInfoValidation,
      travellersInfoValidation,
      exportBillInfoValidation,
      updateContactInfo,
      updateExportBillInfo,
      updateTravellersInfo,
      updateContactInfoValidation,
      updateExportBillInfoValidation,
      updateTravellersInfoValidation,
    } = this.props;
    const { isSelectContact } = this.state;

    const { adults, childrens, students, seniors } = travellersInfo;
    const validCustomerInfo = validCustomerInfoTrain(customerInfoValidation);

    return (
      <div style={{ flex: 1 }}>
        <Box id={'customerinfo' as BookingInfoBoxTrainType}>
          <FieldHeader>
            <Typography variant="h6" style={{ paddingRight: '24px' }}>
              <FormattedMessage id="booking.train.bookingInfo" />
            </Typography>
            {this.renderSelectContact(CONTACT_TYPE.CUSTOMER_INFO)}
          </FieldHeader>
          <div>
            {validCustomerInfo && (
              <Line>
                <IconWarning style={{ width: 'auto', height: '24px', marginRight: '8px' }} />
                <Typography variant="body2" color="error">
                  <FormattedMessage id={validCustomerInfo} />
                </Typography>
              </Line>
            )}
            {renderCustomerInfo(customerInfo)}
          </div>
        </Box>
        <Box id={'travellerInfo' as BookingInfoBoxTrainType}>
          <SectionHeader>
            <Typography variant="h6">
              <FormattedMessage id="booking.train.travellerInfo" />
            </Typography>
          </SectionHeader>
          <Line>
            <IconWarning style={{ width: 'auto', height: '24px', marginRight: '8px' }} />
            <Typography variant="body2" color="error">
              <FormattedMessage id="booking.train.warningNote" />
            </Typography>
          </Line>
          {adults.map((info: AdultTravellerInfo, i: number) => (
            <div key={i}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${i + 1}. `}
                  <FormattedMessage id="booking.adult" values={{ num: adults.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.ADULT_INFO, i)}
              </FieldHeader>
              <AdultForm
                ticktock={ticktock}
                info={info}
                validation={travellersInfoValidation.adults[i]}
                update={(info: AdultTravellerInfo, validation: AdultInfoValidation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    adults: [...travellersInfo.adults.slice(0, i), info, ...travellersInfo.adults.slice(i + 1)],
                  });
                  updateTravellersInfoValidation({
                    ...travellersInfoValidation,
                    adults: [
                      ...travellersInfoValidation.adults.slice(0, i),
                      validation,
                      ...travellersInfoValidation.adults.slice(i + 1),
                    ],
                  });
                }}
              />
            </div>
          ))}
          {students.map((info: StudentTravellerInfo, i: number) => (
            <div key={i}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${adults.length + i + 1}. `}
                  <FormattedMessage id="booking.student" values={{ num: students.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.STUDENT_INFO, i)}
              </FieldHeader>
              <StudentForm
                ticktock={ticktock}
                info={info}
                validation={travellersInfoValidation.students[i]}
                update={(info: StudentTravellerInfo, validation: StudentInfoValidation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    students: [...travellersInfo.students.slice(0, i), info, ...travellersInfo.students.slice(i + 1)],
                  });
                  updateTravellersInfoValidation({
                    ...travellersInfoValidation,
                    students: [
                      ...travellersInfoValidation.students.slice(0, i),
                      validation,
                      ...travellersInfoValidation.students.slice(i + 1),
                    ],
                  });
                }}
              />
            </div>
          ))}
          <div>
            {childrens.map((info: ChildTravellerInfo, i: number) => (
              <div key={i}>
                <FieldHeader>
                  <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                    {`${adults.length + students.length + i + 1}. `}
                    <FormattedMessage id="booking.child" values={{ num: childrens.length === 1 ? '' : `${i + 1}` }} />
                  </Typography>
                  {this.renderSelectContact(CONTACT_TYPE.CHILD_INFO, i)}
                </FieldHeader>
                <ChildrenForm
                  ticktock={ticktock}
                  info={info}
                  validation={travellersInfoValidation.childrens[i]}
                  update={(info: ChildTravellerInfo, validation: ChildInfoValidation) => {
                    updateTravellersInfo({
                      ...travellersInfo,
                      childrens: [
                        ...travellersInfo.childrens.slice(0, i),
                        info,
                        ...travellersInfo.childrens.slice(i + 1),
                      ],
                    });
                    updateTravellersInfoValidation({
                      ...travellersInfoValidation,
                      childrens: [
                        ...travellersInfoValidation.childrens.slice(0, i),
                        validation,
                        ...travellersInfoValidation.childrens.slice(i + 1),
                      ],
                    });
                  }}
                />
              </div>
            ))}
            <div style={{ marginBottom: '12px' }}>
              <Typography variant="body2" color="secondary">
                <FormattedMessage id="booking.train.note" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">
                <FormattedMessage id="booking.train.childNote" />
              </Typography>
            </div>
          </div>
          {seniors.map((info: ChildTravellerInfo, i: number) => (
            <div key={i}>
              <FieldHeader>
                <Typography variant="subtitle2" style={{ paddingRight: '24px' }}>
                  {`${adults.length + students.length + childrens.length + i + 1}. `}
                  <FormattedMessage id="booking.senior" values={{ num: seniors.length === 1 ? '' : `${i + 1}` }} />
                </Typography>
                {this.renderSelectContact(CONTACT_TYPE.OLDER_INFO, i)}
              </FieldHeader>
              <OlderForm
                ticktock={ticktock}
                info={info}
                validation={travellersInfoValidation.seniors[i]}
                update={(info: SeniorTravellerInfo, validation: SeniorInfoValidation) => {
                  updateTravellersInfo({
                    ...travellersInfo,
                    seniors: [...travellersInfo.seniors.slice(0, i), info, ...travellersInfo.seniors.slice(i + 1)],
                  });
                  updateTravellersInfoValidation({
                    ...travellersInfoValidation,
                    seniors: [
                      ...travellersInfoValidation.seniors.slice(0, i),
                      validation,
                      ...travellersInfoValidation.childrens.slice(i + 1),
                    ],
                  });
                }}
              />
            </div>
          ))}
        </Box>
        <Box id={'contactInfo' as BookingInfoBoxTrainType}>
          <FieldHeader>
            <Typography variant="h6" style={{ paddingRight: '24px' }}>
              <FormattedMessage id="contactInfo" />
            </Typography>
            {this.renderSelectContact(CONTACT_TYPE.CONTACT_INFO)}
          </FieldHeader>
          <div style={{ display: 'flex', width: '700px' }}>
            <IconWarning style={{ alignSelf: 'start', marginRight: '8px' }} />
            <div>
              <div>
                <Typography variant="body2" color="error">
                  <FormattedMessage id="booking.train.contactNote1" />
                </Typography>{' '}
              </div>{' '}
              <div>
                <Typography variant="body2" color="error">
                  <FormattedMessage id="booking.train.contactNote2" />
                </Typography>
              </div>
            </div>
          </div>
          <div style={{ paddingTop: '16px' }}>
            <ContactForm
              ticktock={ticktock}
              info={contactInfo}
              validation={contactInfoValidation}
              update={(info: ContactInfo, validation: ContactInfoValidation) => {
                updateContactInfo(info);
                updateContactInfoValidation(validation);
              }}
            />
          </div>
        </Box>{' '}
        <Box id={'exportBillInfo' as BookingInfoBoxTrainType}>
          <Typography variant="body2">
            <FormattedMessage
              id="booking.train.exportBillNote"
              values={{ num: <span style={{ color: BLUE }}>19006469</span> }}
            />
          </Typography>
          <ExportBillForm
            ticktock={ticktock}
            info={exportBillInfo}
            validation={exportBillInfoValidation}
            update={(info: ExportBillInfo, validation: ExportBillInfoValidation) => {
              updateExportBillInfo(info);
              updateExportBillInfoValidation(validation);
            }}
          />
        </Box>
        <div style={{ marginTop: '24px', marginBottom: '40px', textAlign: 'center' }}>
          <Button variant="contained" color="secondary" style={{ width: '220px', height: '40px' }} onClick={continue_}>
            <Typography variant="body1">
              <FormattedMessage id="continue" />
            </Typography>
          </Button>
        </div>
        <SelectContactDialog
          show={isSelectContact}
          onClose={() => this.setState({ isSelectContact: false })}
          onSelect={customer => this.onSelectContact(customer)}
        />
      </div>
    );
  }
}

export default connect(mapState2Props)(TrainBookingInfoBox);
