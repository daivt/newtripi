import { Typography } from '@material-ui/core';
import TrendingFlatIcon from '@material-ui/icons/TrendingFlat';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { GREY } from '../../../../../colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconTrain } from '../../../../../svg/Train.svg';
import { Line } from '../../../../common/components/elements';

interface Props extends ReturnType<typeof mapStateToProps> {
  data: some[];
}
const TicketInfoBox: React.FunctionComponent<Props> = props => {
  const { data } = props;

  return (
    <div style={{ padding: '24px 32px 32px 32px' }}>
      {data &&
        data.map((v: some, index: number) => (
          <div key={v.Id}>
            <Typography variant="subtitle2">
              <FormattedMessage id={v.trip === 1 ? 'outbound' : 'inbound'} />
            </Typography>
            <Line style={{ minHeight: '58px', marginTop: '8px' }}>
              <Line style={{ flex: 1.5 }}>
                <IconTrain style={{ height: '24px', width: 'auto' }} />
                <Typography variant="h6">{v.tripCode}</Typography>
              </Line>
              <div style={{ flex: 1 }}>
                <Line>
                  <Typography variant="h6">{v.departureHour}</Typography>&emsp;
                  <Typography variant="body2" color="textSecondary">
                    {moment(v.arriveDate).format('L')}
                  </Typography>
                </Line>
                <Typography variant="body2" color="textSecondary" style={{ marginTop: '8px' }}>
                  {v.departureStationName}
                </Typography>
              </div>
              <div
                style={{ flex: 1, display: 'flex', flexDirection: 'column', alignItems: 'center' }}
              >
                <TrendingFlatIcon style={{ color: GREY }} />
                <Typography variant="body2" color="textSecondary" style={{ marginTop: '4px' }}>
                  {v.duration}
                </Typography>
              </div>
              <div style={{ flex: 1 }}>
                <Line>
                  <Typography variant="h6">{v.arriveHour}</Typography>&emsp;
                  <Typography variant="body2" color="textSecondary">
                    {moment(v.arriveDate).format('L')}
                  </Typography>
                </Line>
                <Typography variant="body2" color="textSecondary" style={{ marginTop: '8px' }}>
                  {v.arrivedStationName}
                </Typography>
              </div>
            </Line>
          </div>
        ))}
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    bookingData: state.booking.train.bookingData,
  };
};
export default connect(mapStateToProps)(TicketInfoBox);
