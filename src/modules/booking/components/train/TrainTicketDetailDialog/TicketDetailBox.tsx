import { Divider, Typography } from '@material-ui/core';
import { sumBy } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { some, C_DATE_FORMAT } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import {
  ADULT_CUSTOMER_GROUP_ID,
  CHILDREN_CUSTOMER_GROUP_ID,
  SENIOR_CUSTOMER_GROUP_ID,
  STUDENT_CUSTOMER_GROUP_ID,
} from '../../../constants';
import moment from 'moment';

const Line = styled.div`
  display: flex;
  align-items: center;
  min-height: 32px;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
`;
const ScrollBar = styled.div`
  overflow: auto;
  &::-webkit-scrollbar-thumb {
    background: gray;
    border-right: 8px solid white;
    /* border-left: 8px solid white; */
  }
  &::-webkit-scrollbar {
    background: transparent;
    width: 16px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  data?: some[];
}

function renderTravellerInfo(data: some) {
  const name =
    data.customerGroupId === ADULT_CUSTOMER_GROUP_ID
      ? 'search.trainTravellersInfo.adult'
      : data.customerGroupId === STUDENT_CUSTOMER_GROUP_ID
      ? 'search.trainTravellersInfo.student'
      : data.customerGroupId === CHILDREN_CUSTOMER_GROUP_ID
      ? 'search.trainTravellersInfo.children'
      : data.customerGroupId === SENIOR_CUSTOMER_GROUP_ID
      ? 'search.trainTravellersInfo.senior'
      : undefined;

  return (
    <div key={data.id} style={{ marginBottom: '16px', marginTop: '8px' }}>
      <Line style={{ justifyContent: 'flex-start' }}>
        <Typography variant="subtitle2">{data.customerName}&nbsp;-&nbsp;</Typography>
        <Typography variant="body2">
          <FormattedMessage id={name} />
        </Typography>
      </Line>
      {data.idCardNumber && (
        <Line style={{ justifyContent: 'flex-start' }}>
          <Typography variant="body2">
            <FormattedMessage id="booking.identity" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">{data.idCardNumber}</Typography>
        </Line>
      )}
      {data.birthDay && (
        <Line>
          <Typography variant="body2">
            <FormattedMessage id="booking.birthday" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">{moment(data.birthDay, C_DATE_FORMAT).format('L')}</Typography>
        </Line>
      )}
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="train.seatTitle" />
          :&nbsp;
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="train.seat" values={{ num: data.seatNo }} />
          &nbsp;-&nbsp;
          <FormattedMessage id="train.carNum" values={{ num: data.carriageNo }} />
        </Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="train.seatStyle" />
          :&nbsp;
        </Typography>
        <Typography variant="body2">{data.seatGroupName}</Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="train.ticketPrice" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedNumber value={data.ticketPrice} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
      {data.insuranceFee && (
        <Line>
          <Typography variant="body2">
            <FormattedMessage id="train.insurance" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">
            <FormattedNumber value={data.insuranceFee} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>
      )}
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="note" />
          :&nbsp;
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="booking.train.remind" />
        </Typography>
      </Line>
      <Line>
        <Typography variant="body2">
          <FormattedMessage id="booking.totalPay" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" color="secondary">
          <FormattedNumber value={data.totalFinalPrice} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
    </div>
  );
}

const TicketDetailBox: React.FC<Props> = props => {
  const { data } = props;
  if (!data || !data.length) {
    return null;
  }
  return (
    <>
      <ScrollBar style={{ padding: '0px 42px 12px 42px', maxHeight: '436px' }}>
        <div>{data.map((v: some) => renderTravellerInfo(v))}</div>
      </ScrollBar>
      <Divider style={{ margin: '8px 32px' }} />
      <Line
        style={{
          padding: '0px 32px 32px 32px',
          justifyContent: 'space-between',
          position: 'sticky',
          bottom: '0px',
        }}
      >
        <Column>
          <Typography variant="body2">
            <FormattedMessage id="booking.train.outboundPrice" />
          </Typography>
          <Typography variant="caption" color="textSecondary">
            <FormattedMessage id="booking.includeTaxesAndFees" />
          </Typography>
        </Column>
        <Typography variant="body2" color="secondary">
          <FormattedNumber value={sumBy(data, 'totalFinalPrice')} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return {};
};

export default connect(mapStateToProps)(TicketDetailBox);
