import { Dialog, DialogContent, IconButton } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import SwipeableViews from 'react-swipeable-views';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { CustomTab, CustomTabs } from '../../../../common/components/elements';
import TicketDetailBox from './TicketDetailBox';
import TicketInfoBox from './TicketInfoBox';

const ID = 'swipeableView';
interface Props extends ReturnType<typeof mapStateToProps> {
  open: boolean;
  onClose?: () => void;
  ticketInfo: some[];
  departureData: some[];
  returnData?: some[];
  tabIndex?: number;
}

const TABS = [
  { id: 'booking.train.ticketInfo' },
  { id: 'booking.train.outboundInfo' },
  { id: 'booking.train.inboundInfo' },
];

const TrainTicketDetailDialog: React.FunctionComponent<Props> = props => {
  const { open, onClose, departureData, returnData, ticketInfo, tabIndex } = props;
  const [currentTabIndex, setTabIndex] = React.useState(0);
  const scrollTop = React.useCallback(() => {
    const div = document.getElementById(ID);
    if (div) {
      div.scrollTo({ top: 0 });
    }
  }, []);

  React.useEffect(() => {
    setTabIndex(tabIndex || 0);
  }, [tabIndex]);
  return (
    <>
      <Dialog open={open} fullWidth maxWidth="sm">
        <IconButton style={{ position: 'absolute', top: 0, right: 0, padding: '8px' }} size="small" onClick={onClose}>
          <IconClose />
        </IconButton>
        <DialogContent
          style={{
            padding: '0px',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <CustomTabs
            style={{ margin: '12px 32px 0px 32px' }}
            variant="fullWidth"
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => setTabIndex(val)}
          >
            {(returnData && returnData.length > 0 ? TABS : TABS.slice(0, 2)).map((tab, index) => (
              <CustomTab fullWidth label={<FormattedMessage id={tab.id} />} key={tab.id} />
            ))}
          </CustomTabs>
          <div
            style={{
              maxHeight: 'calc(100vh - 250px)',
              overflowX: 'hidden',
              overflowY: 'auto',
              // display: 'flex',
              justifyContent: 'center',
              flex: 1,
            }}
          >
            <SwipeableViews
              id={ID}
              onSwitching={() => {
                scrollTop();
              }}
              index={currentTabIndex}
              onChangeIndex={val => setTabIndex(val)}
            >
              <TicketInfoBox data={ticketInfo} />
              <TicketDetailBox data={departureData} />
              {returnData && <TicketDetailBox data={returnData} />}
            </SwipeableViews>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return {};
};
export default connect(mapStateToProps)(TrainTicketDetailDialog);
