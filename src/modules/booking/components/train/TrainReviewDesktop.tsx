import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { DESKTOP_WIDTH } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { PageWrapper, StickyDiv } from '../../../common/components/elements';
import Footer from '../../../common/components/Footer';
import Header from '../../../common/components/Header';
import TimeCountHolding from './TimeCountHolding';
import TrainBookingInfoSideBox from './TrainBookingInfoSideBox';
import TrainBreadcrumbs from './TrainBreadcrumbs';
import TrainReviewBox from './TrainReviewBox';

interface Props extends ReturnType<typeof mapStateToProps> {}
const TrainReviewDesktop: React.FunctionComponent<Props> = props => {
  const { time } = props;
  return (
    <>
      <PageWrapper style={{ minWidth: DESKTOP_WIDTH }}>
        <Header light />
        <Container style={{ flex: 1 }}>
          <TrainBreadcrumbs step={4} />
          <Typography variant="h5" style={{ marginBottom: '16px' }}>
            <FormattedMessage id="booking.clientInfo" />
          </Typography>
          <TimeCountHolding timeExpired={time} />
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>
              <TrainReviewBox />
            </div>
            <div style={{ width: '370px', marginLeft: '30px', marginBottom: '102px' }}>
              <StickyDiv style={{ top: 80 }}>
                <TrainBookingInfoSideBox isReview />
              </StickyDiv>
            </div>
          </div>
        </Container>
        <Footer />
      </PageWrapper>
    </>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    time: state.booking.train.timeCountHolding,
  };
};
export default connect(mapStateToProps)(TrainReviewDesktop);
