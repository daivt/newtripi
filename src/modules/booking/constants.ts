import { validBuyerValidation } from '../common/models';

export type PassportInfoType = 'passportCountry' | 'nationalityCountry';
export type BookingInfoBoxTrainType = 'contactInfo' | 'travellerInfo' | 'customerinfo' | 'exportBillInfo';

export const HOLD_PARAM_NAME = 'hold';

export const ADULT_CUSTOMER_GROUP_ID = 1;
export const CHILDREN_CUSTOMER_GROUP_ID = 2;
export const STUDENT_CUSTOMER_GROUP_ID = 12;
export const SENIOR_CUSTOMER_GROUP_ID = 13;
export const TRAIN_ROUND_TRIP_CODE = 2; // 1 sẽ là một chiều 2 là 2 chiều
export const TRIP_CREDITS_CODE = 'CD';

export const DEFAULT_FLIGHT_STATE = {
  paying: false,
  inbound: { ticket: null, extraBaggages: null },
  outbound: { ticket: null, extraBaggages: null },
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  buyInsurance: false,
  validatingInsurance: false,
  fetchingTicketData: false,
};

export const DEFAULT_HOTEL_STATE = {
  additionalRequest: '',
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
};

export const DEFAULT_MOBILE_STATE = {
  buyerInfoValidation: validBuyerValidation,
  // additionalRequest: '',
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
  buyerMobileTopupInfo: {},
  buyerMobileCardInfo: { amount: 1 },
};

export const DEFAULT_TOUR_CUSTOMER_COUNT = {
  adultCount: 0,
  childrenCount: 0,
  babyCount: 0,
  infantCount: 0,
};

export const DEFAULT_TOUR_STATE = {
  tourCustomerCountInfo: DEFAULT_TOUR_CUSTOMER_COUNT,
  additionalRequest: '',
  usePointPayment: false,
  pointUsing: 0,
  promotionCode: '',
  paying: false,
};

export const defaultSeatingInfo = { adults: [], students: [], seniors: [], children: [] };
export const defaultTrainTicketParams = {
  inbound: {},
  outbound: {},
  inboundSeating: defaultSeatingInfo,
  outboundSeating: defaultSeatingInfo,
};
export const DEFAULT_TRAIN_STATE = {
  loading: false,
  paying: false,
  guestInfo: {},
  usePointPayment: false,
  pointUsing: 0,
  ticketParams: defaultTrainTicketParams,
};
