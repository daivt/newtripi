/*----------Train----------------*/
import {
  CustomerInfoValidation,
  CustomerInfo,
  AdultTravellerInfo,
  AdultInfoValidation,
  StudentInfoValidation,
  StudentTravellerInfo,
  ChildTravellerInfo,
  ChildInfoValidation,
  ContactInfo,
  ContactInfoValidation,
  ExportBillInfo,
  ExportBillInfoValidation,
  TravellersInfo,
  TravellersInfoValidation,
  SeniorTravellerInfo,
  SeniorInfoValidation,
} from '../../common/models/train';
import { some, DATE_FORMAT } from '../../../constants';
import { validEmailRegex } from '../../common/utils';
import moment from 'moment';
import voca from 'voca';
export function transformCustomerInfoTrain(contact: some): CustomerInfo {
  return {
    bookerContactId: contact.id,
    name: `${contact.lastName} ${contact.firstName}`,
    email: contact.email,
    phone: contact.phone,
    idCardNumber: contact.passport,
  };
}
export function validateCustomerInfoTrain(info: CustomerInfo): CustomerInfoValidation {
  const bookerContactId = true;
  const name = info.name ? !!info.name.trim() : false;
  const phone = info.phone ? !!info.phone.trim() : false;
  const email = info.email && info.email.trim() ? validEmailRegex.test(info.email) : false;
  const idCardNumber = true;
  return {
    bookerContactId,
    name,
    email,
    phone,
    idCardNumber,
  };
}
export function transformContactInfoTrain(contact: some): ContactInfo {
  return {
    familyName: contact.lastName,
    givenName: contact.firstName,
    email: contact.email,
    phone: contact.phone,
    identity: contact.passport,
  };
}
export function validateContactInfoTrain(info: ContactInfo): ContactInfoValidation {
  const familyName = info.familyName ? !!info.familyName.trim() : false;
  const givenName = info.givenName ? !!info.givenName.trim() : false;
  const phone = info.phone ? info.phone.length <= 13 : false;
  const email = info.email && info.email.trim() ? validEmailRegex.test(info.email) : false;
  const identity = info.identity ? info.identity.trim().length >= 8 && info.identity.trim().length <= 12 : false;
  return {
    familyName,
    givenName,
    email,
    phone,
    identity,
  };
}
export function validateExportBillInfoTrain(info: ExportBillInfo): ExportBillInfoValidation {
  const company = info.company ? !!info.company.trim() : false;
  const taxNumber = info.taxNumber ? info.taxNumber.length <= 20 : false;
  const address = info.address ? !!info.address.trim() : false;
  const phone = info.phone ? !!info.phone.trim() : false;
  return {
    company,
    taxNumber,
    address,
    phone,
  };
}
export function transformAdultInfoTrain(contact: some) {
  return {
    birthday: moment(contact.formattedDateOfBirth, DATE_FORMAT).format('L'),
    familyName: contact.lastName,
    givenName: contact.firstName,
    identity: contact.passport,
  };
}
function isValidWord(val: string) {
  return voca.isAlphaDigit(
    voca
      .latinise(val)
      .trim()
      .split(' ')
      .join(''),
  );
}
export function validateAdultInfoTrain(info: AdultTravellerInfo): AdultInfoValidation {
  const familyName = info.familyName ? isValidWord(info.familyName) : false;
  const givenName = info.givenName ? isValidWord(info.givenName) : false;
  const birthdayRaw = moment(info.birthday, 'L', true);
  const validDate =
    birthdayRaw.isValid() &&
    moment().isSameOrAfter(birthdayRaw) &&
    moment().diff(birthdayRaw, 'years') < 60 &&
    moment().diff(birthdayRaw, 'years') >= 10;
  const birthday = info.option === 'birthday' ? validDate : true;
  const identity =
    info.option === 'identity'
      ? info.identity
        ? info.identity.trim().length >= 8 && info.identity.trim().length <= 12
        : false
      : true;

  return {
    familyName,
    givenName,
    birthday,
    identity,
  };
}
export function transformStudentInfoTrain(contact: some) {
  return {
    familyName: contact.lastName,
    givenName: contact.firstName,
    identity: contact.passport,
  };
}
export function validateStudentInfoTrain(info: StudentTravellerInfo): StudentInfoValidation {
  const familyName = info.familyName ? isValidWord(info.familyName) : false;
  const givenName = info.givenName ? isValidWord(info.givenName) : false;
  const identity = info.identity ? info.identity.trim().length >= 8 && info.identity.trim().length <= 12 : false;
  return {
    familyName,
    givenName,
    identity,
  };
}
export function transformChildInfoTrain(contact: some) {
  return {
    familyName: contact.lastName,
    givenName: contact.firstName,
    birthday: moment(contact.formattedDateOfBirth, DATE_FORMAT).format('L'),
  };
}
export function validateChildInfoTrain(info: ChildTravellerInfo): ChildInfoValidation {
  const familyName = info.familyName ? isValidWord(info.familyName) : false;
  const givenName = info.givenName ? isValidWord(info.givenName) : false;
  const birthday = moment(info.birthday, 'L', true);
  return {
    familyName,
    givenName,
    birthday: birthday.isValid() && moment().isSameOrAfter(birthday) && moment().diff(birthday, 'years') < 10,
  };
}
export function transformSeniorInfoTrain(contact: some) {
  return {
    familyName: contact.lastName,
    givenName: contact.firstName,
    birthday: moment(contact.formattedDateOfBirth, DATE_FORMAT).format('L'),
  };
}
export function validateSeniorInfoTrain(info: SeniorTravellerInfo): SeniorInfoValidation {
  const familyName = info.familyName ? isValidWord(info.familyName) : false;
  const givenName = info.givenName ? isValidWord(info.givenName) : false;
  const birthday = moment(info.birthday, 'L', true);
  return {
    familyName,
    givenName,
    birthday: birthday.isValid() && moment().isSameOrAfter(birthday) && moment().diff(birthday, 'years') >= 60,
  };
}
export const exportBillDefault: ExportBillInfo = {
  address: '',
  company: '',
  phone: '',
  taxNumber: '',
};
export function validateTrvellersInfoTrain(travellersInfo: TravellersInfo): TravellersInfoValidation {
  return {
    adults: travellersInfo.adults.map(one => ({
      ...validateAdultInfoTrain(one),
    })),
    students: travellersInfo.students.map(one => ({
      ...validateStudentInfoTrain(one),
    })),
    childrens: travellersInfo.childrens.map(one => ({
      ...validateChildInfoTrain(one),
    })),
    seniors: travellersInfo.seniors.map(one => ({
      ...validateSeniorInfoTrain(one),
    })),
  };
}

export function validTravellersInfoTrain(validation: TravellersInfoValidation) {
  for (const key in validation.adults) {
    const adult = validation.adults[key];
    if (!(adult.birthday && adult.familyName && adult.givenName && adult.identity)) {
      return false;
    }
  }
  for (const key in validation.students) {
    const students = validation.students[key];
    if (!(students.familyName && students.givenName && students.identity)) {
      return false;
    }
  }
  for (const key in validation.childrens) {
    const childrens = validation.childrens[key];
    if (!(childrens.birthday && childrens.familyName && childrens.givenName)) {
      return false;
    }
  }
  for (const key in validation.seniors) {
    const olders = validation.seniors[key];
    if (!(olders.birthday && olders.familyName && olders.givenName)) {
      return false;
    }
  }
  return true;
}

export function validCustomerInfoTrain(validation: CustomerInfoValidation) {
  if (!validation.email) {
    return 'booking.train.warningCustomerInfoEmail';
  }
  if (!validation.name) {
    return 'booking.train.warningCustomerInfoName';
  }
  if (!validation.phone) {
    return 'booking.train.warningCustomerInfoPhone';
  }

  return '';
}
export function validContactInfoTrain(validation: ContactInfoValidation) {
  return validation.familyName && validation.givenName && validation.email && validation.phone && validation.identity;
}
export function validExportBillInfoTrain(validation: ExportBillInfoValidation) {
  return validation.company && validation.address && validation.taxNumber && validation.phone;
}
