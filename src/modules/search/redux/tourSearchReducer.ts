import { ActionType, createAction, getType } from 'typesafe-actions';
import { has } from 'lodash';

export type Term = {
  term: string;
  locationId?: number;
};

export type LocationId = {
  locationId: number;
  locationName: string;
};

export function isTermInput(input: Term | LocationId): input is Term {
  return has(input, 'term');
}

export interface TourSearchParamsState {
  input: Term | LocationId;
}

export const setParams = createAction(
  'search/tour/setParams',
  resolve => (state: TourSearchParamsState) => resolve({ state }),
);

const actions = { setParams };

type ActionT = ActionType<typeof actions>;

export default function reduce(
  state: TourSearchParamsState = { input: { term: '' } },
  action: ActionT,
): TourSearchParamsState {
  switch (action.type) {
    case getType(setParams):
      return action.payload.state;
    default:
      return state;
  }
}
