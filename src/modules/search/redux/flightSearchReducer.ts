import moment, { Moment } from 'moment';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { Airport, defaultTravellerInfo, SeatClass, TravellerCountInfo } from '../../common/models';

export interface FlightSearchParamsState {
  readonly destination?: Airport;
  readonly origin?: Airport;
  readonly departureDate: Moment;
  readonly returnDate?: Moment;
  readonly travellerCountInfo: TravellerCountInfo;
  readonly seatClass: SeatClass[];
}

export const setParams = createAction('search/flight/setParams', resolve => (state: FlightSearchParamsState) =>
  resolve({ state }),
);

export const setDates = createAction(
  'search/flight/setDates',
  resolve => (departureDate: Moment, returnDate?: Moment) => resolve({ departureDate, returnDate }),
);

export const setTravellerCountInfo = createAction(
  'search/flight/setTravellerCountInfo',
  resolve => (travellerInfo: TravellerCountInfo) => resolve({ travellerInfo }),
);

export const setSeatClass = createAction('search/flight/setSeatClass', resolve => (seatClass: SeatClass[]) =>
  resolve({ seatClass }),
);

export const setOrigin = createAction('search/flight/setOrigin', resolve => (origin?: Airport) => resolve({ origin }));

export const setDestination = createAction('search/flight/setDestination', resolve => (destination?: Airport) =>
  resolve({ destination }),
);

const actions = {
  setParams,
  setDates,
  setTravellerCountInfo,
  setSeatClass,
  setOrigin,
  setDestination,
};

type Action = ActionType<typeof actions>;

export default function reducer(
  state: FlightSearchParamsState = {
    departureDate: moment().startOf('day'),
    returnDate: undefined,
    travellerCountInfo: defaultTravellerInfo,
    seatClass: [],
  },
  action: Action,
): FlightSearchParamsState {
  switch (action.type) {
    case getType(setParams):
      return action.payload.state;
    case getType(setDates):
      return {
        ...state,
        departureDate: action.payload.departureDate,
        returnDate: action.payload.returnDate,
      };
    case getType(setTravellerCountInfo):
      return {
        ...state,
        travellerCountInfo: action.payload.travellerInfo,
      };
    case getType(setSeatClass):
      return {
        ...state,
        seatClass: action.payload.seatClass,
      };
    case getType(setOrigin):
      return {
        ...state,
        origin: action.payload.origin,
      };
    case getType(setDestination):
      return { ...state, destination: action.payload.destination };
    default:
      return state;
  }
}
