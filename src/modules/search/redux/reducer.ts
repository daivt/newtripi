import { combineReducers } from 'redux';
import flightReducer, { FlightSearchParamsState } from './flightSearchReducer';
import hotelReducer, { HotelSearchParamsState } from './hotelSearchReducer';
import tourReducer, { TourSearchParamsState } from './tourSearchReducer';
import flightHuntSearchReducer, { FlightHuntSearchState } from './flightHuntSearchReducer';
import trainReducer, { TrainSearchParamsState } from './trainSearchReducer';

export interface SearchState {
  flight: FlightSearchParamsState;
  train: TrainSearchParamsState;
  hotel: HotelSearchParamsState;
  tour: TourSearchParamsState;
  flightHunt: FlightHuntSearchState;
}

export default combineReducers({
  flight: flightReducer,
  train: trainReducer,
  hotel: hotelReducer,
  tour: tourReducer,
  flightHunt: flightHuntSearchReducer,
});
