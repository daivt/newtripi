import moment, { Moment } from 'moment';
import { ActionType, createAction, getType } from 'typesafe-actions';
import {
  TrainStation,
  TrainTravellersInfo,
  defaultTrainTravellersInfo,
  TrainSeatClass,
} from '../../common/models';
import { AllTrainSeatClasses } from '../constants';

export interface TrainSearchParamsState {
  readonly destination?: TrainStation;
  readonly origin?: TrainStation;
  readonly departureDate: Moment;
  readonly returnDate?: Moment;
  readonly travellersInfo: TrainTravellersInfo;
  readonly seatClass: TrainSeatClass[];
}

export const setParams = createAction(
  'search/train/setParams',
  resolve => (state: TrainSearchParamsState) => resolve({ state }),
);

export const setDates = createAction(
  'search/train/setDates',
  resolve => (departureDate: Moment, returnDate?: Moment) => resolve({ departureDate, returnDate }),
);

export const setOrigin = createAction(
  'search/train/setOrigin',
  resolve => (origin?: TrainStation) => resolve({ origin }),
);

export const setDestination = createAction(
  'search/train/setDestination',
  resolve => (destination?: TrainStation) => resolve({ destination }),
);

export const setTravellersInfo = createAction(
  'search/train/setTravellersInfo',
  resolve => (info: TrainTravellersInfo) => resolve({ info }),
);

export const setSeatClass = createAction(
  'search/train/setSeatClass',
  resolve => (seatClass: TrainSeatClass[]) => resolve({ seatClass }),
);

const actions = { setParams, setDates, setOrigin, setDestination, setTravellersInfo, setSeatClass };

type ActionT = ActionType<typeof actions>;

export default function reducer(
  state: TrainSearchParamsState = {
    departureDate: moment().startOf('day'),
    returnDate: undefined,
    travellersInfo: defaultTrainTravellersInfo,
    seatClass: AllTrainSeatClasses,
  },
  action: ActionT,
): TrainSearchParamsState {
  switch (action.type) {
    case getType(setParams):
      return action.payload.state;
    case getType(setDates):
      return {
        ...state,
        departureDate: action.payload.departureDate,
        returnDate: action.payload.returnDate,
      };
    case getType(setOrigin):
      return {
        ...state,
        origin: action.payload.origin,
      };
    case getType(setDestination):
      return { ...state, destination: action.payload.destination };
    case getType(setTravellersInfo):
      return { ...state, travellersInfo: action.payload.info };
    case getType(setSeatClass):
      return { ...state, seatClass: action.payload.seatClass };
    default:
      return state;
  }
}
