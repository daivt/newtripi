import moment, { Moment } from 'moment';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { defaultGuestCountInfo, GuestCountInfo, Location } from '../../common/models';

export interface HotelSearchParamsState {
  readonly location?: Location;
  readonly checkIn: Moment;
  readonly checkOut: Moment;
  readonly guestInfo: GuestCountInfo;
}

export const setParams = createAction('search/hotel/setParams', resolve => (state: HotelSearchParamsState) =>
  resolve({ state }),
);

export const setDates = createAction('search/hotel/setDates', resolve => (checkIn: Moment, checkOut: Moment) =>
  resolve({ checkIn, checkOut }),
);

export const setGuestInfo = createAction('search/hotel/setGuestInfo', resolve => (guestInfo: GuestCountInfo) =>
  resolve({ guestInfo }),
);

export const setLocation = createAction('search/hotel/setLocation', resolve => (location?: Location) =>
  resolve({ location }),
);

export function getDefaultHotelSearchState(): HotelSearchParamsState {
  return {
    checkIn: moment().startOf('day'),
    checkOut: moment()
      .add(1, 'days')
      .startOf('day'),
    guestInfo: defaultGuestCountInfo,
  };
}

const actions = { setParams, setDates, setGuestInfo, setLocation };

type Action = ActionType<typeof actions>;

export default function reducer(
  state: HotelSearchParamsState = getDefaultHotelSearchState(),
  action: Action,
): HotelSearchParamsState {
  switch (action.type) {
    case getType(setParams):
      return action.payload.state;
    case getType(setDates):
      return {
        ...state,
        checkIn: action.payload.checkIn,
        checkOut: action.payload.checkOut,
      };
    case getType(setGuestInfo):
      return {
        ...state,
        guestInfo: action.payload.guestInfo,
      };
    case getType(setLocation):
      return {
        ...state,
        location: action.payload.location,
      };
    default:
      return state;
  }
}
