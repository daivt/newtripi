import { createAction, ActionType, getType } from 'typesafe-actions';
import { Airport, TravellerCountInfo, defaultTravellerInfo } from '../../common/models';

export interface FlightHuntSearchState {
  readonly origin?: Airport;
  readonly destination?: Airport;
  readonly travellerCountInfo: TravellerCountInfo;
  readonly oneWay: boolean;
}

export const setOrigin = createAction(
  'search/flightHunt/setOrigin',
  resolve => (origin?: Airport) => resolve({ origin }),
);

export const setDestination = createAction(
  'search/flightHunt/setDestination',
  resolve => (destination?: Airport) => resolve({ destination }),
);

export const setTravellerCountInfo = createAction(
  'search/flightHunt/setTravellerCountInfo',
  resolve => (travellerInfo: TravellerCountInfo) => resolve({ travellerInfo }),
);

export const setOneWay = createAction('search/flightHunt/setOneWay', resolve => (val: boolean) =>
  resolve({ val }),
);

export const setParams = createAction(
  'search/flightHunt/setParams',
  resolve => (val: FlightHuntSearchState) => resolve({ val }),
);

const actions = {
  setOrigin,
  setDestination,
  setTravellerCountInfo,
  setOneWay,
  setParams,
};

type ActionT = ActionType<typeof actions>;

export default function reducer(
  state: FlightHuntSearchState = { travellerCountInfo: defaultTravellerInfo, oneWay: true },
  action: ActionT,
): FlightHuntSearchState {
  switch (action.type) {
    case getType(setOrigin):
      return { ...state, origin: action.payload.origin };
    case getType(setDestination):
      return { ...state, destination: action.payload.destination };
    case getType(setTravellerCountInfo):
      return { ...state, travellerCountInfo: action.payload.travellerInfo };
    case getType(setOneWay):
      return { ...state, oneWay: action.payload.val };
    case getType(setParams):
      return action.payload.val;
    default:
      return state;
  }
}
