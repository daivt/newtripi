import React from 'react';
import FlightSearch from '../wvComponents/FlightSearch';
import HomeHeader from '../../common/wvComponents/HomeHeader';

interface Props {}

const Flight: React.FC<Props> = () => {
  return (
    <div>
      <HomeHeader />
      <FlightSearch />
    </div>
  );
};

export default Flight;
