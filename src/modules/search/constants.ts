import { keyframes } from 'styled-components';
import { Airport, TrainSeatClass } from '../common/models';

export const MAX_SEARCH_HISTORY = 10;
export const MAX_CHILDREN_AGE = 12;

export const NotableAirports: Airport[] = [
  { code: 'HAN', location: 'Hà Nội', name: 'Nội Bài' },
  { code: 'SGN', location: 'Hồ Chí Minh', name: 'Tân Sơn Nhất' },
  { code: 'DAD', location: 'Đà Nẵng', name: 'Quốc tế Đà Nẵng' },
  { code: 'CXR', location: 'Nha Trang', name: 'Cam Ranh' },
  { code: 'PQC', location: 'Phú Quốc', name: 'Quốc tế Phú Quốc' },
  { code: 'HUI', location: 'Huế', name: 'Quốc tế Phú Bài' },
  { code: 'HPH', location: 'Hải Phòng', name: 'Quốc tế Cát bi' },
];

export const VIBRATE_KEYFRAME = keyframes`
  0%{
    left: -8px;
  }
  50%{
    left: 8px;
  }
  100% {
    left: -8px;
  }
`;

export const AllTrainSeatClasses: TrainSeatClass[] = [
  { code: 'NGM', id: 'search.trainSeatClass.softSeat' },
  { code: 'NGC', id: 'search.trainSeatClass.hardSeat' },
  { code: 'NAM', id: 'search.trainSeatClass.room4Beds' },
  { code: 'NAC', id: 'search.trainSeatClass.room6Beds' },
  { code: 'GP', id: 'search.trainSeatClass.extraSeat' },
];
