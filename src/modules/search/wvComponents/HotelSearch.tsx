import { Button, ButtonBase, Divider, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import { FocusedInputShape } from 'react-dates';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../API';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IcCalendar } from '../../../svg/calendar.svg';
import { ReactComponent as IcPin } from '../../../svg/ic_pin.svg';
import { ReactComponent as IcTraveller } from '../../../svg/ic_traveller.svg';
import { setParams } from '../../booking/redux/hotelBookingReducer';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../common/constants';
import { GuestCountInfo, Location } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_HOTEL_FILTER_STATE } from '../../result/constants';
import { setFilter } from '../../result/redux/hotelResultReducer';
import { getInputStr } from '../components/GuestInfoBox';
import { getLabel, renderOption } from '../components/HotelSearch';
import { setDates, setGuestInfo, setLocation } from '../redux/hotelSearchReducer';
import {
  addHotelRecentSearch,
  HotelSearchParamsError,
  stringifyHotelSearchState,
  validateHotelSearchParams,
} from '../utils';
import AsyncSelect from './AsyncSelect';
import GuestInfoBox from './GuestInfoBox';
import { HotelChooseDate } from './HotelChooseDate';

const Wrapper = styled.div`
  display: flex;
  min-height: 48px;
  align-items: center;
`;
interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const mapStateToProps = (state: AppState) => ({
  search: state.search.hotel,
  notableLocationsHotel: state.common.notableLocationsHotel,
  router: state.router,
});

const HotelSearch: React.FC<Props> = props => {
  const { search, dispatch, intl, notableLocationsHotel, router } = props;
  const { guestInfo } = search;
  const { enqueueSnackbar } = useSnackbar();
  const [showSelectBox, setShowSelectBox] = React.useState(false);
  const [showCalendar, setShowCalendar] = React.useState(false);
  const [focusedInput, setFocusedInput] = React.useState<FocusedInputShape>('startDate');
  const [error, setError] = React.useState<HotelSearchParamsError>({
    location: false,
    date: false,
    guestInfo: false,
  });

  const onSelect = React.useCallback(
    (location?: Location) => {
      if (location && location.hotelId !== -1) {
        addHotelRecentSearch({ ...search, location });
        const searchParams = `?${stringifyHotelSearchState(search, undefined, undefined, true)}&${
          HOTEL_BOOK_PARAMS_NAMES.hotelId
        }=${location.hotelId}`;
        dispatch(setParams({ ...search, hotelId: location.hotelId }));
        dispatch(push({ pathname: `${ROUTES.hotel.hotelDetail}`, search: searchParams }));
      } else {
        dispatch(setLocation(location));
      }
    },
    [dispatch, search],
  );

  const handleChangeGuestInfo = React.useCallback(
    (newGuestInfo: GuestCountInfo) => {
      dispatch(setGuestInfo(newGuestInfo));
      const newError = validateHotelSearchParams({ ...search, guestInfo: newGuestInfo });
      setError(newError);
      if (newError.guestInfo) {
        enqueueSnackbar(intl.formatMessage({ id: 'search.hotel.invalidChildrenAges' }), {
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
          autoHideDuration: 1000,
        });
      } else {
        setShowSelectBox(false);
      }
    },
    [dispatch, enqueueSnackbar, intl, search],
  );

  return (
    <div style={{ padding: '24px 0px' }}>
      <AsyncSelect<Location>
        loadOptions={async (str: string) => {
          if (str) {
            const json = await dispatch(fetchThunk(API_PATHS.suggestLocation(str)));
            return json.data.locations.concat(json.data.hotels);
          }
          return [];
        }}
        defaultValue={search.location ? getLabel(search.location) : ''}
        icon={<IcPin style={{ width: '24px', height: '24px', marginLeft: '6px' }} />}
        placeholder={<FormattedMessage id="search.hotel.choseLocationOrHotel" />}
        searchInputPlaceholder={intl.formatMessage({ id: 'search.hotel.choseLocationOrHotel' })}
        focusOptions={notableLocationsHotel}
        getLabel={getLabel}
        renderOption={renderOption}
        onSelect={location => {
          onSelect(location);
          setError({ ...error, location: false });
        }}
        direction="up"
      />
      <div style={{ padding: '0px 16px' }}>
        <Divider style={{ marginTop: '4px' }} />
        <Wrapper
          onClick={() => {
            setFocusedInput('startDate');
            setShowCalendar(true);
          }}
        >
          <IcCalendar style={{ marginRight: '10px' }} />
          {search.checkIn ? (
            <Typography variant="body2">{moment(search.checkIn.valueOf()).format('L')}</Typography>
          ) : (
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="search.hotel.choseCheckInDate" />
            </Typography>
          )}
        </Wrapper>
        <Divider style={{ marginTop: '4px' }} />
        <Wrapper
          onClick={() => {
            setFocusedInput('endDate');
            setShowCalendar(true);
          }}
        >
          <IcCalendar style={{ marginRight: '10px' }} />
          {search.checkOut ? (
            <Typography variant="body2">{moment(search.checkOut.valueOf()).format('L')}</Typography>
          ) : (
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="search.hotel.choseCheckOutDate" />
            </Typography>
          )}
        </Wrapper>
        <Divider style={{ marginTop: '4px' }} />
        <HotelChooseDate
          showCalendar={showCalendar}
          checkInDate={search.checkIn}
          checkOutDate={search.checkOut}
          onClose={() => setShowCalendar(false)}
          onChange={(checkIn, checkOut) => {
            dispatch(setDates(checkIn, checkOut));
            setError({ ...error, date: false });
            setShowCalendar(false);
          }}
          focusedInput={focusedInput}
          key="HotelChooseDate"
        />
        <Wrapper>
          <ButtonBase style={{ display: 'flex', alignItems: 'center' }} onClick={() => setShowSelectBox(true)}>
            <IcTraveller style={{ marginRight: '10px' }} />
            <Typography variant="body2" noWrap>
              {getInputStr(guestInfo, intl)}
            </Typography>
          </ButtonBase>
          <GuestInfoBox
            showSelectBox={showSelectBox}
            guestInfo={guestInfo}
            handleChangeGuestInfo={handleChangeGuestInfo}
            onClose={() => setShowSelectBox(false)}
            error={error.guestInfo}
          />
        </Wrapper>
        <Divider style={{ marginTop: '4px' }} />
        <Button
          style={{ marginTop: '32px', width: '100%' }}
          size="large"
          color="secondary"
          variant="contained"
          onClick={() => {
            const newError = validateHotelSearchParams(search);
            if (newError.date || newError.location || newError.guestInfo) {
              setError(newError);
            } else {
              const params = stringifyHotelSearchState(search);
              if (`?${params}` !== router.location.search) {
                dispatch(setFilter(DEFAULT_HOTEL_FILTER_STATE, true));
                dispatch(push({ pathname: `${ROUTES.hotel.hotelResult}`, search: `?${params}` }));
                addHotelRecentSearch(search);
              }
            }
          }}
        >
          <FormattedMessage id="booking.search" />
        </Button>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(injectIntl(HotelSearch));
