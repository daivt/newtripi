import { Typography } from '@material-ui/core';
import { push, replace } from 'connected-react-router';
import { has } from 'lodash';
import * as React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { GREY } from '../../../colors';
import { ROUTES, some, WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import icPin from '../../../svg/ic_pin.svg';
import { setBookingParams } from '../../booking/redux/tourBookingReducer';
import { TOUR_BOOK_PARAMS_NAMES } from '../../common/constants';
import { Tour, TourLocation } from '../../common/models';
import { fetchNotableTourLocation, setTourGeneralInfo } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_TOUR_FILTER_STATE } from '../../result/constants';
import { setFilter } from '../../result/redux/tourResultReducer';
import { isTermInput, setParams, TourSearchParamsState } from '../redux/tourSearchReducer';
import { addTourRecentSearch, stringifyTourSearchState } from '../utils';
import AsyncSelect from './AsyncSelect';

function isTour(option: Tour | TourLocation): option is Tour {
  return has(option, 'fromDestination') || has(option, 'mainDestination');
}

function getLabel(option: Tour | TourLocation) {
  return option.name;
}

function renderOption(option: Tour | TourLocation, index: number, total: number) {
  const last = index === total - 1;
  if (isTour(option)) {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          padding: '8px 10px 8px 0',
          position: 'relative',
          flex: 1,
        }}
      >
        <div
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            borderBottom: `1px solid ${!last ? GREY : 'transparent'}`,
            bottom: 0,
            marginLeft: '6px',
            marginRight: '6px',
          }}
        />
        <img
          alt=""
          style={{ margin: '0 15px 0 5px', width: '56px', height: '56px', flexShrink: 0 }}
          src={option.thumb || option.imageURL}
        />

        <div style={{ overflow: 'hidden' }}>
          <Typography noWrap variant="body2">
            {option.name}
          </Typography>
          <Typography variant="caption" color="textSecondary">
            {option.mainDestination.name}
          </Typography>
        </div>
      </div>
    );
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        padding: '6px 10px 6px 0',
        position: 'relative',
        flex: 1,
      }}
    >
      <div
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          borderBottom: `1px solid ${!last ? GREY : 'transparent'}`,
          bottom: 0,
          marginLeft: '6px',
          marginRight: '6px',
        }}
      />
      <img alt="" style={{ margin: '6px', width: '24px', height: '24px' }} src={option.imageUrl || icPin} />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography variant="body2" style={{ padding: '5px' }}>
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  icon?: React.ReactNode;
  inputStyle?: React.CSSProperties;
  textInputStyle?: React.CSSProperties;
  type?: TypeSearch;
}
type TypeSearch = 'location' | 'term';

const TourSearch: React.FunctionComponent<Props> = props => {
  const {
    intl,
    search,
    dispatch,
    notableLocationsTour,
    fetchSignal,
    common,
    icon,
    inputStyle,
    textInputStyle,
    type,
  } = props;
  const [tickTock, setTickTock] = React.useState(false);

  React.useEffect(() => {
    if (!common.generalInfo.tour && common.notableLocationsTour.length === 0) {
      dispatch(fetchThunk(API_PATHS.tourGeneralInfo, 'get', false)).then(json =>
        dispatch(setTourGeneralInfo(json.data)),
      );
      dispatch(fetchNotableTourLocation());
    }
  }, [dispatch, common]);

  return (
    <AsyncSelect<Tour | TourLocation>
      icon={icon}
      inputStyle={inputStyle}
      textInputStyle={textInputStyle}
      placeholder={intl.formatMessage({ id: 'tour.placeOrNameEx' })}
      searchInputPlaceholder={intl.formatMessage({ id: 'tour.placeOrNameEx' })}
      defaultValue={isTermInput(search.input) ? search.input.term : search.input.locationName}
      focusOptions={type === 'location' ? [] : notableLocationsTour}
      getLabel={getLabel}
      loadOptions={async term => {
        const json = await dispatch(
          fetchThunk(
            `${API_PATHS.tourAutoComplete}`,
            'post',
            true,
            JSON.stringify({
              term,
              maxItems: 15,
              destinationId: type === 'location' ? search.input.locationId : undefined,
            }),
          ),
        );
        if (json.code === 200) {
          return json.data.locations
            .map((v: some) => {
              return { ...v, locationId: v.id };
            })
            .concat(json.data.tours);
        }
        return [];
      }}
      renderOption={renderOption}
      onSelect={option => {
        if (option) {
          if (isTour(option)) {
            dispatch(
              push({
                pathname: `${ROUTES.tour.tourDetail}`,
                search: `?${TOUR_BOOK_PARAMS_NAMES.tourId}=${option.id}`,
                state: {
                  backableToSearch: 1,
                },
              }),
            );
            dispatch(setBookingParams({ tourId: option.id }));
          } else {
            const params: TourSearchParamsState = {
              ...search,
              input: { locationId: option.locationId, locationName: option.name },
            };
            dispatch(setParams(params));
            dispatch(setFilter(DEFAULT_TOUR_FILTER_STATE, true));
            dispatch(
              push({
                pathname: `${WV_ROUTES.tour.tourResultLocation}`,
                search: `?${stringifyTourSearchState(params)}`,
              }),
            );
            addTourRecentSearch(params);
          }
        }
        setTickTock(!tickTock);
      }}
      onTextEnter={term => {
        const params: TourSearchParamsState = {
          ...search,
          input: type === 'location' ? { term, locationId: search.input.locationId } : { term },
        };
        const searchStr = `?${stringifyTourSearchState(search)}`;
        if (term.trim() !== '' && fetchSignal !== searchStr) {
          dispatch(setParams(params));
          dispatch(setFilter(DEFAULT_TOUR_FILTER_STATE, true));
          if (type === 'location') {
            dispatch(
              replace({
                pathname: `${WV_ROUTES.tour.tourResultLocation}`,
                search: `?${stringifyTourSearchState(params)}`,
              }),
            );
          } else if (type === 'term') {
            dispatch(
              replace({
                pathname: `${WV_ROUTES.tour.tourResultTerm}`,
                search: `?${stringifyTourSearchState(params)}`,
              }),
            );
          } else {
            dispatch(
              push({
                pathname: `${WV_ROUTES.tour.tourResultTerm}`,
                search: `?${stringifyTourSearchState(params)}`,
              }),
            );
          }
        }
      }}
      inputChange={term => dispatch(setParams({ ...search, input: { term, locationId: search.input.locationId } }))}
    />
  );
};
TourSearch.defaultProps = {};
function mapStateToProps(state: AppState) {
  return {
    fetchSignal: state.router.location.search,
    search: state.search.tour,
    notableLocationsTour: state.common.notableLocationsTour,
    common: state.common,
  };
}

export default connect(mapStateToProps)(injectIntl(TourSearch));
