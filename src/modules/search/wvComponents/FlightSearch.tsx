import { Button, Divider, IconButton, Typography } from '@material-ui/core';
import ArrowForwardIosRoundedIcon from '@material-ui/icons/ArrowForwardIosRounded';
import SwapVertSharpIcon from '@material-ui/icons/SwapVertSharp';
import { push } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, injectIntl, useIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { DARK_GREY, GREY } from '../../../colors';
import { WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconFlightDown } from '../../../svg/ic_flight_down.svg';
import { ReactComponent as IconFlightUp } from '../../../svg/ic_flight_up.svg';
import { Airport } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { FlightFilterState, setFilter } from '../../result/redux/flightResultReducer';
import { NotableAirports } from '../constants';
import { setDates, setDestination, setOrigin, setSeatClass, setTravellerCountInfo } from '../redux/flightSearchReducer';
import { addFlightSearchToHistory, stringifyFlightSearchStateAndFilterParams } from '../utils';
import AsyncSelect from './AsyncSelect/index';
import FlightChooseDate from './FlightChooseDate/index';
import FlightSelectSeatClass from './FlightSelectSeatClass';
import FlightTravellerCountInfo from './FlightTravellerCountInfo';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

export function renderOption(option: Airport) {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        borderBottom: `0.5px solid ${GREY}`,
        flex: 1,
        margin: '0px 16px',
        padding: '5px 0',
      }}
    >
      <div style={{ flex: 1 }}>
        <Typography variant="body2">
          {option.location}
          &nbsp;&#40;
          {option.code}
          &#41;
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Typography variant="body2" color="primary">
          {option.code}
        </Typography>
        <ArrowForwardIosRoundedIcon style={{ marginLeft: 3, fontSize: '14px', color: DARK_GREY }} />
      </div>
    </div>
  );
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}

const FlightSearch: React.FunctionComponent<Props> = props => {
  const { dispatch, search, lang, allTicketClasses, router } = props;

  const intl = useIntl();

  return (
    <div style={{ padding: '16px 16px 16px 6px', backgroundColor: 'white' }}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
          <AsyncSelect<Airport>
            loadOptions={async (str: string) => {
              const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
              return json.data;
            }}
            defaultValue={search.origin ? getLabel(search.origin) : ''}
            icon={<IconFlightUp />}
            placeholder={<FormattedMessage id="search.chooseOrigin" />}
            searchInputPlaceholder={intl.formatMessage({ id: 'search.enterOrigin' })}
            focusOptions={NotableAirports}
            getLabel={getLabel}
            renderOption={renderOption}
            onSelect={airport => {
              dispatch(setOrigin(airport));
            }}
          />
          <Divider style={{ marginBottom: '8px', marginLeft: '6px', marginRight: '16px' }} />
          <AsyncSelect<Airport>
            loadOptions={async (str: string) => {
              const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
              return json.data;
            }}
            defaultValue={search.destination ? getLabel(search.destination) : ''}
            icon={<IconFlightDown />}
            placeholder={<FormattedMessage id="search.chooseDestination" />}
            searchInputPlaceholder={intl.formatMessage({ id: 'search.chooseDestination' })}
            focusOptions={NotableAirports}
            getLabel={getLabel}
            renderOption={renderOption}
            onSelect={airport => {
              dispatch(setDestination(airport));
            }}
          />
        </div>

        <IconButton
          style={{ backgroundColor: GREY }}
          size="small"
          onClick={async () => {
            dispatch(setOrigin(search.destination));
            dispatch(setDestination(search.origin));
          }}
        >
          <SwapVertSharpIcon style={{ color: 'white' }} />
        </IconButton>
      </div>
      <Divider style={{ marginBottom: '8px', marginLeft: '6px' }} />

      <FlightChooseDate
        departureDate={search.departureDate}
        returnDate={search.returnDate}
        onChange={(startDate, endDate) => dispatch(setDates(startDate, endDate))}
      />

      <Divider style={{ marginBottom: '8px', marginLeft: '6px' }} />
      <FlightTravellerCountInfo
        travellerInfo={search.travellerCountInfo}
        onChange={travellerInfo => dispatch(setTravellerCountInfo(travellerInfo))}
      />

      <Divider style={{ marginBottom: '8px', marginLeft: '6px' }} />
      <FlightSelectSeatClass
        lang={lang}
        allTicketClasses={allTicketClasses}
        ticketClasses={search.seatClass}
        onSeatChange={val => dispatch(setSeatClass(val))}
      />
      <Divider style={{ marginBottom: '8px', marginLeft: '6px' }} />

      <div style={{ margin: '32px 24px' }}>
        <Button
          variant="contained"
          color="secondary"
          style={{ width: '100%' }}
          onClick={() => {
            const params = `?${stringifyFlightSearchStateAndFilterParams(search)}`;
            if (params !== router.location.search) {
              addFlightSearchToHistory(search);

              const filter: FlightFilterState = {
                ...props.filter,
                airline: [],
              };
              dispatch(setFilter(filter));
              dispatch(push({ pathname: `${WV_ROUTES.flight.flightResult}`, search: params }));
            }
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="booking.search" />
          </Typography>
        </Button>
      </div>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    search: state.search.flight,
    allTicketClasses: state.common.generalInfo.flight ? state.common.generalInfo.flight.ticketclass : [],
    lang: state.intl.locale.substring(0, 2),
    filter: state.result.flight.filterParams,
    router: state.router,
    common: state.common,
    locale: state.intl.locale,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightSearch));
