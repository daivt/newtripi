import { ButtonBase, Typography } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { DARK_GREY, GREY, RED } from '../../../../colors';
import SelectAgeModal from './SelectAgeModal';

interface Props extends WrappedComponentProps {
  childrenAges: number[];
  onChange: (value: number, index: number) => void;
  error: boolean;
}

const ChildrenAgeSelectSection: React.FC<Props> = props => {
  const { childrenAges, onChange, error } = props;
  const [openAgeModal, setOpenAgeModal] = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const onAccept = React.useCallback(
    (newValue: number, newIndex: number) => {
      onChange(newValue, newIndex);
      setOpenAgeModal(false);
    },
    [onChange],
  );
  const onClose = React.useCallback(() => {
    setOpenAgeModal(false);
  }, []);
  return (
    <div>
      {childrenAges.length > 0 && (
        <Typography variant="subtitle2" style={{ marginTop: '12px' }}>
          <FormattedMessage id="childrenAgesTitle" />
        </Typography>
      )}
      {childrenAges.map(item => (
        <ButtonBase
          key={item}
          style={{
            border: error && item === -1 ? `0.5px solid ${RED}` : 'none',
            display: 'flex',
            marginTop: '10px',
            padding: '10px 0px 10px 10px',
            borderBottom: error && item === -1 ? `0.5px solid ${RED}` : `0.5px solid ${GREY}`,
            justifyContent: 'space-between',
            borderRadius: '4px',
          }}
          onClick={() => {
            setOpenAgeModal(true);
            setIndex(index);
          }}
        >
          <Typography variant="body2">
            <FormattedMessage id="search.guestInfo.children" />
            &nbsp;
            {index + 1}
          </Typography>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {item === -1 ? (
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="selectAge" />
              </Typography>
            ) : item === 0 ? (
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="underOneYearOld" />
              </Typography>
            ) : (
              <Typography variant="body2" style={{ color: DARK_GREY }}>
                <FormattedMessage id="childrenAge" values={{ number: item }} />
              </Typography>
            )}
            <ArrowDropDownIcon style={{ marginLeft: '10px', color: DARK_GREY }} />
          </div>
        </ButtonBase>
      ))}

      <SelectAgeModal age={-1} open={openAgeModal} onAccept={onAccept} onClose={onClose} index={index} key={index} />
    </div>
  );
};
export default injectIntl(ChildrenAgeSelectSection);
