import { Button, Modal, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import 'rmc-picker/assets/index.css';
import Picker from 'rmc-picker/lib/Picker';
import { MAX_CHILDREN_AGE } from '../../constants';

interface Props {
  index: number;
  age: number;
  open: boolean;
  onAccept: (value: number, index: number) => void;
  onClose: () => void;
}

const SelectAgeModal: React.FC<Props> = props => {
  const { age, open, onAccept, onClose, index } = props;
  const [select, setSelect] = React.useState(age === -1 ? 0 : age);
  const handleChange = React.useCallback((value: number) => {
    setSelect(value);
  }, []);
  return (
    <Modal open={open} onClose={onClose} keepMounted={false}>
      <div
        style={{
          position: 'fixed',
          bottom: '40px',
          right: 0,
          left: 0,
          backgroundColor: '#fff',
          borderRadius: '10px 10px 0 0',
          zIndex: 200,
        }}
      >
        <Picker selectedValue={select} onScrollChange={handleChange} onValueChange={handleChange}>
          {Array.from(Array(MAX_CHILDREN_AGE + 1).keys()).map(item => (
            <Picker.Item value={item} key={item}>
              <Typography variant="body2" style={{ color: '#231f20' }}>
                {item === 0 ? (
                  <FormattedMessage id="underOneYearOld" />
                ) : (
                  <FormattedMessage id="childrenAge" values={{ number: item }} />
                )}
              </Typography>
            </Picker.Item>
          ))}
        </Picker>
        <div style={{ width: '100%', padding: '0px 32px' }}>
          <Button
            style={{ margin: '20px 0px', width: '100%', borderRadius: '99px' }}
            size="large"
            color="secondary"
            variant="contained"
            onClick={() => onAccept(select, index)}
          >
            <FormattedMessage id="done" />
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default SelectAgeModal;
