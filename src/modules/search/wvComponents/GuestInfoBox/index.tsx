import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';
import CloseIcon from '@material-ui/icons/Close';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREY, PRIMARY } from '../../../../colors';
import { ReactComponent as IcAdult } from '../../../../svg/ic_adult.svg';
import { ReactComponent as IcChildren } from '../../../../svg/ic_children.svg';
import { ReactComponent as IcDoor } from '../../../../svg/ic_door.svg';
import { GuestCountInfo } from '../../../common/models';
import { SlideUp } from '../../../common/wvComponents/elements';
import {
  CountBox,
  TypeBox,
  TypeLine,
  ValueBox,
  ValueControl,
} from '../../components/TravellerCountInfoBox/styles';
import { validGuestCount } from '../../utils';
import ChildrenAgeSelectSection from './ChildrenAgeSelectSection';

const SelectBox = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: white;
  z-index: 1;
`;

const TitleBar = styled.div`
  width: 100%;
  height: 48px;
  background: ${PRIMARY};
  display: flex;
`;

const Line = styled.div`
  align-items: center;
  display: flex;
`;

interface Props {
  guestInfo: GuestCountInfo;
  handleChangeGuestInfo: (guestInfo: GuestCountInfo) => void;
  showSelectBox: boolean;
  onClose: () => void;
  error: boolean;
}

const GuestInfoBox: React.FC<Props> = props => {
  const { handleChangeGuestInfo, showSelectBox, onClose, error } = props;
  const [guestInfo, setGuestInfo] = React.useState<GuestCountInfo>(props.guestInfo);
  const handleChangeChildrenAge = React.useCallback(
    (value: number, index: number) => {
      const newAgesArray = [...guestInfo.childrenAges];
      newAgesArray[index] = value;
      setGuestInfo({
        ...guestInfo,
        childrenAges: newAgesArray,
      });
    },
    [guestInfo],
  );

  return (
    <Dialog fullScreen open={showSelectBox} keepMounted={false} TransitionComponent={SlideUp}>
      <SelectBox>
        <TitleBar>
          <IconButton
            style={{ position: 'absolute', color: 'white' }}
            size="medium"
            onClick={onClose}
          >
            <CloseIcon style={{ fontSize: '24px' }} />
          </IconButton>
          <Typography style={{ color: 'white', margin: 'auto' }} variant="body1">
            <FormattedMessage id="search.hotel.roomAndTravellerInfo" />
          </Typography>
        </TitleBar>
        <div style={{ padding: '0px 20px' }}>
          <CountBox style={{ marginTop: '8px' }}>
            <Line>
              <IcDoor style={{ marginRight: '8px' }} />
              <TypeLine>
                <Typography variant="body2">
                  <FormattedMessage id="search.guestInfo.room" />
                </Typography>
              </TypeLine>
            </Line>

            <ValueControl>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(
                    guestInfo.roomCount - 1,
                    guestInfo.adultCount,
                    guestInfo.childCount,
                  )
                }
                onClick={() => setGuestInfo({ ...guestInfo, roomCount: guestInfo.roomCount - 1 })}
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '18px',
                    color: !validGuestCount(
                      guestInfo.roomCount - 1,
                      guestInfo.adultCount,
                      guestInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
              <ValueBox>{guestInfo.roomCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(
                    guestInfo.roomCount + 1,
                    guestInfo.adultCount,
                    guestInfo.childCount,
                  )
                }
                onClick={() => setGuestInfo({ ...guestInfo, roomCount: guestInfo.roomCount + 1 })}
              >
                <AddCircle
                  style={{
                    fontSize: '18px',
                    color: !validGuestCount(
                      guestInfo.roomCount + 1,
                      guestInfo.adultCount,
                      guestInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
          <CountBox>
            <Line>
              <IcAdult style={{ marginRight: '8px' }} />
              <TypeBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.guestInfo.adult" />
                  </Typography>
                </TypeLine>

                <TypeLine style={{ color: GREY }}>
                  <Typography variant="body2">
                    <FormattedMessage id="search.guestInfo.adultDef" />
                  </Typography>
                </TypeLine>
              </TypeBox>
            </Line>

            <ValueControl>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(
                    guestInfo.roomCount,
                    guestInfo.adultCount - 1,
                    guestInfo.childCount,
                  )
                }
                onClick={() => setGuestInfo({ ...guestInfo, adultCount: guestInfo.adultCount - 1 })}
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '18px',
                    color: !validGuestCount(
                      guestInfo.roomCount,
                      guestInfo.adultCount - 1,
                      guestInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
              <ValueBox>{guestInfo.adultCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(
                    guestInfo.roomCount,
                    guestInfo.adultCount + 1,
                    guestInfo.childCount,
                  )
                }
                onClick={() => setGuestInfo({ ...guestInfo, adultCount: guestInfo.adultCount + 1 })}
              >
                <AddCircle
                  style={{
                    fontSize: '18px',
                    color: !validGuestCount(
                      guestInfo.roomCount,
                      guestInfo.adultCount + 1,
                      guestInfo.childCount,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
          <CountBox>
            <Line>
              <IcChildren style={{ marginRight: '8px' }} />
              <TypeBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.guestInfo.children" />
                  </Typography>
                </TypeLine>

                <TypeLine style={{ color: GREY }}>
                  <Typography variant="body2">
                    <FormattedMessage id="search.guestInfo.childrenDef" />
                  </Typography>
                </TypeLine>
              </TypeBox>
            </Line>
            <ValueControl>
              <IconButton
                size="small"
                disabled={guestInfo.childCount === 0}
                onClick={() =>
                  setGuestInfo({
                    ...guestInfo,
                    childCount: guestInfo.childCount - 1,
                    childrenAges: guestInfo.childrenAges.slice(
                      0,
                      guestInfo.childrenAges.length - 1,
                    ),
                  })
                }
              >
                <RemoveCircleOutline
                  style={{
                    fontSize: '18px',
                    color: guestInfo.childCount === 0 ? GREY : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
              <ValueBox>{guestInfo.childCount}</ValueBox>
              <IconButton
                size="small"
                disabled={
                  !validGuestCount(
                    guestInfo.roomCount,
                    guestInfo.adultCount,
                    guestInfo.childCount + 1,
                  )
                }
                onClick={() =>
                  setGuestInfo({
                    ...guestInfo,
                    childCount: guestInfo.childCount + 1,
                    childrenAges: guestInfo.childrenAges.concat(-1),
                  })
                }
              >
                <AddCircle
                  style={{
                    fontSize: '18px',
                    color: !validGuestCount(
                      guestInfo.roomCount,
                      guestInfo.adultCount,
                      guestInfo.childCount + 1,
                    )
                      ? GREY
                      : undefined,
                  }}
                  color="secondary"
                />
              </IconButton>
            </ValueControl>
          </CountBox>
          <ChildrenAgeSelectSection
            childrenAges={guestInfo.childrenAges}
            onChange={handleChangeChildrenAge}
            error={error}
          />
        </div>

        <div style={{ width: '100%', padding: '0px 32px' }}>
          <Button
            style={{ marginTop: '32px', width: '100%' }}
            size="large"
            color="secondary"
            variant="contained"
            onClick={() => handleChangeGuestInfo(guestInfo)}
          >
            <Typography variant="button">
              <FormattedMessage id="done" />
            </Typography>
          </Button>
        </div>
      </SelectBox>
    </Dialog>
  );
};

export default GuestInfoBox;
