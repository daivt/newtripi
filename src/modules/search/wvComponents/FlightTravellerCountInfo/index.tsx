import { Backdrop, Button, ButtonBase, Dialog, IconButton, Typography } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import React, { useState } from 'react';
import { FormattedMessage, injectIntl, IntlShape, WrappedComponentProps } from 'react-intl';
import { DARK_GREY, GREY } from '../../../../colors';
import travellerSvg from '../../../../svg/ic_traveller.svg';
import { TravellerCountInfo } from '../../../common/models';
import { SlideUp } from '../../../common/wvComponents/elements';
import {
  CountBox,
  TypeBox,
  TypeLine,
  ValueBox,
  ValueControl,
} from '../../components/TravellerCountInfoBox/styles';
import { validTravellerCount } from '../../utils';

function getInputStr(travellerInfo: TravellerCountInfo, intl: IntlShape) {
  const travellerArr = [];

  if (travellerInfo.adultCount) {
    travellerArr.push(
      intl.formatMessage(
        {
          id: 'search.travellerInfo.adultCount',
        },
        { adultCount: travellerInfo.adultCount },
      ),
    );
  }

  if (travellerInfo.childCount) {
    travellerArr.push(
      intl.formatMessage(
        {
          id: 'search.travellerInfo.childCount',
        },
        { childCount: travellerInfo.childCount },
      ),
    );
  }

  if (travellerInfo.babyCount) {
    travellerArr.push(
      intl.formatMessage(
        {
          id: 'search.travellerInfo.babyCount',
        },
        { babyCount: travellerInfo.babyCount },
      ),
    );
  }

  return travellerArr.join(', ');
}

interface Props extends WrappedComponentProps {
  travellerInfo: TravellerCountInfo;
  onChange(newValue: TravellerCountInfo): void;
}

const FlightTravellerCountInfo: React.FC<Props> = props => {
  const { onChange, intl } = props;
  const [travellerInfo, setTravellerInfo] = useState<TravellerCountInfo>(props.travellerInfo);
  const [open, setOpen] = useState(false);

  return (
    <div>
      <ButtonBase
        onClick={() => setOpen(true)}
        style={{
          display: 'flex',
          minHeight: '48px',
          outline: 'none',
          alignItems: 'center',
          width: '100%',
          justifyContent: 'flex-start',
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', flexShrink: 0 }}>
          <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
            <img alt="" style={{ width: '24px', height: '24px' }} src={travellerSvg} />
          </div>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage id="ticketAlert.passengerCount" />
          </Typography>
        </div>

        <div
          style={{
            display: 'flex',
            flex: 1,
            justifyContent: 'flex-end',
            width: '50%',
            paddingLeft: '16px',
          }}
        >
          <Typography variant="body2" noWrap>
            {getInputStr(props.travellerInfo, intl)}
          </Typography>
          <ArrowDropDownIcon fontSize="small" style={{ color: DARK_GREY }} />
        </div>
      </ButtonBase>

      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        closeAfterTransition
        TransitionComponent={SlideUp}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        onEnter={() => setTravellerInfo(props.travellerInfo)}
      >
        <div
          style={{
            position: 'fixed',
            bottom: 0,
            right: 0,
            left: 0,
            backgroundColor: '#fff',
          }}
        >
          <div style={{ padding: 16 }}>
            <CountBox>
              <TypeBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.adult" />
                  </Typography>
                </TypeLine>

                <TypeLine style={{ color: GREY }}>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.adultDef" />
                  </Typography>
                </TypeLine>
              </TypeBox>
              <ValueControl>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount - 1,
                      travellerInfo.childCount,
                      travellerInfo.babyCount,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, adultCount: travellerInfo.adultCount - 1 })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount - 1,
                        travellerInfo.childCount,
                        travellerInfo.babyCount,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <ValueBox>{travellerInfo.adultCount}</ValueBox>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount + 1,
                      travellerInfo.childCount,
                      travellerInfo.babyCount,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, adultCount: travellerInfo.adultCount + 1 })
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount + 1,
                        travellerInfo.childCount,
                        travellerInfo.babyCount,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </ValueControl>
            </CountBox>
            <CountBox>
              <TypeBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.children" />
                  </Typography>
                </TypeLine>

                <TypeLine style={{ color: GREY }}>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.childrenDef" />
                  </Typography>
                </TypeLine>
              </TypeBox>
              <ValueControl>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount,
                      travellerInfo.childCount - 1,
                      travellerInfo.babyCount,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, childCount: travellerInfo.childCount - 1 })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount - 1,
                        travellerInfo.babyCount,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <ValueBox>{travellerInfo.childCount}</ValueBox>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount,
                      travellerInfo.childCount + 1,
                      travellerInfo.babyCount,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, childCount: travellerInfo.childCount + 1 })
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount + 1,
                        travellerInfo.babyCount,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </ValueControl>
            </CountBox>
            <CountBox>
              <TypeBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.baby" />
                  </Typography>
                </TypeLine>

                <TypeLine style={{ color: GREY }}>
                  <Typography variant="body2">
                    <FormattedMessage id="search.travellerInfo.babyDef" />
                  </Typography>
                </TypeLine>
              </TypeBox>
              <ValueControl>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount,
                      travellerInfo.childCount,
                      travellerInfo.babyCount - 1,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, babyCount: travellerInfo.babyCount - 1 })
                  }
                >
                  <RemoveCircleOutline
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount,
                        travellerInfo.babyCount - 1,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
                <ValueBox>{travellerInfo.babyCount}</ValueBox>
                <IconButton
                  size="small"
                  disabled={
                    !validTravellerCount(
                      travellerInfo.adultCount,
                      travellerInfo.childCount,
                      travellerInfo.babyCount + 1,
                    )
                  }
                  onClick={() =>
                    setTravellerInfo({ ...travellerInfo, babyCount: travellerInfo.babyCount + 1 })
                  }
                >
                  <AddCircle
                    style={{
                      fontSize: '21px',
                      color: !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount,
                        travellerInfo.babyCount + 1,
                      )
                        ? GREY
                        : undefined,
                    }}
                    color="primary"
                  />
                </IconButton>
              </ValueControl>
            </CountBox>
            <div style={{ margin: '16px 24px' }}>
              <Button
                variant="contained"
                color="secondary"
                style={{ width: '100%' }}
                onClick={() => {
                  onChange(travellerInfo);
                  setOpen(false);
                }}
              >
                <Typography variant="button">
                  <FormattedMessage id="apply" />
                </Typography>
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default injectIntl(FlightTravellerCountInfo);
