import {
  Button,
  ButtonBase,
  Collapse,
  Dialog,
  Divider,
  FormControlLabel,
  IconButton,
  Switch,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment, { Moment } from 'moment';
import React from 'react';
import { DayPickerRangeController, FocusedInputShape, isInclusivelyAfterDay } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import { PRIMARY } from '../../../../colors';
import calenderBackIcon from '../../../../svg/calender_back.svg';
import calenderNextIcon from '../../../../svg/calender_next.svg';
import { renderMonthText } from '../../../common/utils';
import { SlideUp } from '../../../common/wvComponents/elements';
import Header from '../../../common/wvComponents/Header';

function isDayHighlighted(day: Moment) {
  return moment().format('L') === day.format('L') ? (
    <span style={{ fontWeight: 'bold', color: PRIMARY }}>{day.format('D')}</span>
  ) : (
    day.format('D')
  );
}

interface Props {
  departureDate: Moment;
  returnDate?: Moment;
  onChange(departureDate: Moment, returnDate?: Moment): void;
}

interface State {
  open: boolean;
  twoWay: boolean;
  focusedInput: FocusedInputShape;
  departureDate: Moment;
  returnDate?: Moment;
}

class FlightChooseDate extends React.PureComponent<Props, State> {
  state: State = {
    open: false,
    twoWay: !!this.props.returnDate,
    focusedInput: 'startDate' as FocusedInputShape,
    departureDate: this.props.departureDate,
  };

  initDatePicker = () => {
    const { departureDate, returnDate } = this.props;
    this.setState(state => ({
      departureDate,
      returnDate: state.twoWay ? returnDate : undefined,
    }));
  };

  render() {
    const { onChange } = this.props;
    const { open, departureDate, returnDate, twoWay } = this.state;

    return (
      <>
        <div style={{ display: 'flex', flex: 1 }}>
          <div style={{ display: 'flex', flex: 1, alignItems: 'flex-end' }}>
            <ButtonBase
              onClick={() => this.setState({ open: true, focusedInput: 'startDate' })}
              style={{
                display: 'flex',
                minHeight: '38px',
                outline: 'none',
                alignItems: 'center',
                width: '100%',
                justifyContent: 'flex-start',
              }}
            >
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
                <img alt="" style={{ width: '24px', height: '24px' }} src={calenderNextIcon} />
              </div>
              <Typography variant="body2" color="textSecondary">
                <FormattedMessage
                  id={this.props.departureDate ? 'search.departureDate' : 'search.chooseDepartureDate'}
                />
              </Typography>
              <Typography variant="body2">
                {this.props.departureDate && (
                  <>
                    :&nbsp;
                    {moment(this.props.departureDate).format('L')}
                  </>
                )}
              </Typography>
            </ButtonBase>
          </div>

          <FormControlLabel
            style={{ margin: 0 }}
            onChange={(e, value) => {
              const newReturnDate = value ? departureDate.clone().add(3, 'days') : undefined;
              this.setState({
                twoWay: value,
                focusedInput: 'startDate',
                returnDate: newReturnDate,
              });
              onChange(departureDate, newReturnDate);
            }}
            control={<Switch color="primary" checked={twoWay} />}
            label={
              <Typography variant="caption" color="textSecondary">
                <FormattedMessage id="search.twoWay" />
              </Typography>
            }
            labelPlacement="top"
          />
        </div>

        <Collapse in={twoWay}>
          <Divider style={{ margin: '0 10px 8px' }} />
          <ButtonBase
            onClick={() => this.setState({ open: true, focusedInput: 'endDate' })}
            style={{
              display: 'flex',
              minHeight: '48px',
              outline: 'none',
              alignItems: 'center',
              width: '100%',
              justifyContent: 'flex-start',
            }}
          >
            <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
              <img alt="" style={{ width: '24px', height: '24px' }} src={calenderBackIcon} />
            </div>
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id={this.props.returnDate ? 'search.returnDate' : 'search.chooseReturnDate'} />
            </Typography>
            <Typography variant="body2">
              {this.props.returnDate && (
                <>
                  :&nbsp;
                  {moment(this.props.returnDate).format('L')}
                </>
              )}
            </Typography>
          </ButtonBase>
        </Collapse>

        <Dialog
          fullScreen
          open={open}
          onClose={() => this.setState({ open: false })}
          TransitionComponent={SlideUp}
          onEnter={this.initDatePicker}
        >
          <div style={{ display: 'flex', flexDirection: 'column', backgroundColor: '#fff' }}>
            <Header
              simple
              action={
                <IconButton
                  onClick={() => this.setState({ open: false })}
                  style={{ padding: '6px', width: '40px', flexShrink: 0 }}
                  color="inherit"
                >
                  <CloseIcon />
                </IconButton>
              }
              content={
                <Typography variant="button">
                  <FormattedMessage id="search.selectSchedule" />
                </Typography>
              }
            />
            <DayPickerRangeController
              hideKeyboardShortcutsPanel
              startDate={departureDate}
              renderMonthText={renderMonthText}
              endDate={returnDate || null}
              focusedInput={this.state.focusedInput}
              daySize={window.innerWidth / 8}
              numberOfMonths={1}
              minimumNights={0}
              onDatesChange={({ startDate, endDate }) => {
                const newStartDate = startDate || moment().startOf('days');
                this.setState({ departureDate: newStartDate, returnDate: endDate || undefined });
              }}
              renderDayContents={day => isDayHighlighted(day)}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              onFocusChange={focusedInput =>
                this.setState({
                  focusedInput: !twoWay ? 'startDate' : focusedInput || 'startDate',
                })
              }
            />

            <div style={{ margin: '16px 24px' }}>
              <Button
                variant="contained"
                color="secondary"
                style={{ width: '100%' }}
                onClick={() => {
                  onChange(departureDate, returnDate);
                  this.setState({ open: false });
                }}
              >
                <Typography variant="button">
                  <FormattedMessage id="apply" />
                </Typography>
              </Button>
            </div>
          </div>
        </Dialog>
      </>
    );
  }
}

export default FlightChooseDate;
