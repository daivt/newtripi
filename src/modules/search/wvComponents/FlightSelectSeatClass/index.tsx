import { Backdrop, ButtonBase, List, ListItem, Typography } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import CheckIcon from '@material-ui/icons/Check';
import { map, remove } from 'lodash';
import React, { useCallback, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { DARK_GREY } from '../../../../colors';
import { ReactComponent as IconSeat } from '../../../../svg/ic_flight_ticket_seat.svg';
import { SeatClass } from '../../../common/models';
import { BottomDialog, SlideUp } from '../../../common/wvComponents/elements';

interface Props {
  allTicketClasses: SeatClass[];
  ticketClasses: SeatClass[];
  lang: string;
  onSeatChange?: (newValue: SeatClass[]) => void;
}

const FlightSelectSeatClass: React.FC<Props> = props => {
  const [open, setOpen] = useState(false);
  const { allTicketClasses, ticketClasses, onSeatChange, lang } = props;

  const changeSeat = useCallback(
    (seat: SeatClass) => {
      const classes = [...ticketClasses];
      const classesCodes = map(classes, 'code');

      if (classesCodes.indexOf(seat.code) === -1) {
        classes.push(seat);
      } else {
        remove(classes, obj => obj.code === seat.code);
      }

      if (onSeatChange) {
        onSeatChange(classes);
      }
    },
    [onSeatChange, ticketClasses],
  );

  return (
    <div>
      <ButtonBase
        onClick={() => setOpen(true)}
        style={{
          display: 'flex',
          minHeight: '48px',
          outline: 'none',
          alignItems: 'center',
          width: '100%',
          justifyContent: 'flex-start',
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', flexShrink: 0 }}>
          <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
            <IconSeat style={{ width: '24px', height: '24px' }} />
          </div>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage id="filter.ticketClass" />
          </Typography>
        </div>
        <div
          style={{
            display: 'flex',
            flex: 1,
            justifyContent: 'flex-end',
            width: '50%',
            paddingLeft: '16px',
          }}
        >
          <Typography variant="body2" noWrap>
            {ticketClasses.length === allTicketClasses.length || !ticketClasses.length ? (
              <FormattedMessage id="all" />
            ) : (
              map(ticketClasses, lang.startsWith('vi') ? 'v_name' : 'i_name').join(', ')
            )}
          </Typography>
          <ArrowDropDownIcon fontSize="small" style={{ color: DARK_GREY }} />
        </div>
      </ButtonBase>

      <BottomDialog
        open={open}
        onClose={() => setOpen(false)}
        closeAfterTransition
        TransitionComponent={SlideUp}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <div
          style={{
            backgroundColor: '#fff',
            alignItems: 'flex-end',
          }}
        >
          <List
            style={{
              overflow: 'auto',
              WebkitOverflowScrolling: 'touch',
              flex: 1,
            }}
          >
            <ListItem
              button
              onClick={() => onSeatChange && onSeatChange(allTicketClasses)}
              style={{ display: 'flex', alignItems: 'center', minHeight: 48 }}
            >
              <Typography variant="body2" style={{ flex: 1 }}>
                <FormattedMessage id="all" />
              </Typography>

              {allTicketClasses.length === ticketClasses.length && <CheckIcon color="primary" />}
            </ListItem>
            {allTicketClasses &&
              allTicketClasses.map(item => {
                const seatClassCodeArr = map(ticketClasses, 'code');
                const selected = seatClassCodeArr.indexOf(item.code) !== -1;

                return (
                  <ListItem
                    key={item.code}
                    button
                    onClick={() => changeSeat(item)}
                    style={{ display: 'flex', alignItems: 'center', minHeight: 48 }}
                  >
                    <Typography variant="body2" style={{ flex: 1 }}>
                      {lang.startsWith('vi') ? item.v_name : item.i_name}
                    </Typography>

                    {selected && <CheckIcon color="primary" />}
                  </ListItem>
                );
              })}
          </List>
        </div>
      </BottomDialog>
    </div>
  );
};

export default FlightSelectSeatClass;
