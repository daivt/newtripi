import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment, { Moment } from 'moment';
import React from 'react';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { PRIMARY } from '../../../colors';
import { SlideUp } from '../../common/wvComponents/elements';
import { renderMonthText } from '../../common/utils';

const ChoseDateBox = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: white;
  z-index: 1;
`;

const TitleBar = styled.div`
  width: 100%;
  height: 48px;
  background: ${PRIMARY};
  display: flex;
`;

interface Props {
  showCalendar: boolean;
  checkInDate: Moment;
  checkOutDate: Moment;
  onChange(checkInDate: Moment, checkOutDate: Moment): void;
  onClose: () => void;
  focusedInput: FocusedInputShape;
}

export const HotelChooseDate: React.FC<Props> = props => {
  const { showCalendar, onChange, onClose } = props;
  const [checkInDate, setCheckInDate] = React.useState<Moment>(props.checkInDate);
  const [checkOutDate, setCheckOutDate] = React.useState<Moment | undefined>(props.checkOutDate);
  const [focusedInput, setFocusedInput] = React.useState<FocusedInputShape>(props.focusedInput);
  return (
    <Dialog open={showCalendar} fullScreen keepMounted={false} TransitionComponent={SlideUp}>
      <ChoseDateBox>
        <TitleBar>
          <IconButton style={{ position: 'absolute', color: 'white' }} size="medium" onClick={onClose}>
            <CloseIcon style={{ fontSize: '24px' }} />
          </IconButton>
          <Typography style={{ color: 'white', margin: 'auto' }} variant="body1">
            <FormattedMessage id="search.hotel.choseCheckInOutDate" />
          </Typography>
        </TitleBar>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <DayPickerRangeController
            hideKeyboardShortcutsPanel
            daySize={Math.floor(window.innerWidth / 8)}
            numberOfMonths={1}
            startDate={checkInDate}
            endDate={checkOutDate || null}
            focusedInput={focusedInput}
            onDatesChange={({ startDate, endDate }) => {
              const newStartDate = startDate || moment();
              setCheckInDate(newStartDate);
              setCheckOutDate(endDate || undefined);
            }}
            onFocusChange={focusedInputElem => setFocusedInput(focusedInputElem || 'startDate')}
            isOutsideRange={date =>
              moment()
                .startOf('day')
                .isAfter(date)
            }
            minimumNights={1}
            renderMonthText={renderMonthText}
          />
        </div>
        <div style={{ width: '100%', padding: '0px 16px' }}>
          <Button
            style={{ marginTop: '32px', width: '100%' }}
            size="large"
            color="secondary"
            variant="contained"
            onClick={() => {
              onChange(checkInDate, checkOutDate || checkInDate.clone().add(1, 'days'));
            }}
          >
            <Typography variant="button">
              <FormattedMessage id="done" />
            </Typography>
          </Button>
        </div>
      </ChoseDateBox>
    </Dialog>
  );
};
