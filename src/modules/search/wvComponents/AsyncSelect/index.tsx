import {
  AppBar,
  ButtonBase,
  Dialog,
  IconButton,
  List,
  ListItem,
  Typography,
} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { debounce } from 'lodash';
import * as React from 'react';
import { some } from '../../../../constants';
import { SlideUp, WVInput } from '../../../common/wvComponents/elements';

interface Props<T extends some> {
  icon?: React.ReactNode;
  placeholder: React.ReactNode;
  searchInputPlaceholder: string;
  loadOptions: (input: string) => Promise<T[]>;
  focusOptions: T[];
  inputChange?: (input: string) => void;
  defaultValue?: string;
  onSelect(option?: T): void;
  getLabel: (option: T) => string;
  renderOption(option: T, index: number, length: number): React.ReactNode;
  direction?: 'left' | 'right' | 'up' | 'down';
  onTextEnter?: (text: string) => void;
  inputStyle?: React.CSSProperties;
  textInputStyle?: React.CSSProperties;
}

interface State<T> {
  open: boolean;
  options: T[];
  input: string;
}

class AsyncSelect<T extends Object> extends React.PureComponent<Props<T>, State<T>> {
  state: State<T> = {
    open: false,
    options: [] as T[],
    input: this.props.defaultValue || '',
  };

  loadOptions = async (input: string) => {
    const { loadOptions } = this.props;
    const options = await loadOptions(input);
    if (options.length > 0) {
      this.setState({ options });
    } else if (input.trim() === '') {
      this.setState({ options: this.props.focusOptions });
    }
  };

  debounceLoadOptions = debounce(this.loadOptions, 500, { trailing: true, leading: false });

  inputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target.value;
    const { inputChange, onSelect } = this.props;
    if (inputChange) {
      inputChange(input);
    }
    this.setState({ input });
    onSelect(undefined);
    this.debounceLoadOptions(input);
  };

  componentDidUpdate(prevProps: Props<T>) {
    if (prevProps.focusOptions !== this.props.focusOptions) {
      if (this.state.options === prevProps.focusOptions) {
        this.setState({ options: this.props.focusOptions });
      }
    }
  }

  onClear = () => {
    this.setState({ input: '', options: this.props.focusOptions });
  };

  keyUp = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 13) {
      const { onTextEnter } = this.props;
      if (onTextEnter) {
        this.setState({ open: false });
        onTextEnter(this.state.input);
      }
    }
  };

  render() {
    const {
      icon,
      defaultValue,
      placeholder,
      renderOption,
      searchInputPlaceholder,
      onSelect,
      inputStyle,
      textInputStyle,
    } = this.props;
    const { open, options, input } = this.state;

    return (
      <>
        <ButtonBase
          onClick={() => this.setState({ open: true })}
          style={{
            display: 'flex',
            minHeight: '40px',
            outline: 'none',
            alignItems: 'center',
            width: '100%',
            justifyContent: 'flex-start',
            flex: 1,
            borderRadius: '8px',
            background: 'white',
            ...inputStyle,
          }}
        >
          <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>
          <Typography
            variant="body2"
            color={defaultValue ? 'textPrimary' : 'textSecondary'}
            style={textInputStyle}
          >
            {defaultValue ? defaultValue : placeholder}
          </Typography>
        </ButtonBase>
        <Dialog
          fullScreen
          open={open}
          onClose={() => this.setState({ open: false })}
          keepMounted={false}
          TransitionComponent={SlideUp}
          onEnter={this.onClear}
        >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <AppBar position="sticky">
              <div
                style={{
                  minHeight: '56px',
                  display: 'flex',
                  alignItems: 'center',
                  paddingRight: '12px',
                }}
              >
                <IconButton color="inherit" onClick={() => this.setState({ open: false })}>
                  <CloseOutlinedIcon />
                </IconButton>

                <WVInput
                  fullWidth
                  placeholder={searchInputPlaceholder}
                  onChange={this.inputChange}
                  onKeyUp={this.keyUp}
                  value={input}
                  startAdornment={
                    <div style={{ margin: '0 6px', display: 'flex', alignItems: 'center' }}>
                      {icon}
                    </div>
                  }
                  autoFocus
                  endAdornment={
                    !!input && (
                      <IconButton onClick={this.onClear} style={{ padding: '6px' }}>
                        <CancelIcon fontSize="small" />
                      </IconButton>
                    )
                  }
                />
              </div>
            </AppBar>
            <List
              style={{
                overflow: 'auto',
                WebkitOverflowScrolling: 'touch',
                flex: 1,
              }}
            >
              {options.map((option, index) => (
                <ListItem
                  onClick={() => {
                    onSelect(option);
                    this.setState({
                      open: false,
                    });
                  }}
                  button
                  key={`${JSON.stringify(option)}`}
                  style={{ padding: 0 }}
                >
                  {renderOption(option, index, options.length)}
                </ListItem>
              ))}
            </List>
          </div>
        </Dialog>
      </>
    );
  }
}

export default AsyncSelect;
