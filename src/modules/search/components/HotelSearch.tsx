import { Button, Container, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { PRIMARY } from '../../../colors';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IcHotel } from '../../../svg/ic_hotel.svg';
import { ReactComponent as IcPin } from '../../../svg/ic_pin.svg';
import icSearch from '../../../svg/ic_search.svg';
import travellerSvg from '../../../svg/ic_traveller.svg';
import { setParams } from '../../booking/redux/hotelBookingReducer';
import { HOTEL_BOOK_PARAMS_NAMES, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../common/constants';
import { Location } from '../../common/models';
import { fetchNotableHotelLocation } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { DEFAULT_HOTEL_FILTER_STATE } from '../../result/constants';
import { setFilter } from '../../result/redux/hotelResultReducer';
import { setDates, setGuestInfo, setLocation } from '../redux/hotelSearchReducer';
import {
  addHotelRecentSearch,
  HotelSearchParamsError,
  stringifyHotelSearchState,
  validateHotelSearchParams,
} from '../utils';
import AsyncSelect from './AsyncSelect';
import GuestInfoBox from './GuestInfoBox';
import HotelChooseDate from './HotelChooseDate';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dark?: boolean;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  home?: boolean;
}

export function renderOption(option: Location) {
  const style: React.CSSProperties = {
    flexShrink: 0,
    margin: '0 15px 0 5px',
    width: '28px',
    height: '28px',
    borderRadius: option.thumbnailUrl ? '50%' : undefined,
  };
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {option.hotelId !== -1 ? (
        <IcHotel style={style} />
      ) : option.thumbnailUrl ? (
        <img style={style} alt="" src={option.thumbnailUrl} />
      ) : (
        <IcPin style={style} />
      )}

      <div style={{ display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
        <Typography variant="body2" style={{ padding: '5px' }} noWrap>
          {option.name}
        </Typography>
        {option.hotelId !== -1 && (
          <Typography variant="caption" style={{ padding: '0px 5px 3px' }}>
            {option.provinceName}
          </Typography>
        )}
      </div>
    </div>
  );
}

export const getLabel = (option: Location) => {
  return `${option.name || option.provinceName}`;
};

interface State {
  error: HotelSearchParamsError;
  tick: boolean;
}

class HotelSearch extends PureComponent<Props, State> {
  state: State = { error: { location: false, date: false, guestInfo: false }, tick: false };

  date = React.createRef<HTMLDivElement>() as React.MutableRefObject<HTMLDivElement>;

  componentDidMount() {
    const { dispatch, common } = this.props;
    if (common.notableLocationsHotel.length === 0) {
      dispatch(fetchNotableHotelLocation());
    }
  }

  onSelect(location?: Location) {
    const { dispatch, search } = this.props;
    if (location && location.hotelId !== -1) {
      addHotelRecentSearch({ ...search, location });
      const searchParams = `?${stringifyHotelSearchState(search, undefined, undefined, true)}&${
        HOTEL_BOOK_PARAMS_NAMES.hotelId
      }=${location.hotelId}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${true}`;
      dispatch(setParams({ ...search, hotelId: location.hotelId, direct: true }));
      dispatch(push({ pathname: `${ROUTES.hotel.hotelDetail}`, search: searchParams }));
    } else {
      dispatch(setLocation(location));
      if (location && this.date.current) {
        this.date.current.focus();
      }
    }
  }

  render() {
    const { dispatch, search, home, notableLocationsHotel, router } = this.props;
    const { error, tick } = this.state;
    return (
      <div
        style={{
          background: !home ? PRIMARY : undefined,
          color: !home ? 'white' : undefined,
          position: 'relative',
          zIndex: 1000,
        }}
      >
        <Container
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            padding: '16px 0 0 18px',
            height: '140px',
          }}
        >
          <AsyncSelect<Location>
            key={`location${tick}`}
            error={error.location}
            minimizedWidth="350px"
            loadOptions={async (str: string) => {
              if (str) {
                const json = await dispatch(fetchThunk(API_PATHS.suggestLocation(str)));
                return json.data.locations.concat(json.data.hotels);
              }
              return [];
            }}
            icon={<IcPin style={{ width: '24px', height: '24px' }} />}
            title={<FormattedMessage id="search.chooseLocation" />}
            optionHeader={<FormattedMessage id="search.locationsAndHotels" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={this.props.intl.formatMessage({ id: 'search.chooseLocation' })}
            focusOptions={notableLocationsHotel}
            getLabel={getLabel}
            renderOption={renderOption}
            defaultValue={search.location ? getLabel(search.location) : ''}
            onSelect={location => {
              this.onSelect(location);
              this.setState({ error: { ...error, location: false } });
            }}
          />

          <HotelChooseDate
            locale={this.props.locale}
            onChange={(checkIn, checkOut) => {
              dispatch(setDates(checkIn, checkOut));
              this.setState({ error: { ...error, date: false } });
            }}
            checkInDate={search.checkIn}
            checkOutDate={search.checkOut}
            minimizedWidth="310px"
            error={error.date}
            iref={this.date}
            key={error.date ? `date${tick}` : 'date'}
          />

          <GuestInfoBox
            onChange={info => {
              dispatch(setGuestInfo(info));
              const currentlyError = validateHotelSearchParams({ ...search, guestInfo: info });
              this.setState({ error: currentlyError });
            }}
            guestInfo={search.guestInfo}
            error={error.guestInfo}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={travellerSvg} />}
            minimizedWidth="310px"
            key={`guestCount${tick}`}
          />

          <div
            style={{
              padding: '8px 0',
              height: '86px',
              display: 'flex',
              alignItems: 'flex-end',
              marginLeft: '12px',
              marginRight: '18px',
            }}
          >
            <Button
              size="large"
              color="secondary"
              variant="contained"
              onClick={() => {
                const currentlyError = validateHotelSearchParams(search);
                if (currentlyError.date || currentlyError.location || currentlyError.guestInfo) {
                  this.setState({ error: currentlyError, tick: !tick });
                } else {
                  const params = stringifyHotelSearchState(search);
                  if (`?${params}` !== router.location.search) {
                    dispatch(setFilter(DEFAULT_HOTEL_FILTER_STATE, true));
                    dispatch(push({ pathname: `${ROUTES.hotel.hotelResult}`, search: `?${params}` }));
                    addHotelRecentSearch(search);
                  }
                }
              }}
            >
              <img alt="search" src={icSearch} style={{ width: '24px', height: '24px' }} />
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    search: state.search.hotel,
    notableLocationsHotel: state.common.notableLocationsHotel,
    router: state.router,
    common: state.common,
    locale: state.intl.locale,
  };
}
export default connect(mapStateToProps)(injectIntl(HotelSearch));
