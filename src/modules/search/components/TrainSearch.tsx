import { Button, Container, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import * as React from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import voca from 'voca';
import { API_PATHS } from '../../../API';
import { PRIMARY } from '../../../colors';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import greyTrain from '../../../svg/greyTrain.svg';
import icPin from '../../../svg/ic_pin.svg';
import { ReactComponent as IconSearch } from '../../../svg/ic_search.svg';
import travellerSvg from '../../../svg/ic_traveller.svg';
import trainOrigin from '../../../svg/trainOrigin.svg';
import Swap from '../../common/components/Swap';
import { TrainStation } from '../../common/models';
import { setTrainGeneralInfo } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { setDates, setDestination, setOrigin, setSeatClass, setTravellersInfo } from '../redux/trainSearchReducer';
import {
  addTrainSearchToHistory,
  stringifyTrainSearchParams,
  TrainSearchParamsError,
  validateTrainSearchParamState,
} from '../utils';
import AsyncSelect from './AsyncSelect';
import FlightChooseDate from './FlightChooseDate';
import TrainTravellersInfoBox from './TrainTravellersInfoBox';

const noError: TrainSearchParamsError = { origin: false, destination: false, date: false };

export function renderOption(option: TrainStation) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={greyTrain} />
      <div>
        <Typography variant="body2">
          {option.trainStationName}
          &nbsp;&#40;
          {option.trainStationCode}
          &#41;
        </Typography>
      </div>
    </div>
  );
}

export function getLabel(option: TrainStation) {
  return `${option.trainStationName} (${option.trainStationCode})`;
}
interface ITrainSearchProps extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  home?: boolean;
  locale: string;
}

const TrainSearch: React.FunctionComponent<ITrainSearchProps> = props => {
  const { search, home, dispatch, intl, generalTrainInfo, locale, router } = props;
  const [error, setError] = React.useState<TrainSearchParamsError>(noError);
  const [tick, setTick] = React.useState(true);

  const dest = React.useRef<HTMLElement>(null);
  const date = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (!generalTrainInfo) {
      dispatch(fetchThunk(API_PATHS.trainGeneralInfo, 'get', false)).then(json =>
        dispatch(setTrainGeneralInfo(json.data)),
      );
    }
  }, [dispatch, generalTrainInfo]);

  return (
    <div
      style={{
        background: !home ? PRIMARY : undefined,
        color: !home ? 'white' : undefined,
        position: 'relative',
        zIndex: 1000,
      }}
    >
      <Container
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
          padding: '16px 0 0 18px',
          height: '140px',
        }}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'flex-end',
            flex: 1,
            minWidth: '430px',
          }}
        >
          <AsyncSelect<TrainStation>
            key={`origin${tick}`}
            error={error.origin}
            minimizedWidth="215px"
            loadOptions={(str: string) => {
              const searchStr = voca.latinise(str).toLowerCase();
              if (!generalTrainInfo) {
                return [];
              }
              return generalTrainInfo.trainStations.filter((item: TrainStation) => item.sKeys.includes(searchStr));
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={trainOrigin} />}
            defaultValue={search.origin ? getLabel(search.origin) : ''}
            title={<FormattedMessage id="search.chooseOrigin" />}
            optionHeader={<FormattedMessage id="search.stations" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseOrigin' })}
            focusOptions={generalTrainInfo ? generalTrainInfo.trainStations : []}
            getLabel={getLabel}
            renderOption={renderOption}
            onSelect={station => {
              setError(oldError => ({ ...oldError, origin: false }));
              dispatch(setOrigin(station));
              if (station && dest.current) {
                dest.current.focus();
              }
            }}
          />
          <div
            style={{
              padding: '8px 0',
              width: '0px',
              overflow: 'visible',
              position: 'relative',
              zIndex: 10,
              display: 'flex',
              justifyContent: 'center',
              height: '40px',
              boxSizing: 'content-box',
              alignItems: 'center',
            }}
          >
            <Swap
              home={home}
              onClick={async () => {
                const { origin } = search;
                dispatch(setOrigin(search.destination));
                // Need await here so store is updated before changing tick
                await dispatch(async () => dispatch(setDestination(origin)));
                setTick(oldTick => !oldTick);
                setError(oldError => ({
                  ...oldError,
                  destination: oldError.origin,
                  origin: oldError.destination,
                }));
              }}
            />
          </div>
          <AsyncSelect<TrainStation>
            key={`dest${tick}`}
            error={error.destination}
            minimizedWidth="215px"
            loadOptions={(str: string) => {
              const searchStr = voca.latinise(str).toLowerCase();
              if (!generalTrainInfo) {
                return [];
              }
              return generalTrainInfo.trainStations.filter((item: TrainStation) => item.sKeys.includes(searchStr));
            }}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={icPin} />}
            defaultValue={search.destination ? getLabel(search.destination) : ''}
            title={<FormattedMessage id="search.chooseDestination" />}
            optionHeader={<FormattedMessage id="search.stations" />}
            focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
            placeholder={intl.formatMessage({ id: 'search.chooseDestination' })}
            focusOptions={generalTrainInfo ? generalTrainInfo.trainStations : []}
            getLabel={getLabel}
            renderOption={renderOption}
            iref={dest}
            onSelect={station => {
              setError(oldError => ({ ...oldError, destination: false }));
              dispatch(setDestination(station));
              if (station && date.current) {
                date.current.focus();
              }
            }}
          />
        </div>
        <FlightChooseDate
          onChange={(depart, returnDate) => {
            setError(oldError => ({ ...oldError, date: false }));
            dispatch(setDates(depart, returnDate));
          }}
          departureDate={search.departureDate}
          returnDate={search.returnDate}
          minimizedWidth="324px"
          error={error.date}
          key={error.date ? `date${tick}` : 'date'}
          iref={date}
          locale={locale}
        />
        <TrainTravellersInfoBox
          onChange={info => dispatch(setTravellersInfo(info))}
          setSeatClass={seatClass => dispatch(setSeatClass(seatClass))}
          trainTravellersInfo={search.travellersInfo}
          icon={<img alt="" style={{ width: '24px', height: '24px' }} src={travellerSvg} />}
          minimizedWidth="200px"
          fullWidth="270px"
          seatClasses={search.seatClass}
          translate="translateX(-40px)"
        />
        <div
          style={{
            padding: '8px 0',
            height: '86px',
            display: 'flex',
            alignItems: 'flex-end',
            marginLeft: '12px',
            marginRight: '18px',
          }}
        >
          <Button
            size="large"
            color="secondary"
            variant="contained"
            onClick={() => {
              const currentlyError = validateTrainSearchParamState(search);
              if (currentlyError.date || currentlyError.destination || currentlyError.origin) {
                setError(currentlyError);
                setTick(v => !v);
              } else {
                const params = `?${stringifyTrainSearchParams(search)}`;
                if (params !== router.location.search) {
                  addTrainSearchToHistory(search);
                }
                if (params !== router.location.search) {
                  dispatch(
                    push({
                      pathname: ROUTES.train.trainResult,
                      search: params,
                    }),
                  );
                }
              }
            }}
          >
            <IconSearch />
          </Button>
        </div>
      </Container>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    search: state.search.train,
    generalTrainInfo: state.common.generalInfo.train,
    locale: state.intl.locale,
    router: state.router,
  };
}

export default connect(mapStateToProps)(injectIntl(TrainSearch));
