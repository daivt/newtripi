import styled from 'styled-components';

export const CountBox = styled.div`
  height: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TypeBox = styled.div`
  height: 40px;
`;

export const TypeLine = styled.div`
  height: 20px;
  display: flex;
  align-items: center;
`;

export const ValueBox = styled.div`
  width: 44px;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ValueControl = styled.div`
  display: flex;
  align-items: center;
`;
