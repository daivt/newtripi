import { MenuItem, Select, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { MAX_CHILDREN_AGE } from '../../constants';
import { BootstrapInput } from '../../../common/components/elements';

interface Props extends WrappedComponentProps {
  childrenAges: number[];
  onChange(value: any, index: number): void;
  error: boolean;
}

const ChildrenAgeSelectSection: React.FC<Props> = props => {
  const { childrenAges, onChange, error } = props;

  return (
    <div>
      {childrenAges.length > 0 && (
        <Typography variant="subtitle2" style={{ marginTop: '12px' }}>
          <FormattedMessage id="childrenAgesTitle" />
        </Typography>
      )}

      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          maxWidth: '580px',
        }}
      >
        {childrenAges.map((item, index) => (
          <Select
            value={item}
            onChange={e => onChange(e.target.value, index)}
            style={{ minWidth: '120px', maxWidth: '135px', flex: 1, margin: '8px 4px' }}
            input={<BootstrapInput />}
            key={index}
            error={error && item === -1}
          >
            <MenuItem value={-1} disabled style={{ display: 'none' }}>
              <Typography variant="body2">
                <FormattedMessage id="selectAge" />
              </Typography>
            </MenuItem>
            <MenuItem value={0}>
              <Typography variant="body2">
                <FormattedMessage id="underOneYearOld" />
              </Typography>
            </MenuItem>
            {Array.from(Array(MAX_CHILDREN_AGE).keys()).map(item => (
              <MenuItem value={item + 1} key={item + 1}>
                <Typography variant="body2">
                  <FormattedMessage id="childrenAge" values={{ number: item + 1 }} />
                </Typography>
              </MenuItem>
            ))}
          </Select>
        ))}
      </div>
    </div>
  );
};

export default injectIntl(ChildrenAgeSelectSection);
