import { Divider, IconButton, Typography } from '@material-ui/core';
import MUIButton from '@material-ui/core/Button';
import { fade } from '@material-ui/core/styles';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, IntlShape, WrappedComponentProps } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY, RED } from '../../../../colors';
import { GuestCountInfo } from '../../../common/models';
import { VIBRATE_KEYFRAME } from '../../constants';
import { validGuestCount } from '../../utils';
import styles from '../styles.module.scss';
import ChildrenAgeSelectSection from './ChildrenAgeSelectSection';
import { CountBox, TypeBox, TypeLine, ValueBox, ValueControl } from './styles';

const FULL_WIDTH = '310px';
const MIN_WIDTH = '200px';

export function getInputStr(guestInfo: GuestCountInfo, intl: IntlShape) {
  return `${intl.formatMessage(
    {
      id: 'search.guestInfo.adultCount',
    },
    { adultCount: guestInfo.adultCount },
  )}${
    guestInfo.childCount
      ? `, ${intl.formatMessage({ id: 'search.guestInfo.childCount' }, { childCount: guestInfo.childCount })}`
      : ''
  }${
    guestInfo.roomCount
      ? `, ${intl.formatMessage({ id: 'search.guestInfo.roomCount' }, { roomCount: guestInfo.roomCount })}`
      : ''
  }`;
}

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  overflow: hidden;
  min-width: 100%;
`;

const InputContainer = styled.div<{ isFocused: boolean; error: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  padding-right: 2px;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  color: ${BLACK_TEXT};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : GREY)};
  background-color: white;
  box-shadow: ${props =>
    props.isFocused
      ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem`
      : props.error
      ? `${fade(RED, 0.25)} 0 0 0 0.2rem`
      : ''};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : '')};
  color: ${BLACK_TEXT};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: ${props => (props.error ? '5' : '0')};
`;

interface Props extends WrappedComponentProps {
  icon: React.ReactNode;
  minimizedWidth?: string;
  guestInfo: GuestCountInfo;
  error: boolean;
  onChange(newValue: GuestCountInfo): void;
}

interface State {
  isFocused: boolean;
}

class GuestInfoBox extends PureComponent<Props, State> {
  state = {
    isFocused: false,
  };

  parent = React.createRef<HTMLDivElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }
    this.setState({ isFocused: false });
  };

  render() {
    const { isFocused } = this.state;
    const { minimizedWidth, icon, guestInfo, intl, onChange, error } = this.props;
    const handleChangeChildrenAge = (value: any, index: number) => {
      const newAgesArray = [...guestInfo.childrenAges];
      newAgesArray[index] = value;
      onChange({
        ...guestInfo,
        childrenAges: newAgesArray,
      });
    };
    return (
      <div
        style={{
          maxWidth: minimizedWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          flex: 1,
          height: '86px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
        }}
        ref={this.parent}
        tabIndex={-1}
        onFocus={() => this.setState({ isFocused: true })}
        onBlur={this.onBlur}
      >
        <Wrapper
          style={{
            maxWidth: isFocused ? FULL_WIDTH : minimizedWidth || FULL_WIDTH,
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
          }}
        >
          <div
            style={{
              padding: '8px 12px',
              backgroundColor: isFocused ? 'white' : 'transparent',
            }}
          >
            <div style={{ display: 'flex', height: '30px', alignItems: 'center' }}>
              <Typography variant="body2">
                <FormattedMessage id="search.guestInfo" />
              </Typography>
            </div>
            <InputContainer isFocused={isFocused} error={error}>
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  flex: 1,
                  overflow: 'hidden',
                }}
              >
                <Typography
                  variant="body2"
                  noWrap={true}
                  style={{
                    cursor: 'pointer',
                  }}
                >
                  {getInputStr(guestInfo, intl)}
                </Typography>
              </div>
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              style={{
                transition: 'all 300ms ease',
                textAlign: 'start',
                transformOrigin: '0 0',
                backgroundColor: 'white',
                padding: '0 12px 8px 12px',
              }}
            >
              <CountBox>
                <TypeLine>
                  <Typography variant="body2">
                    <FormattedMessage id="search.guestInfo.room" />
                  </Typography>
                </TypeLine>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={!validGuestCount(guestInfo.roomCount - 1, guestInfo.adultCount, guestInfo.childCount)}
                    onClick={() => onChange({ ...guestInfo, roomCount: guestInfo.roomCount - 1 })}
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validGuestCount(guestInfo.roomCount - 1, guestInfo.adultCount, guestInfo.childCount)
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{guestInfo.roomCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={!validGuestCount(guestInfo.roomCount + 1, guestInfo.adultCount, guestInfo.childCount)}
                    onClick={() => onChange({ ...guestInfo, roomCount: guestInfo.roomCount + 1 })}
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validGuestCount(guestInfo.roomCount + 1, guestInfo.adultCount, guestInfo.childCount)
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="search.guestInfo.adult" />
                    </Typography>{' '}
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="search.guestInfo.adultDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={!validGuestCount(guestInfo.roomCount, guestInfo.adultCount - 1, guestInfo.childCount)}
                    onClick={() => onChange({ ...guestInfo, adultCount: guestInfo.adultCount - 1 })}
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validGuestCount(guestInfo.roomCount, guestInfo.adultCount - 1, guestInfo.childCount)
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{guestInfo.adultCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={!validGuestCount(guestInfo.roomCount, guestInfo.adultCount + 1, guestInfo.childCount)}
                    onClick={() => onChange({ ...guestInfo, adultCount: guestInfo.adultCount + 1 })}
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validGuestCount(guestInfo.roomCount, guestInfo.adultCount + 1, guestInfo.childCount)
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="search.guestInfo.children" />
                    </Typography>
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="search.guestInfo.childrenDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={guestInfo.childCount === 0}
                    onClick={() =>
                      onChange({
                        ...guestInfo,
                        childCount: guestInfo.childCount - 1,
                        childrenAges: guestInfo.childrenAges.slice(0, guestInfo.childrenAges.length - 1),
                      })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: guestInfo.childCount === 0 ? GREY : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{guestInfo.childCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={!validGuestCount(guestInfo.roomCount, guestInfo.adultCount, guestInfo.childCount + 1)}
                    onClick={() =>
                      onChange({
                        ...guestInfo,
                        childCount: guestInfo.childCount + 1,
                        childrenAges: guestInfo.childrenAges.concat(-1),
                      })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validGuestCount(guestInfo.roomCount, guestInfo.adultCount, guestInfo.childCount + 1)
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <Divider style={{ marginTop: '8px' }} />

              <ChildrenAgeSelectSection
                childrenAges={guestInfo.childrenAges}
                onChange={handleChangeChildrenAge}
                error={error}
              />
              <div style={{ marginBottom: '10px', textAlign: 'end', marginTop: '12px' }}>
                <MUIButton
                  size="large"
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    this.parent.current && this.parent.current.blur();
                    this.setState({ isFocused: false });
                  }}
                >
                  <FormattedMessage id="done" />
                </MUIButton>
              </div>
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default injectIntl(GuestInfoBox);
