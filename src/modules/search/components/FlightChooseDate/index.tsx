import { Button, Checkbox, FormControlLabel, Typography } from '@material-ui/core';
import { CheckboxProps } from '@material-ui/core/Checkbox';
import { fade } from '@material-ui/core/styles';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import withStyles from '@material-ui/styles/withStyles';
import moment, { Moment } from 'moment';
import React, { PureComponent } from 'react';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY, RED } from '../../../../colors';
import { ReactComponent as CalendarSvg } from '../../../../svg/calendar.svg';
import { renderMonthText } from '../../../common/utils';
import { VIBRATE_KEYFRAME } from '../../constants';
import styles from '../styles.module.scss';

const FULL_WIDTH = '600px';
const MIN_WIDTH = '250px';

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  min-width: 100%;
  overflow: hidden;
`;

const InputContainer = styled.div<{ isFocused: boolean; error: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : GREY)};
  background-color: white;
  color: ${BLACK_TEXT};
  box-shadow: ${props =>
    props.isFocused
      ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem`
      : props.error
      ? `${fade(RED, 0.25)} 0 0 0 0.2rem`
      : ''};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : '')};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: ${props => (props.error ? '5' : '0')};
`;

const CustomCheckbox = withStyles({
  root: {
    color: 'inherit',
  },
})((props: CheckboxProps) => <Checkbox {...props} />);

interface Props extends WrappedComponentProps {
  minimizedWidth?: string;
  onChange(departureDate: Moment, returnDate?: Moment): void;
  departureDate: Moment;
  returnDate?: Moment;
  error: boolean;
  iref?: React.RefObject<HTMLDivElement>;
  locale: string;
}

interface State {
  isFocused: boolean;
  oneWay: boolean;
  focusedInput: FocusedInputShape;
}

class FlightChooseDate extends PureComponent<Props, State> {
  state = {
    isFocused: false,
    oneWay: !this.props.returnDate,
    focusedInput: 'startDate' as FocusedInputShape,
  };

  parent = this.props.iref || React.createRef<HTMLDivElement>();

  firefoxWorkaroundHelper = React.createRef<HTMLButtonElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        if (this.parent.current) {
          this.parent.current.focus();
        }
        return;
      }
    }
    if (!this.state.oneWay && !this.props.returnDate) {
      const { departureDate } = this.props;
      const returnDate = departureDate.clone().add(3, 'days');
      this.props.onChange(departureDate, returnDate);
    }
    this.setState({ isFocused: false });
  };

  render() {
    const { isFocused, oneWay } = this.state;
    const { minimizedWidth, departureDate, returnDate, onChange, intl, error } = this.props;

    return (
      <div
        style={{
          maxWidth: minimizedWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          flex: 1,
          height: '86px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
          transition: 'all 0.1s',
          transform: isFocused ? 'translateX(-50px)' : 'translateX(0)',
          zIndex: isFocused ? 1 : 0,
        }}
        tabIndex={-1}
        ref={this.parent}
        onBlur={this.onBlur}
        role="group"
        onFocus={e => {
          if (e.target !== this.parent.current && this.firefoxWorkaroundHelper.current) {
            this.firefoxWorkaroundHelper.current.focus();
          }
          /* need this check for oneWay checkbox */ if (e.target === e.currentTarget) {
            this.setState({ isFocused: true });
          }
        }}
      >
        <button
          type="button"
          onFocus={e => e.stopPropagation()}
          ref={this.firefoxWorkaroundHelper}
          style={{ position: 'absolute', opacity: 0, zIndex: -1 }}
        >
          FF
        </button>
        <Wrapper
          style={{
            maxWidth: isFocused ? FULL_WIDTH : minimizedWidth || FULL_WIDTH,
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
            backgroundColor: isFocused ? 'rgba(255,255,255,1)' : 'rgba(255,255,255,0)',
          }}
        >
          <div
            style={{
              padding: '8px 12px',
            }}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                height: '30px',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="search.date" />
              </Typography>
              <FormControlLabel
                style={{ outline: 'none' }}
                tabIndex={-1}
                control={
                  <CustomCheckbox
                    color="secondary"
                    // icon={<CheckBoxOutlineBlankIcon fontSize="small" style={{ filter: 'brightness(285%)' }} />}
                    checkedIcon={<CheckBoxIcon fontSize="small" style={{ background: 'white', borderRadius: '3px' }} />}
                    checked={!oneWay}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      this.setState({ oneWay: !e.target.checked, focusedInput: 'startDate' });
                      if (!e.target.checked) {
                        onChange(departureDate, undefined);
                      } else {
                        onChange(departureDate, departureDate.clone().add(3, 'days'));
                      }
                    }}
                  />
                }
                label={
                  <Typography variant="caption" color="inherit">
                    {intl.formatMessage({ id: 'search.twoWay' })}
                  </Typography>
                }
                labelPlacement="start"
              />
            </div>
            <InputContainer isFocused={isFocused} error={error}>
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
                <CalendarSvg />
              </div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  flex: 1,
                }}
              >
                <Typography
                  variant="body2"
                  style={{
                    cursor: 'pointer',
                  }}
                >
                  {moment(departureDate.valueOf()).format('L') // need recreate to gets the newest locale
                  }
                </Typography>
                {!oneWay && (
                  <>
                    &nbsp;&nbsp; -&nbsp;&nbsp;
                    <Typography
                      variant="body2"
                      style={{
                        cursor: 'pointer',
                      }}
                    >
                      {returnDate
                        ? moment(returnDate.valueOf()).format('L') // need recreate to gets the newest locale
                        : this.props.intl.formatMessage({ id: 'notChosen' })}
                    </Typography>
                  </>
                )}
              </div>
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              key={1}
              style={{
                transition: 'all 300ms ease',
                backgroundColor: 'white',
                padding: '12px 11px',
              }}
            >
              <DayPickerRangeController
                hideKeyboardShortcutsPanel
                daySize={36}
                numberOfMonths={2}
                startDate={departureDate}
                endDate={returnDate || null}
                focusedInput={this.state.focusedInput}
                onDatesChange={({ startDate, endDate }) => {
                  const newStartDate = startDate || moment().startOf('days');
                  this.props.onChange(newStartDate, endDate || undefined);
                  if (this.firefoxWorkaroundHelper.current) {
                    this.firefoxWorkaroundHelper.current.focus();
                  }
                }}
                onFocusChange={focusedInput =>
                  this.setState({
                    focusedInput: oneWay ? 'startDate' : focusedInput || 'startDate',
                  })
                }
                isOutsideRange={date => {
                  return (
                    moment()
                      .startOf('day')
                      .isAfter(date) ||
                    moment()
                      .startOf('day')
                      .add(1, 'year')
                      .isSameOrBefore(date)
                  );
                }}
                minimumNights={0}
                renderMonthText={renderMonthText}
              />
              <div style={{ textAlign: 'end', marginTop: '12px' }}>
                <Button
                  style={{ textTransform: 'none' }}
                  size="large"
                  color="secondary"
                  variant="contained"
                  onClick={() => this.parent.current && this.parent.current.blur()}
                >
                  <FormattedMessage id="done" />
                </Button>
              </div>
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default injectIntl(FlightChooseDate);
