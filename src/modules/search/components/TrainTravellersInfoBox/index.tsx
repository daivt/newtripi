import { IconButton, Typography } from '@material-ui/core';
import MUIButton from '@material-ui/core/Button';
import { fade } from '@material-ui/core/styles';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import React, { PureComponent } from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl, IntlShape } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../colors';
import icWarning from '../../../../svg/ic_warning.svg';
import { CheckButton, Line } from '../../../common/components/elements';
import { TrainSeatClass, TrainTravellersInfo } from '../../../common/models';
import { AllTrainSeatClasses } from '../../constants';
import { validTrainTravellersInfo } from '../../utils';
import { CountBox, TypeBox, TypeLine, ValueBox, ValueControl } from '../GuestInfoBox/styles';
import styles from '../styles.module.scss';

const FULL_WIDTH = '340px';
const MIN_WIDTH = '200px';

export function getInputStr(trainTravellersInfo: TrainTravellersInfo, seatClasses: TrainSeatClass[], intl: IntlShape) {
  const travellers = [];

  if (trainTravellersInfo.adultCount) {
    travellers.push(
      intl.formatMessage(
        { id: 'search.trainTravellersInfo.adultCount' },
        { adultCount: trainTravellersInfo.adultCount },
      ),
    );
  }

  if (trainTravellersInfo.seniorCount) {
    travellers.push(
      intl.formatMessage(
        { id: 'search.trainTravellersInfo.seniorCount' },
        { seniorCount: trainTravellersInfo.seniorCount },
      ),
    );
  }

  if (trainTravellersInfo.studentCount) {
    travellers.push(
      intl.formatMessage(
        { id: 'search.trainTravellersInfo.studentCount' },
        { studentCount: trainTravellersInfo.studentCount },
      ),
    );
  }

  if (trainTravellersInfo.childCount) {
    travellers.push(
      intl.formatMessage(
        { id: 'search.trainTravellersInfo.childCount' },
        { childCount: trainTravellersInfo.childCount },
      ),
    );
  }

  return `${travellers.join(', ')}, ${
    AllTrainSeatClasses.length === seatClasses.length
      ? intl.formatMessage({ id: 'all' })
      : seatClasses.map(v => intl.formatMessage({ id: v.id })).join(', ')
  }`;
}

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  overflow: hidden;
  min-width: 100%;
`;

const InputContainer = styled.div<{ isFocused: boolean }>`
  display: flex;
  align-items: center;
  padding-right: 2px;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  color: ${BLACK_TEXT};
  border-color: ${props => (props.isFocused ? props.theme.primary : GREY)};
  background-color: white;
  box-shadow: ${props => (props.isFocused ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem` : '')};
  border-color: ${props => (props.isFocused ? props.theme.primary : '')};
`;

interface Props extends WrappedComponentProps {
  icon: React.ReactNode;
  minimizedWidth?: string;
  fullWidth?: string;
  trainTravellersInfo: TrainTravellersInfo;
  onChange(newValue: TrainTravellersInfo): void;
  setSeatClass(newValue: TrainSeatClass[]): void;
  seatClasses: TrainSeatClass[];
  translate?: string;
}

interface State {
  isFocused: boolean;
}

class TrainTravellersInfoBox extends PureComponent<Props, State> {
  state = {
    isFocused: false,
  };

  parent = React.createRef<HTMLDivElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }
    this.setState({ isFocused: false });
  };

  changeSeat(seat: TrainSeatClass) {
    const { setSeatClass: onSeatChange, seatClasses } = this.props;

    let newValues;
    if (seatClasses.length === AllTrainSeatClasses.length) {
      newValues = [seat];
    } else {
      newValues = seatClasses.filter(seatClass => seatClass.code !== seat.code);
      if (newValues.length === seatClasses.length) {
        newValues.push(seat);
      }
    }
    if (onSeatChange) {
      onSeatChange(newValues);
    }
  }

  render() {
    const { isFocused } = this.state;
    const {
      minimizedWidth,
      icon,
      trainTravellersInfo,
      intl,
      onChange,
      setSeatClass,
      seatClasses,
      fullWidth,
      translate,
    } = this.props;
    return (
      <div
        style={{
          maxWidth: fullWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          flex: 1,
          height: '86px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
          transition: 'transform 0.1s linear, z-index 0.1s step-end',
          transform: isFocused && translate ? translate : 'translateX(0)',
          zIndex: isFocused ? 1 : 0,
        }}
        ref={this.parent}
        tabIndex={-1}
        onFocus={() => this.setState({ isFocused: true })}
        onBlur={this.onBlur}
      >
        <Wrapper
          style={{
            maxWidth: isFocused ? FULL_WIDTH : minimizedWidth || FULL_WIDTH,
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
            backgroundColor: isFocused ? 'white' : 'transparent',
          }}
        >
          <div
            style={{
              padding: '8px 12px',
            }}
          >
            <div style={{ display: 'flex', height: '30px', alignItems: 'center' }}>
              <Typography variant="body2">
                <FormattedMessage id="search.trainTravellersInfo" />
              </Typography>
            </div>
            <InputContainer isFocused={isFocused}>
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  flex: 1,
                  overflow: 'hidden',
                }}
              >
                <Typography
                  variant="body2"
                  noWrap={true}
                  style={{
                    cursor: 'pointer',
                  }}
                >
                  {getInputStr(trainTravellersInfo, seatClasses, intl)}
                </Typography>
              </div>
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              style={{
                transition: 'all 300ms ease',
                textAlign: 'start',
                transformOrigin: '0 0',
                backgroundColor: 'white',
                padding: '0 12px 8px 12px',
                minWidth: '340px',
              }}
            >
              <Typography variant="subtitle2" style={{ marginTop: '12px' }}>
                <FormattedMessage id="search.travellerInfo.seatClass" />
              </Typography>

              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  whiteSpace: 'nowrap',
                  flexWrap: 'wrap',
                  margin: '0 10px',
                }}
              >
                <CheckButton
                  backgroundColor="secondary"
                  active={AllTrainSeatClasses.length === seatClasses.length}
                  style={{ marginTop: '12px', width: '133px' }}
                  onClick={() => setSeatClass(AllTrainSeatClasses)}
                >
                  <FormattedMessage id="all" />
                </CheckButton>
                {AllTrainSeatClasses.map((item: TrainSeatClass) => {
                  const selected =
                    seatClasses.findIndex(one => item.code === one.code) > -1 &&
                    seatClasses.length !== AllTrainSeatClasses.length;

                  return (
                    <CheckButton
                      backgroundColor="secondary"
                      key={item.id}
                      active={selected}
                      style={{ marginTop: '12px', width: '133px' }}
                      onClick={() => this.changeSeat(item)}
                    >
                      <FormattedMessage id={item.id} />
                    </CheckButton>
                  );
                })}
              </div>

              <Line style={{ marginTop: '12px', height: '40px' }}>
                <Typography variant="subtitle2">
                  <FormattedMessage id="search.travellerInfo.travellers" />
                </Typography>
              </Line>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.adult" />
                    </Typography>{' '}
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.adultDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount - 1,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount,
                        trainTravellersInfo.studentCount,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        adultCount: trainTravellersInfo.adultCount - 1,
                      })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount - 1,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount,
                          trainTravellersInfo.studentCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{trainTravellersInfo.adultCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount + 1,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount,
                        trainTravellersInfo.studentCount,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        adultCount: trainTravellersInfo.adultCount + 1,
                      })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount + 1,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount,
                          trainTravellersInfo.studentCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.children" />
                    </Typography>
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.childrenDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={trainTravellersInfo.childCount === 0}
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        childCount: trainTravellersInfo.childCount - 1,
                      })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: trainTravellersInfo.childCount === 0 ? GREY : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{trainTravellersInfo.childCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount,
                        trainTravellersInfo.childCount + 1,
                        trainTravellersInfo.seniorCount,
                        trainTravellersInfo.studentCount,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        childCount: trainTravellersInfo.childCount + 1,
                      })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount,
                          trainTravellersInfo.childCount + 1,
                          trainTravellersInfo.seniorCount,
                          trainTravellersInfo.studentCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>

              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2" style={{ marginTop: '10px' }}>
                      <FormattedMessage id="search.trainTravellersInfo.student" />
                    </Typography>{' '}
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount,
                        trainTravellersInfo.studentCount - 1,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        studentCount: trainTravellersInfo.studentCount - 1,
                      })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount,
                          trainTravellersInfo.studentCount - 1,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{trainTravellersInfo.studentCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount,
                        trainTravellersInfo.studentCount + 1,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        studentCount: trainTravellersInfo.studentCount + 1,
                      })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount,
                          trainTravellersInfo.studentCount + 1,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.senior" />
                    </Typography>{' '}
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="search.trainTravellersInfo.seniorDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount - 1,
                        trainTravellersInfo.studentCount,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        seniorCount: trainTravellersInfo.seniorCount - 1,
                      })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount - 1,
                          trainTravellersInfo.studentCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{trainTravellersInfo.seniorCount}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTrainTravellersInfo(
                        trainTravellersInfo.adultCount,
                        trainTravellersInfo.childCount,
                        trainTravellersInfo.seniorCount + 1,
                        trainTravellersInfo.studentCount,
                      )
                    }
                    onClick={() =>
                      onChange({
                        ...trainTravellersInfo,
                        seniorCount: trainTravellersInfo.seniorCount + 1,
                      })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTrainTravellersInfo(
                          trainTravellersInfo.adultCount,
                          trainTravellersInfo.childCount,
                          trainTravellersInfo.seniorCount + 1,
                          trainTravellersInfo.studentCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <div style={{ display: 'flex', marginTop: '12px' }}>
                <img src={icWarning} alt="" style={{ width: '16px' }} />
                <Typography variant="caption" style={{ marginLeft: '12px' }}>
                  <FormattedMessage id="search.trainTravellersInfo.warning" />
                </Typography>
              </div>
              <div style={{ marginBottom: '10px', textAlign: 'end', position: 'relative' }}>
                <MUIButton size="large" variant="contained" color="secondary">
                  <FormattedMessage id="done" />
                </MUIButton>
                <div
                  style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    right: 0,
                    width: '50%',
                    cursor: 'pointer',
                  }}
                  onClick={() => {
                    this.setState({ isFocused: false });
                    this.parent.current && this.parent.current.blur();
                  }}
                />
              </div>
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default injectIntl(TrainTravellersInfoBox);
