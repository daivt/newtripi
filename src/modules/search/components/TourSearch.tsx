import { Button, Container, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import { has } from 'lodash';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { GREY, PRIMARY } from '../../../colors';
import { ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import icFlag from '../../../svg/activities_main.svg';
import icPin from '../../../svg/ic_pin.svg';
import icSearch from '../../../svg/ic_search.svg';
import { setBookingParams } from '../../booking/redux/tourBookingReducer';
import { TOUR_BOOK_PARAMS_NAMES, HOTEL_ACTIVITY_DIRECT_PARAM } from '../../common/constants';
import { Tour, TourLocation } from '../../common/models';
import { fetchThunk } from '../../common/redux/thunks';
import { setFilter } from '../../result/redux/tourResultReducer';
import { isTermInput, setParams, TourSearchParamsState } from '../redux/tourSearchReducer';
import { stringifyTourSearchState, addTourRecentSearch } from '../utils';
import AsyncSelect from './AsyncSelect';
import { setTourGeneralInfo, fetchNotableTourLocation } from '../../common/redux/reducer';
import { DEFAULT_TOUR_FILTER_STATE } from '../../result/constants';

function isTour(option: Tour | TourLocation): option is Tour {
  return has(option, 'fromDestination') || has(option, 'mainDestination');
}

function getLabel(option: Tour | TourLocation) {
  return option.name;
}

function renderOption(option: Tour | TourLocation, index: number, total: number) {
  const last = index === total - 1;
  if (isTour(option)) {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          padding: '8px 10px 8px 0',
          position: 'relative',
        }}
      >
        <div
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            borderBottom: `1px solid ${!last ? GREY : 'transparent'}`,
            bottom: 0,
            marginLeft: '76px',
          }}
        />
        <img
          alt=""
          style={{ margin: '0 15px 0 5px', width: '56px', height: '56px', flexShrink: 0 }}
          src={option.thumb || option.imageURL}
        />

        <div style={{ overflow: 'hidden' }}>
          <Typography noWrap variant="body2">
            {option.name}
          </Typography>
          <Typography variant="caption" color="textSecondary">
            {option.mainDestination.name}
          </Typography>
        </div>
      </div>
    );
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        padding: '6px 10px 6px 0',
        position: 'relative',
      }}
    >
      <div
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          borderBottom: `1px solid ${!last ? GREY : 'transparent'}`,
          bottom: 0,
          marginLeft: '6px',
        }}
      />
      <img alt="" style={{ margin: '6px', width: '24px', height: '24px' }} src={option.imageUrl || icPin} />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography variant="body2" style={{ padding: '5px' }}>
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  home?: boolean;
}

const TourSearch: React.FunctionComponent<Props> = props => {
  const { intl, search, home, dispatch, notableLocationsTour, fetchSignal, common } = props;
  const [error, setError] = React.useState(false);
  const [tickTock, setTickTock] = React.useState(false);

  React.useEffect(() => {
    if (!common.generalInfo.tour && common.notableLocationsTour.length === 0) {
      dispatch(fetchThunk(API_PATHS.tourGeneralInfo, 'get', false)).then(json =>
        dispatch(setTourGeneralInfo(json.data)),
      );
      dispatch(fetchNotableTourLocation());
    }
  }, [dispatch, common]);

  return (
    <div
      style={{
        background: !home ? PRIMARY : undefined,
        color: !home ? 'white' : undefined,
      }}
    >
      <Container
        style={{
          padding: '16px 0 0 18px',
          height: '140px',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <div style={{ width: '670px' }}>
          <AsyncSelect<Tour | TourLocation>
            key={`${tickTock}`}
            title={<FormattedMessage id="tour.placeOrName" />}
            placeholder={intl.formatMessage({ id: 'tour.placeOrNameEx' })}
            optionHeader={null}
            defaultValue={isTermInput(search.input) ? search.input.term : search.input.locationName}
            focusOptions={notableLocationsTour}
            focusOptionHeader={<FormattedMessage id="tour.popularDestinations" />}
            error={error}
            getLabel={getLabel}
            icon={<img alt="" style={{ width: '24px', height: '24px' }} src={icFlag} />}
            loadOptions={async term => {
              const json = await dispatch(
                fetchThunk(`${API_PATHS.tourAutoComplete}`, 'post', true, JSON.stringify({ term, maxItems: 15 })),
              );
              if (json.code === 200) {
                return json.data.locations
                  .map((v: some) => {
                    return { ...v, locationId: v.id };
                  })
                  .concat(json.data.tours);
              }
              return [];
            }}
            renderOption={renderOption}
            minimizedWidth="670px"
            fullWidth="680px"
            onSelect={option => {
              if (option) {
                if (isTour(option)) {
                  dispatch(
                    push({
                      pathname: `${ROUTES.tour.tourDetail}`,
                      search: `?${TOUR_BOOK_PARAMS_NAMES.tourId}=${option.id}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${true}`,
                      state: {
                        backableToSearch: 1,
                      },
                    }),
                  );
                  dispatch(setBookingParams({ tourId: option.id, direct: true }));
                  setError(false);
                } else {
                  const params: TourSearchParamsState = {
                    ...search,
                    input: { locationId: option.locationId, locationName: option.name },
                  };
                  dispatch(setParams(params));
                  dispatch(setFilter(DEFAULT_TOUR_FILTER_STATE, true));
                  dispatch(
                    push({
                      pathname: `${ROUTES.tour.tourResult}`,
                      search: `?${stringifyTourSearchState(params)}`,
                    }),
                  );
                  setError(false);
                  addTourRecentSearch(params);
                }
              }
            }}
            onTextEnter={term => {
              const params: TourSearchParamsState = { ...search, input: { term } };
              const searchStr = `?${stringifyTourSearchState(search)}`;
              if (term.trim() !== '' && fetchSignal !== searchStr) {
                dispatch(setParams(params));
                dispatch(setFilter(DEFAULT_TOUR_FILTER_STATE, true));
                dispatch(
                  push({
                    pathname: `${ROUTES.tour.tourResult}`,
                    search: `?${stringifyTourSearchState(params)}`,
                  }),
                );
                setError(false);
              }
            }}
            inputChange={term => dispatch(setParams({ ...search, input: { term } }))}
          />
        </div>
        <div
          style={{
            padding: '8px 0',
            height: '86px',
            display: 'flex',
            alignItems: 'flex-end',
            marginLeft: '12px',
            marginRight: '18px',
          }}
        >
          <Button
            size="large"
            color="secondary"
            variant="contained"
            onClick={() => {
              setTickTock(!tickTock);
              const currentlyError = isTermInput(search.input)
                ? search.input.term.trim() === ''
                : search.input.locationName.trim() === '';
              setError(currentlyError);
              const searchStr = `?${stringifyTourSearchState(search)}`;
              if (!currentlyError && fetchSignal !== searchStr) {
                dispatch(setFilter(DEFAULT_TOUR_FILTER_STATE, true));
                dispatch(
                  push({
                    pathname: `${ROUTES.tour.tourResult}`,
                    search: searchStr,
                  }),
                );
                if (!isTermInput(search.input)) {
                  addTourRecentSearch(search);
                }
              }
            }}
          >
            <img alt="search" src={icSearch} style={{ width: '24px', height: '24px' }} />
          </Button>
        </div>
      </Container>
    </div>
  );
};
TourSearch.defaultProps = {
  home: true,
};
function mapStateToProps(state: AppState) {
  return {
    fetchSignal: state.router.location.search,
    search: state.search.tour,
    notableLocationsTour: state.common.notableLocationsTour,
    common: state.common,
  };
}

export default connect(mapStateToProps)(injectIntl(TourSearch));
