import { IconButton, Typography } from '@material-ui/core';
import MUIButton from '@material-ui/core/Button';
import { fade } from '@material-ui/core/styles';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import { map, remove } from 'lodash';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, IntlShape, WrappedComponentProps } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY } from '../../../../colors';
import { CheckButton } from '../../../common/components/elements';
import { SeatClass, TravellerCountInfo } from '../../../common/models';
import { validTravellerCount } from '../../utils';
import styles from '../styles.module.scss';
import { CountBox, TypeBox, TypeLine, ValueBox, ValueControl } from './styles';
import { MY_TOUR } from '../../../../constants';

const FULL_WIDTH = '340px';
const MIN_WIDTH = '200px';

function getInputStr(seatClass: SeatClass[], travellerInfo: TravellerCountInfo, intl: IntlShape) {
  return `${intl.formatMessage(
    {
      id: 'search.travellerInfo.adultCount',
    },
    { adultCount: travellerInfo.adultCount },
  )}${
    travellerInfo.childCount
      ? `, ${intl.formatMessage({ id: 'search.travellerInfo.childCount' }, { childCount: travellerInfo.childCount })}`
      : ''
  }${
    travellerInfo.babyCount
      ? `, ${intl.formatMessage({ id: 'search.travellerInfo.babyCount' }, { babyCount: travellerInfo.babyCount })}`
      : ''
  }${seatClass.length ? `, ${map(seatClass, intl.locale.startsWith('vi') ? 'v_name' : 'i_name').join(', ')}` : ''}`;
}

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  overflow: hidden;
  min-width: 100%;
`;

const InputContainer = styled.div<{ isFocused: boolean }>`
  display: flex;
  align-items: center;
  padding-right: 2px;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  color: ${BLACK_TEXT};
  border-color: ${props => (props.isFocused ? props.theme.primary : GREY)};
  background-color: white;
  box-shadow: ${props => (props.isFocused ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem` : '')};
  border-color: ${props => (props.isFocused ? props.theme.primary : '')};
`;

interface Props extends WrappedComponentProps {
  icon: React.ReactNode;
  minimizedWidth?: string;
  travellerInfo: TravellerCountInfo;
  ticketClasses: SeatClass[];
  onChange(newValue: TravellerCountInfo): void;
  onSeatChange?: (newValue: SeatClass[]) => void;
  allTicketClasses: SeatClass[];
  lang: string;
  compact?: boolean;
  oneWayControl?: React.ReactNode;
  fullWidth?: string;
  translate?: string;
}

interface State {
  isFocused: boolean;
}

class TravellerCountInfoBox extends PureComponent<Props, State> {
  state = {
    isFocused: false,
  };

  parent = React.createRef<HTMLDivElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        this.parent.current && this.parent.current.focus();
        return;
      }
    }
    this.setState({ isFocused: false });
  };

  changeSeat(seat: SeatClass) {
    const { onSeatChange, ticketClasses } = this.props;

    const classes = [...ticketClasses];
    const classesCodes = map(classes, 'code');

    if (classesCodes.indexOf(seat.code) === -1) {
      classes.push(seat);
    } else {
      remove(classes, obj => obj.code === seat.code);
    }

    if (onSeatChange) {
      onSeatChange(classes);
    }
  }

  render() {
    const { isFocused } = this.state;
    const {
      minimizedWidth,
      icon,
      travellerInfo,
      ticketClasses,
      intl,
      onChange,
      allTicketClasses,
      lang,
      compact,
      oneWayControl,
      fullWidth,
      translate,
    } = this.props;
    return (
      <div
        style={{
          maxWidth: fullWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          flex: 1,
          height: '86px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
          transition: 'all 0.1s linear',
          transform: isFocused && translate ? translate : 'translateX(0)',
          zIndex: isFocused ? 1 : 0,
        }}
        role="group"
        ref={this.parent}
        tabIndex={-1}
        onFocus={e => e.target === e.currentTarget && this.setState({ isFocused: true })}
        onBlur={this.onBlur}
      >
        <Wrapper
          style={{
            maxWidth: isFocused ? FULL_WIDTH : minimizedWidth || FULL_WIDTH,
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
          }}
        >
          <div
            style={{
              padding: '8px 12px',
              backgroundColor: isFocused ? 'white' : 'transparent',
            }}
          >
            <div
              style={{
                display: 'flex',
                height: '30px',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id={compact ? 'search.travellerInfoCompact' : 'search.travellerInfo'} />
              </Typography>
              {oneWayControl}
            </div>
            <InputContainer isFocused={isFocused}>
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  flex: 1,
                  overflow: 'hidden',
                }}
              >
                <Typography
                  variant="body2"
                  noWrap
                  style={{
                    cursor: 'pointer',
                  }}
                >
                  {getInputStr(ticketClasses, travellerInfo, intl)}
                </Typography>
              </div>
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              key={1}
              style={{
                transition: 'all 300ms ease',
                textAlign: 'start',
                transformOrigin: '0 0',
                backgroundColor: 'white',
                padding: '0 12px 8px 12px',
                minWidth: '340px',
              }}
            >
              {!compact && (
                <div>
                  <div style={{ display: 'flex', alignItems: 'center', height: '40px' }}>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="search.travellerInfo.seatClass" />
                    </Typography>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '300px',
                      whiteSpace: 'nowrap',
                      flexWrap: 'wrap',
                      margin: 'auto',
                    }}
                  >
                    {allTicketClasses &&
                      allTicketClasses.map(item => {
                        const seatClassCodeArr = map(ticketClasses, 'code');
                        const selected = seatClassCodeArr.indexOf(item.code) !== -1;

                        return (
                          <CheckButton
                            key={item.code}
                            active={selected}
                            onClick={() => this.changeSeat(item)}
                            style={{ marginTop: '3px' }}
                            backgroundColor={MY_TOUR ? 'primary' : 'secondary'}
                          >
                            {lang.startsWith('vi') ? item.v_name : item.i_name}
                          </CheckButton>
                        );
                      })}
                  </div>
                </div>
              )}
              <div>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    height: '40px',
                    paddingTop: '12px',
                  }}
                >
                  <Typography variant="subtitle2">
                    <FormattedMessage id="search.travellerInfo.travellers" />
                  </Typography>
                </div>
                <CountBox>
                  <TypeBox>
                    <TypeLine>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.adult" />
                      </Typography>{' '}
                    </TypeLine>

                    <TypeLine style={{ color: GREY }}>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.adultDef" />
                      </Typography>
                    </TypeLine>
                  </TypeBox>
                  <ValueControl>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount - 1,
                          travellerInfo.childCount,
                          travellerInfo.babyCount,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, adultCount: travellerInfo.adultCount - 1 })}
                    >
                      <RemoveCircleOutline
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount - 1,
                            travellerInfo.childCount,
                            travellerInfo.babyCount,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                    <ValueBox>{travellerInfo.adultCount}</ValueBox>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount + 1,
                          travellerInfo.childCount,
                          travellerInfo.babyCount,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, adultCount: travellerInfo.adultCount + 1 })}
                    >
                      <AddCircle
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount + 1,
                            travellerInfo.childCount,
                            travellerInfo.babyCount,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                  </ValueControl>
                </CountBox>
                <CountBox>
                  <TypeBox>
                    <TypeLine>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.children" />
                      </Typography>
                    </TypeLine>

                    <TypeLine style={{ color: GREY }}>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.childrenDef" />
                      </Typography>
                    </TypeLine>
                  </TypeBox>
                  <ValueControl>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount - 1,
                          travellerInfo.babyCount,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, childCount: travellerInfo.childCount - 1 })}
                    >
                      <RemoveCircleOutline
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount,
                            travellerInfo.childCount - 1,
                            travellerInfo.babyCount,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                    <ValueBox>{travellerInfo.childCount}</ValueBox>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount + 1,
                          travellerInfo.babyCount,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, childCount: travellerInfo.childCount + 1 })}
                    >
                      <AddCircle
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount,
                            travellerInfo.childCount + 1,
                            travellerInfo.babyCount,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                  </ValueControl>
                </CountBox>
                <CountBox>
                  <TypeBox>
                    <TypeLine>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.baby" />
                      </Typography>
                    </TypeLine>

                    <TypeLine style={{ color: GREY }}>
                      <Typography variant="body2">
                        <FormattedMessage id="search.travellerInfo.babyDef" />
                      </Typography>
                    </TypeLine>
                  </TypeBox>
                  <ValueControl>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount,
                          travellerInfo.babyCount - 1,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, babyCount: travellerInfo.babyCount - 1 })}
                    >
                      <RemoveCircleOutline
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount,
                            travellerInfo.childCount,
                            travellerInfo.babyCount - 1,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                    <ValueBox>{travellerInfo.babyCount}</ValueBox>
                    <IconButton
                      size="small"
                      disabled={
                        !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount,
                          travellerInfo.babyCount + 1,
                        )
                      }
                      onClick={() => onChange({ ...travellerInfo, babyCount: travellerInfo.babyCount + 1 })}
                    >
                      <AddCircle
                        style={{
                          fontSize: '16px',
                          color: !validTravellerCount(
                            travellerInfo.adultCount,
                            travellerInfo.childCount,
                            travellerInfo.babyCount + 1,
                          )
                            ? GREY
                            : undefined,
                        }}
                        color="primary"
                      />
                    </IconButton>
                  </ValueControl>
                </CountBox>
                <div style={{ marginBottom: '10px', textAlign: 'end', position: 'relative' }}>
                  <MUIButton size="large" variant="contained" color="secondary">
                    <FormattedMessage id="done" />
                  </MUIButton>
                  <div
                    style={{
                      position: 'absolute',
                      top: 0,
                      bottom: 0,
                      right: 0,
                      width: '50%',
                      cursor: 'pointer',
                    }}
                    onClick={() => {
                      this.setState({ isFocused: false });
                      this.parent.current && this.parent.current.blur();
                    }}
                  />
                </div>
              </div>
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default injectIntl(TravellerCountInfoBox);
