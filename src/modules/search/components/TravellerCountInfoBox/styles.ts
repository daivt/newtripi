import { Button as MUIButton } from '@material-ui/core';
import styled from 'styled-components';
import { DARK_GREY } from '../../../../colors';
import { MUI_THEME } from '../../../../setupTheme';

export const CountBox = styled.div`
  height: 52px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TypeBox = styled.div`
  height: 40px;
`;

export const TypeLine = styled.div`
  height: 20px;
  display: flex;
  align-items: center;
`;

export const ValueBox = styled.div`
  width: 44px;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ValueControl = styled.div`
  display: flex;
  align-items: center;
`;

export const Button = styled(MUIButton)<{ selected: boolean }>`
  &&& {
    background-color: ${props => (props.selected ? props.theme.secondary : undefined)};
    border-radius: 15px;
    box-shadow: none;
    padding: 3px 8px;
    text-transform: none;
    color: ${props => (props.selected ? '#fff' : DARK_GREY)};
    border-color: ${props => (props.selected ? props.theme.secondary : undefined)};
    font-size: ${MUI_THEME.typography.body2.fontSize};
    min-width: 115px;
  }
`;
