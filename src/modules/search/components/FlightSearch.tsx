import { Button, Container, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { PRIMARY } from '../../../colors';
import { MY_TOUR, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import airportSvg from '../../../svg/airport.svg';
import { ReactComponent as IconFlightDown } from '../../../svg/ic_flight_down.svg';
import { ReactComponent as IconFlightUp } from '../../../svg/ic_flight_up.svg';
import { ReactComponent as IcSearch } from '../../../svg/ic_search.svg';
import { ReactComponent as TravellerSvg } from '../../../svg/ic_traveller.svg';
import Swap from '../../common/components/Swap';
import { Airport } from '../../common/models';
import { setFlightGeneralInfo } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { FlightFilterState, setFilter } from '../../result/redux/flightResultReducer';
import { NotableAirports } from '../constants';
import { setDates, setDestination, setOrigin, setSeatClass, setTravellerCountInfo } from '../redux/flightSearchReducer';
import {
  addFlightSearchToHistory,
  FlightSearchParamsError,
  stringifyFlightSearchStateAndFilterParams,
  validateFlightSearchParams,
} from '../utils';
import AsyncSelect from './AsyncSelect';
import FlightChooseDate from './FlightChooseDate';
import TravellerCountInfoBox from './TravellerCountInfoBox';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  home?: boolean;
}

export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2">
          {option.location}
          &nbsp;&#40;
          {option.code}
          &#41;
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}

const noError = { origin: false, destination: false, date: false };

class FlightSearch extends PureComponent<Props, { error: FlightSearchParamsError; tick: boolean }> {
  dest = React.createRef<HTMLElement>();

  date = React.createRef<HTMLDivElement>();

  constructor(props: Props) {
    super(props);
    this.state = {
      error: noError,
      tick: true,
    };
  }

  componentDidMount() {
    const { dispatch, common } = this.props;
    if (!common.generalInfo.flight) {
      dispatch(fetchThunk(API_PATHS.flightGeneralInfo, 'get', false)).then(json =>
        dispatch(setFlightGeneralInfo(json.data)),
      );
    }
  }

  render() {
    const { dispatch, search, allTicketClasses, lang, home, router, locale } = this.props;
    const { error, tick } = this.state;
    return (
      <div
        style={{
          background: MY_TOUR
            ? 'linear-gradient(to top, #ffffff, rgba(60, 193, 244, 0.94) 40%, #0066da)'
            : !home
            ? PRIMARY
            : undefined,
          color: !home ? 'white' : undefined,
          position: 'relative',
          zIndex: 1000,
        }}
      >
        <Container
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            padding: '16px 0 0 18px',
            height: '140px',
          }}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'flex-end',
              flex: 1,
              minWidth: '430px',
            }}
          >
            <AsyncSelect<Airport>
              key={`origin${tick}`}
              error={error.origin}
              minimizedWidth="215px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str.trim())));
                return json.data;
              }}
              icon={<IconFlightUp />}
              defaultValue={search.origin ? getLabel(search.origin) : ''}
              title={<FormattedMessage id="search.chooseOrigin" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseOrigin' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              onSelect={airport => {
                this.setState({
                  error: { ...error, origin: false },
                });
                dispatch(setOrigin(airport));
                if (airport && this.dest.current) {
                  this.dest.current.focus();
                }
              }}
            />
            <div
              style={{
                padding: '8px 0',
                width: '0px',
                overflow: 'visible',
                position: 'relative',
                zIndex: 10,
                display: 'flex',
                justifyContent: 'center',
                height: '40px',
                boxSizing: 'content-box',
                alignItems: 'center',
              }}
            >
              <Swap
                home={home}
                onClick={async () => {
                  const { origin } = search;
                  dispatch(setOrigin(search.destination));
                  dispatch(setDestination(origin));
                  this.setState(state => ({
                    tick: !state.tick,
                    error: { ...error, destination: error.origin, origin: error.destination },
                  }));
                }}
              />
            </div>
            <AsyncSelect<Airport>
              key={`dest${tick}`}
              error={error.destination}
              minimizedWidth="215px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str.trim())));
                return json.data;
              }}
              icon={<IconFlightDown />}
              defaultValue={search.destination ? getLabel(search.destination) : ''}
              title={<FormattedMessage id="search.chooseDestination" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseDestination' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              iref={this.dest}
              onSelect={airport => {
                this.setState({
                  error: { ...error, destination: false },
                });
                dispatch(setDestination(airport));
                if (airport && this.date.current) {
                  this.date.current.focus();
                }
              }}
            />
          </div>
          <FlightChooseDate
            locale={locale}
            onChange={(depart, returnDate) => {
              this.setState({ error: { ...error, date: false } });
              dispatch(setDates(depart, returnDate));
            }}
            departureDate={search.departureDate}
            returnDate={search.returnDate}
            minimizedWidth="324px"
            error={error.date}
            key={error.date ? `date${tick}` : 'date'}
            iref={this.date}
          />

          <TravellerCountInfoBox
            onChange={travellerInfo => dispatch(setTravellerCountInfo(travellerInfo))}
            onSeatChange={val => dispatch(setSeatClass(val))}
            travellerInfo={search.travellerCountInfo}
            ticketClasses={search.seatClass}
            icon={<TravellerSvg />}
            minimizedWidth="200px"
            fullWidth="270px"
            allTicketClasses={allTicketClasses}
            lang={lang}
            translate="translateX(-40px)"
          />

          <div
            style={{
              padding: '8px 0',
              height: '86px',
              display: 'flex',
              alignItems: 'flex-end',
              marginLeft: '12px',
              marginRight: '18px',
            }}
          >
            <Button
              size="large"
              // style={{ background: ORANGE }}
              color="secondary"
              variant="contained"
              onClick={() => {
                const currentlyError = validateFlightSearchParams(search);
                if (currentlyError.date || currentlyError.destination || currentlyError.origin) {
                  this.setState({ error: currentlyError, tick: !tick });
                } else {
                  const params = stringifyFlightSearchStateAndFilterParams(search);
                  if (`?${params}` !== router.location.search) {
                    addFlightSearchToHistory(search);

                    const filter: FlightFilterState = {
                      ...this.props.filter,
                      airline: [],
                    };
                    dispatch(setFilter(filter));

                    dispatch(push({ pathname: `${ROUTES.flight.flightResult}`, search: `?${params}` }));
                  }
                }
              }}
            >
              <IcSearch
                className="svgFillAll"
                role="search"
                style={{ width: '24px', height: '24px', stroke: MY_TOUR ? 'black' : 'white' }}
              />
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    search: state.search.flight,
    allTicketClasses: state.common.generalInfo.flight ? state.common.generalInfo.flight.ticketclass : [],
    lang: state.intl.locale.substring(0, 2),
    filter: state.result.flight.filterParams,
    router: state.router,
    common: state.common,
    locale: state.intl.locale,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightSearch));
