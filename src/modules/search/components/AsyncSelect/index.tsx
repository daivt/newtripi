import { Input, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import lodash from 'lodash';
import React, { PureComponent } from 'react';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY, PRIMARY, RED } from '../../../../colors';
import { some } from '../../../../constants';
import { MUI_THEME } from '../../../../setupTheme';
import { VIBRATE_KEYFRAME } from '../../constants';
import styles from '../styles.module.scss';

const FULL_WIDTH = '376px';

const Option = styled.div`
  :hover {
    background: ${fade(PRIMARY, 0.15)};
  }
`;

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  overflow: hidden;
`;

const InputContainer = styled.div<{ isFocused: boolean; error: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  height: 40px;
  padding-right: 9px;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : GREY)};
  background-color: white;
  box-shadow: ${props =>
    props.isFocused
      ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem`
      : props.error
      ? `${fade(RED, 0.25)} 0 0 0 0.2rem`
      : ''};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : '')};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: ${props => (props.error ? '5' : '0')};
`;

interface Props<T extends some> {
  title?: React.ReactNode;
  defaultValue?: string;
  placeholder: string;
  optionHeader: React.ReactNode;
  focusOptionHeader: React.ReactNode;
  icon: React.ReactNode;
  loadOptions: (input: string) => Promise<T[]>;
  focusOptions: T[];
  getLabel: (option: T) => string;
  renderOption(option: T, index: number, length: number): React.ReactNode;
  minimizedWidth?: string;
  fullWidth?: string;
  onSelect(option?: T): void;
  onTextEnter?: (text: string) => void;
  inputChange?: (input: string) => void;
  error: boolean;
  dropdownMaxHeight?: string;
  iref?: React.RefObject<HTMLElement>;
}

interface State<T> {
  isFocused: boolean;
  input: string;
  options: T[];
  currentOptionIndex: number;
}

class AsyncSelect<T extends Object> extends PureComponent<Props<T>, State<T>> {
  state = {
    isFocused: false,
    input: this.props.defaultValue || '',
    options: [] as T[],
    currentOptionIndex: -1,
  };

  loadOptions = async (input: string) => {
    const { loadOptions } = this.props;

    const options = await loadOptions(input);

    if (options.length > 0) {
      this.setState({ options, currentOptionIndex: -1 });
    } else if (input.trim() === '') {
      this.setState({
        options: this.props.focusOptions,
        currentOptionIndex: -1,
      });
    }
  };

  debouncedLoadOptions = lodash.debounce(this.loadOptions, 500, { trailing: true, leading: false });

  inputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target.value;
    const { inputChange } = this.props;
    if (inputChange) {
      inputChange(input);
    }

    this.setState({ input });
    this.props.onSelect(undefined);
    this.debouncedLoadOptions(input);
  };

  onFocus = async (e: React.FocusEvent<HTMLInputElement>) => {
    e.target.select();
    if (this.state.input !== '' && this.state.options.length > 0) {
      /*Extra feature need more Testing*/
      this.setState({ currentOptionIndex: -1, isFocused: true });
    } else {
      this.setState({
        options: this.props.focusOptions,
        currentOptionIndex: -1,
        isFocused: true,
      });
    }
  };

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        return;
      }
    }
    this.setState({ isFocused: false });
  };

  keyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 40) {
      const { options, currentOptionIndex } = this.state;
      if (currentOptionIndex + 1 < options.length) {
        this.setState({ currentOptionIndex: currentOptionIndex + 1 });
      }
    } else if (e.keyCode === 38) {
      const { currentOptionIndex } = this.state;
      if (currentOptionIndex >= 0) {
        this.setState({ currentOptionIndex: currentOptionIndex - 1 });
      }
    } else if (e.keyCode === 13) {
      const { options, currentOptionIndex } = this.state;
      const { onTextEnter } = this.props;
      if (currentOptionIndex >= 0) {
        const { getLabel } = this.props;
        this.setState({ input: getLabel(options[currentOptionIndex]), isFocused: false });
        this.props.onSelect(options[currentOptionIndex]);
      } else if (onTextEnter) {
        this.setState({ isFocused: false });
        onTextEnter(this.state.input);
      }
    }
  };

  componentDidUpdate(prevProps: Props<T>) {
    if (prevProps.focusOptions !== this.props.focusOptions) {
      if (this.state.options === prevProps.focusOptions) {
        this.setState({ options: this.props.focusOptions });
      }
    }
  }

  render() {
    const { input, options, currentOptionIndex, isFocused } = this.state;
    const {
      icon,
      title,
      optionHeader,
      focusOptionHeader,
      getLabel,
      renderOption,
      minimizedWidth,
      fullWidth,
      focusOptions,
      error,
      dropdownMaxHeight,
      iref,
    } = this.props;
    return (
      <div
        style={{
          minWidth: minimizedWidth || fullWidth || FULL_WIDTH,
          flex: 1,
          maxWidth: fullWidth || FULL_WIDTH,
          height: title ? '86px' : '56px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
        }}
        tabIndex={-1}
        onBlur={this.onBlur}
      >
        <Wrapper
          style={{
            minWidth: isFocused ? fullWidth || FULL_WIDTH : '100%',
            width: '100%',
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
          }}
        >
          <div
            style={{
              padding: '8px 12px',
              backgroundColor: isFocused ? 'white' : 'transparent',
            }}
          >
            {title && (
              <div style={{ display: 'flex', height: '30px', alignItems: 'center' }}>
                <Typography variant="body2">{title}</Typography>
              </div>
            )}
            <InputContainer isFocused={isFocused} error={error}>
              {icon && <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>{icon}</div>}
              <Input
                fullWidth
                disableUnderline
                value={input}
                onFocus={this.onFocus}
                onChange={this.inputChange}
                onKeyDown={this.keyDown}
                placeholder={this.props.placeholder}
                inputProps={{
                  style: {
                    outline: 'none',
                    border: 'none',
                    height: '24px',
                    fontSize: MUI_THEME.typography.body2.fontSize,
                  },
                }}
                inputRef={iref}
              />
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              key={1}
              style={{
                transition: 'all 300ms ease',
                textAlign: 'start',
                transformOrigin: '0 0',
                backgroundColor: 'white',
                paddingBottom: '10px',
                maxHeight: dropdownMaxHeight || '360px',
                overflowY: 'auto',
                overflowX: 'hidden',
              }}
            >
              <Typography
                variant="subtitle2"
                style={{
                  padding: '8px 12px',
                }}
              >
                {focusOptions === options ? focusOptionHeader : optionHeader}
              </Typography>
              {options.map((option, index) => (
                <Option
                  key={`${JSON.stringify(option)}`}
                  onClick={() => {
                    this.setState({
                      currentOptionIndex: index,
                      input: getLabel(options[index]),
                      options: [],
                      isFocused: false,
                    });
                    this.props.onSelect(options[index]);
                  }}
                  style={{
                    cursor: 'pointer',
                    padding: '0 12px',
                    background: index === currentOptionIndex ? fade(PRIMARY, 0.35) : undefined,
                  }}
                >
                  {renderOption(option, index, options.length)}
                </Option>
              ))}
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default AsyncSelect;
