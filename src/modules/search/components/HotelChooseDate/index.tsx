import { Button, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import moment, { Moment } from 'moment';
import React, { PureComponent } from 'react';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { BLACK_TEXT, GREY, RED } from '../../../../colors';
import calendarSvg from '../../../../svg/calendar.svg';
import { renderMonthText } from '../../../common/utils';
import { VIBRATE_KEYFRAME } from '../../constants';
import styles from '../styles.module.scss';

const FULL_WIDTH = '600px';
const MIN_WIDTH = '250px';

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  min-width: 100%;
  overflow: hidden;
`;

const InputContainer = styled.div<{ isFocused: boolean; error: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-radius: 4px;
  border-style: solid;
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : GREY)};
  background-color: white;
  box-shadow: ${props =>
    props.isFocused
      ? `${fade(props.theme.primary, 0.25)} 0 0 0 0.2rem`
      : props.error
      ? `${fade(RED, 0.25)} 0 0 0 0.2rem`
      : ''};
  border-color: ${props => (props.isFocused ? props.theme.primary : props.error ? RED : '')};
  color: ${BLACK_TEXT};
  animation-name: ${VIBRATE_KEYFRAME};
  animation-duration: 0.175s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: ${props => (props.error ? '5' : '0')};
`;
interface Props extends WrappedComponentProps {
  minimizedWidth?: string;
  onChange(checkInDate: Moment, checkOutDate: Moment): void;
  checkInDate: Moment;
  checkOutDate: Moment;
  error: boolean;
  iref?: React.RefObject<HTMLDivElement>;
  locale: string;
}

interface State {
  isFocused: boolean;
  focusedInput: FocusedInputShape;
  checkInDate: Moment;
  checkOutDate?: Moment;
}

class HotelChooseDate extends PureComponent<Props, State> {
  state: State = {
    isFocused: false,
    focusedInput: 'startDate' as FocusedInputShape,
    checkInDate: this.props.checkInDate,
    checkOutDate: this.props.checkOutDate,
  };

  parent = this.props.iref || React.createRef<HTMLDivElement>();

  firefoxWorkaroundHelper = React.createRef<HTMLButtonElement>();

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        if (this.parent.current) {
          this.parent.current.focus();
        }
        return;
      }
    }
    this.setState(
      state => ({
        checkInDate: state.checkInDate,
        checkOutDate: state.checkOutDate || state.checkInDate.clone().add(1, 'days'),
        isFocused: false,
      }),
      () => {
        this.props.onChange(this.state.checkInDate, this.state.checkOutDate!);
      },
    );
  };

  render() {
    const { isFocused } = this.state;
    const { minimizedWidth, error } = this.props;
    const { checkInDate, checkOutDate } = this.state;
    return (
      <div
        style={{
          maxWidth: minimizedWidth || FULL_WIDTH,
          minWidth: MIN_WIDTH,
          flex: 1,
          height: '86px',
          position: 'relative',
          outline: 'none',
          color: isFocused ? BLACK_TEXT : undefined,
        }}
        role="group"
        tabIndex={-1}
        ref={this.parent}
        onBlur={this.onBlur}
        onFocus={e => {
          if (e.target !== this.parent.current && this.firefoxWorkaroundHelper.current) {
            this.firefoxWorkaroundHelper.current.focus();
          }
          /* need this check for oneWay checkbox */ if (e.target === e.currentTarget) {
            this.setState({ isFocused: true });
          }
        }}
      >
        <button
          type="button"
          onFocus={e => e.stopPropagation()}
          ref={this.firefoxWorkaroundHelper}
          style={{ position: 'absolute', opacity: 0, zIndex: -1 }}
        >
          FF
        </button>
        <Wrapper
          style={{
            maxWidth: isFocused ? FULL_WIDTH : minimizedWidth || FULL_WIDTH,
            boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
            zIndex: isFocused ? 100 : 0,
            backgroundColor: isFocused ? 'rgba(255,255,255,1)' : 'rgba(255,255,255,0)',
          }}
        >
          <div
            style={{
              padding: '8px 12px',
            }}
          >
            <Typography
              variant="body2"
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                height: '30px',
              }}
            >
              <FormattedMessage id="search.stayDuration" />
            </Typography>
            <InputContainer isFocused={isFocused} error={error}>
              <div style={{ margin: '0 10px', display: 'flex', alignItems: 'center' }}>
                <img alt="" src={calendarSvg} />
              </div>
              <div
                style={{
                  display: 'flex',
                  height: '24px',
                  margin: '7px 1px',
                  paddingRight: '5px',
                  flex: 1,
                }}
              >
                <Typography
                  variant="body2"
                  style={{
                    cursor: 'pointer',
                  }}
                >
                  {moment(checkInDate.valueOf()).format('L')}
                </Typography>
                <>
                  &nbsp;&nbsp;-&nbsp;&nbsp;
                  <Typography
                    variant="body2"
                    style={{
                      cursor: 'pointer',
                    }}
                  >
                    {checkOutDate
                      ? moment(checkOutDate.valueOf()).format('L')
                      : this.props.intl.formatMessage({ id: 'notChosen' })}
                  </Typography>
                </>
              </div>
            </InputContainer>
          </div>
          <CSSTransition
            timeout={200}
            in={isFocused}
            classNames={{
              enter: styles.enter,
              enterActive: styles.enterActive,
              exit: styles.leave,
              exitActive: styles.leaveActive,
            }}
            unmountOnExit
          >
            <div
              key={1}
              style={{
                transition: 'all 300ms ease',
                backgroundColor: 'white',
                padding: '12px 11px',
              }}
            >
              <DayPickerRangeController
                hideKeyboardShortcutsPanel
                daySize={36}
                numberOfMonths={2}
                startDate={checkInDate}
                endDate={checkOutDate || null}
                focusedInput={this.state.focusedInput}
                onDatesChange={({ startDate, endDate }) => {
                  const newCheckInDate = startDate || moment();
                  this.setState({ checkInDate: newCheckInDate, checkOutDate: endDate || undefined });
                  if (newCheckInDate && endDate) {
                    this.props.onChange(newCheckInDate, endDate);
                  }
                }}
                onFocusChange={focusedInput =>
                  this.setState({
                    focusedInput: focusedInput || 'startDate',
                  })
                }
                isOutsideRange={date =>
                  moment()
                    .startOf('day')
                    .isAfter(date)
                }
                minimumNights={1}
                renderMonthText={renderMonthText}
              />
              <div style={{ textAlign: 'end', marginTop: '12px' }}>
                <Button
                  style={{ textTransform: 'none' }}
                  size="large"
                  color="secondary"
                  variant="contained"
                  onClick={() => this.parent.current && this.parent.current.blur()}
                >
                  <FormattedMessage id="done" />
                </Button>
              </div>
            </div>
          </CSSTransition>
        </Wrapper>
      </div>
    );
  }
}

export default injectIntl(HotelChooseDate);
