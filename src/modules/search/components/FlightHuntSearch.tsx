import { Button, Container, FormControlLabel, Typography } from '@material-ui/core';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import { withStyles } from '@material-ui/styles';
import { push } from 'connected-react-router';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { PRIMARY } from '../../../colors';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import airportSvg from '../../../svg/airport.svg';
import { ReactComponent as IconFlightDown } from '../../../svg/ic_flight_down.svg';
import { ReactComponent as IconFlightUp } from '../../../svg/ic_flight_up.svg';
import icSearch from '../../../svg/ic_search.svg';
import { ReactComponent as TravellerSvg } from '../../../svg/ic_traveller.svg';
import Swap from '../../common/components/Swap';
import { Airport } from '../../common/models';
import { setFlightGeneralInfo } from '../../common/redux/reducer';
import { fetchThunk } from '../../common/redux/thunks';
import { setDates } from '../../result/redux/flightHuntResultReducer';
import { NotableAirports } from '../constants';
import { setDestination, setOneWay, setOrigin, setTravellerCountInfo } from '../redux/flightHuntSearchReducer';
import { FlightHuntParamsError, stringifyFlightHuntSearchParams, validateFlightHuntSearchParams } from '../utils';
import AsyncSelect from './AsyncSelect';
import TravellerCountInfoBox from './TravellerCountInfoBox';

interface Props extends ReturnType<typeof mapStateToProps>, WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  home?: boolean;
}

const CustomCheckbox = withStyles({
  root: {
    color: 'inherit',
  },
})((props: CheckboxProps) => <Checkbox {...props} />);

export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <img alt="" style={{ margin: '0 15px 0 5px' }} src={airportSvg} />
      <div>
        <Typography variant="body2">
          {option.location}
          &nbsp;&#40;
          {option.code}
          &#41;
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}

class FlightHuntSearch extends PureComponent<
  Props,
  {
    error: FlightHuntParamsError;
    tick: boolean;
  }
> {
  dest = React.createRef<HTMLElement>();

  constructor(props: Props) {
    super(props);
    this.state = {
      error: { origin: false, destination: false },
      tick: true,
    };
  }

  componentDidMount() {
    const { dispatch, common } = this.props;
    if (!common.generalInfo.flight) {
      dispatch(fetchThunk(API_PATHS.flightGeneralInfo, 'get', false)).then(json =>
        dispatch(setFlightGeneralInfo(json.data)),
      );
    }
  }

  render() {
    const { dispatch, lang, home, params, filterParams } = this.props;
    const { error, tick } = this.state;
    return (
      <div
        style={{
          background: !home ? PRIMARY : undefined,
          color: !home ? 'white' : undefined,
          position: 'relative',
          zIndex: 1000,
        }}
      >
        <Container
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: '16px 0 0 18px',
            height: '140px',
          }}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'flex-end',
              minWidth: '430px',
            }}
          >
            <AsyncSelect<Airport>
              error={error.origin}
              key={`origin${tick}$`}
              minimizedWidth="235px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              icon={<IconFlightUp />}
              defaultValue={params.origin ? getLabel(params.origin) : ''}
              title={<FormattedMessage id="search.chooseOrigin" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseOrigin' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              onSelect={airport => {
                this.setState({
                  error: { ...error, origin: false },
                });
                dispatch(setOrigin(airport));
                if (airport && this.dest.current) {
                  this.dest.current.focus();
                }
              }}
            />
            <div
              style={{
                padding: '8px 0',
                width: '0px',
                overflow: 'visible',
                position: 'relative',
                zIndex: 10,
                display: 'flex',
                justifyContent: 'center',
                height: '40px',
                boxSizing: 'content-box',
                alignItems: 'center',
              }}
            >
              <Swap
                home={home}
                onClick={async () => {
                  dispatch(setOrigin(params.destination));
                  dispatch(setDestination(params.origin));
                  this.setState(state => ({
                    tick: !state.tick,
                    error: { destination: error.origin, origin: error.destination },
                  }));
                }}
              />
            </div>
            <AsyncSelect<Airport>
              error={error.destination}
              key={`dest${tick}`}
              iref={this.dest}
              minimizedWidth="235px"
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              icon={<IconFlightDown />}
              defaultValue={params.destination ? getLabel(params.destination) : ''}
              title={<FormattedMessage id="search.chooseDestination" />}
              optionHeader={<FormattedMessage id="search.airports" />}
              focusOptionHeader={<FormattedMessage id="search.notablePlaces" />}
              placeholder={this.props.intl.formatMessage({ id: 'search.chooseDestination' })}
              focusOptions={NotableAirports}
              getLabel={getLabel}
              renderOption={renderOption}
              onSelect={airport => {
                this.setState({
                  error: { ...error, destination: false },
                });
                dispatch(setDestination(airport));
              }}
            />
          </div>

          <TravellerCountInfoBox
            onChange={travellerInfo => {
              dispatch(setTravellerCountInfo(travellerInfo));
            }}
            travellerInfo={params.travellerCountInfo}
            ticketClasses={[]}
            icon={<TravellerSvg />}
            minimizedWidth="285px"
            allTicketClasses={[]}
            lang={lang}
            compact
            oneWayControl={
              <FormControlLabel
                style={{ outline: 'none' }}
                tabIndex={-1}
                control={
                  <CustomCheckbox
                    color="secondary"
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={<CheckBoxIcon fontSize="small" style={{ background: 'white', borderRadius: '3px' }} />}
                    checked={!params.oneWay}
                    onChange={e => {
                      dispatch(setOneWay(!params.oneWay));
                    }}
                  />
                }
                label={
                  <Typography variant="caption" color="inherit">
                    <FormattedMessage id="search.twoWay" />
                  </Typography>
                }
                labelPlacement="start"
              />
            }
          />

          <div
            style={{
              padding: '8px 0',
              height: '86px',
              display: 'flex',
              alignItems: 'flex-end',
              marginLeft: '12px',
              marginRight: '18px',
            }}
          >
            <Button
              size="large"
              color="secondary"
              variant="contained"
              onClick={() => {
                const currentlyError = validateFlightHuntSearchParams(params);
                if (currentlyError.destination || currentlyError.origin) {
                  this.setState({ error: currentlyError, tick: !tick });
                } else {
                  dispatch(setDates(undefined, undefined));
                  dispatch(
                    push({
                      pathname: ROUTES.flight.flightHuntResult,
                      search: stringifyFlightHuntSearchParams(params, filterParams),
                    }),
                  );
                }
              }}
            >
              <img alt="search" src={icSearch} style={{ width: '24px', height: '24px' }} />
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    params: state.search.flightHunt,
    filterParams: state.result.flightHunt.filterParams,
    lang: state.intl.locale.substring(0, 2),
    common: state.common,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightHuntSearch));
