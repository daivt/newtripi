import { remove, uniq } from 'lodash';
import moment, { Moment } from 'moment';
import {
  DATE_FORMAT,
  FLIGHT_SEARCH_HISTORY,
  HOTEL_SEARCH_HISTORY,
  TOUR_SEARCH_HISTORY,
  TRAIN_SEARCH_HISTORY,
} from '../../constants';
import {
  FLIGHT_HUNT_PARAM_NAMES,
  FLIGHT_SEARCH_FILTER_PARAM_NAMES,
  FLIGHT_SEARCH_PARAM_NAMES,
  HOTEL_FILTER_PARAM_NAMES,
  HOTEL_SEARCH_PARAM_NAMES,
  TOUR_FILTER_PARAM_NAMES,
  TOUR_SEARCH_PARAM_NAMES,
  TRAIN_SEARCH_PARAM_NAMES,
} from '../common/constants';
import { defaultGuestCountInfo, SeatClass } from '../common/models';
import { isValidGuestInfo } from '../common/utils';
import { DEFAULT_HOTEL_FILTER_STATE, DEFAULT_TOUR_FILTER_STATE } from '../result/constants';
import { FlightHuntFilterState } from '../result/redux/flightHuntResultReducer';
import { FlightFilterState } from '../result/redux/flightResultReducer';
import { HotelFilterState, ValidHotelSearchParamsState } from '../result/redux/hotelResultReducer';
import { TourFilterState } from '../result/redux/tourResultReducer';
import { MAX_SEARCH_HISTORY } from './constants';
import { FlightHuntSearchState } from './redux/flightHuntSearchReducer';
import { FlightSearchParamsState } from './redux/flightSearchReducer';
import { HotelSearchParamsState } from './redux/hotelSearchReducer';
import { isTermInput, TourSearchParamsState } from './redux/tourSearchReducer';
import { TrainSearchParamsState } from './redux/trainSearchReducer';

/** ----------------Filght-------------------- */
export interface FlightSearchParamsError {
  origin: boolean;
  destination: boolean;
  date: boolean;
}

export interface TrainSearchParamsError extends FlightSearchParamsError {}

export function parseFlightSearchParams(params: URLSearchParams): FlightSearchParamsState {
  const destination = params.get(FLIGHT_SEARCH_PARAM_NAMES.destination);
  if (!destination) {
    throw new Error('No destination');
  }
  const destinationJson = JSON.parse(destination);

  const origin = params.get(FLIGHT_SEARCH_PARAM_NAMES.origin);
  if (!origin) {
    throw new Error('No origin');
  }
  const originJson = JSON.parse(origin);

  const departureDateStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.departureDate);
  if (!departureDateStr) {
    throw new Error('No departure date');
  }
  const departureDate = moment(departureDateStr, DATE_FORMAT);
  if (!departureDate.isValid() || departureDate.isBefore(moment().startOf('day'))) {
    throw new Error('Invalid departure date');
  }

  let returnDate;
  const returnDateStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.returnDate);
  if (returnDateStr) {
    returnDate = moment(returnDateStr, DATE_FORMAT);
    if (!returnDate.isValid() || returnDate.isBefore(departureDate)) {
      throw new Error('Invalid return date');
    }
  }
  const travellerInfoStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.travellerInfo);
  if (!travellerInfoStr) {
    throw new Error('No traveller info');
  }

  const travellerInfo = JSON.parse(travellerInfoStr);

  const classStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.seatClass);
  if (!classStr) {
    throw new Error('No seat class');
  }
  const seatClass: SeatClass[] = JSON.parse(classStr);

  if (!seatClass) {
    throw new Error('Invalid seat class');
  }

  return {
    returnDate,
    seatClass,
    departureDate,
    travellerCountInfo: travellerInfo,
    destination: destinationJson,
    origin: originJson,
  };
}

export function stringifyFlightSearchStateAndFilterParams(
  state: FlightSearchParamsState,
  filterParams?: FlightFilterState,
) {
  const arr = [];
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.destination}=${encodeURIComponent(JSON.stringify(state.destination))}`);
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.origin}=${encodeURIComponent(JSON.stringify(state.origin))}`);
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.departureDate}=${state.departureDate.format(DATE_FORMAT)}`);
  if (state.returnDate) {
    arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.returnDate}=${state.returnDate.format(DATE_FORMAT)}`);
  }
  arr.push(
    `${FLIGHT_SEARCH_PARAM_NAMES.travellerInfo}=${encodeURIComponent(JSON.stringify(state.travellerCountInfo))}`,
  );
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.seatClass}=${encodeURIComponent(JSON.stringify(state.seatClass))}`);
  if (filterParams) {
    arr.push(`${FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline}=${encodeURIComponent(JSON.stringify(filterParams.airline))}`);
  }
  return arr.join('&');
}

export function validateFlightSearchParams(params: FlightSearchParamsState): FlightSearchParamsError {
  let dateError = false;
  if (params.departureDate.isBefore(moment().startOf('day'))) {
    dateError = true;
  }
  if (params.returnDate && params.returnDate.startOf('days').isBefore(params.departureDate.startOf('days'))) {
    dateError = true;
  }

  const retval = { origin: !params.origin, destination: !params.destination, date: dateError };
  if (params.destination && params.origin && params.destination.code === params.origin.code) {
    retval.origin = true;
    retval.destination = true;
  }
  return retval;
}
function removeInValidFlightSearch(flightSearch: string[]): string[] {
  remove(flightSearch, search => {
    try {
      parseFlightSearchParams(new URLSearchParams(search));
      return false;
    } catch (_) {
      return true;
    }
  });

  return flightSearch;
}

export function addFlightSearchToHistory(search: FlightSearchParamsState) {
  const flightSearchStr = localStorage.getItem(FLIGHT_SEARCH_HISTORY) || false;

  const searchString = stringifyFlightSearchStateAndFilterParams(search);

  if (!flightSearchStr) {
    localStorage.setItem(FLIGHT_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let flightSearch: string[] = removeInValidFlightSearch(JSON.parse(flightSearchStr));
  flightSearch.unshift(searchString);

  flightSearch = [...uniq(flightSearch)];

  if (flightSearch.length > MAX_SEARCH_HISTORY) {
    flightSearch.pop();
  }

  localStorage.setItem(FLIGHT_SEARCH_HISTORY, JSON.stringify(flightSearch));
}

export function getFlightSearchHistory(): string[] {
  const flightSearchStr = localStorage.getItem(FLIGHT_SEARCH_HISTORY) || false;

  if (!flightSearchStr) {
    return [];
  }

  const flightSearch: string[] = removeInValidFlightSearch(JSON.parse(flightSearchStr));

  return flightSearch;
}
export function clearFlightSearchHistory() {
  localStorage.removeItem(FLIGHT_SEARCH_HISTORY);
}

/** ---------------------FlightHunt----------------------------- */

export interface FlightHuntParamsError {
  origin: boolean;
  destination: boolean;
}

export function parseFlightHuntFilterParams(filterParams: URLSearchParams): FlightHuntFilterState {
  const airlinesStr = filterParams.get(FLIGHT_HUNT_PARAM_NAMES.airlines);
  let airlineIds;
  if (airlinesStr && airlinesStr !== 'undefined') {
    airlineIds = JSON.parse(airlinesStr);
  }
  return {
    airlineIds,
  };
}

export function parseFlightHuntDateParams(params: URLSearchParams): { departureDate?: Moment; returnDate?: Moment } {
  let departureDate;
  const departureDateStr = params.get(FLIGHT_HUNT_PARAM_NAMES.departureDate);
  if (departureDateStr) {
    departureDate = moment(departureDateStr, DATE_FORMAT);
  }

  let returnDate;
  const returnDateStr = params.get(FLIGHT_HUNT_PARAM_NAMES.returnDate);
  if (returnDateStr) {
    returnDate = moment(returnDateStr, DATE_FORMAT);
  }
  return {
    departureDate,
    returnDate,
  };
}

export function parseFlightHuntSearchParams(params: URLSearchParams): FlightHuntSearchState {
  const destination = params.get(FLIGHT_HUNT_PARAM_NAMES.destination);
  if (!destination) {
    throw new Error('No destination');
  }
  const destinationJson = JSON.parse(destination);

  const origin = params.get(FLIGHT_HUNT_PARAM_NAMES.origin);
  if (!origin) {
    throw new Error('No origin');
  }
  const originJson = JSON.parse(origin);

  const travellerInfoStr = params.get(FLIGHT_HUNT_PARAM_NAMES.travellerInfo);
  if (!travellerInfoStr) {
    throw new Error('No traveller info');
  }
  const travellerInfo = JSON.parse(travellerInfoStr);

  const oneWayStr = params.get(FLIGHT_HUNT_PARAM_NAMES.oneWay);
  if (!oneWayStr) {
    throw new Error('No one way info');
  }
  const oneWay = JSON.parse(oneWayStr);
  return {
    oneWay,
    travellerCountInfo: travellerInfo,
    destination: destinationJson,
    origin: originJson,
  };
}

export function stringifyFlightHuntSearchParams(
  state: FlightHuntSearchState,
  filterParams: FlightHuntFilterState,
  departureDate?: moment.Moment,
  returnDate?: moment.Moment,
) {
  const arr = [];
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.airlines}=${encodeURIComponent(JSON.stringify(filterParams.airlineIds))}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.destination}=${encodeURIComponent(JSON.stringify(state.destination))}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.oneWay}=${state.oneWay}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.origin}=${encodeURIComponent(JSON.stringify(state.origin))}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.travellerInfo}=${encodeURIComponent(JSON.stringify(state.travellerCountInfo))}`);

  if (departureDate) {
    arr.push(`${FLIGHT_HUNT_PARAM_NAMES.departureDate}=${encodeURIComponent(departureDate.format(DATE_FORMAT))}`);
  }
  if (returnDate) {
    arr.push(`${FLIGHT_HUNT_PARAM_NAMES.returnDate}=${encodeURIComponent(returnDate.format(DATE_FORMAT))}`);
  }
  return arr.join('&');
}

export function stringifyFlightHuntResultParams(
  state: FlightHuntSearchState,
  filterParams: FlightHuntFilterState,
  departureDate?: moment.Moment,
  returnDate?: moment.Moment,
) {
  const arr = [];
  if (filterParams.airlineIds) {
    arr.push(
      `${FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline}=${encodeURIComponent(JSON.stringify(filterParams.airlineIds))}`,
    );
  }
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.destination}=${encodeURIComponent(JSON.stringify(state.destination))}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.oneWay}=${state.oneWay}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.origin}=${encodeURIComponent(JSON.stringify(state.origin))}`);
  arr.push(`${FLIGHT_HUNT_PARAM_NAMES.travellerInfo}=${encodeURIComponent(JSON.stringify(state.travellerCountInfo))}`);

  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.departureDate}=${departureDate && departureDate.format(DATE_FORMAT)}`);

  if (returnDate) {
    arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.returnDate}=${returnDate.format(DATE_FORMAT)}`);
  }
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.seatClass}=${encodeURIComponent(JSON.stringify([]))}`);
  return arr.join('&');
}

export function validateFlightHuntSearchParams(params: FlightHuntSearchState): FlightHuntParamsError {
  const retval = { origin: !params.origin, destination: !params.destination };
  if (params.destination && params.origin && params.destination.code === params.origin.code) {
    retval.origin = true;
    retval.destination = true;
  }
  return retval;
}
/** ----------------------Hotel---------------------*/
export interface HotelSearchParamsError {
  location: boolean;
  date: boolean;
  guestInfo: boolean;
}

export function parseHotelSearchParams(params: URLSearchParams, skipLocation = false): ValidHotelSearchParamsState {
  const locationStr = params.get(HOTEL_SEARCH_PARAM_NAMES.location);
  if (!locationStr && !skipLocation) {
    throw new Error('No location');
  }
  const location = locationStr ? JSON.parse(locationStr) : undefined;

  const checkInStr = params.get(HOTEL_SEARCH_PARAM_NAMES.checkIn);
  const checkIn = checkInStr ? moment(checkInStr, DATE_FORMAT) : moment().startOf('day');
  if (!checkIn.isValid() || checkIn.isBefore(moment().startOf('day'))) {
    throw new Error('Invalid checkin date');
  }

  const checkOutStr = params.get(HOTEL_SEARCH_PARAM_NAMES.checkOut);
  const checkOut = checkOutStr
    ? moment(checkOutStr, DATE_FORMAT)
    : moment()
        .startOf('day')
        .add(1, 'day');
  if (!checkOut.isValid() || !checkOut.isAfter(checkIn)) {
    throw new Error('Invalid checkout date');
  }

  const guestInfoStr = params.get(HOTEL_SEARCH_PARAM_NAMES.guestInfo);
  const guestInfo = guestInfoStr ? JSON.parse(guestInfoStr) : defaultGuestCountInfo;
  if (!isValidGuestInfo(guestInfo)) {
    throw new Error('Invalid guest info');
  }

  return { location, checkIn, checkOut, guestInfo };
}

export function parseHotelFilterParams(params: URLSearchParams): HotelFilterState {
  const retval = { ...DEFAULT_HOTEL_FILTER_STATE };

  let str = params.get(HOTEL_FILTER_PARAM_NAMES.area);
  if (str) {
    retval.subLocations = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.facility);
  if (str) {
    retval.facilities = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.price);
  if (str) {
    retval.price = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.star);
  if (str) {
    retval.stars = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.type);
  if (str) {
    retval.hotelTypes = JSON.parse(str);
  }

  return retval;
}

export function stringifyHotelSearchState(
  state: HotelSearchParamsState,
  filter?: HotelFilterState,
  sortBy?: string,
  skipLocation = false,
) {
  const arr = [];
  if (!skipLocation) {
    arr.push(`${HOTEL_SEARCH_PARAM_NAMES.location}=${encodeURIComponent(JSON.stringify(state.location))}`);
  }
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkIn}=${state.checkIn.format(DATE_FORMAT)}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkOut}=${state.checkOut.format(DATE_FORMAT)}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(state.guestInfo))}`);

  if (filter) {
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.area}=${encodeURIComponent(JSON.stringify(filter.subLocations))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.facility}=${encodeURIComponent(JSON.stringify(filter.facilities))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.price}=${encodeURIComponent(JSON.stringify(filter.price))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.star}=${encodeURIComponent(JSON.stringify(filter.stars))}`);
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.type}=${encodeURIComponent(JSON.stringify(filter.hotelTypes))}`);
  }

  if (sortBy) {
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.sort}=${encodeURIComponent(sortBy)}`);
  }

  return arr.join('&');
}

export function validateHotelSearchParams(params: HotelSearchParamsState): HotelSearchParamsError {
  const dateError = params.checkIn.isBefore(moment().startOf('day')) || !params.checkOut.isAfter(params.checkIn);
  const guestInfoError = params.guestInfo.childrenAges.includes(-1);
  return { location: !params.location, date: dateError, guestInfo: guestInfoError };
}

export function addHotelRecentSearch(search: HotelSearchParamsState) {
  const recentSearchData = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;
  const searchString = stringifyHotelSearchState(search);

  if (!recentSearchData) {
    localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let hotelSearch: string[] = JSON.parse(recentSearchData);

  hotelSearch.unshift(searchString);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  hotelSearch = [...uniq(hotelSearch)];

  if (hotelSearch.length > MAX_SEARCH_HISTORY) {
    hotelSearch.pop();
  }
  localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify(hotelSearch));
}

export function getHotelSearchHistory(): string[] {
  const hotelSearchStr = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;

  if (!hotelSearchStr) {
    return [];
  }

  const hotelSearch: string[] = JSON.parse(hotelSearchStr);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  return hotelSearch;
}
export function clearHotelSearchHistory() {
  localStorage.removeItem(HOTEL_SEARCH_HISTORY);
}
/** ---------------------Tour---------------------------- */
export function parseTourFilterParams(params: URLSearchParams): TourFilterState {
  const retval = { ...DEFAULT_TOUR_FILTER_STATE };

  let str = params.get(TOUR_FILTER_PARAM_NAMES.activityTags);
  if (str) {
    retval.activityTags = JSON.parse(str);
  }

  str = params.get(TOUR_FILTER_PARAM_NAMES.maxPrice);
  if (str) {
    retval.maxPrice = parseInt(str, 10);
  }

  str = params.get(TOUR_FILTER_PARAM_NAMES.tourServices);
  if (str) {
    retval.tourServices = JSON.parse(str);
  }

  str = params.get(TOUR_FILTER_PARAM_NAMES.subjects);
  if (str) {
    retval.tourSubjects = JSON.parse(str);
  }

  str = params.get(TOUR_FILTER_PARAM_NAMES.tourTypes);
  if (str) {
    retval.tourTypes = JSON.parse(str);
  }
  str = params.get(TOUR_FILTER_PARAM_NAMES.destinations);
  if (str) {
    retval.destinations = JSON.parse(str);
  }

  str = params.get(TOUR_FILTER_PARAM_NAMES.transportations);
  if (str) {
    retval.transportations = JSON.parse(str);
  }

  retval.groupName = params.get(TOUR_FILTER_PARAM_NAMES.groupName) || '';

  return retval;
}
export function parseTourSearchParams(params: URLSearchParams): TourSearchParamsState {
  const term = params.get(TOUR_SEARCH_PARAM_NAMES.term);
  const locationId = parseInt(params.get(TOUR_SEARCH_PARAM_NAMES.locationId) || '', 10);
  const locationName = params.get(TOUR_SEARCH_PARAM_NAMES.locationName) || '';

  if (term !== null) {
    return { input: { term, locationId } };
  }
  if (Number.isInteger(locationId)) {
    return { input: { locationId, locationName } };
  }
  throw new Error('No term or location ID');
}
export function stringifyTourSearchState(state: TourSearchParamsState, filter?: TourFilterState, sortBy?: string) {
  const arr = [];
  if (isTermInput(state.input)) {
    arr.push(`${TOUR_SEARCH_PARAM_NAMES.term}=${encodeURIComponent(state.input.term)}`);
    if (state.input.locationId) {
      arr.push(`${TOUR_SEARCH_PARAM_NAMES.locationId}=${encodeURIComponent(state.input.locationId)}`);
    }
  } else {
    arr.push(`${TOUR_SEARCH_PARAM_NAMES.locationId}=${state.input.locationId}`);
    arr.push(`${TOUR_SEARCH_PARAM_NAMES.locationName}=${state.input.locationName}`);
  }

  if (filter) {
    arr.push(`${TOUR_FILTER_PARAM_NAMES.subjects}=${JSON.stringify(filter.tourSubjects)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.activityTags}=${JSON.stringify(filter.activityTags)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.maxPrice}=${filter.maxPrice}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.destinations}=${JSON.stringify(filter.destinations)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.tourServices}=${JSON.stringify(filter.tourServices)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.tourTypes}=${JSON.stringify(filter.tourTypes)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.transportations}=${JSON.stringify(filter.transportations)}`);
    arr.push(`${TOUR_FILTER_PARAM_NAMES.groupName}=${encodeURIComponent(filter.groupName)}`);
  }
  if (sortBy) {
    arr.push(`${TOUR_FILTER_PARAM_NAMES.sort}=${sortBy}`);
  }
  return arr.join('&');
}
export function addTourRecentSearch(search: TourSearchParamsState) {
  const recentSearchData = localStorage.getItem(TOUR_SEARCH_HISTORY) || false;
  const searchString = stringifyTourSearchState(search);
  if (!recentSearchData) {
    localStorage.setItem(TOUR_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let tourSearch: string[] = JSON.parse(recentSearchData);

  tourSearch.unshift(searchString);

  remove(tourSearch, n => {
    try {
      parseTourSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  tourSearch = [...uniq(tourSearch)];

  if (tourSearch.length > MAX_SEARCH_HISTORY) {
    tourSearch.pop();
  }
  localStorage.setItem(TOUR_SEARCH_HISTORY, JSON.stringify(tourSearch));
}
export function getTourSearchHistory(): string[] {
  const tourSearchHistory = localStorage.getItem(TOUR_SEARCH_HISTORY) || false;
  if (!tourSearchHistory) {
    return [];
  }

  const tourSearch: string[] = JSON.parse(tourSearchHistory);

  remove(tourSearch, n => {
    try {
      parseTourSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  return tourSearch;
}
export function clearTourSearchHistory() {
  localStorage.removeItem(TOUR_SEARCH_HISTORY);
}

/** -------------------Train--------------------- */
export function stringifyTrainSearchParams(state: TrainSearchParamsState) {
  const arr = [];

  arr.push(`${TRAIN_SEARCH_PARAM_NAMES.origin}=${encodeURIComponent(JSON.stringify(state.origin))}`);
  arr.push(`${TRAIN_SEARCH_PARAM_NAMES.destination}=${encodeURIComponent(JSON.stringify(state.destination))}`);
  arr.push(`${TRAIN_SEARCH_PARAM_NAMES.departureDate}=${state.departureDate.format(DATE_FORMAT)}`);
  if (state.returnDate) {
    arr.push(`${TRAIN_SEARCH_PARAM_NAMES.returnDate}=${state.returnDate.format(DATE_FORMAT)}`);
  }
  arr.push(`${TRAIN_SEARCH_PARAM_NAMES.travellersInfo}=${encodeURIComponent(JSON.stringify(state.travellersInfo))}`);
  arr.push(`${TRAIN_SEARCH_PARAM_NAMES.seatClass}=${encodeURIComponent(JSON.stringify(state.seatClass))}`);

  return arr.join('&');
}

export function parseTrainSearchState(params: URLSearchParams): TrainSearchParamsState {
  const destination = params.get(TRAIN_SEARCH_PARAM_NAMES.destination);
  if (!destination) {
    throw new Error('No Destination');
  }

  const destinationJson = JSON.parse(destination);

  const origin = params.get(TRAIN_SEARCH_PARAM_NAMES.origin);
  if (!origin) {
    throw new Error('No origin');
  }
  const originJson = JSON.parse(origin);

  const seatClass = params.get(TRAIN_SEARCH_PARAM_NAMES.seatClass);
  if (!seatClass) {
    throw new Error('No seat class');
  }
  const seatClassJson = JSON.parse(seatClass);

  const departureDateStr = params.get(TRAIN_SEARCH_PARAM_NAMES.departureDate);
  if (!departureDateStr) {
    throw new Error('No departure date');
  }

  const departureDate = moment(departureDateStr, DATE_FORMAT);
  if (!departureDate.isValid() || departureDate.isBefore(moment().startOf('day'))) {
    throw new Error('Invalid departure date');
  }

  let returnDate;
  const returnDateStr = params.get(TRAIN_SEARCH_PARAM_NAMES.returnDate);
  if (returnDateStr) {
    returnDate = moment(returnDateStr, DATE_FORMAT);
    if (!returnDate.isValid() || returnDate.isBefore(departureDate)) {
      throw new Error('Invalid return date');
    }
  }
  const travellerInfoStr = params.get(TRAIN_SEARCH_PARAM_NAMES.travellersInfo);
  if (!travellerInfoStr) {
    throw new Error('No traveller info');
  }

  const travellersInfo = JSON.parse(travellerInfoStr);

  return {
    returnDate,
    departureDate,
    travellersInfo,
    seatClass: seatClassJson,
    destination: destinationJson,
    origin: originJson,
  };
}

export function validateTrainSearchParamState(params: TrainSearchParamsState): TrainSearchParamsError {
  let dateError = false;
  if (params.departureDate.isBefore(moment().startOf('day'))) {
    dateError = true;
  }
  if (params.returnDate && params.returnDate.startOf('days').isBefore(params.departureDate.startOf('days'))) {
    dateError = true;
  }

  const retval = { origin: !params.origin, destination: !params.destination, date: dateError };
  if (params.destination && params.origin && params.destination.trainStationCode === params.origin.trainStationCode) {
    retval.origin = true;
    retval.destination = true;
  }

  return retval;
}

export function validTrainTravellersInfo(
  adultCount: number,
  childCount: number,
  seniorCount: number,
  studentCount: number,
) {
  return (
    (adultCount >= 1 || seniorCount >= 1 || studentCount >= 1) &&
    childCount >= 0 &&
    adultCount >= 0 &&
    seniorCount >= 0 &&
    studentCount >= 0 &&
    adultCount + childCount + seniorCount + studentCount <= 4
  );
}

export function getTrainSearchHistory(): string[] {
  const trainSearchStr = localStorage.getItem(TRAIN_SEARCH_HISTORY) || false;
  if (!trainSearchStr) {
    return [];
  }

  const trainSearch: string[] = removeInValidTrainSearch(JSON.parse(trainSearchStr));

  return trainSearch;
}

function removeInValidTrainSearch(trainSearch: string[]): string[] {
  remove(trainSearch, search => {
    try {
      parseTrainSearchState(new URLSearchParams(search));
      return false;
    } catch (_) {
      return true;
    }
  });

  return trainSearch;
}

export function clearTrainSearchHistory() {
  localStorage.removeItem(TRAIN_SEARCH_HISTORY);
}

export function addTrainSearchToHistory(search: TrainSearchParamsState) {
  const trainSearchStr = localStorage.getItem(TRAIN_SEARCH_HISTORY) || false;

  const searchString = stringifyTrainSearchParams(search);

  if (!trainSearchStr) {
    localStorage.setItem(TRAIN_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let trainSearch: string[] = removeInValidTrainSearch(JSON.parse(trainSearchStr));
  trainSearch.unshift(searchString);

  trainSearch = [...uniq(trainSearch)];

  if (trainSearch.length > MAX_SEARCH_HISTORY) {
    trainSearch.pop();
  }

  localStorage.setItem(TRAIN_SEARCH_HISTORY, JSON.stringify(trainSearch));
}

/** ------------------------------ */

export function validGuestCount(roomCount: number, adultCount: number, childCount: number) {
  return (
    roomCount >= 1 &&
    roomCount <= 9 &&
    adultCount >= 1 &&
    adultCount <= 36 &&
    childCount <= 9 &&
    adultCount >= roomCount
  );
}

export function validTravellerCount(adultCount: number, childCount: number, babyCount: number) {
  return (
    adultCount >= 1 && childCount >= 0 && babyCount >= 0 && adultCount + childCount <= 9 && babyCount <= adultCount
  );
}

export function validateChildrenAgesInfo(childrenAges: number[]) {
  return childrenAges.includes(-1);
}
