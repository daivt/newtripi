import { find } from 'lodash';
import memoizeOne from 'memoize-one';
import moment, { Moment } from 'moment';
import voca from 'voca';
import { some } from '../../constants';
import { GuestCountInfo } from './models';
import { CommonState } from './redux/reducer';
import { HotelBookingParams } from '../booking/redux/hotelBookingReducer';

// tslint:disable-next-line:max-line-length
export const validEmailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// export const noEmtyString = /(.)+/;
export const validNameRegex = /^[a-zA-Z ]+$/;
// tslint:disable-next-line:max-line-length
// export const validNameVietnamRegex = /^[a-zA-Z àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]{1,50}$/;
export const validIdRegex = /^[a-zA-Z0-9 ]+$/;
export const validTelephoneRegex = /^[0-9]+$/;
export const validOTPRegex = /^[0-9]+$/;
export const validVietnamTelephoneRegex = /^[0-9]{8,11}$/;
export const validSpaceRegex = /\s/;

function makeGetAirlineInfoCore(state: CommonState) {
  let map: { [id: number]: some };
  return (id: number) => {
    if (!map && state.generalInfo.flight) {
      map = {};
      if (state.generalInfo.flight.airlines) {
        state.generalInfo.flight.airlines.forEach((airline: some) => {
          map[airline.aid] = { logo: airline.logo, name: airline.name };
        });
      }
    }

    return (map && map[id]) || {};
  };
}

export const makeGetAirlineInfo = memoizeOne(makeGetAirlineInfoCore);

export function durationMillisecondToHour(millisecond: number) {
  const second = millisecond / 1000;
  const hours = Math.floor(second / 3600);
  const minutes = Math.floor((second - hours * 3600) / 60);

  if (minutes) {
    return `${hours}h ${minutes}m`;
  }

  return `${hours}h`;
}

export function durationMinuteToHour(minute: number) {
  const startDay = moment().startOf('day');
  startDay.add(minute, 'minutes');

  return startDay.format('HH:mm');
}

export const LONG_TRANSIT_DURATION = 5 * 3600 * 1000;

export const makeGetTicketClass = memoizeOne(makeGetTicketClassCore);

function makeGetTicketClassCore(state: CommonState) {
  return (classCode: string) =>
    find<some>(state.generalInfo.flight ? state.generalInfo.flight.ticketclass : [], {
      code: classCode,
    });
}

export function isValidGuestInfo(info: GuestCountInfo) {
  if (info.roomCount > 0 && info.adultCount > 0 && !info.childrenAges.includes(-1)) {
    return true;
  }
  return false;
}

export const renderMonthText = (month: Moment) => voca.capitalize(moment(month.valueOf()).format('MMMM - YYYY'));

export const getRoomData = (data: some[], params: HotelBookingParams) =>
  data.find((roomData: some) => {
    const str1 = roomData.SHRUI as string;
    const str2 = (params && params.shrui) || '';
    return str1.slice(0, str1.indexOf('@')) === str2.slice(0, str2.indexOf('@'));
  });
