import MomentUtils from '@date-io/moment';
import {
  createMuiTheme,
  Dialog,
  IconButton,
  InputAdornment,
  InputBase,
  Slide,
  TextField,
  Theme,
} from '@material-ui/core';
import { StandardTextFieldProps } from '@material-ui/core/TextField';
import { TransitionProps } from '@material-ui/core/transitions/transition';
import CancelIcon from '@material-ui/icons/Cancel';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider, withStyles } from '@material-ui/styles';
import moment from 'moment';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { GREY, SECONDARY } from '../../../colors';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as CalendarIcon } from '../../../svg/calendar.svg';

export const SlideUp = React.forwardRef<unknown, TransitionProps>((props, ref) => {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const SlideDown = React.forwardRef<unknown, TransitionProps>((props, ref) => {
  return <Slide direction="down" ref={ref} {...props} />;
});

export const PageWrapper = styled.div`
  min-height: ${window.innerHeight}px;
  position: relative;
  display: flex;
  flex-direction: column;
`;

export const WVInput = withStyles((theme: Theme) => ({
  root: {
    borderRadius: 4,
    border: `1px solid ${GREY}`,
    backgroundColor: 'white',
  },
  input: {
    padding: '5px 0',
    display: 'flex',
    alignItems: 'center',
    minHeight: '40px',
    fontSize: `${theme.typography.body2.fontSize}`,
    boxSizing: 'border-box',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&::placeholder': {
      color: GREY,
      opacity: 1,
    },
  },
}))(InputBase);

export const BaseTextField = withStyles((theme: Theme) => ({
  root: {
    marginTop: '8px',
    '& input': { fontSize: `${theme.typography.body1.fontSize}` },
    '& .MuiInput-underline:before': {
      border: 'none',
      borderBottom: `1px solid ${GREY}`,
    },
    '& .MuiInput-underline:hover:not(.Mui-disabled):before': {
      border: 'none',
      borderBottom: `1px solid ${GREY}`,
    },
  },
}))(TextField);

interface WVTextFieldProps extends StandardTextFieldProps {
  regex?: RegExp;
  clear?: boolean;
  update: (text: string) => void;
}

export const WVFreeTextField: React.FC<WVTextFieldProps> = props => {
  const { regex, update, clear, ...rest } = props;
  const [focused, setFocused] = React.useState(false);

  const reset = React.useCallback(() => update(''), [update]);

  return (
    <BaseTextField
      {...rest}
      onChange={e => {
        if (!regex || regex.test(e.target.value) || !e.target.value) {
          update(e.target.value);
        }
      }}
      onFocus={() => setFocused(true)}
      onBlur={e => {
        setImmediate(() => setFocused(false));
      }}
      InputProps={{
        endAdornment: clear && !!props.value && focused && (
          <InputAdornment position="end">
            <IconButton style={{ padding: 3 }} onClick={reset}>
              <CancelIcon fontSize="small" />
            </IconButton>
          </InputAdornment>
        ),
        ...props.InputProps,
      }}
    />
  );
};

interface WVDateFieldProps extends StandardTextFieldProps {
  disableFuture?: boolean;
  disablePast?: boolean;
  locale: string;
  update: (val: string) => void;
  value: string;
}

export const WVDateField = connect((state: AppState) => ({
  locale: state.intl.locale,
}))((props: WVDateFieldProps) => {
  const { disableFuture, disablePast, locale, update, value, ...rest } = props;
  const theme = useMemo(() => createMuiTheme({ palette: { primary: { main: SECONDARY } } }), []);
  const prevLocale = useRef(locale);
  const [dateFormat, setDateFormat] = useState(moment.localeData().longDateFormat('L'));
  const { mDate, time } = useMemo(() => {
    const dateObj = moment(value, dateFormat);
    if (dateObj.isValid()) {
      return { mDate: dateObj, time: dateObj.valueOf() };
    }
    return { mDate: null, time: null };
  }, [value, dateFormat]);
  useEffect(() => {
    if (locale !== prevLocale.current) {
      const newDateFormat = moment.localeData().longDateFormat('L');
      setDateFormat(newDateFormat);
      if (time !== null) {
        const newDateStr = moment(time).format(newDateFormat);
        update(newDateStr);
      }
    }
  }, [locale, time, update, value]);
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <BaseTextField
        {...rest}
        onClick={() => setOpen(true)}
        InputProps={{
          readOnly: true,
          endAdornment: (
            <InputAdornment position="end">
              <CalendarIcon />
            </InputAdornment>
          ),
          ...props.InputProps,
        }}
      />

      <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
        <ThemeProvider theme={theme}>
          <DatePicker
            variant="dialog"
            autoOk
            open={open}
            disableFuture={disableFuture}
            disablePast={disablePast}
            maxDate={disableFuture ? moment() : undefined}
            minDate={disablePast ? moment() : undefined}
            openTo="year"
            views={['year', 'month', 'date']}
            value={mDate || undefined}
            onChange={date => {
              if (date) {
                update(date.format(dateFormat));
              }
            }}
            TextFieldComponent={() => <></>}
            onAccept={() => setOpen(false)}
            onClose={() => setOpen(false)}
            okLabel={<FormattedMessage id="ok" />}
            cancelLabel={<FormattedMessage id="cancel" />}
          />
        </ThemeProvider>
      </MuiPickersUtilsProvider>
    </>
  );
});

export const BottomDialog = withStyles({
  scrollPaper: { alignItems: 'flex-end' },
  paper: { margin: 0, display: 'flex', width: '100%' },
})(Dialog);

export const TopDialog = withStyles({
  scrollPaper: { alignItems: 'flex-start' },
  paper: { margin: 0, display: 'flex', width: '100%' },
})(Dialog);
