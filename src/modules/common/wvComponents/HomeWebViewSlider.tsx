import * as React from 'react';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconNext } from '../../../svg/ic_next.svg';
import { ReactComponent as IconPrev } from '../../../svg/ic_prev.svg';
import ProgressiveImage from '../components/ProgressiveImage';
import { fetchThunk } from '../redux/thunks';

const Arrow = ({ img, className, style, onClick }: some) => {
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      {img}
    </div>
  );
};

interface IHomeSliderProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  data: some[];
}

class HomeWebViewSlider extends React.PureComponent<IHomeSliderProps, State> {
  state = {
    data: [],
  };

  async componentDidMount() {
    const json = await this.props.dispatch(fetchThunk(API_PATHS.breakingNews));
    if (json.code === 200) {
      this.setState({ data: json.data });
    }
  }

  public render() {
    const { data } = this.state;
    return (
      <div style={{ overflow: 'hidden', height: '65vw', width: '100vw' }}>
        <Slider
          autoplay
          arrows
          dots
          nextArrow={<Arrow img={<IconNext />} />}
          prevArrow={<Arrow img={<IconPrev />} />}
          customPaging={() => (
            <div style={{ padding: '8px 0', background: 'transparent' }}>
              <div
                style={{
                  height: 4,
                  width: 50,
                  borderRadius: 2,
                  borderWidth: 3,
                }}
              />
            </div>
          )}
        >
          {data &&
            data
              .filter((one: some) => one.active && one.activeDesktop)
              .map((one: some) => (
                <div key={one.thumb}>
                  <ProgressiveImage
                    src={one.thumb}
                    alt=""
                    style={{
                      width: '100%',
                      height: '100%',
                      objectFit: 'contain',
                    }}
                  />
                </div>
              ))}
        </Slider>
      </div>
    );
  }
}

export default connect()(HomeWebViewSlider);
