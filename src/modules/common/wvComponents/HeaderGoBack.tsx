import { IconButton } from '@material-ui/core';
import ArrowBackIosRounded from '@material-ui/icons/ArrowBackIosRounded';
import { goBack } from 'connected-react-router';
import * as React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import Header from './Header';

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  content?: React.ReactNode;
  style?: React.CSSProperties;
  boundaryElementRef?: React.RefObject<HTMLElement>;
  simple?: boolean;
}

const HeaderGoBack: React.FunctionComponent<Props> = props => {
  const { content, dispatch, style, boundaryElementRef, simple } = props;
  const [transparent, setTransparent] = React.useState(!!boundaryElementRef);

  const headerRef = React.useRef<HTMLElement>(null);

  React.useEffect(() => {
    if (boundaryElementRef) {
      const listener = () => {
        if (
          (headerRef.current?.clientHeight || 0) < (boundaryElementRef.current?.getBoundingClientRect().bottom || 0)
        ) {
          setTransparent(true);
        } else {
          setTransparent(false);
        }
      };
      window.addEventListener('scroll', listener);
      return () => window.removeEventListener('scroll', listener);
    }
    return undefined;
  }, [boundaryElementRef, headerRef]);

  return (
    <Header
      simple={simple}
      ref={headerRef}
      position={boundaryElementRef ? 'fixed' : 'sticky'}
      content={transparent ? undefined : content}
      action={
        <div
          style={{
            borderRadius: '50%',
            backgroundColor: transparent ? 'rgba(0,0,0,0.3)' : 'transparent',
          }}
        >
          <IconButton size="small" onClick={() => dispatch(goBack())} color="inherit">
            <ArrowBackIosRounded />
          </IconButton>
        </div>
      }
      style={{
        ...style,
        background: transparent ? 'transparent' : style?.background,
        boxShadow: transparent ? 'none' : style?.boxShadow,
        transition: transparent ? 'none' : style?.boxShadow,
      }}
    />
  );
};
function mapStateToProps(state: AppState) {
  return {};
}

export default connect(mapStateToProps)(injectIntl(HeaderGoBack));
