import * as React from 'react';
import { Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { FormattedMessage } from 'react-intl';
import { fetchThunk } from '../redux/thunks';
import { API_PATHS } from '../../../API';
import ProgressiveImage from '../components/ProgressiveImage';

interface IPromotionProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  promotionData: some[];
  selectedFooter: number;
}

class Promotion extends React.PureComponent<IPromotionProps, State> {
  state: State = { promotionData: [], selectedFooter: 1 };
  async componentDidMount() {
    const json = await this.props.dispatch(
      fetchThunk(`${API_PATHS.getPromotionPrograms}`, 'post', true, '{}'),
    );
    if (json.code === 200) {
      this.setState({ promotionData: json.data });
    }
  }

  public render() {
    const { promotionData } = this.state;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div style={{ overflow: 'hidden', margin: '0px 10px', maxHeight: '50vh' }}>
        <Typography variant="h6" style={{ paddingBottom: 10 }}>
          <FormattedMessage id="tour.promotion" />
        </Typography>
        <Slider {...settings}>
          {promotionData.map((v: some, index: number) => (
            <div key={index}>
              <ProgressiveImage
                title={v.shortDescription}
                style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                src={v.thumb}
                alt=""
              />
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}

export default connect()(Promotion);
