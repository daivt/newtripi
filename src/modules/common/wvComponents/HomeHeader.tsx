import { AppBar, IconButton } from '@material-ui/core';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import React, { PureComponent } from 'react';
import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined';
import { ReactComponent as Notification } from '../../../svg/notification.svg';
import { ReactComponent as LogoTripi } from '../../../svg/TripiLogo.svg';
import DrawerMenu from './DrawerMenu';

interface Props {}
interface State {
  openSidebar: boolean;
}
class HomeHeader extends PureComponent<Props, State> {
  state: State = {
    openSidebar: false,
  };

  render() {
    const { openSidebar } = this.state;
    return (
      <AppBar
        position="sticky"
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '48px',
          flexDirection: 'row',
        }}
      >
        <IconButton color="inherit" onClick={() => this.setState({ openSidebar: true })} style={{ padding: 6 }}>
          <MenuOutlinedIcon />
        </IconButton>
        <div style={{ flexGrow: 1, display: 'flex' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              flex: 1,
              paddingLeft: '36px',
              alignItems: 'center',
            }}
          >
            <LogoTripi />
          </div>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <IconButton style={{ padding: 6 }} color="inherit">
              <Notification />
            </IconButton>
            <IconButton style={{ padding: 6 }} color="inherit">
              <AccountCircleRoundedIcon />
            </IconButton>
          </div>
        </div>
        <DrawerMenu openSidebar={openSidebar} onClose={() => this.setState({ openSidebar: false })} />
      </AppBar>
    );
  }
}

export default HomeHeader;
