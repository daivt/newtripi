import { AppBar, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';

interface Props {
  title?: React.ReactNode;
  action?: React.ReactNode;
  onClose(): void;
}

const PopupHeader: React.FC<Props> = props => {
  const { onClose, title, action } = props;

  return (
    <AppBar position="sticky">
      <div
        style={{
          minHeight: '48px',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <IconButton
          onClick={onClose}
          style={{ padding: '6px', width: '40px', flexShrink: 0 }}
          color="inherit"
        >
          <CloseIcon />
        </IconButton>
        <div
          style={{
            flex: 1,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {!!title && (
            <div
              style={{
                flex: 1,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
                paddingRight: !action ? '40px' : undefined,
              }}
            >
              {title}
            </div>
          )}
          {!!action && (
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              {action}
            </div>
          )}
        </div>
      </div>
    </AppBar>
  );
};

export default PopupHeader;
