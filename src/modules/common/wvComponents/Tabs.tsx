import { ButtonBase, Paper, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as FlightSvg } from '../../../svg/Flight.svg';
import { ReactComponent as HotelSvg } from '../../../svg/Hotel.svg';
import { ReactComponent as TourSvg } from '../../../svg/TourismActivities.svg';
import { ReactComponent as TrainSvg } from '../../../svg/Train.svg';
import { RawLink as Link } from '../components/Link';

const tabs = [
  {
    img: <FlightSvg />,
    id: 'flight',
    url: WV_ROUTES.flight.flight,
  },
  {
    img: <HotelSvg />,
    id: 'hotel',
    url: WV_ROUTES.hotel.hotel,
  },
  {
    img: <TourSvg />,
    id: 'tourTitle',
    url: WV_ROUTES.tour.tour,
  },
  {
    img: <TrainSvg />,
    id: 'trainTickets',
    url: WV_ROUTES.train.train,
  },
];

interface Props extends ReturnType<typeof mapStateToProps> {}

const Tabs: React.FC<Props> = props => {
  const { router } = props;
  return (
    <Paper elevation={2} style={{ background: 'white', borderRadius: '8px', display: 'flex', width: '100%' }}>
      {tabs.map(tab => (
        <Link
          to={{
            pathname: tab.url,
            state: {
              backableToHomePage: router.location.pathname === '/',
            },
          }}
          style={{ flex: 1, flexBasis: 1, display: 'flex', justifyContent: 'center' }}
          key={tab.id}
        >
          <ButtonBase
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              padding: '5px 10px',
              justifyContent: 'flex-start',
            }}
          >
            <div style={{ display: 'flex', height: '40px', alignItems: 'flex-end' }}>{tab.img}</div>
            <div style={{ wordBreak: 'break-word', marginTop: '5px' }}>
              <Typography variant="body2">
                <FormattedMessage id={tab.id} />
              </Typography>
            </div>
          </ButtonBase>
        </Link>
      ))}
    </Paper>
  );
};

const mapStateToProps = (state: AppState) => ({ router: state.router });

export default connect(mapStateToProps)(Tabs);
