import {
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  Typography,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { WV_ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import FlightSvg from '../../../svg/Flight.svg';
import HotelSvg from '../../../svg/Hotel.svg';
import IconHelp from '../../../svg/ic_sidebar_help.svg';
import TourSvg from '../../../svg/TourismActivities.svg';
import TrainSvg from '../../../svg/Train.svg';
import { RawLink as Link } from '../components/Link';

interface MenuItem {
  icon: string;
  labelId: string;
  url: string;
}

interface Props extends ReturnType<typeof mapStateToProps> {
  openSidebar: boolean;
  onClose(): void;
}

interface State {}

const sidebarMenu: MenuItem[][] = [
  [
    {
      icon: FlightSvg,
      labelId: 'flight',
      url: WV_ROUTES.flight.flight,
    },
    {
      icon: TrainSvg,
      labelId: 'trainTickets',
      url: WV_ROUTES.train.train,
    },
    {
      icon: HotelSvg,
      labelId: 'hotel',
      url: WV_ROUTES.hotel.hotel,
    },
    {
      icon: TourSvg,
      labelId: 'tour',
      url: WV_ROUTES.tour.tour,
    },
  ],
  [
    {
      icon: IconHelp,
      labelId: 'sidebar.help',
      url: 'https://hotro.tripi.vn',
    },
  ],
];

class DrawerMenu extends PureComponent<Props, State> {
  toggleDrawer = (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }
    this.props.onClose();
  };

  render() {
    const { openSidebar, router } = this.props;
    return (
      <Drawer open={openSidebar} onClose={this.toggleDrawer}>
        <div
          style={{
            minHeight: '30px',
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        >
          <IconButton size="small" onClick={this.toggleDrawer} style={{ padding: '6px' }}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <div style={{ width: '270px' }} role="presentation" onClick={this.toggleDrawer}>
          <Divider />
          {sidebarMenu.map((group, index) => (
            <div key={index}>
              <List style={{ paddingTop: 0, paddingBottom: 0 }}>
                {group.map(item => (
                  <Link
                    to={{
                      pathname: item.url,
                      state: {
                        backableToHomePage: router.location.pathname === '/' ? true : false,
                      },
                    }}
                    key={item.labelId}
                  >
                    <ListItem button style={{ height: 48 }}>
                      <ListItemIcon style={{ width: 24 }}>
                        <img alt="" src={item.icon} />
                      </ListItemIcon>
                      <Typography variant="body2">
                        <FormattedMessage id={item.labelId} />
                      </Typography>
                    </ListItem>
                  </Link>
                ))}
              </List>
              <Divider style={{ padding: 0, margin: '0 16px' }} />
            </div>
          ))}
        </div>
      </Drawer>
    );
  }
}

const mapStateToProps = (state: AppState) => ({ router: state.router });

export default connect(mapStateToProps)(DrawerMenu);
