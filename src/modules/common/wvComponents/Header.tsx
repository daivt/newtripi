import { AppBar, AppBarProps } from '@material-ui/core';
import React from 'react';

interface Props extends AppBarProps {
  action: React.ReactNode;
  content: React.ReactNode;
  simple?: boolean;
}

const Header: React.RefForwardingComponent<unknown, Props> = (props, ref) => {
  const { action, content, simple, position = 'sticky', ...rest } = props;

  return (
    <AppBar
      {...rest}
      ref={ref}
      position={position}
      style={{
        height: '48px',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        ...rest.style,
      }}
    >
      <div style={{ minWidth: '40px', flexShrink: 0, display: 'flex', justifyContent: 'center' }}>{action}</div>
      <div
        style={{
          flex: 1,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginRight: simple ? '40px' : 0,
          overflow: 'hidden',
        }}
      >
        {content}
      </div>
    </AppBar>
  );
};

export default React.forwardRef(Header);
