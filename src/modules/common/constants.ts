export const FLIGHT_SEARCH_PARAM_NAMES = {
  destination: 'destination',
  origin: 'origin',
  departureDate: 'departDate',
  returnDate: 'returnDate',
  travellerInfo: 'travellerInfo',
  seatClass: 'class',
};

export const FLIGHT_SEARCH_FILTER_PARAM_NAMES = {
  airline: 'airlineIds',
};

export const HOTEL_SEARCH_PARAM_NAMES = {
  location: 'location',
  checkIn: 'checkIn',
  checkOut: 'checkOut',
  guestInfo: 'guestInfo',
  mapParams: 'mapParams',
};

export const FLIGHT_HUNT_PARAM_NAMES = {
  destination: 'destination',
  origin: 'origin',
  travellerInfo: 'travellerInfo',
  oneWay: 'oneWay',
  airlines: 'airlineIds',
  departureDate: 'departDate',
  returnDate: 'returnDate',
};

export const TOUR_SEARCH_PARAM_NAMES = {
  term: 'term',
  locationId: 'locationId',
  locationName: 'locationName',
};

export const HOTEL_FILTER_PARAM_NAMES = {
  price: 'price',
  star: 'star',
  area: 'area',
  type: 'type',
  facility: 'facility',
  sort: 'sort',
};

export const TOUR_FILTER_PARAM_NAMES = {
  subjects: 'subjects',
  sort: 'sort',
  activityTags: 'activityTags',
  maxPrice: 'maxPrice',
  tourServices: 'tourServices',
  tourTypes: 'tourTypes',
  transportations: 'transportations',
  groupName: 'groupName',
  destinations: 'destinations',
};

export const TRAIN_SEARCH_PARAM_NAMES = {
  origin: 'origin',
  destination: 'destination',
  departureDate: 'departureDate',
  returnDate: 'returnDate',
  travellersInfo: 'travellersInfo',
  seatClass: 'seatClass',
};

export const HOTEL_BOOK_PARAMS_NAMES = {
  hotelId: 'hotelId',
  shrui: 'shrui',
};
export const HOTEL_ACTIVITY_DIRECT_PARAM = 'direct';

export const TOUR_BOOK_PARAMS_NAMES = {
  tourId: 'tourId',
};

export const FLIGHT_BOOKING_PARAM_NAME = 'booking';

export const PAYMENT_HOLDING_CODE = 'PL';
export const PAYMENT_TRIPI_CREDIT_CODE = 'CD';
export const PAYMENT_ATM_CODE = 'ATM';
export const PAYMENT_VISA_CODE = 'VM';
export const PAYMENT_VNPAY_CODE = 'QR';

export const SEND_HOME_REASONS = {
  noDataInSession: 'NoDataInCurrentSession',
  invalidInput: 'InvalidInputData',
  invalidLink: 'InvalidLink',
  expiredTicket: 'ExpiredTicket',
  invalidAccessToken: 'InvalidAccessToken',
};

export const IMAGE_ALLOW_TYPE = 'image/jpeg, image/png, image/gif';

export const MYTOUR_BOOKING_POLICIES = [
  {
    name: 'footer.myTour.termsAndPolicies',
    url: 'https://mytour.vn/news/135152-chinh-sach-va-quy-dinh-chung.html',
  },
  {
    name: 'footer.myTour.paymentPolicies',
    url: 'https://mytour.vn/news/135633-quy-dinh-ve-thanh-toan.html',
  },
  {
    name: 'footer.myTour.bookingConfirmationPolicies',
    url: 'https://mytour.vn/news/135634-quy-dinh-ve-xac-nhan-thong-tin-dat-phong.html',
  },
  {
    name: 'footer.myTour.bookingCancelPolicies',
    url: 'https://mytour.vn/news/135154-chinh-sach-huy-phong-va-hoan-tien.html',
  },
  {
    name: 'footer.myTour.privacyPolicies',
    url: 'https://mytour.vn/news/135636-chinh-sach-bao-mat-thong-tin-danh-cho-website-tmdt.html',
  },
];

export const MYTOUR_BOOKING_TOURING_POLICIES = [
  {
    name: 'footer.myTour.operationRegulation',
    url: 'https://mytour.vn/news/135155-quy-che-hoat-dong.html',
  },
  {
    name: 'footer.myTour.privacy',
    url: 'https://mytour.vn/news/135156-chinh-sach-bao-mat-thong-tin-danh-cho-san-gdtmdt.html',
  },
  {
    name: 'footer.myTour.handlingComplaints',
    url: 'https://mytour.vn/news/135420-quy-trinh-giai-quyet-tranh-chap-khieu-nai.html',
  },
];

export const MYTOUR_CLIENTS = [
  {
    name: 'footer.myTour.vPoint',
    url: 'https://mytour.vn/promo-news/135699-chuong-trinh-diem-thuong-vpoint-danh-cho-thanh-vien-mytour.html',
  },
  {
    name: 'footer.myTour.signIn',
    url: 'https://hms.mytour.vn/auth/login',
  },
  {
    name: 'footer.myTour.register',
    url: 'https://mytour.vn/sign-up',
  },
];

export const MYTOUR_ABOUT = [
  {
    name: 'footer.myTour.introduction',
    url: 'https://mytour.vn/news/135150-gioi-thieu-ve-mytourvn.html',
  },
  {
    name: 'footer.myTour.news',
    url: 'https://mytour.vn/news',
  },
  {
    name: 'footer.myTour.recruitment',
    url: 'https://career.mytour.vn/',
  },
  {
    name: 'footer.myTour.contact',
    url: 'https://mytour.vn/help/30-lien-he.html',
  },
];
