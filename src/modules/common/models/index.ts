import { Moment } from 'moment';
import { some } from '../../../constants';

export interface PassportInfo {
  passport: string;
  passportExpiredDate: string; // 'DD-MM-YYYY'
  passportCountry: some; // Quoc gia cap
  nationalityCountry: some; //
}

export const defaultPassportInfo: PassportInfo = {
  passport: '',
  passportExpiredDate: '',
  passportCountry: {},
  nationalityCountry: {},
};

export interface TravellerCountInfo {
  readonly adultCount: number;
  readonly childCount: number;
  readonly babyCount: number;
}

export const defaultTravellerInfo: TravellerCountInfo = {
  adultCount: 1,
  childCount: 0,
  babyCount: 0,
};

export interface TravellersInfo {
  readonly adults: TravellerInfo[];
  readonly children: TravellerInfo[];
  readonly babies: TravellerInfo[];
}

export type Gender = 'm' | 'f';
export interface TravellerInfo {
  readonly familyName: string;
  readonly givenName: string;
  readonly birthday: string;
  readonly gender: Gender;
  readonly passportInfo: PassportInfo;
}

export interface TravellerInfoValidation {
  readonly birthday: boolean;
  readonly familyName: boolean;
  readonly givenName: boolean;
  readonly passportInfo: PassportInfoValidation;
}

export interface PassportInfoValidation {
  readonly passport: boolean;
  readonly passportExpiredDate: boolean;
  readonly passportCountry: boolean;
  readonly nationalityCountry: boolean;
}

export interface ContactInfo {
  readonly familyName: string;
  readonly givenName: string;
  readonly gender: Gender;
  readonly address: string;
  readonly email: string;
  readonly telephone: string;
}
export interface MobileCardBuyer {
  readonly name?: string;
  readonly email?: string;
  readonly phone?: string;
  readonly amount: number;
  readonly cardValue?: number;
  readonly operatorCode?: string;
}
export interface MobileTopupBuyer {
  readonly phone?: string;
  readonly cardValue?: number;
  readonly operatorCode?: string;
}

export interface ContactInfoValidation {
  readonly birthday: boolean;
  readonly familyName: boolean;
  readonly givenName: boolean;
  readonly email: boolean;
  readonly telephone: boolean;
}

export interface BuyerInfoValidation {
  readonly email: boolean;
  readonly name: boolean;
  readonly phone: boolean;
}

export const validTravellerInfoValidation: TravellerInfoValidation = {
  birthday: true,
  familyName: true,
  givenName: true,
  passportInfo: {
    passport: true,
    passportExpiredDate: true,
    passportCountry: true,
    nationalityCountry: true,
  },
};

export const validContactInfoValidation: ContactInfoValidation = {
  birthday: true,
  familyName: true,
  givenName: true,
  email: true,
  telephone: true,
};

export const validBuyerValidation: BuyerInfoValidation = {
  name: true,
  email: true,
  phone: true,
};

export interface TravellersInfoValidation {
  readonly adults: TravellerInfoValidation[];
  readonly children: TravellerInfoValidation[];
  readonly babies: TravellerInfoValidation[];
}

export interface Airport {
  readonly code: string;
  readonly name: string;
  readonly location: string;
}

export interface SeatClass {
  cid: number;
  code: string;
  i_name: string;
  v_name: string;
}

export interface GuestCountInfo {
  readonly roomCount: number;
  readonly adultCount: number;
  readonly childCount: number;
  readonly childrenAges: number[];
}

export const defaultGuestCountInfo: GuestCountInfo = {
  roomCount: 1,
  adultCount: 2,
  childCount: 0,
  childrenAges: [],
};

export interface TrainStation {
  readonly sKeys: string;
  readonly trainStationCode: string;
  readonly trainStationName: string;
}

export interface TrainTravellersInfo {
  readonly adultCount: number;
  readonly childCount: number;
  readonly seniorCount: number;
  readonly studentCount: number;
}

export interface TrainSeatClass {
  readonly code: string;
  readonly id: string;
}

export const defaultTrainTravellersInfo: TrainTravellersInfo = {
  adultCount: 1,
  childCount: 0,
  seniorCount: 0,
  studentCount: 0,
};

export interface TrainCustomersGroup {
  readonly customerGroupId: number;
  readonly ageGroupName: string;
  readonly discountPercent: number;
}

export interface RoomBookingInfo {
  name: string;
  phoneNumber?: string;
  specialRequest?: string;
}

export interface RoomBookingInfoValidation {
  name: boolean;
}

export const validRoomBookingInfo: RoomBookingInfoValidation = {
  name: true,
};

export interface TourCustomerInfo {
  id: number;
  fullName: string;
  phone: string;
  email: string;
}

export interface TourCustomerCountInfo {
  adultCount: number;
  childrenCount: number;
  babyCount: number;
  infantCount: number;
}

export interface TourCustomerInfoValidation {
  fullName: boolean;
  phone: boolean;
}

export const validTourCustomerInfoValidation: TourCustomerInfoValidation = {
  fullName: true,
  phone: true,
};

export interface Location {
  readonly hotelId: number;
  readonly provinceId: number;
  readonly provinceName: string;
  readonly streetId: number;
  readonly streetName: string;
  readonly districtId: number;
  readonly districtName: string;
  readonly slug: string;
  readonly numHotels: number;
  readonly name: string;
  readonly latitude: number;
  readonly longitude: number;
  readonly thumbnailUrl: string | null;
}

export interface PointPayment {
  readonly availablePoint: number;
  readonly min: number;
  readonly max: number;
  readonly paymentMethodCode: string;
  readonly pointFactor: number;
  readonly credit: number;
  readonly maxPointOnModuleTag: string;
  readonly usedPointOnModuleTag: string;
}

export interface AgencyInformation {
  readonly bankCode: string;
  readonly businessProfiles: string[];
  readonly identityCard: string;
  readonly frontIdentityCard: string;
  readonly accountName: string;
  readonly accountNumber: string;
  readonly backIdentityCard: string;
}

export interface AgencyInformationValidation {
  readonly bankCode: boolean;
  readonly businessProfiles: boolean;
  readonly identityCard: boolean;
  readonly frontIdentityCard: boolean;
  readonly accountName: boolean;
  readonly accountNumber: boolean;
  readonly backIdentityCard: boolean;
}

export const validAgencyInformationValidation: AgencyInformationValidation = {
  bankCode: true,
  businessProfiles: true,
  identityCard: true,
  frontIdentityCard: true,
  accountName: true,
  accountNumber: true,
  backIdentityCard: true,
};

export interface SignUpInformation {
  readonly phone: string;
  readonly otp: string;
  readonly name: string;
  readonly email: string;
  readonly password: string;
  readonly rePassword: string;
}

export interface SignUpInformationValidation {
  readonly phone: boolean;
  readonly otp: boolean;
  readonly name: boolean;
  readonly email: boolean;
  readonly password: boolean;
  readonly rePassword: boolean;
}

export const validSignUpInformationValidation: SignUpInformationValidation = {
  phone: true,
  otp: true,
  name: true,
  email: true,
  password: true,
  rePassword: true,
};

export interface Tour {
  readonly name: string;
  readonly fromDestination?: some;
  readonly mainDestination: some;
  readonly id: number;
  readonly imageURL: string;
  readonly thumb: string;
}

export interface TourLocation {
  readonly name: string;
  readonly numTours: number;
  readonly imageUrl: string;
  readonly locationId: number;
  readonly thumb: string;
}

export interface TicketAlertInfo {
  readonly id?: number;
  readonly navigator: string;
  readonly notifyChannels: string[];
  readonly fromAirport?: Airport;
  readonly toAirport?: Airport;
  readonly roundTrip: boolean;
  readonly numAdults: number;
  readonly numChildren: number;
  readonly numInfants: number;
  readonly totalExpectedPrice: number;
  readonly outboundTimeSegments: string[];
  readonly outboundFromDate: Moment | null;
  readonly outboundToDate: Moment | null;
  readonly inboundTimeSegments: string[];
  readonly inboundFromDate: Moment | null;
  readonly inboundToDate: Moment | null;
}

export interface TicketAlertValidation {
  readonly fromAirport: boolean;
  readonly toAirport: boolean;
  readonly outboundDate: boolean;
  readonly outboundDateOver: string;
  readonly inboundDate: boolean;
  readonly inboundDateOver: string;
}

export interface CustomerInfo {
  readonly id?: number;
  readonly gender: Gender;
  readonly firstName: string;
  readonly lastName: string;
  readonly dateOfBirth?: number;
  readonly email?: string;
  readonly phone: string;
  readonly address?: string;
  readonly city?: string;
  readonly province?: string;
  readonly country?: string;
  readonly passport?: string;
  readonly passportExpiry?: number;
}

export interface CustomerInfoValidation {
  readonly firstName: boolean;
  readonly lastName: boolean;
  readonly phone: boolean;
}

export const validCustomerInfoValidation: CustomerInfoValidation = {
  firstName: true,
  lastName: true,
  phone: true,
};

export interface ProfileInfo {
  profilePhoto?: string;
  gender?: Gender;
  name: string;
  isAgencyManager: boolean;
  companyName: string;
  address: string;
  country: some;
  countryName: string;
  email: string;
  phone: string;
  dateOfBirth: string;
}
export interface ProfileInfoValidation {
  gender: boolean;
  name: boolean;
  address: boolean;
  email: boolean;
  phone: boolean;
  dateOfBirth: boolean;
  country: boolean;
}

export const defaultProfileInfoValidation: ProfileInfoValidation = {
  gender: true,
  name: true,
  address: true,
  email: true,
  phone: true,
  dateOfBirth: true,
  country: true,
};
