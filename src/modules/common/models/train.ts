export interface CustomerInfo {
  readonly bookerContactId: number;
  readonly idCardNumber: string;
  readonly name: string;
  readonly phone: string;
  readonly email: string;
}
export interface ContactInfo extends TravellerInfo {
  readonly identity: string;
  readonly email: string;
  readonly phone: string;
}
export interface ExportBillInfo {
  readonly company: string;
  readonly taxNumber: string;
  readonly address: string;
  readonly phone: string;
}

export interface TravellersInfo {
  readonly adults: AdultTravellerInfo[];
  readonly students: StudentTravellerInfo[];
  readonly childrens: ChildTravellerInfo[];
  readonly seniors: SeniorTravellerInfo[];
}
export type Option = 'identity' | 'birthday';
export interface AdultTravellerInfo extends TravellerInfo {
  readonly identity: string;
  readonly birthday: string;
  readonly option: Option;
}
export interface StudentTravellerInfo extends TravellerInfo {
  readonly identity: string;
}
export interface ChildTravellerInfo extends TravellerInfo {
  readonly birthday: string;
}
export interface SeniorTravellerInfo extends TravellerInfo {
  readonly birthday: string;
}
export interface TravellerInfo {
  readonly familyName: string;
  readonly givenName: string;
}

/*----------------------------------*/
export interface TravellerInfoValidation {
  readonly familyName: boolean;
  readonly givenName: boolean;
}
export interface CustomerInfoValidation {
  readonly bookerContactId: boolean;
  readonly name: boolean;
  readonly phone: boolean;
  readonly email: boolean;
  readonly idCardNumber: boolean;
}
export interface ContactInfoValidation extends TravellerInfoValidation {
  readonly identity: boolean;
  readonly email: boolean;
  readonly phone: boolean;
}
export interface ExportBillInfoValidation {
  readonly company: boolean;
  readonly taxNumber: boolean;
  readonly address: boolean;
  readonly phone: boolean;
}
export interface AdultInfoValidation extends TravellerInfoValidation {
  readonly identity: boolean;
  readonly birthday: boolean;
}
export interface StudentInfoValidation extends TravellerInfoValidation {
  readonly identity: boolean;
}
export interface ChildInfoValidation extends TravellerInfoValidation {
  readonly birthday: boolean;
}

export interface SeniorInfoValidation extends TravellerInfoValidation {
  readonly birthday: boolean;
}

export interface TravellersInfoValidation {
  readonly adults: AdultInfoValidation[];
  readonly students: StudentInfoValidation[];
  readonly childrens: ChildInfoValidation[];
  readonly seniors: SeniorInfoValidation[];
}
/*----------------------------------*/

const validTravellerValidation: TravellerInfoValidation = {
  familyName: true,
  givenName: true,
};
export const validTravellersInfoValidation: TravellersInfoValidation = {
  adults: [],
  students: [],
  childrens: [],
  seniors: [],
};
export const validCustomerInfoValidation: CustomerInfoValidation = {
  name: true,
  phone: true,
  email: true,
  bookerContactId: true,
  idCardNumber: true,
};
export const validContactInfoValidation: ContactInfoValidation = {
  ...validTravellerValidation,
  identity: true,
  email: true,
  phone: true,
};
export const validExportBillInfoValidation: ExportBillInfoValidation = {
  company: true,
  taxNumber: true,
  address: true,
  phone: true,
};

export const validAdultInfoValidation: AdultInfoValidation = {
  ...validTravellerValidation,
  identity: true,
  birthday: true,
};
export const validStudentInfoValidation: StudentInfoValidation = {
  ...validTravellerValidation,
  identity: true,
};
export const validChildInfoValidation: ChildInfoValidation = {
  ...validTravellerValidation,
  birthday: true,
};

export const validSeniorInfoValidation: SeniorInfoValidation = {
  ...validTravellerValidation,
  birthday: true,
};
export const defaultTravellersInfo: TravellersInfo = {
  adults: [],
  students: [],
  childrens: [],
  seniors: [],
};
export const defaultAdultTravellerInfo: AdultTravellerInfo = {
  birthday: '',
  familyName: '',
  givenName: '',
  identity: '',
  option: 'identity',
};
export const defaultStudentTravellerInfo: StudentTravellerInfo = {
  familyName: '',
  givenName: '',
  identity: '',
};
export const defaultChildTravellerInfo: ChildTravellerInfo = {
  birthday: '',
  familyName: '',
  givenName: '',
};
export const defaultSeniorTravellerInfo: SeniorTravellerInfo = {
  birthday: '',
  familyName: '',
  givenName: '',
};
export const defaultCustomerInfo: CustomerInfo = {
  name: '',
  phone: '',
  email: '',
  bookerContactId: 0,
  idCardNumber: '',
};
export const defaultContactInfo: ContactInfo = {
  email: '',
  familyName: '',
  givenName: '',
  identity: '',
  phone: '',
};
