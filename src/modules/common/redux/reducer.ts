import { Action as ActionRedux } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { Location, Tour, TourLocation } from '../models';
import { fetchThunk } from './thunks';

export interface CommonState {
  readonly networkErrorMsg?: string;
  readonly generalInfo: {
    flight?: Readonly<some>;
    tour?: Readonly<some>;
    train?: Readonly<some>;
  };
  readonly notableLocationsHotel: Location[];
  readonly notableLocationsTour: Tour[] | TourLocation[];
  readonly rewardsCategories?: some[];
  readonly allCountries: some[];
}

export const setNetworkError = createAction('common/setNetworkError', resolve => (errorMsg?: string) =>
  resolve({ errorMsg }),
);

export const setFlightGeneralInfo = createAction('common/setFlightGeneralInfo', resolve => (data: some) =>
  resolve({ data }),
);

export const setTourGeneralInfo = createAction('common/setTourGeneralInfo', resolve => (data: some) =>
  resolve({ data }),
);

export const setTrainGeneralInfo = createAction('common/setTrainGeneralInfo', resolve => (data: some) =>
  resolve({ data }),
);

export const setNotableLocationsHotel = createAction('common/setNotableLocationsHotel', resolve => (data: Location[]) =>
  resolve({ data }),
);
export const setNotableLocationsTour = createAction(
  'common/setNotableLocationsTour',
  resolve => (data: Tour[] | TourLocation[]) => resolve({ data }),
);
export const setRewardsCategories = createAction('common/setRewardsCategories', resolve => (vals: some[]) =>
  resolve({ vals }),
);
export const setAllCountries = createAction('common/setAllCountries', resolve => (vals: some[]) => resolve({ vals }));

export function fetchContactList(): ThunkAction<Promise<some>, AppState, null, ActionRedux<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getContactList}`,
        'post',
        true,
        JSON.stringify({
          filter: {},
          paging: {
            pageSize: 9999,
            page: 1,
          },
        }),
      ),
    );

    return json;
  };
}

export function fetchAllCountries(): ThunkAction<void, AppState, null, ActionRedux<string>> {
  return async (dispatch, getState) => {
    if (getState().common.allCountries.length === 0) {
      const json = await dispatch(fetchThunk(`${API_PATHS.getAllCountries}`, 'get', true));
      if (json.code === 200) {
        dispatch(setAllCountries(json.data.countries));
      }
    }
  };
}

export function fetchNotableTourLocation(): ThunkAction<void, AppState, null, ActionRedux<string>> {
  return (dispatch, getState) => {
    dispatch(fetchThunk(API_PATHS.getTopTourLocation, 'get', false)).then(json => {
      if (json.code === 200) {
        dispatch(setNotableLocationsTour(json.data));
      }
    });
  };
}

export function fetchNotableHotelLocation(): ThunkAction<void, AppState, null, ActionRedux<string>> {
  return (dispatch, getState) => {
    dispatch(fetchThunk(API_PATHS.getTopHotelLocation(10), 'get', false)).then(json => {
      if (json.code === 200) {
        dispatch(
          setNotableLocationsHotel(
            json.data.map((v: some) => {
              return { ...v, thumbnailUrl: v.thumb, name: v.provinceName, hotelId: -1 };
            }),
          ),
        );
      }
    });
  };
}

const actions = {
  setNetworkError,
  setFlightGeneralInfo,
  setTourGeneralInfo,
  setTrainGeneralInfo,
  setNotableLocationsHotel,
  setNotableLocationsTour,
  setRewardsCategories,
  setAllCountries,
};

type Action = ActionType<typeof actions>;

export default function reducer(
  state: CommonState = {
    generalInfo: {},
    notableLocationsHotel: [],
    notableLocationsTour: [],
    allCountries: [],
  },
  action: Action,
): CommonState {
  switch (action.type) {
    case getType(setNetworkError):
      return { ...state, networkErrorMsg: action.payload.errorMsg };
    case getType(setFlightGeneralInfo):
      return { ...state, generalInfo: { ...state.generalInfo, flight: action.payload.data } };
    case getType(setTourGeneralInfo):
      return { ...state, generalInfo: { ...state.generalInfo, tour: action.payload.data } };
    case getType(setTrainGeneralInfo):
      return { ...state, generalInfo: { ...state.generalInfo, train: action.payload.data } };
    case getType(setNotableLocationsHotel):
      return {
        ...state,
        notableLocationsHotel: action.payload.data,
      };
    case getType(setNotableLocationsTour):
      return {
        ...state,
        notableLocationsTour: action.payload.data,
      };
    case getType(setRewardsCategories):
      return {
        ...state,
        rewardsCategories: action.payload.vals,
      };
    case getType(setAllCountries):
      return { ...state, allCountries: action.payload.vals };
    default:
      return state;
  }
}
