import { push } from 'connected-react-router';
import { get, remove } from 'js-cookie';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { DEV, DINOGO, MY_TOUR, some, TEST } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { setUserData } from '../../account/redux/accountReducer';
import { ACCESS_TOKEN, USER_ID } from '../../auth/constants';
import { out } from '../../auth/redux/authReducer';
import { REASON } from '../components/SendHomeReasonDialog';
import { SEND_HOME_REASONS } from '../constants';
import { setNetworkError } from './reducer';

export function fetchThunk(
  url: string,
  method: 'get' | 'post' | 'delete' | 'put' = 'get',
  auth = true,
  body?: string | FormData,
  fallbackResponse?: some, // if given, will not retry at all and return this
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const done = false;
    while (!done) {
      let res;
      const { fingerPrint } = getState().auth;
      try {
        const headers = {
          login_token: get(ACCESS_TOKEN) || '',
          'Content-Type': 'application/json',
          'Accept-Language': getState().intl.locale.substring(0, 2),
          deviceInfo: 'New_Web_Tablet_Desktop',
          caId: MY_TOUR ? '17' : DINOGO ? '3' : '1',
          deviceId: fingerPrint,
          userId: get(USER_ID) || '',
        };
        if (!auth || !get(ACCESS_TOKEN)) {
          delete headers.login_token;
        }
        // if (!DINOGO) {
        //   delete headers.caid;
        // }
        if (!MY_TOUR || !get(USER_ID)) {
          delete headers.userId;
        }
        if (DEV || TEST) {
          // delete headers.deviceid;
        }
        if (body instanceof FormData) {
          delete headers['Content-Type'];
        }

        res = await fetch(url, {
          method,
          body,
          headers,
          cache: 'no-store',
        });
      } catch (_) {}

      if (res !== undefined) {
        if (res.ok) {
          const json = await res.json();
          return json;
        }

        if (res.status === 400) {
          return res.json();
        }
        if (res.status === 401 || res.status === 403) {
          dispatch(out());
          remove(ACCESS_TOKEN);
          dispatch(setUserData());
          dispatch(push(`/?${REASON}=${encodeURIComponent(SEND_HOME_REASONS.invalidAccessToken)}`));
          return { code: res.status };
        }
        if (res.status === 413) {
          let msg = '413';
          try {
            msg = await res.text();
          } finally {
          }
          throw new Error(msg);
        }
      }

      if (fallbackResponse) {
        return fallbackResponse;
      }

      let hasInternet = true;
      try {
        await fetch('https://tripi.vn', { mode: 'no-cors' });
      } catch (_) {
        hasInternet = false;
      }

      dispatch(setNetworkError(hasInternet ? 'serverProblem' : 'unstableNetwork'));

      do {
        await new Promise(resolve => setTimeout(resolve, 350));
      } while (getState().common.networkErrorMsg);
    }
    return {};
  };
}
