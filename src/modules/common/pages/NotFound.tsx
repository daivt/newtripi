import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import NotFoundTabletDesktop from '../components/NotFoundTabletDesktop';
import SendHomeReasonDialog from '../components/SendHomeReasonDialog';

interface INotFoundProps extends WrappedComponentProps {}

const NotFound: React.FunctionComponent<INotFoundProps> = props => {
  const { intl } = props;
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'title' })}</title>
      </Helmet>
      <SendHomeReasonDialog />
      <NotFoundTabletDesktop />
    </>
  );
};

export default injectIntl(NotFound);
