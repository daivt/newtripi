import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import MediaQuery from 'react-responsive';
import { RouteComponentProps, withRouter } from 'react-router';
import { MOBILE_WIDTH_NUM, ModuleType } from '../../../constants';
import CheckoutFailMobile from '../../booking/components/checkout/CheckoutFailMobile';
import CheckoutFailTabletDesktop from '../../booking/components/checkout/CheckoutFailTabletDesktop';

interface Props extends WrappedComponentProps, RouteComponentProps {}

interface State {
  moduleType?: ModuleType | 'all';
}

class PaymentFail extends React.PureComponent<Props, State> {
  state: State = {};

  componentDidMount() {
    const { match } = this.props;
    const params = match.params as State;

    this.setState({
      moduleType: params.moduleType,
    });
  }

  public render() {
    const { intl, location } = this.props;
    const { moduleType } = this.state;

    const msg = location.state && location.state.msg ? location.state.msg : undefined;

    if (!moduleType) {
      return <div />;
    }

    return (
      <>
        <Helmet>
          <title>{intl.formatMessage({ id: 'booking.paymentFail' })}</title>
        </Helmet>
        <MediaQuery minWidth={MOBILE_WIDTH_NUM}>
          {match => {
            if (match) {
              return <CheckoutFailTabletDesktop message={msg} moduleType={moduleType} />;
            }
            return <CheckoutFailMobile message={msg} moduleType={moduleType} />;
          }}
        </MediaQuery>
      </>
    );
  }
}

export default injectIntl(withRouter(PaymentFail));
