import { replace } from 'connected-react-router';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ModuleType, ROUTES, some, MOBILE_WIDTH_NUM } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import CheckoutSuccessMobile from '../../booking/components/checkout/CheckoutSuccessMobile';
import CheckoutSuccessTabletDesktop from '../../booking/components/checkout/CheckoutSuccessTabletDesktop';
import { HOLD_PARAM_NAME } from '../../booking/constants';
import LoadingIcon from '../components/LoadingIcon';
import { fetchThunk } from '../redux/thunks';

interface Props
  extends WrappedComponentProps,
    RouteComponentProps<{ moduleType: ModuleType; bookingId: string }>,
    ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  moduleType?: ModuleType;
  bookingId: string;
  fetching: boolean;
  mobileData?: some;
  msg?: string;
}

class PaymentSuccess extends React.PureComponent<Props, State> {
  state: State = {
    bookingId: '',
    fetching: false,
    mobileData: this.props.stateRouter && this.props.stateRouter.mobilePayResutlData,
  };

  async componentDidMount() {
    const { match, dispatch } = this.props;
    const { params } = match;
    this.setState({
      moduleType: params.moduleType,
      bookingId: params.bookingId,
      fetching: true,
    });

    const json = await dispatch(
      fetchThunk(API_PATHS.checkoutSuccess, 'post', false, JSON.stringify({ url: window.location.href })),
    );
    if (json.code === 200 || json.code === '200') {
      this.setState({ msg: json.message });
    } else {
      dispatch(
        replace({
          pathname: ROUTES.payFailure.gen(params.moduleType),
          state: { msg: json.message },
        }),
      );
    }

    if (params.moduleType === 'mobile_card') {
      const json1 = await dispatch(
        fetchThunk(
          API_PATHS.getBookingInfoCode,
          'post',
          false,
          JSON.stringify({
            bookingId: params.bookingId,
          }),
        ),
      );
      if (json1.code === 200) {
        this.setState({ mobileData: json1.data });
      }
    }
    if (params.moduleType === 'mobile_topup') {
      const json2 = await dispatch(
        fetchThunk(
          API_PATHS.getBookingInfoDiret,
          'post',
          false,
          JSON.stringify({
            bookingId: params.bookingId,
          }),
        ),
      );
      if (json2.code === 200) {
        this.setState({ mobileData: json2.data });
      }
    }

    this.setState({ fetching: false });
  }

  public render() {
    const { intl } = this.props;
    const { moduleType, bookingId, fetching, mobileData, msg } = this.state;

    if ((!moduleType && !bookingId) || fetching) {
      return (
        <div style={{ margin: '40px' }}>
          <LoadingIcon />
        </div>
      );
    }
    const params = new URLSearchParams(window.location.search);
    const hold = !!params.get(HOLD_PARAM_NAME);

    return (
      <>
        <Helmet>
          <title>
            {msg ||
              intl.formatMessage({
                id: hold ? 'booking.checkout.holdSuccess' : 'booking.checkout.success',
              })}
          </title>
        </Helmet>
        <MediaQuery minWidth={MOBILE_WIDTH_NUM}>
          {match => {
            if (match) {
              return (
                <CheckoutSuccessTabletDesktop
                  moduleType={moduleType}
                  bookingId={bookingId}
                  msg={msg}
                  mobileData={mobileData}
                  hold={hold}
                />
              );
            }
            return (
              <CheckoutSuccessMobile
                moduleType={moduleType}
                bookingId={bookingId}
                msg={msg}
                mobileData={mobileData}
                hold={hold}
              />
            );
          }}
        </MediaQuery>
      </>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    stateRouter: state.router.location.state,
    router: state.router,
  };
};

export default connect(mapStateToProps)(injectIntl(PaymentSuccess));
