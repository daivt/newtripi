import * as React from 'react';
import { ReactComponent as RedArrow } from '../../../svg/ic_red_arrow.svg';
import { Typography } from '@material-ui/core';
import { RED } from '../../../colors';
interface Props {
  text?: string | number;
  style?: React.CSSProperties;
}

const HotDealTag: React.FunctionComponent<Props> = props => {
  return (
    <div
      style={{
        ...props.style,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        width: '68px',
        height: '24px',
        color: '#fff',
        background: RED,
      }}
    >
      <RedArrow style={{ position: 'absolute', bottom: '-5px', left: 0 }} />
      <Typography variant="subtitle2">
        {props.text}
        {props.children}
      </Typography>
    </div>
  );
};

export default HotDealTag;
