import React, { PureComponent } from 'react';
import { Dialog, Typography, IconButton, Button } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import { FormattedMessage } from 'react-intl';

interface Props {
  show: boolean;
  message: React.ReactNode;
  onCancel(): void;
  onAccept(): void;
  header?: React.ReactNode;
  cancelMessageId?: string;
  confirmMessageId?: string;
}

export default class ConfirmDialog extends PureComponent<Props> {
  render() {
    const { show, message, onCancel, onAccept, header, cancelMessageId, confirmMessageId } = this.props;
    return (
      <Dialog
        open={show}
        onClose={onCancel}
        PaperProps={{
          style: {
            width: '500px',
            textAlign: 'center',
          },
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', justifyContent: 'flex-end', flex: !header ? 1 : undefined }}>
            <IconButton style={{ padding: '8px' }} size="small" onClick={onCancel}>
              <IconClose />
            </IconButton>
          </div>
          {!!header && (
            <div style={{ display: 'flex', flex: 1, paddingLeft: '16px', justifyContent: 'center' }}>{header}</div>
          )}
          {message}
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              paddingTop: '32px',
              paddingBottom: '44px',
            }}
          >
            <div style={{ padding: '0 16px' }}>
              <Button style={{ width: '170px' }} variant="outlined" size="large" onClick={onCancel}>
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id={cancelMessageId || 'cancel'} />
                </Typography>
              </Button>
            </div>
            <div style={{ padding: '0 16px' }}>
              <Button style={{ width: '170px' }} variant="contained" color="secondary" size="large" onClick={onAccept}>
                <Typography variant="button">
                  <FormattedMessage id={confirmMessageId || 'accept'} />
                </Typography>
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}
