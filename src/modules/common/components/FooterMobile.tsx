import { Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { BLACK_TEXT } from '../../../colors';
import { ReactComponent as IconEmail } from '../../../svg/ic_email.svg';
import { ReactComponent as IconLocation } from '../../../svg/ic_location.svg';
import { ReactComponent as IconPhone } from '../../../svg/ic_phone.svg';

interface Props {}

const FooterMobile = (props: Props) => {
  const intl = useIntl();

  return (
    <Typography variant="caption">
      <Container style={{ backgroundColor: BLACK_TEXT, color: 'white' }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            padding: '32px 16px',
          }}
        >
          <div
            style={{
              fontSize: '16px',
              fontWeight: 500,
              lineHeight: '24px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <IconPhone style={{ paddingRight: '10px' }} />
            <FormattedMessage id="footer.phoneNumber" />
          </div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              padding: '6px 0',
            }}
          >
            <IconEmail style={{ paddingRight: '10px' }} />
            <a
              style={{ color: '#fff', textDecoration: 'none' }}
              href={`mailto:${intl.formatMessage({ id: 'footer.emailContact' })}`}
            >
              <FormattedMessage id="footer.emailContact" />
            </a>
          </div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <IconLocation style={{ paddingRight: '10px', flexShrink: 0 }} />
            <FormattedMessage id="footer.address" />
          </div>
        </div>
      </Container>
    </Typography>
  );
};

export default FooterMobile;
