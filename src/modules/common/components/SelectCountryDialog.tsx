import { Dialog, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { latinise } from 'voca';
import { HOVER_GREY } from '../../../colors';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { fetchAllCountries } from '../redux/reducer';
import { BootstrapInput } from './elements';

const ItemRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 0.5px solid rgb(189, 189, 189);
  min-height: 48px;
  padding: 0px 8px;
  cursor: pointer;

  :hover {
    background: ${HOVER_GREY};
  }
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  show: boolean;
  onClose(): void;
  onSelect(data: some): void;
}

function mapStateToProps(state: AppState) {
  return { allCountries: state.common.allCountries };
}

const SelectCountryDialog: React.FC<Props> = props => {
  const { show, onClose, onSelect, allCountries, dispatch } = props;
  const [str, setStr] = React.useState('');
  const intl = useIntl();

  React.useEffect(() => {
    dispatch(fetchAllCountries());
  }, [dispatch]);

  return (
    <Dialog open={show} keepMounted={false} maxWidth="md">
      <div style={{ width: '500px', height: 'calc(100vh - 120px)', display: 'flex', flexDirection: 'column' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography
            variant="h5"
            style={{
              display: 'flex',
              alignItems: 'center',
              height: '40px',
              marginTop: '16px',
              marginLeft: '24px',
            }}
          >
            <FormattedMessage id="country.selectCountry" />
          </Typography>

          <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '6px' }}>
            <IconButton style={{ padding: '8px' }} size="small" onClick={() => onClose()}>
              <IconClose />
            </IconButton>
          </div>
        </div>
        <div style={{ flex: 1, overflow: 'auto', padding: '0 24px' }}>
          <div style={{ margin: '15px 0' }}>
            <BootstrapInput
              autoFocus
              fullWidth
              value={str}
              onChange={e => setStr(e.target.value)}
              placeholder={intl.formatMessage({ id: 'typeToFilter' })}
            />
          </div>
          {allCountries &&
            allCountries
              .filter(country => {
                const name = country.name as string;
                return name.toLocaleLowerCase().indexOf(latinise(str.trim()).toLocaleLowerCase()) > -1;
              })
              .map(country => (
                <ItemRow key={country.id} onClick={() => onSelect(country)}>
                  <Typography variant="body2">{country.name}</Typography>
                </ItemRow>
              ))}
        </div>
      </div>
    </Dialog>
  );
};

export default connect(mapStateToProps)(SelectCountryDialog);
