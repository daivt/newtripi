import { Container } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { withTheme } from 'styled-components';
import { some } from '../../../constants';
import '../../../scss/svg.scss';
import { ReactComponent as TripiLogo } from '../../../svg/TripiLogo.svg';
import { BoxHeader, HeaderContainer, SubHeader } from './Header.styles';

interface Props {
  theme: some;
}
interface State {}

class HeaderMobile extends PureComponent<Props, State> {
  render() {
    const { theme } = this.props;

    return (
      <HeaderContainer>
        <BoxHeader tabIndex={-1} style={{ position: 'static', outline: 'none', zIndex: 100 }} light>
          <SubHeader light>
            <Container
              style={{
                height: '66px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Link to="/" style={{ marginRight: '30px' }}>
                <TripiLogo className="svgFillAll" fill={theme.primary} />
              </Link>
            </Container>
          </SubHeader>
        </BoxHeader>
      </HeaderContainer>
    );
  }
}

export default withTheme(HeaderMobile);
