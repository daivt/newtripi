import { Button, Dialog, DialogContent, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { replace } from 'connected-react-router';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Dispatch } from 'redux';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { stringifyFlightSearchStateAndFilterParams } from '../../search/utils';
import { SEND_HOME_REASONS } from '../constants';

export const REASON = 'reason';
export const MESSAGE = 'message';

interface Props extends RouteComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}

interface State {}

class SendHomeReasonDialog extends PureComponent<Props, State> {
  close = () => {
    this.props.history.replace({ search: '' });
    if (this.props.result.flight.searchParams) {
      // Attempt to send to flight result page if we can

      const params = new URLSearchParams(this.props.location.search);
      const reason = params.get(REASON);
      if (reason === SEND_HOME_REASONS.expiredTicket) {
        this.props.dispatch(
          replace(
            `${ROUTES.flight.flightResult}?${stringifyFlightSearchStateAndFilterParams(
              this.props.result.flight.searchParams,
            )}`,
          ),
        );
      }
    }
  };

  render() {
    const params = new URLSearchParams(this.props.location.search);
    const reason = params.get(REASON);
    const message = params.get(MESSAGE);
    let msgId =
      reason === SEND_HOME_REASONS.noDataInSession
        ? 'home.noDataInSessionSendHome'
        : reason === SEND_HOME_REASONS.invalidInput
        ? 'home.invalidInputSendHome'
        : reason === SEND_HOME_REASONS.invalidLink
        ? 'home.invalidLink'
        : reason === SEND_HOME_REASONS.expiredTicket
        ? 'home.expiredTicket'
        : reason === SEND_HOME_REASONS.invalidAccessToken
        ? 'home.invalidAccessToken'
        : undefined;

    if (reason === SEND_HOME_REASONS.expiredTicket) {
      const { searchParams } = this.props.result.flight;
      if (searchParams) {
        msgId = 'home.expiredTicket2';
      }
    }

    return (
      <Dialog open={!!reason} maxWidth="sm">
        <div style={{ position: 'relative' }}>
          <div
            style={{
              width: '100%',
              textAlign: 'end',
              paddingRight: '3px',
              paddingTop: '3px',
              position: 'absolute',
            }}
          >
            <IconButton size="small" onClick={this.close}>
              <CloseIcon style={{ fontSize: '24px' }} />
            </IconButton>
          </div>
        </div>
        <DialogContent
          style={{
            padding: '0 34px',
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'space-around',
          }}
        >
          <div style={{ backgroundColor: 'white', padding: '15px', textAlign: 'center' }}>
            <div>{msgId && <FormattedMessage id={msgId} />}</div>
            <div>{message && `(${message})`}</div>
            <Button
              style={{
                padding: '1px 0',
                width: '90px',
                boxShadow: 'none',
                marginTop: '20px',
              }}
              color="secondary"
              variant="contained"
              size="small"
              onClick={this.close}
            >
              <FormattedMessage id="ok" />
            </Button>
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    result: state.result,
  };
}

export default connect(mapStateToProps)(withRouter(SendHomeReasonDialog));
