import { FormHelperText, IconButton, InputAdornment } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React from 'react';
import { WrappedComponentProps, injectIntl, FormattedMessage } from 'react-intl';
import { FreeTextField } from '../../booking/components/Form';

interface Props extends WrappedComponentProps {
  password: string;
  confirmPassword: string;
  passwordValidation: boolean;
  confirmPasswordValidation: boolean;
  updatePassword(value: string, validation: boolean): void;
  updateConfirmPassword(value: string, validation: boolean): void;
  ticktock: boolean;
}

const NewPasswordBox: React.FC<Props> = props => {
  const {
    password,
    confirmPassword,
    passwordValidation,
    confirmPasswordValidation,
    updatePassword,
    updateConfirmPassword,
    intl,
    ticktock,
  } = props;

  const [showPassword, setShowPassword] = React.useState<boolean>(false);

  return (
    <>
      <form>
        <FreeTextField
          key={`password${ticktock}`}
          text={password}
          valid={passwordValidation}
          header={intl.formatMessage({ id: 'auth.newPassword' })}
          placeholder="******"
          type={showPassword ? 'text' : 'password'}
          update={value => updatePassword(value, true)}
          endAdornment={
            <InputAdornment position="end">
              <IconButton onClick={() => setShowPassword(!showPassword)}>
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
        />
        <FormHelperText error={true} style={{ marginTop: '16px' }}>
          {!passwordValidation && <FormattedMessage id="auth.passwordLengthError" />}
        </FormHelperText>
      </form>
      <form>
        <FreeTextField
          key={`rePassword${ticktock}`}
          text={confirmPassword}
          valid={confirmPasswordValidation}
          header={intl.formatMessage({ id: 'auth.rePassword' })}
          placeholder="******"
          type={showPassword ? 'text' : 'password'}
          update={value => updateConfirmPassword(value, true)}
        />
        <FormHelperText error={true}>
          {!confirmPasswordValidation && <FormattedMessage id="auth.checkPassword" />}
        </FormHelperText>
      </form>
    </>
  );
};

export default injectIntl(NewPasswordBox);
