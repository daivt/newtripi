import * as React from 'react';
import styles from './styles.module.scss';
import dinogoStyles from './dinogoStyles.module.scss';
import myTourStyles from './myTourStyles.module.scss';
import { DINOGO, MY_TOUR } from '../../../../constants';

class LoadingIcon extends React.PureComponent<{ style?: React.CSSProperties }> {
  public render(): JSX.Element {
    const { style } = this.props;
    return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', ...style }}>
        <div
          className={DINOGO ? dinogoStyles['lds-roller'] : MY_TOUR ? myTourStyles['lds-roller'] : styles['lds-roller']}
        >
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    );
  }
}

export default LoadingIcon;
