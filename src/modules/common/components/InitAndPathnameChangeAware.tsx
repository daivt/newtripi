import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { matchPath } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../API';
import { ROUTES, ServiceType } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { validateAccessToken } from '../../auth/redux/authThunks';
import { TAB_PARAM_NAME } from '../../management/constants';
import { fetchRewards } from '../../result/redux/rewardResultReducer';
import {
  fetchNotableHotelLocation,
  fetchNotableTourLocation,
  setFlightGeneralInfo,
  setTourGeneralInfo,
  setTrainGeneralInfo,
} from '../redux/reducer';
import { fetchThunk } from '../redux/thunks';

function matchService(service: ServiceType, pathname: string) {
  return pathname.startsWith(`/${service}`);
}

function matchManagementTab(service: ServiceType, pathname: string, search: string) {
  if (matchPath(pathname, ROUTES.management)) {
    const params = new URLSearchParams(search);
    const tab = params.get(TAB_PARAM_NAME);

    return tab === service;
  }
  return false;
}
interface Props extends React.PropsWithChildren<ReturnType<typeof mapStateToProps>> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  webview?: boolean;
}

const InitAndPathnameChangeAware: React.FC<Props> = props => {
  const { common, location, dispatch, webview, action } = props;

  useEffect(() => {
    dispatch(validateAccessToken(true));
  }, [dispatch]);

  useEffect(() => {
    if (!webview && action === 'PUSH') {
      window.scrollTo({ top: 0 });
    }
  }, [location.pathname, webview, action]);

  useEffect(() => {
    if (matchService('flight', location.pathname) || matchManagementTab('flight', location.pathname, location.search)) {
      if (!common.generalInfo.flight) {
        dispatch(fetchThunk(API_PATHS.flightGeneralInfo, 'get', false)).then(json =>
          dispatch(setFlightGeneralInfo(json.data)),
        );
      }
    }
  }, [location, common.generalInfo.flight, dispatch]);

  useEffect(() => {
    if (matchService('hotel', location.pathname) || matchManagementTab('hotel', location.pathname, location.search)) {
      if (common.notableLocationsHotel.length === 0) {
        dispatch(fetchNotableHotelLocation());
      }
    }
  }, [location, common.notableLocationsHotel.length, dispatch]);

  useEffect(() => {
    if (matchService('train', location.pathname) || matchManagementTab('train', location.pathname, location.search)) {
      if (!common.generalInfo.train) {
        dispatch(fetchThunk(API_PATHS.trainGeneralInfo, 'get', false)).then(json =>
          dispatch(setTrainGeneralInfo(json.data)),
        );
      }
    }
  }, [location, common.generalInfo.train, dispatch]);

  useEffect(() => {
    if (matchService('tour', location.pathname) || matchManagementTab('tour', location.pathname, location.search)) {
      if (!common.generalInfo.tour && common.notableLocationsTour.length === 0) {
        dispatch(fetchThunk(API_PATHS.tourGeneralInfo, 'get', false)).then(json =>
          dispatch(setTourGeneralInfo(json.data)),
        );
        dispatch(fetchNotableTourLocation());
      }
    }
  }, [location, common.generalInfo.tour, common.notableLocationsTour.length, dispatch]);

  useEffect(() => {
    if (matchService('reward', location.pathname) || matchPath(location.pathname, ROUTES.management)) {
      if (!common.rewardsCategories) {
        dispatch(fetchRewards()); // call to get reward categories since there is no general info for rewards
      }
    }
  }, [location, common.rewardsCategories, dispatch]);

  return <>{props.children}</>;
};

function mapStateToProps(state: AppState) {
  return { location: state.router.location, action: state.router.action, common: state.common, auth: state.auth };
}

export default connect(mapStateToProps)(InitAndPathnameChangeAware);
