import { Button, IconButton, Typography } from '@material-ui/core';
import { AddCircleOutline } from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useDropzone } from 'react-dropzone';
import { FormattedMessage, useIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../API';
import {
  BLUE,
  DARK_GREY,
  GREEN,
  GREY,
  LIGHT_BLUE,
  LIGHT_GREY,
  PURPLE,
  RED,
  YELLOW,
} from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as Rating1Icon } from '../../../../svg/rating_1_icon.svg';
import { ReactComponent as Rating2Icon } from '../../../../svg/rating_2_icon.svg';
import { ReactComponent as Rating3Icon } from '../../../../svg/rating_3_icon.svg';
import { ReactComponent as Rating4Icon } from '../../../../svg/rating_4_icon.svg';
import { ReactComponent as Rating5Icon } from '../../../../svg/rating_5_icon.svg';
import { ReactComponent as ThankYouIcon } from '../../../../svg/thank_you_icon.svg';
import { uploadFile } from '../../../auth/redux/authThunks';
import { FreeTextField } from '../../../booking/components/Form';
import { fetchThunk } from '../../redux/thunks';
import { CheckButton } from '../elements';
import LoadingButton from '../LoadingButton';
import ProgressiveImage from '../ProgressiveImage';

const UploadImageBox = styled.div`
  background: ${LIGHT_GREY};
  border: 0.5px dashed ${GREY};
  box-sizing: border-box;
  width: 100px;
  height: 56px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

interface Props extends ReturnType<typeof mapStateToProps> {
  onClose: () => void;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const mapStateToProps = (state: AppState) => ({});

const sentimentIcons = [
  {
    id: 1,
    icon: (
      <Rating1Icon style={{ width: '34px', height: '34px', fill: DARK_GREY }} className="svgFill" />
    ),
    selectedIcon: <Rating1Icon style={{ width: '34px', height: '34px', fill: RED }} />,
    message: 'header.feedback.terrible',
    color: RED,
  },
  {
    id: 2,
    icon: (
      <Rating2Icon style={{ width: '34px', height: '34px', fill: DARK_GREY }} className="svgFill" />
    ),
    selectedIcon: <Rating2Icon style={{ width: '34px', height: '34px', fill: YELLOW }} />,
    message: 'header.feedback.bad',
    color: YELLOW,
  },
  {
    id: 3,
    icon: (
      <Rating3Icon style={{ width: '34px', height: '34px', fill: DARK_GREY }} className="svgFill" />
    ),
    selectedIcon: <Rating3Icon style={{ width: '34px', height: '34px', fill: LIGHT_BLUE }} />,
    message: 'header.feedback.normal',
    color: LIGHT_BLUE,
  },
  {
    id: 4,
    icon: (
      <Rating4Icon style={{ width: '34px', height: '34px', fill: DARK_GREY }} className="svgFill" />
    ),
    selectedIcon: <Rating4Icon style={{ width: '34px', height: '34px', fill: PURPLE }} />,
    message: 'header.feedback.good',
    color: PURPLE,
  },
  {
    id: 5,
    icon: (
      <Rating5Icon style={{ width: '34px', height: '34px', fill: DARK_GREY }} className="svgFill" />
    ),
    selectedIcon: <Rating5Icon style={{ width: '34px', height: '34px', fill: GREEN }} />,
    message: 'header.feedback.awesome',
    color: GREEN,
  },
];

const requestTypes = [
  { id: 1, message: 'header.feedback.question', category: 'question' },
  { id: 2, message: 'header.feedback.request', category: 'request' },
  { id: 3, message: 'header.feedback.report', category: 'report' },
  { id: 4, message: 'header.feedback.other', category: 'other' },
];

const SendFeedbackDialog: React.FC<Props> = props => {
  const { onClose, dispatch } = props;
  const [feedbackContent, setFeedbackContent] = React.useState('');
  const [sentiment, setSentiment] = React.useState(0);
  const [category, setCategory] = React.useState(0);
  const [uploading, setUploading] = React.useState(false);
  const [imageList, setImageList] = React.useState<string[]>([]);
  const { enqueueSnackbar } = useSnackbar();
  const [sending, setSending] = React.useState(false);
  const [sendSuccess, setSendSuccess] = React.useState(false);
  const intl = useIntl();

  const { getRootProps, getInputProps } = useDropzone({
    noKeyboard: true,
    multiple: false,
    onDrop: acceptedFiles => uploadPhoto(acceptedFiles),
  });

  const uploadPhoto = React.useCallback(
    async (files: File[]) => {
      setUploading(true);
      try {
        const json = await dispatch(uploadFile(files[0]));
        if (json.code === 200) {
          const newImageList = [...imageList];
          newImageList.unshift(json.photo.link);
          setImageList(newImageList);
        } else {
          const arr = JSON.parse(json.message);
          enqueueSnackbar(arr[0].value, {
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          });
        }
      } catch (err) {
        enqueueSnackbar('File too large', {
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        });
      }
      setUploading(false);
    },
    [dispatch, enqueueSnackbar, imageList],
  );

  const sendFeedback = React.useCallback(async () => {
    setSending(true);
    const selectedCategory = requestTypes.find(item => item.id === category);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.sendFeedback,
        'post',
        true,
        JSON.stringify({
          category: selectedCategory ? selectedCategory.category : '',
          images: imageList,
          description: feedbackContent,
          startNum: sentiment,
        }),
      ),
    );
    if (json.code === 200) {
      setSendSuccess(true);
    }
    setSending(false);
  }, [category, dispatch, feedbackContent, imageList, sentiment]);

  return (
    <>
      {sendSuccess ? (
        <div
          style={{
            minHeight: '268px',
            width: '444px',
            textAlign: 'end',
            padding: '10px',
          }}
        >
          <IconButton size="small" onClick={onClose}>
            <CloseIcon style={{ fontSize: '24px' }} />
          </IconButton>
          <div style={{ textAlign: 'center' }}>
            <ThankYouIcon style={{ margin: 'auto' }} />
            <Typography variant="body2" style={{ marginTop: '24px' }}>
              <FormattedMessage id="header.feedback.thanksForYourFeedback" />
            </Typography>
            <Button
              size="large"
              color="secondary"
              variant="contained"
              style={{
                minWidth: '114px',
                marginTop: '24px',
              }}
              onClick={onClose}
            >
              <FormattedMessage id="accept" />
            </Button>
          </div>
        </div>
      ) : (
        <>
          <div
            style={{
              textAlign: 'end',
              paddingRight: '3px',
              paddingTop: '3px',
            }}
          >
            <IconButton size="small" onClick={onClose}>
              <CloseIcon style={{ fontSize: '24px' }} />
            </IconButton>
          </div>
          <div style={{ padding: '24px', paddingTop: 0, width: '600px', overflowY: 'auto' }}>
            <Typography variant="subtitle1">
              <FormattedMessage id="header.feedback.sendInfo" />
            </Typography>
            <Typography variant="body1" style={{ marginTop: '16px' }} color="textSecondary">
              <FormattedMessage id="header.feedback.description" />
            </Typography>
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '8px' }}>
              {sentimentIcons.map(item => (
                <Button key={item.id}>
                  <div
                    style={{
                      cursor: 'pointer',
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      padding: '8px',
                      minWidth: '80px',
                    }}
                    onClick={() => setSentiment(item.id)}
                  >
                    {item.id === sentiment ? item.selectedIcon : item.icon}
                    <Typography
                      variant="caption"
                      style={{
                        color: item.id === sentiment ? item.color : 'black',
                        marginTop: '4px',
                      }}
                    >
                      <FormattedMessage id={item.message} />
                    </Typography>
                  </div>
                </Button>
              ))}
            </div>
            <Typography variant="body1" style={{ marginTop: '16px' }} color="textSecondary">
              <FormattedMessage id="header.feedback.questionForUs" />
            </Typography>
            <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px' }}>
              {requestTypes.map(item => (
                <CheckButton
                  size="large"
                  style={{
                    width: '130px',
                    cursor: 'pointer',
                    display: 'flex',
                    justifyContent: 'center',
                    textAlign: 'center',
                    borderRadius: undefined,
                  }}
                  color="primary"
                  active={category === item.id}
                  key={item.id}
                  onClick={() => setCategory(item.id)}
                >
                  <Typography variant="body1">
                    <FormattedMessage id={item.message} />
                  </Typography>
                </CheckButton>
              ))}
            </div>
            <FreeTextField
              style={{ marginTop: '32px', marginRight: '0px' }}
              text={feedbackContent}
              placeholder={intl.formatMessage({ id: 'content' })}
              update={value => setFeedbackContent(value)}
              multiline={true}
              rows={'5'}
            />
            <div style={{ marginTop: '12px' }}>
              <Typography variant="body2" style={{ fontWeight: 'bold' }}>
                <FormattedMessage id="header.feedback.image" />
              </Typography>
              <div style={{ marginTop: '8px', display: 'flex' }}>
                {imageList.map(item => (
                  <ProgressiveImage
                    style={{
                      width: '100px',
                      height: '56px',
                      objectFit: 'cover',
                      marginRight: '12px',
                    }}
                    src={item}
                    alt=""
                    key={item}
                  />
                ))}
                {imageList.length < 5 && (
                  <LoadingButton loading={uploading} style={{ padding: '0px' }}>
                    <UploadImageBox {...getRootProps()}>
                      {!uploading && <input {...getInputProps()} />}
                      <AddCircleOutline style={{ fontSize: '18px', color: BLUE }} />
                      <Typography variant="caption" style={{ color: BLUE, marginTop: '4px' }}>
                        <FormattedMessage id="header.feedback.addImage" />
                      </Typography>
                    </UploadImageBox>
                  </LoadingButton>
                )}
              </div>
            </div>
            <div style={{ textAlign: 'center' }}>
              <Typography
                variant="caption"
                style={{ opacity: sentiment === 0 ? 1 : 0, color: RED }}
              >
                <FormattedMessage id="header.feedback.pleaseGiveFeedback" />
              </Typography>
            </div>
            <div style={{ marginTop: '10px', display: 'flex', justifyContent: 'center' }}>
              <LoadingButton
                size="large"
                color="secondary"
                variant="contained"
                style={{ minWidth: '144px' }}
                disabled={uploading || sentiment === 0}
                loading={sending}
                onClick={sendFeedback}
              >
                <FormattedMessage id="header.feedback.send" />
              </LoadingButton>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export default connect(mapStateToProps)(SendFeedbackDialog);
