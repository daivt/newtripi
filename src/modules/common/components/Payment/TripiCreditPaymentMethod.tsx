import { Button, Checkbox, Collapse, FormControlLabel, Popover, Slider, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE, GREY } from '../../../../colors';
import { ModuleType, ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconLoyalty } from '../../../../svg/ic_loyalty.svg';
import { ReactComponent as IconInfo } from '../../../../svg/ic_payment_info.svg';
import { validateAccessToken } from '../../../auth/redux/authThunks';
import {
  fetchPointPayment as fetchFlightPointPayment,
  setPointUsing as setFlightPointUsing,
  setUsePointPayment as setFlightUsePointPayment,
} from '../../../booking/redux/flightBookingReducer';
import {
  fetchPointPayment as fetchHotelPointPayment,
  setPointUsing as setHotelPointUsing,
  setUsePointPayment as setHotelUsePointPayment,
} from '../../../booking/redux/hotelBookingReducer';
import {
  fetchPointPaymentCode as fetchMobilePointPaymentCode,
  fetchPointPaymentDirect as fetchMobilePointPaymentDirect,
  setPointUsing as setMobilePointUsing,
  setUsePointPayment as setMobileUsePointPayment,
} from '../../../booking/redux/mobileReducer';
import {
  fetchPointPayment as fetchTourPointPayment,
  setPointUsing as setTourPointUsing,
  setUsePointPayment as setTourUsePointPayment,
} from '../../../booking/redux/tourBookingReducer';
import {
  fetchPointPayment as fetchTrainBookingPointPayment,
  setPointUsing as setTrainBookingPointUsing,
  setUsePointPayment as setTrainBookingUsePointPayment,
} from '../../../booking/redux/trainBookingReducer';
import { getPointPaymentData, initPointPayment } from '../../../booking/utils';
import { ACTION_PARAM_NAME } from '../../../management/constants';
import {
  fetchPointPayment as fetchHoldingBookingPointPayment,
  setPointUsing as setHoldingBookingPointUsing,
  setUsePointPayment as setHoldingBookingUsePointPayment,
} from '../../../management/redux/flightOrderReducer';
import {
  fetchPointPayment as fetchHotelHoldingBookingPointPayment,
  setPointUsing as setHotelHoldingBookingPointUsing,
  setUsePointPayment as setHotelHoldingBookingUsePointPayment,
} from '../../../management/redux/hotelOrderReducer';
import {
  fetchPointPayment as fetchTourHoldingBookingPointPayment,
  setPointUsing as setTourHoldingBookingPointUsing,
  setUsePointPayment as setTourHoldingBookingUsePointPayment,
} from '../../../management/redux/tourOrderReducer';
import { PointPayment } from '../../models';
import { PointLabelComponent } from '../elements';
import LoadingIcon from '../LoadingIcon';
import { NewTabLink } from '../NewTabLink';

interface ITripiCreditPaymentMethodOwnProps {
  moduleType: ModuleType;
}

interface ITripiCreditPaymentMethodProps extends ITripiCreditPaymentMethodOwnProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

interface State {
  pointPayment: PointPayment;
  pointUsing: number;
  anchorEl?: Element | undefined;
}

class TripiCreditPaymentMethod extends React.PureComponent<ITripiCreditPaymentMethodProps, State> {
  state: State = {
    pointPayment: {
      ...initPointPayment,
      credit: this.props.userData ? this.props.userData.credit : 0,
    },
    pointUsing: 0,
  };

  async componentDidMount() {
    const { dispatch, moduleType } = this.props;
    dispatch(validateAccessToken());
    let json;
    if (moduleType === 'flight') {
      dispatch(setFlightUsePointPayment(false));
      json = await dispatch(fetchFlightPointPayment());
    } else if (moduleType === 'hotel') {
      dispatch(setHotelUsePointPayment(false));
      json = await dispatch(fetchHotelPointPayment());
    } else if (moduleType === 'tour') {
      dispatch(setTourUsePointPayment(false));
      json = await dispatch(fetchTourPointPayment());
    } else if (moduleType === 'mobile_card') {
      dispatch(setMobileUsePointPayment(false));
      json = await dispatch(fetchMobilePointPaymentCode());
    } else if (moduleType === 'mobile_topup') {
      dispatch(setMobileUsePointPayment(false));
      json = await dispatch(fetchMobilePointPaymentDirect());
    } else if (moduleType === 'flightPayLater') {
      dispatch(setHoldingBookingUsePointPayment(false));
      json = await dispatch(fetchHoldingBookingPointPayment());
    } else if (moduleType === 'hotelPayLater') {
      dispatch(setHotelHoldingBookingUsePointPayment(false));
      json = await dispatch(fetchHotelHoldingBookingPointPayment());
    } else if (moduleType === 'tourPayLater') {
      dispatch(setTourHoldingBookingUsePointPayment(false));
      json = await dispatch(fetchTourHoldingBookingPointPayment());
    } else if (moduleType === 'train') {
      dispatch(setTrainBookingUsePointPayment(false));
      json = await dispatch(fetchTrainBookingPointPayment());
    }
    if (json && json.code === 200) {
      const pointPayment: PointPayment = getPointPaymentData(json.data);
      this.setState({
        pointPayment,
        pointUsing: pointPayment.min,
      });
    }
  }

  onAfterChange(value: number) {
    const { dispatch, moduleType } = this.props;
    if (moduleType === 'flight') {
      dispatch(setFlightPointUsing(value));
    } else if (moduleType === 'hotel') {
      dispatch(setHotelPointUsing(value));
    } else if (moduleType === 'tour') {
      dispatch(setTourPointUsing(value));
    } else if (moduleType === 'mobile_card' || moduleType === 'mobile_topup') {
      dispatch(setMobilePointUsing(value));
    } else if (moduleType === 'flightPayLater') {
      dispatch(setHoldingBookingPointUsing(value));
    } else if (moduleType === 'hotelPayLater') {
      dispatch(setHotelHoldingBookingPointUsing(value));
    } else if (moduleType === 'tourPayLater') {
      dispatch(setTourHoldingBookingPointUsing(value));
    } else if (moduleType === 'train') {
      dispatch(setTrainBookingPointUsing(value));
    }
  }

  onCheck(value: boolean) {
    const { dispatch, moduleType } = this.props;

    if (moduleType === 'flight') {
      dispatch(setFlightUsePointPayment(value));
    } else if (moduleType === 'hotel') {
      dispatch(setHotelUsePointPayment(value));
    } else if (moduleType === 'tour') {
      dispatch(setTourUsePointPayment(value));
    } else if (moduleType === 'mobile_card' || moduleType === 'mobile_topup') {
      dispatch(setMobileUsePointPayment(value));
    } else if (moduleType === 'flightPayLater') {
      dispatch(setHoldingBookingUsePointPayment(value));
    } else if (moduleType === 'hotelPayLater') {
      dispatch(setHotelHoldingBookingUsePointPayment(value));
    } else if (moduleType === 'tourPayLater') {
      dispatch(setTourHoldingBookingUsePointPayment(value));
    }
  }

  render() {
    const { pointPayment, pointUsing, anchorEl } = this.state;
    const { pointPaymentData, usePointPayment, userData } = this.props;

    if (!userData) {
      return null;
    }
    if (!pointPaymentData) {
      return (
        <div
          style={{
            borderRadius: '4px',
            border: `1px solid ${GREY}`,
            minWidth: '480px',
            marginBottom: '24px',
            height: '128px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      );
    }

    return (
      <>
        <div
          style={{
            borderRadius: '4px',
            border: `1px solid ${GREY}`,
            width: '480px',
            marginBottom: '24px',
          }}
        >
          <div
            style={{
              borderBottom: `0.5px solid ${GREY}`,
              margin: '0 16px',
              padding: '3px 0',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <FormControlLabel
              style={{ outline: 'none', margin: '0' }}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="booking.payWithTripCredits" />
                </Typography>
              }
              labelPlacement="end"
              control={
                <Checkbox
                  style={{ padding: '3px' }}
                  size="medium"
                  checked={usePointPayment || false}
                  onChange={event => this.onCheck(event.target.checked)}
                  color="secondary"
                  disabled={pointPaymentData.max === 0}
                />
              }
            />
            <Button
              style={{
                padding: '3px 10px',
              }}
              onClick={event => this.setState({ anchorEl: event.currentTarget })}
            >
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2" style={{ color: BLUE, paddingRight: '8px' }}>
                  <FormattedMessage id="tour.booking.seeMore" />
                </Typography>
                <IconInfo />
              </div>
            </Button>

            <Popover
              open={Boolean(anchorEl)}
              anchorEl={anchorEl}
              onClose={() => this.setState({ anchorEl: undefined })}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  flexDirection: 'column',
                  alignItems: 'center',
                  textAlign: 'center',
                  maxWidth: '420px',
                  padding: '32px 0',
                }}
              >
                <IconLoyalty style={{ width: '140px', height: '116px' }} />
                <Typography variant="body2" style={{ padding: '16px 30px' }}>
                  <FormattedMessage id="booking.tripiPointDescription" />
                </Typography>

                <Button
                  variant="contained"
                  color="secondary"
                  size="large"
                  style={{ marginTop: '16px' }}
                  onClick={() => this.setState({ anchorEl: undefined })}
                >
                  <Typography variant="button" style={{ padding: '0 16px' }}>
                    <FormattedMessage id="close" />
                  </Typography>
                </Button>
              </div>
            </Popover>
          </div>
          <div
            style={{
              padding: '10px 16px 16px',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                minHeight: '32px',
                alignItems: 'center',
              }}
            >
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                  <FormattedMessage id="booking.accountBalance" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" color={usePointPayment ? 'secondary' : 'textSecondary'}>
                  <FormattedNumber value={Math.round(pointPayment.credit)} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>

              <NewTabLink href={`${ROUTES.management}?current=m.general&${ACTION_PARAM_NAME}=recharge`}>
                <Button variant="contained" color="secondary" style={{ width: '115px' }}>
                  <Typography variant="button">
                    <FormattedMessage id="account.recharge" />
                  </Typography>
                </Button>
              </NewTabLink>
            </div>

            <div style={{ display: 'flex', alignItems: 'center', minHeight: '32px' }}>
              <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                <FormattedMessage id="booking.usePoints" />
                :&nbsp;
                <FormattedNumber value={pointUsing} />
                &nbsp;
              </Typography>
              <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                <FormattedMessage id="booking.point" />
              </Typography>
            </div>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                minHeight: '32px',
              }}
            >
              <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                <FormattedMessage id="booking.remainningPoints" />
                :&nbsp;
                <FormattedNumber value={(pointPayment.availablePoint || 0) - pointUsing} />
                &nbsp;
              </Typography>
              <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                <FormattedMessage id="booking.point" />
              </Typography>
            </div>
            <Collapse in={usePointPayment}>
              {pointPayment.maxPointOnModuleTag && (
                <Typography variant="body2" style={{ minHeight: '32px' }} color="error">
                  {pointPayment.maxPointOnModuleTag}
                </Typography>
              )}
            </Collapse>
            <div style={{ display: 'flex', alignItems: 'center', margin: '8px 0' }}>
              <div style={{ width: '100%' }}>
                <Slider
                  key={JSON.stringify(pointUsing)}
                  defaultValue={pointUsing}
                  min={pointPayment.min}
                  max={pointPayment.max}
                  disabled={!usePointPayment}
                  onChangeCommitted={(e: any, value: any) => {
                    if (usePointPayment) {
                      this.setState({ pointUsing: value });
                      this.onAfterChange(value);
                    }
                  }}
                  ValueLabelComponent={PointLabelComponent}
                />
              </div>
              <div style={{ paddingLeft: '16px' }}>
                <Typography variant="body2" color={usePointPayment ? 'textPrimary' : 'textSecondary'}>
                  &nbsp;
                  <FormattedNumber value={pointUsing} />
                  /
                  <FormattedNumber value={pointPayment.max} />
                </Typography>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

function mapStateToProps(state: AppState, ownProps: ITripiCreditPaymentMethodOwnProps) {
  const { moduleType } = ownProps;
  const pointPaymentData =
    moduleType === 'baggages' || moduleType === 'itinerary_changing' || moduleType === 'flightPayLater'
      ? state.management.flightOrder.pointPaymentData
      : moduleType === 'tourPayLater'
      ? state.management.tourOrder.pointPaymentData
      : moduleType === 'hotelPayLater'
      ? state.management.hotelOrder.pointPaymentData
      : moduleType === 'flight'
      ? state.booking.flight.pointPaymentData
      : moduleType === 'hotel'
      ? state.booking.hotel.pointPaymentData
      : moduleType === 'mobile_card' || moduleType === 'mobile_topup'
      ? state.booking.mobile.pointPaymentData
      : moduleType === 'tour'
      ? state.booking.tour.pointPaymentData
      : moduleType === 'train'
      ? state.booking.train.pointPaymentData
      : undefined;

  const usePointPayment =
    moduleType === 'baggages' || moduleType === 'itinerary_changing' || moduleType === 'flightPayLater'
      ? state.management.flightOrder.usePointPayment
      : moduleType === 'tourPayLater'
      ? state.management.tourOrder.usePointPayment
      : moduleType === 'hotelPayLater'
      ? state.management.hotelOrder.usePointPayment
      : moduleType === 'flight'
      ? state.booking.flight.usePointPayment
      : moduleType === 'hotel'
      ? state.booking.hotel.usePointPayment
      : moduleType === 'mobile_card' || moduleType === 'mobile_topup'
      ? state.booking.mobile.usePointPayment
      : moduleType === 'tour'
      ? state.booking.tour.usePointPayment
      : moduleType === 'train'
      ? state.booking.train.usePointPayment
      : undefined;

  return {
    pointPaymentData,
    usePointPayment,
    userData: state.account.userData,
  };
}

export default connect(mapStateToProps)(TripiCreditPaymentMethod);
