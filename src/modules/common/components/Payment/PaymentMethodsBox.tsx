import { ButtonBase, Checkbox, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import IconCheck from '@material-ui/icons/CheckCircle';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { GREY, HOVER_GREY, ORANGE, PRIMARY } from '../../../../colors';
import { ModuleType, MY_TOUR, ROUTES, some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import {
  flightPay,
  setPayResponseMessage as flightSetPayResponseMessage,
} from '../../../booking/redux/flightBookingReducer';
import {
  hotelPay,
  setPayResponseMessage as hotelSetPayResponseMessage,
} from '../../../booking/redux/hotelBookingReducer';
import { mobileCardPay, mobileTopupPay, setPayMobileMessage } from '../../../booking/redux/mobileReducer';
import { setPayTourMessage, tourPay } from '../../../booking/redux/tourBookingReducer';
import { setTrainPayMessage, trainPay } from '../../../booking/redux/trainBookingReducer';
import { computePaymentFees, getPaymentMethodIcon } from '../../../booking/utils';
import {
  flightBaggagesPay,
  flightItineraryChangingPay,
  flightPayHolding,
  setPayResponseMessage as flightOrderSetPayResponseMessage,
} from '../../../management/redux/flightOrderReducer';
import {
  hotelPayLater,
  setPayResponseMessage as hotelSetPayLaterResponseMessage,
} from '../../../management/redux/hotelOrderReducer';
import { setTourPayLaterMessage, tourPayLatterPay } from '../../../management/redux/tourOrderReducer';
import { PAYMENT_ATM_CODE, PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../constants';
import ConfirmDialog from '../ConfirmDialog';
import Link from '../Link';
import LoadingButton from '../LoadingButton';
import MessageDialog from '../MessageDialog';
import NoOTPWarningBox from './NoOTPWarningBox';
import OTPDialog from './OTPDialog';
import TripiCreditPaymentMethod from './TripiCreditPaymentMethod';

const PRIMARY_COLOR = MY_TOUR ? PRIMARY : ORANGE;

const ItemDiv = styled.div<{ current: boolean }>`
  display: flex;
  align-items: center;
  cursor: pointer;
  border-bottom: 1px solid ${props => (props.current ? PRIMARY_COLOR : GREY)};
  min-height: 48px;
  flex-direction: column;
  :hover {
    background: ${props => (props.current ? '' : fade(PRIMARY_COLOR, 0.025))};
  }
`;

const BankBox = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 106px;
  height: 60px;
  border-radius: 2px;
  margin: 4px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 5px, rgba(0, 0, 0, 0.12) 0px 3px 4px, rgba(0, 0, 0, 0.14) 0px 2px 4px;
  :hover {
    background-color: ${HOVER_GREY};
  }
`;

interface IPaymentMethodsBoxProps extends ReturnType<typeof mapStateToProps> {
  paymentMethods: ReadonlyArray<some>;
  selectedMethod: some;
  total: number;
  setSelectedMethod(method: some): void;
  priceAfter: number;
  moduleType: ModuleType;
  promotionCode?: string;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const PaymentMethodsBox: React.FunctionComponent<IPaymentMethodsBoxProps> = props => {
  const {
    paymentMethods,
    total,
    setSelectedMethod,
    selectedMethod,
    priceAfter,
    moduleType,
    promotionCode,
    dispatch,
    booking,
    flightOrder,
    hotelOrder,
    tourOrder,
  } = props;
  const [agreed, setAgreed] = React.useState(false);
  const [showConfirmDialog, setShowConfirmDialog] = React.useState(false);
  const [showHoldingConfirm, setShowHoldingConfirm] = React.useState(false);
  const [showOTPDialog, setShowOTPDialog] = React.useState(false);

  const paymentFee = computePaymentFees(selectedMethod, total);
  const totalPayable = priceAfter + paymentFee;

  const { pay, message, clearMessage, paying } = React.useMemo(() => {
    if (moduleType === 'flight') {
      return {
        pay: (pass?: string) => {
          dispatch(flightPay(pass));
        },
        clearMessage: () => {
          dispatch(flightSetPayResponseMessage());
        },
        message: booking.flight.payResponseMessage,
        paying: booking.flight.paying,
      };
    }
    if (moduleType === 'flightPayLater') {
      return {
        pay: (pass?: string) => {
          dispatch(flightPayHolding(pass));
        },
        clearMessage: () => {
          dispatch(flightOrderSetPayResponseMessage());
        },
        message: flightOrder.payResponseMessage,
        paying: flightOrder.paying,
      };
    }
    if (moduleType === 'hotel') {
      return {
        pay: (pass?: string) => {
          dispatch(hotelPay(pass));
        },
        clearMessage: () => {
          dispatch(hotelSetPayResponseMessage());
        },
        message: booking.hotel.payResponseMessage,
        paying: booking.hotel.paying,
      };
    }
    if (moduleType === 'hotelPayLater') {
      return {
        pay: (pass?: string) => {
          dispatch(hotelPayLater(pass));
        },
        clearMessage: () => {
          dispatch(hotelSetPayLaterResponseMessage());
        },
        message: hotelOrder.payResponseMessage,
        paying: hotelOrder.paying,
      };
    }
    if (moduleType === 'tour') {
      return {
        pay: (pass?: string) => {
          dispatch(tourPay(pass));
        },
        clearMessage: () => {
          dispatch(setPayTourMessage());
        },
        message: booking.tour.payMessage,
        paying: booking.tour.paying,
      };
    }
    if (moduleType === 'tourPayLater') {
      return {
        pay: (pass?: string) => {
          dispatch(tourPayLatterPay(pass));
        },
        clearMessage: () => {
          dispatch(setTourPayLaterMessage());
        },
        message: tourOrder.payMessage,
        paying: tourOrder.paying,
      };
    }
    if (moduleType === 'mobile_card') {
      return {
        pay: (pass?: string) => {
          dispatch(mobileCardPay(pass));
        },
        clearMessage: () => {
          dispatch(setPayMobileMessage());
        },
        message: booking.mobile.payMessage,
        paying: booking.mobile.paying,
      };
    }
    if (moduleType === 'mobile_topup') {
      return {
        pay: (pass?: string) => {
          dispatch(mobileTopupPay(pass));
        },

        clearMessage: () => {
          dispatch(setPayMobileMessage());
        },
        message: booking.flight.payResponseMessage,
        paying: booking.flight.paying,
      };
    }
    if (moduleType === 'train') {
      return {
        pay: (pass?: string) => {
          dispatch(trainPay(pass));
        },
        clearMessage: () => {
          dispatch(setTrainPayMessage());
        },
        message: booking.train.payMessage,
        paying: booking.train.paying,
      };
    }
    if (moduleType === 'baggages') {
      return {
        pay: (pass?: string) => {
          dispatch(flightBaggagesPay(pass));
        },
        clearMessage: () => {
          dispatch(flightOrderSetPayResponseMessage());
        },
        message: booking.flight.payResponseMessage,
        paying: booking.flight.paying,
      };
    }
    if (moduleType === 'itinerary_changing') {
      return {
        pay: (pass?: string) => {
          dispatch(flightItineraryChangingPay(pass));
        },
        clearMessage: () => {
          dispatch(flightOrderSetPayResponseMessage());
        },
        message: booking.flight.payResponseMessage,
        paying: booking.flight.paying,
      };
    }
    return { pay: () => {}, message: '', clearMessage: () => {}, paying: false };
  }, [
    booking.flight.payResponseMessage,
    booking.flight.paying,
    booking.hotel.payResponseMessage,
    booking.hotel.paying,
    booking.mobile.payMessage,
    booking.mobile.paying,
    booking.tour.payMessage,
    booking.tour.paying,
    booking.train.payMessage,
    booking.train.paying,
    dispatch,
    flightOrder.payResponseMessage,
    flightOrder.paying,
    hotelOrder.payResponseMessage,
    hotelOrder.paying,
    moduleType,
    tourOrder.payMessage,
    tourOrder.paying,
  ]);

  return (
    <div style={{ paddingLeft: '6px', paddingRight: '10px' }}>
      {paymentMethods.find(v => v.code === PAYMENT_TRIPI_CREDIT_CODE) && <NoOTPWarningBox />}
      <RadioGroup radioGroup="method">
        {paymentMethods.map(method => {
          const current = method.code === selectedMethod.code;
          const fee = computePaymentFees(method, total);

          return (
            <ItemDiv current={current} key={method.id}>
              <ButtonBase
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  width: '100%',
                  height: 48,
                  background:
                    current && method.code !== PAYMENT_TRIPI_CREDIT_CODE && method.code !== PAYMENT_ATM_CODE
                      ? fade(PRIMARY_COLOR, 0.05)
                      : undefined,
                }}
                onClick={() => setSelectedMethod(method)}
              >
                <img
                  src={getPaymentMethodIcon(method.code)}
                  style={{
                    width: '24px',
                    height: '24px',
                    marginRight: '8px',
                  }}
                  alt=""
                />
                <div
                  style={{
                    whiteSpace: 'nowrap',
                    display: 'flex',
                    alignItems: 'baseline',
                    flex: 1,
                  }}
                >
                  <Typography>
                    {method.name}
                    &nbsp;
                  </Typography>
                  <Typography variant="body2" style={{ color: ORANGE }}>
                    {fee < 0 ? (
                      <FormattedMessage
                        id="booking.paymentFixedDiscount"
                        values={{
                          value: <FormattedNumber value={-fee} maximumFractionDigits={0} />,
                        }}
                      />
                    ) : fee > 0 ? (
                      <FormattedMessage
                        id="booking.paymentFixedFee"
                        values={{
                          value: <FormattedNumber value={fee} maximumFractionDigits={0} />,
                        }}
                      />
                    ) : (
                      ''
                    )}
                  </Typography>
                </div>
                <Radio value={method.code} checked={current} />
              </ButtonBase>
              {selectedMethod.code === PAYMENT_TRIPI_CREDIT_CODE && method.code === PAYMENT_TRIPI_CREDIT_CODE && (
                <TripiCreditPaymentMethod key={`code-${promotionCode}`} moduleType={moduleType} />
              )}
              {selectedMethod.code === PAYMENT_ATM_CODE && method.code === PAYMENT_ATM_CODE && (
                <div
                  style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    border: `0.5px solid ${GREY}`,
                    borderRadius: '2px',
                    padding: '12px',
                    margin: '32px 44px',
                    width: '482px',
                  }}
                >
                  {method.bankList.map((bank: some) => (
                    <BankBox
                      key={bank.code}
                      style={{
                        boxShadow:
                          selectedMethod.bankId === bank.id
                            ? `${ORANGE} 0px 1px 5px, ${ORANGE} 0px 3px 4px, ${ORANGE} 0px 2px 4px`
                            : undefined,
                      }}
                      onClick={() => setSelectedMethod({ ...selectedMethod, bankId: bank.id })}
                    >
                      <img style={{ maxWidth: '106px' }} src={bank.logoUrl} alt={bank.name} />
                      {selectedMethod.bankId === bank.id && (
                        <IconCheck
                          style={{
                            position: 'absolute',
                            transition: 'opacity 300ms',
                            right: '4px',
                            top: '4px',
                            color: ORANGE,
                            width: '14px',
                            height: '14px',
                          }}
                        />
                      )}
                    </BankBox>
                  ))}
                </div>
              )}
            </ItemDiv>
          );
        })}
      </RadioGroup>
      <FormControlLabel
        style={{ marginBottom: '16px', marginTop: '16px' }}
        control={<Checkbox checked={agreed} />}
        onChange={(e, value) => setAgreed(value)}
        label={
          <Typography variant="body2" style={{ marginTop: '4px' }}>
            <FormattedMessage
              id="booking.agreeWithTerms"
              values={{
                link: (
                  <Link to={ROUTES.terms}>
                    <FormattedMessage id={MY_TOUR ? 'booking.conditionsOfMyTour' : 'booking.conditionsOfTripi'} />
                  </Link>
                ),
              }}
            />
          </Typography>
        }
      />
      <div style={{ textAlign: 'center' }}>
        <LoadingButton
          color="secondary"
          variant="contained"
          disabled={
            paying || !agreed || (selectedMethod.code === PAYMENT_ATM_CODE && selectedMethod.bankId === undefined)
          }
          size="large"
          style={{ width: '170px' }}
          onClick={() =>
            promotionCode && selectedMethod && selectedMethod.code === PAYMENT_HOLDING_CODE
              ? setShowHoldingConfirm(true)
              : setShowConfirmDialog(true)
          }
          loading={paying}
        >
          <FormattedMessage id="booking.pay" />
        </LoadingButton>
      </div>
      <MessageDialog
        show={!!message}
        onClose={clearMessage}
        buttonMessageId="close"
        message={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              padding: '0 32px',
              flexDirection: 'column',
            }}
          >
            <IconWarning />
            <Typography variant="body1" style={{ paddingTop: '18px' }}>
              {message}
            </Typography>
          </div>
        }
      />
      <ConfirmDialog
        header={
          <Typography variant="h5">
            <FormattedMessage id="attention" />
          </Typography>
        }
        message={
          <div style={{ textAlign: 'center', padding: '16px 32px 0' }}>
            <Typography variant="body1">
              <FormattedMessage id="booking.holdingNotice" />
            </Typography>
          </div>
        }
        show={showHoldingConfirm}
        cancelMessageId="cancel"
        confirmMessageId="continue"
        onCancel={() => setShowHoldingConfirm(false)}
        onAccept={() => {
          setShowHoldingConfirm(false);
          setShowConfirmDialog(true);
        }}
      />
      <ConfirmDialog
        header={
          <Typography variant="h5">
            <FormattedMessage id="attention" />
          </Typography>
        }
        message={
          <div style={{ textAlign: 'center', padding: '16px 32px 0' }}>
            <Typography variant="body1">
              <FormattedMessage
                id={`booking.paymentMessage.${selectedMethod.code.toLowerCase()}`}
                values={{
                  currency: (
                    <>
                      <span style={{ fontWeight: 500, color: ORANGE, whiteSpace: 'nowrap' }}>
                        <FormattedNumber value={totalPayable < 0 ? 0 : totalPayable} maximumFractionDigits={0} />
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </span>
                      <br />
                      <FormattedMessage id="booking.confirmToContinue" />
                    </>
                  ),
                }}
              />
            </Typography>
          </div>
        }
        show={showConfirmDialog}
        cancelMessageId="cancel"
        confirmMessageId="booking.accept"
        onCancel={() => setShowConfirmDialog(false)}
        onAccept={() => {
          setShowConfirmDialog(false);
          if (selectedMethod.code === PAYMENT_TRIPI_CREDIT_CODE) {
            setShowOTPDialog(true);
          } else {
            pay();
          }
        }}
      />
      <OTPDialog show={showOTPDialog} pay={pay} close={() => setShowOTPDialog(false)} />
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    booking: state.booking,
    flightOrder: state.management.flightOrder,
    hotelOrder: state.management.hotelOrder,
    tourOrder: state.management.tourOrder,
  };
}

export default connect(mapStateToProps)(PaymentMethodsBox);
