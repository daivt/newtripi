/* eslint-disable react/sort-comp */
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Typography from '@material-ui/core/Typography';
import RadioButtonIcon from '@material-ui/icons/RadioButtonUnchecked';
import IconSearch from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import moment from 'moment';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedMessage, FormattedNumber, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY, PRIMARY, RED } from '../../../../colors';
import { ModuleType, ROUTES, some, MY_TOUR } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconGiftBox } from '../../../../svg/ic_flight_payment_giftbox.svg';
import { fetchRewardsHistory as fetchFlightRewardsHistory } from '../../../booking/redux/flightBookingReducer';
import { fetchRewardsHistory as fetchHotelRewardsHistory } from '../../../booking/redux/hotelBookingReducer';
import {
  fetchRewardsHistoryCode as fetchMobileRewardsHistoryCode,
  fetchRewardsHistoryDirect as fetchMobileRewardsHistoryDirect,
} from '../../../booking/redux/mobileReducer';
import { fetchRewardsHistory as fetchTourRewardsHistory } from '../../../booking/redux/tourBookingReducer';
import { BootstrapInput, Col } from '../elements';
import LoadingIcon from '../LoadingIcon';
import { NewTabLink } from '../NewTabLink';
import { fetchRewardsHistoryTourOrder } from '../../../management/redux/tourOrderReducer';
import { fetchRewardsHistoryHotelOrder } from '../../../management/redux/hotelOrderReducer';

const EllipseLeft = styled.div<{ top: number }>`
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px inset, rgba(0, 0, 0, 0.12) 0px 2px 2px inset,
    rgba(0, 0, 0, 0.14) 0px 0px 2px inset;
  position: absolute;
  width: 12px;
  height: 8px;
  top: ${props => props.top}%;
  left: -1.17%;
  background: rgb(255, 255, 255);
  border-radius: 50%;
`;

const EllipseRight = styled.div<{ top: number }>`
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px inset, rgba(0, 0, 0, 0.12) 0px 2px 2px inset,
    rgba(0, 0, 0, 0.14) 0px 0px 2px inset;
  position: absolute;
  width: 12px;
  height: 8px;
  top: ${props => props.top}%;
  right: -1.17%;
  background: rgb(255, 255, 255);
  border-radius: 50%;
`;

export interface IPromotionListBoxProps extends WrappedComponentProps {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  close(): void;
  selectCode(promotionCode: string): void;
  moduleType: ModuleType;
}

interface IPromotionListBoxState {
  promotions?: some[];
  resultTotal: number;
  promotionCode: string;
  search: string;
}

function drawEllipse(top: number) {
  return (
    <>
      <EllipseLeft top={top} />
      <EllipseRight top={top} />
    </>
  );
}

const MAX_PAGE = 10;
class PromotionListBox extends React.PureComponent<IPromotionListBoxProps, IPromotionListBoxState> {
  state: IPromotionListBoxState = {
    resultTotal: 0,
    promotionCode: '',
    search: '',
  };

  scrollingDiv = React.createRef<HTMLDivElement>();

  async componentDidMount() {
    this.coreSearch();
  }

  coreSearch = async (page?: number) => {
    const { search, promotions } = this.state;
    const { moduleType } = this.props;
    if (page && page > MAX_PAGE) {
      return;
    }
    if (!page) {
      this.setState({ promotions: undefined });
    }

    try {
      let json;
      if (moduleType === 'flight' || moduleType === 'flightPayLater') {
        json = await this.props.dispatch(fetchFlightRewardsHistory(search, page));
      } else if (moduleType === 'hotel') {
        json = await this.props.dispatch(fetchHotelRewardsHistory(search, page));
      } else if (moduleType === 'tour') {
        json = await this.props.dispatch(fetchTourRewardsHistory(search, page));
      } else if (moduleType === 'mobile_card') {
        json = await this.props.dispatch(fetchMobileRewardsHistoryCode(search, page));
      } else if (moduleType === 'mobile_topup') {
        json = await this.props.dispatch(fetchMobileRewardsHistoryDirect(search, page));
      } else if (moduleType === 'tourPayLater') {
        json = await this.props.dispatch(fetchRewardsHistoryTourOrder(search, page));
      } else if (moduleType === 'hotelPayLater') {
        json = await this.props.dispatch(fetchRewardsHistoryHotelOrder(search, page));
      }

      if (json && json.code === 200) {
        this.setState({
          promotions: page ? (promotions || []).concat(json.data.list) : json.data.list,
          resultTotal: json.data.total,
        });
      }
    } catch (_) {}
  };

  search = debounce(this.coreSearch, 500, { trailing: true });

  public render() {
    const { intl, close, selectCode } = this.props;
    const { promotions, promotionCode, resultTotal } = this.state;

    return (
      <>
        <div
          ref={this.scrollingDiv}
          style={{
            height: 'calc(100vh - 300px)',
            maxHeight: '500px',
            overflowY: 'auto',
            overflowX: 'hidden',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              minWidth: '560px',
            }}
          >
            <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
              <div style={{ display: 'flex', flexDirection: 'column', margin: '16px 24px 8px 16px' }}>
                <Typography variant="h5" style={{ fontWeight: 'normal' }}>
                  <FormattedMessage id="booking.promotion.myPromotion" />
                </Typography>

                <Typography variant="body2" style={{ color: DARK_GREY }}>
                  <FormattedMessage id="booking.promotion.promotionDescription" />
                </Typography>

                <Typography variant="body2" style={{ padding: '12px 12px 6px' }}>
                  <FormattedMessage id="booking.promotion.whatYourPromotionCode" />
                </Typography>

                <BootstrapInput
                  onChange={e => this.setState({ search: e.target.value }, this.search)}
                  margin="dense"
                  style={{
                    height: '40px',
                  }}
                  value={this.state.search}
                  fullWidth
                  placeholder={intl.formatMessage({ id: 'booking.discountCodeExample' })}
                  endAdornment={
                    <InputAdornment position="end" variant="filled">
                      <IconSearch style={{ color: GREY }} />
                    </InputAdornment>
                  }
                />
                {promotions && !!promotions.length && (
                  <Typography variant="subtitle1" style={{ paddingTop: '20px', paddingLeft: '12px' }}>
                    <FormattedMessage id="booking.promotion.selectPromotionCode" />
                  </Typography>
                )}
              </div>

              <RadioGroup
                value={promotionCode}
                radioGroup="promotion"
                onChange={(e, value) => this.setState({ promotionCode: value })}
              >
                {!promotions ? (
                  <LoadingIcon />
                ) : !promotions.length ? (
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Typography variant="body2" style={{ paddingTop: '20px', paddingLeft: '12px' }}>
                      <FormattedMessage id="booking.promotion.noResult" />
                    </Typography>
                  </div>
                ) : (
                  <InfiniteScroll
                    pageStart={1}
                    initialLoad={false}
                    loadMore={page => this.coreSearch(page)}
                    hasMore={promotions.length < resultTotal}
                    getScrollParent={() => this.scrollingDiv.current}
                    loader={
                      <LoadingIcon
                        key="loader"
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      />
                    }
                    useWindow={false}
                  >
                    {promotions.map((promotion, index) => (
                      <Col
                        key={promotion.id}
                        style={{
                          overflow: 'hidden',
                          marginLeft: '16px',
                          marginRight: '24px',
                          paddingBottom: '6px',
                          paddingTop: '2px',
                          cursor: 'pointer',
                        }}
                        onClick={() => this.setState({ promotionCode: promotion.codeDetail })}
                      >
                        <div
                          style={{
                            display: 'flex',
                            borderRadius: '4px 0 0 4px',
                            backgroundColor: '#BDBDBD',
                            boxShadow: `0px 1px 3px rgba(0, 0, 0, 0.2),
                              0px 2px 2px rgba(0, 0, 0, 0.12), 0px 0px 2px rgba(0, 0, 0, 0.14)`,
                            minHeight: '90px',
                            marginLeft: '1px',
                            position: 'relative',
                          }}
                        >
                          <div
                            style={{
                              width: '100%',
                              background: '#fff',
                              borderRadius: '4px 0px 0px 4px',
                            }}
                          >
                            <div
                              style={{
                                display: 'flex',
                                padding: '8px 16px',
                              }}
                            >
                              <div style={{ paddingTop: '12px' }}>
                                <img style={{ width: '24px' }} src={promotion.iconType} alt="" />
                              </div>
                              <div
                                style={{
                                  display: 'flex',
                                  paddingLeft: '16px',
                                  flexDirection: 'column',
                                  flex: 1,
                                }}
                              >
                                <div
                                  style={{
                                    minHeight: '48px',
                                  }}
                                >
                                  <Typography
                                    variant="body2"
                                    style={{
                                      fontWeight: 500,
                                    }}
                                  >
                                    {promotion.rewardProgram.title}
                                  </Typography>
                                </div>

                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                  <div
                                    style={{
                                      minWidth: '60px',
                                      backgroundColor: PRIMARY,
                                      borderRadius: '99px',
                                      height: '16px',
                                      display: 'flex',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Typography
                                      variant="body2"
                                      style={{ fontSize: 10, color: '#fff', padding: '0 10px' }}
                                    >
                                      {promotion.typeDetail}
                                    </Typography>
                                  </div>
                                  <div style={{ paddingLeft: '6px' }}>
                                    <Typography
                                      variant="body2"
                                      style={{ fontSize: '12px', textTransform: 'uppercase' }}
                                    >
                                      {promotion.codeDetail}
                                    </Typography>
                                  </div>
                                </div>

                                <div
                                  style={{
                                    display: 'flex',
                                  }}
                                >
                                  <div
                                    style={{
                                      display: 'flex',
                                      flexDirection: 'column',
                                      paddingRight: '8px',
                                      minWidth: '80px',
                                    }}
                                  >
                                    <Typography variant="caption" style={{ color: GREY }}>
                                      <FormattedMessage id="booking.promotion.expired" />
                                    </Typography>
                                    <Typography
                                      variant="body2"
                                      style={{ fontSize: '12px', lineHeight: '16px', color: RED }}
                                    >
                                      {moment(promotion.rewardProgram.toDate * 1000).format('L')}
                                    </Typography>
                                  </div>
                                  <div style={{ borderRight: `1px solid ${GREY}` }} />
                                  <div
                                    style={{
                                      display: 'flex',
                                      flexDirection: 'column',
                                      paddingLeft: '8px',
                                    }}
                                  >
                                    <Typography variant="caption" style={{ color: GREY }}>
                                      <FormattedMessage id="booking.promotion.remainNumber" />
                                    </Typography>
                                    <Typography variant="body2" style={{ fontSize: '12px', lineHeight: '16px' }}>
                                      <FormattedNumber value={promotion.remainNum} />
                                    </Typography>
                                  </div>
                                </div>
                              </div>

                              <div style={{ display: 'flex', alignItems: 'center' }}>
                                <Radio
                                  color="primary"
                                  value={promotion.codeDetail}
                                  icon={<RadioButtonIcon style={{ color: GREY }} />}
                                />
                              </div>
                            </div>
                          </div>
                          {drawEllipse(6)}
                          {drawEllipse(22)}
                          {drawEllipse(37)}
                          {drawEllipse(53)}
                          {drawEllipse(68)}
                          {drawEllipse(84)}
                        </div>
                      </Col>
                    ))}
                  </InfiniteScroll>
                )}
              </RadioGroup>
            </div>
          </div>
        </div>
        {!MY_TOUR && (
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <NewTabLink href={ROUTES.reward.myRewards}>
              <Button
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: '12px',
                }}
              >
                <IconGiftBox />
                <Typography variant="body2" style={{ paddingLeft: '3px', color: BLUE }}>
                  <FormattedMessage id="booking.seeGiftList" />
                </Typography>
              </Button>
            </NewTabLink>
          </div>
        )}

        <div
          style={{
            boxShadow: `0px 1px 15px rgba(0, 0, 0, 0.2),
              0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)`,
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              padding: '12px 0',
            }}
          >
            <Button
              variant="contained"
              color="secondary"
              style={{
                width: '170px',
              }}
              size="large"
              onClick={() => selectCode(promotionCode)}
              disabled={!promotionCode}
            >
              <FormattedMessage id="booking.promotion.apply" />
            </Button>
            <div style={{ paddingLeft: '30px' }}>
              <Button
                variant="outlined"
                color={MY_TOUR ? 'primary' : 'secondary'}
                style={{
                  width: '170px',
                }}
                size="large"
                onClick={close}
              >
                <FormattedMessage id="booking.promotion.return" />
              </Button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapState2Props = (state: AppState) => {
  return {};
};

export default connect(mapState2Props)(injectIntl(PromotionListBox));
