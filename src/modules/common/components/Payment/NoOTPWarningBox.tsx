import * as React from 'react';
import { AppState } from '../../../../redux/reducers';
import { connect } from 'react-redux';
import { Typography, Button } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { NewTabLink } from '../NewTabLink';
import { ROUTES } from '../../../../constants';
import { ACTION_PARAM_NAME, CURRENT_PARAM_NAME } from '../../../management/constants';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';

interface INoOTPWarningBox extends ReturnType<typeof mapStateToProps> {}

const NoOTPWarningBox: React.FunctionComponent<INoOTPWarningBox> = props => {
  const { userData } = props;

  return (
    <div style={{ display: userData && userData.useCreditPassword ? 'none' : undefined }}>
      <div style={{ display: 'flex' }}>
        <IconWarning style={{ height: '18px', width: '18px', marginRight: '10px' }} />
        <Typography variant="body2" color="error">
          <FormattedMessage id="booking.youNeedCreateSmartOTP" />
        </Typography>
      </div>

      <div
        style={{
          display: 'flex',
          marginTop: '8px',
          marginBottom: '20px',
          alignItems: 'center',
        }}
      >
        <Typography
          variant="caption"
          color="textSecondary"
          style={{ marginRight: '12px', maxWidth: '500px' }}
        >
          <FormattedMessage id="booking.smartOTPDescription" />
        </Typography>
        <NewTabLink
          href={`${ROUTES.management}?${CURRENT_PARAM_NAME}=m.profile&${ACTION_PARAM_NAME}=smartOTP`}
        >
          <Button
            variant="contained"
            color="secondary"
            style={{ width: '140px', margin: '12px 0' }}
          >
            <FormattedMessage id="booking.createSmartOTP" />
          </Button>
        </NewTabLink>
      </div>
    </div>
  );
};

function mapStateToProps(state: AppState) {
  return {
    userData: state.account.userData,
  };
}

export default connect(mapStateToProps)(NoOTPWarningBox);
