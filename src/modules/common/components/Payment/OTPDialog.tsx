import { Button, Dialog, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../API';
import { ROUTES } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import { validateAccessToken } from '../../../auth/redux/authThunks';
import { FreeTextField } from '../../../booking/components/Form';
import { ACTION_PARAM_NAME, CURRENT_PARAM_NAME } from '../../../management/constants';
import { fetchThunk } from '../../redux/thunks';
import { validOTPRegex } from '../../utils';
import { Line } from '../elements';
import LoadingButton from '../LoadingButton/index';
import { NewTabLink } from '../NewTabLink';

interface IOTPDialogProps extends ReturnType<typeof mapStateToProps> {
  show: boolean;
  pay(pass?: string): void;
  close(): void;
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const OTPDialog: React.FunctionComponent<IOTPDialogProps> = props => {
  const { show, pay, close, dispatch, userData } = props;
  const [pass, setPass] = React.useState('');
  const [message, setMessage] = React.useState('');
  const [verifying, setVerifying] = React.useState(false);

  const validateCreditPassword = React.useCallback(async () => {
    const { dispatch } = props;
    setVerifying(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.validateCreditPassword,
        'post',
        true,
        JSON.stringify({
          password: pass,
        }),
      ),
    );
    setVerifying(false);
    if (json.code === 200) {
      pay(pass);
      close();
    } else {
      setMessage(json.message);
    }
  }, [close, pass, pay, props]);

  if (!userData) {
    return <></>;
  }

  return (
    <Dialog
      open={show}
      maxWidth="md"
      onExited={e => {
        setPass('');
      }}
      onEnter={() => dispatch(validateAccessToken())}
    >
      {userData.useCreditPassword ? (
        <form
          autoComplete="off"
          onSubmit={e => {
            e.preventDefault();
            validateCreditPassword();
          }}
        >
          <div style={{ padding: '24px', width: '444px' }}>
            <Typography variant="h6">
              <FormattedMessage id="payment.enterSmartOTP" />
            </Typography>
            <Typography variant="body1" color="textSecondary" style={{ marginTop: '16px' }}>
              <FormattedMessage id="payment.OTPDescription" />
            </Typography>
            <FreeTextField
              text={pass}
              placeholder=""
              update={value => {
                setPass(value);
                if (message) {
                  setMessage('');
                }
              }}
              regex={validOTPRegex}
              type="password"
              style={{ marginTop: '24px', marginRight: '0px' }}
              autoFocus
            />

            {!!message && (
              <Typography variant="body2" color="error">
                {message}
              </Typography>
            )}

            <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '12px' }}>
              <Button>
                <NewTabLink
                  href={`${ROUTES.management}?${CURRENT_PARAM_NAME}=m.profile&${ACTION_PARAM_NAME}=smartOTP`}
                >
                  <Typography variant="body2">
                    <FormattedMessage id="payment.changeCode" />
                  </Typography>
                </NewTabLink>
              </Button>
            </div>
            <Line style={{ marginTop: '20px', justifyContent: 'center' }}>
              <LoadingButton
                loading={verifying}
                variant="contained"
                color="secondary"
                disabled={!pass}
                type="submit"
              >
                <Typography variant="button">
                  <FormattedMessage id="pay" />
                </Typography>
              </LoadingButton>

              <Button variant="outlined" onClick={close} style={{ marginLeft: '16px' }}>
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="ignore" />
                </Typography>
              </Button>
            </Line>
          </div>
        </form>
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '32px',
            flexDirection: 'column',
          }}
        >
          <IconWarning />
          <Typography variant="body1" style={{ paddingTop: '18px' }}>
            <FormattedMessage id="booking.youNeedCreateSmartOTP" />
          </Typography>

          <Button variant="outlined" onClick={close} style={{ marginTop: '32px' }}>
            <Typography variant="button" color="textSecondary">
              <FormattedMessage id="close" />
            </Typography>
          </Button>
        </div>
      )}
    </Dialog>
  );
};

function mapStateToProps(state: AppState) {
  return {
    userData: state.account.userData,
  };
}

export default connect(mapStateToProps)(OTPDialog);
