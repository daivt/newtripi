import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE, PRIMARY, RED } from '../../../../colors';
import { ModuleType, ROUTES, MY_TOUR } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconGiftBox } from '../../../../svg/ic_flight_payment_giftbox.svg';
import {
  checkPromotion as checkFlightPromotion,
  setPromotion as setFlightPromotion,
  setPromotionCode as setFlightPromotionCode,
} from '../../../booking/redux/flightBookingReducer';
import {
  checkPromotion as checkHotelPromotion,
  setPromotion as setHotelPromotion,
  setPromotionCode as setHotelPromotionCode,
} from '../../../booking/redux/hotelBookingReducer';
import {
  checkPromotionCode as checkMobilePromotionCode,
  checkPromotionDirect as checkMobilePromotionDirect,
  fetchPaymentMethodsCode,
  fetchPaymentMethodsDirect,
  setPromotion as setMobilePromotion,
  setPromotionCode as setMobilePromotionCode,
} from '../../../booking/redux/mobileReducer';
import {
  checkPromotion as checkTourPromotion,
  setPromotion as setTourPromotion,
  setPromotionCode as setTourPromotionCode,
} from '../../../booking/redux/tourBookingReducer';
import {
  checkPromotion as checkHoldingBookingPromotion,
  setPromotion as setHoldingBookingPromotion,
  setPromotionCode as setHoldingBookingPromotionCode,
} from '../../../management/redux/flightOrderReducer';
import {
  checkPromotion as checkHotelHoldingBookingPromotion,
  setPromotion as setHotelHoldingBookingPromotion,
  setPromotionCode as setHotelHoldingBookingPromotionCode,
} from '../../../management/redux/hotelOrderReducer';
import {
  checkPromotion as checkTourHoldingBookingPromotion,
  setPromotion as setTourHoldingBookingPromotion,
  setPromotionCode as setTourHoldingBookingPromotionCode,
} from '../../../management/redux/tourOrderReducer';
import { BootstrapInput } from '../elements';
import { NewTabLink } from '../NewTabLink';
import PromotionListBox from './PromotionListBox';

export interface IDiscountCodeBoxProps extends WrappedComponentProps, ReturnType<typeof mapState2Props> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  moduleType: ModuleType;
}

interface IDiscountCodeBoxState {
  showPromotions: boolean;
}

class DiscountCodeBox extends React.PureComponent<IDiscountCodeBoxProps, IDiscountCodeBoxState> {
  state: IDiscountCodeBoxState = {
    showPromotions: false,
  };

  componentDidMount() {
    this.removePromotionCode();
  }

  checkPromotion(promotionCode: string) {
    const { dispatch, moduleType } = this.props;
    this.setState({ showPromotions: false });
    if (moduleType === 'flight') {
      dispatch(checkFlightPromotion(promotionCode));
    } else if (moduleType === 'hotel') {
      dispatch(checkHotelPromotion(promotionCode));
    } else if (moduleType === 'tour') {
      dispatch(checkTourPromotion(promotionCode));
    } else if (moduleType === 'mobile_card') {
      dispatch(checkMobilePromotionCode(promotionCode));
    } else if (moduleType === 'mobile_topup') {
      dispatch(checkMobilePromotionDirect(promotionCode));
    } else if (moduleType === 'flightPayLater') {
      dispatch(checkHoldingBookingPromotion(promotionCode));
    } else if (moduleType === 'hotelPayLater') {
      dispatch(checkHotelHoldingBookingPromotion(promotionCode));
    } else if (moduleType === 'tourPayLater') {
      dispatch(checkTourHoldingBookingPromotion(promotionCode));
    }
  }

  removePromotionCode() {
    const { dispatch, moduleType } = this.props;
    if (moduleType === 'flight') {
      dispatch(setFlightPromotionCode(''));
      dispatch(setFlightPromotion());
    } else if (moduleType === 'hotel') {
      dispatch(setHotelPromotionCode(''));
      dispatch(setHotelPromotion());
    } else if (moduleType === 'tour') {
      dispatch(setTourPromotionCode(''));
      dispatch(setTourPromotion());
    } else if (moduleType === 'mobile_card') {
      dispatch(setMobilePromotion());
      dispatch(setMobilePromotionCode(''));
      dispatch(fetchPaymentMethodsCode());
    } else if (moduleType === 'mobile_topup') {
      dispatch(setMobilePromotion());
      dispatch(setMobilePromotionCode(''));
      dispatch(fetchPaymentMethodsDirect());
    } else if (moduleType === 'flightPayLater') {
      dispatch(setHoldingBookingPromotion());
      dispatch(setHoldingBookingPromotionCode(''));
    } else if (moduleType === 'hotelPayLater') {
      dispatch(setHotelHoldingBookingPromotion());
      dispatch(setHotelHoldingBookingPromotionCode(''));
    } else if (moduleType === 'tourPayLater') {
      dispatch(setTourHoldingBookingPromotion());
      dispatch(setTourHoldingBookingPromotionCode(''));
    }
  }

  public render() {
    const { booking, flightOrder, hotelOrder, tourOrder, intl, moduleType } = this.props;
    const { showPromotions } = this.state;
    let { promotion } = booking.flight;
    let { promotionCode } = booking.flight;

    if (moduleType === 'hotel') {
      promotion = booking.hotel.promotion;
      promotionCode = booking.hotel.promotionCode;
    } else if (moduleType === 'tour') {
      promotion = booking.tour.promotion;
      promotionCode = booking.tour.promotionCode;
    } else if (moduleType === 'mobile_card' || moduleType === 'mobile_topup') {
      promotion = booking.mobile.promotion;
      promotionCode = booking.mobile.promotionCode;
    } else if (moduleType === 'flightPayLater') {
      promotion = flightOrder.promotion;
      promotionCode = flightOrder.promotionCode;
    } else if (moduleType === 'hotelPayLater') {
      promotion = hotelOrder.promotion;
      promotionCode = hotelOrder.promotionCode;
    } else if (moduleType === 'tourPayLater') {
      promotion = tourOrder.promotion;
      promotionCode = tourOrder.promotionCode;
    }

    return (
      <div>
        <Typography variant="body2" style={{ paddingLeft: '12px' }}>
          <FormattedMessage id="booking.enterDiscountCode" />
        </Typography>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex' }}>
            <BootstrapInput
              onChange={() => {}}
              margin="dense"
              style={{
                width: '368px',
                height: '40px',
              }}
              readOnly
              placeholder={intl.formatMessage({ id: 'booking.discountCodeExample' })}
              onClick={() => this.setState({ showPromotions: true })}
              value={promotionCode}
            />
            <div
              style={{
                display: 'flex',
                paddingLeft: '16px',
              }}
            >
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    minWidth: '175px',
                    height: '40px',
                  }}
                  onClick={() => this.setState({ showPromotions: true })}
                >
                  {!promotionCode ? (
                    <FormattedMessage id="booking.promotion.selectCode" />
                  ) : (
                    <FormattedMessage id="booking.promotion.selectAnotherCode" />
                  )}
                </Button>
              </div>

              {promotionCode && (
                <div style={{ paddingLeft: '32px' }}>
                  <Button
                    style={{
                      width: '166px',
                      height: '40px',
                      borderRadius: '4px',
                    }}
                    onClick={() => this.removePromotionCode()}
                  >
                    <Typography variant="body1" color="textSecondary">
                      <FormattedMessage id="booking.promotion.cancel" />
                    </Typography>
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>
        <div>
          <Typography
            variant="caption"
            style={{
              color: promotion && promotion.promotionDescription && promotion.price ? PRIMARY : RED,
              paddingLeft: '16px',
            }}
          >
            {promotion && promotion.promotionDescription ? promotion.promotionDescription : ''}
          </Typography>
        </div>
        {!MY_TOUR && (
          <div>
            <NewTabLink href={ROUTES.reward.myRewards} style={{ marginTop: '4px 0px 12px 8px' }}>
              <Button variant="text" style={{ padding: '4px' }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <IconGiftBox />
                  <Typography variant="body2" style={{ paddingLeft: '4px', color: BLUE }}>
                    <FormattedMessage id="booking.seeGiftList" />
                  </Typography>
                </div>
              </Button>
            </NewTabLink>
          </div>
        )}

        <Dialog open={showPromotions} fullWidth keepMounted={false} maxWidth="sm">
          <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '6px' }}>
            <IconButton size="small" onClick={() => this.setState({ showPromotions: false })}>
              <IconClose />
            </IconButton>
          </div>
          <PromotionListBox
            close={() => this.setState({ showPromotions: false })}
            selectCode={(value: string) => this.checkPromotion(value)}
            moduleType={moduleType}
          />
        </Dialog>
      </div>
    );
  }
}

function mapState2Props(state: AppState) {
  return {
    booking: state.booking,
    flightOrder: state.management.flightOrder,
    hotelOrder: state.management.hotelOrder,
    tourOrder: state.management.tourOrder,
  };
}

export default connect(mapState2Props)(injectIntl(DiscountCodeBox));
