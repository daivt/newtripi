import React from 'react';
import { MY_TOUR } from '../../../constants';
import FooterMyTour from './FooterMyTour';
import FooterTripi from './FooterTripi';

interface Props {
  compact?: boolean;
}
const Footer: React.FC<Props> = props => {
  const { compact } = props;
  if (MY_TOUR) {
    return <FooterMyTour />;
  }
  return <FooterTripi compact={compact} />;
};

export default Footer;
