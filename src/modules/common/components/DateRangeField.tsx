import * as React from 'react';
import styled from 'styled-components';
import { Typography } from '@material-ui/core';
import { Moment } from 'moment';
import { injectIntl, WrappedComponentProps, FormattedMessage } from 'react-intl';
import { DayPickerRangeController, FocusedInputShape } from 'react-dates';
import { BootstrapInput } from './elements';
import { RED } from '../../../colors';
import { renderMonthText } from '../utils';

const FieldDiv = styled.div`
  flex: 1;
  position: relative;
  outline: none;
`;

const redMark = <span style={{ color: RED }}>*</span>;

interface IDateRangeFieldProps extends WrappedComponentProps {
  headerId: string;
  optional: boolean;
  startDate?: Moment;
  endDate?: Moment;
  update(startDate?: Moment, endDate?: Moment): void;
  style?: React.CSSProperties;
}

const DateRangeField: React.FunctionComponent<IDateRangeFieldProps> = props => {
  const { headerId, optional, startDate, endDate, update, intl } = props;

  const [focused, setFocus] = React.useState(false);
  const [focusedInput, setFocusedInput] = React.useState<FocusedInputShape>('startDate');

  return (
    <FieldDiv tabIndex={-1} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)}>
      <div
        style={{
          position: 'absolute',
          minWidth: '100%',
          boxShadow: focused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
          background: focused ? 'white' : undefined,
          zIndex: focused ? 100 : 0,
        }}
      >
        <div>
          <Typography variant="body2">
            <FormattedMessage id={headerId} />
            &nbsp;
            {!optional && redMark}
          </Typography>
        </div>
        <BootstrapInput
          style={{ maxWidth: '400px' }}
          fullWidth
          value={`${startDate ? startDate.format('L') : intl.formatMessage({ id: 'nothingYet' })} -  ${
            endDate ? endDate.format('L') : intl.formatMessage({ id: 'nothingYet' })
          }`}
          placeholder=""
          readOnly
        />
        {focused && (
          <div style={{ padding: '15px' }}>
            <DayPickerRangeController
              renderMonthText={renderMonthText}
              hideKeyboardShortcutsPanel
              numberOfMonths={2}
              startDate={startDate || null}
              endDate={endDate || null}
              onFocusChange={val => setFocusedInput(val || 'startDate')}
              focusedInput={focusedInput}
              onDatesChange={({ startDate: start, endDate: end }) => update(start || undefined, end || undefined)}
            />
          </div>
        )}
      </div>
    </FieldDiv>
  );
};

export default injectIntl(DateRangeField);
