import { Location, UnregisterCallback } from 'history';
import { useCallback, useEffect } from 'react';
import { history } from '../../../redux/configureStore';
import { DEV, TEST, AUTOTEST } from '../../../constants';
import { analyticsRef } from '../../../utils';

interface Props {
  children: JSX.Element;
}
const GAListener = ({ children }: Props): JSX.Element => {
  const sendPageView = useCallback((location: Location): void => {
    if (analyticsRef.current && !DEV && !TEST && !AUTOTEST) {
      analyticsRef.current.logEvent('page_view', {
        page_path: location.pathname,
        page_title: document.title,
        page_location: window.location.href,
      });
    }
  }, []);

  useEffect((): UnregisterCallback | void => {
    sendPageView(history.location);
    return history.listen(sendPageView);
  }, [sendPageView]);

  return children;
};

export default GAListener;
