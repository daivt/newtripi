import { Button } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { connect } from 'react-redux';
import { ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import DiscountSvg from '../../../svg/DiscountNotify.svg';
import FlightSvg from '../../../svg/Flight.svg';
import HotelSvg from '../../../svg/Hotel.svg';
import IconCorporate from '../../../svg/ic_sidebar_corporate.svg';
import IconHelp from '../../../svg/ic_sidebar_help.svg';
import IconInfo from '../../../svg/ic_sidebar_info.svg';
import IconMessage from '../../../svg/ic_sidebar_message.svg';
import IconOliveRoom from '../../../svg/ic_sidebar_oliveroom.svg';
import IconPartner from '../../../svg/ic_sidebar_partner.svg';
import IconRecruitment from '../../../svg/ic_sidebar_recruitment.svg';
import MobileCodeSvg from '../../../svg/phone_card.svg';
import MobileDirectSvg from '../../../svg/phone_money.svg';
import PromotionSvg from '../../../svg/Promotion.svg';
import TicketHuntSvg from '../../../svg/TicketHunt.svg';
import TourSvg from '../../../svg/TourismActivities.svg';
import TrainSvg from '../../../svg/Train.svg';
import { RawLink as Link } from './Link';
import { NewTabLink } from './NewTabLink';

interface MenuItem {
  icon: string;
  labelId: string;
  url: string;
  newTab?: boolean;
}

interface Props extends ReturnType<typeof mapStateToProps> {
  openSidebar: boolean;
  onClose(): void;
}

interface State {}

const sidebarMenu: MenuItem[][] = [
  [
    {
      icon: FlightSvg,
      labelId: 'flight',
      url: ROUTES.flight.flight,
    },
    {
      icon: TrainSvg,
      labelId: 'trainTickets',
      url: ROUTES.train.train,
    },
    {
      icon: HotelSvg,
      labelId: 'hotel',
      url: ROUTES.hotel.hotel,
    },
    {
      icon: TourSvg,
      labelId: 'tour',
      url: ROUTES.tour.tour,
    },
    {
      icon: DiscountSvg,
      labelId: 'discountNotify',
      url: ROUTES.flight.ticketAlert,
    },
    {
      icon: TicketHuntSvg,
      labelId: 'ticketHunt',
      url: ROUTES.flight.flightHunt,
    },
    {
      icon: PromotionSvg,
      labelId: 'promotion',
      url: ROUTES.reward.rewards,
    },
    {
      icon: MobileDirectSvg,
      labelId: 'mobile.titleDirect',
      url: ROUTES.mobile.mobileTopup,
    },
    {
      icon: MobileCodeSvg,
      labelId: 'mobile.titleCode',
      url: ROUTES.mobile.mobileCard,
    },
  ],
  [
    {
      icon: IconPartner,
      labelId: 'header.partner',
      url: 'https://partner.tripi.vn',
      newTab: true,
    },
    {
      icon: IconOliveRoom,
      labelId: 'header.oliveRoom',
      url: 'https://olive.tripi.vn',
      newTab: true,
    },
    {
      icon: IconCorporate,
      labelId: 'header.corporate',
      url: 'https://corporate.tripi.vn',
      newTab: true,
    },
  ],
  [
    {
      icon: IconInfo,
      labelId: 'sidebar.info',
      url: 'https://partner.tripi.vn',
      newTab: true,
    },
    {
      icon: IconRecruitment,
      labelId: 'footer.recruitment',
      url: 'https://tripi.vn/blog/tuyen-dung',
      newTab: true,
    },
    {
      icon: IconMessage,
      labelId: 'footer.blogs',
      url: 'https://tripi.vn/blog/',
      newTab: true,
    },
  ],
  [
    // {
    //   icon: IconLoyalty,
    //   labelId: 'sidebar.loyalty',
    //   url: '/',
    // },
    // {
    //   icon: IconPayment,
    //   labelId: 'footer.onlinePayment',
    //   url: '/',
    // },
    {
      icon: IconHelp,
      labelId: 'sidebar.help',
      url: 'https://hotro.tripi.vn',
      newTab: true,
    },
  ],
];

class SidebarMenu extends PureComponent<Props, State> {
  toggleDrawer = () => (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }
    this.props.onClose();
  };

  render() {
    const { openSidebar, router } = this.props;
    return (
      <Drawer open={openSidebar} onClose={this.toggleDrawer()}>
        <PerfectScrollbar>
          <div
            style={{
              minHeight: '46px',
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginRight: '12px',
            }}
          >
            <IconButton size="small" onClick={this.toggleDrawer()}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <div
            style={{ minWidth: '305px' }}
            role="presentation"
            onClick={this.toggleDrawer()}
            onKeyDown={this.toggleDrawer()}
          >
            <Divider />
            <List>
              <ListItem>
                <NewTabLink href="https://legacy.tripi.vn">
                  <Button color="secondary" variant="text">
                    <Typography variant="body2">
                      <FormattedMessage id="useLegacyWebsite" />
                    </Typography>
                  </Button>
                </NewTabLink>
              </ListItem>
            </List>
            <Divider />
            {sidebarMenu.map((group, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <div key={index}>
                <List>
                  {group.map(item => (
                    <Link
                      to={{
                        pathname: item.url,
                        state: {
                          backableToHomePage: router.location.pathname === '/',
                        },
                      }}
                      key={item.labelId}
                      target={item.newTab ? '_blank' : undefined}
                      rel="noopener noreferrer"
                    >
                      <ListItem button style={{ height: '48px' }}>
                        <ListItemIcon style={{ width: '24px', minWidth: '24px' }}>
                          <img alt="" src={item.icon} />
                        </ListItemIcon>
                        <Typography variant="body2">
                          <span
                            style={{
                              paddingLeft: '8px',
                            }}
                          >
                            <FormattedMessage id={item.labelId} />
                          </span>
                        </Typography>
                      </ListItem>
                    </Link>
                  ))}
                </List>
                <Divider style={{ margin: '0 16px' }} />
              </div>
            ))}
          </div>
        </PerfectScrollbar>
      </Drawer>
    );
  }
}

const mapStateToProps = (state: AppState) => ({ router: state.router });

export default connect(mapStateToProps)(SidebarMenu);
