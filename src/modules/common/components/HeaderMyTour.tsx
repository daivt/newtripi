import { Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router';
import { BACKGROUND_MT, RED } from '../../../colors';
import { ROUTES } from '../../../constants';
import iconPhone from '../../../svg/my_tour/ic_phone.svg';
import { ReactComponent as Logo } from '../../../svg/my_tour/logo.svg';
import { Col, Row } from './elements';
import { HeaderContainer, MenuLabelMT } from './Header.styles';
import Link from './Link';
import { NewTabLink } from './NewTabLink';

const menus = [
  {
    name: 'header.hotel',
    url: 'https://mytour.vn/',
  },
  {
    name: 'header.flight',
    url: ROUTES.flight.flight,
    current: true,
    new: true,
  },
  {
    name: 'header.promotion',
    url: 'https://mytour.vn/promo',
  },
  {
    name: 'header.tour',
    url: 'https://mytour.vn/tour',
  },
  {
    name: 'header.handBook',
    url: 'https://mytour.vn/location',
  },
];

interface Props {}
const HeaderMyTour: React.FC<Props> = props => {
  // const {} = props;
  const history = useHistory();

  return (
    <HeaderContainer style={{ width: '100%', background: BACKGROUND_MT, boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.05)' }}>
      <Container
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '66px',
        }}
      >
        <a href="https://mytour.vn/">
          <Logo
            style={{
              margin: '0 30px 0 15px',
            }}
          />
        </a>
        <ul
          style={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'flex-start',
            listStyleType: 'none',
            flexShrink: 0,
          }}
        >
          {menus.map(menu => (
            <MenuLabelMT
              key={menu.name}
              style={{ background: history.location.pathname.includes(menu.url) ? '#35405214' : undefined }}
            >
              {menu.current ? (
                <>
                  <Link to={menu.url} style={{ color: 'inherit' }}>
                    <Typography variant="body2" color="textSecondary">
                      <FormattedMessage id={menu.name} />
                    </Typography>
                  </Link>
                </>
              ) : (
                <NewTabLink href={menu.url} style={{ color: 'inherit' }}>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id={menu.name} />
                  </Typography>
                </NewTabLink>
              )}
              {menu.new && (
                <Row
                  style={{
                    width: 32,
                    height: 16,
                    background: RED,
                    borderRadius: 12,
                    position: 'absolute',
                    top: -12,
                    right: 0,
                    justifyContent: 'center',
                  }}
                >
                  <Typography style={{ color: 'white', fontSize: '75%', fontWeight: 'bold' }}>new</Typography>
                </Row>
              )}
            </MenuLabelMT>
          ))}
        </ul>
        <div style={{ flex: 1 }} />
        <Row
          style={{
            display: 'flex',
            margin: '8px 10px 8px 0',
          }}
        >
          <Col style={{ padding: 8, borderRadius: '4px', background: '#e0f7ff' }}>
            <Row style={{ height: 16, marginBottom: 2 }}>
              <img src={iconPhone} alt="" style={{ height: 20, width: 18, marginRight: 8 }} />
              <Typography variant="caption" style={{ fontSize: 10, lineHeight: '16px' }}>
                <Typography
                  variant="caption"
                  style={{ fontWeight: 'bold', fontSize: 10, lineHeight: '16px' }}
                  component="span"
                >
                  Hà Nội
                </Typography>
                : 024 7109 9999
              </Typography>
            </Row>
            <Row style={{ height: 16, marginBottom: 2 }}>
              <Typography
                variant="caption"
                style={{ fontWeight: 'bold', fontSize: 8, marginRight: 8, lineHeight: '16px' }}
                component="span"
              >
                24/7
              </Typography>
              <Typography variant="caption" style={{ fontSize: 10, lineHeight: '16px' }}>
                <Typography
                  variant="caption"
                  style={{ fontWeight: 'bold', fontSize: 10, lineHeight: '16px' }}
                  component="span"
                >
                  TPHCM
                </Typography>
                : 028 7109 9998
              </Typography>
            </Row>
          </Col>

          <Col
            style={{
              alignItems: 'center',
              padding: 8,
              borderRadius: '4px',
              background: '#e0f7ff',
              marginLeft: 2,
              position: 'relative',
            }}
          >
            <div
              style={{
                content: '',
                position: 'absolute',
                top: 3,
                bottom: 3,
                width: 1,
                borderRight: '2px dashed #00adef4d',
                left: -2,
                zIndex: 1,
              }}
            />
            <Typography variant="caption" style={{ fontSize: 10, lineHeight: '16px', marginBottom: 2 }}>
              <FormattedMessage id="header.flightBookingSupport" />
            </Typography>
            <Typography
              variant="caption"
              style={{ fontSize: 14, fontWeight: 'bold', lineHeight: '16px', marginBottom: 2 }}
            >
              1900 2083
            </Typography>
          </Col>
        </Row>
      </Container>
    </HeaderContainer>
  );
};

export default HeaderMyTour;
