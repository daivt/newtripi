import { FormControl, FormHelperText, InputBaseProps, InputLabel } from '@material-ui/core';
import React from 'react';
import { redMark } from '../../booking/components/Form';
import { BootstrapInput2 } from './elements';

export interface FormControlTextFieldProps extends InputBaseProps {
  id?: string;
  label?: React.ReactNode;
  formControlStyle?: React.CSSProperties;
  errorMessage?: string;
  optional?: boolean;
}

const FormControlTextField = (props: FormControlTextFieldProps) => {
  const { id, label, formControlStyle, errorMessage, optional, value, fullWidth, ...rest } = props;

  return (
    <FormControl style={{ minWidth: 220, ...formControlStyle }} error={!!errorMessage} fullWidth>
      {label && (
        <InputLabel shrink htmlFor={id}>
          {label}
          {!optional && (
            <>
              &nbsp;
              {redMark}
            </>
          )}
        </InputLabel>
      )}
      <BootstrapInput2 id={id} value={value || ''} {...rest} error={!!errorMessage} />
      <FormHelperText id={id} style={{ minHeight: 20 }}>
        {errorMessage}
      </FormHelperText>
    </FormControl>
  );
};

export default FormControlTextField;
