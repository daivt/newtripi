import { Divider, Typography } from '@material-ui/core';
import { sum } from 'lodash';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLACK_TEXT, GREEN, ORANGE, RED } from '../../../../colors';
import { some } from '../../../../constants';
import { MUI_THEME } from '../../../../setupTheme';
import { ReactComponent as IconCoin } from '../../../../svg/coin.svg';
import { FlightBookingState } from '../../../booking/redux/flightBookingReducer';
import { computePayableNumbers, computePoints } from '../../../booking/utils';
import { Col } from '../elements';

const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const MoneySpan = styled.span`
  color: ${ORANGE};
`;

const CountSpan = styled.span`
  color: ${BLACK_TEXT};
`;

const Item = (props: { data: some; titleId: string; searchRequest: some; extraBaggages: number[] }) => {
  const { data, searchRequest, extraBaggages } = props;
  const { numAdults, numChildren, numInfants } = searchRequest;
  const totalPriceExtraBaggages =
    data.baggages && data.baggages.length ? sum(extraBaggages.map(val => data.baggages[val].price)) : 0;

  const { priceAdultUnit, priceChildUnit, priceInfantUnit } = data.ticketdetail;

  return (
    <div style={{ flex: 1, marginTop: '10px' }}>
      <div style={{ height: '40px', display: 'flex', alignItems: 'center' }}>
        <Typography variant="subtitle1">
          <FormattedMessage id={props.titleId} />
        </Typography>
      </div>
      {!!numAdults && (
        <Line>
          <FormattedMessage id="booking.adultPrice" />
          <MoneySpan>
            <CountSpan>
              {numAdults}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={priceAdultUnit} />
            &nbsp;
            <FormattedMessage id="currency" />
          </MoneySpan>
        </Line>
      )}
      {!!numChildren && (
        <Line>
          <FormattedMessage id="booking.childPrice" />
          <MoneySpan>
            <CountSpan>
              {numChildren}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={priceChildUnit} />
            &nbsp;
            <FormattedMessage id="currency" />
          </MoneySpan>
        </Line>
      )}
      {!!numInfants && (
        <Line>
          <FormattedMessage id="booking.babyPrice" />
          <MoneySpan>
            <CountSpan>
              {numInfants}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={priceInfantUnit} />
            &nbsp;
            <FormattedMessage id="currency" />
          </MoneySpan>
        </Line>
      )}
      {!!totalPriceExtraBaggages && (
        <Line>
          <FormattedMessage id="booking.extraBaggages" />
          <MoneySpan>
            <FormattedNumber value={totalPriceExtraBaggages} />
            &nbsp;
            <FormattedMessage id="currency" />
          </MoneySpan>
        </Line>
      )}

      <Line>
        <Typography variant="body2">
          <FormattedMessage
            id="booking.total"
            values={{
              text: (
                <span style={{ textTransform: 'lowercase' }}>
                  <FormattedMessage id={props.titleId} />
                </span>
              ),
            }}
          />
        </Typography>
        <MoneySpan style={{ fontWeight: MUI_THEME.typography.subtitle2.fontWeight }}>
          <FormattedNumber
            value={
              totalPriceExtraBaggages +
              numAdults * priceAdultUnit +
              numChildren * priceChildUnit +
              numInfants * priceInfantUnit
            }
          />
          &nbsp;
          <FormattedMessage id="currency" />
        </MoneySpan>
      </Line>
    </div>
  );
};

interface Props {
  booking: FlightBookingState;
}

const FlightPriceDetail = (props: Props) => {
  const { booking } = props;
  const payableNumbers = computePayableNumbers(booking);
  const { extraBaggagesCosts, adult, children, baby } = payableNumbers;
  const totalPayable = payableNumbers.originPrice;
  return (
    <div style={{ width: '570px', margin: '10px auto' }}>
      <div style={{ display: 'flex', alignItems: 'flex-start' }}>
        {booking.outbound.ticket && (
          <Item
            data={booking.outbound.ticket.outbound}
            searchRequest={booking.outbound.searchRequest}
            extraBaggages={booking.outbound.extraBaggages}
            titleId="booking.outboundPrice"
          />
        )}
        {booking.inbound.ticket && (
          <>
            <div style={{ width: '30px' }} />
            <Item
              data={booking.inbound.ticket.outbound}
              searchRequest={booking.inbound.searchRequest}
              extraBaggages={booking.inbound.extraBaggages}
              titleId="booking.inboundPrice"
            />
          </>
        )}
      </div>
      <Divider />
      <Col style={{ marginTop: '4px' }}>
        {booking.insurancePackage && booking.buyInsurance && (
          <Line>
            <FormattedMessage id="booking.travelInsurance" />
            <MoneySpan>
              <CountSpan>
                {adult.number + children.number + baby.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={booking.insurancePackage.price} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        {!!extraBaggagesCosts && (
          <Line>
            <FormattedMessage id="booking.buyMoreCheckin" />
            <MoneySpan>
              <FormattedNumber value={extraBaggagesCosts} />
              &nbsp;
              <FormattedMessage id="currency" />
            </MoneySpan>
          </Line>
        )}
        <Line style={{ justifyContent: 'space-between' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="booking.totalPayable" />
          </Typography>
          <Typography variant="subtitle1" style={{ color: RED }}>
            <FormattedNumber value={totalPayable > 0 ? totalPayable : 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>

        <Line style={{ justifyContent: 'space-between' }}>
          <Typography variant="caption" color="textSecondary">
            <FormattedMessage id="booking.includeTaxesAndFees" />
          </Typography>
          {computePoints(booking) > 0 && (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
              <IconCoin style={{ marginRight: '10px' }} />
              <Typography variant="body2" style={{ color: GREEN }}>
                <FormattedNumber value={computePoints(booking)} />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
          )}
        </Line>
      </Col>
    </div>
  );
};

export default FlightPriceDetail;
