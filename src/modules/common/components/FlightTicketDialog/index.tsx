import { Dialog, DialogActions, DialogContent, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { connect } from 'react-redux';
import SwipeableViews from 'react-swipeable-views';
import { Dispatch } from 'redux';
import styled from 'styled-components';
import { GREY } from '../../../../colors';
import { AppState } from '../../../../redux/reducers';
import { makeGetAirlineInfo } from '../../utils';
import { CustomTab, CustomTabs } from '../elements';
import FlightIteraryDetail from './FlightIteraryDetail';
import FlightPriceDetail from './FlightPriceDetail';

const ID = 'swipeableView';

const Content = styled.div`
  transition: opacity 300ms;
  overflow-x: visible;
`;

interface Props extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  close(): void;
  open: boolean;
  button?: React.ReactNode;
}

interface State {
  tabValue: number;
}

const FlightTicketDialog: React.FC<Props> = props => {
  const { booking, getAirlineInfo, intl, close, open, button } = props;
  const [tabValue, setTabValue] = React.useState(1);

  const scrollTop = React.useCallback(() => {
    const div = document.getElementById(ID);
    if (div) {
      div.scrollTo({ top: 0 });
    }
  }, []);

  return open ? ( // do this because of bug in Material UI dialog rendering dialog even when open is false
    <Dialog open fullWidth maxWidth="md">
      <div style={{ position: 'relative' }}>
        <div
          style={{
            right: '3px',
            top: '3px',
            position: 'absolute',
            zIndex: 1000,
          }}
        >
          <IconButton size="small" onClick={close}>
            <CloseIcon style={{ fontSize: '24px' }} />
          </IconButton>
        </div>
      </div>

      <DialogContent
        style={{
          padding: '0 34px',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <CustomTabs
          variant="fullWidth"
          value={tabValue}
          centered
          onChange={(e, val) => {
            scrollTop();
            setTabValue(val);
          }}
        >
          <CustomTab fullWidth label={intl.formatMessage({ id: 'booking.flightDetails' })} />
          <CustomTab fullWidth label={intl.formatMessage({ id: 'booking.priceDetails' })} />
        </CustomTabs>
        <div
          style={{
            maxHeight: 'calc(100vh - 250px)',
            display: 'flex',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          <SwipeableViews
            id={ID}
            index={tabValue}
            onSwitching={() => {
              scrollTop();
            }}
          >
            <Content>
              <FlightIteraryDetail booking={booking} getAirlineInfo={getAirlineInfo} />
            </Content>
            <Content>
              <FlightPriceDetail booking={booking} />
            </Content>
          </SwipeableViews>
        </div>
      </DialogContent>
      {button ? (
        <DialogActions
          style={{
            margin: '0 34px 20px 34px',
            padding: '15px 0 0 0',
            borderTop: `1px solid ${GREY}`,
          }}
        >
          {button}
        </DialogActions>
      ) : (
        <br />
      )}
    </Dialog>
  ) : (
    <></>
  );
};

function mapStateToProps(state: AppState) {
  return {
    getAirlineInfo: makeGetAirlineInfo(state.common),
    booking: state.booking.flight,
  };
}

export default connect(mapStateToProps)(injectIntl(FlightTicketDialog));
