import { Divider } from '@material-ui/core';
import * as React from 'react';
import { FlightBookingState } from '../../../booking/redux/flightBookingReducer';
import { FlightDirectionDetail } from '../../../result/components/flight/FlightTicketDetail';
import { some } from '../../../../constants';

export interface IFlightIteraryDetailProps {
  booking: FlightBookingState;
  getAirlineInfo(id: number): some;
}

const FlightIteraryDetail = (props: IFlightIteraryDetailProps) => {
  const { booking, getAirlineInfo } = props;
  return (
    <div style={{ margin: '10px 0' }}>
      {booking.outbound.ticket && (
        <FlightDirectionDetail ticket={booking.outbound.ticket} getAirlineInfo={getAirlineInfo} />
      )}
      {booking.inbound.ticket && (
        <>
          <Divider style={{ margin: '10px 0' }} />
          <FlightDirectionDetail ticket={booking.inbound.ticket} getAirlineInfo={getAirlineInfo} />
        </>
      )}
    </div>
  );
};

export default FlightIteraryDetail;
