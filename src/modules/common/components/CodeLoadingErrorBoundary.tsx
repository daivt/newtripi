import { Button, Container, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { PageWrapper } from './elements';

export interface ICodeLoadingErrorBoundaryProps {}

export interface ICodeLoadingErrorBoundaryState {
  error: boolean;
}

export default class CodeLoadingErrorBoundary extends React.Component<
  ICodeLoadingErrorBoundaryProps,
  ICodeLoadingErrorBoundaryState
> {
  constructor(props: ICodeLoadingErrorBoundaryProps) {
    super(props);

    this.state = { error: false };
  }

  componentDidCatch() {
    this.setState({ error: true });
  }

  public render() {
    const { error } = this.state;
    return (
      <div style={{ minHeight: '100vh' }}>
        <PageWrapper>
          <Container
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {error ? (
              <>
                <Typography variant="body2" color="error">
                  <FormattedMessage id="asyncModuleLoadError" />
                </Typography>
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={() => this.setState({ error: false })}
                >
                  <FormattedMessage id="retry" />
                </Button>
              </>
            ) : (
              this.props.children
            )}
          </Container>
        </PageWrapper>
      </div>
    );
  }
}
