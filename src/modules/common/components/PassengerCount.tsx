import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';

interface Props extends WrappedComponentProps {
  adults?: number;
  children?: number;
  infants?: number;
  babies?: number;
}

const PassengerCount: React.FC<Props> = props => {
  const { intl, adults, children, infants, babies } = props;
  const passengers = [];

  if (adults) {
    passengers.push(`${adults} ${intl.formatMessage({ id: 'adult' }).toLowerCase()}`);
  }

  if (children) {
    passengers.push(`${children} ${intl.formatMessage({ id: 'children' }).toLowerCase()}`);
  }

  if (infants) {
    passengers.push(`${infants} ${intl.formatMessage({ id: 'infant' }).toLowerCase()}`);
  }

  if (babies) {
    passengers.push(`${babies} ${intl.formatMessage({ id: 'baby' }).toLowerCase()}`);
  }

  return <span>{passengers.join(', ')}</span>;
};

export default injectIntl(PassengerCount);
