import React from 'react';
import { MY_TOUR } from '../../../constants';
import HeaderMyTour from './HeaderMyTour';
import HeaderTripi from './HeaderTripi';

interface Props {
  light?: boolean;
}
const Header: React.FC<Props> = props => {
  const { light } = props;
  // const HeaderMyTour = Loadable({
  //   loader: () => import('./HeaderMyTour'),
  //   loading: RouteLoading,
  // });
  if (MY_TOUR) {
    return <HeaderMyTour />;
  }
  return <HeaderTripi light={light} />;
};

export default Header;
