import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl, WrappedComponentProps } from 'react-intl';
import styled from 'styled-components';
import { ROUTES } from '../../../constants';
import { ReactComponent as IconEmail } from '../../../svg/ic_email.svg';
import { ReactComponent as IconFacebook } from '../../../svg/ic_facebook.svg';
import { ReactComponent as IconGoogle } from '../../../svg/ic_google.svg';
import { ReactComponent as IconLocation } from '../../../svg/ic_location.svg';
import { ReactComponent as IconPhone } from '../../../svg/ic_phone.svg';
import { ReactComponent as IconYoutube } from '../../../svg/ic_youtube.svg';
import A from './A';
import { NewTabLink } from './NewTabLink';

const FooterContainer = styled.div`
  background: #212121;
  color: #fff;
`;

interface Props extends WrappedComponentProps {
  compact?: boolean;
}

class FooterTripi extends PureComponent<Props, {}> {
  render() {
    const { compact, intl } = this.props;

    return (
      <Typography variant="caption">
        <FooterContainer>
          {!compact && (
            <Container>
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'row',
                  justifyContent: 'space-around',
                  flexWrap: 'nowrap',
                  flex: 1,
                  paddingTop: '16px',
                }}
              >
                <div
                  style={{
                    marginBottom: '32px',
                    display: 'flex',
                  }}
                >
                  <div>
                    <img alt="" src="https://tripiweb.s3-ap-southeast-1.amazonaws.com/trade_security.png" />
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                      flexFlow: 'column',
                      paddingLeft: '30px',
                    }}
                  >
                    <div style={{ paddingBottom: '26px' }}>
                      <A href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=21798">
                        <img alt="" src="https://tripiweb.s3-ap-southeast-1.amazonaws.com/certify.png" />
                      </A>
                    </div>
                    <div>
                      <img alt="" src="https://tripiweb.s3-ap-southeast-1.amazonaws.com/logo_vitm.png" />
                    </div>
                  </div>
                </div>
                <div style={{ paddingTop: '32px' }}>
                  <Typography variant="body1">
                    <FormattedMessage id="footer.termsOfUse" />
                  </Typography>
                  <p>
                    <NewTabLink href={ROUTES.securityPolicies} style={{ color: 'inherit' }}>
                      <FormattedMessage id="footer.privacyPolicy" />
                    </NewTabLink>
                  </p>
                  <p>
                    <NewTabLink href={ROUTES.terms} style={{ color: 'inherit' }}>
                      <FormattedMessage id="footer.websiteRegulations" />
                    </NewTabLink>
                  </p>
                </div>

                <div style={{ paddingTop: '32px' }}>
                  <Typography variant="body1">
                    <FormattedMessage id="footer.guide" />
                  </Typography>
                  <p>
                    <A href="https://hotro.tripi.vn/knowledgebase/cac-hinh-thuc-thanh-toan-tren-tripi-partner/">
                      <FormattedMessage id="footer.onlinePayment" />
                    </A>
                  </p>
                  <p>
                    <A href="https://hotro.tripi.vn/knowledgebase/lam-the-nao-de-su-dung-ma-giam-gia/">
                      <FormattedMessage id="footer.usingDiscountCodes" />
                    </A>
                  </p>
                </div>
                <div style={{ paddingTop: '32px' }}>
                  <Typography variant="body1">
                    <FormattedMessage id="footer.about" />
                  </Typography>
                  <p>
                    <A href="https://partner.tripi.vn/">
                      <FormattedMessage id="footer.introduce" />
                    </A>
                  </p>
                  <p>
                    <A href="https://www.tripi.vn/blog/tuyen-dung">
                      <FormattedMessage id="footer.recruitment" />
                    </A>
                  </p>
                  <p>
                    <A href="https://www.tripi.vn/blog/">
                      <FormattedMessage id="footer.blogs" />
                    </A>
                  </p>
                </div>

                <div style={{ paddingTop: '32px', maxWidth: '20%' }}>
                  <div
                    style={{
                      fontSize: '16px',
                      fontWeight: 500,
                      lineHeight: '24px',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Typography variant="body1">
                      <FormattedMessage id="contactInfo" />
                    </Typography>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      padding: '6px 0',
                    }}
                  >
                    <IconPhone style={{ paddingRight: '10px' }} />
                    <FormattedMessage id="footer.phoneNumber" />
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      padding: '6px 0',
                    }}
                  >
                    <IconEmail style={{ paddingRight: '10px' }} />
                    <a
                      style={{ color: '#fff', textDecoration: 'none' }}
                      href={`mailto:${intl.formatMessage({ id: 'footer.emailContact' })}`}
                    >
                      <FormattedMessage id="footer.emailContact" />
                    </a>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <IconLocation style={{ paddingRight: '10px', flexShrink: 0 }} />
                    <FormattedMessage id="footer.address" />
                  </div>
                </div>
              </div>
            </Container>
          )}

          <div
            style={{
              backgroundColor: 'rgba(255, 255, 255, 0.1)',
              width: '100%',
            }}
          >
            <Container
              style={{
                justifyContent: 'space-around',
                flexWrap: 'wrap',
                display: 'flex',
                alignItems: 'center',
                minHeight: '40px',
                textAlign: 'center',
              }}
            >
              <span>
                <FormattedMessage id="footer.copyright" />
              </span>

              <span style={{ display: 'flex', opacity: 0 }}>
                <IconFacebook style={{ padding: '0 10px' }} />
                <IconGoogle style={{ padding: '0 10px' }} />
                <IconYoutube style={{ padding: '0 10px' }} />
              </span>
            </Container>
          </div>
        </FooterContainer>
      </Typography>
    );
  }
}

export default injectIntl(FooterTripi);
