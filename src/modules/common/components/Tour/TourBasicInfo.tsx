import * as React from 'react';
import { Typography } from '@material-ui/core';
import { BLUE } from '../../../../colors';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { some } from '../../../../constants';

interface Props {
  tour: some;
}

const TourBasicInfo: React.FunctionComponent<Props> = props => {
  const { tour } = props;
  return (
    <div
      style={{
        display: 'flex',
        minHeight: '48px',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}
    >
      <div
        style={{
          background: BLUE,
          borderRadius: '4px',
          minWidth: '32px',
          height: '32px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          padding: '0px 2px',
        }}
      >
        <Typography variant="h6" style={{ color: 'white' }}>
          <FormattedNumber value={tour.expertRating} />
        </Typography>
      </div>
      <div style={{ marginLeft: '12px' }}>
        <Typography variant="body2" style={{ color: BLUE, lineHeight: '16px', fontWeight: 500 }}>
          <FormattedMessage
            id={
              tour.expertRating > 9
                ? 'hotel.result.info.ratingExcellent'
                : tour.expertRating > 8 && tour.expertRating < 9
                ? 'hotel.result.info.ratingVeryGood'
                : tour.expertRating > 7 && tour.expertRating < 8
                ? 'hotel.result.info.ratingGood'
                : tour.expertRating > 6 && tour.expertRating < 7
                ? 'hotel.result.info.ratingSatisfied'
                : 'hotel.result.info.ratingNormal'
            }
          />
        </Typography>
        {tour.numberOfComments > 0 && (
          <Typography color="textSecondary" variant="caption">
            <FormattedMessage
              id={'hotel.result.info.ratingNote'}
              values={{ num: tour.numberOfComments }}
            />
          </Typography>
        )}
      </div>
    </div>
  );
};

export default TourBasicInfo;
