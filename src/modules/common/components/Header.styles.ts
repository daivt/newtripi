import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { some } from '../../../constants';

export const HeaderContainer = styled.div`
  left: 0;
  right: 0;
  top: 0;
  position: sticky;
  z-index: 1200;
  outline: none;
`;

export const BoxHeader = styled.div<{ light: boolean; subMenuCount?: number }>`
  width: 100%;
  background: ${props =>
    props.light
      ? '#fff'
      : props.subMenuCount
      ? 'rgba(0, 0, 0, 0.8)'
      : 'linear-gradient(rgb(33, 33, 33) 0%, rgba(33, 33, 33, 0) 100%)'};
  transition: all 300ms;
`;

export const SubHeader = styled.div<{ light: boolean }>`
  box-shadow: ${props => (props.light ? 'rgba(0, 0, 0, 0.25) 0px 4px 4px' : 'none')};
  width: 100%;
  color: ${props => (props.light ? '#000' : '#fff')};
  :hover {
    box-shadow: ${props => (props.light ? 'rgba(0, 0, 0, 0.25) 0px 4px 4px' : 'none')};
  }
  transition: all 300ms;
`;

export const MenuLabel = styled.li<{ menu?: some }>`
  margin-right: 25px;
  ::after {
    content: '';
    display: block;
    width: 0;
    height: 2px;
    left: 50%;
    background: ${props => props.theme.primary};
    transition: all ease-in-out 300ms;
  }

  :hover {
    cursor: pointer;
    ::after {
      width: ${props => (!props.menu?.subMenu ? '100%' : 0)};
    }
  }
`;
export const MenuLabelMT = styled.li`
  margin-right: 8px;
  border-radius: 26px;
  height: 26px;
  display: flex;
  align-items: center;
  font-size: 14px;
  position: relative;
  padding: 3px 6px;
  :hover {
    background: #35405214;
  }
`;

export const SignUpLabel = styled.span<{ light: boolean }>`
  padding-right: 12px;
  color: ${props => (props.light ? props.theme.secondary : '#fff')};
  :hover {
    cursor: pointer;
  }
`;

export const SubMenuItem = styled(Link)`
  display: flex;
  flex-flow: column;
  width: 100%;
  position: relative;
  text-decoration: none;
  color: inherit;
  ::after {
    content: '';
    display: block;
    position: absolute;
    width: 0;
    bottom: 0;
    height: 4px;
    left: 50%;
    background: ${props => props.theme.primary};
    transition: all ease-in-out 0.3s;
  }
  :hover {
    cursor: pointer;
    ::after {
      width: 100%;
      left: 0;
    }
  }
`;

export const Sidebar = styled.div`
  padding-right: 10px;
  :hover {
    cursor: pointer;
  }
`;
