import React from 'react';
import styled from 'styled-components';
import { BLUE } from '../../../colors';

const Link = styled.a`
  text-decoration: none;
  color: ${BLUE};
`;

interface Props {
  href: string;
  style?: React.CSSProperties;
}

export const NewTabLink: React.FC<Props> = props => {
  return (
    <Link href={props.href} rel="noopener noreferrer" target="_blank" style={props.style}>
      {props.children}
    </Link>
  );
};
