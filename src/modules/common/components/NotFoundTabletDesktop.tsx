import { Container } from '@material-ui/core';
import * as React from 'react';
import { TABLET_WIDTH } from '../../../constants';
import { PageWrapper } from './elements';
import Footer from './Footer';
import Header from './Header';
import NotFoundBox from './NotFoundBox';

interface INotFoundTabletDesktopProps {}

const NotFoundTabletDesktop: React.FunctionComponent<INotFoundTabletDesktopProps> = props => {
  return (
    <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
      <Header light={true} />
      <Container style={{ flex: 1 }}>
        <NotFoundBox />
      </Container>
      <Footer />
    </PageWrapper>
  );
};

export default NotFoundTabletDesktop;
