import * as React from 'react';
import { Paper, IconButton } from '@material-ui/core';
import SwapHorizRoundedIcon from '@material-ui/icons/SwapHorizRounded';
import { useTheme } from '@material-ui/styles';
import { some } from '../../../constants';

interface ISwapProps {
  onClick: () => unknown;
  home?: boolean;
}

const Swap: React.FunctionComponent<ISwapProps> = props => {
  const theme = useTheme() as some;

  return (
    <Paper
      style={{
        borderRadius: '50%',
        background: props.home ? theme.palette.primary.main : theme.palette.secondary.main,
        cursor: 'pointer',
      }}
      elevation={3}
    >
      <IconButton style={{ padding: 0 }} onClick={props.onClick}>
        <SwapHorizRoundedIcon
          style={{
            color: 'white',
            display: 'block',
            margin: '6px',
          }}
        />
      </IconButton>
    </Paper>
  );
};

export default Swap;
