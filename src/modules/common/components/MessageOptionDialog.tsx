import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

export interface IMessageDialogProps {
  show: boolean;
  message: React.ReactNode;
  onClose(): void;
  onClick(): void;
  header?: React.ReactNode;
  buttonMessageIdFirst?: string;
  buttonMessageIdSecond?: string;
  style?: React.CSSProperties;
  showCloseButton?: boolean;
}

export default class MessageOptionDialog extends React.PureComponent<IMessageDialogProps> {
  public render() {
    const {
      show,
      message,
      onClose,
      header,
      buttonMessageIdFirst,
      buttonMessageIdSecond,
      style,
      onClick,
      showCloseButton,
    } = this.props;

    return (
      <Dialog open={show} fullWidth maxWidth="xs" onClose={onClose} PaperProps={{ style }}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            {!!header && (
              <div style={{ display: 'flex', flex: 1, paddingLeft: '16px' }}>
                <Typography variant="h6">{header}</Typography>
              </div>
            )}
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              {!!showCloseButton && (
                <IconButton style={{ padding: '8px' }} size="small" onClick={onClose}>
                  <IconClose />
                </IconButton>
              )}
            </div>
          </div>
          {message}
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              padding: '0px 36px 32px 36px',
            }}
          >
            <Button
              style={{ marginRight: '16px', width: '150px' }}
              variant="outlined"
              onClick={onClose}
            >
              <Typography color="textSecondary">
                <FormattedMessage id={buttonMessageIdFirst || 'ignore'} />
              </Typography>
            </Button>
            <Button
              style={{ marginLeft: '16px', width: '150px' }}
              variant="contained"
              color="secondary"
              onClick={onClick}
            >
              <Typography>
                <FormattedMessage id={buttonMessageIdSecond || 'ok'} />
              </Typography>
            </Button>
          </div>
        </div>
      </Dialog>
    );
  }
}
