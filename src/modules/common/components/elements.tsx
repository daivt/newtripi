import {
  IconButton,
  InputBase,
  PropTypes,
  SnackbarContent,
  Tab,
  Tabs,
  Typography,
  withStyles,
  Tooltip,
} from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import { fade, Theme, createStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { OptionsObject } from 'notistack';
import React from 'react';
import styled from 'styled-components';
import MaskedInput from 'react-text-mask';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { GREY, PRIMARY, SECONDARY, LIGHT_GREY } from '../../../colors';
import LoadingButton from './LoadingButton';
import { MUI_THEME } from '../../../setupTheme';
import { durationMillisecondToHour } from '../utils';

export const Row = styled.div`
  display: flex;
  align-items: center;
`;
export const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

export function snackbarSetting(message: React.ReactNode, closeSnackbar: (key: string) => void) {
  return {
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'right',
    },
    preventDuplicate: true,
    autoHideDuration: 2000,
    content: (key: string) => (
      <SnackbarContent
        id={key}
        style={{
          background: 'white',
          boxShadow:
            '0px 3px 5px rgba(0, 0, 0, 0.2), 0px 1px 18px rgba(0, 0, 0, 0.12), 0px 6px 10px rgba(0, 0, 0, 0.14)',
          borderRadius: '8px',
          minWidth: '250px',
        }}
        aria-describedby="client-snackbar"
        message={
          <Typography variant="body2" color="textPrimary">
            {message}
          </Typography>
        }
        action={[
          <IconButton key="close" aria-label="close" onClick={() => closeSnackbar(key)}>
            <CloseIcon />
          </IconButton>,
        ]}
      />
    ),
  } as OptionsObject;
}

export const Line = styled.div`
  display: flex;
  align-items: center;
`;
export const subtitle3Styles: React.CSSProperties = {
  fontWeight: 'bold',
  fontSize: MUI_THEME.typography.body2.fontSize,
};
export const subHeader2: React.CSSProperties = {
  fontWeight: 500,
  letterSpacing: '0.1px',
  fontSize: MUI_THEME.typography.body2.fontSize,
  lineHeight: '24px',
};

export const BootstrapInput = withStyles((theme: Theme) => ({
  '@keyframes vibrate': {
    '0%': {
      left: '-8px',
    },
    '50%': {
      left: '8px',
    },
    '100%': {
      left: '-8px',
    },
  },
  root: {
    borderRadius: 4,
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: `${GREY}`,
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    position: 'relative',
  },
  input: {
    borderRadius: 4,
    backgroundColor: 'transparent',
    padding: '5px 12px',
    display: 'flex',
    alignItems: 'center',
    minHeight: '40px',
    fontSize: `${theme.typography.body2.fontSize}`,
    boxSizing: 'border-box',
    '&::placeholder': {
      color: GREY,
      opacity: 1,
    },
  },
  error: {
    boxShadow: `${fade(theme.palette.error.main, 0.25)} 0 0 0 0.2rem`,
    borderColor: theme.palette.error.main,
    animationName: '$vibrate',
    animationDuration: '175ms',
    animationTimingFunction: `${theme.transitions.easing.easeInOut}`,
    animationIterationCount: 5,
  },
  focused: {
    boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
    borderColor: theme.palette.primary.main,
  },
}))(InputBase);

export const BootstrapInput2 = withStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight: 40,
      padding: 0,
      border: '1px solid #ced4da',
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.common.white,
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      overflow: 'hidden',
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      fontSize: theme.typography.body2.fontSize,
      padding: '8px',
    },
    focused: {
      boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
    },
    error: {
      boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.error.main,
    },
    disabled: {
      backgroundColor: LIGHT_GREY,
      color: 'black',
    },
  }),
)(InputBase);

export const CustomTabs = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${GREY}`,
  },
  indicator: {
    backgroundColor: theme.palette.primary.main,
    height: 4,
  },
}))(Tabs);

export const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: 500,
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

export const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  min-height: 100vh;
`;

// cannot use inline, do this to have vendor prefix for sticky
export const StickyDiv = styled.div`
  position: sticky;
`;

export const CheckButton = (
  props: ButtonProps & {
    active: boolean;
    loading?: boolean;
    loadingColor?: PropTypes.Color;
    backgroundColor?: PropTypes.Color;
  },
) => {
  const { active, style, ref, loading, loadingColor, backgroundColor, ...rest } = props;
  return (
    <LoadingButton
      {...rest}
      loadingColor={loadingColor}
      loading={loading}
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active
          ? loading
            ? fade(backgroundColor === 'secondary' ? SECONDARY : PRIMARY, 0.4)
            : backgroundColor === 'secondary'
            ? SECONDARY
            : PRIMARY
          : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        textTransform: 'none',
        color: active ? 'white' : '#757575',
        borderColor: active ? (backgroundColor === 'secondary' ? SECONDARY : PRIMARY) : undefined,
        minHeight: '30px',
        display: 'flex',
        alignItems: 'center',
        padding: '0px 16px',
        ...style,
      }}
    >
      {props.children}
    </LoadingButton>
  );
};

interface DateMaskCustomProps {
  inputRef: (ref: HTMLInputElement | null) => void;
  placeholder: string;
}

export const DateMaskCustomRange = (props: DateMaskCustomProps) => {
  const { inputRef, placeholder, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        ' ',
        '-',
        ' ',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ]}
      placeholder={placeholder}
      guide={false}
      // placeholderChar="\u2000"
      keepCharPositions
    />
  );
};

export const DateMaskCustomSingle = (props: DateMaskCustomProps) => {
  const { inputRef, placeholder, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
      placeholder={placeholder}
      guide={false}
    />
  );
};

export const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  min-width: 100%;
  overflow: hidden;
`;

interface PropsLabel {
  children: React.ReactElement;
  open: boolean;
  value: number;
}
export function PointLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={
        <>
          <FormattedNumber value={value} />
        </>
      }
    >
      {children}
    </Tooltip>
  );
}
export function PriceLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={
        <>
          <FormattedNumber value={value} />
          &nbsp;
          <FormattedMessage id="currency" />
        </>
      }
    >
      {children}
    </Tooltip>
  );
}
export function TimeLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={durationMillisecondToHour(value)}>
      {children}
    </Tooltip>
  );
}
