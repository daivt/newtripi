import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import IconClose from '@material-ui/icons/Clear';
import IconSearch from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import * as React from 'react';
import { FormattedMessage, WrappedComponentProps, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { BLUE, DARK_GREY, GREY, HOVER_GREY } from '../../../colors';
import { ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconNoResult } from '../../../svg/ic_contact_no_result.svg';
import { ReactComponent as IconPeople } from '../../../svg/ic_contact_people.svg';
import { CURRENT_PARAM_NAME } from '../../management/constants';
import { fetchContactList } from '../redux/reducer';
import { BootstrapInput } from './elements';
import Link from './Link';
import LoadingIcon from './LoadingIcon';

const ItemRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 0.5px solid rgb(189, 189, 189);
  min-height: 48px;
  padding: 0px 8px;
  cursor: pointer;

  :hover {
    background: ${HOVER_GREY};
  }
`;

const BoxResultMessage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 50px;
  padding-left: 50px;
  padding-right: 50px;
  text-align: center;
`;

interface ISelectContactDialogProps
  extends WrappedComponentProps,
    ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  onClose(): void;
  onSelect(customer: some): void;
  show: boolean;
}

interface ISelectContactDialogState {
  search: string;
  loading: boolean;
  isSearch: boolean;
  contactData?: some[];
  contactDataOriginal: some[];
}
class SelectContactDialog extends React.PureComponent<
  ISelectContactDialogProps,
  ISelectContactDialogState
> {
  state: ISelectContactDialogState = {
    loading: false,
    isSearch: false,
    search: '',
    contactDataOriginal: [],
  };

  fetchData = async () => {
    this.setState({ loading: true });

    const json = await this.props.dispatch(fetchContactList());

    if (json.code === 200) {
      this.setState({ loading: false, contactData: json.data, contactDataOriginal: json.data });
    }
  };

  search = debounce(
    async () => {
      const { contactDataOriginal } = this.state;

      if (this.state.search) {
        const contacts = contactDataOriginal.filter(
          item =>
            `${item.lastName.trim()} ${item.firstName.trim()}`
              .toLowerCase()
              .indexOf(this.state.search.toLowerCase()) !== -1,
        );

        this.setState({ contactData: contacts, isSearch: true });
      } else {
        this.setState({ contactData: contactDataOriginal, isSearch: false });
      }
    },
    500,
    { trailing: true },
  );

  public render() {
    const { search, contactData, isSearch, loading } = this.state;
    const { intl, show, onClose, onSelect } = this.props;

    return (
      <Dialog open={show} keepMounted={false} maxWidth="md" onEnter={this.fetchData}>
        <div style={{ width: '500px', height: '600px', display: 'flex', flexDirection: 'column' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Typography
              variant="h5"
              style={{
                display: 'flex',
                alignItems: 'center',
                height: '40px',
                marginTop: '16px',
                marginLeft: '24px',
              }}
            >
              <FormattedMessage id="contact.findCustomer" />
            </Typography>

            <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '6px' }}>
              <IconButton style={{ padding: '8px' }} size="small" onClick={() => onClose()}>
                <IconClose />
              </IconButton>
            </div>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', margin: '0px 24px' }}>
            <Typography variant="body2" style={{ paddingTop: '12px', paddingBottom: '8px' }}>
              <FormattedMessage id="contact.contactNameWantAdd" />
            </Typography>

            <BootstrapInput
              onChange={e => this.setState({ search: e.target.value }, this.search)}
              margin="dense"
              style={{
                height: '40px',
              }}
              value={search}
              fullWidth
              placeholder={intl.formatMessage({ id: 'contact.enterCustomerName' })}
              endAdornment={
                <InputAdornment position="end" variant="filled">
                  <IconSearch style={{ color: GREY }} />
                </InputAdornment>
              }
            />

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                paddingTop: '24px',
                minHeight: '40px',
                justifyContent: 'space-between',
              }}
            >
              <Typography variant="subtitle1">
                <FormattedMessage id="contact.customerList" />
              </Typography>

              <Link
                to={{
                  pathname: ROUTES.management,
                  search: `?${CURRENT_PARAM_NAME}=${encodeURIComponent('m.customers')}`,
                }}
              >
                <Button>
                  <Typography variant="body2" style={{ color: BLUE }}>
                    <FormattedMessage id="contact.managerContact" />
                  </Typography>
                </Button>
              </Link>
            </div>
          </div>
          <div style={{ flex: 1, overflow: 'auto', padding: '0 24px' }}>
            {loading ? (
              <div style={{ margin: '40px' }}>
                <LoadingIcon />
              </div>
            ) : (
              <>
                {contactData &&
                  contactData.map(contact => (
                    <ItemRow key={contact.id} onClick={() => onSelect(contact)}>
                      <Typography variant="body2">
                        {contact.lastName} {contact.firstName}
                      </Typography>
                      <Typography variant="body2">{contact.phone}</Typography>
                    </ItemRow>
                  ))}

                {(!contactData || contactData.length === 0) && (
                  <BoxResultMessage>
                    {!isSearch ? (
                      <>
                        <IconPeople />
                        <Typography variant="body2" style={{ paddingTop: '8px', color: DARK_GREY }}>
                          <FormattedMessage id="contact.emptyContact" />
                        </Typography>
                        <Typography
                          variant="body2"
                          style={{ paddingBottom: '16px', color: DARK_GREY }}
                        >
                          <FormattedMessage id="contact.addNewContact" />
                        </Typography>
                      </>
                    ) : (
                      <>
                        <IconNoResult />
                        <Typography
                          variant="body2"
                          style={{ paddingTop: '8px', paddingBottom: '16px', color: DARK_GREY }}
                        >
                          <FormattedMessage id="contact.noResult" />
                        </Typography>
                        <Link
                          to={{
                            pathname: ROUTES.management,
                            search: `?${CURRENT_PARAM_NAME}=${encodeURIComponent('m.customers')}`,
                          }}
                        >
                          <Button
                            variant="contained"
                            size="large"
                            color="secondary"
                            style={{ width: '260px' }}
                          >
                            <Typography variant="button">
                              <FormattedMessage id="contact.goToContactManager" />
                            </Typography>
                          </Button>
                        </Link>
                      </>
                    )}
                  </BoxResultMessage>
                )}
              </>
            )}
          </div>
        </div>
      </Dialog>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {};
}

export default connect(mapStateToProps)(injectIntl(SelectContactDialog));
