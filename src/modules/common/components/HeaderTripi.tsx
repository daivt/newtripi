import { Button, Container, Dialog, Typography } from '@material-ui/core';
import FeedbackOutlinedIcon from '@material-ui/icons/FeedbackOutlined';
import { push } from 'connected-react-router';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Link } from 'react-router-dom';
import { Dispatch } from 'redux';
import { withTheme } from 'styled-components';
import { DEV, TEST, DESKTOP_WIDTH, DINOGO, ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import '../../../scss/svg.scss';
import FlightSvg from '../../../svg/Flight.svg';
import HotelSvg from '../../../svg/Hotel.svg';
import { ReactComponent as IconMenu } from '../../../svg/icon_menu.svg';
import TourismActivities from '../../../svg/ic_activities_travel.svg';
import Flight from '../../../svg/ic_airplane.svg';
import TicketHunt from '../../../svg/ic_find_sale.svg';
import Hotel from '../../../svg/ic_hotel_menu.svg';
import Promotion from '../../../svg/ic_promo.svg';
import { ReactComponent as TripiLogo } from '../../../svg/TripiLogo.svg';
import MobileCodeSvg from '../../../svg/phone_card.svg';
import MobileCodeWhiteSvg from '../../../svg/phone_card_white.svg';
import MobileDirectSvg from '../../../svg/phone_money.svg';
import MobileDirectWhiteSvg from '../../../svg/phone_money_white.svg';
import PromotionSvg from '../../../svg/Promotion.svg';
import TicketHuntSvg from '../../../svg/TicketHunt.svg';
import TourSvg from '../../../svg/TourismActivities.svg';
import Train from '../../../svg/ic_train.svg';
import TrainSvg from '../../../svg/Train.svg';
import Badge from '../../account/component/Badge';
import { AuthDialog, setAuthDialog } from '../../auth/redux/authReducer';
import LanguageSelecter from '../../intl/components/LanguageSelecter';
import { BoxHeader, HeaderContainer, MenuLabel, Sidebar, SignUpLabel, SubHeader, SubMenuItem } from './Header.styles';
import { NewTabLink } from './NewTabLink';
import SendFeedbackBox from './SendFeedbackBox';
import SidebarMenu from './SidebarMenu';

const menus = [
  {
    name: 'header.product',
    subMenu: [
      {
        img: FlightSvg,
        imgLight: Flight,
        id: 'flight',
        link: ROUTES.flight.flight,
      },
      {
        img: HotelSvg,
        imgLight: Hotel,
        id: 'hotel',
        link: ROUTES.hotel.hotel,
      },
      {
        img: TourSvg,
        imgLight: TourismActivities,
        id: 'tour',
        link: ROUTES.tour.tour,
      },
      {
        img: TrainSvg,
        imgLight: Train,
        id: 'trainTickets',
        link: ROUTES.train.train,
      },
      {
        img: TicketHuntSvg,
        imgLight: TicketHunt,
        id: 'ticketHunt',
        link: ROUTES.flight.flightHunt,
      },
      {
        img: MobileCodeSvg,
        imgLight: MobileCodeWhiteSvg,
        id: 'mobile.titleCode',
        link: ROUTES.mobile.mobileCard,
      },
      {
        img: MobileDirectSvg,
        imgLight: MobileDirectWhiteSvg,
        id: 'mobile.titleDirect',
        link: ROUTES.mobile.mobileTopup,
      },
      {
        img: PromotionSvg,
        imgLight: Promotion,
        id: 'promotion',
        link: ROUTES.reward.rewards,
      },
    ],
  },
  {
    name: 'header.partner',
    url: 'https://partner.tripi.vn',
  },
  {
    name: 'header.oliveRoom',
    url: 'https://olive.tripi.vn',
  },
  {
    name: 'header.corporate',
    url: 'https://corporate.tripi.vn',
  },
  {
    name: 'sidebar.help',
    url: 'https://hotro.tripi.vn',
  },
];

const mapStateToProps = (state: AppState) => ({
  auth: state.auth,
  router: state.router,
});

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  light?: boolean;
  theme: some;
}
interface State {
  subMenu: some[];
  openSidebar: boolean;
  light: boolean;
  openFeedbackDialog: boolean;
}

class HeaderTripi extends PureComponent<Props, State> {
  state: State = {
    subMenu: [],
    openSidebar: false,
    light: !!this.props.light,
    openFeedbackDialog: false,
  };

  container = React.createRef<HTMLDivElement>();

  componentDidMount() {
    window.addEventListener('scroll', this.scrollHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
  }

  scrollHandler = () => {
    const divElement = this.container.current;
    if (divElement) {
      if (divElement.offsetTop > 480) {
        this.setState({ light: true });
      } else {
        this.setState({ light: !!this.props.light });
      }
    }
  };

  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        if (this.container.current) {
          this.container.current.focus();
        }
        return;
      }
    }
    this.removeSubMenu();
  };

  removeSubMenu = () => {
    this.setState({ subMenu: [] });
  };

  openSubMenu = (menu: some) => {
    if (menu.subMenu) {
      this.setState({ subMenu: menu.subMenu });
    } else {
      this.setState({ subMenu: [] });
    }
  };

  render() {
    const { dispatch, auth, router, theme } = this.props;
    const { subMenu, openSidebar, light, openFeedbackDialog } = this.state;
    return (
      <HeaderContainer ref={this.container} onBlur={this.onBlur}>
        <BoxHeader
          tabIndex={-1}
          style={{ position: this.props.light ? 'static' : 'absolute', outline: 'none', zIndex: 100 }}
          light={light}
          subMenuCount={subMenu.length}
        >
          <SubHeader light={light}>
            <Container
              style={{
                height: '66px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Sidebar onClick={() => this.setState({ openSidebar: true })}>
                <Button size="small" disableRipple>
                  <IconMenu
                    style={{
                      fill: light ? theme.primary : 'white',
                      stroke: light ? theme.primary : 'white',
                    }}
                    className="svgFill"
                  />
                </Button>
              </Sidebar>
              <Link to="/" style={{ marginRight: '30px' }}>
                <TripiLogo className="svgFillAll" fill={light ? theme.primary : 'white'} />
              </Link>
              <ul
                style={{
                  alignItems: 'center',
                  display: 'flex',
                  justifyContent: 'flex-start',
                  listStyleType: 'none',
                  paddingInlineStart: 0,
                  flexShrink: 0,
                }}
              >
                {menus.map(menu => (
                  <MenuLabel
                    key={menu.name}
                    menu={menu}
                    onClick={() => {
                      if (!subMenu.length) {
                        this.openSubMenu(menu);
                      } else {
                        this.removeSubMenu();
                      }
                    }}
                  >
                    {menu.subMenu ? (
                      <Typography variant="body1" style={{ display: 'flex' }}>
                        <FormattedMessage id={menu.name} />
                        {menu.subMenu && (
                          <span
                            style={{
                              display: 'inline-block',
                              marginLeft: '6px',
                              fontSize: '10px',
                              transition: 'all 300ms',
                              transform: subMenu.length > 0 ? 'rotate(0deg)' : 'rotate(180deg)',
                            }}
                          >
                            &#9650;
                          </span>
                        )}
                      </Typography>
                    ) : (
                      <NewTabLink href={menu.url} style={{ color: 'inherit' }}>
                        <Typography variant="body1">
                          <FormattedMessage id={menu.name} />
                        </Typography>
                      </NewTabLink>
                    )}
                  </MenuLabel>
                ))}
              </ul>
              {auth.auth && (
                <MediaQuery minWidth={DESKTOP_WIDTH}>
                  {match => {
                    if (match) {
                      return (
                        <Button
                          size="medium"
                          color="secondary"
                          variant="contained"
                          onClick={() => this.setState({ openFeedbackDialog: true })}
                        >
                          <FormattedMessage id="header.sendFeedback" />
                        </Button>
                      );
                    }
                    return (
                      <Button
                        size="medium"
                        color="secondary"
                        variant="contained"
                        onClick={() => this.setState({ openFeedbackDialog: true })}
                      >
                        <FeedbackOutlinedIcon />
                      </Button>
                    );
                  }}
                </MediaQuery>
              )}
              {!auth.auth ? (
                <div
                  style={{
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'flex-end',
                    flexShrink: 0,
                    flexGrow: 1,
                  }}
                >
                  {(DINOGO || DEV || TEST) && <LanguageSelecter />}
                  <SignUpLabel light={light} onClick={() => dispatch(push(ROUTES.signUp))}>
                    <Typography variant="body2">
                      <FormattedMessage id="signUp" />
                    </Typography>
                  </SignUpLabel>
                  <Button
                    style={{
                      textTransform: 'none',
                      borderRadius: '18px',
                      minWidth: '135px',
                      backgroundColor: light ? undefined : '#fff',
                    }}
                    color={light ? 'secondary' : 'default'}
                    variant="contained"
                    onClick={() => dispatch(setAuthDialog(AuthDialog.login))}
                  >
                    <Typography
                      variant="body2"
                      style={{
                        color: light ? '#fff' : theme.primary,
                      }}
                    >
                      <FormattedMessage id="signIn" />
                    </Typography>
                  </Button>
                </div>
              ) : (
                <Badge light={light} />
              )}
            </Container>
          </SubHeader>
          <div
            style={{
              boxShadow: light ? '0px 4px 4px rgba(0, 0, 0, 0.25)' : 'none',
              width: '100%',
              color: light ? '#000' : '#fff',
              position: 'absolute',
              backgroundColor: 'inherit',
            }}
          >
            <Container
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                transition: 'all 300ms',
                opacity: subMenu.length > 0 ? 1 : 0,
                height: subMenu.length > 0 ? '100px' : '0px',
              }}
            >
              {subMenu.length > 0 &&
                subMenu.map((item: some) => (
                  <SubMenuItem
                    key={item.id}
                    to={
                      item.link
                        ? {
                            pathname: item.link,
                            state: {
                              backableToHomePage: router.location.pathname === '/',
                            },
                          }
                        : '/'
                    }
                    style={{ height: 100 }}
                    onClick={() => {
                      this.setState({ subMenu: [] });
                    }}
                  >
                    <img
                      alt=""
                      style={{ paddingTop: '15px', paddingBottom: '3px', height: '42px' }}
                      src={light ? item.img : item.imgLight}
                    />
                    <Typography variant="body2" style={{ paddingBottom: '12px', textAlign: 'center' }}>
                      <FormattedMessage id={item.id} />
                    </Typography>
                  </SubMenuItem>
                ))}
            </Container>
          </div>
        </BoxHeader>
        <SidebarMenu openSidebar={openSidebar} onClose={() => this.setState({ openSidebar: false })} />
        <Dialog open={openFeedbackDialog} keepMounted={false} maxWidth="md">
          <SendFeedbackBox onClose={() => this.setState({ openFeedbackDialog: false })} />
        </Dialog>
      </HeaderContainer>
    );
  }
}

export default connect(mapStateToProps)(withTheme(HeaderTripi));
