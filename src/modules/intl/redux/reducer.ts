import { createAction, ActionType, getType } from 'typesafe-actions';
import moment from 'moment';
import { LS_LANG } from '../../../constants';

export interface IntlState {
  readonly locale: string;
}

export const setLocale = (locale: string) => {
  moment.locale(locale);
  localStorage.setItem(LS_LANG, locale);
  return setLocaleAction(locale);
};

const setLocaleAction = createAction('setLocale', resolve => (locale: string) =>
  resolve({ locale }),
);

const actions = { setLocale };

type Action = ActionType<typeof actions>;

export default function reducer(state: IntlState = { locale: 'vi' }, action: Action) {
  switch (action.type) {
    case getType(setLocaleAction):
      return { ...state, locale: action.payload.locale };
    default:
      return state;
  }
}
