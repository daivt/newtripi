import React from 'react';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import { AppState } from '../../../redux/reducers';
import enMessages from '../en.json';
import viMessages from '../vi.json';
import dinogoEnMessages from '../dinogoEn.json';
import dinogoViMessages from '../dinogoVi.json';
import { DINOGO, some } from '../../../constants';

function getMessages(locale: string): some {
  if (locale.startsWith('vi')) {
    if (DINOGO) {
      return { ...viMessages, ...dinogoViMessages };
    }
    return viMessages;
  }
  if (locale.startsWith('en')) {
    if (DINOGO) {
      return { ...enMessages, ...dinogoEnMessages };
    }
    return enMessages;
  }
  return enMessages;
}

function mapStateToProps(state: AppState) {
  return { locale: state.intl.locale, messages: getMessages(state.intl.locale) };
}

interface Props extends ReturnType<typeof mapStateToProps> {}

const ConnectedIntlProvider: React.FunctionComponent<Props> = props => {
  return <IntlProvider {...props} />;
};

export default connect(mapStateToProps)(ConnectedIntlProvider);
