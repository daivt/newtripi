import * as React from 'react';
import { connect } from 'react-redux';
import iconFlagVi from '../../../svg/flag_vi.svg';
import iconFlagEn from '../../../svg/flag_en.svg';
import { Typography, Collapse } from '@material-ui/core';
import styled from 'styled-components';
import { GREY, LIGHT_GREY } from '../../../colors';
import { some } from '../../../constants';
import { Dispatch } from 'redux';
import { setLocale } from '../redux/reducer';
import { AppState } from '../../../redux/reducers';

const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  overflow: hidden;
`;
const Tip = styled.div`
  display: flex;
  align-items: flex-right;
  justify-content: flex-start;
  padding: 8px 12px;
  border-radius: 4px;
  white-space: nowrap;
  &:hover {
    background: ${LIGHT_GREY};
  }
  cursor: pointer;
`;
const data = [
  { src: iconFlagVi, value: 'vi', label: 'Tiếng Việt' },
  { src: iconFlagEn, value: 'en-US', label: 'English' },
];
interface State {
  open: boolean;
  flag: string;
}
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
}
class LanguageSelecter extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      open: false,
      flag: (data.find(one => one.value === props.locale) || data[0]).src,
    };
  }
  onBlur = (e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        return;
      }
    }
    this.setState({ open: false });
  };
  render() {
    const { open, flag } = this.state;
    const { dispatch } = this.props;
    return (
      <div
        style={{
          padding: '0px 18px',
        }}
      >
        <div
          style={{
            cursor: 'pointer',
            display: 'flex',
            justifyContent: 'center',
            outline: 'none',
            alignItems: 'center',
          }}
          tabIndex={-1}
          onClick={() => this.setState({ open: !open })}
          onBlur={this.onBlur}
        >
          <img src={flag} alt="" style={{ width: '38px', height: '25px', objectFit: 'cover' }} />

          <Wrapper
            style={{
              position: 'absolute',
              height: 'auto',
              color: 'black',
              zIndex: 1100,
              background: 'white',
              top: '66px',
              left: 'auto',
            }}
          >
            <Collapse in={open}>
              <div
                style={{
                  display: 'flex',
                  flexFlow: 'column',
                  padding: '8px 12px',
                  boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
                  border: `1px solid ${GREY}`,
                  justifyContent: 'space-between',
                  cursor: 'auto',
                  borderRadius: '4px',
                }}
              >
                {data.map((v: some, index: number) => {
                  return (
                    <Tip
                      key={index}
                      onClick={() => {
                        this.setState({ flag: v.src });
                        dispatch(setLocale(v.value));
                      }}
                    >
                      <img src={v.src} alt="" style={{ width: '38px', height: '25px', objectFit: 'cover' }} />
                      &nbsp;&nbsp;&nbsp;
                      <Typography variant={'body2'}>{v.label}</Typography>
                    </Tip>
                  );
                })}
              </div>
            </Collapse>
          </Wrapper>

          <Typography style={{ marginLeft: '8px' }}>
            <span
              style={{
                display: 'inline-block',
                marginLeft: '6px',
                fontSize: '10px',
                transition: 'all 300ms',
                transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
                cursor: 'pointer',
              }}
            >
              &#9650;
            </span>
          </Typography>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
  };
}

export default connect(mapStateToProps)(LanguageSelecter);
