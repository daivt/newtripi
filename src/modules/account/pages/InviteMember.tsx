import React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps, FormattedMessage } from 'react-intl';
import { Typography, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { replace } from 'connected-react-router';
import { TABLET_WIDTH } from '../../../constants';
import { PageWrapper } from '../../common/components/elements';
import Header from '../../common/components/Header';
import Footer from '../../common/components/Footer';
import { AppState } from '../../../redux/reducers';
import { GREY } from '../../../colors';
import { fetchThunk } from '../../common/redux/thunks';
import { API_PATHS } from '../../../API';
import MessageDialog from '../../common/components/MessageDialog';

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

interface InviteMemberProps extends WrappedComponentProps, ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
}

const InviteMember: React.FunctionComponent<InviteMemberProps> = props => {
  const { intl, router, dispatch } = props;
  const params = new URLSearchParams(router.location.search);
  const inviteToken = params.get('inviteToken');
  const [errorMessage, setMessage] = React.useState('');
  const [successMessage, setSuccessMessage] = React.useState(false);
  const acceptInvite = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(`${API_PATHS.acceptInvite}?token=${inviteToken}`, 'get', true));
    if (json.code === 200) {
      setSuccessMessage(true);
    } else {
      setMessage(json.message);
    }
  }, [dispatch, inviteToken]);
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'title' })}</title>
      </Helmet>
      <PageWrapper style={{ minWidth: TABLET_WIDTH }}>
        <Header light />
        <div style={{ flex: 1, display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
          <Typography variant="h6" style={{ paddingTop: '50px' }}>
            <FormattedMessage id="inviteMember" values={{ name: params.get('agencyName') }} />
          </Typography>
          <div style={{ paddingTop: '10px' }}>
            <Button
              style={{
                width: '110px',
              }}
              variant="contained"
              color="secondary"
              size="large"
              onClick={acceptInvite}
            >
              <Typography variant="button">
                <FormattedMessage id="accept" />
              </Typography>
            </Button>
            <Button
              style={{
                width: '110px',
                border: `1px solid ${GREY}`,
                marginLeft: '16px',
              }}
              size="large"
              onClick={() => {
                dispatch(replace('/'));
              }}
            >
              <Typography color="textPrimary" variant="button">
                <FormattedMessage id="cancel" />
              </Typography>
            </Button>
          </div>
        </div>
        <Footer />
        <MessageDialog
          show={!!errorMessage}
          message={
            <Typography variant="body1" style={{ textAlign: 'center', padding: '16px' }}>
              {errorMessage}
            </Typography>
          }
          onClose={() => setMessage('')}
        />
        <MessageDialog
          show={successMessage}
          message={
            <Typography variant="body1" style={{ textAlign: 'center', padding: '16px' }}>
              <FormattedMessage id="successInvitationAccept" />
            </Typography>
          }
          onClose={() => {
            dispatch(replace('/'));
          }}
        />
      </PageWrapper>
    </>
  );
};

export default connect(mapStateToProps)(injectIntl(InviteMember));
