import { Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { Action } from 'typesafe-actions';
import { GREY, LIGHT_GREY } from '../../../colors';
import { ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconEmail } from '../../../svg/email.svg';
import { ReactComponent as IconEmailOpen } from '../../../svg/email_open.svg';
import Link from '../../common/components/Link';
import { ACTION_PARAM_NAME, CURRENT_PARAM_NAME, NotificationActionType } from '../../management/constants';
import { readNotification } from '../redux/accountReducer';

const Line = styled.div`
  display: flex;
  align-items: center;
  cursor: 'pointer';
  &:hover {
    background: ${LIGHT_GREY};
  }
  cursor: pointer;
`;
interface Props extends ReturnType<typeof mapStateToProps> {
  data: some;
  isHome?: boolean;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const NotificationCard: React.FunctionComponent<Props> = props => {
  const { data, isHome, search, state, dispatch } = props;

  const seeDetail = React.useCallback(
    (id: number) => {
      const params = new URLSearchParams(search);
      params.set(CURRENT_PARAM_NAME, 'm.notification');
      params.set(ACTION_PARAM_NAME, 'detail' as NotificationActionType);
      params.set('id', `${id}`);
      return params.toString();
    },
    [search],
  );
  const backableToList = state && state.backableToList;
  const lastExecuteTime = moment(data.lastExecuteTime, 'HH:mm DD/MM/YYYY');
  const isToday = lastExecuteTime.isSame(moment(), 'day');
  const isYesterday = lastExecuteTime.isSame(moment().subtract(1, 'days'), 'day');
  return (
    <Link
      to={{
        pathname: ROUTES.management,
        search: `?${seeDetail(data.id)}`,
        state: {
          notificationDataDetail: data,
          backableToList: backableToList ? backableToList + 1 : undefined,
        },
      }}
      onClick={() => {
        if (!data.read) {
          dispatch(readNotification(data.id as number));
        }
      }}
    >
      <Line style={{ borderBottom: !isHome ? `1px solid ${GREY}` : undefined }}>
        <div
          style={{
            width: '48px',
            paddingTop: '9px',
            height: '100%',
            alignSelf: 'start',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          {data.read ? <IconEmailOpen /> : <IconEmail />}
        </div>
        <div
          style={{
            flex: 1,
            borderBottom: isHome ? `1px solid ${GREY}` : undefined,
            padding: '9px 16px 12px 0px',
            minHeight: '72px',
            overflow: 'hidden',
          }}
        >
          <Line
            style={{
              justifyContent: 'space-between',
            }}
          >
            <Typography
              variant={data.read ? 'body1' : 'subtitle2'}
              color={data.read ? 'textSecondary' : 'textPrimary'}
              style={{
                flex: 1,
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                maxWidth: '650px',
                overflow: 'hidden',
              }}
            >
              {data.message && data.message.title}
            </Typography>
            <Typography variant="caption" color={data.read ? 'textSecondary' : 'textPrimary'}>
              {isToday ? (
                <FormattedMessage id="today" />
              ) : isYesterday ? (
                <FormattedMessage id="yesterday" />
              ) : (
                data.lastExecuteTime
              )}
            </Typography>
          </Line>
          <Typography
            variant="caption"
            color={data.read ? 'textSecondary' : 'textPrimary'}
            style={{
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {data.message && data.message.description}
          </Typography>
        </div>
      </Line>
    </Link>
  );
};
function mapStateToProps(state: AppState) {
  return {
    search: state.router.location.search,
    state: state.router.location.state,
  };
}
export default connect(mapStateToProps)(NotificationCard);
