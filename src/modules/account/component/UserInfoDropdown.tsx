import { Button, Collapse, Typography } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import { History } from 'history';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { DARK_GREY, GREY, LIGHT_BLUE, RED } from '../../../colors';
import { DESKTOP_WIDTH, ROUTES } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconNext } from '../../../svg/ic_next_black.svg';
import { logout } from '../../auth/redux/authThunks';
import Link, { RawLink } from '../../common/components/Link';
import { CURRENT_PARAM_NAME, RECHARGE_PARAM_NAME } from '../../management/constants';

const Line = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 38px;
  border-bottom: 1px solid ${GREY};
  width: 100%;
`;
const Row = styled.div<{ hover: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px 12px;
  &:hover {
    background: ${props => (props.hover ? fade(`${LIGHT_BLUE}`, 0.2) : undefined)};
  }
  cursor: ${props => (props.hover ? 'pointer' : undefined)};
`;
const mapStateToProps = (state: AppState) => ({
  userData: state.account.userData,
  router: state.router,
});

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const UserInfoDropdown: React.FunctionComponent<Props> = props => {
  const { userData, dispatch } = props;
  const [open, setOpen] = React.useState(false);

  const getLink = React.useCallback((menuItem: string): History.LocationDescriptor => {
    const params = new URLSearchParams();
    params.set(CURRENT_PARAM_NAME, menuItem);
    return { pathname: ROUTES.management, search: params.toString() };
  }, []);

  const onBlur = React.useCallback((e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        return;
      }
    }

    setOpen(false);
  }, []);

  return (
    <>
      {userData && (
        <div
          tabIndex={-1}
          onBlur={onBlur}
          style={{
            outline: 'none',
          }}
        >
          <Button
            style={{
              color: 'inherit',
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'flex-end',
              cursor: 'pointer',
            }}
            onClick={() => {
              setOpen(!open);
            }}
            title={`${userData.phoneInfo || userData.email || userData.name}\nID:${userData.id}`}
          >
            <MediaQuery minWidth={DESKTOP_WIDTH}>
              {match => {
                if (match) {
                  return (
                    <>
                      {userData.profilePhoto ? (
                        <img
                          src={userData && userData.profilePhoto}
                          alt=""
                          style={{
                            borderRadius: '50%',
                            height: '48px',
                            width: '48px',
                            objectFit: 'cover',
                          }}
                        />
                      ) : (
                        <div
                          style={{
                            background: GREY,
                            borderRadius: '50%',
                            height: '48px',
                            width: '48px',
                            textAlign: 'center',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            color: 'white',
                          }}
                        >
                          <span>
                            {userData.name &&
                              userData.name
                                .split(' ')
                                .map((v: string) => v.substring(0, 1).toUpperCase())
                                .join('')}
                          </span>
                        </div>
                      )}
                      <div style={{ marginLeft: '12px', textAlign: 'start' }}>
                        <Typography variant="subtitle2">
                          {userData.phoneInfo || userData.email || userData.name}
                        </Typography>
                        <Typography variant="caption" style={{ color: 'inherit' }}>
                          ID:&nbsp;
                          {userData.id}
                        </Typography>
                      </div>
                      <Typography style={{ marginLeft: '20px' }}>
                        <span
                          style={{
                            display: 'inline-block',
                            marginLeft: '6px',
                            fontSize: '10px',
                            transition: 'all 300ms',
                            transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
                            cursor: 'pointer',
                          }}
                        >
                          &#9650;
                        </span>
                      </Typography>
                    </>
                  );
                }
                return (
                  <>
                    {userData.profilePhoto ? (
                      <img
                        src={userData && userData.profilePhoto}
                        alt=""
                        style={{
                          borderRadius: '50%',
                          height: '48px',
                          width: '48px',
                          objectFit: 'cover',
                        }}
                      />
                    ) : (
                      <div
                        style={{
                          background: GREY,
                          borderRadius: '50%',
                          height: '48px',
                          width: '48px',
                          textAlign: 'center',
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                          color: 'white',
                        }}
                      >
                        <span>
                          {userData.name &&
                            userData.name
                              .split(' ')
                              .map((v: string) => v.substring(0, 1).toUpperCase())
                              .join('')}
                        </span>
                      </div>
                    )}
                  </>
                );
              }}
            </MediaQuery>
          </Button>
          <div
            style={{
              position: 'absolute',
              width: '290px',
              color: 'black',
              zIndex: 110,
              top: '66px',
              right: '0px',
            }}
          >
            <Collapse in={open}>
              <div
                style={{
                  background: 'white',
                  boxShadow:
                    '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
                  justifyContent: 'space-between',
                  cursor: 'auto',
                  overflow: 'hidden',
                  borderRadius: '4px',
                }}
              >
                <Row hover={false}>
                  <Line style={{ justifyContent: 'normal', height: '100%', padding: '16px 0px 8px' }}>
                    <img
                      style={{
                        width: '33px',
                        height: '33px',
                        alignSelf: 'flex-start',
                        marginRight: '16px',
                      }}
                      src={userData.userTitle.icon}
                      alt=""
                    />
                    <div>
                      <Typography variant="subtitle2">{userData.userTitle.name}</Typography>
                      <div style={{ display: 'flex', marginTop: '8px', alignItems: 'center' }}>
                        <Typography variant="caption">
                          <FormattedMessage id="account" />
                          :&nbsp;
                        </Typography>
                        <Typography variant="caption" style={{ color: RED }}>
                          <FormattedNumber value={userData.credit} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      </div>
                      <div style={{ display: 'flex', marginTop: '8px', alignItems: 'center' }}>
                        <Typography
                          variant="caption"
                          style={{
                            color: DARK_GREY,
                            wordBreak: 'break-word',
                          }}
                        >
                          <FormattedMessage id="account.pointCanUse" />
                          :&nbsp;
                        </Typography>
                        <Typography variant="caption" color="primary">
                          <FormattedNumber value={userData.loyalty.availablePoint} />
                          &nbsp;
                          <FormattedMessage id="point" />
                        </Typography>
                      </div>
                      <Link to={{ pathname: ROUTES.management }}>
                        <Button
                          size="large"
                          color="primary"
                          variant="contained"
                          style={{ minWidth: '144px', marginTop: '8px' }}
                        >
                          <FormattedMessage id="m.title" />
                        </Button>
                      </Link>
                    </div>
                  </Line>
                </Row>
                <Row hover={false} />
                <RawLink to={{ pathname: ROUTES.reward.myRewards }}>
                  <Row hover>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="account.rewardNumber" />
                      </Typography>
                      <div style={{ display: 'flex' }}>
                        <Typography variant="body2">{userData.loyaltyInfo.rewardNum}</Typography>
                        <IconNext style={{ padding: '0px 8px 0px 16px', height: '16px', width: 'auto' }} />
                      </div>
                    </Line>
                  </Row>
                </RawLink>
                <RawLink to={getLink('m.orders')}>
                  <Row hover>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="m.ordersManagement" />
                      </Typography>
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconNext style={{ padding: '0px 8px 0px 16px', height: '16px', width: 'auto' }} />
                      </div>
                    </Line>
                  </Row>
                </RawLink>
                <RawLink to={getLink('m.profile')}>
                  <Row hover>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="account.information" />
                      </Typography>
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconNext style={{ padding: '0px 8px 0px 16px', height: '16px', width: 'auto' }} />
                      </div>
                    </Line>
                  </Row>
                </RawLink>
                <div
                  style={{
                    marginTop: '12px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    textAlign: 'center',
                    fontWeight: 500,
                    padding: '12px',
                  }}
                >
                  <Link
                    to={{
                      pathname: ROUTES.management,
                      search: `?${RECHARGE_PARAM_NAME}=recharge`,
                    }}
                  >
                    <Button
                      size="large"
                      color="secondary"
                      variant="contained"
                      style={{
                        minWidth: '114px',
                        display: !userData.isAgencyManager ? 'none' : undefined,
                      }}
                    >
                      <FormattedMessage id="account.recharge" />
                    </Button>
                  </Link>
                  <Button
                    variant="outlined"
                    style={{
                      minWidth: '114px',
                    }}
                    onClick={() => {
                      dispatch(logout());
                    }}
                    size="large"
                  >
                    <Typography style={{ color: '#757575' }}>
                      <FormattedMessage id="signOut" />
                    </Typography>
                  </Button>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
      )}
    </>
  );
};

export default connect(mapStateToProps)(UserInfoDropdown);
