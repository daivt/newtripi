import { Button, Collapse, IconButton, Typography } from '@material-ui/core';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedMessage } from 'react-intl';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { BLUE, DARK_GREY, GREY, SECONDARY } from '../../../colors';
import { ROUTES, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconBellBlack } from '../../../svg/ic_bell_black.svg';
import Link from '../../common/components/Link';
import LoadingIcon from '../../common/components/LoadingIcon';
import NoDataResult from '../../common/components/NoDataResult';
import { CURRENT_PARAM_NAME } from '../../management/constants';
import { addMoreNotification, fetchNotification, readAllNotification } from '../redux/accountReducer';
import NotificationCard from './NotificationCard';

interface Props extends ReturnType<typeof mapStateToProps> {
  light: boolean;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}
const NotificationDropdown: React.FunctionComponent<Props> = props => {
  const { dispatch, light, data } = props;
  const [open, setOpen] = React.useState(false);
  React.useEffect(() => {
    dispatch(fetchNotification());
  }, [dispatch]);
  const loadMore = React.useCallback(() => {
    dispatch(addMoreNotification());
  }, [dispatch]);
  const seeAll = React.useCallback(async () => {
    dispatch(readAllNotification());
  }, [dispatch]);
  const onBlur = React.useCallback((e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        return;
      }
    }
    setOpen(false);
  }, []);
  const getLink = React.useCallback(() => {
    const params = new URLSearchParams();
    params.set(CURRENT_PARAM_NAME, 'm.notification');
    return params.toString();
  }, []);
  return (
    <div
      tabIndex={-1}
      onBlur={onBlur}
      style={{
        outline: 'none',
      }}
    >
      <IconButton
        style={{
          marginRight: '10px',
          position: 'relative',
        }}
        onClick={() => {
          setOpen(!open);
        }}
      >
        <IconBellBlack className="svgFill" style={{ stroke: light ? DARK_GREY : 'white' }} />
        {data && data.numberNotiUnread > 0 && (
          <span
            style={{
              position: 'absolute',
              top: '6px',
              right: '4px',
              background: SECONDARY,
              fontSize: '10px',
              color: 'white',
              width: '20px',
              height: '20px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '50%',
            }}
          >
            {data.numberNotiUnread > 50 ? '50+' : data.numberNotiUnread}
          </span>
        )}
      </IconButton>

      <div
        style={{
          position: 'absolute',
          width: '424px',
          color: 'black',
          zIndex: 110,
          top: '66px',
          right: 0,
        }}
      >
        {data && (
          <Collapse in={open} unmountOnExit>
            <div
              style={{
                background: 'white',
                border: `1px solid ${GREY}`,
                boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
                justifyContent: 'space-between',
                cursor: 'auto',
                overflow: 'hidden',
                borderRadius: '4px',
                display: 'flex',
                flexDirection: 'column',
                height: 'calc(100vh - 100px)',
              }}
            >
              <>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    padding: '16px 18px',
                    justifyContent: 'space-between',
                    borderBottom: `1px solid ${GREY}`,
                  }}
                >
                  <Typography variant="h6">
                    <FormattedMessage id="home.newNotification" />
                  </Typography>
                  {data.numberNotiUnread > 0 && (
                    <Button variant="text" style={{ padding: '0px' }} onClick={seeAll}>
                      <Typography variant="body2" style={{ color: BLUE }}>
                        <FormattedMessage id="home.markReadAll" values={{ num: data.numberNotiUnread }} />
                      </Typography>
                    </Button>
                  )}
                </div>
                <PerfectScrollbar options={{ wheelPropagation: false }} style={{ flex: 1 }}>
                  {data.notificationList.length > 0 ? (
                    <InfiniteScroll
                      pageStart={1}
                      initialLoad={false}
                      loadMore={loadMore}
                      hasMore={data.notificationList.length < data.total}
                      loader={
                        <LoadingIcon
                          key="loader"
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        />
                      }
                      useWindow={false}
                    >
                      <div>
                        {data.notificationList.map((v: some, index: number) => (
                          <NotificationCard key={v.id} data={v} isHome />
                        ))}
                      </div>
                    </InfiniteScroll>
                  ) : (
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: '100%',
                      }}
                    >
                      <NoDataResult id="m.notification.noNewData" />
                    </div>
                  )}
                </PerfectScrollbar>
                <div
                  style={{
                    height: '42px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: BLUE,
                  }}
                >
                  {data.notificationList.length > 0 && (
                    <Link to={{ pathname: ROUTES.management, search: getLink() }}>
                      <Button variant="text" style={{ padding: '0px' }}>
                        <Typography variant="body2">
                          <FormattedMessage id="seeAll" />
                        </Typography>
                      </Button>
                    </Link>
                  )}
                </div>
              </>
            </div>
          </Collapse>
        )}
      </div>
    </div>
  );
};
const mapStateToProps = (state: AppState) => {
  return {
    data: state.account.notification.data,
  };
};
export default connect(mapStateToProps)(NotificationDropdown);
