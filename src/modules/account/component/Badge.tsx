import * as React from 'react';
import { DEV, TEST } from '../../../constants';
import LanguageSelecter from '../../intl/components/LanguageSelecter';
import NotificationDropdown from './NotificationDropdown';
import UserInfoDropdown from './UserInfoDropdown';

interface Props {
  light: boolean;
}

const Badge: React.FunctionComponent<Props> = props => {
  const { light } = props;
  return (
    <div
      style={{
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'flex-end',
        flexShrink: 0,
        flexGrow: 1,
        position: 'relative',
      }}
    >
      {(DEV || TEST) && <LanguageSelecter />}
      <NotificationDropdown light={light} />
      <UserInfoDropdown />
    </div>
  );
};

export default Badge;
