import { get } from 'js-cookie';
import moment from 'moment';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from '../../../API';
import { DATE_FORMAT, PAGE_SIZE, some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { ACCESS_TOKEN } from '../../auth/constants';
import { fetchThunk } from '../../common/redux/thunks';

interface Booker {
  readonly dashboad?: Readonly<some>;
  readonly request?: Readonly<some>;
  readonly report?: Readonly<some>;
}
export interface ParamsNotification {
  type: string | null;
  term: string | null;
  pageSize: number;
  page: number;
}
interface NotificationState {
  data?: some;
  params: ParamsNotification;
  loading: boolean;
}
export interface AccountState {
  readonly userData?: Readonly<some>;
  readonly booker: Booker;
  readonly notification: NotificationState;
}

export const setUserData = createAction('account/setUserData', resolve => (data?: some) => resolve({ data }));
export const setNotification = createAction('account/setNotification', resolve => (data: some) => resolve({ data }));
export const addNotification = createAction('account/addNotification', resolve => (data: some) => resolve({ data }));
export const setLoadingNotification = createAction('account/setLoadingNotification', resolve => (val: boolean) =>
  resolve({ val }),
);
export const setParamsNotification = createAction(
  'account/setParamsNotification',
  resolve => (val: ParamsNotification) => resolve({ val }),
);
export const setBooker = createAction('auth/setBooker', resolve => (dashboard: some, request: some, report: some) =>
  resolve({ dashboard, request, report }),
);
export function fetchNotification(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { params } = getState().account.notification;
    dispatch(setParamsNotification({ ...params, page: 1 }));
    const json = await dispatch(
      fetchThunk(`${API_PATHS.getNotificationList}`, 'post', true, JSON.stringify({ ...params, page: 1 })),
    );
    if (json.code === 200) {
      dispatch(setNotification(json.data));
    }
  };
}
export function addMoreNotification(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const { params } = getState().account.notification;
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getNotificationList}`,
        'post',
        true,
        JSON.stringify({ ...params, page: params.page + 1 }),
      ),
    );
    if (json.code === 200) {
      dispatch(addNotification(json.data));
      dispatch(setParamsNotification({ ...params, page: params.page + 1 }));
    }
  };
}
export function readAllNotification(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(`${API_PATHS.readAllNotification}`, 'post', true, '{}'));
    if (json.code === 200) {
      dispatch(fetchNotification());
    }
  };
}
export function readNotification(id: number): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const notiData = getState().account.notification.data;
    dispatch(
      setNotification({
        ...notiData,
        notificationList:
          notiData &&
          notiData.notificationList.map((v: some) => {
            if (v.id === id) {
              return { ...v, read: true };
            }
            return v;
          }),
        numberNotiUnread: notiData && notiData.numberNotiUnread > 0 ? notiData.numberNotiUnread - 1 : 0,
      }),
    );
    await dispatch(fetchThunk(`${API_PATHS.readNotification}`, 'post', true, JSON.stringify({ notificationId: id })));
  };
}
export function deteleAllNotification(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(`${API_PATHS.deleteAllNotification}`, 'post', true, '{}'));
    if (json.code === 200) {
      dispatch(fetchNotification());
    }
  };
}
export function fetchBookerInfo(): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const accessToken = get(ACCESS_TOKEN);
    if (accessToken) {
      const fromDate = moment()
        .startOf('month')
        .format(DATE_FORMAT);
      const toDate = moment()
        .endOf('month')
        .format(DATE_FORMAT);
      const [json1, json2, json3] = await Promise.all([
        dispatch(fetchThunk(`${API_PATHS.bookerDashboard}`, 'get', true)),
        dispatch(
          fetchThunk(
            `${API_PATHS.bookerRequest}`,
            'post',
            true,
            JSON.stringify({
              paging: { page: 1, pageSize: 20 },
              filters: {
                fromDate: null,
                toDate: null,
                modules: null,
                types: null,
                status: ['open'],
                bookingId: null,
              },
              sortBy: null,
            }),
          ),
        ),
        dispatch(
          fetchThunk(
            `${API_PATHS.bookingsReport}`,
            'post',
            true,
            JSON.stringify({
              fromDate,
              toDate,
            }),
          ),
        ),
      ]);
      // if (json1.code === 200 && json2.code === 200 && json3.code === 200) {
      dispatch(setBooker(json1.data, json2.data, json3.data));
      // }
    }
  };
}
export const DEFAULT_ACCOUNT_STATE = {
  notification: {
    params: { page: 1, pageSize: PAGE_SIZE, term: null, type: null },
    loading: false,
  },
  booker: {},
};
const actions = {
  setUserData,
  setBooker,
  setNotification,
  addNotification,
  setParamsNotification,
  setLoadingNotification,
};

type ActionT = ActionType<typeof actions>;

export default function reducer(state: AccountState = DEFAULT_ACCOUNT_STATE, action: ActionT): AccountState {
  switch (action.type) {
    case getType(setNotification):
      return { ...state, notification: { ...state.notification, data: action.payload.data } };
    case getType(addNotification):
      return {
        ...state,
        notification: {
          ...state.notification,
          data: {
            ...action.payload.data,
            notificationList: state.notification.data
              ? state.notification.data.notificationList.concat(action.payload.data.notificationList)
              : action.payload.data.notificationList,
          },
        },
      };
    case getType(setParamsNotification):
      return {
        ...state,
        notification: {
          ...state.notification,
          params: action.payload.val,
        },
      };
    case getType(setLoadingNotification):
      return {
        ...state,
        notification: {
          ...state.notification,
          loading: action.payload.val,
        },
      };
    case getType(setUserData):
      return { ...state, userData: action.payload.data };
    case getType(setBooker):
      return {
        ...state,
        booker: {
          dashboad: action.payload.dashboard,
          request: action.payload.request,
          report: action.payload.report,
        },
      };
    default:
      return state;
  }
}
