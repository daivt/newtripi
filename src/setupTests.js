/* eslint-disable import/no-extraneous-dependencies */
import { JSDOM } from 'jsdom';
import matchMediaPolyfill from 'mq-polyfill';

const jsdom = new JSDOM('<!doctype html><html><body></body></html>', { runScripts: 'dangerously' });
const { window } = jsdom;

function copyProps(src, target) {
  Object.defineProperties(target, {
    ...Object.getOwnPropertyDescriptors(src),
    ...Object.getOwnPropertyDescriptors(target),
  });
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'Automated tester',
};
global.requestAnimationFrame = callback => {
  return setTimeout(callback, 0);
};
global.cancelAnimationFrame = id => {
  clearTimeout(id);
};

// mock so Slick can compile
matchMediaPolyfill(global.window);
// mock localStorage and session storage
global.window.localStorage = {
  setItem(name, value) {
    this[name] = value;
  },

  getItem(name) {
    return this[name];
  },

  removeItem(name) {
    this[name] = null;
  },
};
global.window.sessionStorage = {
  setItem(name, value) {
    this[name] = value;
  },

  getItem(name) {
    return this[name];
  },

  removeItem(name) {
    this[name] = null;
  },
};
global.window.scrollTo = () => {};

copyProps(window, global);
